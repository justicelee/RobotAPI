add_subdirectory(interface)
add_subdirectory(libraries)
add_subdirectory(components)
add_subdirectory(drivers)
add_subdirectory(statecharts)
add_subdirectory(gui-plugins)

add_subdirectory(applications)

