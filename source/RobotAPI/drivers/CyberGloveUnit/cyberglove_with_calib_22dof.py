#!/usr/bin/env python

import serial
import os
import time
import signal
import sys
import tty
import termios

import numpy as np

import sys, traceback, Ice
import IceStorm
import time

from thread import start_new_thread, allocate_lock

os.system("clear")

status = 0
ic = None

class bcolors:
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

print bcolors.OKBLUE+"""
   ______      __              ________              
  / ____/_  __/ /_  ___  _____/ ____/ /___ _   _____ 
 / /   / / / / __ \/ _ \/ ___/ / __/ / __ \ | / / _ \\
/ /___/ /_/ / /_/ /  __/ /  / /_/ / / /_/ / |/ /  __/
\____/\__, /_.___/\___/_/   \____/_/\____/|___/\___/ 
     /____/                                          
""" + bcolors.ENDC

print bcolors.WARNING + "Permission needed to open ports for gloves. If prompted, please type your sudo password below." + bcolors.ENDC
#os.system("sudo chmod 777 /dev/ttyUSB0")
#os.system("sudo chmod 777 /dev/ttyUSB1")

print ""
print ""
print ""

PI = 3.14159

def signal_handler(signal, frame):
    print '\nExiting...'
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

right_port = '/dev/ttyUSB0'

right_ser = serial.Serial(right_port, 115200, timeout=1)
right_ser.write("1dw") #disable wifi
right_ser.write("1eu") #enable usb
right_ser.write("1ds") #disable sd card
right_ser.write("1S") #enable data streaming

right_s = ""
right_cal = ["" for x in range(33)]
right_time = ""
glove_data_lock = allocate_lock()
show_stream = 1
show_cal_stream = 0

class CalibStruct():
    gain = np.zeros(24)
    offset = np.zeros(24)

calib_fac = CalibStruct()
#calib_fac.gain = np.zeros(24)
#calib_fac.offset = np.zeros(24)

def read_glove():

    ########################################################## ICE part
    try:
        properties = Ice.createProperties(sys.argv)
        properties.load("./default.generated.cfg")
        properties.load("./default.cfg")

        init_data = Ice.InitializationData()
        init_data.properties = properties

        ic = Ice.initialize(init_data)

        Ice.loadSlice('-I%s %s -DMININTERFACE' % (Ice.getSliceDir(), 'CyberGloveInterface.ice'))
        print("loadSlice")
        import armarx


        obj = ic.stringToProxy("IceStorm/TopicManager")
        print("stringToProxy")

        topicMan = IceStorm.TopicManagerPrx.checkedCast(obj)
        print("checkedCast")
        topicName = "CyberGloveValues"
        print(topicName)

        try:
            topic = topicMan.retrieve(topicName);
        except IceStorm.NoSuchTopic:
            print("No such topic.")
            try:

                topic = topicMan.create(topicName)
                print("Try to create topic.")

            except IceStorm.TopicExists:
                # if the topic has been created in the meanwhile
                # retry the retrieval.
                topic = topicMan.retrieve(topicName)
                print("Try again.")

        pub = topic.getPublisher().ice_oneway();
        glove = armarx.CyberGloveListenerInterfacePrx.uncheckedCast(pub);


        #base = ic.stringToProxy("SimpleprosthesisInterfaceUnit")

        #prosthesis = armarx.SimpleprosthesisInterfacePrx.checkedCast(base)
        if not glove:
                raise RuntimeError("Invalid proxy")

        #val = 0
        #print(val)
    
    except:
        traceback.print_exc()
        status = 1

    ############################################################ Glove part

    Rcache = right_ser.read(61)
    #Rcache = Rcache[15:]
    Rcache = Rcache[Rcache.index("S")+1:]
    Rcache = Rcache + right_ser.read(61-len(Rcache))
    
    while(1):
		
        ######RIGHT SENSE#######
        #right_s = ""
        #right_ser.write("1G")
        #right_ser = serial.Serial(right_port, 115200, timeout=0)
        global right_s
        #global right_cal
        global show_stream
        #global show_cal_stream
        #global right_ser
        #right_ser.write("1G")
        #Rcache = right_ser.read(53)
        #right_s = right_s[15:]
        #right_s = right_s[right_s.index("S")-13:right_s.index("S")+50]
        #print len(right_s)
        glove_data_lock.acquire()
        right_s = right_ser.read(61)
        if len(right_s) != 0 and right_s[13] != "S":
            print "Missed beginning."
            #print right_s[10]
            right_s = right_s[right_s.index("S"):]
            cut_len = 48 - len(right_s)
            if cut_len <= 0:
                cut_len = 14
            right_s = right_s + right_ser.read(cut_len)
            #print len(right_s)
            #print right_s[0]
            #print right_s[39]
            right_s = ""
        if len(right_s) != 0 and show_stream:
            timestamp = ""
            sensorData = np.zeros(23)
            i = 0;
            for n in range(14):
                #print right_s[n]
                timestamp = timestamp + right_s[n]
            for n in range(14, len(right_s)-1, 2):
                #print ord(right_s[n])
                #print ord(right_s[n+1])
                dataReading =  256*ord(right_s[n]) + ord(right_s[n+1])
                #print dataReading
                sensorData[i] = dataReading
                i = i + 1
            print str(len(right_s)) + " " + timestamp + " " + str(time.time())
            values = armarx.CyberGloveValues(name = "gloveValues", time = timestamp, thumbCMC = sensorData[0], thumbMCP = sensorData[1], thumbIP = sensorData[2], thumbAbd = sensorData[3], indexMCP = sensorData[4], indexPIP = sensorData[5], indexDIP = sensorData[6], middleMCP = sensorData[7], middlePIP = sensorData[8], middleDIP = sensorData[9], middleAbd = sensorData[10], ringMCP = sensorData[11], ringPIP = sensorData[12], ringDIP = sensorData[13], ringAbd = sensorData[14], littleMCP = sensorData[15], littlePIP = sensorData[16], littleDIP = sensorData[17], littleAbd = sensorData[18], palmArch = sensorData[19], wristFlex = sensorData[20], wristAbd = sensorData[21])
            glove.reportGloveValues(values)

        glove_data_lock.release()
        #right_glove_value = right_s[38].encode('hex')
        #print int(glove_value, 16)-open_avg
        #print "RIGHT is released"

def main_loop():
    global show_stream
    global show_cal_stream
    while(1):
        c = raw_input("Choose raw sensor value (s), constant raw stream (r), glove calibration (c), stop stream (d) or quit (q)")
        if c == "s":
            #print "lock is acquired"
            glove_data_lock.acquire()
            #right_ser.write("1G")
            global right_s
            for n in range(14):
                print right_s[n]
            for n in range(14, len(right_s)-1, 2):
                print 256*ord(right_s[n]) + ord(right_s[n+1])
                #print ord(right_s[n])
                #print ord(right_s[n+1])
            glove_data_lock.release()
        elif c == "r":
            show_stream = 1
        elif c == "t":
            glove_data_lock.acquire()
            act_s = right_s
            glove_data_lock.release()
            if len(act_s) != 0:
                for n in range(14):
                    right_cal[n] = act_s[n]
                    #if show_cal_stream:
                    print right_cal[n]
                for n in range(14, len(act_s)-1, 2):
                    #print ord(right_s[n])
                    #print ord(right_s[n+1])
                    right_cal[(n-14)/2 + 14] = (256*ord(act_s[n]) + ord(act_s[n+1]) - calib_fac.offset[(n-14)/2])*calib_fac.gain[(n-14)/2]
                    #if show_cal_stream:
                    print right_cal[(n-14)/2 + 14] 

        elif c == "c":
            calibrate_glove()
        elif c == "d":
            show_stream = 0
        elif c == "q":
            break

def calibrate_glove():
    global calib_fac
    raw_input("Flat hand straight fingers pose.")
    glove_data_lock.acquire()
    for finger in range(5):
        for joint in range(4):
            if joint == 3:
                if finger <= 1:
                    break
            if joint == 0:
                if finger == 0:
                    joint = joint + 1
            if finger == 0:
                sensorNo = joint
                calNo = sensorNo
            elif finger == 1:
                sensorNo = 4 + joint
                calNo = sensorNo
            else:
                sensorNo = 7 + (finger - 2)*4 + joint
                calNo = finger*4 + joint
            sensorNo = sensorNo*2 + 14 
            calib_fac.offset[calNo] = 256*ord(right_s[sensorNo]) + ord(right_s[sensorNo + 1])
            print "Finger" + str(finger)
            print "Joint" + str(joint)
            print sensorNo
            print calNo
            print calib_fac.offset[calNo]
    calib_fac.offset[20] = 256*ord(right_s[52]) + ord(right_s[53])
    glove_data_lock.release()

    calNo = 1
    sensorNo = calNo*2 + 14
    fingerAngle = 37
    raw_input("Thumb proximal joint bent 37 degrees.")
    glove_data_lock.acquire()
    calib_fac.gain[calNo] = (fingerAngle*PI/180)/(256*ord(right_s[sensorNo]) + ord(right_s[sensorNo+1]) - calib_fac.offset[calNo])
    print 256*ord(right_s[sensorNo]) + ord(right_s[sensorNo+1])
    glove_data_lock.release()
    #print "Thumb1"
    print calib_fac.gain[calNo]

    calNo = 2
    sensorNo = calNo*2 + 14
    fingerAngle = 76
    raw_input("Thumb distal joint bent 76 degrees.")
    glove_data_lock.acquire()
    calib_fac.gain[calNo] = (fingerAngle*PI/180)/(256*ord(right_s[sensorNo]) + ord(right_s[sensorNo+1]) - calib_fac.offset[calNo])
    print 256*ord(right_s[sensorNo]) + ord(right_s[sensorNo+1])
    glove_data_lock.release()
    print calib_fac.gain[calNo]

    for finger in range(1,5):
        for joint in range(3):
            fingerAngle = 76
            if finger == 1:
                calNo = 4 + joint
                sensorNo = calNo*2 + 14
            else:
                calNo = finger*4 + joint
                sensorNo = (7 + (finger - 2)*4 + joint)*2 + 14
            raw_input("Finger " + str(finger) + ", joint " + str(joint) + " bent " + str(fingerAngle) + " degrees.")
            glove_data_lock.acquire()
            calib_fac.gain[calNo] = (fingerAngle*PI/180)/(256*ord(right_s[sensorNo]) + ord(right_s[sensorNo+1]) - calib_fac.offset[calNo])
            print 256*ord(right_s[sensorNo]) + ord(right_s[sensorNo+1])
            glove_data_lock.release()
            print calib_fac.gain[calNo]

    raw_input("Finger abducted with 25, 20 and 21 degrees.")
    glove_data_lock.acquire()
    calib_fac.gain[11] = (25*PI/180)/(256*ord(right_s[34]) + ord(right_s[35]) - calib_fac.offset[11])
    print "Middle: " + str(256*ord(right_s[34]) + ord(right_s[35]))
    print calib_fac.gain[11]
    calib_fac.gain[15] = (20*PI/180)/(256*ord(right_s[42]) + ord(right_s[43]) - calib_fac.offset[15])
    print "Ring: " + str(256*ord(right_s[42]) + ord(right_s[43]))
    print calib_fac.gain[15]
    calib_fac.gain[19] = (21*PI/180)/(256*ord(right_s[50]) + ord(right_s[51]) - calib_fac.offset[19])
    print "Little: " + str(256*ord(right_s[50]) + ord(right_s[51]))
    print calib_fac.gain[19]
    glove_data_lock.release()

    raw_input("Thumb metacarpal extension 90 degrees.")
    glove_data_lock.acquire()
    calib_fac.gain[0] = (90*PI/180)/(256*ord(right_s[14]) + ord(right_s[15]) - calib_fac.offset[0])
    glove_data_lock.release()

    raw_input("Thumb fully abducted.")
    glove_data_lock.acquire()
    calib_fac.offset[3] = 256*ord(right_s[20]) + ord(right_s[21])
    glove_data_lock.release()

    raw_input("Thumb adducted 76 degrees.")
    glove_data_lock.acquire()
    calib_fac.gain[3] = (76*PI/180)/(256*ord(right_s[20]) + ord(right_s[21]) - calib_fac.offset[3])
    glove_data_lock.release()

    fingerAngle = input("Palm arch, please enter acquired angle: ")
    print fingerAngle
    glove_data_lock.acquire()
    calib_fac.gain[20] = (fingerAngle*PI/180)/(256*ord(right_s[52]) + ord(right_s[53]) - calib_fac.offset[20])
    glove_data_lock.release()
    
    file = open("generateGloveCalibration.cal","w")
 
    file.write("VTi CyberGlove Calibration File (v2.0.0)") 
    for finger in range(6):
        file.write("Finger " + str(finger) + ":")
        for joint in range(4):
            file.write("   " + str(joint) + "   " + str(calib_fac.offset[finger*4 + joint]) + "   " + str(calib_fac.gain[finger*4 + joint]) + "   0")
 
    file.close() 

start_new_thread(read_glove,())
main_loop()

if ic:
    #clean up
    try:
        ic.destroy()
    except:
        traceback.print_exc()
        status = 1

    sys.exit(status)
#read_glove()
