/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <vector>
#include <Eigen/Core>

#include "CalibrationInfo.h"

namespace armarx
{
    class CalibrationHelper
    {
    public:
        CalibrationHelper(int rows, int cols, float noiseThreshold);

        void addNoiseSample(Eigen::MatrixXf data);
        bool addMaxValueSample(Eigen::MatrixXf data);

        CalibrationInfo getCalibrationInfo(float calibratedMinimum, float calibratedMaximum);

        bool checkMaximumValueThreshold(float threshold);

        Eigen::MatrixXf getMaximumValues();

        int getNoiseSampleCount();

    private:
        std::vector<Eigen::MatrixXf> noiseSamples;
        Eigen::MatrixXf maximumValues;
        float noiseThreshold;

        Eigen::MatrixXf getMatrixAverage(std::vector<Eigen::MatrixXf> samples);
    };
}

