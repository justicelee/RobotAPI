/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Peter Kaiser
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WeissHapticUnit.h"

#include <boost/regex.hpp>
#include <filesystem>


using namespace armarx;

void WeissHapticUnit::onInitHapticUnit()
{

    std::vector<std::string> devices = getDevices();

    for (std::vector<std::string>::iterator it = devices.begin(); it != devices.end(); ++it)
    {
        WeissHapticSensorPtr sensor(new WeissHapticSensor(*it, 20)); // minimumReportIntervalMs = 20, limit to maximum 50 frames/s
        this->sensors.push_back(sensor);
    }

    std::cout << "Connect Interfaces" << std::endl;

    for (std::vector<std::shared_ptr<WeissHapticSensor> >::iterator it = sensors.begin(); it != sensors.end(); ++it)
    {
        (*it)->connect();
    }

}

std::vector< std::string > WeissHapticUnit::getDevices()
{
    const std::string target_path("/dev/");
    const boost::regex my_filter("ttyACM[0-9]+");

    std::vector< std::string > files;

    std::filesystem::directory_iterator end_itr; // Default ctor yields past-the-end

    for (std::filesystem::directory_iterator i(target_path); i != end_itr; ++i)
    {
        // Skip if not a file
        //if( !std::filesystem::is_( i->status() ) ) continue;

        boost::smatch what;

        // Skip if no match
        if (!boost::regex_match(i->path().filename().string(), what, my_filter))
        {
            continue;
        }


        // File matches, store it
        files.push_back("/dev/" + i->path().filename().string());
    }

    std::sort(files.begin(), files.end());

    if (files.size() == 0)
    {
        ARMARX_WARNING << "No ACM-Interfaces found";
    }
    else
    {
        ARMARX_INFO << "Detected ACM-Interfaces: " << files.size();

        for (std::string file : files)
        {
            ARMARX_INFO << "Found device: " << file;
        }
    }

    return files;
}

void WeissHapticUnit::setDeviceTag(const std::string& deviceName, const std::string& tag, const Ice::Current&)
{
    for (WeissHapticSensorPtr sensor : sensors)
    {
        if (sensor->getDeviceName() == deviceName)
        {
            ARMARX_IMPORTANT << "scheduling to set new device tag for " << deviceName << ": " << tag;
            sensor->scheduleSetDeviceTag(tag);
            return;
        }
    }

    ARMARX_WARNING << "device not found: " << deviceName;
}

void WeissHapticUnit::startLogging(const Ice::Current&)
{
    // @@@ TODO NotImplemented
}

void WeissHapticUnit::stopLogging(const Ice::Current&)
{
    // @@@ TODO NotImplemented
}

void WeissHapticUnit::onStartHapticUnit()
{

    for (std::vector<std::shared_ptr<WeissHapticSensor> >::iterator it = sensors.begin(); it != sensors.end(); ++it)
    {
        (*it)->setListenerPrx(hapticTopicPrx);
        (*it)->startSampling();
    }

}

void WeissHapticUnit::onExitHapticUnit()
{
    for (std::vector<std::shared_ptr<WeissHapticSensor> >::iterator it = sensors.begin(); it != sensors.end(); ++it)
    {
        (*it)->disconnect();
    }
}

/*void WeissHapticUnit::onConnectComponent()
{

}*/

void WeissHapticUnit::onDisconnectComponent()
{
}


PropertyDefinitionsPtr WeissHapticUnit::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new WeissHapticUnitPropertyDefinitions(getConfigIdentifier()));
}

