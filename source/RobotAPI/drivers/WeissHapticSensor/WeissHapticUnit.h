/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar4::units
 * @author     Simon Ottenhaus <simon dot ottenhaus at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/HapticUnit.h>
#include <RobotAPI/interface/units/WeissHapticUnit.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <string>
#include "WeissHapticSensor.h"

namespace armarx
{
    class WeissHapticUnitPropertyDefinitions : public HapticUnitPropertyDefinitions
    {
    public:
        WeissHapticUnitPropertyDefinitions(std::string prefix):
            HapticUnitPropertyDefinitions(prefix)
        {
        }
    };

    class WeissHapticUnit :
        virtual public WeissHapticUnitInterface,
        virtual public HapticUnit
    {
    public:
        virtual std::string getDefaultName()
        {
            return "WeissHapticUnit";
        }

        void onInitHapticUnit() override;
        void onStartHapticUnit() override;
        void onExitHapticUnit() override;

        //virtual void onConnectComponent();
        void onDisconnectComponent() override;

        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        //void proceedSensorCategory(SensorCategoryDefinition<MatrixFloat> *category);

        //std::map<std::string, MatrixFloatPtr> currentValues;



        //HapticSensorProtocolMaster hapticProtocol;

        //bool remoteSystemReady;

    private:
        std::vector< std::string > getDevices();

        std::vector< std::shared_ptr< WeissHapticSensor > > sensors;

        // WeissHapticUnitInterface interface
    public:
        void setDeviceTag(const std::string& deviceName, const std::string& tag, const Ice::Current&) override;
        void startLogging(const Ice::Current&) override;
        void stopLogging(const Ice::Current&) override;
    };
}

