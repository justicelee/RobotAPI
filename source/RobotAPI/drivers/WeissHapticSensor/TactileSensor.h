/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "AbstractInterface.h"

#define extract_short( array, index )               ( (unsigned short)array[index] | ( (unsigned short)array[index + 1] << 8 ) )
#define TAC_CHECK_RES( res, expected, resp )    { \
        if ( res < expected ) { \
            dbgPrint( "Response length is too short, should be = %d (is %d)\n", expected, res ); \
            if ( res > 0 ) free( resp ); \
            return -1; \
        } \
        status_t status = cmd_get_response_status( resp ); \
        if ( status != E_SUCCESS ) \
        { \
            dbgPrint( "Command not successful: %s\n", status_to_str( status ) ); \
            free( resp ); \
            return -1; \
        } \
    }

typedef struct
{
    int res_x; // Horizontal matrix resolution. For non-rectangular matrices, this is the horizontal resolution of the surrounding rectangle measured in sensor cells.
    int res_y; // Vertical matrix resolution. For non-rectangular matrices, this is the vertical resolution of the surrounding rectangle measured in sensor cells.
    int cell_width; // Width of one sensor cell in 1/100 millimeters.
    int cell_height; // Height of one sensor cell in 1/100 millimeters.
    int fullscale; // Full scale value of the output signal.
} tac_matrix_info_t;

typedef struct
{
    unsigned char type; // System Type: 0 - unknown, 4 - WTS Tactile Sensor Module
    unsigned char hw_rev; // Hardware Revision
    unsigned short fw_version; // Firmware Version: D15...12: major version, D11...8: minor version 1, D7..4 minor version 2, D3..0: 0 for release version, 1..15 for release candidate versions
    unsigned short sn; // Serial Number of the device
} tac_system_information_t;

struct FrameData
{
public:
    FrameData(std::shared_ptr<std::vector<short> > data, int count)
        : data(data), count(count)
    {}
    std::shared_ptr<std::vector<short> > data;
    int count;
};
struct PeriodicFrameData
{
public:
    PeriodicFrameData(std::shared_ptr<std::vector<short> > data, int count, unsigned int timestamp)
        : data(data), count(count), timestamp(timestamp)
    {}
    std::shared_ptr<std::vector<short> > data;
    int count;
    unsigned int timestamp;
};

class TactileSensor
{
public:
    TactileSensor(std::shared_ptr<AbstractInterface> interface);
    virtual ~TactileSensor();

    tac_matrix_info_t getMatrixInformation();
    static void printMatrixInfo(tac_matrix_info_t* mi);
    FrameData readSingleFrame();
    static void printMatrix(short* matrix, int width, int height);

    void startPeriodicFrameAcquisition(unsigned short delay_ms);
    void stopPeriodicFrameAcquisition(void);
    PeriodicFrameData receicePeriodicFrame();
    void tareSensorMatrix(unsigned char operation);
    void setAquisitionWindow(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2);
    int setAdvanvedAcquisitionMask(char* mask);
    int getAcquisitionMask(char** mask, int* mask_len);
    void setThreshold(short threshold);
    unsigned short getThreshold();
    void setFrontEndGain(unsigned char gain);
    unsigned char getFrontEndGain();
    std::string getSensorType();
    float readDeviceTemperature();
    tac_system_information_t getSystemInformation();
    static void printSystemInformation(tac_system_information_t si);
    void setDeviceTag(std::string tag);
    std::string getDeviceTag();
    bool tryGetDeviceTag(std::string& tag);
    int loop(char* data, int data_len);

    std::string getInterfaceInfo();

private:
    std::shared_ptr<AbstractInterface> interface;
    FrameData getFrameData(Response* response);
    PeriodicFrameData getPeriodicFrameData(Response* response);

private:
    friend std::ostream& operator<<(std::ostream&, const TactileSensor&);
};

std::ostream& operator<<(std::ostream& strm, const TactileSensor& a);

