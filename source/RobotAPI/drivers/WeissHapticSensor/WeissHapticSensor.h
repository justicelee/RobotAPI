/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include "SerialInterface.h"
#include "TactileSensor.h"
#include <RobotAPI/interface/units/HapticUnit.h>
#include <ArmarXCore/util/variants/eigen3/Eigen3VariantObjectFactories.h>
//#include <ArmarXCore/util/variants/eigen3/Eigen3LibRegistry.h>
#include "TextWriter.h"
#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <RobotAPI/libraries/core/math/SlidingWindowVectorMedian.h>
#include <ArmarXCore/util/variants/eigen3/MatrixVariant.h>

#include <mutex>

namespace armarx
{

    class WeissHapticSensor;
    using WeissHapticSensorPtr = std::shared_ptr<WeissHapticSensor>;

    class WeissHapticSensor : public Logging
    {
    public:
        WeissHapticSensor(std::string device, int minimumReportIntervalMs);

        void connect();
        void disconnect();

        void setListenerPrx(HapticUnitListenerPrx listenerPrx);
        void startSampling();

        std::string getDeviceName();
        void scheduleSetDeviceTag(std::string tag);

    private:
        RunningTask<WeissHapticSensor>::pointer_type sensorTask;
        std::shared_ptr<TextWriter> jsWriter;
        void frameAcquisitionTaskLoop();
        std::string device;
        std::string deviceFileName;
        std::shared_ptr<SerialInterface> interface;
        std::shared_ptr<TactileSensor> sensor;
        bool connected;
        std::string tag;
        tac_matrix_info_t mi;
        HapticUnitListenerPrx listenerPrx;
        void writeMatrixToJs(MatrixFloatPtr matrix, TimestampVariantPtr timestamp);

        bool setDeviceTagScheduled;
        std::string setDeviceTagValue;

        std::mutex mutex;
        int minimumReportIntervalMs;
    };
}

