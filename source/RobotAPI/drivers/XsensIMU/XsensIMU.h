/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::XsensIMU
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>


#include <RobotAPI/components/units/InertialMeasurementUnit.h>

#include "IMU/IMU.h"

namespace armarx
{
    /**
     * @class XsensIMUPropertyDefinitions
     * @brief
     */
    class XsensIMUPropertyDefinitions:
        public InertialMeasurementUnitPropertyDefinitions
    {
    public:
        XsensIMUPropertyDefinitions(std::string prefix):
            InertialMeasurementUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("deviceConnection", "/dev/ttyUSB0", "");

            defineOptionalProperty<std::string>("frequency", "", "");
            defineOptionalProperty<std::string>("maxPendingEvents", "", "");
            defineOptionalProperty<bool>("EnableSimpleCalibration", false, "");
        }
    };

    /**
     * @class XsensIMU
     * @brief A brief description
     *
     * Detailed Description
     */
    class XsensIMU :
        virtual public InertialMeasurementUnit,
        virtual public IMU::CIMUDeducedReckoning
    {
    public:


        XsensIMU(): CIMUDeducedReckoning(false)
        {

        }

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "XsensIMU";
        }

    protected:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

        void frameAcquisitionTaskLoop();

        // InertialMeasurementUnit interface

        void onInitIMU() override;
        void onStartIMU() override;
        void onExitIMU() override;


    private:

        RunningTask<XsensIMU>::pointer_type sensorTask;
        IMU::CIMUDevice IMUDevice;
    };
}

