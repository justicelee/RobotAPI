/*
 * IMU.cpp
 *
 *  Created on: Mar 16, 2014
 *      Author: Dr.-Ing. David Israel González Aguirre
 *      Mail:   david.gonzalez@kit.edu
 */

#include "IMUDevice.h"
#include "IIMUEventDispatcher.h"

namespace IMU
{
    CIMUDevice::CIMUDevice() :
        m_DeviceId(0), m_SamplingFrequency(SamplingFrequency(0)), m_PeriodMicroSeconds(0), m_FusionStrategy(eNoFusion), m_SamplesPerFusion(0), m_CollectedFusionSamples(0), m_IsActive(false), m_IsDispatching(false), m_IsInitialized(false), m_pInternalThreadHandel(0), m_IMUEventDispatchers(), m_ReferenceTimeStamp(CTimeStamp::s_Zero), m_LastFrameTimeStamp(CTimeStamp::s_Zero)

#ifdef _IMU_USE_XSENS_DEVICE_

        , m_pXsensMTiModule(nullptr)
#endif

    {
        pthread_mutex_init(&m_IsActiveMutex, nullptr);
        pthread_mutex_init(&m_IsDispatchingMutex, nullptr);
        pthread_mutex_init(&m_EventDispatchersMutex, nullptr);
        pthread_mutex_init(&m_DeviceMutex, nullptr);
    }

    CIMUDevice::~CIMUDevice()
    {
        FinalizeModuleDevice();
    }

    uint64_t CIMUDevice::GetDeviceId() const
    {
        return m_DeviceId;
    }

    bool CIMUDevice::Connect(const std::string& PortName, const SamplingFrequency Frequency)
    {
        if (m_IsInitialized)
        {
            return true;
        }

        if (!PortName.length())
        {
            std::cerr << "[IMU Error: Cannot connect to empty port name!]\n\t[Operation result: (PortName.length()==0)]\n[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            return false;
        }

        m_IsInitialized = InitializeDevice(PortName, Frequency);

        return m_IsInitialized;
    }

    bool CIMUDevice::Start(const bool Blocking)
    {
        if (m_IsInitialized && (!m_IsActive))
        {
            const int Result = pthread_create(&m_pInternalThreadHandel, nullptr, CIMUDevice::ThreadLoop, (void*) this);

            if (Result == 0)
            {
                while (Blocking && !m_IsActive)
                {
                    pthread_yield();
                }

                return true;
            }
        }

        return false;
    }

    void CIMUDevice::Stop(const bool Blocking)
    {
        if (m_IsActive)
        {
            _MINIMAL___LOCK(m_IsActiveMutex)
            m_IsActive = false;
            _MINIMAL_UNLOCK(m_IsActiveMutex)
            pthread_join(m_pInternalThreadHandel, nullptr);

            while (Blocking && m_IsDispatching)
            {
                pthread_yield();
            }
        }
    }

    bool CIMUDevice::SetFusion(const FusionStrategy Strategy, const ushort SamplesPerFusion)
    {
        if (SamplesPerFusion > 1)
        {
            if ((m_FusionStrategy != Strategy) || (m_SamplesPerFusion != SamplesPerFusion))
            {
                m_FusionStrategy = Strategy;
                m_SamplesPerFusion = SamplesPerFusion;
                m_CollectedFusionSamples = 0;
            }

            return true;
        }
        else
        {
            std::cerr << "[IMU Device error: Cannot set fusion with less than 2 samples per fusion!]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            return false;
        }
    }

    bool CIMUDevice::IsActive() const
    {
        return m_IsActive;
    }

    bool CIMUDevice::RegisterEventDispatcher(IIMUEventDispatcher* pIMUEventDispatcher)
    {
        if (pIMUEventDispatcher)
        {
            _MINIMAL___LOCK(m_EventDispatchersMutex)

            if (m_IMUEventDispatchers.find(pIMUEventDispatcher) == m_IMUEventDispatchers.end())
            {
                pIMUEventDispatcher->SetIMU(this);
                pIMUEventDispatcher->SetReferenceTimeStamps(m_ReferenceTimeStamp);
                std::pair<std::set<IIMUEventDispatcher*>::iterator, bool> Result = m_IMUEventDispatchers.insert(pIMUEventDispatcher);
                _MINIMAL_UNLOCK(m_EventDispatchersMutex)
                return Result.second;
            }

            _MINIMAL_UNLOCK(m_EventDispatchersMutex)
        }

        return false;
    }

    bool CIMUDevice::UnregisterEventDispatcher(IIMUEventDispatcher* pIMUEventDispatcher)
    {
        if (pIMUEventDispatcher)
        {
            _MINIMAL___LOCK(m_EventDispatchersMutex)
            std::set<IIMUEventDispatcher*>::iterator ppElement = m_IMUEventDispatchers.find(pIMUEventDispatcher);

            if (ppElement != m_IMUEventDispatchers.end())
            {
                pIMUEventDispatcher->SetIMU(nullptr);
                m_IMUEventDispatchers.erase(ppElement);
                _MINIMAL_UNLOCK(m_EventDispatchersMutex)
                return true;
            }

            _MINIMAL_UNLOCK(m_EventDispatchersMutex)
        }

        return false;
    }

    void CIMUDevice::UnregisterEventDispatchers()
    {
        if (m_IMUEventDispatchers.size())
        {
            _MINIMAL___LOCK(m_EventDispatchersMutex)

            for (auto m_IMUEventDispatcher : m_IMUEventDispatchers)
            {
                m_IMUEventDispatcher->SetIMU(nullptr);
            }

            m_IMUEventDispatchers.clear();
            _MINIMAL_UNLOCK(m_EventDispatchersMutex)
        }
    }

#ifdef _IMU_USE_XSENS_DEVICE_

    bool CIMUDevice::InitializeXsensDevice(const std::string& PortName, const SamplingFrequency Frequency)
    {
        if (m_IsInitialized)
        {
            return true;
        }

        m_pXsensMTiModule = new Xsens::CXsensMTiModule();

        if (m_pXsensMTiModule->openPort(PortName.c_str()) != MTRV_OK)
        {
            std::cerr << "[IMU Device error: Cannot open port!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            DestroyXsensModuleDevice();
            return false;
        }

        if (m_pXsensMTiModule->writeMessage(MID_GOTOCONFIG) != MTRV_OK)
        {
            std::cerr << "[IMU Device error: Cannot set configuration state!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            DestroyXsensModuleDevice();
            return false;
        }

        if (m_pXsensMTiModule->setDeviceMode(OUTPUTMODE_CALIB | OUTPUTMODE_ORIENT, OUTPUTSETTINGS_ORIENTMODE_QUATERNION | OUTPUTSETTINGS_TIMESTAMP_SAMPLECNT) != MTRV_OK)
        {
            std::cerr << "[IMU Device error: Cannot set output mode!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            DestroyXsensModuleDevice();
            return false;
        }

        if (m_pXsensMTiModule->setSetting(MID_SETPERIOD, Frequency, LEN_PERIOD) != MTRV_OK)
        {
            std::cerr << "[IMU Device error: Cannot set sampling period!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            DestroyXsensModuleDevice();
            return false;
        }

        unsigned long DeviceId;

        if (m_pXsensMTiModule->reqSetting(MID_REQDID, DeviceId) != MTRV_OK)
        {
            std::cerr << "[IMU Device error: Cannot get device ID!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            DestroyXsensModuleDevice();
            return false;
        }

        m_DeviceId = DeviceId;

        if (m_pXsensMTiModule->writeMessage(MID_GOTOMEASUREMENT) != MTRV_OK)
        {
            std::cerr << "[IMU Device error: Cannot enter measurement state!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            DestroyXsensModuleDevice();
            return false;
        }

        return true;
    }

    void CIMUDevice::FinalizeXsensModuleDevice()
    {
        if (m_IsInitialized)
        {
            while (m_IsActive || m_IsDispatching)
            {
                pthread_yield();
            }

            _MINIMAL___LOCK(m_DeviceMutex)
            DestroyXsensModuleDevice();
            _MINIMAL_UNLOCK(m_DeviceMutex)
        }
    }

    void CIMUDevice::DestroyXsensModuleDevice()
    {
        if (m_pXsensMTiModule)
        {
            if (m_pXsensMTiModule->isPortOpen())
            {
                m_pXsensMTiModule->close();
            }

            delete m_pXsensMTiModule;
            m_pXsensMTiModule = nullptr;
            m_DeviceId = 0;
        }
    }

#endif

    bool CIMUDevice::LoadCurrentState()
    {

        _MINIMAL___LOCK(m_DeviceMutex)

#ifdef _IMU_USE_XSENS_DEVICE_

        if (m_pXsensMTiModule->readDataMessage(m_XsensMTiFrame.m_Data, m_XsensMTiFrame.m_DataLength) == MTRV_OK)
            if (m_pXsensMTiModule->getValue(VALUE_CALIB_ACC, m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_Acceleration, m_XsensMTiFrame.m_Data) == MTRV_OK)
                if (m_pXsensMTiModule->getValue(VALUE_CALIB_GYR, m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_GyroscopeRotation, m_XsensMTiFrame.m_Data) == MTRV_OK)
                    if (m_pXsensMTiModule->getValue(VALUE_CALIB_MAG, m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_MagneticRotation, m_XsensMTiFrame.m_Data) == MTRV_OK)
                        if (m_pXsensMTiModule->getValue(VALUE_ORIENT_QUAT, m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_QuaternionRotation, m_XsensMTiFrame.m_Data) == MTRV_OK)
                            if (m_pXsensMTiModule->getValue(VALUE_SAMPLECNT, m_XsensMTiFrame.m_IMUState.m_ControlData.m_CurrentSampleCount, m_XsensMTiFrame.m_Data) == MTRV_OK)
                            {
                                if (m_XsensMTiFrame.m_IMUState.m_ControlData.m_PreviousSampleCount != -1)
                                {
                                    m_XsensMTiFrame.m_IMUState.m_ControlData.m_IsConsecutive = ((m_XsensMTiFrame.m_IMUState.m_ControlData.m_PreviousSampleCount + 1) % 65536) == m_XsensMTiFrame.m_IMUState.m_ControlData.m_CurrentSampleCount;
                                }

                                m_XsensMTiFrame.m_IMUState.m_ControlData.m_PreviousSampleCount = m_XsensMTiFrame.m_IMUState.m_ControlData.m_CurrentSampleCount;
                                m_XsensMTiFrame.m_IMUState.m_ControlData.m_MessageCounter++;
                                gettimeofday(&m_XsensMTiFrame.m_IMUState.m_ControlData.m_TimeStamp, nullptr);
                                m_LastFrameTimeStamp = m_XsensMTiFrame.m_IMUState.m_ControlData.m_TimeStamp;

                                m_XsensMTiFrame.m_IMUState.m_PhysicalData.UpdateAccelerationMagnitud();

                                _MINIMAL_UNLOCK(m_DeviceMutex)
                                return true;
                            }
                            else
                            {
                                std::cerr << "[IMU Device error: Fail to get sample count!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
                            }
                        else
                        {
                            std::cerr << "[IMU Device error: Fail to get quaternion rotation!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
                        }
                    else
                    {
                        std::cerr << "[IMU Device error: Fail to get magnetic rotation!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
                    }
                else
                {
                    std::cerr << "[IMU Device error: Fail to get gyroscope rotation!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
                }
            else
            {
                std::cerr << "[IMU Device error: Fail to get acceleration vector!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            }
        else
        {
            std::cerr << "[IMU Device error: Fail to read message!]\n\t[Operation result: " << m_pXsensMTiModule->getLastRetVal() << "]\n\t[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
        }

#endif

        _MINIMAL_UNLOCK(m_DeviceMutex)

        return false;
    }

    bool CIMUDevice::MeanFuseCurrentState()
    {
        IncorporateCurrentStateMeanFusion();

        if (m_CollectedFusionSamples == m_SamplesPerFusion)
        {
            MeanFusion();
            return true;
        }

        return false;
    }

    void CIMUDevice::IncorporateCurrentStateMeanFusion()
    {
        m_FusedPhysicalData.m_Acceleration[0] += m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_Acceleration[0];
        m_FusedPhysicalData.m_Acceleration[1] += m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_Acceleration[1];
        m_FusedPhysicalData.m_Acceleration[2] += m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_Acceleration[2];
        ++m_CollectedFusionSamples;
    }

    void CIMUDevice::MeanFusion()
    {
        //Execution the fusion
        const float NormalizationFactor = 1.0f / float(m_CollectedFusionSamples);
        m_FusedIMUState.m_PhysicalData.m_Acceleration[0] = m_FusedPhysicalData.m_Acceleration[0] * NormalizationFactor;
        m_FusedIMUState.m_PhysicalData.m_Acceleration[1] = m_FusedPhysicalData.m_Acceleration[1] * NormalizationFactor;
        m_FusedIMUState.m_PhysicalData.m_Acceleration[2] = m_FusedPhysicalData.m_Acceleration[2] * NormalizationFactor;

        //Derivated from fusion
        m_FusedIMUState.m_PhysicalData.UpdateAccelerationMagnitud();

        //Reset counters and accumulators
        memset(m_FusedPhysicalData.m_Acceleration, 0, sizeof(float) * 3);
        m_CollectedFusionSamples = 0;
    }

    bool CIMUDevice::GaussianFuseCurrentState()
    {
        IncorporateCurrentStateGaussianFusion();

        if (m_CollectedFusionSamples == m_SamplesPerFusion)
        {
            GaussianFusion();
            return true;
        }

        return false;
    }

    void CIMUDevice::IncorporateCurrentStateGaussianFusion()
    {
        m_FusedPhysicalData.m_Acceleration[0] += m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_Acceleration[0];
        m_FusedPhysicalData.m_Acceleration[1] += m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_Acceleration[1];
        m_FusedPhysicalData.m_Acceleration[2] += m_XsensMTiFrame.m_IMUState.m_PhysicalData.m_Acceleration[2];

        ++m_CollectedFusionSamples;
    }

    void CIMUDevice::GaussianFusion()
    {
        //Execution the fusion
        const float NormalizationFactor = 1.0f / float(m_CollectedFusionSamples);
        m_FusedIMUState.m_PhysicalData.m_Acceleration[0] = m_FusedPhysicalData.m_Acceleration[0] * NormalizationFactor;
        m_FusedIMUState.m_PhysicalData.m_Acceleration[1] = m_FusedPhysicalData.m_Acceleration[1] * NormalizationFactor;
        m_FusedIMUState.m_PhysicalData.m_Acceleration[2] = m_FusedPhysicalData.m_Acceleration[2] * NormalizationFactor;

        //Derivated from fusion
        m_FusedIMUState.m_PhysicalData.UpdateAccelerationMagnitud();

        //Reset counters and accumulators
        memset(m_FusedPhysicalData.m_Acceleration, 0, sizeof(float) * 3);
        m_CollectedFusionSamples = 0;
    }

    bool CIMUDevice::IntegrateCurrentState()
    {
        if (m_FusionStrategy == eNoFusion)
        {
            return IntegrateWithOutFusion();
        }
        else
        {
            return IntegrateWithFusion();
        }
    }

    bool CIMUDevice::IntegrateWithOutFusion()
    {
        return true;
    }

    bool CIMUDevice::IntegrateWithFusion()
    {
        return true;
    }

    bool CIMUDevice::InitializeDevice(const std::string& PortName, const SamplingFrequency Frequency)
    {

        _MINIMAL___LOCK(m_DeviceMutex)

#ifdef _IMU_USE_XSENS_DEVICE_

        if (InitializeXsensDevice(PortName, Frequency))
        {
            m_SamplingFrequency = Frequency;
            const int Cylces = 0X1C200 / int(m_SamplingFrequency);
            m_PeriodMicroSeconds = int(round((1000000.0f * _IMU_DEVICE_DEFAUL_CHECK_PERIOD_FACTOR_) / float(Cylces)));
            _MINIMAL_UNLOCK(m_DeviceMutex)
            return true;
        }
        else
        {
            _MINIMAL_UNLOCK(m_DeviceMutex)
            return false;
        }

#endif

    }

    void CIMUDevice::FinalizeModuleDevice()
    {
        Stop(true);

#ifdef _IMU_USE_XSENS_DEVICE_

        FinalizeXsensModuleDevice();

#endif

        UnregisterEventDispatchers();

    }

    void CIMUDevice::ShouldYield()
    {
        const long int RemainingTime = m_PeriodMicroSeconds - CTimeStamp::GetElapsedMicroseconds(m_LastFrameTimeStamp);

        if (RemainingTime > 0)
        {
            usleep(__useconds_t(RemainingTime));
        }
    }

    bool CIMUDevice::DispatchCylcle()
    {
        if (LoadCurrentState())
        {
            SendEvent(CIMUEvent(m_LastFrameTimeStamp, CIMUEvent::eOnIMUCycle, this));

            switch (m_FusionStrategy)
            {
                case eMeanFusion:
                    if (MeanFuseCurrentState())
                    {
                        SendEvent(CIMUEvent(m_LastFrameTimeStamp, CIMUEvent::eOnIMUFusedCycle, this, m_FusedIMUState));

                        if (IntegrateCurrentState())
                        {
                            SendEvent(CIMUEvent(m_LastFrameTimeStamp, CIMUEvent::eOnIMUIntegratedState, this, m_IntegratedIMUState));
                        }
                    }

                    break;

                case eGaussianFusion:
                    if (GaussianFuseCurrentState())
                    {
                        SendEvent(CIMUEvent(m_LastFrameTimeStamp, CIMUEvent::eOnIMUFusedCycle, this, m_FusedIMUState));

                        if (IntegrateCurrentState())
                        {
                            SendEvent(CIMUEvent(m_LastFrameTimeStamp, CIMUEvent::eOnIMUIntegratedState, this, m_IntegratedIMUState));
                        }
                    }

                    break;

                case eNoFusion:


                    if (IntegrateCurrentState())
                    {
                        SendEvent(CIMUEvent(m_LastFrameTimeStamp, CIMUEvent::eOnIMUFusedCycle, this, m_FusedIMUState));
                        SendEvent(CIMUEvent(m_LastFrameTimeStamp, CIMUEvent::eOnIMUIntegratedState, this, m_IntegratedIMUState));
                    }

                    break;
            }

            return true;
        }

        return false;
    }

    void CIMUDevice::SendEvent(const CIMUEvent& Event)
    {
        _MINIMAL___LOCK(m_EventDispatchersMutex)
        const unsigned long int TotalDispatchers = m_IMUEventDispatchers.size();

        if (TotalDispatchers)
        {
            if (TotalDispatchers == 1)
            {
                (*m_IMUEventDispatchers.begin())->ReceiveEvent(Event);
            }
            else
                for (auto m_IMUEventDispatcher : m_IMUEventDispatchers)
                {
                    m_IMUEventDispatcher->ReceiveEvent(Event);
                }
        }

        _MINIMAL_UNLOCK(m_EventDispatchersMutex)
    }

    void CIMUDevice::SetReferenceTimeStamps()
    {
        _MINIMAL___LOCK(m_EventDispatchersMutex)
        gettimeofday(&m_ReferenceTimeStamp, nullptr);
        gettimeofday(&m_LastFrameTimeStamp, nullptr);

        if (m_IMUEventDispatchers.size())
            for (auto m_IMUEventDispatcher : m_IMUEventDispatchers)
            {
                m_IMUEventDispatcher->SetReferenceTimeStamps(m_ReferenceTimeStamp);
            }

        _MINIMAL_UNLOCK(m_EventDispatchersMutex)
    }

    bool CIMUDevice::SetThreadRunnigMode(const ThreadPolicyType ThreadPolicy, const float NormalizedPriority)
    {
        if (m_IsActive)
        {
            int Policy = -1;
            struct sched_param SchedulingParameters;

            if (pthread_getschedparam(m_pInternalThreadHandel, &Policy, &SchedulingParameters) == 0)
            {
                const int MaximalPriority = sched_get_priority_max(ThreadPolicy);
                const int MinimalPriority = sched_get_priority_min(ThreadPolicy);
                const int PriorityRange = MaximalPriority - MinimalPriority;
                const int Priority = int(round(float(PriorityRange) * NormalizedPriority + float(MinimalPriority)));
                SchedulingParameters.sched_priority = Priority;
                const int Result = pthread_setschedparam(m_pInternalThreadHandel, ThreadPolicy, &SchedulingParameters);

                switch (Result)
                {
                    case 0:
                        return true;
                        break;

                    case EINVAL:
                        std::cerr << "[IMU Device error: SetThreadRunnigMode() returns EINVAL!]\n[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
                        break;

                    case ENOTSUP:
                        std::cerr << "[IMU Device error: SetThreadRunnigMode() returns ENOTSUP!]\n[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
                        break;

                    case EPERM:
                        std::cerr << "[IMU Device error: SetThreadRunnigMode() returns EPERM!]\n[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
                        break;

                    case ESRCH:
                        std::cerr << "[IMU Device error: SetThreadRunnigMode() returns ESRCH!]\n[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
                        break;
                }
            }

            return false;
        }
        else
        {
            std::cerr << "[IMU Device error: SetThreadRunnigMode() cannot set running mode while thread is not active!]\n[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
            return false;
        }
    }

    void* CIMUDevice::ThreadLoop(void* pData)
    {
        if (pData)
        {

            CIMUDevice* pIMUDevice = (CIMUDevice*) pData;

            _MINIMAL___LOCK(pIMUDevice->m_IsActiveMutex)
            pIMUDevice->m_IsActive = true;
            _MINIMAL_UNLOCK(pIMUDevice->m_IsActiveMutex)

            pIMUDevice->SetReferenceTimeStamps();

            pIMUDevice->SendEvent(CIMUEvent(CIMUEvent::eOnIMUStart, pIMUDevice));

            while (pIMUDevice->m_IsActive)
            {
                pIMUDevice->ShouldYield();

                _MINIMAL___LOCK(pIMUDevice->m_IsDispatchingMutex)
                pIMUDevice->m_IsDispatching = true;

                const bool DispatchingResult = pIMUDevice->DispatchCylcle();

                pIMUDevice->m_IsDispatching = false;
                _MINIMAL_UNLOCK(pIMUDevice->m_IsDispatchingMutex)

                if (!DispatchingResult)
                {
                    std::cerr << "[IMU Device error: DispatchCylcle() returns false!]\n[Source location: " << __FILE__ << ":" << __LINE__ << "]" << std::endl;
                    break;
                }

            }

            if (pIMUDevice->m_IsActive)
            {
                _MINIMAL___LOCK(pIMUDevice->m_IsActiveMutex)
                pIMUDevice->m_IsActive = false;
                _MINIMAL_UNLOCK(pIMUDevice->m_IsActiveMutex)
            }

            pIMUDevice->SendEvent(CIMUEvent(CIMUEvent::eOnIMUStop, pIMUDevice));
        }

        return nullptr;
    }
}
