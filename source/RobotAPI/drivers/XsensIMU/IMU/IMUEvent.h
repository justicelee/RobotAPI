/*
 * IMUEvent.h
 *
 *  Created on: Mar 16, 2014
 *      Author: Dr.-Ing. David Israel González Aguirre
 *      Mail:   david.gonzalez@kit.edu
 */

#pragma once

#include "IMUHelpers.h"
#include "IMUState.h"

namespace IMU
{
    class CIMUDevice;
    class CIMUEvent
    {
    public:

        enum EventType
        {
            eOnIMUStart = 0X0001, eOnIMUStop = 0X0002, eOnIMUCycle = 0X0004, eOnIMUFusedCycle = 0X0008, eOnIMUIntegratedState = 0X0010, eOnIMUCustomEvent = 0X8000
        };

        CIMUEvent(const timeval& TimeStamp, const EventType EventType, const CIMUDevice* pIMUDevice, const IMUState& EventState);
        CIMUEvent(const timeval& TimeStamp, const EventType EventType, const CIMUDevice* pIMUDevice);
        CIMUEvent(const EventType EventType, const CIMUDevice* pIMUDevice);
        CIMUEvent(const CIMUEvent& Event);
        CIMUEvent();

        virtual ~CIMUEvent();

        inline uint32_t GetId() const
        {
            return m_Id;
        }

        inline EventType GetEventType() const
        {
            return m_EventType;
        }

        inline const CIMUDevice* GetIMU() const
        {
            return m_pIMUDevice;
        }

        inline const timeval& GetTimeStamp() const
        {
            return m_TimeStamp;
        }

    protected:

        uint32_t m_Id;
        const timeval m_TimeStamp;
        const EventType m_EventType;
        const CIMUDevice* m_pIMUDevice;
        const IMUState m_IMUState;

    private:

        static uint32_t CreatId();
        static uint32_t s_IdCounter;
        static pthread_mutex_t s_IdCounterMutex;
    };
}

