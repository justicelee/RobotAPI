/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::HokuyoLaserUnit
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <RobotAPI/components/units/SensorActorUnit.h>
#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <RobotAPI/interface/components/RobotHealthInterface.h>

#include <HokuyoLaserScannerDriver/urg_sensor.h>
#include <vector>

namespace armarx
{
    /**
     * @class HokuyoLaserUnitPropertyDefinitions
     * @brief
     */
    class HokuyoLaserUnitPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        HokuyoLaserUnitPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("LaserScannerTopicName", "LaserScans", "Name of the laser scan topic.");
            defineOptionalProperty<int>("UpdatePeriod", 25, "Update period for laser scans");
            defineOptionalProperty<float>("AngleOffset", -2.3561944902, "Offset is applied the raw angles before reporting them");
            defineOptionalProperty<std::string>("Devices", "", "List of devices in form of 'IP1,port1,frame1;IP2,port2,frame2;...'");
            defineOptionalProperty<std::string>("RobotHealthTopicName", "RobotHealthTopic", "Name of the RobotHealth topic");
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
        }
    };

    struct HokuyoLaserScanDevice
    {
        std::string ip;
        int port = 0;
        std::string frame;
        float angleOffset = 0.0f;
        bool connected = false;
        urg_t urg;
        std::vector<long> lengthData;
        int errorCounter = 0;

        bool reconnect();

        void run();

        RunningTask<HokuyoLaserScanDevice>::pointer_type task;
        LaserScan scan;

        std::string componentName;
        LaserScannerUnitListenerPrx scanTopic;
        RobotHealthInterfacePrx robotHealthTopic;
        DebugObserverInterfacePrx debugObserver;
    };

    /**
     * @defgroup Component-HokuyoLaserUnit HokuyoLaserUnit
     * @ingroup RobotAPI-Components
     * A description of the component HokuyoLaserUnit.
     *
     * @class HokuyoLaserUnit
     * @ingroup Component-HokuyoLaserUnit
     * @brief Brief description of class HokuyoLaserUnit.
     *
     * Detailed description of class HokuyoLaserUnit.
     */
    class HokuyoLaserUnit :
        virtual public armarx::LaserScannerUnitInterface,
        virtual public armarx::SensorActorUnit
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "HokuyoLaserUnit";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        std::string getReportTopicName(const Ice::Current& c) const override;

        LaserScannerInfoSeq getConnectedDevices(const Ice::Current& c) const override;

    private:
        std::string topicName;
        LaserScannerUnitListenerPrx topic;
        int updatePeriod = 25;
        float angleOffset = 0.0f;
        std::vector<HokuyoLaserScanDevice> devices;
        LaserScannerInfoSeq connectedDevices;
        RobotHealthInterfacePrx robotHealthTopic;
        DebugObserverInterfacePrx debugObserver;
    };
}

