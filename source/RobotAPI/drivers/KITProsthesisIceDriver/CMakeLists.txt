#requires the Packages libqt5bluetooth5 qtconnectivity5-dev
set(LIB_NAME    KITProsthesisIceDriver)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

set(LIBS ArmarXCoreObservers RobotAPICore KITProstheticHandDriver)
set(LIB_FILES   KITProsthesisIceDriver.cpp)
set(LIB_HEADERS KITProsthesisIceDriver.h)

armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

add_subdirectory(example)
