#include <Ice/Ice.h>
#include <RobotAPI/drivers/KITProsthesisIceDriver/KITProsthesisIceDriver.h>
#include <RobotAPI/interface/units/KITProstheticHandInterface.h>
#include <QCoreApplication>
#include <thread>
int main(int argc, char* argv[])
{
    for (int i = 0; i < argc; ++i)
    {
        if (std::string(argv[i]) == "-h" ||
            std::string(argv[i]) == "--print-options") // needed for ArmarX Doc generation
        {
            // TODO: print help
            return 0;
        }
    }

    QCoreApplication a(argc, argv);
    try
    {
        Ice::CommunicatorHolder iceServer(argc, argv);
        Ice::ObjectAdapterPtr adapter = iceServer->createObjectAdapterWithEndpoints("KITProsthesisControlAdapter", "default -h localhost -p 10000");
        KITProsthesis::KITProstheticHandInterfacePtr object = new KITProsthesisIceDriver;
        Ice::ObjectPrx prx = adapter->add(object, Ice::stringToIdentity("KITProsthesisControl"));
        std::cout << prx->ice_toString() << std::endl;
        adapter->activate();
        iceServer->waitForShutdown();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}
