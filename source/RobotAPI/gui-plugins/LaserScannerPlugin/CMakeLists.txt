armarx_set_target("LaserScannerPluginGuiPlugin")

set(SOURCES LaserScannerPluginGuiPlugin.cpp LaserScannerPluginWidgetController.cpp)
set(HEADERS LaserScannerPluginGuiPlugin.h   LaserScannerPluginWidgetController.h)
set(GUI_MOC_HDRS LaserScannerPluginGuiPlugin.h   LaserScannerPluginWidgetController.h)
set(GUI_UIS      LaserScannerPluginWidget.ui)

set(COMPONENT_LIBS RobotAPICore SimpleConfigDialog)


if(ArmarXGui_FOUND)
	armarx_gui_library(LaserScannerPluginGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
