#pragma once

#include <functional>
#include <map>
#include <sstream>

#include <QTreeWidget>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx
{

    /**
     * A class to efficiently build and maintain sorted items of `QTreeWidget`
     * or `QTreeWidgetItem` based on a sorted container matching the intended structure.
     */
    template <class ContainerT>
    struct TreeWidgetBuilder
    {
        using ElementT = typename ContainerT::value_type;

        /// Return < 0 if `element < item`, 0 if `element == item`, and > 0 if `element > item`.
        using CompareFn = std::function<int(const ElementT& element, QTreeWidgetItem* item)>;
        using NameFn = std::function<std::string(const ElementT& element)>;
        using MakeItemFn = std::function<QTreeWidgetItem*(const ElementT& element)>;
        using UpdateItemFn = std::function<bool(const ElementT& element, QTreeWidgetItem* item)>;


        TreeWidgetBuilder() = default;
        TreeWidgetBuilder(const ContainerT&)
        {}

        TreeWidgetBuilder(CompareFn compareFn, MakeItemFn makeItemFn, UpdateItemFn updateItemFn = NoUpdate) :
            compareFn(compareFn), makeItemFn(makeItemFn), updateItemFn(updateItemFn)
        {}
        TreeWidgetBuilder(NameFn nameFn, MakeItemFn makeItemFn, UpdateItemFn updateItemFn = NoUpdate) :
            compareFn(MakeCompareNameFn(nameFn)), makeItemFn(makeItemFn), updateItemFn(updateItemFn)
        {}


        void setCompareFn(CompareFn compareFn)
        {
            this->compareFn = compareFn;
        }
        void setNameFn(NameFn nameFn)
        {
            compareFn = MakeCompareNameFn(nameFn);
        }
        void setMakeItemFn(MakeItemFn makeItemFn)
        {
            this->makeItemFn = makeItemFn;
        }
        void setUpdateItemFn(UpdateItemFn updateItemFn)
        {
            this->updateItemFn = updateItemFn;
        }


        template <class ParentT>
        void updateTree(ParentT* parent, const ContainerT& elements);


        /// No update function (default).
        static bool NoUpdate(const ElementT& element, QTreeWidgetItem* item)
        {
            (void) element, (void) item;
            return true;
        }

        /// Use the name for comparison.
        static CompareFn MakeCompareNameFn(NameFn nameFn)
        {
            return [nameFn](const ElementT & element, QTreeWidgetItem * item)
            {
                std::string name = nameFn(element);
                return name.compare(item->text(0).toStdString());
            };
        }


    private:

        CompareFn compareFn;
        MakeItemFn makeItemFn;
        UpdateItemFn updateItemFn;

    };


    /**
     * A class to efficiently build and maintain sorted items of `QTreeWidget`
     * or `QTreeWidgetItem` based on a map matching the intended structure.
     */
    template <class KeyT, class ValueT>
    struct MapTreeWidgetBuilder
    {
        using MapT = std::map<KeyT, ValueT>;
        using Base = TreeWidgetBuilder<MapT>;
        using ElementT = typename Base::ElementT;

        using NameFn = std::function<std::string(const KeyT& key, const ValueT& value)>;

        using MakeItemFn = std::function<QTreeWidgetItem*(const KeyT& key, const ValueT& value)>;
        using UpdateItemFn = std::function<bool(const KeyT& key, const ValueT& value, QTreeWidgetItem* item)>;


        MapTreeWidgetBuilder()
        {
            setNameFn(KeyAsName);
        }
        /// Allows declaring instance from container without explicit template arguments.
        MapTreeWidgetBuilder(const MapT&) : MapTreeWidgetBuilder()
        {}

        MapTreeWidgetBuilder(MakeItemFn makeItemFn, UpdateItemFn updateItemFn = NoUpdate)  : MapTreeWidgetBuilder()
        {
            setMakeItemFn(makeItemFn);
            setUpdateItemFn(updateItemFn);
        }
        MapTreeWidgetBuilder(NameFn nameFn, MakeItemFn makeItemFn, UpdateItemFn updateItemFn = NoUpdate)
        {
            setNameFn(nameFn);
            setMakeItemFn(makeItemFn);
            setUpdateItemFn(updateItemFn);
        }

        void setNameFn(NameFn nameFn)
        {
            builder.setNameFn([nameFn](const ElementT & element)
            {
                const auto& [key, value] = element;
                return nameFn(key, value);
            });
        }
        void setMakeItemFn(MakeItemFn makeItemFn)
        {
            builder.setMakeItemFn([makeItemFn](const ElementT & element)
            {
                const auto& [key, value] = element;
                return makeItemFn(key, value);
            });
        }
        void setUpdateItemFn(UpdateItemFn updateItemFn)
        {
            builder.setUpdateItemFn([updateItemFn](const ElementT & element, QTreeWidgetItem * item)
            {
                const auto& [key, value] = element;
                return updateItemFn(key, value, item);
            });
        }


        template <class ParentT>
        void updateTree(ParentT* tree, const MapT& elements)
        {
            builder.updateTree(tree, elements);
        }


        /// A name function using the key as name.
        static std::string KeyAsName(const KeyT& key, const ValueT& value)
        {
            (void) value;
            if constexpr(std::is_same<KeyT, std::string>())
            {
                return key;
            }
            else
            {
                std::stringstream ss;
                ss << key;
                return ss.str();
            }
        }

        /// No update function (default).
        static bool NoUpdate(const KeyT& key, const ValueT& value, QTreeWidgetItem* item)
        {
            (void) key, (void) value, (void) item;
            return true;
        }


    private:

        TreeWidgetBuilder<MapT> builder;

    };



    namespace detail
    {
        template <class ParentT> struct ParentAPI;

        template <> struct ParentAPI<QTreeWidget>
        {
            static int getItemCount(QTreeWidget* tree)
            {
                return tree->topLevelItemCount();
            }
            static QTreeWidgetItem* getItem(QTreeWidget* tree, int index)
            {
                return tree->topLevelItem(index);
            }
            static void insertItem(QTreeWidget* tree, int index, QTreeWidgetItem* item)
            {
                tree->insertTopLevelItem(index, item);
            }
            static QTreeWidgetItem* takeItem(QTreeWidget* tree, int index)
            {
                return tree->takeTopLevelItem(index);
            }
        };

        template <> struct ParentAPI<QTreeWidgetItem>
        {
            static int getItemCount(QTreeWidgetItem* parent)
            {
                return parent->childCount();
            }
            static QTreeWidgetItem* getItem(QTreeWidgetItem* parent, int index)
            {
                return parent->child(index);
            }
            static QTreeWidgetItem* takeItem(QTreeWidgetItem* parent, int index)
            {
                return parent->takeChild(index);
            }
            static void insertItem(QTreeWidgetItem* parent, int index, QTreeWidgetItem* item)
            {
                parent->insertChild(index, item);
            }
        };
    }


    template <class ContainerT>
    template <typename ParentT>
    void TreeWidgetBuilder<ContainerT>::updateTree(ParentT* parent, const ContainerT& elements)
    {
        using api = detail::ParentAPI<ParentT>;

        int currentIndex = 0;
        for (const auto& element : elements)
        {
            QTreeWidgetItem* item = nullptr;
            if (currentIndex >= api::getItemCount(parent))
            {
                // Add elements to the end of the list.
                item = makeItemFn(element);
                api::insertItem(parent, api::getItemCount(parent), item);
                ++currentIndex;
            }
            else
            {
                QTreeWidgetItem* currentItem = api::getItem(parent, currentIndex);
                while (currentItem != nullptr && compareFn(element, currentItem) > 0)
                {
                    delete api::takeItem(parent, currentIndex);
                    currentItem = api::getItem(parent, currentIndex);
                }
                if (currentItem == nullptr || compareFn(element, currentItem) < 0)
                {
                    // Insert new item before child.
                    item = makeItemFn(element);
                    api::insertItem(parent, currentIndex, item);
                    ++currentIndex;
                }
                else if (currentItem != nullptr && compareFn(element, currentItem) == 0)
                {
                    // Already existing.
                    item = currentItem;
                    ++currentIndex;
                }
            }
            ARMARX_CHECK_NOT_NULL(item);
            if (!updateItemFn(element, item))
            {
                break;
            }
        }
    }

}
