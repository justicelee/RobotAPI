/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "SensorDevicesWidget.h"

#include "StyleSheets.h"

#include <QCheckBox>

namespace armarx
{
    SensorDevicesWidget::SensorDevicesWidget(QWidget* parent) :
        RobotUnitWidgetTemplateBase("SensorDevicesWidget", parent)
    {
        ui->treeWidget->setColumnCount(4);
        QTreeWidgetItem* head = ui->treeWidget->headerItem();
        head->setText(idxName, "Name");
        head->setText(idxTags, "Tags");
        head->setText(idxValType, "Value Type");
        head->setText(idxValue, "Value");
        head->setToolTip(idxName, "The control device's name");
        head->setToolTip(idxTags, "The control device's tags");
        head->setToolTip(idxValType, "The sensor value's' type");
        head->setToolTip(idxValue, "The sensor value");
        ui->treeWidget->setColumnWidth(idxName, 150);
        ui->treeWidget->setColumnWidth(idxTags, 100);
        ui->treeWidget->setColumnWidth(idxValType, 450);
    }

    SensorDevicesWidget::~SensorDevicesWidget()
    {
        delete ui;
    }

    void SensorDevicesWidget::sensorDeviceStatusChanged(const SensorDeviceStatusSeq& allStatus)
    {
        std::unique_lock<std::recursive_timed_mutex> guard {mutex, std::defer_lock};
        if (!guard.try_lock_for(std::chrono::microseconds(100)))
        {
            return;
        }
        for (const auto& status : allStatus)
        {
            if (statusUpdates[status.deviceName].timestampUSec < status.timestampUSec)
            {
                statusUpdates[status.deviceName] = status;
                if (doMetaCall)
                {
                    QMetaObject::invokeMethod(this, "updateContent", Qt::QueuedConnection);
                }
            }
        }
    }

    void SensorDevicesWidget::clearAll()
    {
        entries.clear();
        statusUpdates.clear();
        resetData.clear();
    }

    void SensorDevicesWidget::doContentUpdate()
    {
        for (const auto& pair : statusUpdates)
        {
            if (!entries.count(pair.second.deviceName))
            {
                add(robotUnit->getSensorDeviceDescription(pair.second.deviceName));
            }
            entries.at(pair.second.deviceName)->update(pair.second);
        }
        statusUpdates.clear();
    }

    void SensorDevicesWidget::getResetData()
    {
        auto temp = robotUnit->getSensorDeviceDescriptions();
        {
            std::unique_lock<std::recursive_timed_mutex> guard {mutex};
            resetData = std::move(temp);
        }
    }

    bool SensorDevicesWidget::addOneFromResetData()
    {
        if (resetData.empty())
        {
            return true;
        }
        add(resetData.back());
        resetData.pop_back();
        return false;
    }

    void SensorDevicesWidget::add(const SensorDeviceDescription& desc)
    {
        std::unique_lock<std::recursive_timed_mutex> guard {mutex};
        if (!entries.count(desc.deviceName))
        {
            entries[desc.deviceName] = new SensorDevicesWidgetEntry(*this, *(ui->treeWidget), desc);
        }
    }

    SensorDevicesWidgetEntry::SensorDevicesWidgetEntry(SensorDevicesWidget& parent, QTreeWidget& treeWidget, const SensorDeviceDescription& desc):
        QObject {&parent}
    {
        header = new QTreeWidgetItem;
        treeWidget.addTopLevelItem(header);
        header->setText(SensorDevicesWidget::idxName, QString::fromStdString(desc.deviceName));
        //tags
        {
            QCheckBox* boxTags = new QCheckBox{QString::number(desc.tags.size()) + " Tags"};
            treeWidget.setItemWidget(header, SensorDevicesWidget::idxTags, boxTags);
            boxTags->setStyleSheet(checkboxStyleSheet());
            boxTags->setChecked(false);
            connect(boxTags, SIGNAL(clicked(bool)), this, SLOT(hideTagList(bool)));
            for (const auto& tag : desc.tags)
            {
                QTreeWidgetItem* child = new QTreeWidgetItem {{QString::fromStdString(tag)}};
                treeWidget.addTopLevelItem(child);
                tags.emplace_back(child);
            }
        }
        header->setText(SensorDevicesWidget::idxValType, QString::fromStdString(desc.sensorValueType));
        value = new VariantWidget;
        QTreeWidgetItem* valchild = new QTreeWidgetItem;
        header->addChild(valchild);
        treeWidget.setItemWidget(valchild, SensorDevicesWidget::idxValue, value);
    }

    void SensorDevicesWidgetEntry::update(const SensorDeviceStatus& status)
    {
        value->update(status.sensorValue);
        value->layout()->setContentsMargins(0, 0, 0, 0);
    }

    bool SensorDevicesWidgetEntry::matchName(const QString& name)
    {
        return header->text(SensorDevicesWidget::idxName).contains(name, Qt::CaseInsensitive);
    }

    bool SensorDevicesWidgetEntry::matchTag(const QString& tag)
    {
        for (const auto& tagit : tags)
        {
            if (tagit->text(SensorDevicesWidget::idxTags).contains(tag, Qt::CaseInsensitive))
            {
                return true;
            }
        }
        return false;
    }

    bool SensorDevicesWidgetEntry::matchValueType(const QString& type)
    {
        return header->text(SensorDevicesWidget::idxValType).contains(type, Qt::CaseInsensitive);
    }

    void SensorDevicesWidgetEntry::setVisible(bool vis)
    {
        if (!vis)
        {
            hideTagList(true);
        }
        header->setHidden(!vis);
    }

    void SensorDevicesWidgetEntry::hideTagList(bool hide)
    {
        for (QTreeWidgetItem* tag : tags)
        {
            tag->setHidden(hide);
        }
    }

}
