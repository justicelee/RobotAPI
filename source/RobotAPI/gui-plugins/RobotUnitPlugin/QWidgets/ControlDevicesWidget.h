/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <atomic>

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QTreeWidget>
#include <QCheckBox>
#include <QComboBox>
#include <QTreeWidgetItem>

#include <ArmarXGui/libraries/WidgetDescription/WidgetDescription.h>
#include <ArmarXGui/libraries/VariantWidget/VariantWidget.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include "RobotUnitWidgetBase.h"
#include <RobotAPI/gui-plugins/RobotUnitPlugin/ui_ControlDevicesWidget.h>

namespace armarx
{
    class ControlDevicesWidgetEntry;

    class ControlDevicesWidget : public RobotUnitWidgetTemplateBase<Ui::ControlDevicesWidget>
    {
    public:
        explicit ControlDevicesWidget(QWidget* parent = 0);
        ~ControlDevicesWidget() override;
        void controlDeviceStatusChanged(const ControlDeviceStatusSeq& allStatus);

    protected:
        void clearAll() override;
        void doContentUpdate() override;
        void getResetData() override;
        bool addOneFromResetData() override;
    private:
        void add(const ControlDeviceDescription& desc);
        void update(const ControlDeviceStatus& status);

        std::map<std::string, ControlDevicesWidgetEntry*> entries;
        std::map<std::string, ControlDeviceStatus> statusUpdates;
        ControlDeviceDescriptionSeq resetData;

    public:
        static constexpr int idxName = 0;
        static constexpr int idxTags = 1;
        static constexpr int idxMode = 2;
        static constexpr int idxHWMode = 3;
        static constexpr int idxAct = 4;
        static constexpr int idxReq = 5;
        static constexpr int idxType = 6;
        static constexpr int idxVal = 7;
    };

    class ControlDevicesWidgetEntry : QObject
    {

        Q_OBJECT
    public:
        ControlDevicesWidgetEntry(
            ControlDevicesWidget& parent,
            QTreeWidget& treeWidget,
            const ControlDeviceDescription& desc
        );


        void update(const ControlDeviceStatus& status);

        bool matchName(const QString& name);
        bool matchTag(const QString& tag);
        std::set<QTreeWidgetItem*> matchMode(const QString& mode);
        std::set<QTreeWidgetItem*> isActiveState(const QString& state);
        std::set<QTreeWidgetItem*> isRequestedState(const QString& state);
        std::set<QTreeWidgetItem*> matchTargetType(const QString& type);

    public slots:
        void setVisible(bool vis);
        void setChildVis(bool vis, std::set<QTreeWidgetItem*> children);

    private slots:
        void hideTagList(bool hide);

    private:
        struct ControllerEntry
        {
            QTreeWidgetItem* child;
            VariantWidget* value;
        };
        std::map<std::string, ControllerEntry> subEntries;
        std::string activeMode;
        std::string requestedMode;

        QTreeWidgetItem* header;

        std::vector<QTreeWidgetItem*> tags;
    };
}
