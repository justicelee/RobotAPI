/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <atomic>

#include <boost/thread.hpp>

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QTreeWidget>
#include <QCheckBox>
#include <QComboBox>
#include <QStackedWidget>
#include <QSettings>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

namespace armarx
{

    class RobotUnitWidgetBase : public QWidget
    {
        Q_OBJECT

    public:
        explicit RobotUnitWidgetBase(QString name, QWidget* parent);
        ~RobotUnitWidgetBase();
        void reset(RobotUnitInterfacePrx ru);

        virtual void loadSettings(QSettings*) {}
        virtual void saveSettings(QSettings*) {}

        void setVisible(bool visible) override;
    protected slots:
        void updateContent();
    private slots:
        void doReset();

    protected:
        virtual QTreeWidget& getTreeWidget() = 0;
        virtual QStackedWidget& getStackedWidget() = 0;
        virtual QLabel& getResettigLabel() = 0;
        virtual void addFilter() {}
        virtual void clearAll() = 0;
        virtual void doContentUpdate() = 0;

        virtual void getResetData() = 0;
        /**
         * @return true if all objects were added
         */
        virtual bool addOneFromResetData() = 0;

        void timerEvent(QTimerEvent*) override;

        RobotUnitInterfacePrx robotUnit;
        mutable std::recursive_timed_mutex mutex;
        std::atomic_bool gotResetData {false};
        int resetTimerId {0};
        int resetCount {0};
        std::atomic_bool isResetting {false};
        std::atomic_bool doMetaCall {false};
    private:
        boost::thread _resetThread;
    };

    template<class UI>
    class RobotUnitWidgetTemplateBase : public RobotUnitWidgetBase
    {
    public:
        RobotUnitWidgetTemplateBase(QString name, QWidget* parent):
            RobotUnitWidgetBase(name, parent),
            ui {new UI}
        {
            ui->setupUi(this);
        }

    protected:
        QTreeWidget& getTreeWidget() final override
        {
            return *(ui->treeWidget);
        }
        QStackedWidget& getStackedWidget() final override
        {
            return *(ui->stackedWidget);
        }
        QLabel& getResettigLabel() final override
        {
            return *(ui->labelResetting);
        }

        UI* ui;
    };
}
