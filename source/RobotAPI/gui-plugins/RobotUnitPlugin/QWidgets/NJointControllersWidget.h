/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <atomic>

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QCheckBox>

#include <ArmarXCore/core/util/algorithm.h>
#include <ArmarXGui/libraries/WidgetDescription/WidgetDescription.h>
#include <ArmarXGui/libraries/VariantWidget/VariantWidget.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/interface/units/RobotUnit/NJointController.h>

#include "RobotUnitWidgetBase.h"
#include <RobotAPI/gui-plugins/RobotUnitPlugin/ui_NJointControllersWidget.h>

namespace armarx
{
    class NJointControllersWidget;
    class NJointControllersWidgetEntry;
    class NJointControllersWidgetRemoteFunction;

    class NJointControllersWidget : public RobotUnitWidgetTemplateBase<Ui::NJointControllersWidget>
    {
        Q_OBJECT
    public:
        explicit NJointControllersWidget(QWidget* parent = 0);
        ~NJointControllersWidget() override;
        void nJointControllerStatusChanged(const NJointControllerStatusSeq& allStatus);

        virtual void nJointControllerCreated(std::string name);
        virtual void nJointControllerDeleted(std::string name);

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

    protected:
        void clearAll() override;
        void doContentUpdate() override;
        void getResetData() override;
        bool addOneFromResetData() override;

    private:
        void add(const NJointControllerDescriptionWithStatus& ds);

        std::map<std::string, NJointControllersWidgetEntry*> entries;
        std::map<std::string, NJointControllerStatus> statusUpdates;
        std::map<std::string, NJointControllerDescriptionWithStatus> controllersCreated;
        std::set<std::string> controllersDeleted;
    private slots:
        void onPushButtonStopAll_clicked();
        void onPushButtonRemoveAll_clicked();

    public:
        static constexpr int idxName       = 0;
        static constexpr int idxClass      = 1;
        static constexpr int idxActive     = 2;
        static constexpr int idxRequested  = 3;
        static constexpr int idxError      = 4;
        static constexpr int idxInternal   = 5;
        static constexpr int idxCtrlDev    = 6;
        static constexpr int idxCtrlMode   = 7;
        static constexpr int idxActivate   = 8;
        static constexpr int idxDeactivate = 9;
        static constexpr int idxDelete     = 10;
    };

    class NJointControllersWidgetEntry : public QObject
    {
        Q_OBJECT
    public:
        friend class NJointControllersWidget;

        NJointControllersWidgetEntry(
            NJointControllersWidget& parent,
            QTreeWidget& treeWidget,
            const NJointControllerDescription& desc
        );

        void update(const NJointControllerStatus& status);

        bool matchName(const QString& name);
        bool matchClass(const QString& name);
        bool isActiveState(const QString& state);
        bool isRequestedState(const QString& state);
        bool isErrorState(const QString& state);
        bool matchDevice(const QString& dev);
        bool matchMode(const QString& mode);
        void deleteContent();

    public slots:
        void setVisible(bool vis);
    private slots:
        void activateController();
        void deactivateController();
        void deleteController();

        void hideDeviceList();

        void setDeviceListVisible(bool vis);
    private:
        const bool deletable;
        QCheckBox* boxDev;
        QCheckBox* boxMod;
        QTreeWidgetItem* header;
        NJointControllerInterfacePrx controller;
        std::vector<QTreeWidgetItem*> devsToModes;

    };

    class NJointControllersWidgetRemoteFunction : public WidgetDescription::ValueChangedListenerInterface
    {
        Q_OBJECT
    public:
        NJointControllersWidgetRemoteFunction(
            QTreeWidget& treeWidget,
            QTreeWidgetItem& header,
            const std::string& functionName,
            const NJointControllerInterfacePrx& ctrl,
            const WidgetDescription::WidgetPtr& w);
    private slots:
        void execFunction();
    private:
        QTreeWidgetItem* functionHeader;

        std::string functionName;
        NJointControllerInterfacePrx ctrl;

        QCheckBox* execOnParamChange;
        WidgetDescription::DescribedWidgetBase* params;

        std::map<std::string, VariantBasePtr> paramValues;

    public slots:
        void valueChangedSlot(std::string name, VariantBasePtr value) override;
    };
}
