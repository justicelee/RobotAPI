/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPluginWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotUnitPluginWidgetController.h"
#include "../QWidgets/StyleSheets.h"

#include <string>
#include <regex>
#include <filesystem>

#include <QDir>
#include <QSortFilterProxyModel>
#include <QAction>

#include <ArmarXCore/util/CPPUtility/Iterator.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/FileSystemPathBuilder.h>

using namespace armarx;

RobotUnitPluginWidgetController::RobotUnitPluginWidgetController()
{
    widget.setupUi(getWidget());

    //add subwidget ControlDevices
    {
        controlDevices = new ControlDevicesWidget;
        widget.groupBoxCDev->layout()->addWidget(controlDevices);
        widget.groupBoxCDev->setVisible(false);
        controlDevices->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        controlDevices->setVisible(false);
    }
    //add subwidget SensorDevices
    {
        sensorDevices = new SensorDevicesWidget;
        widget.groupBoxSDev->layout()->addWidget(sensorDevices);
        widget.groupBoxSDev->setVisible(false);
        sensorDevices->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        sensorDevices->setVisible(false);
    }
    //add subwidget NJointControllers
    {
        nJointControllers = new NJointControllersWidget;
        widget.groupBoxNJointCtrl->layout()->addWidget(nJointControllers);
        widget.groupBoxNJointCtrl->setVisible(true);
        nJointControllers->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        nJointControllers->setVisible(true);
    }
    //add subwidget NJointControllerClasses
    {
        nJointControllerClasses = new NJointControllerClassesWidget;
        widget.groupBoxNJointCtrlClasses->layout()->addWidget(nJointControllerClasses);
        widget.groupBoxNJointCtrlClasses->setVisible(false);
        nJointControllerClasses->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        nJointControllerClasses->setVisible(false);
    }
    //update logging
    {
        widget.groupBoxLogging->setVisible(false);
        widget.groupBoxLogging->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);

        connect(widget.pushButtonLoggingStart, SIGNAL(clicked()), this, SLOT(on_pushButtonLoggingStart_clicked()));
        connect(widget.pushButtonLoggingStop, SIGNAL(clicked()), this, SLOT(on_pushButtonLoggingStop_clicked()));
        connect(widget.pushButtonLoggingMark1, SIGNAL(clicked()), this, SLOT(on_pushButtonLoggingMark1_clicked()));
        connect(widget.pushButtonLoggingMark2, SIGNAL(clicked()), this, SLOT(on_pushButtonLoggingMark2_clicked()));
        connect(widget.pushButtonLoggingMark3, SIGNAL(clicked()), this, SLOT(on_pushButtonLoggingMark3_clicked()));
        connect(widget.lineEditLoggingFilter, SIGNAL(textChanged(const QString&)),
                this, SLOT(on_lineEditLoggingFilter_textChanged(const QString&)));
        connect(widget.treeWidgetLoggingNames, SIGNAL(itemChanged(QTreeWidgetItem*, int)),
                this, SLOT(on_treeWidgetLoggingNames_itemChanged(QTreeWidgetItem*, int)));
    }
    updateToolBarActionCheckedState();
}


void RobotUnitPluginWidgetController::loadSettings(QSettings* settings)
{
    robotUnitProxyName = settings->value("robotUnitProxyName", QString::fromStdString(robotUnitProxyName)).toString().toStdString();
    nJointControllerClasses->loadSettings(settings);
    nJointControllers->loadSettings(settings);
    controlDevices->loadSettings(settings);
    sensorDevices->loadSettings(settings);
    widget.groupBoxCDev->setVisible(settings->value("cdevVisible", false).toBool());
    widget.groupBoxSDev->setVisible(settings->value("sdevVisible", false).toBool());
    widget.groupBoxNJointCtrl->setVisible(settings->value("ctrlVisible", true).toBool());
    widget.groupBoxNJointCtrlClasses->setVisible(settings->value("classVisible", false).toBool());
    updateToolBarActionCheckedState();
}

void RobotUnitPluginWidgetController::saveSettings(QSettings* settings)
{
    settings->setValue("robotUnitProxyName", QString::fromStdString(robotUnitProxyName));
    nJointControllerClasses->saveSettings(settings);
    nJointControllers->saveSettings(settings);
    controlDevices->saveSettings(settings);
    sensorDevices->saveSettings(settings);
    settings->setValue("cdevVisible", widget.groupBoxCDev->isVisible());
    settings->setValue("sdevVisible", widget.groupBoxSDev->isVisible());
    settings->setValue("ctrlVisible", widget.groupBoxNJointCtrl->isVisible());
    settings->setValue("classVisible", widget.groupBoxNJointCtrlClasses->isVisible());
}

void RobotUnitPluginWidgetController::onInitComponent()
{
    usingProxy(robotUnitProxyName);
    ARMARX_INFO << "RobotUnitPluginWidgetController::onInitComponent()" << std::flush;
    QMetaObject::invokeMethod(this, "startOnConnectTimer", Qt::QueuedConnection);
}
void RobotUnitPluginWidgetController::onExitComponent()
{
    QMetaObject::invokeMethod(this, "stopOnConnectTimer", Qt::QueuedConnection);
}

void RobotUnitPluginWidgetController::onConnectComponent()
{
    std::lock_guard guard{robotUnitPrxMutex};
    robotUnitPrx = getProxy<RobotUnitInterfacePrx>(robotUnitProxyName);
    listenerTopicName = robotUnitPrx->getRobotUnitListenerTopicName();
    usingTopic(listenerTopicName);
    updateToolBarActionCheckedState();
    timerLastIterationRuWasRunning = false;
}

void RobotUnitPluginWidgetController::onDisconnectComponent()
{
    std::lock_guard guard{robotUnitPrxMutex};
    unsubscribeFromTopic(listenerTopicName);
    robotUnitPrx = nullptr;
    QMetaObject::invokeMethod(this, "refreshNJointControllersClicked", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "refreshNJointControllerClassesClicked", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "refreshControlDevicesClicked", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "refreshSensorDevicesClicked", Qt::QueuedConnection);
}

QPointer<QDialog> RobotUnitPluginWidgetController::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new SimpleConfigDialog(parent);
        dialog->addProxyFinder<RobotUnitInterfacePrx>({"RobotUnit", "The RobotUnit", "*"});
    }
    return qobject_cast<SimpleConfigDialog*>(dialog);
}

void RobotUnitPluginWidgetController::configured()
{
    robotUnitProxyName = dialog->getProxyName("RobotUnit");
}

QPointer<QWidget> RobotUnitPluginWidgetController::getCustomTitlebarWidget(QWidget* parent)
{
    if (customToolbar)
    {
        customToolbar->setParent(parent);
    }
    else
    {
        customToolbar = new QToolBar(parent);
        customToolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        customToolbar->setIconSize(QSize(16, 16));
        customToolbar->addAction(QIcon(":/icons/view-refresh-7.png"), "", this, SLOT(refreshControlDevicesClicked()))
        ->setToolTip("Update the list of Control Devices");
        showCDevs = new QAction {"Control Devices", customToolbar};
        showCDevs->setCheckable(true);
        showCDevs->setToolTip("Hide/Show the list of Control Devices");
        connect(showCDevs, SIGNAL(toggled(bool)), widget.groupBoxCDev, SLOT(setVisible(bool)));
        connect(showCDevs, SIGNAL(toggled(bool)), controlDevices, SLOT(setVisible(bool)));
        customToolbar->addAction(showCDevs);
        customToolbar->addSeparator();

        customToolbar->addAction(QIcon(":/icons/view-refresh-7.png"), "", this, SLOT(refreshSensorDevicesClicked()))
        ->setToolTip("Update the list of Sensor Devices");
        showSDevs = new QAction {"Sensor Devices", customToolbar};
        showSDevs->setCheckable(true);
        showSDevs->setToolTip("Hide/Show the list of Sensor Devices");
        connect(showSDevs, SIGNAL(toggled(bool)), widget.groupBoxSDev, SLOT(setVisible(bool)));
        connect(showSDevs, SIGNAL(toggled(bool)), sensorDevices, SLOT(setVisible(bool)));
        customToolbar->addAction(showSDevs);
        customToolbar->addSeparator();

        customToolbar->addAction(QIcon(":/icons/view-refresh-7.png"), "", this, SLOT(refreshNJointControllersClicked()))
        ->setToolTip("Update the list of NJointControllers");
        showNJoint = new QAction {"NJointControllers", customToolbar};
        showNJoint->setCheckable(true);
        showNJoint->setToolTip("Hide/Show the list of NJointControllers");
        connect(showNJoint, SIGNAL(toggled(bool)), widget.groupBoxNJointCtrl, SLOT(setVisible(bool)));
        connect(showNJoint, SIGNAL(toggled(bool)), nJointControllers, SLOT(setVisible(bool)));
        customToolbar->addAction(showNJoint);
        customToolbar->addSeparator();

        customToolbar->addAction(QIcon(":/icons/view-refresh-7.png"), "", this, SLOT(refreshNJointControllerClassesClicked()))
        ->setToolTip("Update the list of NJointController Classes");
        showNJointClasses = new QAction {"NJointController Classes", customToolbar};
        showNJointClasses->setCheckable(true);
        showNJointClasses->setToolTip("Hide/Show the list of NJointControllers Classes");
        connect(showNJointClasses, SIGNAL(toggled(bool)), widget.groupBoxNJointCtrlClasses, SLOT(setVisible(bool)));
        connect(showNJointClasses, SIGNAL(toggled(bool)), nJointControllerClasses, SLOT(setVisible(bool)));
        customToolbar->addAction(showNJointClasses);
        customToolbar->addSeparator();

        customToolbar->addAction(QIcon(":/icons/view-refresh-7.png"), "", this, SLOT(refreshLogging()));
        showLogging = new QAction {"Logging", customToolbar};
        showLogging->setCheckable(true);
        showLogging->setToolTip("Hide/Show the logging pane");
        connect(showLogging, SIGNAL(toggled(bool)), widget.groupBoxLogging, SLOT(setVisible(bool)));
        customToolbar->addAction(showLogging);
        customToolbar->addSeparator();

        customToolbar->addAction(
            QIcon(":/icons/document-save.svg"), "Write log",
            [&]
        {
            std::lock_guard guard{robotUnitPrxMutex};
            robotUnitPrx->writeRecentIterationsToFile("/tmp/RobotUnitLog-{DateTime}");
        }
        )
        ->setToolTip("Writes the log to /tmp/");
    }
    updateToolBarActionCheckedState();
    return customToolbar.data();
}

void RobotUnitPluginWidgetController::nJointControllerStatusChanged(const NJointControllerStatusSeq& status, const Ice::Current&)
{
    nJointControllers->nJointControllerStatusChanged(status);
}

void RobotUnitPluginWidgetController::controlDeviceStatusChanged(const ControlDeviceStatusSeq& status, const Ice::Current&)
{
    controlDevices->controlDeviceStatusChanged(status);
}

void RobotUnitPluginWidgetController::sensorDeviceStatusChanged(const SensorDeviceStatusSeq& status, const Ice::Current&)
{
    sensorDevices->sensorDeviceStatusChanged(status);
}

void RobotUnitPluginWidgetController::nJointControllerClassAdded(const std::string& name, const Ice::Current&)
{
    nJointControllerClasses->nJointControllerClassAdded(name);
}

void RobotUnitPluginWidgetController::nJointControllerCreated(const std::string& name, const Ice::Current&)
{
    nJointControllers->nJointControllerCreated(name);
}

void RobotUnitPluginWidgetController::nJointControllerDeleted(const std::string& name, const Ice::Current&)
{
    nJointControllers->nJointControllerDeleted(name);
}

void RobotUnitPluginWidgetController::refreshNJointControllersClicked()
{
    std::lock_guard guard{robotUnitPrxMutex};
    nJointControllers->reset(robotUnitPrx);
}

void RobotUnitPluginWidgetController::refreshNJointControllerClassesClicked()
{
    std::lock_guard guard{robotUnitPrxMutex};
    nJointControllerClasses->reset(robotUnitPrx);
}

void RobotUnitPluginWidgetController::refreshControlDevicesClicked()
{
    std::lock_guard guard{robotUnitPrxMutex};
    controlDevices->reset(robotUnitPrx);
}

void RobotUnitPluginWidgetController::refreshSensorDevicesClicked()
{
    std::lock_guard guard{robotUnitPrxMutex};
    sensorDevices->reset(robotUnitPrx);
}

void RobotUnitPluginWidgetController::startOnConnectTimer()
{
    if (!timerId)
    {
        timerId = startTimer(100);
    }
}
void RobotUnitPluginWidgetController::stopOnConnectTimer()
{
    if (timerId)
    {
        killTimer(timerId);
        timerId = 0;
    }
}

void RobotUnitPluginWidgetController::updateToolBarActionCheckedState()
{
    if (customToolbar)
    {
        showCDevs->setChecked(widget.groupBoxCDev->isVisible());
        showSDevs->setChecked(widget.groupBoxSDev->isVisible());
        showNJoint->setChecked(widget.groupBoxNJointCtrl->isVisible());
        showNJointClasses->setChecked(widget.groupBoxNJointCtrlClasses->isVisible());
    }
}

void armarx::RobotUnitPluginWidgetController::timerEvent(QTimerEvent*)
{
    std::lock_guard guard{robotUnitPrxMutex};
    if (robotUnitPrx && robotUnitPrx->isRunning())
    {
        ARMARX_DEBUG << "timerEvent: RobotUnit is running";
        if (!timerLastIterationRuWasRunning)
        {
            refreshNJointControllersClicked();
            refreshNJointControllerClassesClicked();
            refreshControlDevicesClicked();
            refreshSensorDevicesClicked();
            refreshLogging();
        }
        else
        {
            loggingData.localLogging();
        }
        timerLastIterationRuWasRunning = true;
    }
    else
    {
        timerLastIterationRuWasRunning = false;
        ARMARX_DEBUG << "timerEvent: RobotUnit is not running";
    }
}

void armarx::RobotUnitPluginWidgetController::refreshLogging()
{
    std::lock_guard guard{robotUnitPrxMutex};
    on_pushButtonLoggingStop_clicked();

    widget.treeWidgetLoggingNames->clear();
    loggingData.allItems.clear();

    const auto allLoggingNames = robotUnitPrx->getLoggingNames();

    std::map<std::string, QTreeWidgetItem*> separators;
    auto add = [&](const std::string & name, const std::string & display, auto parent)
    {
        QTreeWidgetItem* i = new QTreeWidgetItem(parent);
        i->setText(0, QString::fromStdString(display) + "*");
        i->setData(0, Qt::ToolTipRole, QString::fromStdString(name));
        separators[name] = i;
        loggingData.allItems.emplace_back(i);
        i->setCheckState(0, Qt::Unchecked);
    };
    add("", "", widget.treeWidgetLoggingNames);
    loggingData.top = loggingData.allItems.front();

    for (const auto& name : allLoggingNames)
    {
        const auto parts = Split(name, ".", true, true);
        std::string reassembled;
        for (const auto& p : parts)
        {
            ARMARX_CHECK_EXPRESSION(separators.count(reassembled));
            auto parent = separators.at(reassembled);
            reassembled += (reassembled.empty() ? p : "." + p) ;
            if (!separators.count(reassembled))
            {
                add(reassembled, p, parent);
            }
        }
        loggingData.allItems.back()->setText(0, QString::fromStdString(parts.back()));
    }

}

void armarx::RobotUnitPluginWidgetController::on_pushButtonLoggingStart_clicked()
{
    std::lock_guard guard{robotUnitPrxMutex};
    if (!robotUnitPrx || loggingData.handle || loggingData.streamingHandler)
    {
        return;
    }
    std::vector<std::string> loggingNames;
    std::function<void(QTreeWidgetItem*)> recurseChildren = [&](auto it)
    {
        switch (it->checkState(0))
        {
            case Qt::Checked:
                if (it->childCount() == 0)
                {
                    loggingNames.emplace_back(it->data(0, Qt::ToolTipRole).toString().toStdString());
                }
                else
                {
                    for (int  i = 0; i < it->childCount(); ++i)
                    {
                        recurseChildren(it->child(i));
                    }
                }
                break;
            case Qt::Unchecked:
                break;
            case Qt::PartiallyChecked:
                for (int  i = 0; i < it->childCount(); ++i)
                {
                    recurseChildren(it->child(i));
                }
                break;
        }
    };
    recurseChildren(loggingData.top);
    const std::string saveFormatString = widget.lineEditLoggingPath->text().toStdString();
    if (widget.checkBoxLoggingStream->isChecked())
    {
        ARMARX_INFO << "start streaming " << loggingNames;
        RobotUnitDataStreaming::Config cfg;
        cfg.loggingNames = loggingNames;
        loggingData.streamingHandler =
            make_shared<RobotUnitDataStreamingReceiver>(this, robotUnitPrx, cfg);

        FileSystemPathBuilder pb {saveFormatString};
        loggingData.logStream.open(pb.getPath());
        loggingData.logStream << ";iteration;timestamp;TimeSinceLastIteration";
        const auto& entries = loggingData.streamingHandler->getDataDescription().entries;
        std::stringstream str;
        str << "stream " << entries.size() << " values\n";
        for (const auto& [name, desc] : entries)
        {
            loggingData.logStream << ';' << name;
            str << "    " << name
                << " -> type " << desc.type
                << ", index " << desc.index << '\n';
        }
        loggingData.logStream << std::endl;
        ARMARX_INFO << str.str();
    }
    else
    {
        ARMARX_INFO << "start logging " << loggingNames;
        loggingData.handle = robotUnitPrx->startRtLogging(saveFormatString, loggingNames);
        widget.pushButtonLoggingMark1->setEnabled(true);
        widget.pushButtonLoggingMark2->setEnabled(true);
        widget.pushButtonLoggingMark3->setEnabled(true);
    }
    widget.pushButtonLoggingStart->setEnabled(false);
    widget.pushButtonLoggingStop->setEnabled(true);
}

void armarx::RobotUnitPluginWidgetController::on_pushButtonLoggingStop_clicked()
{
    std::lock_guard guard{robotUnitPrxMutex};
    widget.pushButtonLoggingStart->setEnabled(true);
    widget.pushButtonLoggingStop->setEnabled(false);
    widget.pushButtonLoggingMark1->setEnabled(false);
    widget.pushButtonLoggingMark2->setEnabled(false);
    widget.pushButtonLoggingMark3->setEnabled(false);
    if (loggingData.handle)
    {
        robotUnitPrx->stopRtLogging(loggingData.handle);
    }
    if (loggingData.logStream.is_open())
    {
        loggingData.logStream.close();
    }
    loggingData.handle           = nullptr;
    loggingData.streamingHandler = nullptr;
}

void armarx::RobotUnitPluginWidgetController::on_lineEditLoggingFilter_textChanged(const QString& arg1)
{
    std::regex reg;
    try
    {
        reg = std::regex{arg1.toStdString()};
    }
    catch (...)
    {
        //broken regex
        widget.lineEditLoggingFilter->setStyleSheet("QLineEdit { background: rgb(255, 150, 150); selection-background-color: rgb(255, 0, 0); }");
        return;
    }
    widget.lineEditLoggingFilter->setStyleSheet("QLineEdit {}");

    std::function<void(QTreeWidgetItem* item)> setVisible = [&](auto it)
    {
        it->setHidden(false);
        auto parent = it->parent();
        //set parent state depending on its childrens state
        if (!parent)
        {
            return;
        }
        setVisible(parent);
    };
    for (auto item : loggingData.allItems)
    {
        item->setHidden(true);
    }
    for (auto item : loggingData.allItems)
    {
        const auto str = item->data(0, Qt::ToolTipRole).toString().toStdString();
        std::smatch match;
        if (std::regex_search(str, match, reg))
        {
            setVisible(item);
        }
    }
    widget.treeWidgetLoggingNames->blockSignals(true);
    for (auto item : MakeReversedRange(loggingData.allItems))
    {
        loggingData.updateCheckStateUpward(item, false);
    }
    widget.treeWidgetLoggingNames->blockSignals(false);
}

void armarx::RobotUnitPluginWidgetController::on_treeWidgetLoggingNames_itemChanged(QTreeWidgetItem* item, int)
{
    widget.treeWidgetLoggingNames->blockSignals(true);
    loggingData.updateCheckStateDownward(item, item->checkState(0), true);
    loggingData.updateCheckStateUpward(item->parent(), true);
    widget.treeWidgetLoggingNames->blockSignals(false);
}

void armarx::RobotUnitPluginWidgetController::on_pushButtonLoggingMark1_clicked()
{
    if (!loggingData.handle)
    {
        return;
    }
    const auto mark = widget.lineEditLoggingMark1->text().toStdString();
    ARMARX_INFO << "set mark " << mark;
    robotUnitPrx->addMarkerToRtLog(loggingData.handle, mark);
}

void armarx::RobotUnitPluginWidgetController::on_pushButtonLoggingMark2_clicked()
{
    if (!loggingData.handle)
    {
        return;
    }
    const auto mark = widget.lineEditLoggingMark2->text().toStdString();
    ARMARX_INFO << "set mark " << mark;
    robotUnitPrx->addMarkerToRtLog(loggingData.handle, mark);
}

void armarx::RobotUnitPluginWidgetController::on_pushButtonLoggingMark3_clicked()
{
    if (!loggingData.handle)
    {
        return;
    }
    const auto mark = widget.lineEditLoggingMark3->text().toStdString();
    ARMARX_INFO << "set mark " << mark;
    robotUnitPrx->addMarkerToRtLog(loggingData.handle, mark);
}

void RobotUnitPluginWidgetController::LoggingData::updateCheckStateUpward(QTreeWidgetItem* item, bool recurseParents)
{
    //set item state depending on its childrens state and maybe recurse parent
    for (; item; item = recurseParents ? item->parent() : nullptr)
    {
        if (item->isHidden())
        {
            continue;
        }
        bool anyChecked = false;
        bool anyUnchecked = false;
        bool anyTri = false;
        for (int  i = 0; i < item->childCount(); ++i)
        {
            auto c = item->child(i);
            if (c->isHidden())
            {
                continue;
            }
            switch (c->checkState(0))
            {
                case Qt::Checked:
                    anyChecked = true;
                    break;
                case Qt::Unchecked:
                    anyUnchecked = true;
                    break;
                case Qt::PartiallyChecked:
                    anyTri = true;
                    break;
            }
            if (anyChecked + anyUnchecked + anyTri > 1 || anyTri)
            {
                item->setCheckState(0, Qt::PartiallyChecked);
            }
            else if (anyChecked)
            {
                item->setCheckState(0, Qt::Checked);
            }
            else if (anyUnchecked)
            {
                item->setCheckState(0, Qt::Unchecked);
            }
            //case all false -> leaf -> do nothing
        }
    }
}

void RobotUnitPluginWidgetController::LoggingData::updateCheckStateDownward(
    QTreeWidgetItem* item,
    Qt::CheckState state,
    bool recurseChildren)
{
    if (item->isHidden())
    {
        return;
    }
    item->setCheckState(0, state);

    if (!recurseChildren)
    {
        return;
    }
    for (int  i = 0; i < item->childCount(); ++i)
    {
        auto c = item->child(i);
        if (c->isHidden())
        {
            continue;
        }
        c->setCheckState(0, state);
        updateCheckStateDownward(c, state, true);
    }
}

void RobotUnitPluginWidgetController::LoggingData::localLogging()
{
    if (!streamingHandler)
    {
        ARMARX_DEBUG_S << "localLogging -> no local logging";
        return;
    }
    ARMARX_DEBUG_S << "localLogging -> do local logging";
    auto& data = streamingHandler->getDataBuffer();
    if (data.empty())
    {
        ARMARX_INFO_S << ::deactivateSpam()
                      << "No streaming data received!";
        return;
    }
    ARMARX_INFO_S << ::deactivateSpam(1)
                  << "Writing " << data.size() << " timesteps";
    const auto& descr = streamingHandler->getDataDescription();
    for (const auto& step : data)
    {
        for (const auto& [name, desc] : descr.entries)
        {
            logStream << ';' << step.iterationId
                      << ';' << step.timestampUSec
                      << ';' << step.timesSinceLastIterationUSec;
            using enum_t = RobotUnitDataStreaming::DataEntryType;
            switch (desc.type)
            {
                case enum_t::NodeTypeBool  :
                    logStream << ';' << step.bools  .at(desc.index);
                    break;
                case enum_t::NodeTypeByte  :
                    logStream << ';' << step.bytes  .at(desc.index);
                    break;
                case enum_t::NodeTypeShort :
                    logStream << ';' << step.shorts .at(desc.index);
                    break;
                case enum_t::NodeTypeInt   :
                    logStream << ';' << step.ints   .at(desc.index);
                    break;
                case enum_t::NodeTypeLong  :
                    logStream << ';' << step.longs  .at(desc.index);
                    break;
                case enum_t::NodeTypeFloat :
                    logStream << ';' << step.floats .at(desc.index);
                    break;
                case enum_t::NodeTypeDouble:
                    logStream << ';' << step.doubles.at(desc.index);
                    break;
            };
        }
        logStream << std::endl;
    }
    data.clear();
}
