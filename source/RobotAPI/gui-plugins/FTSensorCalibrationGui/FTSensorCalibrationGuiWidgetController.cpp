/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::FTSensorCalibrationGuiWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <string>

#include <QRegExp>

#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/core/util/FileSystemPathBuilder.h>

#include "FTSensorCalibrationGuiWidgetController.h"

//boilerplate config
namespace armarx
{
    void FTSensorCalibrationGuiWidgetController::loadSettings(QSettings* settings)
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        getRobotStateComponentPlugin().setRobotStateComponentName(settings->value("rsc", "Armar6StateComponent").toString().toStdString());
        getRobotUnitComponentPlugin().setRobotUnitName(settings->value("ru", "Armar6Unit").toString().toStdString());
        getDebugObserverComponentPlugin().setDebugObserverTopic(settings->value("dbgo", "DebugObserver").toString().toStdString());
    }

    void FTSensorCalibrationGuiWidgetController::saveSettings(QSettings* settings)
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        settings->setValue("rsc", QString::fromStdString(getRobotStateComponentPlugin().getRobotStateComponentName()));
        settings->setValue("ru", QString::fromStdString(getRobotUnitComponentPlugin().getRobotUnitName()));
        settings->setValue("dbgo", QString::fromStdString(getDebugObserverComponentPlugin().getDebugObserverTopic()));
    }

    QPointer<QDialog> FTSensorCalibrationGuiWidgetController::getConfigDialog(QWidget* parent)
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        if (!_dialog)
        {
            _dialog = new SimpleConfigDialog(parent);
            _dialog->addProxyFinder<RobotUnitInterfacePrx>("ru", "Robot Unit", "*Unit");
            _dialog->addProxyFinder<RobotStateComponentInterfacePrx>("rsc", "Robot State Component", "*Component");
            _dialog->addLineEdit("dbgo", "DebugObserver Topic", "DebugObserver");
        }
        return qobject_cast<SimpleConfigDialog*>(_dialog);
    }

    void FTSensorCalibrationGuiWidgetController::configured()
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        getRobotStateComponentPlugin().setRobotStateComponentName(_dialog->get("rsc"));
        getRobotUnitComponentPlugin().setRobotUnitName(_dialog->get("ru"));
        getDebugObserverComponentPlugin().setDebugObserverTopic(_dialog->get("dbgo"));
    }

}
namespace armarx
{
    FTSensorCalibrationGuiWidgetController::FTSensorCalibrationGuiWidgetController()
    {
        _widget.setupUi(getWidget());

        _widget.pushButtonLoadCalibFromFile->setVisible(false);
        connect(_widget.pushButtonLoadCalibFromFile, &QPushButton::clicked,
                this, &FTSensorCalibrationGuiWidgetController::loadCalibFromFile);
        connect(_widget.pushButtonStart, &QPushButton::clicked,
                this, &FTSensorCalibrationGuiWidgetController::startRecording);
        connect(_widget.pushButtonStop, &QPushButton::clicked,
                this, &FTSensorCalibrationGuiWidgetController::stopRecording);
        connect(_widget.pushButtonLoadDefaultArmar6FTL, &QPushButton::clicked,
                this, &FTSensorCalibrationGuiWidgetController::loadDefaultArmar6FTL);
        connect(_widget.pushButtonLoadDefaultArmar6FTR, &QPushButton::clicked,
                this, &FTSensorCalibrationGuiWidgetController::loadDefaultArmar6FTR);

        connect(_widget.tableWidgetOffset, &QTableWidget::itemChanged,
                this, &FTSensorCalibrationGuiWidgetController::updateCalibration);
        connect(_widget.tableWidgetOffset, &QTableWidget::itemChanged,
                this, &FTSensorCalibrationGuiWidgetController::updateCalibration);
        connect(_widget.tableWidgetChannelOrder, &QTableWidget::itemChanged,
                this, &FTSensorCalibrationGuiWidgetController::updateCalibration);
        connect(_widget.doubleSpinBoxTickToVolt, qOverload<double>(&QDoubleSpinBox::valueChanged),
                this, &FTSensorCalibrationGuiWidgetController::updateCalibration);
        connect(_widget.doubleSpinBoxTickToVoltE, qOverload<int>(&QSpinBox::valueChanged),
                this, &FTSensorCalibrationGuiWidgetController::updateCalibration);

        connect(_widget.tableWidgetCompensate, &QTableWidget::itemChanged,
                this, &FTSensorCalibrationGuiWidgetController::updateCompensation);

        loadDefaultArmar6FTR();
        startTimer(10);
    }


    FTSensorCalibrationGuiWidgetController::~FTSensorCalibrationGuiWidgetController()
    {}

    void FTSensorCalibrationGuiWidgetController::onConnectComponent()
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        _robot = addOrGetRobot("r", VirtualRobot::RobotIO::RobotDescription::eStructure);
    }

    void FTSensorCalibrationGuiWidgetController::onDisconnectComponent()
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        stopRecording();
        _comboboxes_are_set_up = false;
        _robot = nullptr;
    }

    void FTSensorCalibrationGuiWidgetController::timerEvent(QTimerEvent* event)
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        if (!_robot)
        {
            _widget.widgetFields->setEnabled(false);
            return;
        }
        const auto to_list = [](const auto & container)
        {
            QStringList qnames;
            for (const auto& name : container)
            {
                qnames << QString::fromStdString(name);
            }
            return qnames;
        };
        if (!_comboboxes_are_set_up)
        {
            {
                const auto vec = getRobotUnit()->getLoggingNames();
                _all_logging_names = std::set<std::string>(vec.begin(), vec.end());
            }
            const auto qnames = to_list(_all_logging_names);
            const auto setup = [&](auto box, const auto & lst, auto regex)
            {
                box->clear();
                box->addItems(lst);
                const auto idx = lst.indexOf(QRegExp{regex});
                if (idx >= 0)
                {
                    box->setCurrentIndex(idx);
                }
            };
            setup(_widget.comboBoxADC1Value, qnames, R"(^sens\.FT R\.adc_raw_1_to_3\.x$)");
            setup(_widget.comboBoxADC2Value, qnames, R"(^sens\.FT R\.adc_raw_1_to_3\.y$)");
            setup(_widget.comboBoxADC3Value, qnames, R"(^sens\.FT R\.adc_raw_1_to_3\.z$)");
            setup(_widget.comboBoxADC4Value, qnames, R"(^sens\.FT R\.adc_raw_4_to_6\.x$)");
            setup(_widget.comboBoxADC5Value, qnames, R"(^sens\.FT R\.adc_raw_4_to_6\.y$)");
            setup(_widget.comboBoxADC6Value, qnames, R"(^sens\.FT R\.adc_raw_4_to_6\.z$)");

            setup(_widget.comboBoxADC1Temp, qnames, R"(^sens\.FT R\.adc_1_to_3_temperature$)");
            setup(_widget.comboBoxADC2Temp, qnames, R"(^sens\.FT R\.adc_1_to_3_temperature$)");
            setup(_widget.comboBoxADC3Temp, qnames, R"(^sens\.FT R\.adc_1_to_3_temperature$)");
            setup(_widget.comboBoxADC4Temp, qnames, R"(^sens\.FT R\.adc_4_to_6_temperature$)");
            setup(_widget.comboBoxADC5Temp, qnames, R"(^sens\.FT R\.adc_4_to_6_temperature$)");
            setup(_widget.comboBoxADC6Temp, qnames, R"(^sens\.FT R\.adc_4_to_6_temperature$)");

            setup(_widget.comboBoxKinematicChain,
                  to_list(_robot->getRobotNodeSetNames()),
                  "^Robot$");

            _widget.pushButtonStart->setEnabled(true);
            _widget.widgetFields->setEnabled(true);
            _comboboxes_are_set_up = true;
            return;
        }

        _widget.comboBoxADC1Value->setEnabled(!_recording);
        _widget.comboBoxADC2Value->setEnabled(!_recording);
        _widget.comboBoxADC3Value->setEnabled(!_recording);
        _widget.comboBoxADC4Value->setEnabled(!_recording);
        _widget.comboBoxADC5Value->setEnabled(!_recording);
        _widget.comboBoxADC6Value->setEnabled(!_recording);

        _widget.comboBoxADC1Temp ->setEnabled(!_recording);
        _widget.comboBoxADC2Temp ->setEnabled(!_recording);
        _widget.comboBoxADC3Temp ->setEnabled(!_recording);
        _widget.comboBoxADC4Temp ->setEnabled(!_recording);
        _widget.comboBoxADC5Temp ->setEnabled(!_recording);
        _widget.comboBoxADC6Temp ->setEnabled(!_recording);

        if (!_recording)
        {
            return;
        }
        setDebugObserverBatchModeEnabled(true);
        //do transform, plotting and logging
        {
            auto& data = _streaming_handler->getDataBuffer();
            setDebugObserverDatafield("number_of_timesteps", data.size());
            long time_compensation_us = 0;
            if (!data.empty())
            {
                const auto log = [&](auto n)
                {
                    _logfile << ';' << n;
                };
                for (const auto& s : data)
                {
                    _logfile << s.iterationId << ';' << s.timestampUSec;
                    //logging
                    RobotUnitDataStreamingReceiver::visitEntries(log, s, _adc);
                    RobotUnitDataStreamingReceiver::visitEntries(log, s, _adc_temp);
                    RobotUnitDataStreamingReceiver::visitEntries(log, s, _joints);
                    _logfile << '\n';
                }
                std::vector<float> vals;
                {
                    const auto& last = data.back();
                    const auto pback = [&](auto n)
                    {
                        vals.emplace_back(n);
                    };
                    RobotUnitDataStreamingReceiver::visitEntries(pback, last, _adc);
                    RobotUnitDataStreamingReceiver::visitEntries(pback, last, _adc_temp);
                }
                //calc + compensate

                Eigen::Vector6f raw_ft_unordered;
                raw_ft_unordered[0] = vals.at(0);
                raw_ft_unordered[1] = vals.at(1);
                raw_ft_unordered[2] = vals.at(2);
                raw_ft_unordered[3] = vals.at(3);
                raw_ft_unordered[4] = vals.at(4);
                raw_ft_unordered[5] = vals.at(5);

                Eigen::Vector6f temp_unordered;
                temp_unordered[0] = vals.at(6 + 0);
                temp_unordered[1] = vals.at(6 + 1);
                temp_unordered[2] = vals.at(6 + 2);
                temp_unordered[3] = vals.at(6 + 3);
                temp_unordered[4] = vals.at(6 + 4);
                temp_unordered[5] = vals.at(6 + 5);

                _widget.labelADC1Value->setText(QString::number(raw_ft_unordered(0)));
                _widget.labelADC2Value->setText(QString::number(raw_ft_unordered(1)));
                _widget.labelADC3Value->setText(QString::number(raw_ft_unordered(2)));
                _widget.labelADC4Value->setText(QString::number(raw_ft_unordered(3)));
                _widget.labelADC5Value->setText(QString::number(raw_ft_unordered(4)));
                _widget.labelADC6Value->setText(QString::number(raw_ft_unordered(5)));

                _widget.labelADC1Temp ->setText(QString::number(temp_unordered(0)));
                _widget.labelADC2Temp ->setText(QString::number(temp_unordered(1)));
                _widget.labelADC3Temp ->setText(QString::number(temp_unordered(2)));
                _widget.labelADC4Temp ->setText(QString::number(temp_unordered(3)));
                _widget.labelADC5Temp ->setText(QString::number(temp_unordered(4)));
                _widget.labelADC6Temp ->setText(QString::number(temp_unordered(5)));

                const auto do_compensation = [&]
                {
                    const auto st = std::chrono::high_resolution_clock::now();
                    for (int i = 0; i < 6; ++i)
                    {
                        raw_ft_unordered(i) =
                        _comp_table.compensate(temp_unordered(i), raw_ft_unordered(i));
                    }
                    const auto dt = std::chrono::high_resolution_clock::now() - st;
                    time_compensation_us = std::chrono::duration_cast<std::chrono::microseconds>(dt).count();
                };

                if (_widget.radioButtonCompBeforeOffset->isChecked())
                {
                    do_compensation();
                }
                raw_ft_unordered -= _offset;
                if (_widget.radioButtonCompAfterOffset->isChecked())
                {
                    do_compensation();
                }
                setDebugObserverDatafield("time_compensation_us", time_compensation_us);

                Eigen::Vector6f raw_ft;
                raw_ft[0] = raw_ft_unordered(_channel_order(0));
                raw_ft[1] = raw_ft_unordered(_channel_order(1));
                raw_ft[2] = raw_ft_unordered(_channel_order(2));
                raw_ft[3] = raw_ft_unordered(_channel_order(3));
                raw_ft[4] = raw_ft_unordered(_channel_order(4));
                raw_ft[5] = raw_ft_unordered(_channel_order(5));

                const Eigen::Vector6f ft = _conversion_matrix * (raw_ft * _ticks_to_volt_factor);

                //write to debug observer
                setDebugObserverDatafield("fx", ft(0));
                setDebugObserverDatafield("fy", ft(1));
                setDebugObserverDatafield("fz", ft(2));
                setDebugObserverDatafield("tx", ft(3));
                setDebugObserverDatafield("ty", ft(4));
                setDebugObserverDatafield("tz", ft(5));
            }

            data.clear();
        }
        sendDebugObserverBatch();
    }

    void FTSensorCalibrationGuiWidgetController::startRecording()
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        _widget.pushButtonStart->setEnabled(false);
        _widget.pushButtonStop->setEnabled(true);

        const std::array field_adc_value =
        {
            _widget.comboBoxADC1Value->currentText().toStdString(),
            _widget.comboBoxADC2Value->currentText().toStdString(),
            _widget.comboBoxADC3Value->currentText().toStdString(),
            _widget.comboBoxADC4Value->currentText().toStdString(),
            _widget.comboBoxADC5Value->currentText().toStdString(),
            _widget.comboBoxADC6Value->currentText().toStdString()
        };

        const std::array field_adc_temp  =
        {
            _widget.comboBoxADC1Temp ->currentText().toStdString(),
            _widget.comboBoxADC2Temp ->currentText().toStdString(),
            _widget.comboBoxADC3Temp ->currentText().toStdString(),
            _widget.comboBoxADC4Temp ->currentText().toStdString(),
            _widget.comboBoxADC5Temp ->currentText().toStdString(),
            _widget.comboBoxADC6Temp ->currentText().toStdString()
        };


        //setup streaming
        {
            RobotUnitDataStreaming::Config cfg;

            std::set<std::string> to_log;

            to_log.emplace(field_adc_value.at(0));
            to_log.emplace(field_adc_value.at(1));
            to_log.emplace(field_adc_value.at(2));
            to_log.emplace(field_adc_value.at(3));
            to_log.emplace(field_adc_value.at(4));
            to_log.emplace(field_adc_value.at(5));

            to_log.emplace(field_adc_temp.at(0));
            to_log.emplace(field_adc_temp.at(1));
            to_log.emplace(field_adc_temp.at(2));
            to_log.emplace(field_adc_temp.at(3));
            to_log.emplace(field_adc_temp.at(4));
            to_log.emplace(field_adc_temp.at(5));


            ///TODO
            //filter by rns
            const auto rns_name =  _widget.comboBoxKinematicChain ->currentText().toStdString();
            ARMARX_CHECK_EXPRESSION(_robot->hasRobotNodeSet(rns_name)) << VAROUT(rns_name);
            for (const auto& name : _robot->getRobotNodeSet(rns_name)->getNodeNames())
            {
                auto dataf = "sens." + name + ".position";
                if (_all_logging_names.count(dataf))
                {
                    to_log.emplace(std::move(dataf));
                }
            }
            cfg.loggingNames.insert(cfg.loggingNames.end(), to_log.begin(), to_log.end());
            _streaming_handler =
                make_shared<RobotUnitDataStreamingReceiver>(this, getRobotUnit(), cfg);
        }
        //setup csv log + indices
        {
            auto entries = _streaming_handler->getDataDescription().entries;
            for (std::size_t i = 0; i < 6; ++i)
            {
                ARMARX_CHECK_EXPRESSION(entries.count(field_adc_value.at(i))) << VAROUT(field_adc_value.at(i));
                ARMARX_CHECK_EXPRESSION(entries.count(field_adc_temp.at(i)))  << VAROUT(field_adc_temp.at(i));
                _adc.at(i)      = entries.at(field_adc_value.at(i));
                _adc_temp.at(i) = entries.at(field_adc_temp.at(i));
            }
            for (std::size_t i = 0; i < 6; ++i)
            {
                entries.erase(field_adc_value.at(i));
                entries.erase(field_adc_temp.at(i));
            }

            const std::filesystem::path path = FileSystemPathBuilder::ApplyFormattingAndResolveEnvAndCMakeVars(_widget.lineEditLogFile->text().toStdString());
            if (path.has_parent_path())
            {
                std::filesystem::create_directories(path.parent_path());
            }
            ARMARX_INFO << "Logging to " << path;
            _logfile.open(path.string());
            _logfile << "iteration;timestamp;adc1;adc2;adc3;adc4;adc5;acd1_temp;acd2_temp;acd3_temp;acd4_temp;acd5_temp";

            for (const auto& [name, entry] : entries)
            {
                _joints.emplace_back(entry);
                _logfile << ';' << name;
            }
            _logfile << std::endl;
        }

        _recording = true;
    }
    void FTSensorCalibrationGuiWidgetController::stopRecording()
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        _widget.pushButtonStart->setEnabled(true);
        _widget.pushButtonStop->setEnabled(false);
        _recording = false;
        _streaming_handler = nullptr;
        _logfile = std::ofstream{};
    }

    template<class T>
    T str_to_val(const auto& str)
    {
#define option(type, fn) if constexpr(std::is_same_v<T, type>) { return str.fn() ; } else
        option(double,     toDouble)
        option(float,      toFloat)
        option(int,        toInt)
        option(long,       toLong)
        option(qlonglong,  toLongLong)
        option(short,      toShort)
        option(uint,       toUInt)
        option(ulong,      toULong)
        option(qulonglong, toULongLong)
        option(ushort,     toUShort)
        {
            static_assert(!std::is_same_v<float_t, float_t>, "this function can't handle the given type");
        }
#undef option
    }

    template<class T>
    void str_to_val(const auto& str, T& val)
    {
        val = str_to_val<T>(str);
    }

    template<class T>
    T str_to_val_with_opt_on_empty(const auto& str, T opt)
    {
        return str.isEmpty() ? opt : str_to_val<T>(str);;
    }

    void read(auto& eigen, auto* table)
    {
        for (int c = 0; c < eigen.cols(); ++c)
        {
            for (int r = 0; r < eigen.rows(); ++r)
            {
                auto it = table->item(r, c);
                ARMARX_CHECK_NOT_NULL(it);
                QString str = it->text();
                if (str.isEmpty())
                {
                    str = "0";
                }
                str_to_val(it->text(), eigen(r, c));
            }
        }
    }

    void FTSensorCalibrationGuiWidgetController::updateCalibration()
    {
        ARMARX_TRACE;
        //read all tables:
        read(_conversion_matrix, _widget.tableWidgetMatrix);
        read(_offset, _widget.tableWidgetOffset);
        read(_channel_order, _widget.tableWidgetChannelOrder);
        _ticks_to_volt_factor = _widget.doubleSpinBoxTickToVolt->value() *
                                std::pow(10.0, 1.0 * _widget.doubleSpinBoxTickToVoltE->value());
    }

    void FTSensorCalibrationGuiWidgetController::updateCompensation()
    {
        ARMARX_TRACE;
        _comp_table.table.clear();
        const auto maxRows = _widget.tableWidgetCompensate->rowCount();
        //read all tables:
        for (int r = 0; r < maxRows; ++r)
        {
            const auto read_cell = [&](int col)
            {
                auto it = _widget.tableWidgetCompensate->item(r, col);
                ARMARX_CHECK_NOT_NULL(it);
                return it->text();
            };
            const auto c0 = read_cell(0);
            if (c0.isEmpty())
            {
                continue;
            }

            _comp_table.table.emplace_back(std::array
            {
                str_to_val<float>(c0),
                str_to_val_with_opt_on_empty<float>(read_cell(1), 0),
                str_to_val_with_opt_on_empty<float>(read_cell(2), 0)
            });
            std::sort(_comp_table.table.begin(), _comp_table.table.end());
        }
    }

    void FTSensorCalibrationGuiWidgetController::loadCalibFromFile()
    {
        ARMARX_TRACE;

    }

    void FTSensorCalibrationGuiWidgetController::loadDefaultArmar6FTR()
    {
        ARMARX_TRACE;
        const auto set = [&](auto tab, auto row, auto col, auto str)
        {
            auto it = tab->item(row, col);
            ARMARX_CHECK_NOT_NULL(it) << VAROUT(tab->objectName()) << '\n'
                                      << VAROUT(row) << " / " << VAROUT(col);
            it->setText(str);
        };
        set(_widget.tableWidgetMatrix, 0, 0, "-467.643251792");
        set(_widget.tableWidgetMatrix, 0, 1, "91.3750529228");
        set(_widget.tableWidgetMatrix, 0, 2, "2494.4447796887");
        set(_widget.tableWidgetMatrix, 0, 3, "-21630.9316531438");
        set(_widget.tableWidgetMatrix, 0, 4, "-2015.8633957692");
        set(_widget.tableWidgetMatrix, 0, 5, "21220.5317464812");

        set(_widget.tableWidgetMatrix, 1, 0, "-632.4535042867");
        set(_widget.tableWidgetMatrix, 1, 1, "26666.445463403");
        set(_widget.tableWidgetMatrix, 1, 2, "888.174687977");
        set(_widget.tableWidgetMatrix, 1, 3, "-12567.7138845759");
        set(_widget.tableWidgetMatrix, 1, 4, "2056.9631948303");
        set(_widget.tableWidgetMatrix, 1, 5, "-12362.9602817359");

        set(_widget.tableWidgetMatrix, 2, 0, "32117.1345018829");
        set(_widget.tableWidgetMatrix, 2, 1, "1042.3739547082");
        set(_widget.tableWidgetMatrix, 2, 2, "32619.3235489089");
        set(_widget.tableWidgetMatrix, 2, 3, "401.9862056573");
        set(_widget.tableWidgetMatrix, 2, 4, "32571.279999299");
        set(_widget.tableWidgetMatrix, 2, 5, "1238.1723510684");

        set(_widget.tableWidgetMatrix, 3, 0, "-7.9924072503");
        set(_widget.tableWidgetMatrix, 3, 1, "183.6989072793");
        set(_widget.tableWidgetMatrix, 3, 2, "-528.3700035934");
        set(_widget.tableWidgetMatrix, 3, 3, "-95.0326724598");
        set(_widget.tableWidgetMatrix, 3, 4, "529.7112223958");
        set(_widget.tableWidgetMatrix, 3, 5, "-67.4388404574");

        set(_widget.tableWidgetMatrix, 4, 0, "601.7174062086");
        set(_widget.tableWidgetMatrix, 4, 1, "19.2614267838");
        set(_widget.tableWidgetMatrix, 4, 2, "-343.4582260308");
        set(_widget.tableWidgetMatrix, 4, 3, "144.3830278106");
        set(_widget.tableWidgetMatrix, 4, 4, "-284.5086278763");
        set(_widget.tableWidgetMatrix, 4, 5, "-156.6756177634");

        set(_widget.tableWidgetMatrix, 5, 0, "6.7801800162");
        set(_widget.tableWidgetMatrix, 5, 1, "-339.2302595085");
        set(_widget.tableWidgetMatrix, 5, 2, "35.4255085497");
        set(_widget.tableWidgetMatrix, 5, 3, "-320.187165265");
        set(_widget.tableWidgetMatrix, 5, 4, "40.6862246688");
        set(_widget.tableWidgetMatrix, 5, 5, "-314.5800995414");

        set(_widget.tableWidgetOffset, 0, 0, "0");
        set(_widget.tableWidgetOffset, 1, 0, "0");
        set(_widget.tableWidgetOffset, 2, 0, "0");
        set(_widget.tableWidgetOffset, 3, 0, "0");
        set(_widget.tableWidgetOffset, 4, 0, "0");
        set(_widget.tableWidgetOffset, 5, 0, "0");

        set(_widget.tableWidgetChannelOrder, 0, 0, "0");
        set(_widget.tableWidgetChannelOrder, 1, 0, "1");
        set(_widget.tableWidgetChannelOrder, 2, 0, "2");
        set(_widget.tableWidgetChannelOrder, 3, 0, "3");
        set(_widget.tableWidgetChannelOrder, 4, 0, "4");
        set(_widget.tableWidgetChannelOrder, 5, 0, "5");

        _widget.doubleSpinBoxTickToVolt->setValue(-7.275957614183426);
        _widget.doubleSpinBoxTickToVoltE->setValue(-12);
    }

    void FTSensorCalibrationGuiWidgetController::loadDefaultArmar6FTL()
    {
        ARMARX_TRACE;
        const auto set = [&](auto tab, auto row, auto col, auto str)
        {
            auto it = tab->item(row, col);
            ARMARX_CHECK_NOT_NULL(it) << VAROUT(tab->objectName()) << '\n'
                                      << VAROUT(row) << " / " << VAROUT(col);
            it->setText(str);
        };
        set(_widget.tableWidgetMatrix, 0, 0, "243.0341527991");
        set(_widget.tableWidgetMatrix, 0, 1, "101.5053757216");
        set(_widget.tableWidgetMatrix, 0, 2, "3363.1141700153");
        set(_widget.tableWidgetMatrix, 0, 3, "-22042.8003987865");
        set(_widget.tableWidgetMatrix, 0, 4, "-1473.1343892481");
        set(_widget.tableWidgetMatrix, 0, 5, "22371.6116908654");

        set(_widget.tableWidgetMatrix, 1, 0, "-2855.7194248476");
        set(_widget.tableWidgetMatrix, 1, 1, "26276.7522410658");
        set(_widget.tableWidgetMatrix, 1, 2, "1998.8250953037");
        set(_widget.tableWidgetMatrix, 1, 3, "-12713.8569146837");
        set(_widget.tableWidgetMatrix, 1, 4, "298.7344647758");
        set(_widget.tableWidgetMatrix, 1, 5, "-12925.3451059361");

        set(_widget.tableWidgetMatrix, 2, 0, "32124.3594508476");
        set(_widget.tableWidgetMatrix, 2, 1, "1084.8849712208");
        set(_widget.tableWidgetMatrix, 2, 2, "32074.9427983169");
        set(_widget.tableWidgetMatrix, 2, 3, "1590.5268672115");
        set(_widget.tableWidgetMatrix, 2, 4, "31332.9720074452");
        set(_widget.tableWidgetMatrix, 2, 5, "1975.0541066964");

        set(_widget.tableWidgetMatrix, 3, 0, "-16.374713716");
        set(_widget.tableWidgetMatrix, 3, 1, "180.8318075047");
        set(_widget.tableWidgetMatrix, 3, 2, "-507.7906728024");
        set(_widget.tableWidgetMatrix, 3, 3, "-111.2589978068");
        set(_widget.tableWidgetMatrix, 3, 4, "503.7542430159");
        set(_widget.tableWidgetMatrix, 3, 5, "-59.7462546316");

        set(_widget.tableWidgetMatrix, 4, 0, "598.6997045889");
        set(_widget.tableWidgetMatrix, 4, 1, "15.4552761047");
        set(_widget.tableWidgetMatrix, 4, 2, "-323.6960410703");
        set(_widget.tableWidgetMatrix, 4, 3, "137.7987236203");
        set(_widget.tableWidgetMatrix, 4, 4, "-296.3155623038");
        set(_widget.tableWidgetMatrix, 4, 5, "-171.7206902305");

        set(_widget.tableWidgetMatrix, 5, 0, "49.6982738536");
        set(_widget.tableWidgetMatrix, 5, 1, "-335.3035629766");
        set(_widget.tableWidgetMatrix, 5, 2, "43.0493257519");
        set(_widget.tableWidgetMatrix, 5, 3, "-323.8948308006");
        set(_widget.tableWidgetMatrix, 5, 4, "15.3044378249");
        set(_widget.tableWidgetMatrix, 5, 5, "-329.7565028853");

        set(_widget.tableWidgetOffset, 0, 0, "0");
        set(_widget.tableWidgetOffset, 1, 0, "0");
        set(_widget.tableWidgetOffset, 2, 0, "0");
        set(_widget.tableWidgetOffset, 3, 0, "0");
        set(_widget.tableWidgetOffset, 4, 0, "0");
        set(_widget.tableWidgetOffset, 5, 0, "0");

        set(_widget.tableWidgetChannelOrder, 0, 0, "0");
        set(_widget.tableWidgetChannelOrder, 1, 0, "1");
        set(_widget.tableWidgetChannelOrder, 2, 0, "2");
        set(_widget.tableWidgetChannelOrder, 3, 0, "3");
        set(_widget.tableWidgetChannelOrder, 4, 0, "4");
        set(_widget.tableWidgetChannelOrder, 5, 0, "5");

        _widget.doubleSpinBoxTickToVolt->setValue(-7.275957614183426);
        _widget.doubleSpinBoxTickToVoltE->setValue(-12);
    }
}
