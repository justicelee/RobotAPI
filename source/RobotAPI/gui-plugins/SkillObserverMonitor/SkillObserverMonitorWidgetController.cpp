/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::SkillObserverMonitorWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

#include <ArmarXCore/util/CPPUtility/Iterator.h>

#include "SkillObserverMonitorWidgetController.h"

//config
namespace armarx
{
    QPointer<QDialog> SkillObserverMonitorWidgetController::getConfigDialog(QWidget* parent)
    {
        if (!dialog)
        {
            dialog = new SimpleConfigDialog(parent);
            dialog->addProxyFinder<skills::SkillObserverInterfacePrx>("SkillObserver", "", "Skill*");
        }
        return qobject_cast<SimpleConfigDialog*>(dialog);
    }
    void SkillObserverMonitorWidgetController::configured()
    {
        skills.observerName = dialog->getProxyName("SkillObserver");
    }
    void SkillObserverMonitorWidgetController::loadSettings(QSettings* settings)
    {
        skills.observerName = settings->value("SkillObserver", "SkillObserver").toString().toStdString();
    }

    void SkillObserverMonitorWidgetController::saveSettings(QSettings* settings)
    {
        settings->setValue("SkillObserver", QString::fromStdString(skills.observerName));
    }
}
//other
namespace armarx
{
    SkillObserverMonitorWidgetController::SkillObserverMonitorWidgetController()
    {
        widget.setupUi(getWidget());
        connect(widget.pushButtonRefreshProvidedSkills, &QPushButton::clicked,
                this, &SkillObserverMonitorWidgetController::refreshProvidedSkills);
        connect(widget.pushButtonStartSkill, &QPushButton::clicked,
                this, &SkillObserverMonitorWidgetController::startSkill);
        connect(widget.pushButtonStopSkill, &QPushButton::clicked,
                this, &SkillObserverMonitorWidgetController::stopSkill);
        connect(widget.treeWidgetSkills, &QTreeWidget::currentItemChanged,
                this, &SkillObserverMonitorWidgetController::skillSelectionChanged);
    }


    SkillObserverMonitorWidgetController::~SkillObserverMonitorWidgetController()
    {

    }

    void SkillObserverMonitorWidgetController::onInitComponent()
    {
        usingProxy(skills.observerName);
    }


    void SkillObserverMonitorWidgetController::onConnectComponent()
    {
        getProxy(skills.observer, skills.observerName);
    }

    void SkillObserverMonitorWidgetController::refreshProvidedSkills()
    {
        skills.skills = skills.observer->getSkills();
        skills.providerIndex.clear();
        widget.treeWidgetSkills->clear();
        for (const auto& [i, pr] : MakeIndexedContainer(skills.skills))
        {
            skills.providerIndex[pr.providerName] = i;
            auto it = new QTreeWidgetItem(widget.treeWidgetSkills);
            widget.treeWidgetSkills->addTopLevelItem(it);
            it->setText(0, QString::fromStdString(pr.providerName));
            for (const auto& [name, sk] : pr.skills)
            {
                auto itsk = new QTreeWidgetItem(it);
                it->addChild(itsk);
                itsk->setText(0, QString::fromStdString(name));
            }
        }
    }

    void SkillObserverMonitorWidgetController::startSkill()
    {
        if (skills.idxActiveProvider < 0 || skills.idxActiveProvider > static_cast<int>(skills.skills.size()))
        {
            return;
        }
        const auto& prv = skills.skills.at(skills.idxActiveProvider);
        if (!prv.skills.count(skills.selectedSkill))
        {
            return;
        }
        skills::SkillParametrization param;
        param.skillName = skills.selectedSkill;
        prv.provider->executeSkill(param);
    }

    void SkillObserverMonitorWidgetController::stopSkill()
    {
        if (skills.idxActiveProvider < 0 || skills.idxActiveProvider > static_cast<int>(skills.skills.size()))
        {
            return;
        }
        const auto& prv = skills.skills.at(skills.idxActiveProvider);
        if (!prv.skills.count(skills.selectedSkill))
        {
            return;
        }
        prv.provider->abortSkill(skills.selectedSkill);
    }

    void SkillObserverMonitorWidgetController::skillSelectionChanged(QTreeWidgetItem* current, QTreeWidgetItem*)
    {
        skills.idxActiveProvider = -1;
        widget.groupBoxSkillDetails->setEnabled(false);

        if (!current->parent())
        {
            return;
        }
        skills.idxActiveProvider = skills.providerIndex.at(current->parent()->text(0).toStdString());
        skills.selectedSkill     = current->text(0).toStdString();
        widget.groupBoxSkillDetails->setEnabled(true);
    }
}

