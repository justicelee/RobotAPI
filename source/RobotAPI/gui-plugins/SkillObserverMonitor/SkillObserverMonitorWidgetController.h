/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::SkillObserverMonitorWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/interface/skills/SkillObserverInterface.h>

#include <RobotAPI/gui-plugins/SkillObserverMonitor/ui_SkillObserverMonitorWidget.h>

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-SkillObserverMonitor SkillObserverMonitor
    \brief The SkillObserverMonitor allows visualizing ...

    \image html SkillObserverMonitor.png
    The user can

    API Documentation \ref SkillObserverMonitorWidgetController

    \see SkillObserverMonitorGuiPlugin
    */


    /**
     * \class SkillObserverMonitorGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief SkillObserverMonitorGuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * \class SkillObserverMonitorWidgetController
     * \brief SkillObserverMonitorWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        SkillObserverMonitorWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < SkillObserverMonitorWidgetController >
    {
        Q_OBJECT

    public:
        /// Controller Constructor
        explicit SkillObserverMonitorWidgetController();
        /// Controller destructor
        virtual ~SkillObserverMonitorWidgetController();

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Skills.ObserverMonitor";
        }

        /// \see armarx::Component::onInitComponent()
        void onInitComponent() override;

        /// \see armarx::Component::onConnectComponent()
        void onConnectComponent() override;

    private slots:
        void skillSelectionChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous);
        void stopSkill();
        void startSkill();
        void refreshProvidedSkills();

    private:
        /**
         * Widget Form
         */
        Ui::SkillObserverMonitorWidget  widget;
        QPointer<SimpleConfigDialog>    dialog;
        struct SkillData
        {
            std::string                         observerName;
            skills::SkillObserverInterfacePrx   observer;
            skills::ProviderSkillsSeq           skills;
            std::map<std::string, int>          providerIndex;
            int                                 idxActiveProvider = -1;
            std::string                         selectedSkill;
        };
        SkillData skills;
    };
}


