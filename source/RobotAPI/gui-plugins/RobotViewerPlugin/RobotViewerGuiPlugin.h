/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::RobotViewerGuiPlugin
* @author     Nikolaus Vahrenkamp
* @copyright  2015 KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#pragma once

/* ArmarX headers */
#include <RobotAPI/gui-plugins/RobotViewerPlugin/ui_RobotViewerGuiPlugin.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <ArmarXCore/core/Component.h>

/* Qt headers */
#include <QMainWindow>


#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Visualization/VisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

#include <Inventor/sensors/SoTimerSensor.h>

namespace armarx
{
    class RobotViewerConfigDialog;

    /**
      \class RobotViewerGuiPlugin
      \brief This plugin provides a generic widget showing a 3D model of the robot. The robot is articulated and moved according to the corresponding RemoteRobot.
      Further, DebugDrawer methods are available.
      \see RobotViewerWidget
      */
    class RobotViewerGuiPlugin :
        public ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        RobotViewerGuiPlugin();

        QString getPluginName() override
        {
            return "RobotViewerGuiPlugin";
        }
    };

    /**
     \page RobotAPI-GuiPlugins-RobotViewerPlugin RobotViewerPlugin

     \brief This widget shows a 3D model of the robot. The robot is articulated and moved according to the corresponding RemoteRobot.
     Further, DebugDrawer methods are available.
     \image html RobotViewerGUI.png
     When you add the widget to the Gui, you need to specify the following parameters:

     Parameter Name   | Example Value     | Required?     | Description
     :----------------  | :-------------:   | :-------------- |:--------------------
     Proxy     | RobotStateComponent   | Yes | The name of the robot state component.
     */
    class RobotViewerWidgetController :
        public ArmarXComponentWidgetControllerTemplate<RobotViewerWidgetController>,
        public RobotStateListenerInterface
    {
        Q_OBJECT
    public:

        RobotViewerWidgetController();
        ~RobotViewerWidgetController() override {}

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        // inherited from ArmarXWidget
        static QString GetWidgetName()
        {
            return "Visualization.RobotViewerGUI";
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        void configured() override;

        SoNode* getScene() override;

        // overwrite setMutex, so that we can inform the debugdrawer
        void setMutex3D(RecursiveMutexPtr const& mutex3D) override;

    signals:
        void robotStatusUpdated();
        void configurationChanged();

    public slots:
        void updateRobotVisu();
        void inflateCollisionModel(int inflationValueMM);

    protected:
        void connectSlots();


        Ui::RobotViewerGuiPlugin ui;

        bool verbose;


        RobotStateComponentInterfacePrx robotStateComponentPrx;
        VirtualRobot::RobotPtr robot;

        NameList robotNodeSetNames;
        NameList robotNodeNames;

        std::string robotStateComponentName;

        SoSeparator* rootVisu;
        SoSeparator* robotVisu;

        SoTimerSensor* timerSensor;

        SoSeparator* debugLayerVisu;
        armarx::DebugDrawerComponentPtr debugDrawer;

        static void timerCB(void* data, SoSensor* sensor);
        void setRobotVisu(bool colModel);

    protected slots:
        void showVisuLayers(bool show);
        void showRoot(bool show);
        void showRobot(bool show);
        void colModel(bool c);

        void updateStateSettings(int);
        void copyToClipboard();
        void updateState();
        void onConfigurationChanged();

    private:

        // init stuff
        VirtualRobot::RobotPtr loadRobotFile(std::string fileName);

        VirtualRobot::CoinVisualizationPtr getCoinVisualization(VirtualRobot::RobotPtr robot);

        QPointer<QWidget> __widget;
        QPointer<SimpleConfigDialog> dialog;

        // ArmarXWidgetController interface
    public:
        static QIcon GetWidgetIcon()
        {
            return QIcon(":icons/armarx-gui.png");
        }

        // RobotStateListenerInterface interface
    public:
        void reportGlobalRobotRootPose(const FramedPoseBasePtr& pose, Ice::Long timestamp, bool poseChanged, const Ice::Current&) override;
        void reportJointValues(const NameValueMap& jointAngles, Ice::Long timestamp, bool aValueChanged, const Ice::Current&) override;
    };
    using RobotViewerGuiPluginPtr = std::shared_ptr<RobotViewerWidgetController>;
}

