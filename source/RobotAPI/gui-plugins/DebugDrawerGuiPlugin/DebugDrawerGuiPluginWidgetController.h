/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::DebugDrawerGuiPluginWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/gui-plugins/DebugDrawerGuiPlugin/ui_DebugDrawerGuiPluginWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-DebugDrawerGuiPlugin DebugDrawerGuiPlugin
    \brief The DebugDrawerGuiPlugin allows visualizing ...

    \image html DebugDrawerGuiPlugin.png
    The user can

    API Documentation \ref DebugDrawerGuiPluginWidgetController

    \see DebugDrawerGuiPluginGuiPlugin
    */

    /**
     * \class DebugDrawerGuiPluginWidgetController
     * \brief DebugDrawerGuiPluginWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        DebugDrawerGuiPluginWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < DebugDrawerGuiPluginWidgetController >
    {
        Q_OBJECT

    public:
        explicit DebugDrawerGuiPluginWidgetController();
        virtual ~DebugDrawerGuiPluginWidgetController();

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Visualization.DebugDrawerGuiPlugin";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        //        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        //        void configured() override;

    private slots:
        void on_pushButtonLayerClear_clicked();
        void on_pushButtonPoseDelete_clicked();
        void on_pushButtonArrowDelete_clicked();
        void updateArrow();
        void updatePose();

    private:
        /**
         * Widget Form
         */
        Ui::DebugDrawerGuiPluginWidget _ui;

        std::string                     _layerName;

        std::string                     _debugDrawerTopicName{"DebugDrawerUpdates"};
        DebugDrawerHelper               _debugDrawer;

        QPointer<SimpleConfigDialog>    _dialog;

    };
}


