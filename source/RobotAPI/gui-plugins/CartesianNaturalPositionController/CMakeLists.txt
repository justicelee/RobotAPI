armarx_set_target("CartesianNaturalPositionControllerGuiPlugin")

armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

set(SOURCES
    CartesianNaturalPositionControllerGuiPlugin.cpp
    CartesianNaturalPositionControllerWidgetController.cpp
)

set(HEADERS
    CartesianNaturalPositionControllerGuiPlugin.h
    CartesianNaturalPositionControllerWidgetController.h
)

set(GUI_MOC_HDRS ${HEADERS})

set(GUI_UIS CartesianNaturalPositionControllerWidget.ui)

set(COMPONENT_LIBS
    VirtualRobot

    SimpleConfigDialog

    RobotAPIInterfaces
    RobotAPICore
    natik
)

if(ArmarXGui_FOUND)
    armarx_gui_library(CartesianNaturalPositionControllerGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
