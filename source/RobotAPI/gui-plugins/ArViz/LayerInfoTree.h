#pragma once

#include <QTreeWidget>

#include <SimoxUtility/json/json.hpp>

#include <RobotAPI/components/ArViz/Coin/Visualizer.h>


namespace armarx
{

    /**
     * @brief Manages the layer info `QTreeWidget`.
     */
    class LayerInfoTree : public QObject
    {
        Q_OBJECT

    public:

        LayerInfoTree();
        virtual ~LayerInfoTree() = default;

        void setWidget(QTreeWidget* treeWidget);
        void registerVisualizerCallbacks(viz::CoinVisualizer& visualizer);
        void setMaxElementCountDefault(int max);

        /**
         * @brief Enable or disable the layer info tree.
         *
         * When enabled, the tree will be filled on the first update.
         * When disabled, the tree is cleared and will not be updated.
         */
        void setEnabled(bool enabled);

        /// Set the selected layer.
        void setSelectedLayer(viz::CoinLayerID id, viz::CoinLayer* layer);

        /// Update the currently expanded layer elements.
        void update();


    public:
    signals:


    public slots:


    private:

        void clearLayerElements();

        /**
         * @brief Updates the list of layer elements.
         *
         * Items are inserted, removed and updated accordingly.
         */
        void updateLayerElements(const std::map<std::string, viz::CoinLayerElement>& elements);

        /// Insert a (top-level) layer element at `ìndex`.
        QTreeWidgetItem* insertLayerElementItem(int index, const std::string& name, const viz::data::ElementPtr& element);
        /// Update the layer element's item data. Does not update its children.
        void updateLayerElementItem(QTreeWidgetItem* item, const viz::data::ElementPtr& element);

        /// Update the layer element's properties (children).
        void updateLayerElementProperties(QTreeWidgetItem* item, const viz::data::ElementPtr& element);


        /// Get `element`'s type name.
        std::string getTypeName(const viz::data::ElementPtr& element) const;

        /// Serialize `element` to JSON. If `element`'s type is unknown, returns an empty object.
        nlohmann::json serializeElement(const viz::data::ElementPtr& element) const;


        /**
         * @brief Updates `parent`'s children according to `json`.
         *
         * Items are added, deleted and updated accordingly.
         *
         * @param recursive If true,
         */
        void updateJsonChildren(QTreeWidgetItem* parent, const nlohmann::json& json, bool recursive = false);

        /// Adds a child item with `key` to `parent`.
        QTreeWidgetItem* addJsonChild(QTreeWidgetItem* parent, const std::string& key);

        /**
         * @brief Updates `item`'s key and value.
         * @param recursive If true, also update its children.
         */
        void updateJsonItem(QTreeWidgetItem* item, const std::string& key, const nlohmann::json& value,
                            const nlohmann::json& parentMeta,
                            bool recursive = false);

        void updateJsonItemValue(QTreeWidgetItem* item, const std::string& key, const nlohmann::json& value,
                                 const nlohmann::json& parentMeta);


        QVariant getValuePreview(const nlohmann::json& json) const;
        QWidget* getValueWidget(QTreeWidgetItem* item, const nlohmann::json& json) const;
        void updateValueWidget(const nlohmann::json& json, QWidget* widget) const;

        static bool isMetaKey(const std::string&);


        void addShowMoreItem();


    private:

        QTreeWidget* widget = nullptr;
        /// Connections to the current widget.
        QVector<QMetaObject::Connection> widgetConnections;

        bool enabled = true;

        QTreeWidgetItem* showMoreItem = nullptr;
        int maxElementCountDefault = 25;
        int maxElementCount = maxElementCountDefault;

        viz::CoinLayerID layerID = {"", ""};
        const viz::CoinLayer* layer = nullptr;



    };
}
