armarx_set_target("ArVizGuiPlugin")

# most qt components will be linked against in the call armarx_gui_library
#armarx_find_qt(QtCore QtGui QtDesigner)

# ArmarXGui gets included through depends_on_armarx_package(ArmarXGui "OPTIONAL")
# in the toplevel CMakeLists.txt
armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

set(SOURCES
    ArVizGuiPlugin.cpp
    ArVizWidgetController.cpp

    LayerInfoTree.cpp
)

# do not rename this variable, it is used in armarx_gui_library()...
set(HEADERS
    ArVizGuiPlugin.h
    ArVizWidgetController.h

    LayerInfoTree.h
)

set(GUI_MOC_HDRS ${HEADERS})

set(GUI_UIS ArVizWidget.ui)

# Add more libraries you depend on here, e.g. ${QT_LIBRARIES}.
set(COMPONENT_LIBS
    ArViz
    SimpleConfigDialog
)

set(GUI_RCS icons.qrc)

if(ArmarXGui_FOUND)
    armarx_gui_library(ArVizGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "${GUI_RCS}" "${COMPONENT_LIBS}")

    #find_package(MyLib QUIET)
    #armarx_build_if(MyLib_FOUND "MyLib not available")
    # all target_include_directories must be guarded by if(Xyz_FOUND)
    # for multiple libraries write: if(X_FOUND AND Y_FOUND)....
    #if(MyLib_FOUND)
    #    target_include_directories(ArVizGuiPlugi PUBLIC ${MyLib_INCLUDE_DIRS})
    #endif()
endif()
