/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "KinematicUnitGuiPlugin.h"
#include "KinematicUnitConfigDialog.h"

#include <RobotAPI/gui-plugins/KinematicUnitPlugin/ui_KinematicUnitConfigDialog.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/ArmarXManager.h>

#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/core/util/algorithm.h>

#include <VirtualRobot/XML/RobotIO.h>
#include <SimoxUtility/json.h>

// Qt headers
#include <Qt>
#include <QtGlobal>
#include <QSpinBox>
#include <QSlider>
#include <QPushButton>
#include <QStringList>
#include <QTableView>
#include <QCheckBox>
#include <QTableWidget>
#include <QClipboard>
#include <QInputDialog>

#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <ArmarXCore/observers/filters/MedianFilter.h>

// System
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <cmath>

#include <filesystem>


//#define KINEMATIC_UNIT_FILE_DEFAULT std::string("RobotAPI/robots/Armar3/ArmarIII.xml")
//#define KINEMATIC_UNIT_FILE_DEFAULT_PACKAGE std::string("RobotAPI")
#define KINEMATIC_UNIT_NAME_DEFAULT "Robot"
//#define TOPIC_NAME_DEFAULT "RobotState"
#define SLIDER_POS_DEG_MULTIPLIER 5
#define SLIDER_POS_RAD_MULTIPLIER 100

using namespace armarx;
using namespace VirtualRobot;

KinematicUnitGuiPlugin::KinematicUnitGuiPlugin()
{
    addWidget<KinematicUnitWidgetController>();
}

KinematicUnitWidgetController::KinematicUnitWidgetController() :
    kinematicUnitNode(nullptr),
    enableValueValidator(true),
    historyTime(100000), // 1/10 s
    currentValueMax(5.0f),
    selectedControlMode(ePositionControl)
{
    rootVisu = NULL;
    debugLayerVisu = NULL;

    // init gui
    ui.setupUi(getWidget());
    getWidget()->setEnabled(false);

    ui.tableJointList->setItemDelegateForColumn(eTabelColumnAngleProgressbar, &delegate);
}

void KinematicUnitWidgetController::onInitComponent()
{
    dirty_squaresum_old.resize(5, 0);
    ARMARX_INFO << flush;
    verbose = true;


    rootVisu = new SoSeparator;
    rootVisu->ref();
    robotVisu = new SoSeparator;
    robotVisu->ref();
    rootVisu->addChild(robotVisu);

    // create the debugdrawer component
    std::string debugDrawerComponentName = "KinemticUnitGUIDebugDrawer_" + getName();
    ARMARX_INFO << "Creating component " << debugDrawerComponentName;
    debugDrawer = Component::create<DebugDrawerComponent>(getIceProperties(), debugDrawerComponentName);
    showVisuLayers(false);

    if (mutex3D)
    {
        //ARMARX_IMPORTANT << "mutex3d:" << mutex3D.get();
        debugDrawer->setMutex(mutex3D);
    }
    else
    {
        ARMARX_ERROR << " No 3d mutex available...";
    }

    ArmarXManagerPtr m = getArmarXManager();
    m->addObject(debugDrawer, false);


    {
        std::unique_lock lock(*mutex3D);
        debugLayerVisu = new SoSeparator();
        debugLayerVisu->ref();
        debugLayerVisu->addChild(debugDrawer->getVisualization());
        rootVisu->addChild(debugLayerVisu);
    }

    connectSlots();

    usingProxy(kinematicUnitName);
}

void KinematicUnitWidgetController::onConnectComponent()
{
    ARMARX_INFO << "kinematic unit gui :: onConnectComponent()";
    reportedJointAngles.clear();
    reportedJointVelocities.clear();
    reportedJointControlModes.clear();
    reportedJointTorques.clear();
    jointCurrentHistory.clear();
    reportedJointStatuses.clear();
    reportedJointTemperatures.clear();


    jointAnglesUpdateFrequency = new filters::MedianFilter(100);
    kinematicUnitInterfacePrx = getProxy<KinematicUnitInterfacePrx>(kinematicUnitName);
    topicName = kinematicUnitInterfacePrx->getReportTopicName();
    lastJointAngleUpdateTimestamp = TimeUtil::GetTime().toSecondsDouble();
    robotVisu->removeAllChildren();

    robot.reset();

    std::string rfile;
    Ice::StringSeq includePaths;

    // get robot filename
    try
    {

        Ice::StringSeq packages = kinematicUnitInterfacePrx->getArmarXPackages();
        packages.push_back(Application::GetProjectName());
        ARMARX_VERBOSE << "ArmarX packages " << packages;

        for (const std::string& projectName : packages)
        {
            if (projectName.empty())
            {
                continue;
            }

            CMakePackageFinder project(projectName);
            auto pathsString = project.getDataDir();
            ARMARX_VERBOSE << "Data paths of ArmarX package " << projectName << ": " << pathsString;
            Ice::StringSeq projectIncludePaths = Split(pathsString, ";,", true, true);
            ARMARX_VERBOSE << "Result: Data paths of ArmarX package " << projectName << ": " << projectIncludePaths;
            includePaths.insert(includePaths.end(), projectIncludePaths.begin(), projectIncludePaths.end());
        }

        rfile = kinematicUnitInterfacePrx->getRobotFilename();
        ARMARX_VERBOSE << "Relative robot file " << rfile;
        ArmarXDataPath::getAbsolutePath(rfile, rfile, includePaths);
        ARMARX_VERBOSE << "Absolute robot file " << rfile;

        robotNodeSetName = kinematicUnitInterfacePrx->getRobotNodeSetName();
    }
    catch (...)
    {
        ARMARX_ERROR << "Unable to retrieve robot filename";
    }

    try
    {
        ARMARX_INFO << "Loading robot from file " << rfile;
        robot = loadRobotFile(rfile);
    }
    catch (...)
    {
        ARMARX_ERROR << "Failed to init robot";
    }

    if (!robot || !robot->hasRobotNodeSet(robotNodeSetName))
    {
        getObjectScheduler()->terminate();

        if (getWidget()->parentWidget())
        {
            getWidget()->parentWidget()->close();
        }

        std::cout << "returning" << std::endl;
        return;
    }

    kinematicUnitFile = rfile;
    robotNodeSet = robot->getRobotNodeSet(robotNodeSetName);


    initGUIComboBox(robotNodeSet); // init the pull down menu (QT: ComboBox)
    initGUIJointListTable(robotNodeSet);

    kinematicUnitVisualization = getCoinVisualization(robot);
    kinematicUnitNode = kinematicUnitVisualization->getCoinVisualization();
    robotVisu->addChild(kinematicUnitNode);

    // init control mode map
    try
    {
        reportedJointControlModes = kinematicUnitInterfacePrx->getControlModes();
    }
    catch (...)
    {
    }

    initializeUi();

    usingTopic(topicName);
    QMetaObject::invokeMethod(this, "resetSlider");
    enableMainWidgetAsync(true);
    updateTimerId = startTimer(1000); // slow timer that only ensures that skipped updates are shown at all
}

void KinematicUnitWidgetController::onDisconnectComponent()
{
    killTimer(updateTimerId);
    enableMainWidgetAsync(false);

    {
        std::unique_lock lock(mutexNodeSet);
        robot.reset();
        robotNodeSet.reset();
        currentNode.reset();
    }

    {
        std::unique_lock lock(*mutex3D);
        robotVisu->removeAllChildren();
        debugLayerVisu->removeAllChildren();
    }
}

void KinematicUnitWidgetController::onExitComponent()
{
    enableMainWidgetAsync(false);

    {
        std::unique_lock lock(*mutex3D);

        if (robotVisu)
        {
            robotVisu->removeAllChildren();
            robotVisu->unref();
            robotVisu = NULL;
        }

        if (debugLayerVisu)
        {
            debugLayerVisu->removeAllChildren();
            debugLayerVisu->unref();
            debugLayerVisu = NULL;
        }

        if (rootVisu)
        {
            rootVisu->removeAllChildren();
            rootVisu->unref();
            rootVisu = NULL;
        }
    }

    /*
        if (debugDrawer && debugDrawer->getObjectScheduler())
        {
            ARMARX_INFO << "Removing DebugDrawer component...";
            debugDrawer->getObjectScheduler()->terminate();
            ARMARX_INFO << "Removing DebugDrawer component...done";
        }
    */
}

QPointer<QDialog> KinematicUnitWidgetController::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new KinematicUnitConfigDialog(parent);
        dialog->setName(dialog->getDefaultName());
    }

    return qobject_cast<KinematicUnitConfigDialog*>(dialog);
}

void KinematicUnitWidgetController::configured()
{
    ARMARX_VERBOSE << "KinematicUnitWidget::configured()";
    kinematicUnitName = dialog->proxyFinder->getSelectedProxyName().toStdString();
    enableValueValidator = dialog->ui->checkBox->isChecked();
    viewerEnabled = dialog->ui->checkBox3DViewerEnabled->isChecked();
    historyTime = dialog->ui->spinBoxHistory->value() * 1000;
    currentValueMax = dialog->ui->doubleSpinBoxMaxMinCurrent->value();
}

void KinematicUnitWidgetController::loadSettings(QSettings* settings)
{
    kinematicUnitName = settings->value("kinematicUnitName", KINEMATIC_UNIT_NAME_DEFAULT).toString().toStdString();
    enableValueValidator = settings->value("enableValueValidator", true).toBool();
    viewerEnabled = settings->value("viewerEnabled", true).toBool();
    historyTime = settings->value("historyTime", 100).toInt() * 1000;
    currentValueMax = settings->value("currentValueMax", 5.0).toFloat();
}

void KinematicUnitWidgetController::saveSettings(QSettings* settings)
{
    settings->setValue("kinematicUnitName", QString::fromStdString(kinematicUnitName));
    settings->setValue("enableValueValidator", enableValueValidator);
    settings->setValue("viewerEnabled", viewerEnabled);
    assert(historyTime % 1000 == 0);
    settings->setValue("historyTime", (int)(historyTime / 1000));
    settings->setValue("currentValueMax", currentValueMax);
}




void KinematicUnitWidgetController::showVisuLayers(bool show)
{
    if (debugDrawer)
    {
        if (show)
        {
            debugDrawer->enableAllLayers();
        }
        else
        {
            debugDrawer->disableAllLayers();
        }
    }
}

void KinematicUnitWidgetController::copyToClipboard()
{
    NameValueMap values;
    {
        std::unique_lock lock(mutexNodeSet);

        if (selectedControlMode == ePositionControl)
        {
            values = reportedJointAngles;
        }
        else if (selectedControlMode == eVelocityControl)
        {
            values = reportedJointVelocities;
        }
    }

    JSONObjectPtr serializer = new JSONObject();
    for (auto& kv : values)
    {
        serializer->setFloat(kv.first, kv.second);
    }
    const QString json = QString::fromStdString(serializer->asString(true));
    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setText(json);
    QApplication::processEvents();
}


void KinematicUnitWidgetController::updateGuiElements()
{
    // modelUpdateCB();
}

void KinematicUnitWidgetController::updateKinematicUnitListInDialog() {}

void KinematicUnitWidgetController::modelUpdateCB()
{
}

SoNode* KinematicUnitWidgetController::getScene()
{

    if (viewerEnabled)
    {
        ARMARX_INFO << "Returning scene ";
        return rootVisu;
    }
    else
    {
        ARMARX_INFO << "viewer disabled - returning null scene";
        return NULL;
    }
}

void KinematicUnitWidgetController::connectSlots()
{
    connect(ui.pushButtonKinematicUnitPos1,  SIGNAL(clicked()), this, SLOT(kinematicUnitZeroPosition()));

    connect(ui.nodeListComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(selectJoint(int)));
    connect(ui.horizontalSliderKinematicUnitPos, SIGNAL(valueChanged(int)), this, SLOT(sliderValueChanged(int)));

    connect(ui.horizontalSliderKinematicUnitPos, SIGNAL(sliderReleased()), this, SLOT(resetSliderToZeroPosition()));

    connect(ui.radioButtonPositionControl, SIGNAL(clicked(bool)), this, SLOT(setControlModePosition()));
    connect(ui.radioButtonVelocityControl, SIGNAL(clicked(bool)), this, SLOT(setControlModeVelocity()));
    connect(ui.radioButtonTorqueControl, SIGNAL(clicked(bool)), this, SLOT(setControlModeTorque()));
    connect(ui.pushButtonFromJson, SIGNAL(clicked()), this, SLOT(on_pushButtonFromJson_clicked()));

    connect(ui.copyToClipboard, SIGNAL(clicked()), this, SLOT(copyToClipboard()));
    connect(ui.showDebugLayer, SIGNAL(toggled(bool)), this, SLOT(showVisuLayers(bool)), Qt::QueuedConnection);

    connect(this, SIGNAL(jointAnglesReported()), this, SLOT(updateJointAnglesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointVelocitiesReported()), this, SLOT(updateJointVelocitiesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointTorquesReported()), this, SLOT(updateJointTorquesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointCurrentsReported()), this, SLOT(updateJointCurrentsTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointMotorTemperaturesReported()), this, SLOT(updateMotorTemperaturesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointControlModesReported()), this, SLOT(updateControlModesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointStatusesReported()), this, SLOT(updateJointStatusesTable()), Qt::QueuedConnection);

    connect(ui.tableJointList, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(selectJointFromTableWidget(int, int)), Qt::QueuedConnection);

    connect(ui.checkBoxUseDegree, SIGNAL(clicked()), this, SLOT(resetSlider()), Qt::QueuedConnection);
    connect(ui.checkBoxUseDegree, SIGNAL(clicked()), this, SLOT(setControlModePosition()));
    connect(ui.checkBoxUseDegree, SIGNAL(clicked()), this, SLOT(setControlModeVelocity()));
    connect(ui.checkBoxUseDegree, SIGNAL(clicked()), this, SLOT(setControlModeTorque()));
}

void KinematicUnitWidgetController::initializeUi()
{
    //signal clicked is not emitted if you call setDown(), setChecked() or toggle().
    ui.radioButtonPositionControl->setChecked(true);
    ui.widgetSliderFactor->setVisible(false);
    setControlModePosition();
    updateControlModesTable();
}

void KinematicUnitWidgetController::kinematicUnitZeroPosition()
{
    if (!robotNodeSet)
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    NameValueMap jointAngles;
    NameControlModeMap jointModes;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        jointModes[rn[i]->getName()] = ePositionControl;
        jointAngles[rn[i]->getName()] = 0.0f;
    }

    try
    {
        kinematicUnitInterfacePrx->switchControlMode(jointModes);
        kinematicUnitInterfacePrx->setJointAngles(jointAngles);
    }
    catch (...)
    {
    }

    //set slider to 0 if position control mode is used
    if (selectedControlMode == ePositionControl)
    {
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
    }
}
void KinematicUnitWidgetController::kinematicUnitZeroVelocity()
{
    if (!robotNodeSet)
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    NameValueMap vels;
    NameControlModeMap jointModes;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        jointModes[rn[i]->getName()] = eVelocityControl;
        vels[rn[i]->getName()] = 0.0f;
    }

    try
    {
        kinematicUnitInterfacePrx->switchControlMode(jointModes);
        kinematicUnitInterfacePrx->setJointVelocities(vels);
    }
    catch (...)
    {
    }

    if (selectedControlMode == eVelocityControl)
    {
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
    }
}
void KinematicUnitWidgetController::kinematicUnitZeroTorque()
{
    if (!robotNodeSet)
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    NameValueMap tor;
    NameControlModeMap jointModes;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        jointModes[rn[i]->getName()] = eTorqueControl;
        tor[rn[i]->getName()] = 0.0f;
    }

    try
    {
        kinematicUnitInterfacePrx->switchControlMode(jointModes);
        kinematicUnitInterfacePrx->setJointTorques(tor);
    }
    catch (...)
    {
    }

    if (selectedControlMode == eVelocityControl)
    {
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
    }
}

void KinematicUnitWidgetController::resetSlider()
{
    if (selectedControlMode == eVelocityControl || selectedControlMode == eTorqueControl)
    {
        resetSliderToZeroPosition();
    }
    else if (selectedControlMode == ePositionControl)
    {
        if (currentNode)
        {
            const bool isDeg = ui.checkBoxUseDegree->isChecked();
            const bool isRot = currentNode->isRotationalJoint();
            const auto factor = isRot && ! isDeg ? SLIDER_POS_RAD_MULTIPLIER : SLIDER_POS_DEG_MULTIPLIER;
            float conversionFactor = isRot && isDeg ? 180.0 / M_PI : 1.0f;
            float pos = currentNode->getJointValue() * conversionFactor;

            ui.lcdNumberKinematicUnitJointValue->display((int)pos);
            ui.horizontalSliderKinematicUnitPos->setSliderPosition((int)(pos * factor));
        }
    }

}

void KinematicUnitWidgetController::resetSliderToZeroPosition()
{
    if (selectedControlMode == eVelocityControl || selectedControlMode == eTorqueControl)
    {
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
        ui.lcdNumberKinematicUnitJointValue->display(SLIDER_ZERO_POSITION);
    }
}

void KinematicUnitWidgetController::setControlModePosition()
{
    if (!ui.radioButtonPositionControl->isChecked())
    {
        return;
    }
    NameControlModeMap jointModes;
    selectedControlMode = ePositionControl;
    ui.widgetSliderFactor->setVisible(false);

    if (currentNode)
    {
        QString unit = currentNode->isRotationalJoint() ?
                       (ui.checkBoxUseDegree->isChecked() ? "deg" : "rad") :
                       "mm";
        ui.labelUnit->setText(unit);
        const bool isDeg = ui.checkBoxUseDegree->isChecked();
        const bool isRot = currentNode->isRotationalJoint();
        const auto factor = isRot && ! isDeg ? SLIDER_POS_RAD_MULTIPLIER : SLIDER_POS_DEG_MULTIPLIER;
        float conversionFactor = isRot && isDeg ? 180.0 / M_PI : 1.0f;
        jointModes[currentNode->getName()] = ePositionControl;

        if (kinematicUnitInterfacePrx)
        {
            kinematicUnitInterfacePrx->switchControlMode(jointModes);
        }

        float lo = currentNode->getJointLimitLo() * conversionFactor;
        float hi = currentNode->getJointLimitHi() * conversionFactor;

        if (hi - lo <= 0.0f)
        {
            return;
        }

        float pos = currentNode->getJointValue() * conversionFactor;
        ARMARX_INFO << "setting position control for current Node Name: " << currentNode->getName() << " with current value: " << pos;

        // Setting the slider position to pos will set the position to the slider tick closest to pos
        // This will initially send a position target with a small delta to the joint.
        ui.horizontalSliderKinematicUnitPos->blockSignals(true);

        ui.horizontalSliderKinematicUnitPos->setMaximum(hi * factor);
        ui.horizontalSliderKinematicUnitPos->setMinimum(lo * factor);
        ui.lcdNumberKinematicUnitJointValue->display(pos);

        ui.horizontalSliderKinematicUnitPos->blockSignals(false);
        resetSlider();
    }
}

void KinematicUnitWidgetController::setControlModeVelocity()
{
    if (!ui.radioButtonVelocityControl->isChecked())
    {
        return;
    }
    NameControlModeMap jointModes;

    if (currentNode)
    {
        jointModes[currentNode->getName()] = eVelocityControl;
        const bool isDeg = ui.checkBoxUseDegree->isChecked();
        const bool isRot = currentNode->isRotationalJoint();
        QString unit = isRot ?
                       (isDeg ? "deg/s" : "rad/(100*s)") :
                       "mm/s";
        ui.labelUnit->setText(unit);
        ARMARX_INFO << "setting velocity control for current Node Name: " << currentNode->getName() << flush;
        float lo = isRot ? (isDeg ? -90 : -M_PI * 100) : -1000;
        float hi = isRot ? (isDeg ? +90 : +M_PI * 100) : 1000;

        try
        {
            if (kinematicUnitInterfacePrx)
            {
                kinematicUnitInterfacePrx->switchControlMode(jointModes);
            }
        }
        catch (...)
        {

        }

        selectedControlMode = eVelocityControl;
        ui.widgetSliderFactor->setVisible(true);

        ui.horizontalSliderKinematicUnitPos->blockSignals(true);
        ui.horizontalSliderKinematicUnitPos->setMaximum(hi);
        ui.horizontalSliderKinematicUnitPos->setMinimum(lo);
        ui.horizontalSliderKinematicUnitPos->blockSignals(false);
        resetSlider();


    }

}

void KinematicUnitWidgetController::setControlModeTorque()
{
    if (!ui.radioButtonTorqueControl->isChecked())
    {
        return;
    }
    NameControlModeMap jointModes;

    if (currentNode)
    {
        jointModes[currentNode->getName()] = eTorqueControl;
        ui.labelUnit->setText("Ncm");
        ARMARX_INFO << "setting torque control for current Node Name: " << currentNode->getName() << flush;

        if (kinematicUnitInterfacePrx)
        {
            try
            {
                kinematicUnitInterfacePrx->switchControlMode(jointModes);
            }
            catch (...)
            {

            }
        }

        ui.horizontalSliderKinematicUnitPos->blockSignals(true);
        ui.horizontalSliderKinematicUnitPos->setMaximum(20000.0);
        ui.horizontalSliderKinematicUnitPos->setMinimum(-20000.0);

        selectedControlMode = eTorqueControl;
        ui.widgetSliderFactor->setVisible(true);

        ui.horizontalSliderKinematicUnitPos->blockSignals(false);
        resetSlider();

    }


}

VirtualRobot::RobotPtr KinematicUnitWidgetController::loadRobotFile(std::string fileName)
{
    VirtualRobot::RobotPtr robot;

    if (verbose)
    {
        ARMARX_INFO << "Loading KinematicUnit " << kinematicUnitName << " from " << kinematicUnitFile << " ..." << flush;
    }

    if (!ArmarXDataPath::getAbsolutePath(fileName, fileName))
    {
        ARMARX_INFO << "Could not find Robot XML file with name " << fileName << flush;
    }

    robot = RobotIO::loadRobot(fileName);

    if (!robot)
    {
        ARMARX_INFO << "Could not find Robot XML file with name " << fileName << "(" << kinematicUnitName << ")" << flush;
    }

    return robot;
}

VirtualRobot::CoinVisualizationPtr KinematicUnitWidgetController::getCoinVisualization(VirtualRobot::RobotPtr robot)
{
    CoinVisualizationPtr coinVisualization;

    if (robot != NULL)
    {
        ARMARX_INFO << "getting coin visualization" << flush;
        coinVisualization = robot->getVisualization<CoinVisualization>();

        if (!coinVisualization || !coinVisualization->getCoinVisualization())
        {
            ARMARX_INFO << "could not get coin visualization" << flush;
        }
    }

    return coinVisualization;
}

VirtualRobot::RobotNodeSetPtr KinematicUnitWidgetController::getRobotNodeSet(VirtualRobot::RobotPtr robot, std::string nodeSetName)
{
    VirtualRobot::RobotNodeSetPtr nodeSetPtr;

    if (robot)
    {
        nodeSetPtr = robot->getRobotNodeSet(nodeSetName);

        if (!nodeSetPtr)
        {
            ARMARX_INFO << "RobotNodeSet with name " << nodeSetName << " is not defined" << flush;

        }
    }

    return nodeSetPtr;
}


bool KinematicUnitWidgetController::initGUIComboBox(VirtualRobot::RobotNodeSetPtr robotNodeSet)
{
    ui.nodeListComboBox->clear();

    if (robotNodeSet)
    {
        std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();

        for (unsigned int i = 0; i < rn.size(); i++)
        {
            //            ARMARX_INFO << "adding item to joint combo box" << rn[i]->getName() << flush;
            QString name(rn[i]->getName().c_str());
            ui.nodeListComboBox->addItem(name);
        }
        ui.nodeListComboBox->setCurrentIndex(-1);
        return true;
    }
    return false;
}


bool KinematicUnitWidgetController::initGUIJointListTable(VirtualRobot::RobotNodeSetPtr robotNodeSet)
{
    uint numberOfColumns = 10;

    //dont use clear! It is not required here and somehow causes the tabel to have
    //numberOfColumns additional empty columns and rn.size() additional empty rows.
    //Somehow columncount (rowcount) stay at numberOfColumns (rn.size())
    //ui.tableJointList->clear();

    if (robotNodeSet)
    {
        std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();

        //set dimension of table
        //ui.tableJointList->setColumnWidth(0,110);

        //ui.tableJointList->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
        ui.tableJointList->setRowCount(rn.size());
        ui.tableJointList->setColumnCount(eTabelColumnCount);


        //ui.tableJointList->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

        // set table header
        // if the order is changed dont forget to update the order in the enum JointTabelColumnIndex
        // in theheader file
        QStringList s;
        s << "Joint Name"
          << "Control Mode"
          << "Angle [deg]/Position [mm]"
          << "Velocity [deg/s]/[mm/s]"
          << "Torque [Nm] / PWM"
          << "Current [A]"
          << "Temperature [C]"
          << "Operation"
          << "Error"
          << "Enabled"
          << "Emergency Stop";
        ui.tableJointList->setHorizontalHeaderLabels(s);
        ARMARX_CHECK_EXPRESSION(ui.tableJointList->columnCount() == eTabelColumnCount) << "Current table size: " << ui.tableJointList->columnCount();


        // fill in joint names
        for (unsigned int i = 0; i < rn.size(); i++)
        {
            //         ARMARX_INFO << "adding item to joint table" << rn[i]->getName() << flush;
            QString name(rn[i]->getName().c_str());

            QTableWidgetItem* newItem = new QTableWidgetItem(name);
            ui.tableJointList->setItem(i, eTabelColumnName, newItem);
        }

        // init missing table fields with default values
        for (unsigned int i = 0; i < rn.size(); i++)
        {
            for (unsigned int j = 1; j < numberOfColumns; j++)
            {
                QString state = "--";
                QTableWidgetItem* newItem = new QTableWidgetItem(state);
                ui.tableJointList->setItem(i, j, newItem);
            }
        }

        //hide columns Operation, Error, Enabled and Emergency Stop
        //they will be shown when changes occur
        ui.tableJointList->setColumnHidden(eTabelColumnTemperature, true);
        ui.tableJointList->setColumnHidden(eTabelColumnOperation, true);
        ui.tableJointList->setColumnHidden(eTabelColumnError, true);
        ui.tableJointList->setColumnHidden(eTabelColumnEnabled, true);
        ui.tableJointList->setColumnHidden(eTabelColumnEmergencyStop, true);

        return true;
    }

    return false;
}


void KinematicUnitWidgetController::selectJoint(int i)
{
    std::unique_lock lock(mutexNodeSet);

    if (!robotNodeSet || i < 0 || i >= (int)robotNodeSet->getSize())
    {
        return;
    }

    currentNode = robotNodeSet->getAllRobotNodes()[i];

    /*
    //ui.lcdNumberKinematicUnitJointValue->display(pos*180.0f/(float)M_PI);
    ui.lcdNumberKinematicUnitJointValue->display(posT);
    */
    if (selectedControlMode == ePositionControl)
    {
        setControlModePosition();
    }
    else if (selectedControlMode == eVelocityControl)
    {
        setControlModeVelocity();
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
    }
    else if (selectedControlMode == eTorqueControl)
    {
        setControlModeTorque();
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
    }
}

void KinematicUnitWidgetController::selectJointFromTableWidget(int row, int column)
{
    if (column == eTabelColumnName)
    {
        ui.nodeListComboBox->setCurrentIndex(row);
        //        selectJoint(row);
    }
}

void KinematicUnitWidgetController::sliderValueChanged(int pos)
{
    std::unique_lock lock(mutexNodeSet);

    if (!currentNode)
    {
        return;
    }

    //    float ticks = static_cast<float>(ui.horizontalSliderKinematicUnitPos->maximum() - ui.horizontalSliderKinematicUnitPos->minimum()+1);
    float value = static_cast<float>(ui.horizontalSliderKinematicUnitPos->value());
    //    float perc = (value - static_cast<float>(ui.horizontalSliderKinematicUnitPos->minimum())) / ticks;

    //    NameControlModeMap::const_iterator it;
    //    it = reportedJointControlModes.find(currentNode->getName());
    //    const ControlMode currentControlMode = it->second;
    const ControlMode currentControlMode = selectedControlMode;
    const bool isDeg = ui.checkBoxUseDegree->isChecked();
    const bool isRot = currentNode->isRotationalJoint();

    if (currentControlMode == ePositionControl)
    {
        const auto factor = isRot && ! isDeg ? SLIDER_POS_RAD_MULTIPLIER : SLIDER_POS_DEG_MULTIPLIER;
        float conversionFactor = isRot && isDeg ? 180.0 / M_PI : 1.0f;
        // TODO: Joint limits are not respected
        //float lo = currentNode->getJointLimitLo();
        //float hi = currentNode->getJointLimitHi();
        //float result = lo + (hi-lo)*perc;
        //jointAngles[currentNode->getName()] = (perc - 0.5) * 2 * M_PI;

        NameValueMap jointAngles;

        jointAngles[currentNode->getName()] = value / conversionFactor / factor;
        ui.lcdNumberKinematicUnitJointValue->display(value / factor);
        if (kinematicUnitInterfacePrx)
        {
            try
            {
                kinematicUnitInterfacePrx->setJointAngles(jointAngles);
            }
            catch (...)
            {
            }
            updateModel();
        }
    }
    else if (currentControlMode == eVelocityControl)
    {
        float conversionFactor = isRot ? (isDeg ? 180.0 / M_PI : 100.f) : 1.0f;
        NameValueMap jointVelocities;
        jointVelocities[currentNode->getName()] = value / conversionFactor * static_cast<float>(ui.doubleSpinBoxKinematicUnitPosFactor->value());
        ui.lcdNumberKinematicUnitJointValue->display(value);

        if (kinematicUnitInterfacePrx)
        {
            try
            {
                kinematicUnitInterfacePrx->setJointVelocities(jointVelocities);
            }
            catch (...)
            {
            }
            updateModel();
        }
    }
    else if (currentControlMode == eTorqueControl)
    {
        NameValueMap jointTorques;
        float torqueTargetValue = value / 100.0f * static_cast<float>(ui.doubleSpinBoxKinematicUnitPosFactor->value());
        jointTorques[currentNode->getName()] = torqueTargetValue;
        ui.lcdNumberKinematicUnitJointValue->display(torqueTargetValue);

        if (kinematicUnitInterfacePrx)
        {
            try
            {
                kinematicUnitInterfacePrx->setJointTorques(jointTorques);
            }
            catch (...)
            {
            }
            updateModel();
        }
    }
    else
    {
        ARMARX_INFO << "current ControlModes unknown" << flush;
    }
}



void KinematicUnitWidgetController::updateControlModesTable()
{
    if (!getWidget() || !robotNodeSet)
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        NameControlModeMap::const_iterator it;
        it = reportedJointControlModes.find(rn[i]->getName());
        QString state;

        if (it == reportedJointControlModes.end())
        {
            state = "unknown";
        }
        else
        {
            ControlMode currentMode = it->second;


            switch (currentMode)
            {
                /*case eNoMode:
                    state = "None";
                    break;

                case eUnknownMode:
                    state = "Unknown";
                    break;
                */
                case eDisabled:
                    state = "Disabled";
                    break;

                case eUnknown:
                    state = "Unknown";
                    break;

                case ePositionControl:
                    state = "Position";
                    break;

                case eVelocityControl:
                    state = "Velocity";
                    break;

                case eTorqueControl:
                    state = "Torque";
                    break;


                case ePositionVelocityControl:
                    state = "Position + Velocity";
                    break;

                default:
                    //show the value of the mode so it can be implemented
                    state = QString("<nyi Mode: %1>").arg(static_cast<int>(currentMode));
                    break;
            }
        }

        QTableWidgetItem* newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnControlMode, newItem);
    }
}

void KinematicUnitWidgetController::updateJointStatusesTable()
{
    if (!getWidget() || !robotNodeSet || reportedJointStatuses.empty())
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        NameStatusMap::const_iterator it;
        it = reportedJointStatuses.find(rn[i]->getName());
        JointStatus currentStatus = it->second;

        QString state = translateStatus(currentStatus.operation);
        QTableWidgetItem* newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnOperation, newItem);

        state = translateStatus(currentStatus.error);
        newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnError, newItem);

        state = currentStatus.enabled ? "X" : "-";
        newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnEnabled, newItem);

        state = currentStatus.emergencyStop ? "X" : "-";
        newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnEmergencyStop, newItem);
    }

    //show columns
    ui.tableJointList->setColumnHidden(eTabelColumnOperation, false);
    ui.tableJointList->setColumnHidden(eTabelColumnError, false);
    ui.tableJointList->setColumnHidden(eTabelColumnEnabled, false);
    ui.tableJointList->setColumnHidden(eTabelColumnEmergencyStop, false);
}

QString KinematicUnitWidgetController::translateStatus(OperationStatus status)
{
    switch (status)
    {
        case eOffline:
            return "Offline";

        case eOnline:
            return "Online";

        case eInitialized:
            return "Initialized";

        default:
            return "?";
    }
}

QString KinematicUnitWidgetController::translateStatus(ErrorStatus status)
{
    switch (status)
    {
        case eOk:
            return "Ok";

        case eWarning:
            return "Wr";

        case eError:
            return "Er";

        default:
            return "?";
    }
}

void KinematicUnitWidgetController::updateJointAnglesTable()
{

    float dirty_squaresum = 0;

    std::unique_lock lock(mutexNodeSet);
    if (jointAnglesUpdateFrequency && jointAnglesUpdateFrequency->getValue())
    {

        double deltaT = jointAnglesUpdateFrequency->getValue()->getDouble();
        if (deltaT != 0)
        {
            ui.labelUpdateFreq->setText(QString::number(static_cast<int>(1.0 / deltaT)) + " Hz");
        }
        else
        {
            ui.labelUpdateFreq->setText("- Hz");
        }
    }

    if (!robotNodeSet)
    {
        return;
    }
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();


    for (unsigned int i = 0; i < rn.size(); i++)
    {
        NameValueMap::const_iterator it;
        RobotNodePtr node = rn[i];
        it = reportedJointAngles.find(node->getName());

        if (it == reportedJointAngles.end())
        {
            continue;
        }

        const float currentValue = it->second;
        dirty_squaresum += currentValue * currentValue;

        QModelIndex index = ui.tableJointList->model()->index(i, eTabelColumnAngleProgressbar);
        float conversionFactor = ui.checkBoxUseDegree->isChecked() &&
                                 node->isRotationalJoint() ? 180.0 / M_PI : 1;
        ui.tableJointList->model()->setData(index, (int)(cutJitter(currentValue * conversionFactor) * 100) / 100.0f, eJointAngleRole);
        ui.tableJointList->model()->setData(index, node->getJointLimitHigh() * conversionFactor, eJointHiRole);
        ui.tableJointList->model()->setData(index, node->getJointLimitLow() * conversionFactor, eJointLoRole);
    }

    //update only if values changed
    if ((fabs(dirty_squaresum_old[0] - dirty_squaresum)) > 0.0000005)
    {
        updateModel();
        dirty_squaresum_old[0] = dirty_squaresum;
        //        ARMARX_INFO << "update model" << flush;
    }
}

void KinematicUnitWidgetController::updateJointVelocitiesTable()
{
    if (!getWidget())
    {
        return;
    }

    float dirty_squaresum = 0;
    std::unique_lock lock(mutexNodeSet);
    if (!robotNodeSet)
    {
        return;
    }
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    QTableWidgetItem* newItem;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        NameValueMap::const_iterator it;
        it = reportedJointVelocities.find(rn[i]->getName());

        if (it == reportedJointVelocities.end())
        {
            continue;
        }

        float currentValue = it->second;
        dirty_squaresum += currentValue * currentValue;
        if (ui.checkBoxUseDegree->isChecked() && rn[i]->isRotationalJoint())
        {
            currentValue *= 180.0 / M_PI;
        }
        const QString Text = QString::number(cutJitter(currentValue), 'g', 2);
        newItem = new QTableWidgetItem(Text);
        ui.tableJointList->setItem(i, eTabelColumnVelocity, newItem);
    }

    if ((fabs(dirty_squaresum_old[1] - dirty_squaresum)) > 0.0000005)
    {
        updateModel();
        dirty_squaresum_old[1] = dirty_squaresum;
    }
}

void KinematicUnitWidgetController::updateJointTorquesTable()
{


    std::unique_lock lock(mutexNodeSet);
    if (!getWidget() || !robotNodeSet)
    {
        return;
    }
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    QTableWidgetItem* newItem;
    NameValueMap::const_iterator it;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        it = reportedJointTorques.find(rn[i]->getName());

        if (it == reportedJointTorques.end())
        {
            continue;
        }

        const float currentValue = it->second;
        newItem = new QTableWidgetItem(QString::number(cutJitter(currentValue)));
        ui.tableJointList->setItem(i, eTabelColumnTorque, newItem);
    }
}

void KinematicUnitWidgetController::updateJointCurrentsTable()
{


    std::unique_lock lock(mutexNodeSet);
    if (!getWidget() || !robotNodeSet || jointCurrentHistory.size() == 0)
    {
        return;
    }
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    QTableWidgetItem* newItem;
    NameValueMap reportedJointCurrents = jointCurrentHistory.back().second;
    NameValueMap::const_iterator it;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        it = reportedJointCurrents.find(rn[i]->getName());

        if (it == reportedJointCurrents.end())
        {
            continue;
        }

        const float currentValue = it->second;
        newItem = new QTableWidgetItem(QString::number(cutJitter(currentValue)));
        ui.tableJointList->setItem(i, eTabelColumnCurrent, newItem);
    }

    highlightCriticalValues();
}

void KinematicUnitWidgetController::updateMotorTemperaturesTable()
{


    std::unique_lock lock(mutexNodeSet);
    if (!getWidget() || !robotNodeSet || reportedJointTemperatures.empty())
    {
        return;
    }
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    QTableWidgetItem* newItem;
    NameValueMap::const_iterator it;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        it = reportedJointTemperatures.find(rn[i]->getName());

        if (it == reportedJointTemperatures.end())
        {
            continue;
        }

        const float currentValue = it->second;
        newItem = new QTableWidgetItem(QString::number(cutJitter(currentValue)));
        ui.tableJointList->setItem(i, eTabelColumnTemperature, newItem);
    }
    ui.tableJointList->setColumnHidden(eTabelColumnTemperature, false);

}

void KinematicUnitWidgetController::reportJointAngles(const NameValueMap& jointAngles, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{
    if (!aValueChanged && reportedJointAngles.size() > 0)
    {
        return;
    }


    std::unique_lock lock(mutexNodeSet);
    jointAnglesUpdateFrequency->update(timestamp, new Variant((double)(timestamp - lastJointAngleUpdateTimestamp) * 1e-6));
    lastJointAngleUpdateTimestamp = timestamp;
    mergeMaps(reportedJointAngles, jointAngles);
    if (skipper.checkFrequency(c, 30))
    {
        //        ARMARX_INFO << deactivateSpam(1) << "updating angles";
        emit jointAnglesReported();
    }
    //    else
    //    {
    //        ARMARX_INFO << deactivateSpam(1) << "skipping angles";
    //    }
}

void KinematicUnitWidgetController::reportJointVelocities(const NameValueMap& jointVelocities, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{
    if (!aValueChanged && reportedJointVelocities.size() > 0)
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);
    mergeMaps(reportedJointVelocities, jointVelocities);
    if (skipper.checkFrequency(c))
    {
        emit jointVelocitiesReported();
    }
}

void KinematicUnitWidgetController::reportJointTorques(const NameValueMap& jointTorques, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{
    if (!aValueChanged && reportedJointTorques.size() > 0)
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);
    reportedJointTorques = jointTorques;
    if (skipper.checkFrequency(c))
    {
        emit jointTorquesReported();
    }
}

void KinematicUnitWidgetController::reportJointAccelerations(const NameValueMap& jointAccelerations, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{

}

void KinematicUnitWidgetController::reportControlModeChanged(const NameControlModeMap& jointModes, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{
    //    if(!aValueChanged && reportedJointControlModes.size() > 0)
    //        return;
    std::unique_lock lock(mutexNodeSet);
    mergeMaps(reportedJointControlModes, jointModes);
    if (skipper.checkFrequency(c))
    {
        emit jointControlModesReported();
    }
}

void KinematicUnitWidgetController::reportJointCurrents(const NameValueMap& jointCurrents, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{
    std::unique_lock lock(mutexNodeSet);

    if (aValueChanged && jointCurrents.size() > 0)
    {
        jointCurrentHistory.push_back(std::pair<Ice::Long, NameValueMap>(timestamp, jointCurrents));
    }

    while (jointCurrentHistory.size() > 1 && jointCurrentHistory.back().first - jointCurrentHistory.front().first > historyTime)
    {
        jointCurrentHistory.pop_front();
    }


    if (jointCurrentHistory.size() > 0)
    {
        if (skipper.checkFrequency(c))
        {
            emit jointCurrentsReported();
        }
    }
}

void KinematicUnitWidgetController::reportJointMotorTemperatures(const NameValueMap& jointMotorTemperatures, Ice::Long timestamp, bool aValueChanged,  const Ice::Current& c)
{
    if (!aValueChanged && reportedJointStatuses.size() > 0)
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);
    mergeMaps(this->reportedJointTemperatures, jointMotorTemperatures);
    if (skipper.checkFrequency(c))
    {
        emit jointMotorTemperaturesReported();
    }
}

void KinematicUnitWidgetController::reportJointStatuses(const NameStatusMap& jointStatuses, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{
    if (!aValueChanged && reportedJointStatuses.size() > 0)
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);
    mergeMaps(reportedJointStatuses, jointStatuses);
    if (skipper.checkFrequency(c))
    {
        emit jointStatusesReported();
    }
}


void KinematicUnitWidgetController::updateModel()
{
    //    ARMARX_INFO << "updateModel()" << flush;
    std::unique_lock lock(mutexNodeSet);
    if (!robotNodeSet)
    {
        return;
    }
    robot->setJointValues(reportedJointAngles);
}

void KinematicUnitWidgetController::highlightCriticalValues()
{
    if (!enableValueValidator)
    {
        return;
    }

    std::unique_lock lock(mutexNodeSet);

    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();

    // get standard line colors
    static std::vector<QBrush> standardColors;
    if (standardColors.size() == 0)
    {
        for (unsigned int i = 0; i < rn.size(); i++)
        {
            // all cells of a row have the same color
            standardColors.push_back(ui.tableJointList->item(i, eTabelColumnCurrent)->background());
        }
    }

    // check robot current value of nodes
    for (unsigned int i = 0; i < rn.size(); i++)
    {
        bool isStatic = true;
        float smoothedValue = 0;

        for (auto historyIt = jointCurrentHistory.begin(); historyIt != jointCurrentHistory.end(); historyIt++)
        {
            NameValueMap reportedJointCurrents = historyIt->second;
            NameValueMap::const_iterator it = reportedJointCurrents.find(rn[i]->getName());

            if (it == reportedJointCurrents.end())
            {
                continue;
            }

            const float currentValue = std::fabs(it->second);

            smoothedValue = 0.5f * currentValue + 0.5f * smoothedValue;

            if (currentValue != jointCurrentHistory.begin()->second.find(rn[i]->getName())->second)
            {
                isStatic = false;
            }
        }

        NameStatusMap::const_iterator it;
        it = reportedJointStatuses.find(rn[i]->getName());
        JointStatus currentStatus = it->second;

        if (isStatic)
        {
            if (currentStatus.operation != eOffline)
            {
                // current value is zero, but joint is not offline
                ui.tableJointList->item(i, eTabelColumnCurrent)->setBackground(Qt::yellow);
            }

        }
        else if (smoothedValue > currentValueMax)
        {
            // current value is too high
            ui.tableJointList->item(i, eTabelColumnCurrent)->setBackground(Qt::red);
        }
        else
        {
            // everything seems to work as expected
            ui.tableJointList->item(i, eTabelColumnCurrent)->setBackground(standardColors[i]);
        }
    }
}

void KinematicUnitWidgetController::setMutex3D(RecursiveMutexPtr const& mutex3D)
{
    this->mutex3D = mutex3D;

    if (debugDrawer)
    {
        debugDrawer->setMutex(mutex3D);
    }
}

QPointer<QWidget> KinematicUnitWidgetController::getCustomTitlebarWidget(QWidget* parent)
{
    if (customToolbar)
    {
        customToolbar->setParent(parent);
    }
    else
    {
        customToolbar = new QToolBar(parent);
        customToolbar->addAction("ZeroPosition", this, SLOT(kinematicUnitZeroPosition()));
        customToolbar->addAction("ZeroVelocity", this, SLOT(kinematicUnitZeroVelocity()));
        customToolbar->addAction("ZeroTorque",   this, SLOT(kinematicUnitZeroTorque()));
    }
    return customToolbar.data();
}

float KinematicUnitWidgetController::cutJitter(float value)
{
    return (abs(value) < static_cast<float>(ui.jitterThresholdSpinBox->value())) ? 0 : value;
}

void KinematicUnitWidgetController::timerEvent(QTimerEvent*)
{
    updateJointAnglesTable();
    updateJointVelocitiesTable();
    updateJointTorquesTable();
    updateJointCurrentsTable();
    updateControlModesTable();
    updateJointStatusesTable();
    updateMotorTemperaturesTable();
}



void RangeValueDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() == KinematicUnitWidgetController::eTabelColumnAngleProgressbar)
    {
        float jointValue = index.data(KinematicUnitWidgetController::   eJointAngleRole).toFloat();
        float loDeg = index.data(KinematicUnitWidgetController::eJointLoRole).toFloat();
        float hiDeg = index.data(KinematicUnitWidgetController::eJointHiRole).toFloat();

        if (hiDeg - loDeg <= 0)
        {
            QStyledItemDelegate::paint(painter, option, index);
            return;
        }

        QStyleOptionProgressBar progressBarOption;
        progressBarOption.rect = option.rect;
        progressBarOption.minimum = loDeg;
        progressBarOption.maximum = hiDeg;
        progressBarOption.progress = jointValue;
        progressBarOption.text = QString::number(jointValue);
        progressBarOption.textVisible = true;
        QPalette pal;
        pal.setColor(QPalette::Background, Qt::red);
        progressBarOption.palette = pal;
        QApplication::style()->drawControl(QStyle::CE_ProgressBar,
                                           &progressBarOption, painter);

        //        QProgressBar renderer;
        //        float progressPercentage = (jointValue*180.0f/M_PI-loDeg)/(hiDeg - loDeg);
        //        ARMARX_INFO_S << VAROUT(progressPercentage);
        //        // Customize style using style-sheet..
        //        QColor color((int)(255*progressPercentage), ((int)(255*(1-progressPercentage))), 0);
        //        QString style = renderer.styleSheet();
        //        style += "QProgressBar::chunk { background-color: " + color.name() + "}";
        //        ARMARX_INFO_S << VAROUT(style);
        //        renderer.resize(option.rect.size());
        //        renderer.setMinimum(loDeg);
        //        renderer.setMaximum(hiDeg);
        //        renderer.setValue(jointValue*180.0f);

        //        renderer.setStyleSheet(style);
        //        painter->save();
        //        painter->translate(option.rect.topLeft());
        //        renderer.render(painter);
        //        painter->restore();
    }
    else
    {
        QStyledItemDelegate::paint(painter, option, index);
    }
}

KinematicUnitWidgetController::~KinematicUnitWidgetController() {}

void KinematicUnitWidgetController::on_pushButtonFromJson_clicked()
{
    bool ok;
    const auto text = QInputDialog::getMultiLineText(
                          __widget,
                          tr("JSON Joint values"),
                          tr("Json:"), "{\n}", &ok).toStdString();

    if (!ok || text.empty())
    {
        return;
    }

    NameValueMap jointAngles;
    try
    {
        jointAngles = simox::json::json2NameValueMap(text);
    }
    catch (...)
    {
        ARMARX_ERROR << "invalid json";
    }

    NameControlModeMap jointModes;
    for (const auto& [key, _] : jointAngles)
    {
        jointModes[key] = ePositionControl;
    }

    try
    {
        kinematicUnitInterfacePrx->switchControlMode(jointModes);
        kinematicUnitInterfacePrx->setJointAngles(jointAngles);
    }
    catch (...)
    {
        ARMARX_ERROR << "failed to switch mode or set angles";
    }
}
