/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "HandUnitGuiPlugin.h"
#include "HandUnitConfigDialog.h"
#include <RobotAPI/gui-plugins/HandUnitPlugin/ui_HandUnitConfigDialog.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>

// Qt headers
#include <Qt>
#include <QtGlobal>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QTimer>

#include <cmath>

namespace armarx
{
    HandUnitGuiPlugin::HandUnitGuiPlugin()
    {
        addWidget<HandUnitWidget>();
    }

    HandUnitWidget::HandUnitWidget() :
        handName("NOT SET YET"),
        handUnitProxyName(""),
        setJointAnglesFlag(false)
    {
        // init gui
        ui.setupUi(getWidget());

        jointAngleUpdateTask = new PeriodicTask<HandUnitWidget>(this, &HandUnitWidget::setJointAngles, 50);
        updateInfoTimer = new QTimer(this);
    }


    void HandUnitWidget::onInitComponent()
    {
        usingProxy(handUnitProxyName);
        //usingTopic(handName + "State");
        //ARMARX_WARNING << "Listening on Topic: " << handName + "State";
    }

    void HandUnitWidget::onConnectComponent()
    {
        connectSlots();
        jointAngleUpdateTask->start();
        updateInfoTimer->start(50);

        handUnitProxy = getProxy<HandUnitInterfacePrx>(handUnitProxyName);
        handName = handUnitProxy->getHandName();

        // @@@ In simulation hand is called 'Hand L'/'Hand R'. On 3b Hand is called 'TCP R'
        if (handName != "Hand L" && handName != "Hand R" && handName != "TCP L" && handName != "TCP R")
        {
            //QMessageBox::warning(NULL, "Hand not supported", QString("Hand with name \"") + QString::fromStdString(handName) + " \" is not suppored.");
            ARMARX_WARNING << "Hand with name \"" << handName << "\" is not supported.";
        }

        //ui.labelInfo->setText(QString::fromStdString(handUnitProxyName + " :: " + handName));

        SingleTypeVariantListPtr preshapeStrings = SingleTypeVariantListPtr::dynamicCast(handUnitProxy->getShapeNames());
        QStringList list;
        int preshapeCount = preshapeStrings->getSize();

        for (int i = 0; i < preshapeCount; ++i)
        {
            std::string shape = ((preshapeStrings->getVariant(i))->get<std::string>());
            list << QString::fromStdString(shape);
        }

        ui.comboPreshapes->clear();
        ui.comboPreshapes->addItems(list);
    }

    void HandUnitWidget::onDisconnectComponent()
    {
        jointAngleUpdateTask->stop();
        updateInfoTimer->stop();
    }

    void HandUnitWidget::onExitComponent()
    {
    }

    QPointer<QDialog> HandUnitWidget::getConfigDialog(QWidget* parent)
    {
        if (!dialog)
        {
            dialog = new HandUnitConfigDialog(parent);
            //dialog->ui->editHandUnitName->setText(QString::fromStdString(handUnitProxyName));
            //dialog->ui->editHandName->setText(QString::fromStdString(handName));
        }

        return qobject_cast<HandUnitConfigDialog*>(dialog);
    }

    void HandUnitWidget::configured()
    {
        handUnitProxyName = dialog->proxyFinder->getSelectedProxyName().toStdString();
    }

    void HandUnitWidget::preshapeHand()
    {
        setPreshape(ui.comboPreshapes->currentText().toUtf8().data());
    }

    void HandUnitWidget::setJointAngles()
    {
        if (!handUnitProxy)
        {
            ARMARX_WARNING << "invalid proxy";
            return;
        }

        if (!setJointAnglesFlag)
        {
            return;
        }

        setJointAnglesFlag = false;

        NameValueMap ja;

        if (handName == "Hand L" || handName == "TCP L")
        {
            ja["Hand Palm 2 L"] = ui.horizontalSliderPalm->value() * M_PI / 180;
            ja["Index L J0"] = ui.horizontalSliderIndexJ0->value() * M_PI / 180;
            ja["Index L J1"] = ui.horizontalSliderIndexJ1->value() * M_PI / 180;
            ja["Middle L J0"] = ui.horizontalSliderMiddleJ0->value() * M_PI / 180;
            ja["Middle L J1"] = ui.horizontalSliderMiddleJ1->value() * M_PI / 180;
            ja["Thumb L J0"] = ui.horizontalSliderThumbJ0->value() * M_PI / 180;
            ja["Thumb L J1"] = ui.horizontalSliderThumbJ1->value() * M_PI / 180;
            float rinkyValue = ui.horizontalSliderRinky->value() * M_PI / 180;
            ja["Ring L J0"] = rinkyValue;
            ja["Ring L J1"] = rinkyValue;
            ja["Pinky L J0"] = rinkyValue;
            ja["Pinky L J1"] = rinkyValue;
        }
        else if (handName == "Hand R" || handName == "TCP R")
        {
            ja["Hand Palm 2 R"] = ui.horizontalSliderPalm->value() * M_PI / 180;
            ja["Index R J0"] = ui.horizontalSliderIndexJ0->value() * M_PI / 180;
            ja["Index R J1"] = ui.horizontalSliderIndexJ1->value() * M_PI / 180;
            ja["Middle R J0"] = ui.horizontalSliderMiddleJ0->value() * M_PI / 180;
            ja["Middle R J1"] = ui.horizontalSliderMiddleJ1->value() * M_PI / 180;
            ja["Thumb R J0"] = ui.horizontalSliderThumbJ0->value() * M_PI / 180;
            ja["Thumb R J1"] = ui.horizontalSliderThumbJ1->value() * M_PI / 180;
            float rinkyValue = ui.horizontalSliderRinky->value() * M_PI / 180;
            ja["Ring R J0"] = rinkyValue;
            ja["Ring R J1"] = rinkyValue;
            ja["Pinky R J0"] = rinkyValue;
            ja["Pinky R J1"] = rinkyValue;
        }
        else
        {
            ARMARX_WARNING << "Hand with name \"" << handName << "\" is not supported.";
        }

        handUnitProxy->setJointAngles(ja);
    }

    void HandUnitWidget::requestSetJointAngles()
    {
        setJointAnglesFlag = true;
    }

    void HandUnitWidget::openHand()
    {
        setPreshape("Open");
    }

    void HandUnitWidget::closeHand()
    {
        setPreshape("Close");
    }

    void HandUnitWidget::closeThumb()
    {
        setPreshape("Thumb");
    }

    void HandUnitWidget::relaxHand()
    {
        setPreshape("Relax");
    }

    void HandUnitWidget::updateInfoLabel()
    {
        ui.labelInfo->setText(QString::fromStdString(handUnitProxyName + " :: " + handName + " State: " + handUnitProxy->describeHandState()));
    }

    void HandUnitWidget::setPreshape(std::string preshape)
    {
        ARMARX_INFO << "Setting new hand shape: " << preshape;
        handUnitProxy->setShape(preshape);
    }

    void HandUnitWidget::loadSettings(QSettings* settings)
    {
        handUnitProxyName = settings->value("handUnitProxyName", QString::fromStdString(handUnitProxyName)).toString().toStdString();
        handName = settings->value("handName", QString::fromStdString(handName)).toString().toStdString();
    }

    void HandUnitWidget::saveSettings(QSettings* settings)
    {
        settings->setValue("handUnitProxyName", QString::fromStdString(handUnitProxyName));
        settings->setValue("handName", QString::fromStdString(handName));
    }


    void HandUnitWidget::connectSlots()
    {
        connect(ui.buttonPreshape, SIGNAL(clicked()), this, SLOT(preshapeHand()), Qt::UniqueConnection);
        connect(ui.buttonOpenHand, SIGNAL(clicked()), this, SLOT(openHand()), Qt::UniqueConnection);
        connect(ui.buttonCloseHand, SIGNAL(clicked()), this, SLOT(closeHand()), Qt::UniqueConnection);
        connect(ui.buttonCloseThumb, SIGNAL(clicked()), this, SLOT(closeThumb()), Qt::UniqueConnection);
        connect(ui.buttonRelaxHand, SIGNAL(clicked()), this, SLOT(relaxHand()), Qt::UniqueConnection);
        //connect(ui.comboPreshapes, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(selectPreshape(const QString&)), Qt::UniqueConnection);
        connect(ui.buttonSetJointAngles, SIGNAL(clicked()), this, SLOT(requestSetJointAngles()), Qt::UniqueConnection);
        connect(ui.horizontalSliderIndexJ0, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()), Qt::UniqueConnection);
        connect(ui.horizontalSliderIndexJ1, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()), Qt::UniqueConnection);
        connect(ui.horizontalSliderMiddleJ0, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()), Qt::UniqueConnection);
        connect(ui.horizontalSliderMiddleJ1, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()), Qt::UniqueConnection);
        connect(ui.horizontalSliderRinky, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()), Qt::UniqueConnection);
        connect(ui.horizontalSliderPalm, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()), Qt::UniqueConnection);
        connect(ui.horizontalSliderThumbJ0, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()), Qt::UniqueConnection);
        connect(ui.horizontalSliderThumbJ1, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()), Qt::UniqueConnection);
        connect(updateInfoTimer, SIGNAL(timeout()), this, SLOT(updateInfoLabel()));
    }





    void armarx::HandUnitWidget::reportHandShaped(const std::string& handName, const std::string& handShapeName, const Ice::Current&)
    {
        ARMARX_IMPORTANT << handName << ": " << handShapeName;
    }

    void armarx::HandUnitWidget::reportNewHandShapeName(const std::string& handName, const std::string& handShapeName, const Ice::Current&)
    {
        ARMARX_IMPORTANT << handName << ": " << handShapeName;
    }

    void armarx::HandUnitWidget::reportJointAngles(const armarx::NameValueMap& actualJointAngles, const Ice::Current& c)
    {

    }

    void armarx::HandUnitWidget::reportJointPressures(const armarx::NameValueMap& actualJointPressures, const Ice::Current& c)
    {

    }
}
