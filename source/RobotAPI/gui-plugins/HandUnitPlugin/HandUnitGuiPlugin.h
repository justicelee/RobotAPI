/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::ObjectExaminerGuiPlugin
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @copyright  2012
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#pragma once

/* ArmarX headers */
#include <RobotAPI/gui-plugins/HandUnitPlugin/ui_HandUnitGuiPlugin.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/interface/units/HandUnitInterface.h>

/* Qt headers */
#include <QMainWindow>

#include <string>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>


namespace armarx
{
    class HandUnitConfigDialog;

    /**
      \class HandUnitGuiPlugin
      \brief This plugin offers a widget witch which the HandUnit can be controlled.
      \see HandUnitWidget
      */
    class HandUnitGuiPlugin :
        public ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        HandUnitGuiPlugin();
        QString getPluginName() override
        {
            return "HandUnitGuiPlugin";
        }
    };

    /*!
     * \page RobotAPI-GuiPlugins-HandUnitWidget HandUnitGuiPlugin
     * \brief With this widget the HandUnit can be controlled.
     * \image html HandUnitGUI.png
     * You can either select a preshape from the drop-down-menu on top or set each
     * joint individually.
     * When you add the widget to the Gui, you need to specify the following parameters:
     *
     * Parameter Name   | Example Value     | Required?     | Description
     *  :----------------  | :-------------:   | :-------------- |:--------------------
     * Proxy     | LeftHandUnit   | Yes | The hand unit you want to control.
     */
    class HandUnitWidget :
        public ArmarXComponentWidgetControllerTemplate<HandUnitWidget>,
        public HandUnitListener
    {
        Q_OBJECT
    public:
        HandUnitWidget();
        ~HandUnitWidget() override
        {}

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        // HandUnitListener interface
        void reportHandShaped(const std::string&, const std::string&, const Ice::Current&) override;
        void reportNewHandShapeName(const std::string&, const std::string&, const Ice::Current&) override;


        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "RobotControl.HandUnitGUI";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/hand.svg");
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        void configured() override;

    public slots:

        void preshapeHand();
        void setJointAngles();
        void requestSetJointAngles();
        void openHand();
        void closeHand();
        void closeThumb();
        void relaxHand();
        void updateInfoLabel();

    private:
        void setPreshape(std::string preshape);

    protected:
        void connectSlots();

        Ui::HandUnitGuiPlugin ui;

    private:
        std::string handName;
        //std::string preshapeName;

        std::string handUnitProxyName;
        HandUnitInterfacePrx handUnitProxy;

        //QPointer<QWidget> __widget;
        QPointer<HandUnitConfigDialog> dialog;

        PeriodicTask<HandUnitWidget>::pointer_type jointAngleUpdateTask;
        QTimer* updateInfoTimer;
        bool setJointAnglesFlag;


        // HandUnitListener interface
    public:
        void reportJointAngles(const::armarx::NameValueMap& actualJointAngles, const Ice::Current&) override;
        void reportJointPressures(const::armarx::NameValueMap& actualJointPressures, const Ice::Current&) override;
    };
    using HandUnitGuiPluginPtr = std::shared_ptr<HandUnitWidget>;
}

