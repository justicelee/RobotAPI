/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::GuiHealthClientWidgetController
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/gui-plugins/GuiHealthClient/ui_GuiHealthClientWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/interface/components/RobotHealthInterface.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <QTimer>

namespace armarx
{
    /**
    \page ArmarXGui-GuiPlugins-GuiHealthClient GuiHealthClient
    \brief The GuiHealthClient allows visualizing ...

    \image html GuiHealthClient.png
    The user can

    API Documentation \ref GuiHealthClientWidgetController

    \see GuiHealthClientGuiPlugin
    */

    /**
     * \class GuiHealthClientWidgetController
     * \brief GuiHealthClientWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        GuiHealthClientWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < GuiHealthClientWidgetController >
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit GuiHealthClientWidgetController();

        /**
         * Controller destructor
         */
        virtual ~GuiHealthClientWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "GuiHealthClient";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/heart.svg");
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        void onDisconnectComponent() override;


    protected slots:
        void onConnectComponentQt();
        void onDisconnectComponentQt();
        void healthTimerClb();
        void updateSummaryTimerClb();
        void toggleSendOwnHeartbeats();

    signals:
        /* QT signal declarations */
        void invokeConnectComponentQt();
        void invokeDisconnectComponentQt();

    private:
        /**
         * Widget Form
         */
        Ui::GuiHealthClientWidget widget;
        bool ownHeartbeatsActive = false;

        RobotHealthInterfacePrx robotHealthTopicPrx;
        RobotHealthComponentInterfacePrx robotHealthComponentPrx;
        QTimer* healthTimer;
        QTimer* updateSummaryTimer;
    };
}


