/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::GraspCandidateViewerWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <deque>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/GraspCandidateObserverComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/components/ArViz/Client/Layer.h>
#include <RobotAPI/components/ArViz/Client/elements/Robot.h>

#include <RobotAPI/gui-plugins/GraspCandidateViewer/ui_GraspCandidateViewerWidget.h>

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-GraspCandidateViewer GraspCandidateViewer
    \brief The GraspCandidateViewer allows visualizing ...

    \image html GraspCandidateViewer.png
    The user can

    API Documentation \ref GraspCandidateViewerWidgetController

    \see GraspCandidateViewerGuiPlugin
    */


    /**
     * \class GraspCandidateViewerGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief GraspCandidateViewerGuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * \class GraspCandidateViewerWidgetController
     * \brief GraspCandidateViewerWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        GraspCandidateViewerWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < GraspCandidateViewerWidgetController >,
        virtual public RobotStateComponentPluginUser,
        virtual public ArVizComponentPluginUser,
        virtual public GraspCandidateObserverComponentPluginUser
    {
        Q_OBJECT

        struct entry_gc;
        struct entry_prov;
    public:
        explicit GraspCandidateViewerWidgetController();
        virtual ~GraspCandidateViewerWidgetController();

        static QString GetWidgetName()
        {
            return "Grasping.GraspCandidateViewer";
        }

        void onInitComponent()       override {}
        void onConnectComponent()    override
        {
            _robot = addRobot("state robot", VirtualRobot::RobotIO::eStructure);
        }
        void onDisconnectComponent() override {}
        void onExitComponent()       override {}
    public:
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;
    public slots:
        void update_gc();
        void selected_item_changed();
        void check_state_changed();

    private:
        void show_entry(entry_gc* e);
    private:

        struct entry_prov
        {
            QTreeWidgetItem*      item          = nullptr;
            std::string           provider_name;
            std::deque<entry_gc>  candidates;
        };

        struct entry_gc
        {
            QTreeWidgetItem*            item     = nullptr;
            grasping::GraspCandidatePtr gc       = nullptr;
            entry_prov*                 provider = nullptr;
            int                         idx      = -1;
            bool                        show     = false;

            std::string name() const
            {
                return provider->provider_name + "_" + std::to_string(idx + 1);
            }
        };

        mutable std::recursive_mutex          _mutex;
        QPointer<SimpleConfigDialog>          _dialog;
        Ui::GraspCandidateViewerWidget        _ui;
        VirtualRobot::RobotPtr                _robot;
        std::map<std::string, entry_prov>     _providers;
        std::map<QTreeWidgetItem*, entry_gc*> _tree_item_to_gc;
    };
}


