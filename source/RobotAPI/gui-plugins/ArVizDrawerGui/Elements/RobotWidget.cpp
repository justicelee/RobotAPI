#include "RobotWidget.h"

namespace armarx
{
    void RobotWidget::addTo(viz::Layer& layer) const
    {
        const auto adr = reinterpret_cast<std::intptr_t>(this);
        layer.add(viz::Robot("Robot_" + std::to_string(adr))
                  .file(_ui.lineEditProject->text().toStdString(),
                        _ui.comboBoxFile->currentText().toStdString())
                  .position(_ui.doubleSpinBoxTX->value(),
                            _ui.doubleSpinBoxTY->value(),
                            _ui.doubleSpinBoxTZ->value())
                  .orientation(_ui.doubleSpinBoxRX->value(),
                               _ui.doubleSpinBoxRY->value(),
                               _ui.doubleSpinBoxRZ->value()));
    }
}
