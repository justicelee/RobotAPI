#pragma once


#include <QDialog>

#include <ArmarXCore/core/ManagedIceObject.h>


#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

namespace Ui
{
    class ViewSelectionConfigDialog;
}

class ViewSelectionConfigDialog : public QDialog,
    virtual public armarx::ManagedIceObject
{
    Q_OBJECT

public:
    explicit ViewSelectionConfigDialog(QWidget* parent = 0);
    ~ViewSelectionConfigDialog() override;

    std::string getViewSelectionName()
    {
        return viewSelectionProxyFinder->getSelectedProxyName().toStdString();
    }

signals:

public slots:
    void verifyConfig();

    // ManagedIceObject interface
protected:
    void onInitComponent() override;
    void onConnectComponent() override;
    void onExitComponent() override;
    std::string getDefaultName() const override;

private:

    Ui::ViewSelectionConfigDialog* ui;

    armarx::IceProxyFinderBase* viewSelectionProxyFinder;

    std::string uuid;

};

