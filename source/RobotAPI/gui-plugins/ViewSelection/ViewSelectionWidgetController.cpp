/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::ViewSelectionWidgetController
 * \author     Markus Grotz ( markus dot grotz at kit dot edu )
 * \date       2016
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ViewSelectionWidgetController.h"

#include <QListWidgetItem>
#include <string>

using namespace armarx;

ViewSelectionWidgetController::ViewSelectionWidgetController()
{
    widget.setupUi(getWidget());

    connect(widget.pushButton, SIGNAL(clicked()), this, SLOT(triggerSaliencyMapVisualization()));

    connect(widget.pushButton_2, SIGNAL(clicked()), this, SLOT(clearSaliencyMapVisualization()));

    connect(widget.pushButton_3, SIGNAL(clicked()), this, SLOT(updateSaliencyMapNames()));

    connect(widget.checkBox, SIGNAL(toggled(bool)), this, SLOT(toggleViewSelection(bool)));

}


ViewSelectionWidgetController::~ViewSelectionWidgetController()
{

}



QPointer<QDialog> ViewSelectionWidgetController::getConfigDialog(QWidget* parent)
{
    if (!configDialog)
    {
        configDialog = new ViewSelectionConfigDialog(parent);
    }

    return qobject_cast<ViewSelectionConfigDialog*>(configDialog);
}


void ViewSelectionWidgetController::loadSettings(QSettings* settings)
{
    viewSelectionName = settings->value("viewSelectionName", "").toString().toStdString();
}

void ViewSelectionWidgetController::saveSettings(QSettings* settings)
{
    viewSelectionName = settings->value("viewSelectionName", QString::fromStdString(viewSelectionName)).toString().toStdString();
}


void ViewSelectionWidgetController::onInitComponent()
{
    usingProxy(viewSelectionName);
    usingTopic(viewSelectionName + "Observer");

}


void ViewSelectionWidgetController::onConnectComponent()
{
    viewSelection = getProxy<ViewSelectionInterfacePrx>(viewSelectionName);

    widget.checkBox->setChecked(viewSelection->isEnabledAutomaticViewSelection());
}


void armarx::ViewSelectionWidgetController::onActivateAutomaticViewSelection(const Ice::Current&)
{
    widget.checkBox->setChecked(true);
}

void armarx::ViewSelectionWidgetController::onDeactivateAutomaticViewSelection(const Ice::Current&)
{
    widget.checkBox->setChecked(false);
}

void armarx::ViewSelectionWidgetController::nextViewTarget(Ice::Long, const Ice::Current&)
{
}

void ViewSelectionWidgetController::toggleViewSelection(bool isEnabled)
{
    ARMARX_LOG << "toggling view selection " << isEnabled;

    if (isEnabled)
    {
        viewSelection->activateAutomaticViewSelection();
    }
    else
    {
        viewSelection->deactivateAutomaticViewSelection();
    }
}

void ViewSelectionWidgetController::triggerSaliencyMapVisualization()
{
    std::vector<std::string> names;

    for (int r = 0; r < widget.listWidget->count(); r++)
    {
        QListWidgetItem* item = widget.listWidget->item(r);

        if (item->checkState() == Qt::Checked)
        {
            names.push_back(item->text().toStdString());
        }

    }
    viewSelection->drawSaliencySphere(names);

}

void ViewSelectionWidgetController::updateSaliencyMapNames()
{
    std::vector<std::string> names = viewSelection->getSaliencyMapNames();

    widget.listWidget->clear();

    for (std::string name : names)
    {
        QListWidgetItem* item = new QListWidgetItem(QString::fromStdString(name),  widget.listWidget);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Unchecked);
    }
}


void ViewSelectionWidgetController::clearSaliencyMapVisualization()
{
    viewSelection->clearSaliencySphere();
}

void armarx::ViewSelectionWidgetController::configured()
{
    viewSelectionName = configDialog->getViewSelectionName();
}
