/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::ViewSelectionWidgetController
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/gui-plugins/ViewSelection/ui_ViewSelectionWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/interface/components/ViewSelectionInterface.h>

#include "ViewSelectionConfigDialog.h"

namespace armarx
{

    //class ViewSelectionConfigDialog;


    /**
    \page RobotAPI-GuiPlugins-ViewSelection ViewSelection
    \brief The ViewSelection allows visualizing ...

    \image html ViewSelection.png
    The user can

    API Documentation \ref ViewSelectionWidgetController

    \see ViewSelectionGuiPlugin
    */

    /**
     * \class ViewSelectionWidgetController
     * \brief ViewSelectionWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ViewSelectionWidgetController:
        public ArmarXComponentWidgetControllerTemplate<ViewSelectionWidgetController>,
        public armarx::ViewSelectionObserver
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit ViewSelectionWidgetController();

        /**
         * Controller destructor
         */
        ~ViewSelectionWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        void configured() override;

        void onActivateAutomaticViewSelection(const Ice::Current& c = Ice::emptyCurrent) override;
        void onDeactivateAutomaticViewSelection(const Ice::Current& c = Ice::emptyCurrent) override;
        void nextViewTarget(Ice::Long, const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "RobotControl.ViewSelection";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;


        QPointer<QDialog> getConfigDialog(QWidget* parent) override;

    public slots:

        void triggerSaliencyMapVisualization();
        void clearSaliencyMapVisualization();
        void updateSaliencyMapNames();
        void toggleViewSelection(bool enabled);
        /* QT slot declarations */

    signals:
        /* QT signal declarations */

    private:
        /**
         * Widget Form
         */
        Ui::ViewSelectionWidget widget;

        QPointer<ViewSelectionConfigDialog> configDialog;

        ViewSelectionInterfacePrx viewSelection;

        std::string viewSelectionName;

    };
}

