armarx_set_target("ViewSelectionGuiPlugin")

set(SOURCES ViewSelectionGuiPlugin.cpp ViewSelectionWidgetController.cpp ViewSelectionConfigDialog.cpp)
set(HEADERS ViewSelectionGuiPlugin.h   ViewSelectionWidgetController.h   ViewSelectionConfigDialog.h)
set(GUI_MOC_HDRS ViewSelectionGuiPlugin.h ViewSelectionWidgetController.h ViewSelectionConfigDialog.h)
set(GUI_UIS ViewSelectionWidget.ui ViewSelectionConfigDialog.ui)

# Add more libraries you depend on here, e.g. ${QT_LIBRARIES}.
set(COMPONENT_LIBS RobotAPIInterfaces)

if(ArmarXGui_FOUND)
	armarx_gui_library(ViewSelectionGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
