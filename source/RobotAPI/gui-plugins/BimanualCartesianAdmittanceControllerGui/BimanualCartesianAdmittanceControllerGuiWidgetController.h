/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::BimanualCartesianAdmittanceControllerGuiWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/libraries/NJointControllerGuiPluginUtility/NJointControllerGuiPluginBase.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/SpinBoxToVector.h>
#include <RobotAPI/interface/units/RobotUnit/NJointBimanualCartesianAdmittanceController.h>
#include <RobotAPI/gui-plugins/BimanualCartesianAdmittanceControllerGui/ui_BimanualCartesianAdmittanceControllerGuiWidget.h>

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-BimanualCartesianAdmittanceControllerGui BimanualCartesianAdmittanceControllerGui
    \brief The BimanualCartesianAdmittanceControllerGui allows visualizing ...

    \image html BimanualCartesianAdmittanceControllerGui.png
    The user can

    API Documentation \ref BimanualCartesianAdmittanceControllerGuiWidgetController

    \see BimanualCartesianAdmittanceControllerGuiGuiPlugin
    */

    /**
     * \class BimanualCartesianAdmittanceControllerGuiWidgetController
     * \brief BimanualCartesianAdmittanceControllerGuiWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        BimanualCartesianAdmittanceControllerGuiWidgetController:
        public NJointControllerGuiPluginBase <
        BimanualCartesianAdmittanceControllerGuiWidgetController,
        NJointBimanualCartesianAdmittanceControllerInterfacePrx
        >
    {
        Q_OBJECT

    public:
        explicit BimanualCartesianAdmittanceControllerGuiWidgetController();

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "RobotControl.NJointControllers.BimanualCartesianAdmittanceController";
        }

    private slots:
        void on_pushButtonReadCurrentPose_clicked();
        void on_pushButtonTargAdd_clicked();
        void on_pushButtonCfgSendDefaultPose_clicked();
        void on_pushButtonCfgSendNullspace_clicked();
        void on_pushButtonCfgSendImpedance_clicked();
        void on_pushButtonCfgSendAdmittance_clicked();
        void on_pushButtonCfgSendForce_clicked();
        void on_pushButtonCfgSendAll_clicked();
        void on_pushButtonTargSend_clicked();

    private:
        void setupGuiAfterConnect() override;

        std::array<Ice::FloatSeq, 2> readDesiredJointCFG() const;
        detail::NJBmanCartAdmCtrl::Nullspace readNullspaceCFG() const;
        std::array<detail::NJBmanCartAdmCtrl::Impedance, 2> readImpedanceCFG() const;
        std::array<detail::NJBmanCartAdmCtrl::Force, 2> readForceCFG() const;
        detail::NJBmanCartAdmCtrl::Admittance readAdmittanceCFG() const;
        NJointControllerConfigPtr readFullCFG() const override;
        std::array<Eigen::Vector3f, 2> readPosTarg() const;
        std::array<Eigen::Vector3f, 2> readVelTarg() const;
    private:
        Ui::BimanualCartesianAdmittanceControllerGuiWidget  _ui;
        SpinBoxToVector<QDoubleSpinBox>                     _desiredJointValuesLeft;
        SpinBoxToVector<QDoubleSpinBox>                     _desiredJointValuesRight;
    };
}
