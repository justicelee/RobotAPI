/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::CartesianImpedanceControllerWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/libraries/NJointControllerGuiPluginUtility/NJointControllerGuiPluginBase.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/SpinBoxToVector.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/SpinBoxToPose.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <RobotAPI/libraries/RobotAPINJointControllerWidgets/CartesianImpedanceControllerConfigWidget.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <RobotAPI/gui-plugins/CartesianImpedanceController/ui_CartesianImpedanceControllerWidget.h>

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-CartesianImpedanceController CartesianImpedanceController
    \brief The CartesianImpedanceController allows visualizing ...

    \image html CartesianImpedanceController.png
    The user can

    API Documentation \ref CartesianImpedanceControllerWidgetController

    \see CartesianImpedanceControllerGuiPlugin
    */

    /**
     * \class CartesianImpedanceControllerWidgetController
     * \brief CartesianImpedanceControllerWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        CartesianImpedanceControllerWidgetController:
        public NJointControllerGuiPluginBase <
        CartesianImpedanceControllerWidgetController,
        NJointTaskSpaceImpedanceControlInterfacePrx
        >,
        virtual public ArVizComponentPluginUser
    {
        Q_OBJECT

    public:
        explicit CartesianImpedanceControllerWidgetController();
        ~CartesianImpedanceControllerWidgetController();

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "RobotControl.NJointControllers.CartesianImpedanceControl";
        }
        void setupGuiAfterConnect() override;
        void onConnectComponent() override;

    protected:
        void timerEvent(QTimerEvent* event) override;
        void visuHandAtPose(const Eigen::Matrix4f& tcpInRoot = Eigen::Matrix4f::Identity());

    private slots:
        void on_comboBoxRNS_currentIndexChanged(const QString& arg1);
        void on_pushButtonTargGet_clicked();
        void on_pushButtonTargAdd_clicked();
        void on_pushButtonTargSet_clicked();
        void on_radioButtonSelect_clicked();
        void createController() override;
        void deleteController() override;

    private:
        NJointControllerConfigPtr readFullCFG() const override;
        std::tuple<Eigen::Vector3f, Eigen::Quaternionf> readTargetCFG() const;

    private:
        Ui::CartesianImpedanceControllerWidget  _ui;
        VirtualRobot::RobotNodeSetPtr           _rns;
        SpinBoxToPose<QDoubleSpinBox>           _targ;
        CartesianImpedanceControllerConfigWidget* _settings;

        RobotNameHelper::Arm _left;
        RobotNameHelper::Arm _right;
        std::string _visuHandRobotFile;
        std::string _visuHandRobotProject;
        Eigen::Matrix4f _tcpToBase;
        int _timer{0};
    };
}


