/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QWidget>
#include <QMutex>
#include <eigen3/Eigen/Dense>
#include <valarray>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>


using Eigen::MatrixXf;

namespace Ui
{
    class MatrixDatafieldDisplayWidget;
}

namespace armarx
{
    class MatrixDatafieldDisplayWidget : public QWidget
    {
        Q_OBJECT

    signals:
        void doUpdate();

    private slots:
        void updateRequested();

    public:
        explicit MatrixDatafieldDisplayWidget(DatafieldRefBasePtr matrixDatafield, ObserverInterfacePrx observer, QWidget* parent = 0);
        ~MatrixDatafieldDisplayWidget() override;
        void paintEvent(QPaintEvent*) override;


        void setRange(float min, float max)
        {
            this->min = min;
            this->max = max;
        }
        void enableOffsetFilter(bool enabled);
        QColor getColor(float value, float min, float max);

        void invokeUpdate()
        {
            emit doUpdate();
        }
        void setInfoOverlay(QString infoOverlay)
        {
            mtx.lock();
            this->infoOverlay = infoOverlay;
            mtx.unlock();
        }

    private:
        void drawMatrix(const QRect& target, QPainter& painter);
        void drawPercentiles(const QRect& target, QPainter& painter);

    private:
        Ui::MatrixDatafieldDisplayWidget* ui;
        MatrixXf data;
        std::vector<float> percentiles;
        float min, max;
        std::valarray<QColor> colors;
        QMutex mtx;
        QString infoOverlay;
        DatafieldRefPtr matrixDatafield;
        DatafieldRefPtr matrixDatafieldOffsetFiltered;
        DatafieldRefPtr percentilesDatafield;
        ObserverInterfacePrx observer;
    };
}

