/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::DebugDrawerViewerWidgetController
 * \author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * \date       2016
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "DebugDrawerViewerWidgetController.h"

#include <ArmarXCore/core/ArmarXManager.h>

#include <QTimer>

#include <boost/algorithm/string/join.hpp>

#include <string>


namespace armarx
{
    DebugDrawerViewerWidgetController::DebugDrawerViewerWidgetController()
    {
        rootVisu = nullptr;
        widget.setupUi(getWidget());

        QTimer* timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(updateComboClearLayer()));
        timer->start(100);
    }


    void DebugDrawerViewerWidgetController::loadSettings(QSettings* settings)
    {
        (void) settings;  // unused
    }

    void DebugDrawerViewerWidgetController::saveSettings(QSettings* settings)
    {
        (void) settings;  // unused
    }


    void DebugDrawerViewerWidgetController::onInitComponent()
    {
        rootVisu = new SoSeparator;
        rootVisu->ref();


        enableMainWidgetAsync(false);
        connect(widget.btnClearAll, SIGNAL(clicked()), this, SLOT(on_btnClearAll_clicked()), Qt::UniqueConnection);
        connect(widget.btnClearLayer, SIGNAL(clicked()), this, SLOT(on_btnClearLayer_clicked()), Qt::UniqueConnection);
    }


    void DebugDrawerViewerWidgetController::onConnectComponent()
    {
        // create the debugdrawer component
        std::string debugDrawerComponentName = "GUIDebugDrawer_" + getName();
        ARMARX_INFO << "Creating component " << debugDrawerComponentName;
        debugDrawer = Component::create<DebugDrawerComponent>(getIceProperties(), debugDrawerComponentName);

        if (mutex3D)
        {
            //ARMARX_IMPORTANT << "mutex3d:" << mutex3D.get();
            debugDrawer->setMutex(mutex3D);
        }
        else
        {
            ARMARX_ERROR << " No 3d mutex available...";
        }

        ArmarXManagerPtr m = getArmarXManager();
        m->addObject(debugDrawer, false);

        {
            std::unique_lock lock(*mutex3D);
            rootVisu->addChild(debugDrawer->getVisualization());
        }
        enableMainWidgetAsync(true);
    }

    void armarx::DebugDrawerViewerWidgetController::onExitComponent()
    {
        if (rootVisu)
        {
            rootVisu->removeAllChildren();
            rootVisu->unref();
            rootVisu = nullptr;
        }
    }


    SoNode* DebugDrawerViewerWidgetController::getScene()
    {
        return rootVisu;
    }

    void armarx::DebugDrawerViewerWidgetController::on_btnClearAll_clicked()
    {
        if (debugDrawer)
        {
            ARMARX_INFO << "Clearing all visualization layers";
            debugDrawer->clearAll();
        }
    }

    void DebugDrawerViewerWidgetController::on_btnClearLayer_clicked()
    {
        if (debugDrawer)
        {
            int index = widget.comboClearLayer->currentIndex();
            std::string layerName = widget.comboClearLayer->itemData(index).toString().toStdString();

            if (!layerName.empty())
            {
                ARMARX_INFO << "Clearing layer: '" << layerName << "'";
                debugDrawer->clearLayer(layerName);
            }
        }
    }


    void DebugDrawerViewerWidgetController::updateComboClearLayer()
    {
        QComboBox* combo = widget.comboClearLayer;

        auto setItalic = [combo](bool italic)
        {
            QFont font = combo->font();
            font.setItalic(italic);
            combo->setFont(font);
        };

        auto disableButton = [combo, this, &setItalic](const std::string & hint)
        {
            QString itemText(hint.c_str());
            QString itemData("");
            setItalic(true);

            if (combo->count() != 1)
            {
                combo->clear();
                combo->insertItem(0, itemText, itemData);
            }
            else
            {
                combo->setItemText(0, itemText);
                combo->setItemData(0, itemData);
            }

            this->widget.btnClearLayer->setEnabled(false);
        };

        if (!debugDrawer)
        {
            disableButton("not connected");
            return;
        }

        // fetch layer information
        LayerInformationSequence layers = debugDrawer->layerInformation();

        if (layers.empty())
        {
            disableButton("no layers");
            return;
        }
        else
        {
            setItalic(false);
            this->widget.btnClearLayer->setEnabled(true);
        }

        // sort the layers by name
        std::sort(layers.begin(), layers.end(), [](const LayerInformation & lhs, const LayerInformation & rhs)
        {
            // compare case insensitively
            for (std::size_t i = 0; i < lhs.layerName.size() && i < lhs.layerName.size(); ++i)
            {
                auto lhsLow = std::tolower(lhs.layerName[i]);
                auto rhsLow = std::tolower(rhs.layerName[i]);
                if (lhsLow < rhsLow)
                {
                    return true;
                }
                else if (lhsLow > rhsLow)
                {
                    return false;
                }
            }
            // if one is a prefix of the other, the shorter one "smaller"
            return lhs.layerName.size() < rhs.layerName.size();
        });


        const int numLayers = static_cast<int>(layers.size());

        for (int i = 0; i < numLayers; ++i)
        {
            const LayerInformation& layer = layers[static_cast<std::size_t>(i)];

            QString layerName(layer.layerName.c_str());

            if (i < combo->count())  // in range
            {
                QString itemData = combo->itemData(i).toString();

                // remove deleted layers
                while (itemData.size() != 0 && itemData < layerName)
                {
                    // item layer is smaller than next layer
                    // => item layer was deleted
                    combo->removeItem(i);
                    itemData = i < combo->count() ? combo->itemData(i).toString() : "";
                }

                // update existing layer
                if (itemData == layerName)
                {
                    combo->setItemText(i, makeLayerItemText(layer));
                }
                else // (itemData > layerName)
                {
                    // item layer is further down than current layer
                    // => insert current layer here
                    combo->insertItem(i, makeLayerItemText(layer), layerName);
                }
            }
            else  // out of range
            {
                combo->insertItem(i, makeLayerItemText(layer), layerName);
            }

            // check invariant
            ARMARX_CHECK_EQUAL(combo->itemData(i).toString(), layerName);
        }

        // remove excessive items
        while (combo->count() > numLayers)
        {
            combo->removeItem(combo->count() - 1);
        }
    }



    QString DebugDrawerViewerWidgetController::makeLayerItemText(const LayerInformation& layer)
    {
        std::vector<std::string> annotations;
        if (layer.elementCount == 0)
        {
            annotations.push_back("empty");
        }
        else
        {
            annotations.push_back(std::to_string(layer.elementCount));
        }
        if (!layer.visible)
        {
            annotations.push_back("hidden");
        }

        if (annotations.empty())
        {
            return { layer.layerName.c_str() };
        }
        else
        {
            std::stringstream itemText;
            itemText << layer.layerName
                     << " (" << boost::algorithm::join(annotations, ", ") << ")";
            return { itemText.str().c_str() };
        }
    }
}


