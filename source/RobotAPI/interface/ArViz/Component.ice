#pragma once

#include <RobotAPI/interface/ArViz/Elements.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
module viz
{
module data
{

sequence <Element> ElementSeq;

enum LayerAction
{
    Layer_CREATE_OR_UPDATE,
    Layer_DELETE,
};

struct LayerUpdate
{
    string component;
    string name;
    LayerAction action = Layer_CREATE_OR_UPDATE;
    ElementSeq elements;
};

sequence <LayerUpdate> LayerUpdateSeq;

struct TimestampedLayerUpdate
{
    LayerUpdate update;
    long revision = 0;
    long timestampInMicroseconds = 0;
};

struct LayerUpdates
{
    LayerUpdateSeq updates;
    long revision = 0;
};

struct RecordingHeader
{
    string prefix;
    string comment;
    long firstTimestampInMicroSeconds = 0;
    long lastTimestampInMicroSeconds = 0;
    long firstRevision = 0;
    long lastRevision = 0;
};

struct RecordingBatchHeader
{
    long index = 0;
    long firstTimestampInMicroSeconds = 0;
    long lastTimestampInMicroSeconds = 0;
    long firstRevision = 0;
    long lastRevision = 0;
};

sequence<RecordingBatchHeader> RecordingBatchHeaderSeq;

struct Recording
{
    string id;
    string comment;
    long firstTimestampInMicroSeconds = 0;
    long lastTimestampInMicroSeconds = 0;
    long firstRevision = 0;
    long lastRevision = 0;
    RecordingBatchHeaderSeq batchHeaders;
};

sequence<TimestampedLayerUpdate> TimestampedLayerUpdateSeq;

struct RecordingBatch
{
    RecordingBatchHeader header;
    TimestampedLayerUpdateSeq initialState; // Keyframe with complete state
    TimestampedLayerUpdateSeq updates; // Incremental updates on keyframe
};

sequence<Recording> RecordingSeq;

};

interface StorageInterface
{
    data::LayerUpdates pullUpdatesSince(long revision);

    string startRecording(string prefix);

    void stopRecording();

    data::RecordingSeq getAllRecordings();

    data::RecordingBatch getRecordingBatch(string recordingID, long batchIndex);
};

interface Topic
{
    void updateLayers(data::LayerUpdateSeq updates);
};

interface StorageAndTopicInterface extends StorageInterface, Topic
{
};


};
};
