#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/BasicVectorTypes.ice>
#include <RobotAPI/interface/core/PoseBase.ice>


module armarx
{
module viz
{
module data
{
    struct GlobalPose
    {
        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;
        float qw = 1.0f;
        float qx = 0.0f;
        float qy = 0.0f;
        float qz = 0.0f;
    };

    struct Color
    {
        byte a = 255;
        byte r = 100;
        byte g = 100;
        byte b = 100;
    };

    module ElementFlags
    {
        const int NONE = 0;
        const int OVERRIDE_MATERIAL = 1;
        const int HIDDEN = 2;
    };

    class Element
    {
        string id;
        GlobalPose pose;
        float scale = 1.0f;
        Color color;
        int flags = 0;
    };

    class ElementPose extends Element
    {
    };

    class ElementLine extends Element
    {
        Vector3f from;
        Vector3f to;

        float lineWidth = 10.0f;
    };

    class ElementBox extends Element
    {
        Vector3f size;
    };

    class ElementSphere extends Element
    {
        float radius = 10.0f;
    };

    class ElementEllipsoid extends Element
    {
        Vector3f axisLengths;
        Vector3f curvature;
    };

    class ElementCylinder extends Element
    {
        float height = 10.0f;
        float radius = 10.0f;
    };

    class ElementCylindroid extends Element
    {
        Vector2f axisLengths;
        Vector2f curvature;
        float height;
    };

    class ElementText extends Element
    {
        string text;
    };

    class ElementPolygon extends Element
    {
        Vector3fSeq points;

        Color lineColor;
        float lineWidth = 0.0f;
    };

    class ElementArrow extends Element
    {
        float length = 100.0f;
        float width = 10.0f;
    };

    class ElementArrowCircle extends Element
    {
        float radius = 100.0f;
        float completion = 1.0f;
        float width = 10.0f;
    };

    struct ColoredPoint
    {
        float x;
        float y;
        float z;
        Color color;
    };

    sequence<ColoredPoint> ColoredPointList;

    class ElementPointCloud extends Element
    {
        ColoredPointList points;
        float transparency = 0.0f;
        float pointSizeInPixels = 1.0f;
    };

    sequence<Color> ColorSeq;

    struct Face
    {
        int v0 = 0;
        int v1 = 0;
        int v2 = 0;
        int c0 = 0;
        int c1 = 0;
        int c2 = 0;
    };

    sequence<Face> FaceSeq;

    class ElementMesh extends Element
    {
        Vector3fSeq vertices;
        ColorSeq colors;
        FaceSeq faces;
    };

    module ModelDrawStyle
    {
        const int ORIGINAL  = 0;
        const int COLLISION = 1;
        const int OVERRIDE_COLOR = 2;
    };

    class ElementRobot extends Element
    {
        string project;
        string filename;
        int drawStyle = ModelDrawStyle::ORIGINAL;

        StringFloatDictionary jointValues;
    };

    class ElementObject extends Element
    {
        string project;
        string filename;
        int drawStyle = ModelDrawStyle::ORIGINAL;
    };

};
};
};
