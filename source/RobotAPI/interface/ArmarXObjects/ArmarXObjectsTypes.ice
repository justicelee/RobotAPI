/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotAPI
* @author     Rainer Kartmann
* @copyright  2020 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

module armarx
{
    module data
    {
        struct ObjectID
        {
            /// The dataset name in ArmarXObjects, e.g. "KIT", "YCB", "SecondHands", ...
            string dataset;
            /// The class name in ArmarXObjects, e.g. "Amicelli", "001_chips_can", ...
            string className;
            /// An optional instance name, chosen by the provider.
            string instanceName;
        };
        sequence<ObjectID> ObjectIDSeq;
    };
};

