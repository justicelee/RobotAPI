/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/interface/events/SimulatorResetEvent.ice>
#include <RobotAPI/interface/units/KinematicUnitInterface.ice>
#include <RobotAPI/interface/units/PlatformUnitInterface.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <ArmarXCore/interface/observers/Timestamp.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/serialization/Eigen.ice>
#include <Ice/BuiltinSequences.ice>

module armarx
{
    /**
     * Available VirtualRobot joint types.
     */
    enum JointType
    {
        ePrismatic,
        eFixed,
        eRevolute
    };

    /**
     * Distributed reference counting interface for the SharedRobotInterace
     * instances.
     */
    interface SharedObjectInterface
    {
        void ref();
        void unref();
        void destroy();
    };

    sequence<string> NameList;

    /**
     * The data stored in a VirtualRobot::RobotNodeSet but accessable over Ice.
     */
    class RobotNodeSetInfo
    {
        NameList names;
        string name;
        string tcpName;
        string rootName;
    };

    struct RobotStateConfig
    {
        double timestamp;
        FramedPoseBase globalPose;
        NameValueMap jointMap;
    };

    /**
     * The SharedRobotNodeInterface provides access to a limited amount of
     * VirtualRobot::RobotNode methods over the Ice network.
     */
    interface SharedRobotNodeInterface extends SharedObjectInterface
    {

        ["cpp:const"] idempotent
        float getJointValue();
        ["cpp:const"] idempotent
        string getName();

        ["cpp:const"] idempotent
        PoseBase getLocalTransformation();

        ["cpp:const"] idempotent
        FramedPoseBase getGlobalPose();

        ["cpp:const"] idempotent
        FramedPoseBase getPoseInRootFrame();

        ["cpp:const"] idempotent
        JointType getType();
        ["cpp:const"] idempotent
        Vector3Base getJointTranslationDirection();
        ["cpp:const"] idempotent
        Vector3Base getJointRotationAxis();

        ["cpp:const"] idempotent
        bool hasChild(string name, bool recursive);
        ["cpp:const"] idempotent
        string getParent();
        ["cpp:const"] idempotent
        NameList getChildren();
        ["cpp:const"] idempotent
        NameList getAllParents(string name);

        ["cpp:const"] idempotent
        float getJointValueOffest();
        ["cpp:const"] idempotent
        float getJointLimitHigh();
        ["cpp:const"] idempotent
        float getJointLimitLow();

        ["cpp:const"] idempotent
        Vector3Base getCoM();

        ["cpp:const"] idempotent
        Ice::FloatSeq getInertia();

        ["cpp:const"] idempotent
        float getMass();

    };


    interface RobotStateComponentInterface;

    /**
     * The SharedRobotInterface provides access to a limited amount of
     * VirtualRobot::Robot methods over the Ice network.
     */
    interface SharedRobotInterface extends SharedObjectInterface
    {
        /**
          * @return returns the RobotStateComponent this robot belongs to
          */
        ["cpp:const"] idempotent
        RobotStateComponentInterface* getRobotStateComponent();

        SharedRobotNodeInterface* getRobotNode(string name);
        SharedRobotNodeInterface* getRootNode();
        bool hasRobotNode(string name);
        NameList getRobotNodes();
        /**
          * @return The timestamp of the last joint value update (not the global pose update).
          */
        ["cpp:const"] idempotent
        TimestampBase getTimestamp();

        RobotNodeSetInfo getRobotNodeSet(string name);
        NameList getRobotNodeSets();
        bool hasRobotNodeSet(string name);

        string getName();
        string getType();
        void setGlobalPose(PoseBase globalPose);
        PoseBase getGlobalPose();
        NameValueMap getConfig();
        NameValueMap getConfigAndPose(out PoseBase globalPose);

        float getScaling();
    };

    class RobotInfoNode;
    sequence<RobotInfoNode> RobotInfoNodeList;
    class RobotInfoNode
    {
        string name;
        string profile;
        string value;
        RobotInfoNodeList children;
    };

    interface GlobalRobotPoseLocalizationListener
    {
        void reportGlobalRobotPose(string robotName, Eigen::Matrix4f pose, long timestamp);
    };

    /**
     * The interface used by the RobotStateComponent which allows creating
     * snapshots of a robot configuration or retrieving a proxy to a constantly
     * updating synchronized robot.
     */
    interface RobotStateComponentInterface extends
            KinematicUnitListener,
            PlatformUnitListener,
            GlobalRobotPoseLocalizationListener,
            SimulatorResetEvent
    {
        /**
         * @return proxy to the shared robot which constantly updates all joint values
         */
        ["cpp:const"]
        idempotent
        SharedRobotInterface* getSynchronizedRobot() throws NotInitializedException;

        /**
         * @return proxy to a copy of the shared robot with non updating joint values
         */
        SharedRobotInterface* getRobotSnapshot(string deprecated) throws NotInitializedException;

        /**
          * Create robot snapshot proxy from past timestamp. The time that can be gone back depends on the robotstatecomponent configuration.
          * @return Snapshot
          *
          * */
        SharedRobotInterface* getRobotSnapshotAtTimestamp(double time) throws NotInitializedException;

        /**
          * Return the joint values from past timestamp. The time that can be gone back depends on the robotstatecomponent configuration.
          * @return Jointname-jointvalue map
          *
          * */
        ["cpp:const"]
        idempotent
        NameValueMap getJointConfigAtTimestamp(double time) throws NotInitializedException;

        /**
          * Return the Robot configuration, including joint values and global pose of the root node, from past timestamp. The time that can be gone back depends on the robotstatecomponent configuration.
          * @return Robot configuration containing jointvalue map and globalpose
          *
          * */
        ["cpp:const"]
        idempotent
        RobotStateConfig getRobotStateAtTimestamp(double time) throws NotInitializedException;

        /**
         * @return the robot xml filename as specified in the configuration
         */
        ["cpp:const"]
        idempotent
        string getRobotFilename();

        /**
         * @return All dependent packages, which might contain a robot file.
         */
        ["cpp:const"]
        idempotent
        Ice::StringSeq getArmarXPackages();

        /**
         * @return The name of the robot represented by this component. Same as
         * getSynchronizedRobot()->getName()
         *
         */
       ["cpp:const"]
       idempotent string getRobotName() throws NotInitializedException;

       ["cpp:const"]
       idempotent string getRobotStateTopicName() throws NotInitializedException;


       /**
        * @return The scaling of the robot model represented by this component.
        *
        */
       ["cpp:const"]
       idempotent float getScaling();

       /**
        * @return The name of the robot node set that is represented by this component.
        *
        */
       ["cpp:const"]
       idempotent string getRobotNodeSetName() throws NotInitializedException;

       ["cpp:const"]
       idempotent RobotInfoNode getRobotInfo();

    };

    interface RobotStateListenerInterface
    {
        void reportGlobalRobotRootPose(FramedPoseBase globalPose, long timestamp, bool poseChanged);
        void reportJointValues(NameValueMap jointAngles, long timestamp, bool aValueChanged);
    };

};
