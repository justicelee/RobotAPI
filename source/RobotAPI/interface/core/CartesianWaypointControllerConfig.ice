/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Raphael Grimm
* @copyright  2019 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

module armarx
{
    struct CartesianWaypointControllerConfig
    {
        float maxPositionAcceleration       = 500;
        float maxOrientationAcceleration    = 1;
        float maxNullspaceAcceleration      = 2;

        float kpJointLimitAvoidance         = 1;
        float jointLimitAvoidanceScale      = 2;

        float thresholdOrientationNear      = 0.1;
        float thresholdOrientationReached   = 0.05;
        float thresholdPositionNear         = 50;
        float thresholdPositionReached      = 5;

        float maxOriVel = 1;
        float maxPosVel = 80;
        float kpOri     = 1;
        float kpPos     = 1;
    };
};


