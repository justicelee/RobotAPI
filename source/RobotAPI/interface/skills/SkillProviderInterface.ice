/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::SkillObserver
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>

#include <RobotAPI/interface/aron.ice>

//Forward declaration
module armarx
{
    module skills
    {
        interface SkillProviderInterface;
    }
}

//SkillDescription
module armarx
{
    module skills
    {
        struct TemplateSentencePart
        {
            string text;
            bool   placeholder = false;
        };

        sequence<TemplateSentencePart> TemplateSentence;
        sequence<TemplateSentence>     TemplateSentenceSeq;

        class ParameterConstraint {};
        class ParameterValueRangeConstraint extends ParameterConstraint
        {
            aron::data::AronData min;
            aron::data::AronData max;
        };
        class ParameterValueSetConstraint extends ParameterConstraint
        {
            aron::data::AronList allowedValues;
        };

        struct Parameter
        {
            aron::type::AronType type;
            ParameterConstraint  constraint;
        };
        dictionary<string, Parameter> ParameterDescriptionMap;

        struct SkillDescription
        {
            TemplateSentenceSeq     templateSentences;
            ParameterDescriptionMap parameters;
        };
        dictionary<string, SkillDescription> SkillDescriptionMap;
    }
}

//SkillStatus
module armarx
{
    module skills
    {
        module ExecutionStatus
        {
            enum Status
            {
                Idle,
                RunningButWaitingForDependencies,
                Running,
                Failed,
                Succeeded,
                Aborted
            };
        };

        struct SkillStatus
        {
            ExecutionStatus::Status status;
            string                  message;
            string                  error;
            StringStringDictionary  data; //maybe variants/aaron?
        };

        dictionary<string, SkillStatus> SkillStatusMap;
    }
}

//SkillParametrization
module armarx
{
    module skills
    {
        struct SkillParametrization
        {
            string                   skillName;
            StringStringDictionary   stringStringParams;
            aron::data::AronDataDict stringAronParams;
        };
    }
}

//SkillProviderInterface
module armarx
{
    module skills
    {
        interface SkillProviderInterface
        {
            ["cpp:const"] idempotent SkillDescriptionMap getSkills();
            ["cpp:const"] SkillStatusMap                 getSkillExecutionStatus();
            void                           executeSkill(SkillParametrization skill);

            SkillStatus                    abortSkill(string skill);
        };
    }
}

//ProvidedSkillsTopic
module armarx
{
    module skills
    {
        interface ProvidedSkillsTopic
        {
            void skillsProvided(SkillProviderInterface* provider, SkillDescriptionMap skills);
            void removeProvider(SkillProviderInterface* provider);
        };
    }
}
