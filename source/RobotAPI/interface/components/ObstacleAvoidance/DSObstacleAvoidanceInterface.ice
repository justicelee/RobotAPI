/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DSObstacleAvoidance
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// ArmarX
#include <ArmarXCore/interface/serialization/Eigen.ice>

// RobotAPI
#include <RobotAPI/interface/components/ObstacleAvoidance/ObstacleAvoidanceInterface.ice>
#include <RobotAPI/interface/components/ObstacleAvoidance/ObstacleDetectionInterface.ice>

module armarx
{

    module dsobstacleavoidance
    {

        struct Config
        {
            bool only_2d;
            bool aggregated;
            bool local_modulation;
            bool repulsion;
            bool planar_modulation;
            double critical_distance;
            double weight_power;
            double agent_safety_margin;
        };

    };


    interface DSObstacleAvoidanceInterface extends 
	ObstacleAvoidanceInterface, ObstacleDetectionInterface
    {

        dsobstacleavoidance::Config  getConfig();

        void
        writeDebugPlots(string filename);

    };

};
