#pragma once

// Eigen Ice definition
#include <ArmarXCore/interface/serialization/Eigen.ice>

// AronMacros
#include <RobotAPI/libraries/aron/aroncore/AronConfig.h>

module armarx
{
    module aron
    {

        /*************************
         * General Definitions ***
         ************************/
        sequence<byte> AronByteSequence;
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
        sequence<lowerType> Aron##upperType##Sequence;

        HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO


        /*************************
         * Aron Types ************
         ************************/
        module type
        {
            class AronType { };
            sequence<AronType> AronTypeList;
            dictionary<string, AronType> AronTypeDict;

            // Container Types (serialize to object/list)
            // Please note that either elementTypes xor acceptedType is set!!!
            class AronDictSerializerType extends AronType { AronTypeDict elementTypes; AronType acceptedType;};
            class AronListSerializerType extends AronType { AronTypeList elementTypes; AronType acceptedType;};

            class AronListType extends AronListSerializerType { };
            class AronTupleType extends AronListSerializerType { };

            class AronObjectType extends AronDictSerializerType { AronObjectType parent; string objectName; };
            class AronDictType extends AronDictSerializerType { };

            // Complex Types (serialize to ndarray)
            class AronNDArraySerializerType extends AronType { AronIntSequence dimensions; string typeName; };
            class AronEigenMatrixType extends AronNDArraySerializerType { };
            class AronIVTCByteImageType extends AronNDArraySerializerType { };
            class AronOpenCVMatType extends AronNDArraySerializerType { };
            class AronPCLPointcloudType extends AronNDArraySerializerType { };

            // Primitive Types
    #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
        class Aron##upperType##Type extends AronType { };

            HANDLE_PRIMITIVE_TYPES
    #undef RUN_ARON_MACRO
        };


        /*************************
         * Aron Data *************
         ************************/
        module data
        {
            class AronData { };
            sequence<AronData> AronDataList;
            dictionary<string, AronData> AronDataDict;

            // Container Data
            class AronList extends AronData { AronDataList elements; };
            class AronDict extends AronData { AronDataDict elements; };

            // Complex Data
            class AronNDArray extends AronData { AronByteSequence data; };

            // Basic Data
    #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
        class Aron##upperType extends AronData { lowerType value; };

            HANDLE_PRIMITIVE_TYPES
    #undef RUN_ARON_MACRO
        };

    };
};
