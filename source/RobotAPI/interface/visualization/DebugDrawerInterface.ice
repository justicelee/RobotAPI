/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::RobotAPI
 * @author     Nikolaus Vahrenkamp
 * @copyright  2014
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/units/KinematicUnitInterface.ice>

module armarx
{

    /*!
     * \brief The Color struct
     * Values in [0,1]
     */
    struct DrawColor
    {
         float r;
         float g;
         float b;
         float a;
    };
    sequence<DrawColor> DrawColorSequence;

    struct DrawColor24Bit
    {
         byte r;
         byte g;
         byte b;
    };


    struct HsvColor
    {
        //! 0-360
        float h;
        //! 0 - 1
        float s;
        //! 0 - 1
        float v;
    };


    /*!
     * \brief Contains information about a layer. (name, visibility and number of elements)
     */
    struct LayerInformation
    {
        string layerName;
        bool visible;
        int elementCount;
    };

    sequence<LayerInformation> LayerInformationSequence;

    /*!
     * \brief Represents a 3D point or a 3D normal.
     */
    struct DebugDrawerVertex
    {
        float x;
        float y;
        float z;
    };
    sequence<DebugDrawerVertex> DebugDrawerVertexSequence;

    /*!
     * \brief A triangle face consiting of 3 3D points.
     */
    struct DebugDrawerSimpleFace
    {
        DebugDrawerVertex vertex1;
        DebugDrawerVertex vertex2;
        DebugDrawerVertex vertex3;
    };
    sequence<DebugDrawerSimpleFace> DebugDrawerSimpleFaceSequence;

    struct DebugDrawerSimpleTriMesh
    {
        DebugDrawerSimpleFaceSequence faces;
        DrawColor color;
    };

    /*!
     * \brief An ID reference vertex struct containing position, normal and color IDs
     */
    struct DebugDrawerVertexID
    {
        int vertexID;
        int normalID;
        int colorID;
    };
    
    struct DebugDrawerNormal
    {
        float x;
        float y;
        float z;
    };

    /*!
     * \brief A triangle face consisting of 3 position-normal-color-vertices.
     */
    struct DebugDrawerFace
    {
        DebugDrawerVertexID vertex1;
        DebugDrawerVertexID vertex2;
        DebugDrawerVertexID vertex3;
        DebugDrawerNormal normal;
    };
    sequence<DebugDrawerFace> DebugDrawerFaceSequence;

    struct DebugDrawerTriMesh
    {
        DebugDrawerFaceSequence faces;
        DebugDrawerVertexSequence vertices;
        DrawColorSequence colors;
    };


    const int three = 3;
    const float zero = 0;

    struct DebugDrawerPointCloudElement
    {
        float x;
        float y;
        float z;
    };
    sequence<DebugDrawerPointCloudElement> DebugDrawerPointCloudElementList;

    struct DebugDrawerPointCloud
    {
        DebugDrawerPointCloudElementList points;
        float pointSize = three;
    };

    struct DebugDrawerColoredPointCloudElement
    {
        float x;
        float y;
        float z;
        DrawColor color;
    };
    sequence<DebugDrawerColoredPointCloudElement> DebugDrawerColoredPointCloudElementList;

    struct DebugDrawerColoredPointCloud
    {
        DebugDrawerColoredPointCloudElementList points;
        float pointSize = three;
    };

    struct DebugDrawer24BitColoredPointCloudElement
    {
        float x;
        float y;
        float z;
        DrawColor24Bit color;
    };
    sequence<DebugDrawer24BitColoredPointCloudElement> DebugDrawer24BitColoredPointCloudElementList;

    struct DebugDrawer24BitColoredPointCloud
    {
        DebugDrawer24BitColoredPointCloudElementList points;
        float pointSize = three;
        float transparency = zero;
    };

    sequence<DebugDrawerPointCloudElement> DebugDrawerLineSetPointList;
    sequence<float> DebugDrawerLineSetIntensityList;

    struct DebugDrawerLineSet
    {
        float lineWidth;
        DrawColor colorNoIntensity;
        DrawColor colorFullIntensity;

        DebugDrawerLineSetPointList points;
        DebugDrawerLineSetIntensityList intensities;

        bool useHeatMap = false;
    };

    sequence< Vector3Base > PolygonPointList;

    enum DrawStyle { FullModel, CollisionModel };

    struct DebugDrawerSelectionElement
    {
        string layerName;
        string elementName;
    };

    sequence<DebugDrawerSelectionElement> DebugDrawerSelectionList;

    /*!
      * \brief A layered drawing interface.
      * All drawing operations are identified with a layer name in order to distinguish different drawing entitties.
      * The layered approach allows a visualizer GUI to enable/disable different layers according to the requested visualization.
    */
    interface DebugDrawerInterface
    {
        void exportScene(string filename);
        void exportLayer(string filename, string layerName);

        /*!
         * \brief Draw or update a coordinate system.
         * \param drawLayer A custom name to distinguish different drawing layers.
         * \param poseName The internal name of the pose (can be used to update/disable the drawing).
         * \param globalPose The pose in global coordinate syetem.
         */
        void setPoseVisu(string layerName, string poseName, PoseBase globalPose);
        void setScaledPoseVisu(string layerName, string poseName, PoseBase globalPose, float scale);
        void setLineVisu(string layerName, string lineName, Vector3Base globalPosition1, Vector3Base globalPosition2, float lineWidth, DrawColor color);
        void setLineSetVisu(string layerName, string lineSetName, DebugDrawerLineSet lineSet);
        void setBoxVisu(string layerName, string boxName, PoseBase globalPose, Vector3Base dimensions, DrawColor color);
        void setTextVisu(string layerName, string textName, string text, Vector3Base globalPosition, DrawColor color, int size);
        void setSphereVisu(string layerName, string sphereName, Vector3Base globalPosition, DrawColor color, float radius);
        void setPointCloudVisu(string layerName, string pointCloudName, DebugDrawerPointCloud pointCloud);
        void setColoredPointCloudVisu(string layerName, string pointCloudName, DebugDrawerColoredPointCloud pointCloud);
        void set24BitColoredPointCloudVisu(string layerName, string pointCloudName, DebugDrawer24BitColoredPointCloud pointCloud);
        void setPolygonVisu(string layerName, string polygonName, PolygonPointList polygonPoints, DrawColor colorInner, DrawColor colorBorder, float lineWidth);
        void setTriMeshVisu(string layerName, string triMeshName, DebugDrawerTriMesh triMesh);
        void setArrowVisu(string layerName, string arrowName, Vector3Base position, Vector3Base direction, DrawColor color, float length, float width);
        void setCylinderVisu(string layerName, string cylinderName, Vector3Base globalPosition, Vector3Base direction, float length, float radius, DrawColor color);
        void setCircleArrowVisu(string layerName, string circleName, Vector3Base globalPosition, Vector3Base directionVec, float radius, float circleCompletion, float width, DrawColor color);


        /*!
         * \brief setRobotVisu Initializes a robot visualization
         * \param layerName The layer
         * \param robotName The identifier of the robot
         * \param robotFile The filename of the robot. The robot must be locally present in a project.
         * \param drawStyleType Either full or collision model.
         * \param armarxProject Additional armarx project that should be used to search the robot. Must be locally available and accessible through the armarx cmake search procedure.
         */

        void setRobotVisu(string layerName, string robotName, string robotFile, string armarxProject, DrawStyle drawStyleType);
        void updateRobotPose(string layerName, string robotName, PoseBase globalPose);
        void updateRobotConfig(string layerName, string robotName, NameValueMap configuration);
        /*!
         * \brief updateRobotColor Colorizes the robot visualization
         * \param layerName The layer
         * \param robotName The robot identifyer
         * \param c The draw color, if all is set to 0, the colorization is disabled (i.e. the original vizualization shows up)
         */
        void updateRobotColor(string layerName, string robotName, DrawColor c);
        void updateRobotNodeColor(string layerName, string robotName, string robotNodeName, DrawColor c);
        void removeRobotVisu(string layerName, string robotName);

        /*!
         * \brief setPoseVisu draws on the "debug" layer
         * \param poseName
         * \param globalPose
         */
        void setPoseDebugLayerVisu(string poseName, PoseBase globalPose);
        void setScaledPoseDebugLayerVisu(string poseName, PoseBase globalPose, float scale);
        void setLineDebugLayerVisu(string lineName, Vector3Base globalPosition1, Vector3Base globalPosition2, float lineWidth, DrawColor color);
        void setLineSetDebugLayerVisu(string lineSetName, DebugDrawerLineSet lineSet);
        void setBoxDebugLayerVisu(string boxName, PoseBase globalPose, Vector3Base dimensions, DrawColor color);
        void setTextDebugLayerVisu(string textName, string text, Vector3Base globalPosition, DrawColor color, int size);
        void setSphereDebugLayerVisu(string sphereName, Vector3Base globalPosition, DrawColor color, float radius);
        void setPointCloudDebugLayerVisu(string pointCloudName, DebugDrawerPointCloud pointCloud);
        void set24BitColoredPointCloudDebugLayerVisu(string pointCloudName, DebugDrawer24BitColoredPointCloud pointCloud);
        void setPolygonDebugLayerVisu(string polygonName, PolygonPointList polygonPoints, DrawColor colorInner, DrawColor colorBorder, float lineWidth);
        void setTriMeshDebugLayerVisu(string triMeshName, DebugDrawerTriMesh triMesh);
        void setArrowDebugLayerVisu(string arrowName, Vector3Base position, Vector3Base direction, DrawColor color, float length, float width);
        void setCylinderDebugLayerVisu(string cylinderName, Vector3Base globalPosition, Vector3Base direction, float length, float radius, DrawColor color);
        /*!
         * \brief Draws a circle into the debug drawer.
         * \param circleName
         * \param globalPosition
         * \param directionVec Normal on the circle
         * \param radius
         * \param circleCompletion interval [-1,1], if != +-1 only part of the circle is drawn. If < 0, the circle completion direction is inverted.
         * \param width
         * \param color
         * \param circleDrawingDirection in which direction the circle is started to draw. Has only effect if the circle is not complete (circleCompletion parameter).
         */
        void setCircleDebugLayerVisu(string circleName, Vector3Base globalPosition, Vector3Base directionVec, float radius, float circleCompletion, float width, DrawColor color);

        /*!
         * \brief Remove visualization of coordinate system.
         * \param layerName The name of the drawing layer.
         * \param poseName The name of the pose.
         */
        void removePoseVisu(string layerName, string poseName);
        void removeLineVisu(string layerName, string lineName);
        void removeLineSetVisu(string layerName, string lineSetName);
        void removeBoxVisu(string layerName, string boxName);
        void removeTextVisu(string layerName, string textName);
        void removeSphereVisu(string layerName, string sphereName);
        void removePointCloudVisu(string layerName, string pointCloudName);
        void removeColoredPointCloudVisu(string layerName, string pointCloudName);
        void remove24BitColoredPointCloudVisu(string layerName, string pointCloudName);
        void removePolygonVisu(string layerName, string polygonName);
        void removeTriMeshVisu(string layerName, string triMeshName);
        void removeArrowVisu(string layerName, string arrowName);
        void removeCylinderVisu(string layerName, string cylinderName);
        void removeCircleVisu(string layerName, string circleName);


        /*!
         * \brief Removes pose from the "debug" layer.
         */
        void removePoseDebugLayerVisu(string poseName);
        void removeLineDebugLayerVisu(string lineName);
        void removeLineSetDebugLayerVisu(string lineSetName);
        void removeBoxDebugLayerVisu(string boxName);
        void removeTextDebugLayerVisu(string textName);
        void removeSphereDebugLayerVisu(string sphereName);
        void removePointCloudDebugLayerVisu(string pointCloudName);
        void removeColoredPointCloudDebugLayerVisu(string pointCloudName);
        void remove24BitColoredPointCloudDebugLayerVisu(string pointCloudName);
        void removePolygonDebugLayerVisu(string polygonName);
        void removeTriMeshDebugLayerVisu(string triMeshName);
        void removeArrowDebugLayerVisu(string arrowName);
        void removeCylinderDebugLayerVisu(string cylinderName);
        void removeCircleDebugLayerVisu(string circleName);

        /*!
         * \brief clearAll removes all visualizations for all layers
         */
        void clearAll();

        /*!
         * \brief clearLayer removes all visualizations for the given layer
         * \param layerName The name identifies the layer.
         */
        void clearLayer(string layerName);
        /*!
         * \brief clearDebugLayer calls <code>clear("debug");</code>
         */
        void clearDebugLayer();

        /*!
         * \brief Sets the layer's visibility.
         * \param layerName The layer.
         * \param visible Whether the layer will be visible.
         */
        void enableLayerVisu(string layerName, bool visible);

        void enableDebugLayerVisu(bool visible);

        /*!
         * \brief Returns the names of all layers.
         * \return The names of all layers.
         */
        Ice::StringSeq layerNames();

        /*!
         * \brief Returns information about each layer.
         * \return Information about each layer.
         */
        LayerInformationSequence layerInformation();

        /*!
         * \brief Returns whether a layer exists.
         * \param layerName The layer.
         * \return Whether a layer exists.
         */
        bool hasLayer(string layerName);

        /*!
         * \brief Removes a layer and all of its content.
         * \param layerName The layer.
         */
        void removeLayer(string layerName);

        /*!
         * \brief Disables the visibility of all layers. Only enableAllLayers() can reverse this.
         * \see enableAllLayers
         */
        void disableAllLayers();

        /*!
         * \brief If layers were disabled they are visualized again.
         * If a layer's visibility was changed the changes are applied.
         */
        void enableAllLayers();

        /*!
         * \brief Enable selections in visualizations
         */
        void enableSelections(string layerName);

        /*!
         * \brief Disable selections in visualizations
         */
        void disableSelections(string layerName);

        /*!
         * \brief Clear selections in visualizations
         */
        void clearSelections(string layerName);

        /*!
         * \brief Select element in visualization
         */
        void select(string layerName, string elementName);

        /*!
         * \brief Deselect element in visualization
         */
        void deselect(string layerName, string elementName);

        /*!
         * \brief Return currently selected elements
         */
        DebugDrawerSelectionList getSelections();
    };

    interface DebugDrawerListener
    {
        void reportSelectionChanged(DebugDrawerSelectionList selectedObjects);
    };

    interface DebugDrawerInterfaceAndListener extends DebugDrawerInterface, DebugDrawerListener
    {
    };
};

