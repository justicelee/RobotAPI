#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
module viz
{

struct Header
{
    string frame_id;
    int stamp = 0;
};

struct Vec3
{
    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;
};

struct Orientation
{
    float qw = 1.0f;
    float qx = 0.0f;
    float qy = 0.0f;
    float qz = 0.0f;
};

struct Color
{
    float a = 1.0f;
    float r = 0.0f;
    float g = 0.0f;
    float b = 0.0f;
};

sequence<Vec3> Vec3List;
sequence<Color> ColorList;

struct Marker
{
    Header header;
    string ns;
    int id = 0;
    int type = 0;
    Vec3 position;
    Orientation orientation;
    Vec3 scale;
    Color color;
    Vec3List points;
    ColorList colors;
    float lifetime_in_seconds = 0.0f;
};

sequence<Marker> MarkerList;

interface Topic
{
    void draw(MarkerList markers);
};

}
}
