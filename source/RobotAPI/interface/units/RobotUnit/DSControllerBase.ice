/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>

module armarx
{
    class DSControllerConfig extends NJointControllerConfig
    {
        float posiKp = 5;
        float v_max = 0.15;
        float posiDamping = 10;

        float oriDamping;
        float oriKp;

        float filterTimeConstant = 0.01;
        float torqueLimit = 1;


        string nodeSetName = "";
        string tcpName = "";

        Ice::FloatSeq desiredPosition;
        Ice::FloatSeq desiredQuaternion;

        Ice::FloatSeq qnullspaceVec;

        float nullspaceKp;
        float nullspaceDamping;


        Ice::StringSeq gmmParamsFiles;
        float positionErrorTolerance;

        float dsAdaptorEpsilon;

    };

    interface DSControllerInterface extends NJointControllerInterface
    {

    };

    class DSRTBimanualControllerConfig extends NJointControllerConfig
    {
        float posiKp = 5;
        float v_max = 0.15;
        Ice::FloatSeq posiDamping;
        float couplingStiffness = 10;
        float couplingForceLimit = 50;

        float forwardGain = 1;

        float oriDamping;
        float oriKp;

        float filterTimeConstant = 0.01;
        float torqueLimit = 1;
        float NullTorqueLimit = 0.2;

//        string tcpName = "";

//        Ice::FloatSeq desiredPosition;

        Ice::FloatSeq left_desiredQuaternion;
        Ice::FloatSeq right_desiredQuaternion;

        Ice::FloatSeq leftarm_qnullspaceVec;
        Ice::FloatSeq rightarm_qnullspaceVec;

        float nullspaceKp;
        float nullspaceDamping;



        string gmmParamsFile = "";
        float positionErrorTolerance;

        float contactForce;
        float guardTargetZUp;
        float guardTargetZDown;
        float loweringForce;
        float liftingForce;
        bool guardDesiredDirection;
        float highPassFilterFactor;


        Ice::FloatSeq left_oriUp;
        Ice::FloatSeq left_oriDown;
        Ice::FloatSeq right_oriUp;
        Ice::FloatSeq right_oriDown;
        float forceFilterCoeff;
        float forceErrorZDisturbance;
        float forceErrorZThreshold;
        float forceErrorZGain;
        float desiredTorqueDisturbance;
        float TorqueFilterConstant;
        float leftForceOffsetZ;
        float rightForceOffsetZ;
        float contactDistanceTolerance;
        float mountingDistanceTolerance;
        float mountingCorrectionFilterFactor;

        Ice::FloatSeq forceLeftOffset;
        Ice::FloatSeq forceRightOffset;
    };


    interface DSBimanualControllerInterface extends NJointControllerInterface
    {
        void setToDefaultTarget();

    };

    class DSJointCarryControllerConfig extends NJointControllerConfig
    {
        float posiKp = 5;
        float v_max = 0.15;
        Ice::FloatSeq posiDamping;
        float oriDamping;
        float oriKp;

        float filterTimeConstant = 0.01;
        float torqueLimit = 1;
        float NullTorqueLimit = 0.2;

        Ice::FloatSeq left_desiredQuaternion;
        Ice::FloatSeq right_desiredQuaternion;
        Ice::FloatSeq leftarm_qnullspaceVec;
        Ice::FloatSeq rightarm_qnullspaceVec;

        float nullspaceKp;
        float nullspaceDamping;

        string gmmParamsFile = "";
        float positionErrorTolerance;
        float desiredTorqueDisturbance;
        float TorqueFilterConstant;
        float guardLength;
        float guardGravity;

        Ice::FloatSeq defaultGuardOri;
        float handVelLimit;
        Ice::FloatSeq defaultRotationStiffness;
    };


    interface DSJointCarryControllerInterface extends NJointControllerInterface
    {
        void setGuardInHandPosition(Ice::FloatSeq guardCenterToHandsCenter);
        void setGuardOrientation(Ice::FloatSeq guardOrientationInRobotBase);
        void setDesiredGuardOri(Ice::FloatSeq desiredGuardOriInRobotBase);
        void setRotationStiffness(Ice::FloatSeq guardRotationStiffness);
        void setGuardObsAvoidVel(Ice::FloatSeq guardZVel);
        float getGMMVel();
    };

};
