/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/RemoteReferenceCount.ice>
#include <ArmarXCore/interface/components/EmergencyStopInterface.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>

#include <ArmarXGui/interface/WidgetDescription.ice>

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>

#include <RobotAPI/interface/units/KinematicUnitInterface.ice>
#include <RobotAPI/interface/units/ForceTorqueUnit.ice>
#include <RobotAPI/interface/units/InertialMeasurementUnit.ice>
#include <RobotAPI/interface/units/PlatformUnitInterface.ice>
#include <RobotAPI/interface/units/TCPControlUnit.ice>
#include <RobotAPI/interface/components/TrajectoryPlayerInterface.ice>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.ice>
#include <RobotAPI/interface/components/RobotHealthInterface.ice>

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXGui/interface/WidgetDescription.ice>

//NJointController
module armarx
{
    interface NJointControllerInterface;
    interface RobotUnitInterface;


    module RobotUnitControllerNames
    {
        const string NJointTrajectoryController = "NJointTrajectoryController";
        const string NJointGlobalTCPController = "NJointGlobalTCPController"; /*@@@TODO: move NJointGlobalTCPController to RobotAPI */
        const string NJointTCPController = "NJointTCPController";
        const string NJointCartesianVelocityController = "NJointCartesianVelocityController";
    };


    class NJointControllerConfig {};

    struct NJointControllerDescription
    {
        string instanceName;
        string className;
        NJointControllerInterface* controller;
        StringStringDictionary controlModeAssignment;
        bool deletable;
        bool internal;
    };
    sequence<NJointControllerDescription> NJointControllerDescriptionSeq;

    struct NJointControllerStatus
    {
        string instanceName;
        bool active = false;
        bool requested = false;
        bool error = false;
        long timestampUSec = 0;
    };
    sequence<NJointControllerStatus> NJointControllerStatusSeq;

    struct NJointControllerDescriptionWithStatus
    {
        NJointControllerStatus status;
        NJointControllerDescription description;
    };
    sequence<NJointControllerDescriptionWithStatus> NJointControllerDescriptionWithStatusSeq;

    interface NJointControllerInterface
    {
        ["cpp:const"] idempotent string getClassName();
        ["cpp:const"] idempotent string getInstanceName();
        ["cpp:const"] idempotent StringStringDictionary getControlDeviceUsedControlModeMap();
        ["cpp:const"] idempotent bool isControllerActive();
        ["cpp:const"] idempotent bool isControllerRequested();
        ["cpp:const"] idempotent bool isDeletable();
        ["cpp:const"] idempotent bool hasControllerError();
        ["cpp:const"] idempotent NJointControllerStatus getControllerStatus();
        ["cpp:const"] idempotent NJointControllerDescription getControllerDescription();
        ["cpp:const"] idempotent NJointControllerDescriptionWithStatus getControllerDescriptionWithStatus();
        ["cpp:const"] idempotent RobotUnitInterface* getRobotUnit();

        void activateController();
        void deactivateController();
        void deleteController() throws LogicError;
        void deactivateAndDeleteController() throws LogicError;

        ["cpp:const"] idempotent WidgetDescription::StringWidgetDictionary getFunctionDescriptions();
        void callDescribedFunction(string fuinctionName, StringVariantBaseMap values) throws InvalidArgumentException;
    };

    dictionary<string, NJointControllerInterface*> StringNJointControllerPrxDictionary;
};

//RobotUnit Utility types - ControlDevice
module armarx
{
    dictionary<string, Ice::StringSeq> ControlDeviceNameToControlModesDictionary;
    dictionary<string, string> ControlDeviceNameToNJointControllerNameDictionary;

    struct HWControlModeAndTargetType
    {
        string hardwareControlMode;
        string targetType;
    };
    dictionary<string, HWControlModeAndTargetType> ControlModeToHWControlModeAndTargetTypeDictionary;

    struct ControlDeviceDescription
    {
        string deviceName;
        ControlModeToHWControlModeAndTargetTypeDictionary contolModeToTargetType;
        Ice::StringSeq tags;
    };
    sequence<ControlDeviceDescription> ControlDeviceDescriptionSeq;
    struct ControlDeviceStatus
    {
        string deviceName;
        string activeControlMode;
        string requestedControlMode;
        StringToStringVariantBaseMapMap controlTargetValues;
        long timestampUSec = 0;
    };
    sequence<ControlDeviceStatus> ControlDeviceStatusSeq;
}
//RobotUnit Utility types - SensorDevice
module armarx
{
    struct SensorDeviceDescription
    {
        string deviceName;
        string sensorValueType;
        Ice::StringSeq tags;
    };
    sequence<SensorDeviceDescription> SensorDeviceDescriptionSeq;
    struct SensorDeviceStatus
    {
        string deviceName;
        StringVariantBaseMap sensorValue;
        long timestampUSec = 0;
    };
    sequence<SensorDeviceStatus> SensorDeviceStatusSeq;
}
//RobotUnit Utility types - NJointController
module armarx
{
    struct NJointControllerClassDescription
    {
        string className;
        WidgetDescription::Widget configDescription;
    };
    sequence<NJointControllerClassDescription> NJointControllerClassDescriptionSeq;
}
//RobotUnit Utility types - data streaming
module armarx
{
    module RobotUnitDataStreaming
    {
        enum DataEntryType
        {
            NodeTypeBool,
            NodeTypeByte,
            NodeTypeShort,
            NodeTypeInt,
            NodeTypeLong,
            NodeTypeFloat,
            NodeTypeDouble
        };

        struct DataEntry
        {
            DataEntryType  type;
            long           index = -1;
        };
        dictionary<string, DataEntry> StringDataEntryMap;

        struct DataStreamingDescription
        {
            StringDataEntryMap entries;
        };


        struct TimeStep
        {
            long iterationId;
            long timestampUSec;
            long timesSinceLastIterationUSec;
            Ice::BoolSeq   bools;
            Ice::ByteSeq   bytes;
            Ice::ShortSeq  shorts;
            Ice::IntSeq    ints;
            Ice::LongSeq   longs;
            Ice::FloatSeq  floats;
            Ice::DoubleSeq doubles;
        };
        sequence<TimeStep> TimeStepSeq;

        interface Receiver
        {
            void update(TimeStepSeq data);
        };

        struct Config
        {
            Ice::StringSeq loggingNames;
        };
    }
}

//RobotUnit Listener
module armarx
{
    interface RobotUnitListener
    {
        void nJointControllerStatusChanged(NJointControllerStatusSeq status);
        void controlDeviceStatusChanged(ControlDeviceStatusSeq status);
        void sensorDeviceStatusChanged(SensorDeviceStatusSeq status);
        void nJointControllerClassAdded(string className);
        void nJointControllerCreated(string instanceName);
        void nJointControllerDeleted(string instanceName);
    };
}

//RobotUnit Modules
module armarx
{
    module RobotUnitModule
    {
        interface RobotUnitManagementInterface extends AggregatedRobotHealthInterface
        {
            //state
            ["cpp:const"] idempotent bool isRunning();
            ["cpp:const"] idempotent bool isSimulation();
        };
        interface RobotUnitLoggingInterface
        {
            //rt-logging
            ["cpp:const"] idempotent Ice::StringSeq getLoggingNames();
            RemoteReferenceCounterBase startRtLogging(string filePathFormatString, Ice::StringSeq loggingNames) throws LogicError, InvalidArgumentException;
            RemoteReferenceCounterBase startRtLoggingWithAliasNames(string filePathFormatString, StringStringDictionary aliasNames) throws LogicError, InvalidArgumentException;
            void addMarkerToRtLog(RemoteReferenceCounterBase token, string marker) throws LogicError;
            void stopRtLogging(RemoteReferenceCounterBase token) throws LogicError;

            ["cpp:const"] void writeRecentIterationsToFile(string filePathFormatString) throws LogicError, InvalidArgumentException;

            RobotUnitDataStreaming::DataStreamingDescription startDataStreaming(RobotUnitDataStreaming::Receiver* receiver, RobotUnitDataStreaming::Config config);
            void stopDataStreaming(RobotUnitDataStreaming::Receiver* receiver);
        };
        interface RobotUnitUnitInterface
        {
            ["cpp:const"] idempotent Object* getUnit(string staticIceId);
            ["cpp:const"] idempotent Ice::ObjectProxySeq getUnits();
            ["cpp:const"] idempotent KinematicUnitInterface* getKinematicUnit();
            ["cpp:const"] idempotent ForceTorqueUnitInterface* getForceTorqueUnit();
            ["cpp:const"] idempotent InertialMeasurementUnitInterface* getInertialMeasurementUnit();
            ["cpp:const"] idempotent PlatformUnitInterface* getPlatformUnit();
            ["cpp:const"] idempotent TCPControlUnitInterface* getTCPControlUnit();
            ["cpp:const"] idempotent TrajectoryPlayerInterface* getTrajectoryPlayer();
        };
        interface RobotUnitPublishingInterface
        {
            ["cpp:const"] idempotent string getRobotUnitListenerTopicName();
            ["cpp:const"] idempotent string getDebugDrawerTopicName();
            ["cpp:const"] idempotent string getDebugObserverTopicName();

            ["cpp:const"] idempotent RobotUnitListener* getRobotUnitListenerProxy();
            ["cpp:const"] idempotent DebugDrawerInterface* getDebugDrawerProxy();
            ["cpp:const"] idempotent DebugObserverInterface* getDebugObserverProxy();
        };
        interface RobotUnitDevicesInterface
        {
            //devices
            ["cpp:const"] idempotent Ice::StringSeq                           getControlDeviceNames() throws LogicError;
            ["cpp:const"] idempotent ControlDeviceDescription                 getControlDeviceDescription(string name) throws InvalidArgumentException, LogicError;
            ["cpp:const"] idempotent ControlDeviceDescriptionSeq              getControlDeviceDescriptions() throws LogicError;
            ["cpp:const"] idempotent ControlDeviceStatus                      getControlDeviceStatus(string name) throws InvalidArgumentException, LogicError;
            ["cpp:const"] idempotent ControlDeviceStatusSeq                   getControlDeviceStatuses() throws LogicError;

            ["cpp:const"] idempotent Ice::StringSeq                           getSensorDeviceNames() throws LogicError;
            ["cpp:const"] idempotent SensorDeviceDescription                  getSensorDeviceDescription(string name) throws InvalidArgumentException, LogicError;
            ["cpp:const"] idempotent SensorDeviceDescriptionSeq               getSensorDeviceDescriptions() throws LogicError;
            ["cpp:const"] idempotent SensorDeviceStatus                       getSensorDeviceStatus(string name) throws InvalidArgumentException, LogicError;
            ["cpp:const"] idempotent SensorDeviceStatusSeq                    getSensorDeviceStatuses() throws LogicError;
        };
        interface RobotUnitControllerManagementInterface
        {
            //names
            ["cpp:const"] idempotent Ice::StringSeq getNJointControllerNames();
            ["cpp:const"] idempotent Ice::StringSeq getRequestedNJointControllerNames();
            ["cpp:const"] idempotent Ice::StringSeq getActivatedNJointControllerNames();
            //proxy/information
            ["cpp:const"] idempotent NJointControllerInterface*               getNJointController(string name);
            ["cpp:const"] idempotent StringNJointControllerPrxDictionary      getAllNJointControllers();

            ["cpp:const"] idempotent NJointControllerStatus                   getNJointControllerStatus(string name) throws InvalidArgumentException;
            ["cpp:const"] idempotent NJointControllerStatusSeq                getNJointControllerStatuses();

            ["cpp:const"] idempotent NJointControllerDescription              getNJointControllerDescription(string name) throws InvalidArgumentException;
            ["cpp:const"] idempotent NJointControllerDescriptionSeq           getNJointControllerDescriptions();

            ["cpp:const"] idempotent NJointControllerDescriptionWithStatus    getNJointControllerDescriptionWithStatus(string name) throws InvalidArgumentException;
            ["cpp:const"] idempotent NJointControllerDescriptionWithStatusSeq getNJointControllerDescriptionsWithStatuses();

            //classes
            ["cpp:const"] idempotent Ice::StringSeq                           getNJointControllerClassNames();
            ["cpp:const"] idempotent NJointControllerClassDescription         getNJointControllerClassDescription(string name) throws InvalidArgumentException;
            ["cpp:const"] idempotent NJointControllerClassDescriptionSeq      getNJointControllerClassDescriptions();

            //switching
            void switchNJointControllerSetup(Ice::StringSeq newSetup) throws InvalidArgumentException, LogicError;

            void activateNJointController(string controllerInstanceName) throws InvalidArgumentException, LogicError;
            void activateNJointControllers(Ice::StringSeq controllerInstanceNames) throws InvalidArgumentException, LogicError;
            void deactivateNJointController(string controllerInstanceName)throws InvalidArgumentException, LogicError;
            void deactivateNJointControllers(Ice::StringSeq controllerInstanceNames)throws InvalidArgumentException, LogicError;
            //creting controllers
            NJointControllerInterface* createNJointController(string className, string instanceName, NJointControllerConfig config) throws InvalidArgumentException, LogicError;
            NJointControllerInterface* createNJointControllerFromVariantConfig(string className, string instanceName, StringVariantBaseMap config) throws InvalidArgumentException, LogicError;

            NJointControllerInterface* createOrReplaceNJointController(string className, string instanceName, NJointControllerConfig config) throws InvalidArgumentException, LogicError;

            //deleting controllers
            void deleteNJointController(string controllerInstanceName)throws InvalidArgumentException, LogicError;
            void deleteNJointControllers(Ice::StringSeq controllerInstanceNames)throws InvalidArgumentException, LogicError;

            void deactivateAndDeleteNJointController(string controllerInstanceName)throws InvalidArgumentException, LogicError;
            void deactivateAndDeleteNJointControllers(Ice::StringSeq controllerInstanceNames)throws InvalidArgumentException, LogicError;

            //loading libs
            bool loadLibFromPath(string path);
            bool loadLibFromPackage(string package, string libname);
        };
        interface RobotUnitSelfCollisionCheckerInterface
        {
            ["cpp:const"] idempotent bool isSelfCollisionCheckEnabled();
            ["cpp:const"] idempotent float getSelfCollisionAvoidanceFrequency();
            ["cpp:const"] idempotent float getSelfCollisionAvoidanceDistance();
            idempotent void setSelfCollisionAvoidanceFrequency(float freq);
            idempotent void setSelfCollisionAvoidanceDistance(float dist);
        };
        interface RobotUnitControlThreadInterface
        {
            idempotent void setEmergencyStopState(EmergencyStopState state);
            ["cpp:const"] idempotent EmergencyStopState getEmergencyStopState();
            ["cpp:const"] idempotent EmergencyStopState getRtEmergencyStopState();
        };
    };
}

//RobotUnit
module armarx
{
    interface RobotUnitInterface extends
        RobotUnitModule::RobotUnitUnitInterface,
        RobotUnitModule::RobotUnitDevicesInterface,
        RobotUnitModule::RobotUnitLoggingInterface,
        RobotUnitModule::RobotUnitPublishingInterface,
        RobotUnitModule::RobotUnitManagementInterface,
        RobotUnitModule::RobotUnitControlThreadInterface,
        RobotUnitModule::RobotUnitControllerManagementInterface,
        RobotUnitModule::RobotUnitSelfCollisionCheckerInterface
    {
    };
};

