/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>

module armarx
{
    module NJointTaskSpaceDMPControllerMode
    {
        enum CartesianSelection
        {
            ePosition = 7,
            eOrientation = 8,
            eAll = 15
        };
    };

    class NJointTaskSpaceDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        float DMPKd = 20;
        int kernelSize = 100;
        double tau = 1;
        string dmpMode = "MinimumJerk";
        string dmpStyle = "Discrete";
        double dmpAmplitude = 1;

        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double timeDuration = 10;
        double posToOriRatio = 100;

        // velocity controller configuration
        string nodeSetName = "";
        string tcpName = "";
        string frameName = "";
        NJointTaskSpaceDMPControllerMode::CartesianSelection mode = NJointTaskSpaceDMPControllerMode::eAll;

        double maxLinearVel;
        double maxAngularVel;
        double maxJointVelocity;
        string debugName;

        float Kp_LinearVel;
        float Kd_LinearVel;
        float Kp_AngularVel;
        float Kd_AngularVel;

        float pos_filter;
        float vel_filter;
    };


    interface NJointTaskSpaceDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);
        void runDMPWithTime(Ice::DoubleSeq goals, double time);

        void setSpeed(double times);
        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void removeAllViaPoints();
        void setGoals(Ice::DoubleSeq goals);

        double getCanVal();
        void resetDMP();
        void stopDMP();
        void pauseDMP();
        void resumeDMP();

        void setControllerTarget(float avoidJointLimitsKp, NJointTaskSpaceDMPControllerMode::CartesianSelection mode);
        void setTorqueKp(StringFloatDictionary torqueKp);
        void setNullspaceJointVelocities(StringFloatDictionary nullspaceJointVelocities);
        string getDMPAsString();
        Ice::DoubleSeq createDMPFromString(string dmpString);

        void setMPWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPWeights();

        void setLinearVelocityKd(float kd);
        void setLinearVelocityKp(float kp);
        void setAngularVelocityKd(float kd);
        void setAngularVelocityKp(float kp);
    };

    class NJointCCDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        float DMPKd = 20;
        int kernelSize = 100;
        double tau = 1;
        int dmpMode = 1;
        int dmpNum = 2;

        // phaseStop technique
        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;


        // misc
        Ice::DoubleSeq timeDurations;
        Ice::StringSeq dmpTypes;
        Ice::DoubleSeq amplitudes;


        double posToOriRatio = 100;

        // velocity controller configuration
        string nodeSetName = "";
        string tcpName = "";
        NJointTaskSpaceDMPControllerMode::CartesianSelection mode = NJointTaskSpaceDMPControllerMode::eAll;

    };

    interface NJointCCDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(int dmpId, Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP();

        void setTemporalFactor(int dmpId, double tau);
        void setViaPoints(int dmpId, double canVal, Ice::DoubleSeq point);
        void setGoals(int dmpId, Ice::DoubleSeq goals);

        void setControllerTarget(float avoidJointLimitsKp, NJointTaskSpaceDMPControllerMode::CartesianSelection mode);
        void setTorqueKp(StringFloatDictionary torqueKp);
        void setNullspaceJointVelocities(StringFloatDictionary nullspaceJointVelocities);
    };

    class NJointBimanualCCDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpType = "Discrete";

        // phaseStop technique
        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 10;
        double timeDuration = 10;

        string defautLeader = "Left";

        Ice::FloatSeq leftDesiredJointValues;
        Ice::FloatSeq rightDesiredJointValues;

//        float KoriFollower = 1;
//        float KposFollower = 1;

        double maxLinearVel;
        double maxAngularVel;

        Ice::FloatSeq leftKpos;
        Ice::FloatSeq leftDpos;
        Ice::FloatSeq leftKori;
        Ice::FloatSeq leftDori;

        Ice::FloatSeq rightKpos;
        Ice::FloatSeq rightDpos;
        Ice::FloatSeq rightKori;
        Ice::FloatSeq rightDori;

        float knull;
        float dnull;

        float torqueLimit;

        float startReduceTorque;

//        Ice::FloatSeq Kpf;
//        Ice::FloatSeq Kif;
//        Ice::FloatSeq DesiredForce;

//        float BoxWidth;

//        float FilterTimeConstant;


    };

    interface NJointBimanualCCDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(string whichDMP, Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq leftGoals, Ice::DoubleSeq rightGoals);

        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setGoals(Ice::DoubleSeq goals);

        void changeLeader();
        double getVirtualTime();

        string getLeaderName();

    };


    class NJointBimanualCCDMPVelocityControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpType = "Discrete";

        double timeDuration = 10;
        string defautLeader = "Left";

        Ice::FloatSeq leftDesiredJointValues;
        Ice::FloatSeq rightDesiredJointValues;

        float maxLinearVel;
        float maxAngularVel;

        Ice::FloatSeq leftKpos;
        Ice::FloatSeq leftDpos;
        Ice::FloatSeq leftKori;
        Ice::FloatSeq leftDori;

        Ice::FloatSeq rightKpos;
        Ice::FloatSeq rightDpos;
        Ice::FloatSeq rightKori;
        Ice::FloatSeq rightDori;

        float knull;
        float dnull;

        float maxJointVel;
        Ice::DoubleSeq initRatio;
    };

    interface NJointBimanualCCDMPVelocityControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(string whichDMP, Ice::StringSeq trajfiles);
        void learnDMPFromBothFiles(Ice::StringSeq leftFiles, Ice::StringSeq rightFiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq leftGoals, Ice::DoubleSeq rightGoals);

        void setRatios(Ice::DoubleSeq ratios);

        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setGoals(Ice::DoubleSeq goals);

        void changeLeader();
        double getVirtualTime();

        string getLeaderName();
    };



    class NJointTaskSpaceImpedanceDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpType = "Discrete";
        double timeDuration;
        string nodeSetName;

        // phaseStop technique
        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double posToOriRatio = 100;

        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;

        Ice::FloatSeq Knull;
        Ice::FloatSeq Dnull;



        bool useNullSpaceJointDMP;
        Ice::FloatSeq defaultNullSpaceJointValues;

        float torqueLimit;

    };

    interface NJointTaskSpaceImpedanceDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnJointDMPFromFiles(string jointTrajFile, Ice::FloatSeq currentJVS);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals);
        void runDMPWithTime(Ice::DoubleSeq goals, double timeDuration);

        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setGoals(Ice::DoubleSeq goals);

        double getVirtualTime();

        void resetDMP();
        void stopDMP();
        void resumeDMP();

        void setMPWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPWeights();

    };

    class NJointTaskSpaceAdaptiveDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpType = "Discrete";
        double timeDuration;
        string nodeSetName;

        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;
        Ice::FloatSeq Knull;
        Ice::FloatSeq Dnull;

        bool useNullSpaceJointDMP;
        Ice::FloatSeq defaultNullSpaceJointValues;

        float torqueLimit;
        string forceSensorName;
        float filterCoeff;
    };

    interface NJointTaskSpaceAdaptiveDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnJointDMPFromFiles(string jointTrajFile, Ice::FloatSeq currentJVS);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals);
        void runDMPWithTime(Ice::DoubleSeq goals, double timeDuration);

        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setGoals(Ice::DoubleSeq goals);

        double getVirtualTime();
        void setCanVal(double canVal);

        void resetDMP();
        void stopDMP();
        void resumeDMP();

        void setKdImpedance(Ice::FloatSeq dampings);
        void setKpImpedance(Ice::FloatSeq stiffness);
        void setKpNull(Ice::FloatSeq knull);
        void setKdNull(Ice::FloatSeq dnull);
        Ice::FloatSeq getForce();
        Ice::FloatSeq getVelocityInMM();
        void removeAllViaPoints();

        void setUseNullSpaceJointDMP(bool useJointDMP);
        void setDefaultJointValues(Ice::FloatSeq desiredJointVals);
    };


    class NJointPeriodicTSDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        double dmpAmplitude = 1;
        double timeDuration = 10;

        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 100;


        // velocity controller configuration
        string nodeSetName = "";

        double maxLinearVel;
        double maxAngularVel;
        float maxJointVel;
        float avoidJointLimitsKp = 1;

        float Kpos;
        float Kori;

        string forceSensorName = "FT R";
        string forceFrameName = "ArmR8_Wri2";
        float forceFilter = 0.8;
        float waitTimeForCalibration = 0.1;
        float Kpf;

        float minimumReactForce = 0;
    };


    interface NJointPeriodicTSDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);

        void setSpeed(double times);
        void setGoals(Ice::DoubleSeq goals);
        void setAmplitude(double amplitude);

        double getCanVal();
    };


    class NJointPeriodicTSDMPCompliantControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        double dmpAmplitude = 1;
        double timeDuration = 10;

        double phaseL = 10;
        double phaseK = 10;
        float phaseDist0 = 50;
        float phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 100;


        // velocity controller configuration
        string nodeSetName = "";

        float maxJointTorque;
        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;

        Ice::FloatSeq desiredNullSpaceJointValues;
        float Knull;
        float Dnull;

        string forceSensorName = "FT R";
        string forceFrameName = "ArmR8_Wri2";
        float forceFilter = 0.8;
        float waitTimeForCalibration = 0.1;
        float Kpf;

        float minimumReactForce = 0;

        float forceDeadZone;
        float velFilter;

        float maxLinearVel;
        float maxAngularVel;

        Ice::FloatSeq ws_x;
        Ice::FloatSeq ws_y;
        Ice::FloatSeq ws_z;

        bool ignoreWSLimitChecks = false;

        float adaptCoeff;
        float reactThreshold;
        float dragForceDeadZone;
        float adaptForceCoeff;
        float changePositionTolerance;
        float changeTimerThreshold;
    };


    interface NJointPeriodicTSDMPCompliantControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnDMPFromTrajectory(TrajectoryBase trajectory);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);

        void setSpeed(double times);
        void setGoals(Ice::DoubleSeq goals);
        void setAmplitude(double amplitude);
        void setTargetForceInRootFrame(float force);

        double getCanVal();
    };

    class NJointAdaptiveWipingControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        double dmpAmplitude = 1;
        double timeDuration = 10;

        double phaseL = 10;
        double phaseK = 10;
        float phaseDist0 = 50;
        float phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 100;


        // velocity controller configuration
        string nodeSetName = "";

        float maxJointTorque;
        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;

        Ice::FloatSeq desiredNullSpaceJointValues;
        Ice::FloatSeq Knull;
        Ice::FloatSeq Dnull;

        string forceSensorName = "FT R";
        string forceFrameName = "ArmR8_Wri2";
        float forceFilter = 0.8;
        float waitTimeForCalibration = 0.1;
        float Kpf;

        float minimumReactForce = 0;

        float forceDeadZone;
        float velFilter;

        float maxLinearVel;
        float maxAngularVel;

        Ice::FloatSeq ws_x;
        Ice::FloatSeq ws_y;
        Ice::FloatSeq ws_z;

        float adaptCoeff;
        float reactThreshold;
        float dragForceDeadZone;
        float adaptForceCoeff;
        float changePositionTolerance;
        float changeTimerThreshold;

        Ice::FloatSeq ftOffset;
        Ice::FloatSeq handCOM;
        float handMass;
        float angularKp;
        float frictionCone;
    };


    interface NJointAdaptiveWipingControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnDMPFromTrajectory(TrajectoryBase trajectory);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);

        void setSpeed(double times);
        void setGoals(Ice::DoubleSeq goals);
        void setAmplitude(double amplitude);
        void setTargetForceInRootFrame(float force);

        double getCanVal();
    };

    class NJointAnomalyDetectionAdaptiveWipingControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        double dmpAmplitude = 1;
        double timeDuration = 10;

        double phaseL = 10;
        double phaseK = 10;
        float phaseDist0 = 50;
        float phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 100;


        // velocity controller configuration
        string nodeSetName = "";

        float maxJointTorque;
        Ice::FloatSeq desiredNullSpaceJointValues;

        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;
        Ice::FloatSeq Knull;
        Ice::FloatSeq Dnull;

        string forceSensorName = "FT R";
        string forceFrameName = "ArmR8_Wri2";
        float forceFilter = 0.8;
        float waitTimeForCalibration = 0.1;

        // anomaly detection and friction estimation
        int velocityHorizon = 100;
        int frictionHorizon = 100;

        // pid params
        bool isForceCtrlInForceDir;
        bool isForceControlEnabled;
        bool isRotControlEnabled;
        bool isTorqueControlEnabled;
        bool isLCRControlEnabled;
        Ice::FloatSeq pidForce;
        Ice::FloatSeq pidRot;
        Ice::FloatSeq pidTorque;
        Ice::FloatSeq pidLCR;

        float minimumReactForce = 0;
        float forceDeadZone;
        float velFilter;

        float maxLinearVel;
        float maxAngularVel;

        Ice::FloatSeq ws_x;
        Ice::FloatSeq ws_y;
        Ice::FloatSeq ws_z;

        float adaptCoeff;
        float reactThreshold;
        float dragForceDeadZone;
        float adaptForceCoeff;
        float changePositionTolerance;
        float changeTimerThreshold;

        Ice::FloatSeq ftOffset;
        Ice::FloatSeq handCOM;
        float handMass;

        float ftCommandFilter;
        float frictionCone;
        float fricEstiFilter;
        float velNormThreshold;
        float maxInteractionForce;

        float increaseKpForceCoeff;
        float increaseKpRotCoeff;
        float decreaseKpForceCoeff;
        float decreaseKpRotCoeff;

        float adaptRateIncrease;
        float adaptRateDecrease;
        float adaptRateDecreaseRot;
        float adaptCoeffKdImpIncrease;

        float increaseKpOriCoeff;
        float increaseKdOriCoeff;
        float increaseKpNullCoeff;
        float increaseKdNullCoeff;

        bool isAdaptOriImpEnabled;
        float loseContactForceIntThreshold;
        bool loseContactDetectionEnabled;
        int loseContactCounterMax;
        float rotAngleSigmoid;
        bool useDMPInGlobalFrame;
    };


    interface NJointAnomalyDetectionAdaptiveWipingControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnDMPFromTrajectory(TrajectoryBase trajectory);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);

        void setSpeed(double times);
        void setGoals(Ice::DoubleSeq goals);
        void setAmplitude(double amplitude);
        void setTargetForceInRootFrame(float force);

        double getCanVal();

        Ice::FloatSeq getAnomalyInput();
        Ice::FloatSeq getAnomalyOutput();

        void setTrigerAbnormalEvent(bool abnormal);

        void pauseDMP();
        void resumeDMP();
    };
};

