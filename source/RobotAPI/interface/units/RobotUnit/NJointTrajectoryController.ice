/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>

module armarx
{
    class NJointTrajectoryControllerConfig extends NJointControllerConfig
    {
        float maxAcceleration = 1.5;
        float maxVelocity = 0.5;
        float maxDeviation = 0.01;
        float preSamplingStepMs = 50;
        Ice::StringSeq jointNames;
        float PID_p = 1;
        float PID_i = 0;
        float PID_d = 0;
        bool considerConstraints = true;
        bool isPreview = false;
    };


    interface NJointTrajectoryControllerInterface extends NJointControllerInterface
    {
        void setTrajectory(TrajectoryBase traj);
        bool isFinished();
        double getCurrentTimestamp();
        float getCurrentProgressFraction();
    };
};
