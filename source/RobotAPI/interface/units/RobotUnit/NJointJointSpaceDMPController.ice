/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>

module armarx
{
    class NJointJointSpaceDMPControllerConfig extends NJointControllerConfig
    {
        Ice::StringSeq jointNames;
        int kernelSize = 100;
        int baseMode = 1;

        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 0.3;
        double phaseDist1 = 0.1;
        double phaseKp = 1;

        double timeDuration = 10;
        double maxJointVel = 10;
        bool isPhaseStop = false;
    };

    interface NJointJointSpaceDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);
        void setSpeed(double times);
        void showMessages();
        string getDMPAsString();
        Ice::DoubleSeq createDMPFromString(string dmpString);
        void setViaPoints(double canVal, double point);

        void setMPWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPWeights();
    };

};
