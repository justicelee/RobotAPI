/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Markus Grotz <markus dot grotz at kit dot edu>
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/core/RobotState.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Matrix.ice>
#include <ArmarXCore/interface/observers/Timestamp.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>



module armarx
{
	/**
	* Struct IMUData with which IMU sensor data is represented. It incorporates following entries:
	* @param orientationQuaternion Orientation in quaternion.
	* @param magneticRotation Magnetic rotation.
	* @param gyroscopeRotation Rotation of gyroscope.
	* @param acceleration Acceleration of IMU sensor device.
	**/
    struct IMUData {
        Ice::FloatSeq orientationQuaternion;
        Ice::FloatSeq magneticRotation;
        Ice::FloatSeq gyroscopeRotation; 
        Ice::FloatSeq acceleration;
    };
	/**
	* Implements an interface to an InertialMeasurementUnit.
	**/
    interface InertialMeasurementUnitInterface extends armarx::SensorActorUnitInterface
    {
    };
	/**
	* Implements an interface to an InertialMeasurementUnitListener.
	**/
    interface InertialMeasurementUnitListener
    {	
		/**
		* reportSensorValues reports the IMU sensor values from a given sensor device.
		* @param device Name of IMU sensor device.
		* @param values IMU sensor data.
		* @param timestamp Timestamp of the measurement.
		**/
        void reportSensorValues(string device, string name, IMUData values, TimestampBase timestamp);
    };
	/**
	* Implements an interface to an InertialMeasurementUnitObserver.
	**/
    interface InertialMeasurementUnitObserverInterface extends ObserverInterface, InertialMeasurementUnitListener
    {
    };

};

