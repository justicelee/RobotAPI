/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Core
 * @author     Christian Boege (boege at kit dot edu)
 * @copyright  2011 Christian Boege
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/KinematicUnitInterface.ice>

#include <ArmarXCore/interface/core/UserException.ice>

module armarx
{
    enum HandSelection{
        eNone,
        eLeftHand,
        eRightHand
    };

    interface TCPMoverUnitInterface
    {
//        void moveTCPRelative(bool leftHand, float x, float y, float z, float speedFactor);
        /**
         * Resets the joint angles of the arm to the home position.
         * NO MOVEMENT! Just sets the angles, so not suitable for a real robot.
         * @param selectedHand Select which hand to move to home position.
         */
        void resetArmToHomePosition(HandSelection selectedHand);
        /**
         *  Request control over the robot.
         */
        void request();

        /**
         * Release control over the robot.
         */
        void release();

        /**
         * Set the velocities of one hand.
         * The velocity values should be currently between 0-30.
         * @param selectedHand Select which hand to move.
         * @param x The velocity on the x-axis.
         * @param y The velocity on the y-axis.
         * @param z The velocity on the z-axis.
         * @param speedFactor A factor by which the x,y,z values should be multiplied.
         */
        void setCartesianTCPVelocity(HandSelection selectedHand, float x, float y, float z, float speedFactor);
        //! angles in radiant

        /**
         * Sets the orientation of one hand.<br/>
         * All angles are in radiant.
         * @param selectedHand Select which hand to move.
         * @param alpha
         * @param beta
         * @param gamma
         *
         */
        void setTCPOrientation(HandSelection selectedHand, float alpha, float beta, float gamma);

        /**
         * Let the robot look to a specific point in space. Coordinates in platform coordinates.<br/>
         * Not yet implemented.
         */
        void lookWithHeadTo(float x, float y, float z);

        /**
         * Not yet implemented.
         * @param x
         * @param y
         * @param theta
         */
        void setPlatformVelocities(float x, float y, float theta);

        /**
         * @brief setHandConfiguration
         * @param configValue 		eOPEN     		= 1,
        ePRESHAPE 		= 2,
        eGRASP    		= 4,
        eTHREE_FINGER_GRASP	= 5,
        eMOVE_THUMB 		= 7,
        ePRESHAPE_FINGERS 	= 8,
        eCLOSE_FINGERS 		= 9
         */
        void setHandConfiguration(HandSelection selectedHand, int configValue);

        void stopArm(HandSelection selectedHand);
        void stopHead();
        void stopPlatform();
    };
};

