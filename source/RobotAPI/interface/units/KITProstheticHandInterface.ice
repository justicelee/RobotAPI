/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Julia Starke <julia dot starke at kit dot edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

module KITProsthesis
{
    module ProsthesisState
    {
        //the enclosing namespace scopes the c-style enum values
        enum State
        {
            Created,
            DiscoveringDevices,
            DiscoveringDevicesDone,
            Disconnected,
            Connecting,
            ConnectingDone,
            DiscoveringServices,
            DiscoveringServicesDone,
            ConnectingService,
            Running,
            Killed
        };
    };

    struct ProsthesisMotorValues
    {
        long v;
        long maxPWM;
        long pos;
    };

    struct ProsthesisSensorValues
    {
        ProsthesisState::State state;

        long thumbPWM;
        long thumbPos;

        long fingerPWM;
        long fingerPos;

        //add IMU here
    };

    interface KITProstheticHandInterface
    {
        void sendGrasp(int n);
        void sendThumbPWM(ProsthesisMotorValues motorValues);
        void sendFingerPWM(ProsthesisMotorValues motorValues);

        ProsthesisSensorValues getSensorValues();
    };
};
