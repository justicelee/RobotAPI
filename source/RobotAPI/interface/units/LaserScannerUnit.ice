/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/core/RobotState.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Matrix.ice>
#include <ArmarXCore/interface/observers/Timestamp.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>



module armarx
{
	/**
    * Struct LaserScanStep with which a single scan step is represented. It incorporates following entries:
    * @param angle Angle in which direction a distance was measured [rad].
    * @param distance The measured distance [mm].
	**/
    struct LaserScanStep
    {
        float angle;
        float distance;
    };

    struct LaserScannerInfo
    {
        string device;
        string frame;
        float minAngle;
        float maxAngle;
        float stepSize;
    };

    sequence<LaserScanStep> LaserScan;
    sequence<LaserScannerInfo> LaserScannerInfoSeq;

    interface LaserScannerUnitInterface extends armarx::SensorActorUnitInterface
    {
        ["cpp:const"]
        idempotent string getReportTopicName() throws NotInitializedException;

        ["cpp:const"]
        LaserScannerInfoSeq getConnectedDevices();
    };

    interface LaserScannerUnitListener
    {	
        void reportSensorValues(string device, string name, LaserScan values, TimestampBase timestamp);
    };

    interface LaserScannerUnitObserverInterface extends ObserverInterface, LaserScannerUnitListener
    {
    };

};

