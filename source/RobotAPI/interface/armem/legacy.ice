#pragma once

#include <RobotAPI/interface/aron.ice>


module armarx
{
    module armem
    {
        // data in a commit
        class ArMemCommit
        {
            string producer;
            string stored_in_segment;
            long produce_timestamp_ms;
            long storage_timestamp_ms;
            int priority;
            aron::data::AronDataList data; // Dict?!?
        };

        // map storageTS to commit
        dictionary<long, ArMemCommit> TimestampedArMemCommitList;


        interface ArMemReceiverInterface
        {
            ArMemCommit getLatestCommitFromSegment(string segment);
            ArMemCommit getNextCommitFromSegmentForTimestamp(string segment, long timestamp);
            TimestampedArMemCommitList getAllCommitsBetweenTimestampsFromSegment(string segment, long timestamp1, long timestamp2);
            TimestampedArMemCommitList getAllCommitsFromSegment(string segment);
        };

        interface ArMemProducerInterface
        {
            string commit(string segment, long produceTimestamp, aron::data::AronDataList values);
            string commit_single(string segment, long produceTimestamp, aron::data::AronData value);
        };

        interface ArMemLocalMemoryInterface
                extends ArMemReceiverInterface, ArMemProducerInterface
        {

        };

        interface ArMemGlobalMemoryResolver
        {
            string getHostnameOfCurrentMachine();
            ArMemLocalMemoryInterface* getMemoryOfCurrentMachine();
            ArMemLocalMemoryInterface* getMemoryForHostname(string hostname);

            void dynamicallyRegisterNewLocalMemory(string hostname);

            void exportDataOfAllMemoriesToLocation(string location);
        }

    };
};
