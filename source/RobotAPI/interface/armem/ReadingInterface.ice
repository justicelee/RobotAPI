#pragma once

#include <RobotAPI/interface/aron.ice>


module armarx
{
    module armem
    {
        // WIP

        module data
        {
            struct EntitySnapshotQuery
            {
                /**
                 * @brief The ID of either an entity snapshot or an entity.
                 * If an entity ID, this refers to the newest snapshot.
                 */
                string snapshotID;
            };
            sequence<EntitySnapshotQuery> EntitySnapshotQueryList;
            struct EntitySnapshotQueryResult
            {
                bool success;
                string errorMessage;

                long timeCreatedMicroSeconds;
                aron::data::AronDataList instances;
            };
            sequence<EntitySnapshotQueryResult> EntitySnapshotQueryResultList;
        }


        interface ReadingInterface
        {
            data::EntitySnapshotQueryResultList getEntitySnapshots(data::EntitySnapshotQueryList queries);
        };

    };
};
