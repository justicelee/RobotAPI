#pragma once

#include <RobotAPI/interface/armem/MemoryInterface.ice>


module armarx
{
    module armem
    {
        module data
        {
            struct RegisterMemoryInput
            {
                string name;
                MemoryInterface* proxy;

                bool existOk = true;
            };
            struct RegisterMemoryResult
            {
                bool success;
                string errorMessage;
            };

            struct RemoveMemoryInput
            {
                string name;
                bool notExistOk = true;
            };
            struct RemoveMemoryResult
            {
                bool success;
                string errorMessage;
            };

            struct ResolveMemoryNameInput
            {
                string name;
            };
            struct ResolveMemoryNameResult
            {
                bool success;
                string errorMessage;

                MemoryInterface* proxy;
            };

            struct WaitForMemoryInput
            {
                /// The memory name.
                string name;
                /// Negative for no timeout.
                long timeoutMilliSeconds = -1;
            };
            struct WaitForMemoryResult
            {
                bool success;
                string errorMessage;

                MemoryInterface* proxy;
            };
        }


        interface MemoryNameSystemInterface
        {
            data::RegisterMemoryResult registerMemory(data::RegisterMemoryInput input);
            data::RemoveMemoryResult removeMemory(data::RemoveMemoryInput input);

            data::ResolveMemoryNameResult resolveMemoryName(data::ResolveMemoryNameInput input);
            data::WaitForMemoryResult waitForMemory(data::WaitForMemoryInput input);
        };

    };
};
