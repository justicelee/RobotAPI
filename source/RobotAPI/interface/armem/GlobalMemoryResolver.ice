#pragma once

#include <RobotAPI/interface/armem/MemoryInterface.ice>


module armarx
{
    module armem
    {

        interface GlobalMemoryResolver
        {
            string getHostnameOfCurrentMachine();
            MemoryInterface* getMemoryOfCurrentMachine();
            MemoryInterface* getMemoryForHostname(string hostname);

            void dynamicallyRegisterNewLocalMemory(string hostname);

            void exportDataOfAllMemoriesToLocation(string location);
        }

    };
};
