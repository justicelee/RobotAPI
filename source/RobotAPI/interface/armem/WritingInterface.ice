#pragma once

#include <RobotAPI/interface/aron.ice>


module armarx
{
    module armem
    {
        module data
        {
            struct AddSegmentInput
            {
                string coreSegmentName;
                string providerSegmentName;

                bool clearWhenExists = false;
            };
            sequence<AddSegmentInput> AddSegmentsInput;

            struct AddSegmentResult
            {
                bool success = false;
                string segmentID;

                string errorMessage;
            };
            sequence<AddSegmentResult> AddSegmentsResult;


            struct EntityUpdate
            {
                string entityID;
                aron::data::AronDataList instancesData;
                long timeCreatedMicroSeconds;

                float confidence = 1.0;
                long timeSentMicroSeconds = -1;
            };
            sequence<EntityUpdate> EntityUpdateList;

            struct EntityUpdateResult
            {
                bool success = false;

                string snapshotID;
                long timeArrivedMicroSeconds;

                string errorMessage;
            };
            sequence<EntityUpdateResult> EntityUpdateResultList;

            struct Commit
            {
                EntityUpdateList updates;
            };
            struct CommitResult
            {
                EntityUpdateResultList results;
            };
        }


        interface WritingInterface
        {
            /// Register multiple core or provider segments.
            data::AddSegmentsResult addSegments(data::AddSegmentsInput input);
            // void removeSegments();

            /// Commit data to the memory.
            data::CommitResult commit(data::Commit commit);
        };

    };
};
