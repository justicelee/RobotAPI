#pragma once

#include <RobotAPI/interface/armem/ReadingInterface.ice>
#include <RobotAPI/interface/armem/WritingInterface.ice>


module armarx
{
    module armem
    {

        interface MemoryInterface extends ReadingInterface, WritingInterface
        {
        };

    };
};
