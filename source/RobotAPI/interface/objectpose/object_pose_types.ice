/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotAPI
* @author     Rainer Kartmann
* @copyright  2020 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/ArmarXObjects/ArmarXObjectsTypes.ice>


module armarx
{
    // A struct's name cannot cannot differ only in capitalization from its immediately enclosing module name.
    module objpose
    {
        enum ObjectTypeEnum
        {
            AnyObject, KnownObject, UnknownObject
        };

        class AABB
        {
            Vector3Base center;
            Vector3Base extents;
        };

        /**
         * @brief A 3D box.
         * Box is a a class to allow it being null.
         */
        class Box
        {
            Vector3Base position;
            QuaternionBase orientation;
            Vector3Base extents;
        };


        module data
        {
            /// An object pose provided by an ObjectPoseProvider.
            struct ProvidedObjectPose
            {
                /// Name of the providing component.
                string providerName;
                /// Known or unknown object.
                ObjectTypeEnum objectType = AnyObject;

                /// The object ID, i.e. dataset and name.
                armarx::data::ObjectID objectID;

                /// Pose in `objectPoseFrame`.
                PoseBase objectPose;
                string objectPoseFrame;

                /// Confidence in [0, 1] (1 = full, 0 = none).
                float confidence = 0;
                /// Source timestamp.
                long timestampMicroSeconds = -1;

                /// [Optional] Object bounding box in object's local coordinate frame.
                Box localOOBB;
            };
            sequence<ProvidedObjectPose> ProvidedObjectPoseSeq;


            class ObjectAttachmentInfo;

            /// An object pose as stored by the ObjectPoseObserver.
            struct ObjectPose
            {
                /// Name of the providing component.
                string providerName;
                /// Known or unknown object.
                ObjectTypeEnum objectType = AnyObject;

                /// The object ID, i.e. dataset and name.
                armarx::data::ObjectID objectID;

                PoseBase objectPoseRobot;
                PoseBase objectPoseGlobal;
                PoseBase objectPoseOriginal;
                string objectPoseOriginalFrame;

                StringFloatDictionary robotConfig;
                PoseBase robotPose;

                ObjectAttachmentInfo attachment;

                /// Confidence in [0, 1] (1 = full, 0 = none).
                float confidence = 0;
                /// Source timestamp.
                long timestampMicroSeconds = -1;

                /// Object bounding box in object's local coordinate frame. May be null.
                Box localOOBB;
            };
            sequence<ObjectPose> ObjectPoseSeq;


            class ObjectAttachmentInfo
            {
                string frameName;
                string agentName;

                PoseBase poseInFrame;
            };
        }

    };
};

