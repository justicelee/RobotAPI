/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::StatechartExecutionGroup::StatechartExecutionGroupRemoteStateOfferer
 * @author     Stefan Reither ( stefan dot reither at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StatechartExecutionGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace StatechartExecutionGroup;

// DO NOT EDIT NEXT LINE
StatechartExecutionGroupRemoteStateOfferer::SubClassRegistry StatechartExecutionGroupRemoteStateOfferer::Registry(StatechartExecutionGroupRemoteStateOfferer::GetName(), &StatechartExecutionGroupRemoteStateOfferer::CreateInstance);



StatechartExecutionGroupRemoteStateOfferer::StatechartExecutionGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < StatechartExecutionGroupStatechartContext > (reader)
{
}

void StatechartExecutionGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void StatechartExecutionGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void StatechartExecutionGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string StatechartExecutionGroupRemoteStateOfferer::GetName()
{
    return "StatechartExecutionGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr StatechartExecutionGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new StatechartExecutionGroupRemoteStateOfferer(reader));
}



