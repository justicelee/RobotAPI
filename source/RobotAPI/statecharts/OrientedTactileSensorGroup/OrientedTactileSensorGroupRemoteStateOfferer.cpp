/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::OrientedTactileSensorGroup::OrientedTactileSensorGroupRemoteStateOfferer
 * @author     andreeatulbure ( andreea_tulbure at yahoo dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "OrientedTactileSensorGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace OrientedTactileSensorGroup;

// DO NOT EDIT NEXT LINE
OrientedTactileSensorGroupRemoteStateOfferer::SubClassRegistry OrientedTactileSensorGroupRemoteStateOfferer::Registry(OrientedTactileSensorGroupRemoteStateOfferer::GetName(), &OrientedTactileSensorGroupRemoteStateOfferer::CreateInstance);



OrientedTactileSensorGroupRemoteStateOfferer::OrientedTactileSensorGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < OrientedTactileSensorGroupStatechartContext > (reader)
{
}

void OrientedTactileSensorGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void OrientedTactileSensorGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void OrientedTactileSensorGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string OrientedTactileSensorGroupRemoteStateOfferer::GetName()
{
    return "OrientedTactileSensorGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr OrientedTactileSensorGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new OrientedTactileSensorGroupRemoteStateOfferer(reader));
}



