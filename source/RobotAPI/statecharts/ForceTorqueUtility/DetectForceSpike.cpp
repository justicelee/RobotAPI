/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ForceTorqueUtility
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <thread>

#include "DetectForceSpike.h"

#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace ForceTorqueUtility;

// DO NOT EDIT NEXT LINE
DetectForceSpike::SubClassRegistry DetectForceSpike::Registry(DetectForceSpike::GetName(), &DetectForceSpike::CreateInstance);



void DetectForceSpike::run()
{
    ARMARX_CHECK_EXPRESSION(in.getTriggerInAxisDirection() || in.getTriggerCounterAxisDirection());
    ARMARX_CHECK_GREATER_EQUAL(in.getWindowSizeMs(), 10);
    ARMARX_CHECK_GREATER_EQUAL(in.getWindowSizeMs(), 10);

    const float forceThreshold = in.getForceThreshold();
    ARMARX_CHECK_GREATER(forceThreshold, 0);

    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(in.getFTDatafieldName()));
    const Eigen::Vector3f weights = in.getForceWeights()->toEigen();
    const Eigen::Vector3f axis = in.getAxis()->toEigen().normalized();
    auto getForceAlongAxis = [&]
    {
        return forceDf->getDataField()->get<FramedDirection>()->toEigen().cwiseProduct(weights).transpose() * axis;
    };

    in.getTriggerInAxisDirection();
    in.getTriggerCounterAxisDirection();
    std::deque<float> spikes(in.getWindowSizeMs() / 10, getForceAlongAxis());

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        spikes.push_back(getForceAlongAxis());
        spikes.pop_front();

        float refValue = spikes.at(0);
        bool low = true;
        bool risingEdgeDetected = false;
        bool fallingEdgeDetected = false;

        bool f2rDetected = false;
        bool r2fDetected = false;

        for (const float spike : spikes)
        {
            if (low)
            {
                if (spike < refValue)
                {
                    refValue = spike;
                }
                else if (spike > refValue + forceThreshold)
                {
                    low = false;
                    risingEdgeDetected = true;
                    f2rDetected |= fallingEdgeDetected;
                }
            }

            if (!low)
            {
                if (spike > refValue)
                {
                    refValue = spike;
                }
                else if (spike < refValue - forceThreshold)
                {
                    low = true;
                    fallingEdgeDetected = true;
                    r2fDetected |= risingEdgeDetected;
                }
            }
        }
        if ((in.getTriggerInAxisDirection() && r2fDetected) || (in.getTriggerCounterAxisDirection() && f2rDetected))
        {
            emitSuccess();
            return;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds{10});
    }
    emitFailure();
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DetectForceSpike::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DetectForceSpike(stateData));
}

