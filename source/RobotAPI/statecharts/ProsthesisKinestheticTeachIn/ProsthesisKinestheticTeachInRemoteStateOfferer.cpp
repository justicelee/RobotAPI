/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ProsthesisKinestheticTeachIn::ProsthesisKinestheticTeachInRemoteStateOfferer
 * @author     Julia Starke ( julia dot starke at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ProsthesisKinestheticTeachInRemoteStateOfferer.h"

using namespace armarx;
using namespace ProsthesisKinestheticTeachIn;

// DO NOT EDIT NEXT LINE
ProsthesisKinestheticTeachInRemoteStateOfferer::SubClassRegistry ProsthesisKinestheticTeachInRemoteStateOfferer::Registry(ProsthesisKinestheticTeachInRemoteStateOfferer::GetName(), &ProsthesisKinestheticTeachInRemoteStateOfferer::CreateInstance);



ProsthesisKinestheticTeachInRemoteStateOfferer::ProsthesisKinestheticTeachInRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < ProsthesisKinestheticTeachInStatechartContext > (reader)
{
}

void ProsthesisKinestheticTeachInRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void ProsthesisKinestheticTeachInRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void ProsthesisKinestheticTeachInRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string ProsthesisKinestheticTeachInRemoteStateOfferer::GetName()
{
    return "ProsthesisKinestheticTeachInRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr ProsthesisKinestheticTeachInRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new ProsthesisKinestheticTeachInRemoteStateOfferer(reader));
}



