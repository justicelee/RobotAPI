/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::TrajectoryExecutionCode
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/statecharts/TrajectoryExecutionCode/PlayTrajectory.generated.h>

namespace armarx::TrajectoryExecutionCode
{
    class PlayTrajectory :
        public PlayTrajectoryGeneratedBase < PlayTrajectory >
    {
    public:
        PlayTrajectory(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < PlayTrajectory > (stateData), PlayTrajectoryGeneratedBase < PlayTrajectory > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        // void run();
        // void onBreak();
        void onExit() override;

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
    };
}


