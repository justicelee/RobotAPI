/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::StatechartProfilesTestGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestState.h"

using namespace armarx;
using namespace StatechartProfilesTestGroup;

// DO NOT EDIT NEXT LINE
TestState::SubClassRegistry TestState::Registry(TestState::GetName(), &TestState::CreateInstance);



TestState::TestState(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<TestState>(stateData),  TestStateGeneratedBase<TestState>(stateData)
{
}

void TestState::onEnter()
{
    std::string emptyString = in.getEmptyStringTest();
    std::string TestParam1 = in.getTestParam1();
    std::string TestParam2 = in.getTestParam2();
    std::string TestParam3 = in.getTestParam3();

    ARMARX_IMPORTANT << "TestParam1: " << TestParam1;
    ARMARX_IMPORTANT << "TestParam2: " << TestParam2;
    ARMARX_IMPORTANT << "TestParam3: " << TestParam3;

    ARMARX_CHECK_EXPRESSION(emptyString == "") << "EmptyStringTest is not empty.";
    ARMARX_CHECK_EXPRESSION(TestParam1 == "OnlyRootSet");
    ARMARX_CHECK_EXPRESSION(TestParam2 == "Armar3BaseSet");
    ARMARX_CHECK_EXPRESSION(TestParam3 == "Armar3aSet" || TestParam3 == "Armar3SimulationSet");
    ARMARX_IMPORTANT << "All tests passed.";
}

void TestState::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}

void TestState::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void TestState::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestState::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestState(stateData));
}

