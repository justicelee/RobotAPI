/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::StatechartProfilesTestGroup::StatechartProfilesTestGroupRemoteStateOfferer
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StatechartProfilesTestGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace StatechartProfilesTestGroup;

// DO NOT EDIT NEXT LINE
StatechartProfilesTestGroupRemoteStateOfferer::SubClassRegistry StatechartProfilesTestGroupRemoteStateOfferer::Registry(StatechartProfilesTestGroupRemoteStateOfferer::GetName(), &StatechartProfilesTestGroupRemoteStateOfferer::CreateInstance);



StatechartProfilesTestGroupRemoteStateOfferer::StatechartProfilesTestGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < StatechartProfilesTestGroupStatechartContext > (reader)
{
}

void StatechartProfilesTestGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void StatechartProfilesTestGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void StatechartProfilesTestGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string StatechartProfilesTestGroupRemoteStateOfferer::GetName()
{
    return "StatechartProfilesTestGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr StatechartProfilesTestGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new StatechartProfilesTestGroupRemoteStateOfferer(reader));
}



