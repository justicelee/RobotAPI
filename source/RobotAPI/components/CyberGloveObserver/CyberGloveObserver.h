/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::CyberGloveObserver
 * @author     JuliaStarke ( julia dot starke at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <RobotAPI/interface/units/CyberGloveObserverInterface.h>
#include <ArmarXCore/observers/Observer.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <mutex>

namespace armarx
{
    /**
     * @class CyberGloveObserverPropertyDefinitions
     * @brief
     */
    class CyberGloveObserverPropertyDefinitions:
        public armarx::ObserverPropertyDefinitions
    {
    public:
        CyberGloveObserverPropertyDefinitions(std::string prefix):
            armarx::ObserverPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            defineOptionalProperty<std::string>("CyberGloveTopicName", "CyberGloveValues", "Name of the CyberGlove Topic");
        }
    };

    /**
     * @defgroup Component-CyberGloveObserver CyberGloveObserver
     * @ingroup RobotAPI-Components
     * A description of the component CyberGloveObserver.
     *
     * @class CyberGloveObserver
     * @ingroup Component-CyberGloveObserver
     * @brief Brief description of class CyberGloveObserver.
     *
     * Detailed description of class CyberGloveObserver.
     */
    class CyberGloveObserver :
        virtual public Observer,
        virtual public CyberGloveObserverInterface
    {
    public:
        CyberGloveObserver() {}

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "CyberGloveObserver";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitObserver() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectObserver() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        //void onDisconnectObserver() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitObserver() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:

        std::mutex dataMutex;
        CyberGloveValues latestValues;
        IceUtil::Time lastUpdate;

        // CyberGloveListenerInterface interface
    public:
        void reportGloveValues(const CyberGloveValues& gloveValues, const Ice::Current&) override;
        CyberGloveValues getLatestValues(const Ice::Current&) override;

    };
}
