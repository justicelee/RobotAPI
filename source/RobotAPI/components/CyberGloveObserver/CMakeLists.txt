armarx_component_set_name("CyberGloveObserver")
set(COMPONENT_LIBS ArmarXCore RobotAPICore ArmarXCoreObservers)
set(SOURCES CyberGloveObserver.cpp)
set(HEADERS CyberGloveObserver.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

armarx_generate_and_add_component_executable(APPLICATION_APP_SUFFIX)
