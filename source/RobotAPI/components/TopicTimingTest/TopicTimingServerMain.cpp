#include <RobotAPI/components/TopicTimingTest/TopicTimingServer.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

int main(int argc, char* argv[])
{
    return armarx::runSimpleComponentApp < armarx::TopicTimingServer > (argc, argv, "TopicTimingServer");
}
