/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE RobotAPI::FramedPose::Test
#define ARMARX_BOOST_TEST
#include <RobotAPI/Test.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/components/RobotState/RobotStateComponent.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

using namespace armarx;

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
using namespace armarx;
class RobotStateComponentTestEnvironment
{
public:
    RobotStateComponentTestEnvironment(const std::string& testName, int registryPort = 11220, bool addObjects = true)
    {
        Ice::PropertiesPtr properties = Ice::createProperties();
        armarx::Application::LoadDefaultConfig(properties);
        CMakePackageFinder finder("RobotAPI");
        ArmarXDataPath::addDataPaths({finder.getDataDir()});
        // If you need to set properties (optional):
        properties->setProperty("ArmarX.RobotStateComponent.AgentName", "Armar3");
        properties->setProperty("ArmarX.RobotStateComponent.RobotNodeSetName", "Robot");
        properties->setProperty("ArmarX.RobotStateComponent.RobotFileName", "RobotAPI/robots/Armar3/ArmarIII.xml");

        // The IceTestHelper starts all required Ice processes
        _iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);
        _iceTestHelper->startEnvironment();

        // The manager allows you to create new ArmarX components
        _manager = new TestArmarXManager(testName, _iceTestHelper->getCommunicator(), properties);
        if (addObjects)
        {
            // This is how you create components.
            _rsc = _manager->createComponentAndRun<RobotStateComponent, RobotStateComponentInterfacePrx>("ArmarX", "RobotStateComponent");
        }
    }
    ~RobotStateComponentTestEnvironment()
    {
        _manager->shutdown();
    }
    // In your tests, you can access your component through this proxy
    RobotStateComponentInterfacePrx _rsc;
    TestArmarXManagerPtr _manager;
    IceTestHelperPtr _iceTestHelper;
};
using RobotStateComponentTestEnvironmentPtr = std::shared_ptr<RobotStateComponentTestEnvironment>;

BOOST_AUTO_TEST_CASE(RobotStateComponentHistoryTest)
//void func()
{
    RobotStateComponentTestEnvironment env("RSC");

    while (env._manager->getObjectState("RobotStateComponent") < eManagedIceObjectStarted)
    {
        ARMARX_INFO_S << "Waiting";
        usleep(100000);
    }

    const IceUtil::Time inT1 = IceUtil::Time::seconds(1);
    const IceUtil::Time inT2 = inT1 + IceUtil::Time::milliSeconds(110);

    NameValueMap joints;

    joints["Elbow L"] = 0;
    env._rsc->reportJointAngles(joints, inT1.toMicroSeconds(), true);
    auto t1 = env._rsc->getSynchronizedRobot()->getTimestamp()->timestamp;

    joints["Elbow L"] = 1;
    env._rsc->reportJointAngles(joints, inT2.toMicroSeconds(), true);
    auto t2 = env._rsc->getSynchronizedRobot()->getTimestamp()->timestamp;
    BOOST_CHECK(t2 > t1);

    auto config = env._rsc->getJointConfigAtTimestamp(t1);

    BOOST_REQUIRE_EQUAL(config.count("Elbow L"), 1);

    BOOST_CHECK_EQUAL(config.at("Elbow L"), 0);

    config = env._rsc->getJointConfigAtTimestamp(t2);
    BOOST_CHECK_EQUAL(config.at("Elbow L"), 1);

    config = env._rsc->getJointConfigAtTimestamp(t2 + 1); // future timestamp -> latest values should be returned
    BOOST_CHECK_EQUAL(config.at("Elbow L"), 1);

    config = env._rsc->getJointConfigAtTimestamp((t1 + t2) * 0.5); // interpolated values in the middle
    BOOST_CHECK_CLOSE(config.at("Elbow L"), 0.5f, 0.01);

    config = env._rsc->getJointConfigAtTimestamp(t1 + (t2 - t1) * 0.7); // interpolated values
    ARMARX_INFO_S << "value at t=0.7%: " << config.at("Elbow L");
    BOOST_CHECK_CLOSE(config.at("Elbow L"), 0.7f, 0.01);
}
