/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     David Schiebener <schiebener at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "InertialMeasurementUnitObserver.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

#include <RobotAPI/libraries/core/Pose.h>


namespace armarx
{
    void InertialMeasurementUnitObserver::onInitObserver()
    {
        usingTopic(getProperty<std::string>("IMUTopicName").getValue());

        offerConditionCheck("updated", new ConditionCheckUpdated());
        offerConditionCheck("larger", new ConditionCheckLarger());
        offerConditionCheck("equals", new ConditionCheckEquals());
        offerConditionCheck("smaller", new ConditionCheckSmaller());

        if (getProperty<bool>("EnableVisualization").getValue())
        {
            offeringTopic(getProperty<std::string>("DebugDrawerTopic").getValue());
        }
    }

    void InertialMeasurementUnitObserver::onConnectObserver()
    {
        if (getProperty<bool>("EnableVisualization").getValue())
        {
            debugDrawerPrx = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopic").getValue());
        }
    }

    void InertialMeasurementUnitObserver::onExitObserver()
    {
        if (getProperty<bool>("EnableVisualization").getValue())
        {
            debugDrawerPrx->removePoseVisu("IMU", "orientation");
            debugDrawerPrx->removeLineVisu("IMU", "acceleration");
        }
    }

    void InertialMeasurementUnitObserver::reportSensorValues(
        const std::string& device, const std::string& name, const IMUData& values,
        const TimestampBasePtr& timestamp, const Ice::Current&)
    {
        std::unique_lock lock(dataMutex);

        TimestampVariantPtr timestampPtr = TimestampVariantPtr::dynamicCast(timestamp);


        if (!existsChannel(device))
        {
            offerChannel(device, "IMU data");
        }

        offerOrUpdateDataField(device, "name", Variant(name), "Name of the IMU sensor");
        Vector3Ptr acceleration;
        QuaternionPtr orientationQuaternion;
        if (values.acceleration.size() > 0)
        {
            ARMARX_CHECK_EXPRESSION(values.acceleration.size() == 3);
            acceleration = new Vector3(values.acceleration.at(0), values.acceleration.at(1), values.acceleration.at(2));
            offerValue(device, "acceleration", acceleration);
        }
        if (values.gyroscopeRotation.size() > 0)
        {
            ARMARX_CHECK_EXPRESSION(values.gyroscopeRotation.size() == 3);
            Vector3Ptr gyroscopeRotation = new Vector3(values.gyroscopeRotation.at(0), values.gyroscopeRotation.at(1), values.gyroscopeRotation.at(2));
            offerValue(device, "gyroscopeRotation", gyroscopeRotation);
        }
        if (values.magneticRotation.size() > 0)
        {
            ARMARX_CHECK_EXPRESSION(values.magneticRotation.size() == 3);
            Vector3Ptr magneticRotation = new Vector3(values.magneticRotation.at(0), values.magneticRotation.at(1), values.magneticRotation.at(2));
            offerValue(device, "magneticRotation", magneticRotation);
        }
        if (values.orientationQuaternion.size() > 0)
        {
            ARMARX_CHECK_EXPRESSION(values.orientationQuaternion.size() == 4);
            orientationQuaternion =  new Quaternion(values.orientationQuaternion.at(0), values.orientationQuaternion.at(1), values.orientationQuaternion.at(2), values.orientationQuaternion.at(3));
            offerOrUpdateDataField(device, "orientationQuaternion", orientationQuaternion,  "orientation quaternion values");
        }
        offerOrUpdateDataField(device, "timestamp", timestampPtr, "Timestamp");

        updateChannel(device);



        if (orientationQuaternion && acceleration && getProperty<bool>("EnableVisualization").getValue())
        {
            Eigen::Vector3f zero;
            zero.setZero();

            DrawColor color;
            color.r = 1;
            color.g = 1;
            color.b = 0;
            color.a = 0.5;

            Eigen::Vector3f ac = acceleration->toEigen();
            ac *= 10;

            debugDrawerPrx->setLineVisu("IMU", "acceleration", new Vector3(), new Vector3(ac), 2.0f, color);

            PosePtr posePtr = new Pose(orientationQuaternion->toEigen(), zero);
            debugDrawerPrx->setPoseVisu("IMU", "orientation", posePtr);
            //        debugDrawerPrx->setBoxDebugLayerVisu("floor", new Pose(), new Vector3(5000, 5000, 1), DrawColor {0.7f, 0.7f, 0.7f, 1.0f});
        }
    }

    void InertialMeasurementUnitObserver::offerValue(const std::string& device, const std::string& fieldName, const Vector3Ptr& vec)
    {
        offerOrUpdateDataField(device, fieldName, vec, fieldName + " values");
        offerOrUpdateDataField(device, fieldName + "_x", vec->x, fieldName + "_x value");
        offerOrUpdateDataField(device, fieldName + "_y", vec->y, fieldName + "_y value");
        offerOrUpdateDataField(device, fieldName + "_z", vec->z, fieldName + "_z value");
    }

    PropertyDefinitionsPtr InertialMeasurementUnitObserver::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new InertialMeasurementUnitObserverPropertyDefinitions(getConfigIdentifier()));
    }
}
