/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SpeechObserver.h"
#include <ArmarXCore/util/json/JSONObject.h>

#include <Ice/ObjectAdapter.h>

using namespace armarx;

SpeechObserver::SpeechObserver()
{
}

void SpeechObserver::onInitObserver()
{
    usingTopic(getProperty<std::string>("TextToSpeechTopicName").getValue());
    usingTopic(getProperty<std::string>("TextToSpeechStateTopicName").getValue());


}

void SpeechObserver::onConnectObserver()
{
    offerChannel("TextToSpeech", "TextToSpeech channel");
    offerDataFieldWithDefault("TextToSpeech", "Text", Variant(""), "Current text");
    offerDataFieldWithDefault("TextToSpeech", "TextChangeCounter", Variant(reportTextCounter), "Counter for text updates");

    offerDataFieldWithDefault("TextToSpeech", "State", Variant(""), "Current TTS state");
    offerDataFieldWithDefault("TextToSpeech", "StateChangeCounter", Variant(reportStateCounter), "Counter for state updates");

    offerChannel("SpeechToText", "SpeechToText channel");
    offerDataFieldWithDefault("SpeechToText", "RecognizedText", Variant(std::string()), "Text recognized by the SpeechRecogntion");

    auto proxy = getObjectAdapter()->addWithUUID(new SpeechListenerImpl(this));
    getIceManager()->subscribeTopic(proxy, "Speech_Commands");
}

std::string SpeechObserver::SpeechStateToString(TextToSpeechStateType state)
{
    switch (state)
    {
        case eIdle:
            return "Idle";
        case eStartedSpeaking:
            return "StartedSpeaking";
        case eFinishedSpeaking:
            return "FinishedSpeaking";

        default:
            throw LocalException("Unsupported TextToSpeechStateType: ") << state;
    }
}

void SpeechObserver::reportState(TextToSpeechStateType state, const Ice::Current&)
{
    std::unique_lock lock(dataMutex);
    reportStateCounter++;
    setDataField("TextToSpeech", "State", Variant(SpeechStateToString(state)));
    setDataField("TextToSpeech", "StateChangeCounter", Variant(reportStateCounter));
    updateChannel("TextToSpeech");
}

void SpeechObserver::reportText(const std::string& text, const Ice::Current& c)
{
    std::unique_lock lock(dataMutex);
    reportTextCounter++;
    setDataField("TextToSpeech", "Text", Variant(text));
    setDataField("TextToSpeech", "TextChangeCounter", Variant(reportTextCounter));
    updateChannel("TextToSpeech");
}

void SpeechObserver::reportTextWithParams(const std::string& text, const Ice::StringSeq& params, const Ice::Current&)
{
    std::unique_lock lock(dataMutex);
    ARMARX_WARNING << "reportTextWithParams is not implemented";
}

SpeechListenerImpl::SpeechListenerImpl(SpeechObserver* obs) :
    obs(obs)
{

}


void armarx::SpeechListenerImpl::reportText(const std::string& text, const Ice::Current&)
{
    std::unique_lock lock(dataMutex);
    JSONObject json;
    json.fromString(text);
    obs->setDataField("SpeechToText", "RecognizedText", Variant(json.getString("text")));
}

void armarx::SpeechListenerImpl::reportTextWithParams(const std::string&, const Ice::StringSeq&, const Ice::Current&)
{
    std::unique_lock lock(dataMutex);
    ARMARX_WARNING << "reportTextWithParams is not implemented";
}
