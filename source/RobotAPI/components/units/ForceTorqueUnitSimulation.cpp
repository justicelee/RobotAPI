/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ForceTorqueUnitSimulation.h"

#include <ArmarXCore/core/util/StringHelpers.h>

#include <boost/algorithm/string.hpp>

namespace armarx
{
    void ForceTorqueUnitSimulation::onInitForceTorqueUnit()
    {
        int intervalMs = getProperty<int>("IntervalMs").getValue();

        sensorNamesList = Split(getProperty<std::string>("SensorNames").getValue(), ",");
        for (auto& sensor : sensorNamesList)
        {
            boost::trim(sensor);
        }
        //std::vector<std::string> sensorNamesList;
        //boost::split(sensorNamesList, sensorNames, boost::is_any_of(","));

        for (std::vector<std::string>::iterator s = sensorNamesList.begin(); s != sensorNamesList.end(); s++)
        {
            forces[*s] = new armarx::FramedDirection(Eigen::Vector3f(0, 0, 0), *s, getProperty<std::string>("AgentName").getValue());
            torques[*s] = new armarx::FramedDirection(Eigen::Vector3f(0, 0, 0), *s, getProperty<std::string>("AgentName").getValue());
        }

        ARMARX_VERBOSE << "Starting ForceTorqueUnitSimulation with " << intervalMs << " ms interval";
        simulationTask = new PeriodicTask<ForceTorqueUnitSimulation>(this, &ForceTorqueUnitSimulation::simulationFunction, intervalMs, false, "ForceTorqueUnitSimulation", false);
    }

    void ForceTorqueUnitSimulation::onStartForceTorqueUnit()
    {
        simulationTask->start();
    }


    void ForceTorqueUnitSimulation::onExitForceTorqueUnit()
    {
        simulationTask->stop();
    }


    void ForceTorqueUnitSimulation::simulationFunction()
    {

        for (auto& sensor : sensorNamesList)
        {
            listenerPrx->reportSensorValues(sensor, forces[sensor], torques[sensor]);
        }

        //listenerPrx->reportSensorValues(forces);
        //listenerPrx->reportSensorValues(torques);
    }

    void ForceTorqueUnitSimulation::setOffset(const FramedDirectionBasePtr& forceOffsets, const FramedDirectionBasePtr& torqueOffsets, const Ice::Current& c)
    {
        // Ignore
    }

    void ForceTorqueUnitSimulation::setToNull(const Ice::Current& c)
    {
        // Ignore
    }


    PropertyDefinitionsPtr ForceTorqueUnitSimulation::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new ForceTorqueUnitSimulationPropertyDefinitions(getConfigIdentifier()));
    }
}
