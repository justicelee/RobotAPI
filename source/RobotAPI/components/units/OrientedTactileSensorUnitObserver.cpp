/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "OrientedTactileSensorUnitObserver.h"


#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/observers/variant/TimestampVariant.h>



namespace armarx
{
    void OrientedTactileSensorUnitObserver::onInitObserver()
    {
        usingTopic(getProperty<std::string>("SensorTopicName").getValue());
        offeringTopic(getProperty<std::string>("DebugDrawerTopic").getValue());
    }



    void OrientedTactileSensorUnitObserver::onConnectObserver()
    {
        debugDrawerPrx = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopic").getValue());
    }


    void OrientedTactileSensorUnitObserver::onExitObserver()
    {
        //debugDrawerPrx->removePoseVisu("IMU", "orientation");
        //debugDrawerPrx->removeLineVisu("IMU", "acceleration");
    }

    void OrientedTactileSensorUnitObserver::reportSensorValues(int id, float pressure, float qw, float qx, float qy, float qz, float pressureRate, float rotationRate, float accelerationRate, float accelx, float accely, float accelz, const TimestampBasePtr& timestamp, const Ice::Current&)
    {
        std::unique_lock lock(dataMutex);
        TimestampVariantPtr timestampPtr = TimestampVariantPtr::dynamicCast(timestamp);

        std::stringstream ss;
        ss << "sensor" << id;
        std::string channelName = ss.str();

        if (!existsChannel(channelName))
        {
            offerChannel(channelName, "Sensor Data");
        }

        offerOrUpdateDataField(channelName, "pressure", Variant(pressure), "current pressure");
        QuaternionPtr orientationQuaternion =  new Quaternion(qw, qx, qy, qz);
        offerOrUpdateDataField(channelName, "orientation", orientationQuaternion, "current orientation");
        offerOrUpdateDataField(channelName, "rotationRate", Variant(rotationRate), "current rotationRate");
        offerOrUpdateDataField(channelName, "pressureRate", Variant(pressureRate), "current pressureRate");
        offerOrUpdateDataField(channelName, "accelerationRate", Variant(accelerationRate), "current accelerationRate");
        offerOrUpdateDataField(channelName, "linear acceleration", Variant(new Vector3(accelx, accely, accelz)), "current linear acceleration");
    }
    /* TODO: for integration with debug drawer
        updateChannel(device);

        Eigen::Vector3f zero;
        zero.setZero();

        DrawColor color;
        color.r = 1;
        color.g = 1;
        color.b = 0;
        color.a = 0.5;

        Eigen::Vector3f ac = acceleration->toEigen();
        ac *= 10;

        debugDrawerPrx->setLineVisu("IMU", "acceleration", new Vector3(), new Vector3(ac), 2.0f, color);

        PosePtr posePtr = new Pose(orientationQuaternion->toEigen(), zero);
        debugDrawerPrx->setPoseVisu("IMU", "orientation", posePtr);
        debugDrawerPrx->setBoxDebugLayerVisu("floor", new Pose(), new Vector3(5000, 5000, 1), DrawColor {0.7f, 0.7f, 0.7f, 1.0f});
    */


    PropertyDefinitionsPtr OrientedTactileSensorUnitObserver::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new OrientedTactileSensorUnitObserverPropertyDefinitions(getConfigIdentifier()));
    }
}
