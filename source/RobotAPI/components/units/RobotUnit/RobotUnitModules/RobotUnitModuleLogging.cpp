/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <regex>

#include <boost/iterator/transform_iterator.hpp>

#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/util/CPPUtility/Iterator.h>
#include <ArmarXCore/core/util/FileSystemPathBuilder.h>
#include <ArmarXCore/core/ArmarXManager.h>

#include "RobotUnitModuleLogging.h"

#include "RobotUnitModuleControlThreadDataBuffer.h"
#include "RobotUnitModuleDevices.h"

#include "../util/ControlThreadOutputBuffer.h"

#include <boost/algorithm/string/predicate.hpp>

namespace armarx::RobotUnitModule
{
    void Logging::addMarkerToRtLog(const RemoteReferenceCounterBasePtr& token, const std::string& marker, const Ice::Current&)
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        std::lock_guard<std::mutex> guard {rtLoggingMutex};
        if (!rtLoggingEntries.count(token->getId()))
        {
            throw InvalidArgumentException {"addMarkerToRtLog called for a nonexistent log"};
        }
        rtLoggingEntries.at(token->getId())->marker = marker;
    }

    RemoteReferenceCounterBasePtr Logging::startRtLogging(const std::string& formatString, const Ice::StringSeq& loggingNames, const Ice::Current&)
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        StringStringDictionary alias;
        for (const auto& name : loggingNames)
        {
            alias.emplace(name, "");
        }
        return startRtLoggingWithAliasNames(formatString, alias, Ice::emptyCurrent);
    }

    void Logging::stopRtLogging(const RemoteReferenceCounterBasePtr& token, const Ice::Current&)
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        std::lock_guard<std::mutex> guard {rtLoggingMutex};
        if (!rtLoggingEntries.count(token->getId()))
        {
            throw InvalidArgumentException {"stopRtLogging called for a nonexistent log"};
        }
        ARMARX_DEBUG_S << "RobotUnit: stop RtLogging for file " << rtLoggingEntries.at(token->getId())->filename;
        rtLoggingEntries.at(token->getId())->stopLogging = true;
    }

    Ice::StringSeq Logging::getLoggingNames(const Ice::Current&) const
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        Ice::StringSeq result;
        const auto getName = [](const auto & fieldData)
        {
            return fieldData.name;
        };
        for (const auto& data : sensorDeviceValueMetaData)
        {
            result.insert(
                result.end(),
                boost::make_transform_iterator(data.fields.begin(), getName),
                boost::make_transform_iterator(data.fields.end(), getName));
        }
        for (const auto& datas : controlDeviceValueMetaData)
        {
            for (const auto& data : datas)
            {
                result.insert(
                    result.end(),
                    boost::make_transform_iterator(data.fields.begin(), getName),
                    boost::make_transform_iterator(data.fields.end(), getName));
            }
        }
        return result;
    }

    RemoteReferenceCounterBasePtr Logging::startRtLoggingWithAliasNames(
        const std::string& formatString,
        const StringStringDictionary& aliasNames,
        const Ice::Current&)
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        FileSystemPathBuilder pb {formatString};
        std::lock_guard<std::mutex> guard {rtLoggingMutex};
        if (rtLoggingEntries.count(pb.getPath()))
        {
            throw InvalidArgumentException {"There already is a logger for the path '" + pb.getPath() + "'"};
        }
        rtLoggingEntries[pb.getPath()].reset(new CSVLoggingEntry());
        auto ptr = rtLoggingEntries[pb.getPath()];
        CSVLoggingEntry& e = *ptr;
        e.filename = pb.getPath();
        pb.createParentDirectories();
        e.stream.open(e.filename);
        if (!e.stream)
        {
            rtLoggingEntries.erase(pb.getPath());
            throw LogicError {"RtLogging could not open filestream for '" + pb.getPath() + "'"};
        }
        ARMARX_INFO << "Start logging to " << e.filename
                    << ". Names (pattern, replacement name): " << aliasNames;

        std::stringstream header;
        header << "marker;iteration;timestamp;TimeSinceLastIteration";
        auto logDev = [&](const std::string & dev)
        {
            ARMARX_TRACE_LITE;
            for (const auto& [key, value] : aliasNames)
            {
                if (MatchName(key, dev))
                {
                    header << ";" << (value.empty() ? dev : value);
                    return true;
                }
            }
            return false;
        };

        //get logged sensor device values
        {
            ARMARX_TRACE;
            e.loggedSensorDeviceValues.reserve(sensorDeviceValueMetaData.size());
            for (const auto& valData : sensorDeviceValueMetaData)
            {
                e.loggedSensorDeviceValues.emplace_back();
                auto& svfieldsFlags = e.loggedSensorDeviceValues.back(); //vv
                svfieldsFlags.reserve(valData.fields.size());
                for (const auto& field : valData.fields)
                {
                    svfieldsFlags.emplace_back(logDev(field.name));
                }
            }
        }
        //get logged control device values
        {
            ARMARX_TRACE;
            e.loggedControlDeviceValues.reserve(controlDeviceValueMetaData.size());
            for (const auto& datas : controlDeviceValueMetaData)
            {
                e.loggedControlDeviceValues.emplace_back();
                auto& deviceCtrlFlags = e.loggedControlDeviceValues.back(); //vv
                deviceCtrlFlags.reserve(datas.size());
                for (const auto& valData : datas)
                {
                    deviceCtrlFlags.emplace_back();
                    auto& ctrlFieldFlags = deviceCtrlFlags.back(); //v
                    ctrlFieldFlags.reserve(valData.fields.size());

                    for (const auto& field : valData.fields)
                    {
                        ctrlFieldFlags.emplace_back(logDev(field.name));
                    }
                }
            }
        }
        ARMARX_TRACE;

        //write header
        e.stream << header.str() << std::flush; // newline is written at the beginning of each log line
        //create and return handle
        auto block = getArmarXManager()->createRemoteReferenceCount(
                         [ptr]()
        {
            ARMARX_DEBUG_S << "RobotUnit: stop RtLogging for file " << ptr->filename;
            ptr->stopLogging = true;
        }, e.filename);
        auto counter = block->getReferenceCounter();
        block->activateCounting();
        ARMARX_DEBUG_S << "RobotUnit: start RtLogging for file " << ptr->filename;
        return counter;
    }

    void Logging::writeRecentIterationsToFile(const std::string& formatString, const Ice::Current&) const
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        std::lock_guard<std::mutex> guard {rtLoggingMutex};
        FileSystemPathBuilder pb {formatString};
        pb.createParentDirectories();
        std::ofstream outCSV {pb.getPath() + ".csv"};
        if (!outCSV)
        {
            throw LogicError {"writeRecentIterationsToFile could not open filestream for '" + pb.getPath() + ".csv'"};
        }
        std::ofstream outMsg {pb.getPath() + ".messages"};
        if (!outMsg)
        {
            throw LogicError {"writeRecentIterationsToFile could not open filestream for '" + pb.getPath() + ".messages'"};
        }
        ARMARX_INFO << "writing the last " << backlog.size() << " iterations to " << pb.getPath() << ".{csv, messages}";
        //write csv header
        {
            outCSV << "iteration;timestamp";
            for (const auto& vs : sensorDeviceValueMetaData)
            {
                for (const auto& f : vs.fields)
                {
                    outCSV << ";" << f.name;
                }
            }
            for (const auto& vvs : controlDeviceValueMetaData)
            {
                for (const auto& vs : vvs)
                {
                    for (const auto& f : vs.fields)
                    {
                        outCSV << ";" << f.name;
                    }
                }
            }
            outCSV << std::endl;
        }

        for (const ::armarx::detail::ControlThreadOutputBufferEntry& iteration : backlog)
        {
            //write csv data
            {
                outCSV  << iteration.iteration << ";" << iteration.sensorValuesTimestamp;
                //sens
                {
                    for (const SensorValueBase* val : iteration.sensors)
                    {
                        for (std::size_t idxField = 0; idxField < val->getNumberOfDataFields(); ++ idxField)
                        {
                            std::string s;
                            val->getDataFieldAs(idxField, s);
                            outCSV  << ";" << s;
                        }
                    }
                }
                //ctrl
                {
                    for (const auto& vals : iteration.control)
                    {
                        for (const ControlTargetBase* val : vals)
                        {
                            for (std::size_t idxField = 0; idxField < val->getNumberOfDataFields(); ++ idxField)
                            {
                                std::string s;
                                val->getDataFieldAs(idxField, s);
                                outCSV  << ";" << s;
                            }
                        }
                    }
                }
                outCSV << std::endl;
            }
            //write message data
            {
                bool atLeastOneMessage = false;
                for (const ::armarx::detail::RtMessageLogEntryBase* msg : iteration.messages.getEntries())
                {
                    if (!msg)
                    {
                        break;
                    }
                    outMsg << "[" << msg->getTime().toDateTime() << "] iteration "
                           << iteration.iteration << ":\n"
                           << msg->format() << std::endl;
                    atLeastOneMessage = true;
                }
                if (atLeastOneMessage)
                {
                    outMsg << "\nmessages lost: " << iteration.messages.messagesLost
                           << " (required additional "
                           << iteration.messages.requiredAdditionalBufferSpace << " bytes, "
                           << iteration.messages.requiredAdditionalEntries << " message entries)\n" << std::endl;
                }
            }
        }
    }

    RobotUnitDataStreaming::DataStreamingDescription Logging::startDataStreaming(
        const RobotUnitDataStreaming::ReceiverPrx& receiver,
        const RobotUnitDataStreaming::Config& config,
        const Ice::Current&)
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        if (!receiver)
        {
            throw InvalidArgumentException {"Receiver proxy is NULL!"};
        }
        std::lock_guard<std::mutex> guard {rtLoggingMutex};
        if (rtDataStreamingEntry.count(receiver))
        {
            throw InvalidArgumentException {"There already is a logger for the given receiver"};
        }

        RobotUnitDataStreaming::DataStreamingDescription result;
        DataStreamingEntry& streamingEntry = rtDataStreamingEntry[receiver];
        getProperty(streamingEntry.rtStreamMaxClientErrors,
                    "RTLogging_StreamingDataMaxClientConnectionFailures");

        ARMARX_INFO << "start data streaming to " << receiver->ice_getIdentity().name
                    << ". Values: " << config.loggingNames;
        auto devMatchesAnyKey = [&](const std::string & dev)
        {
            for (const auto& key : config.loggingNames)
            {
                if (MatchName(key, dev))
                {
                    return true;
                }
            }
            return false;
        };

        const auto handleVal = [&](
                                   const ValueMetaData & valData,
                                   DataStreamingEntry & streamingEntry,
                                   RobotUnitDataStreaming::DataStreamingDescription & descr
                               ) -> std::vector<DataStreamingEntry::OutVal>
        {
            ARMARX_TRACE_LITE;
            std::vector<DataStreamingEntry::OutVal> result;
            result.resize(valData.fields.size());
            for (std::size_t i = 0; i < valData.fields.size(); ++i)
            {
                if (!devMatchesAnyKey(valData.fields.at(i).name))
                {
                    continue; //do not add to result and skipp during processing
                }
                auto& descrEntr = descr.entries[valData.fields.at(i).name];
                //formatter failes here!
                //*INDENT-OFF*
                #define make_case(Type, TName)                                    \
                    (typeid(Type) == *valData.fields.at(i).type)                  \
                    {                                                             \
                        descrEntr.index = streamingEntry.num##TName##s;           \
                        descrEntr.type = RobotUnitDataStreaming::NodeType##TName; \
                        result.at(i).idx   = streamingEntry.num##TName##s;        \
                        result.at(i).value = DataStreamingEntry::ValueT::TName;   \
                        ++streamingEntry.num##TName##s;                           \
                    }
                if      make_case(bool,        Bool)
                else if make_case(Ice::Byte,   Byte)
                else if make_case(Ice::Short,  Short)
                else if make_case(Ice::Int,    Int)
                else if make_case(Ice::Long,   Long)
                else if make_case(Ice::Float,  Float)
                else if make_case(Ice::Double, Double)
                else
                {
                    ARMARX_CHECK_EXPRESSION(false)
                            << "This code sould be unreachable! "
                               "The type of "
                            << valData.fields.at(i).name
                            << " is not handled correctly!";
                }
                #undef make_case
                //*INDENT-ON*
            }
            return result;
        };

        //get logged sensor device values
        {
            ARMARX_TRACE;
            streamingEntry.sensDevs.reserve(sensorDeviceValueMetaData.size());
            for (const auto& valData : sensorDeviceValueMetaData)
            {
                streamingEntry.sensDevs.emplace_back(
                    handleVal(valData, streamingEntry, result));
            }
        }
        //get logged control device values
        {
            ARMARX_TRACE;
            streamingEntry.ctrlDevs.reserve(controlDeviceValueMetaData.size());
            for (const auto& devData : controlDeviceValueMetaData)
            {
                streamingEntry.ctrlDevs.emplace_back();
                auto& ctrlDevEntrs = streamingEntry.ctrlDevs.back();
                ctrlDevEntrs.reserve(devData.size());
                for (const auto& valData : devData)
                {
                    ctrlDevEntrs.emplace_back(
                        handleVal(valData, streamingEntry, result));
                }
            }
        }

        return result;
    }

    void Logging::stopDataStreaming(const RobotUnitDataStreaming::ReceiverPrx& receiver, const Ice::Current&)
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        std::lock_guard<std::mutex> guard {rtLoggingMutex};
        if (!rtDataStreamingEntry.count(receiver))
        {
            throw InvalidArgumentException {"stopDataStreaming called for a nonexistent log"};
        }
        ARMARX_INFO_S << "RobotUnit: request to stop DataStreaming for " << receiver->ice_id();
        rtDataStreamingEntry.at(receiver).stopStreaming = true;
    }

    void Logging::_preFinishRunning()
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        defaultLogHandle = nullptr;
        if (rtLoggingTask)
        {
            ARMARX_DEBUG << "shutting down rt logging task";
            rtLoggingTask->stop();
            while (rtLoggingTask->isFunctionExecuting() || rtLoggingTask->isRunning())
            {
                ARMARX_FATAL << deactivateSpam(0.1) << "RT LOGGING TASK IS RUNNING AFTER IT WAS STOPPED!";
            }
            rtLoggingTask = nullptr;
            ARMARX_DEBUG << "shutting down rt logging task done";
        }
    }

    void Logging::_preFinishControlThreadInitialization()
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        controlThreadId = LogSender::getThreadId();
        ControlThreadOutputBuffer::RtLoggingInstance = &(_module<ControlThreadDataBuffer>().getControlThreadOutputBuffer());

        ARMARX_INFO << "starting rt logging with timestep " << rtLoggingTimestep;
        rtLoggingTask->start();
    }

    void Logging::doLogging()
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        std::lock_guard<std::mutex> guard {rtLoggingMutex};
        const auto now = IceUtil::Time::now();
        // entries are removed last

        //remove backlog entries
        {
            while (!backlog.empty() && backlog.front().writeTimestamp + rtLoggingBacklogRetentionTime < now)
            {
                backlog.pop_front();
            }
        }
        //log all
        {
            ARMARX_TRACE;
            if (!rtLoggingEntries.empty() || !rtDataStreamingEntry.empty())
            {
                ARMARX_INFO << deactivateSpam()
                            << "Number of logs    " << rtLoggingEntries.size() << '\n'
                            << "Number of streams " << rtDataStreamingEntry.size();
            }
            _module<ControlThreadDataBuffer>().getControlThreadOutputBuffer().foreachNewLoggingEntry(
                [&](const auto & data, auto i, auto num)
            {
                doLogging(now, data, i, num);
            }
            );
        }
        ARMARX_DEBUG_S << ::deactivateSpam() << "the last " << backlog.size() << " iterations are stored";
        //flush all files
        {
            for (auto& pair : rtLoggingEntries)
            {
                pair.second->stream << std::flush;
            }
        }

        //remove entries
        {
            ARMARX_TRACE;
            std::vector<std::string> toRemove;
            toRemove.reserve(rtLoggingEntries.size());
            for (auto& [key, value] : rtLoggingEntries)
            {
                if (value->stopLogging)
                {
                    //can't remove the current elemet
                    //(this would invalidate the current iterator)
                    toRemove.emplace_back(key);
                }
            }
            for (const auto& rem : toRemove)
            {
                rtLoggingEntries.erase(rem);
            }
        }
        //deal with data streaming
        {
            ARMARX_TRACE;
            std::vector<RobotUnitDataStreaming::ReceiverPrx> toRemove;
            toRemove.reserve(rtDataStreamingEntry.size());
            for (auto& [prx, data] : rtDataStreamingEntry)
            {
                if (data.stopStreaming)
                {
                    toRemove.emplace_back(prx);
                }
                else
                {
                    data.send(prx);
                }
            }
            for (const auto& prx : toRemove)
            {
                rtDataStreamingEntry.erase(prx);
            }
        }
    }

    void Logging::doLogging(const IceUtil::Time& now, const
                            ControlThreadOutputBuffer::Entry& data,
                            std::size_t i, std::size_t num)
    {
        ARMARX_TRACE;
        //header
        {
            ARMARX_TRACE;
            //base (marker;iteration;timestamp)
            for (auto& [_, e] : rtLoggingEntries)
            {
                e->stream << "\n"
                          << e->marker << ";"
                          << data.iteration << ";"
                          << data.sensorValuesTimestamp.toMicroSeconds() << ";"
                          << data.timeSinceLastIteration.toMicroSeconds();
                e->marker.clear();
            }
            //streaming
            for (auto& [_, e] : rtDataStreamingEntry)
            {
                e.processHeader(data);
            }
        }
        //process devices
        {
            //sens
            {
                ARMARX_TRACE;
                //sensors
                for (std::size_t idxDev = 0; idxDev < data.sensors.size(); ++ idxDev)
                {
                    const SensorValueBase* val = data.sensors.at(idxDev);
                    //dimensions of sensor value (e.g. vel, tor, f_x, f_y, ...)
                    for (std::size_t idxField = 0; idxField < val->getNumberOfDataFields(); ++ idxField)
                    {
                        const auto  str = val->getDataFieldAs<std::string>(idxField);
                        for (auto& [_, entry] : rtLoggingEntries)
                        {
                            if (entry->loggedSensorDeviceValues.at(idxDev).at(idxField))
                            {
                                entry->stream  << ";" << str;
                            }
                        }
                        for (auto& [_, data] : rtDataStreamingEntry)
                        {
                            data.processSens(*val, idxDev, idxField);
                        }
                    }
                }
            }
            //ctrl
            {
                ARMARX_TRACE;
                //joint controllers
                for (std::size_t idxDev = 0; idxDev < data.control.size(); ++ idxDev)
                {
                    const auto& vals = data.control.at(idxDev);
                    //control value (e.g. v_platform)
                    for (std::size_t idxVal = 0; idxVal < vals.size(); ++idxVal)
                    {
                        const ControlTargetBase* val = vals.at(idxVal);
                        //dimensions of control value (e.g. v_platform_x, v_platform_y, v_platform_rotate)
                        for (std::size_t idxField = 0; idxField < val->getNumberOfDataFields(); ++ idxField)
                        {
                            std::string str;
                            val->getDataFieldAs(idxField, str);
                            for (auto& [_, entry] : rtLoggingEntries)
                            {
                                if (entry->loggedControlDeviceValues.at(idxDev).at(idxVal).at(idxField))
                                {
                                    entry->stream  << ";" << str;
                                }
                            }
                            for (auto& [_, data] : rtDataStreamingEntry)
                            {
                                data.processCtrl(*val, idxDev, idxVal, idxField);
                            }
                        }
                    }
                }
            }
        }
        //finish processing
        {
            //store data to backlog
            {
                ARMARX_TRACE;
                if (data.writeTimestamp + rtLoggingBacklogRetentionTime >= now)
                {
                    backlog.emplace_back(data, true); //true for minimal copy
                }
            }
            //print + reset messages
            {
                ARMARX_TRACE;
                for (const ::armarx::detail::RtMessageLogEntryBase* ptr : data.messages.getEntries())
                {
                    if (!ptr)
                    {
                        break;
                    }
                    ptr->print(controlThreadId);
                }
            }
        }
    }

    bool Logging::MatchName(const std::string& pattern, const std::string& name)
    {
        ARMARX_TRACE;
        if (pattern.empty())
        {
            return false;
        }
        static const std::regex pattern_regex{R"(^\^?[- ._*a-zA-Z0-9]+\$?$)"};
        if (!std::regex_match(pattern, pattern_regex))
        {
            throw InvalidArgumentException {"Pattern '" + pattern + "' is invalid"};
        }
        static const std::regex reg_dot{"[.]"};
        static const std::regex reg_star{"[*]"};
        const std::string rpl1 = std::regex_replace(pattern, reg_dot,  "\\.");
        const std::string rpl2 = std::regex_replace(rpl1,    reg_star, ".*");
        const std::regex key_regex{rpl2};
        return std::regex_search(name, key_regex);
    }

    void Logging::_postOnInitRobotUnit()
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        rtLoggingTimestep = getProperty<std::size_t>("RTLogging_PeriodMs");
        ARMARX_CHECK_LESS(0, rtLoggingTimestep) << "The property RTLoggingPeriodMs must not be 0";

        messageBufferSize = getProperty<std::size_t>("RTLogging_MessageBufferSize");
        messageBufferMaxSize = getProperty<std::size_t>("RTLogging_MaxMessageBufferSize");
        ARMARX_CHECK_LESS_EQUAL(messageBufferSize, messageBufferMaxSize);

        messageBufferNumberEntries = getProperty<std::size_t>("RTLogging_MessageNumber");
        messageBufferMaxNumberEntries = getProperty<std::size_t>("RTLogging_MaxMessageNumber");
        ARMARX_CHECK_LESS_EQUAL(messageBufferNumberEntries, messageBufferMaxNumberEntries);

        rtLoggingBacklogRetentionTime = IceUtil::Time::milliSeconds(getProperty<std::size_t>("RTLogging_KeepIterationsForMs"));

        ARMARX_CHECK_GREATER(getControlThreadTargetPeriod().toMicroSeconds(), 0);
    }

    void Logging::_postFinishDeviceInitialization()
    {
        ARMARX_TRACE;
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        //init buffer
        {
            ARMARX_TRACE;
            std::size_t ctrlThreadPeriodUs = static_cast<std::size_t>(getControlThreadTargetPeriod().toMicroSeconds());
            std::size_t logThreadPeriodUs = rtLoggingTimestep * 1000;
            std::size_t nBuffers = (logThreadPeriodUs / ctrlThreadPeriodUs + 1) * 100;

            const auto bufferSize = _module<ControlThreadDataBuffer>().getControlThreadOutputBuffer().initialize(
                                        nBuffers, _module<Devices>().getControlDevices(), _module<Devices>().getSensorDevices(),
                                        messageBufferSize, messageBufferNumberEntries,
                                        messageBufferMaxSize, messageBufferMaxNumberEntries);
            ARMARX_INFO << "RTLogging activated! using "
                        << nBuffers << "buffers "
                        << "(buffersize = " << bufferSize << " bytes)";
        }
        //init logging names + field types
        {
            ARMARX_TRACE;
            const auto makeValueMetaData = [&](auto * val, const std::string & namePre)
            {
                ValueMetaData data;
                const auto names = val->getDataFieldNames();
                data.fields.resize(names.size());
                for (const auto& [fieldIdx, fieldName] : MakeIndexedContainer(names))
                {
                    data.fields.at(fieldIdx).name = namePre + '.' + fieldName;
                    data.fields.at(fieldIdx).type = &(val->getDataFieldType(fieldIdx));
                }
                return data;
            };

            //sensorDevices
            controlDeviceValueMetaData.reserve(_module<Devices>().getControlDevices().size());
            for (const auto& cd : _module<Devices>().getControlDevices().values())
            {
                ARMARX_TRACE;
                controlDeviceValueMetaData.emplace_back();
                auto& dataForDev = controlDeviceValueMetaData.back();
                dataForDev.reserve(cd->getJointControllers().size());
                for (auto jointC : cd->getJointControllers())
                {
                    dataForDev.emplace_back(
                        makeValueMetaData(jointC->getControlTarget(),
                                          "ctrl." +
                                          cd->getDeviceName() + "." +
                                          jointC->getControlMode()));
                }
            }
            //sensorDevices
            sensorDeviceValueMetaData.reserve(_module<Devices>().getSensorDevices().size());
            for (const auto& sd : _module<Devices>().getSensorDevices().values())
            {
                ARMARX_TRACE;
                sensorDeviceValueMetaData.emplace_back(
                    makeValueMetaData(sd->getSensorValue(),
                                      "sens." +
                                      sd->getDeviceName()));
            }
        }
        //start logging thread is done in rtinit
        //maybe add the default log
        {
            ARMARX_TRACE;
            const auto loggingpath = getProperty<std::string>("RTLogging_DefaultLog").getValue();
            if (!loggingpath.empty())
            {
                defaultLogHandle = startRtLogging(loggingpath, getLoggingNames());
            }
        }
        //setup thread (don't start it
        rtLoggingTask = new RTLoggingTaskT([&] {doLogging();}, rtLoggingTimestep, false, getName() + "_RTLoggingTask");
        rtLoggingTask->setDelayWarningTolerance(rtLoggingTimestep * 10);
    }

    void Logging::DataStreamingEntry::processHeader(const ControlThreadOutputBuffer::Entry& e)
    {
        ARMARX_TRACE;
        if (stopStreaming)
        {
            return;
        }
        result.emplace_back();
        auto& data = result.back();
        data.iterationId                 = e.iteration;
        data.timestampUSec               = e.sensorValuesTimestamp.toMicroSeconds();
        data.timesSinceLastIterationUSec = e.timeSinceLastIteration.toMicroSeconds();

        data.bools  .resize(numBools);
        data.bytes  .resize(numBytes);
        data.shorts .resize(numShorts);
        data.ints   .resize(numInts);
        data.longs  .resize(numLongs);
        data.floats .resize(numFloats);
        data.doubles.resize(numDoubles);

    }

    void WriteTo(const Logging::DataStreamingEntry::OutVal& out,
                 const auto& val,
                 std::size_t fidx,
                 auto& data)
    {
        ARMARX_TRACE;
        using enum_t = Logging::DataStreamingEntry::ValueT;
        switch (out.value)
        {
            case enum_t::Bool   :
                bool b;
                val.getDataFieldAs(fidx, b);
                data.bools.at(out.idx) = b;
                return;
            case enum_t::Byte   :
                val.getDataFieldAs(fidx, data.bytes.at(out.idx));
                return;
            case enum_t::Short  :
                val.getDataFieldAs(fidx, data.shorts.at(out.idx));
                return;
            case enum_t::Int    :
                val.getDataFieldAs(fidx, data.ints.at(out.idx));
                return;
            case enum_t::Long   :
                val.getDataFieldAs(fidx, data.longs.at(out.idx));
                return;
            case enum_t::Float  :
                val.getDataFieldAs(fidx, data.floats.at(out.idx));
                return;
            case enum_t::Double :
                val.getDataFieldAs(fidx, data.doubles.at(out.idx));
                return;
            case enum_t::Skipped:
                return;
        }
    }

    void Logging::DataStreamingEntry::processCtrl(
        const ControlTargetBase& val,
        std::size_t didx,
        std::size_t vidx,
        std::size_t fidx)
    {
        ARMARX_TRACE;
        if (stopStreaming)
        {
            return;
        }
        auto& data = result.back();
        const OutVal& o = ctrlDevs.at(didx).at(vidx).at(fidx);
        WriteTo(o, val, fidx, data);
    }

    void Logging::DataStreamingEntry::processSens(
        const SensorValueBase& val,
        std::size_t didx,
        std::size_t fidx)
    {
        ARMARX_TRACE;
        if (stopStreaming)
        {
            return;
        }
        auto& data = result.back();
        const OutVal& o = sensDevs.at(didx).at(fidx);
        WriteTo(o, val, fidx, data);
    }

    void Logging::DataStreamingEntry::send(const RobotUnitDataStreaming::ReceiverPrx& r)
    {
        ARMARX_TRACE;
        try
        {
            r->update(result);
            connectionFailures = 0;
            result.clear();
        }
        catch (...)
        {
            ARMARX_TRACE;
            ++connectionFailures;
            if (connectionFailures > rtStreamMaxClientErrors)
            {
                stopStreaming = true;
                ARMARX_WARNING_S << "DataStreaming Receiver was not reachable for "
                                 << connectionFailures << " iterations! Removing receiver";
            }
        }
    }
}
