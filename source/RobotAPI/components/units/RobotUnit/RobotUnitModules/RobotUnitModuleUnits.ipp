/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "RobotUnitModuleUnits.h"

namespace armarx::RobotUnitModule
    {
        inline Units& Units::Instance()
        {
            return ModuleBase::Instance<Units>();
        }

        inline KinematicUnitInterfacePrx Units::getKinematicUnit(const Ice::Current&) const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnitPrx<KinematicUnitInterface>();
        }
        inline ForceTorqueUnitInterfacePrx Units::getForceTorqueUnit(const Ice::Current&) const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnitPrx<ForceTorqueUnitInterface>();
        }
        inline InertialMeasurementUnitInterfacePrx Units::getInertialMeasurementUnit(const Ice::Current&) const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnitPrx<InertialMeasurementUnitInterface>();
        }
        inline PlatformUnitInterfacePrx Units::getPlatformUnit(const Ice::Current&) const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnitPrx<PlatformUnitInterface>();
        }
        inline TCPControlUnitInterfacePrx Units::getTCPControlUnit(const Ice::Current&) const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnitPrx<TCPControlUnitInterface>();
        }
        inline TrajectoryPlayerInterfacePrx Units::getTrajectoryPlayer(const Ice::Current&) const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnitPrx<TrajectoryPlayerInterface>();
        }

        template<class T>
        inline typename T::PointerType Units::getUnit() const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return T::PointerType::dynamicCast(getUnit(T::ice_staticId()));
        }

        template<class T>
        inline typename T::ProxyType Units::getUnitPrx() const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return T::ProxyType::uncheckedCast(getUnit(T::ice_staticId(), Ice::emptyCurrent));
        }

        inline KinematicUnitInterfacePtr Units::getKinematicUnit() const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnit<KinematicUnitInterface>();
        }
        inline ForceTorqueUnitInterfacePtr Units::getForceTorqueUnit() const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnit<ForceTorqueUnitInterface>();
        }
        inline InertialMeasurementUnitInterfacePtr Units::getInertialMeasurementUnit() const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnit<InertialMeasurementUnitInterface>();
        }
        inline PlatformUnitInterfacePtr Units::getPlatformUnit() const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnit<PlatformUnitInterface>();
        }
        inline TCPControlUnitInterfacePtr Units::getTCPControlUnit() const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnit<TCPControlUnitInterface>();
        }
        inline TrajectoryPlayerInterfacePtr Units::getTrajectoryPlayer() const
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getUnit<TrajectoryPlayerInterface>();
        }
    }
