/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DefaultWidgetDescriptions.h"

namespace armarx::WidgetDescription
{
    StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options)
    {
        StringComboBoxPtr rns = new StringComboBox;
        rns->name = std::move(name);
        rns->options = std::move(options);
        rns->defaultIndex = 0;
        return rns;
    }

    StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options,
        const std::set<std::string>& preferredSet)
    {
        StringComboBoxPtr rns = makeStringSelectionComboBox(std::move(name), std::move(options));
        for (std::size_t i = 0; i < rns->options.size(); ++i)
        {
            if (preferredSet.count(rns->options.at(i)))
            {
                rns->defaultIndex = i;
                break;
            }
        }
        return rns;
    }

    StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options,
        const std::string& mostPreferred)
    {
        StringComboBoxPtr rns = makeStringSelectionComboBox(std::move(name), std::move(options));
        for (std::size_t i = 0; i < rns->options.size(); ++i)
        {
            if (mostPreferred == rns->options.at(i))
            {
                rns->defaultIndex = i;
                break;
            }
        }
        return rns;
    }

    StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options,
        const std::set<std::string>& preferredSet,
        const std::string& mostPreferred)
    {
        StringComboBoxPtr rns = makeStringSelectionComboBox(std::move(name), std::move(options), preferredSet);
        for (std::size_t i = 0; i < rns->options.size(); ++i)
        {
            if (mostPreferred == rns->options.at(i))
            {
                rns->defaultIndex = i;
                break;
            }
        }
        return rns;
    }
}
