/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::PlatformSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <Eigen/Core>

#include <VirtualRobot/MathTools.h>

#include <RobotAPI/components/units/PlatformUnit.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointHolonomicPlatformRelativePositionController.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointHolonomicPlatformGlobalPositionController.h>
#include <RobotAPI/libraries/core/Pose.h>

#include "RobotUnitSubUnit.h"
#include "../NJointControllers/NJointHolonomicPlatformUnitVelocityPassThroughController.h"
#include "../SensorValues/SensorValueHolonomicPlatform.h"

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(PlatformSubUnit);
    class PlatformSubUnit:
        virtual public RobotUnitSubUnit,
        virtual public PlatformUnit,
        virtual public PlatformSubUnitInterface
    {
    public:
        void update(const SensorAndControl& sc, const JointAndNJointControllers& c) override;

        // PlatformUnitInterface interface
        void move(Ice::Float vx, Ice::Float vy, Ice::Float vr,  const Ice::Current& = Ice::emptyCurrent) override;
        void moveTo(Ice::Float rx, Ice::Float ry, Ice::Float rr, Ice::Float lac, Ice::Float rac,  const Ice::Current& = Ice::emptyCurrent) override;
        void moveRelative(Ice::Float rx, Ice::Float ry, Ice::Float rr, Ice::Float lac, Ice::Float rac,  const Ice::Current& = Ice::emptyCurrent) override;

        void setMaxVelocities(Ice::Float mxVLin, Ice::Float mxVAng,  const Ice::Current& = Ice::emptyCurrent) override;

        virtual void setGlobalPose(PoseBasePtr globalPose,  const Ice::Current& = Ice::emptyCurrent) /*override*/;
        virtual PoseBasePtr getGlobalPose(const Ice::Current& = Ice::emptyCurrent) /*override*/;


        void reportPlatformPose(PlatformPose const& currentPose, const Ice::Current& c = ::Ice::Current()) override ;
        void reportNewTargetPose(Ice::Float newPlatformPositionX, Ice::Float newPlatformPositionY, Ice::Float newPlatformRotation, const Ice::Current& c = ::Ice::Current()) override;
        void reportPlatformVelocity(Ice::Float currentPlatformVelocityX, Ice::Float currentPlatformVelocityY, Ice::Float currentPlatformVelocityRotation, const Ice::Current& c = ::Ice::Current()) override;

        void reportPlatformOdometryPose(::Ice::Float, ::Ice::Float, ::Ice::Float, const ::Ice::Current& = ::Ice::Current()) override;

        // PlatformUnit interface
        void onInitPlatformUnit()  override
        {
            usingTopic("PlatformState");
        }
        void onStartPlatformUnit() override {}
        void onExitPlatformUnit()  override {}
        void stopPlatform(const Ice::Current& c = Ice::emptyCurrent) override;

        NJointHolonomicPlatformUnitVelocityPassThroughControllerPtr pt;
        NJointHolonomicPlatformRelativePositionControllerPtr relativePosCtrl;
        NJointHolonomicPlatformGlobalPositionControllerPtr globalPosCtrl;
        std::size_t platformSensorIndex;
    protected:
        Eigen::Matrix4f getRelativePose() const;

        mutable std::mutex dataMutex;

        Ice::Float maxVLin = std::numeric_limits<Ice::Float>::infinity();
        Ice::Float maxVAng = std::numeric_limits<Ice::Float>::infinity();

        float relX = 0;
        float relY = 0;
        float relR = 0;

        Eigen::Matrix4f abs;

        Eigen::Matrix4f positionCorrection = Eigen::Matrix4f::Identity();
    };
}
