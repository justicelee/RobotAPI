/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::KinematicSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/components/units/KinematicUnit.h>

#include <ArmarXCore/core/util/IceReportSkipper.h>

#include "RobotUnitSubUnit.h"
#include "../util.h"
#include "../SensorValues/SensorValue1DoFActuator.h"
#include "../NJointControllers/NJointKinematicUnitPassThroughController.h"

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(RobotUnit);

    TYPEDEF_PTRS_HANDLE(KinematicSubUnit);
    class KinematicSubUnit:
        virtual public RobotUnitSubUnit,
        virtual public KinematicUnit
    {
    public:
        struct ActuatorData
        {
            std::string name;
            std::size_t sensorDeviceIndex;
            NJointKinematicUnitPassThroughControllerPtr ctrlPos;
            NJointKinematicUnitPassThroughControllerPtr ctrlVel;
            NJointKinematicUnitPassThroughControllerPtr ctrlTor;

            NJointControllerPtr getController(ControlMode c) const;
            NJointControllerPtr getActiveController() const;
        };
        KinematicSubUnit();

        void setupData(
            std::string relRobFile,
            VirtualRobot::RobotPtr rob,
            std::map<std::string, ActuatorData>&& newDevs,
            std::vector<std::set<std::string> > controlDeviceHardwareControlModeGrps,
            std::set<std::string> controlDeviceHardwareControlModeGrpsMerged,
            RobotUnit* newRobotUnit
        );
        void update(const SensorAndControl& sc, const JointAndNJointControllers& c) override;

        // KinematicUnitInterface interface
        void requestJoints(const Ice::StringSeq&, const Ice::Current&) override;
        void releaseJoints(const Ice::StringSeq&, const Ice::Current&) override;

        void setJointAngles(const NameValueMap& angles, const Ice::Current&) override;
        void setJointVelocities(const NameValueMap& velocities, const Ice::Current&) override;
        void setJointTorques(const NameValueMap& torques, const Ice::Current&) override;
        void switchControlMode(const NameControlModeMap& ncm, const Ice::Current&) override;

        void setJointAccelerations(const NameValueMap&, const Ice::Current&) override;
        void setJointDecelerations(const NameValueMap&, const Ice::Current&) override;

        NameControlModeMap getControlModes(const Ice::Current&)      override;
        NameValueMap   getJointAngles(const Ice::Current&)     const override;
        NameValueMap   getJointVelocities(const Ice::Current&) const override;
        Ice::StringSeq getJoints(const Ice::Current& c)        const override;

        void onInitKinematicUnit()  override {}
        void onStartKinematicUnit() override {}
        void onExitKinematicUnit()  override {}

    private:
        std::map<std::string, ActuatorData> devs;
        // never use this when holding the mutex! (could cause deadlocks)
        RobotUnit* robotUnit = NULL;
        mutable std::mutex dataMutex;
        NameControlModeMap ctrlModes;
        NameValueMap ang;
        NameValueMap vel;
        NameValueMap acc;
        NameValueMap tor;
        NameValueMap motorCurrents;
        NameValueMap motorTemperatures;
        NameStatusMap statuses;
        std::vector<std::set<std::string>> controlDeviceHardwareControlModeGroups;
        std::set<std::string> controlDeviceHardwareControlModeGroupsMerged;
        IceReportSkipper reportSkipper;
        std::vector<VirtualRobot::RobotNodePtr> sensorLessJoints;

        template<class ValueT, class SensorValueT, ValueT SensorValueT::* Member>
        static void UpdateNameValueMap(std::map<std::string, ValueT>& nvm, const std::string& name, const SensorValueBase* sv, bool& changeState)
        {
            const SensorValueT* s = sv->asA<SensorValueT>();
            if (!s)
            {
                return;
            };
            const ValueT& v = s->*Member;
            if (!nvm.count(name) || (nvm.at(name) != v))
            {
                changeState = true;
            }
            nvm[name] = v;
        }
    };
}
