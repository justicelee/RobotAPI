/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::PlatformSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "PlatformSubUnit.h"

#include <boost/algorithm/clamp.hpp>

void armarx::PlatformSubUnit::update(const armarx::SensorAndControl& sc, const JointAndNJointControllers&)
{
    if (!getProxy())
    {
        //this unit is not initialized yet
        ARMARX_IMPORTANT << deactivateSpam(1) << "not initialized yet - skipping this update";
        return;
    }
    if (!listenerPrx)
    {
        ARMARX_INFO << deactivateSpam(1) << "listener is not set - skipping this update";
        return;
    }
    const SensorValueBase* sensorValue = sc.sensors.at(platformSensorIndex).get();
    ARMARX_CHECK_EXPRESSION(sensorValue);
    std::lock_guard<std::mutex> guard {dataMutex};
    //pos
    {
        ARMARX_CHECK_EXPRESSION(sensorValue->isA<SensorValueHolonomicPlatformRelativePosition>());
        const SensorValueHolonomicPlatformRelativePosition* s = sensorValue->asA<SensorValueHolonomicPlatformRelativePosition>();
        relX = s->relativePositionX;
        relY = s->relativePositionY;
        relR = s->relativePositionRotation;


        // @@@ CHECK BELOW:
        if (s->isA<SensorValueHolonomicPlatformAbsolutePosition>())
        {
            const SensorValueHolonomicPlatformAbsolutePosition* sabs = s->asA<SensorValueHolonomicPlatformAbsolutePosition>();
            abs = positionCorrection * VirtualRobot::MathTools::posrpy2eigen4f(sabs->absolutePositionX, sabs->absolutePositionY, 0, 0, 0, sabs->absolutePositionRotation);

            PlatformPose currentPose;
            currentPose.timestampInMicroSeconds = sc.sensorValuesTimestamp.toMicroSeconds();
            currentPose.x = abs(0, 3);
            currentPose.y = abs(1, 3);
            currentPose.rotationAroundZ = VirtualRobot::MathTools::eigen4f2rpy(abs)(2);
            listenerPrx->reportPlatformPose(currentPose);
            globalPosCtrl->setGlobalPos(currentPose);
        }
        else
        {
            abs = positionCorrection * VirtualRobot::MathTools::posrpy2eigen4f(relX, relY, 0, 0, 0, relR);
        }

        listenerPrx->reportPlatformOdometryPose(relX, relY, relR);
    }
    //vel
    {
        ARMARX_CHECK_EXPRESSION(sensorValue->isA<SensorValueHolonomicPlatformVelocity>());
        const SensorValueHolonomicPlatformVelocity* s = sensorValue->asA<SensorValueHolonomicPlatformVelocity>();
        listenerPrx->reportPlatformVelocity(s->velocityX, s->velocityY, s->velocityRotation);
    }
}

void armarx::PlatformSubUnit::move(Ice::Float vx, Ice::Float vy, Ice::Float vr, const Ice::Current&)
{
    //holding the mutex here could deadlock
    if (!pt->isControllerActive())
    {
        pt->activateController();
    }
    std::lock_guard<std::mutex> guard {dataMutex};
    pt->setVelocites(
        boost::algorithm::clamp(vx, -maxVLin, maxVLin),
        boost::algorithm::clamp(vy, -maxVLin, maxVLin),
        boost::algorithm::clamp(vr, -maxVAng, maxVAng)
    );
}

void armarx::PlatformSubUnit::moveTo(Ice::Float rx, Ice::Float ry, Ice::Float rr, Ice::Float lac, Ice::Float rac, const Ice::Current&)
{
    globalPosCtrl->setTarget(rx, ry, rr, lac, rac);
    if (!globalPosCtrl->isControllerActive())
    {
        globalPosCtrl->activateController();
    };
}

void armarx::PlatformSubUnit::moveRelative(Ice::Float rx, Ice::Float ry, Ice::Float rr, Ice::Float lac, Ice::Float rac, const Ice::Current&)
{
    relativePosCtrl->setTarget(rx, ry, rr, lac, rac);
    if (!relativePosCtrl->isControllerActive())
    {
        relativePosCtrl->activateController();
    }
    //holding the mutex here could deadlock
    //    std::lock_guard<std::mutex> guard {dataMutex};
    ARMARX_INFO << "target orientation: " << rr;

}

void armarx::PlatformSubUnit::setMaxVelocities(Ice::Float mxVLin, Ice::Float mxVAng, const Ice::Current&)
{
    std::lock_guard<std::mutex> guard {dataMutex};
    maxVLin = std::abs(mxVLin);
    maxVAng = std::abs(mxVAng);
}

void armarx::PlatformSubUnit::setGlobalPose(armarx::PoseBasePtr globalPose, const Ice::Current&)
{
    std::lock_guard<std::mutex> guard {dataMutex};
    PosePtr p = PosePtr::dynamicCast(globalPose);
    positionCorrection = p->toEigen() * abs.inverse();
}

armarx::PoseBasePtr armarx::PlatformSubUnit::getGlobalPose(const Ice::Current&)
{
    std::lock_guard<std::mutex> guard {dataMutex};
    return new Pose {abs};
}

void armarx::PlatformSubUnit::stopPlatform(const Ice::Current& c)
{
    move(0, 0, 0);
}

Eigen::Matrix4f armarx::PlatformSubUnit::getRelativePose() const
{
    Eigen::Matrix4f rp = VirtualRobot::MathTools::rpy2eigen4f(0, 0, relR);
    rp(0, 3) = relX;
    rp(1, 3) = relY;
    return rp;
}


void armarx::PlatformSubUnit::reportPlatformPose(PlatformPose const& currentPose, const Ice::Current& c)
{
    globalPosCtrl->setGlobalPos(currentPose);
}

void armarx::PlatformSubUnit::reportNewTargetPose(Ice::Float newPlatformPositionX, Ice::Float newPlatformPositionY, Ice::Float newPlatformRotation, const Ice::Current& c)
{

}

void armarx::PlatformSubUnit::reportPlatformVelocity(Ice::Float currentPlatformVelocityX, Ice::Float currentPlatformVelocityY, Ice::Float currentPlatformVelocityRotation, const Ice::Current& c)
{

}



void armarx::PlatformSubUnit::reportPlatformOdometryPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&)
{
}
