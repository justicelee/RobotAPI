/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include "NJointController.h"
#include <VirtualRobot/Robot.h>
#include "../RobotUnit.h"
#include "../ControlTargets/ControlTarget1DoFActuator.h"
#include "../SensorValues/SensorValue1DoFActuator.h"
#include <VirtualRobot/IK/DifferentialIK.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianTorqueController.h>

namespace armarx
{

    TYPEDEF_PTRS_HANDLE(NJointCartesianTorqueController);
    //TYPEDEF_PTRS_HANDLE(NJointCartesianTorqueControllerConfig);

    /*class NJointCartesianTorqueControllerConfig : virtual public NJointControllerConfig
    {
    public:
        NJointCartesianTorqueControllerConfig(std::string const& nodeSetName, const std::string& tcpName, VirtualRobot::IKSolver::CartesianSelection mode):
            nodeSetName(nodeSetName),
            tcpName(tcpName),
            mode(mode)
        {}

        const std::string nodeSetName;
        const std::string tcpName;
        const VirtualRobot::IKSolver::CartesianSelection mode;
    };*/

    TYPEDEF_PTRS_HANDLE(NJointCartesianTorqueControllerControlData);
    class NJointCartesianTorqueControllerControlData
    {
    public:
        float forceX = 0;
        float forceY = 0;
        float forceZ = 0;
        float torqueX = 0;
        float torqueY = 0;
        float torqueZ = 0;

        //VirtualRobot::IKSolver::CartesianSelection mode = VirtualRobot::IKSolver::All;
    };


    /**
     * @brief The NJointCartesianTorqueController class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointCartesianTorqueController :
        public NJointControllerWithTripleBuffer<NJointCartesianTorqueControllerControlData>,
        public NJointCartesianTorqueControllerInterface
    {
    public:
        using ConfigPtrT = NJointCartesianTorqueControllerConfigPtr;
        NJointCartesianTorqueController(RobotUnitPtr prov, const NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const override;
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointCartesianTorqueControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointCartesianTorqueController(
            RobotUnitPtr prov,
            NJointCartesianTorqueControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);

        std::string getNodeSetName() const;
        static ::armarx::WidgetDescription::HBoxLayoutPtr createSliders();
        //WidgetDescription::HBoxLayoutPtr createJointSlidersLayout(float min, float max, float defaultValue) const;

    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        std::vector<ControlTarget1DoFActuatorTorque*> targets;
        //std::vector<const SensorValue1DoFActuatorPosition*> sensors;
        //std::vector<const SensorValue1DoFActuatorTorque*> torqueSensors;
        //std::vector<const SensorValue1DoFGravityTorque*> gravityTorqueSensors;

        std::string nodeSetName;

        VirtualRobot::DifferentialIKPtr ik;
        VirtualRobot::RobotNodePtr tcp;


        // NJointCartesianTorqueControllerInterface interface
    public:
        void setControllerTarget(float forceX, float forceY, float forceZ, float torqueX, float torqueY, float torqueZ, const Ice::Current&) override;
    };

} // namespace armarx

