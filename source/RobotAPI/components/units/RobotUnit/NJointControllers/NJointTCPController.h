/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include "NJointController.h"
#include <VirtualRobot/Robot.h>
#include "../RobotUnit.h"
#include "../ControlTargets/ControlTarget1DoFActuator.h"
#include "../SensorValues/SensorValue1DoFActuator.h"
#include <VirtualRobot/IK/DifferentialIK.h>

namespace armarx
{

    TYPEDEF_PTRS_HANDLE(NJointTCPController);
    TYPEDEF_PTRS_HANDLE(NJointTCPControllerConfig);

    class NJointTCPControllerConfig : virtual public NJointControllerConfig
    {
    public:
        NJointTCPControllerConfig(std::string const& nodeSetName, const std::string& tcpName):
            nodeSetName(nodeSetName),
            tcpName(tcpName)
        {}

        const std::string nodeSetName;
        const std::string tcpName;
    };

    TYPEDEF_PTRS_HANDLE(NJointTCPControllerControlData);
    class NJointTCPControllerControlData
    {
    public:
        float xVel = 0;
        float yVel = 0;
        float zVel = 0;
        float rollVel = 0;
        float pitchVel = 0;
        float yawVel = 0;
        VirtualRobot::IKSolver::CartesianSelection mode = VirtualRobot::IKSolver::All;
    };


    /**
     * @brief The NJointTCPController class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointTCPController :
        public NJointControllerWithTripleBuffer<NJointTCPControllerControlData>
    {
    public:
        using ConfigPtrT = NJointTCPControllerConfigPtr;
        NJointTCPController(RobotUnitPtr prov, const NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const override;
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointTCPControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointTCPController(
            RobotUnitPtr prov,
            NJointTCPControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);

        // for TCPControlUnit
        void setVelocities(float xVel, float yVel, float zVel, float rollVel, float pitchVel, float yawVel, VirtualRobot::IKSolver::CartesianSelection mode);
        std::string getNodeSetName() const;
        static ::armarx::WidgetDescription::WidgetSeq createSliders();
    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        std::vector<ControlTarget1DoFActuatorVelocity*> targets;
        std::vector<const SensorValue1DoFActuatorPosition*> sensors;

        VirtualRobot::RobotNodePtr tcp;
        VirtualRobot::DifferentialIKPtr ik;

        std::string nodeSetName;
    };

} // namespace armarx

