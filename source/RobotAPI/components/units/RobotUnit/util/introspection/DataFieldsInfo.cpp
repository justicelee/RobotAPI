/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DataFieldsInfo.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/OrientedPoint.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/units/KinematicUnitInterfaceStdOverloads.h>

//Eigen::Vector3f
namespace armarx::introspection
{
    std::size_t DataFieldsInfo<Eigen::Vector3f, void>::GetNumberOfFields()
    {
        return 3;
    }
    void DataFieldsInfo<Eigen::Vector3f, void>::GetDataFieldAs(std::size_t i, const Eigen::Vector3f& field, std::string& out)
    {
        ARMARX_CHECK_LESS(i, 3);
        out = to_string(field(i));
    }
    void DataFieldsInfo<Eigen::Vector3f, void>::GetDataFieldAs(std::size_t i, const Eigen::Vector3f& field, float& out)
    {
        ARMARX_CHECK_LESS(i, 3);
        out = field(i);
    }
    const std::type_info& DataFieldsInfo<Eigen::Vector3f, void>::GetDataFieldType(std::size_t i)
    {
        return typeid(float);
    }
    const std::vector<std::string>& DataFieldsInfo<Eigen::Vector3f, void>::GetFieldNames()
    {
        static const std::vector<std::string> result{"x", "y", "z"};
        return result;
    }
    std::map<std::string, VariantBasePtr> DataFieldsInfo<Eigen::Vector3f, void>::ToVariants(
        const Eigen::Vector3f& value,
        const std::string& name,
        const IceUtil::Time& timestamp,
        const std::string& frame,
        const std::string& agent)
    {
        if (!frame.empty())
        {
            return {{name, {new TimedVariant(FramedPosition{value, frame, agent}, timestamp)}}};
        }
        ARMARX_CHECK_EXPRESSION(agent.empty()) << "No frame but an agent given";
        return {{name, {new TimedVariant(Vector3{value}, timestamp)}}};
    }
}
//Eigen::Vector##Num##Type
namespace armarx::introspection
{
#define make_DataFieldsInfo_for_eigen_vector(Type,TypeName,Num)                                                                                     \
    void DataFieldsInfo<Eigen::Vector##Num##Type, void>::GetDataFieldAs(std::size_t i, const Eigen::Vector##Num##Type& field, std::string& out)     \
    {                                                                                                                                               \
        ARMARX_CHECK_LESS(i, Num);                                                                                                                  \
        out = to_string(field(i));                                                                                                                  \
    }                                                                                                                                               \
    void DataFieldsInfo<Eigen::Vector##Num##Type, void>::GetDataFieldAs(std::size_t i, const Eigen::Vector##Num##Type& field, Ice::TypeName& out)   \
    {                                                                                                                                               \
        ARMARX_CHECK_LESS(i, Num);                                                                                                                  \
        out = field(i);                                                                                                                             \
    }                                                                                                                                               \
    const std::type_info& DataFieldsInfo<Eigen::Vector##Num##Type, void>::GetDataFieldType(std::size_t i)                                           \
    {                                                                                                                                               \
        return typeid(Ice::TypeName);                                                                                                               \
    }                                                                                                                                               \
    std::map<std::string, VariantBasePtr> DataFieldsInfo<Eigen::Vector##Num##Type, void>::ToVariants(                                               \
            const Eigen::Vector##Num##Type& value,                                                                                                  \
            const std::string& name,                                                                                                                \
            const IceUtil::Time& timestamp,                                                                                                         \
            const std::string& frame,                                                                                                               \
            const std::string& agent)                                                                                                               \
    {                                                                                                                                               \
        ARMARX_CHECK_EXPRESSION(frame.empty() && agent.empty()) << "There is no framed version of TimestampVariant";                                \
        std::map<std::string, VariantBasePtr> result;                                                                                               \
        for(int i = 0; i < Num; ++i)                                                                                                                \
        {                                                                                                                                           \
            result.emplace(name + "." + to_string(i), VariantBasePtr{new TimedVariant(value(i), timestamp)});                                       \
        }                                                                                                                                           \
        return result;                                                                                                                              \
    }                                                                                                                                               \
    const std::vector<std::string>& DataFieldsInfo<Eigen::Vector##Num##Type, void>::GetFieldNames()                                                 \
    {                                                                                                                                               \
        static const std::vector<std::string> result = []                                                                                           \
                {                                                                                                                                           \
                        std::vector<std::string> r;                                                                                                             \
                        for(int i = 0; i < Num; ++i)                                                                                                            \
    {                                                                                                                                       \
        r.emplace_back(to_string(i));                                                                                                       \
        }                                                                                                                                       \
        return r;                                                                                                                               \
                }();                                                                                                                                        \
        return result;                                                                                                                              \
    }

    make_DataFieldsInfo_for_eigen_vector(f, Float, 2)
    make_DataFieldsInfo_for_eigen_vector(f, Float, 4)
    make_DataFieldsInfo_for_eigen_vector(f, Float, 5)
    make_DataFieldsInfo_for_eigen_vector(f, Float, 6)

    make_DataFieldsInfo_for_eigen_vector(d, Double, 2)
    make_DataFieldsInfo_for_eigen_vector(d, Double, 3)
    make_DataFieldsInfo_for_eigen_vector(d, Double, 4)
    make_DataFieldsInfo_for_eigen_vector(d, Double, 5)
    make_DataFieldsInfo_for_eigen_vector(d, Double, 6)

    make_DataFieldsInfo_for_eigen_vector(i, Int, 2)
    make_DataFieldsInfo_for_eigen_vector(i, Int, 3)
    make_DataFieldsInfo_for_eigen_vector(i, Int, 4)
    make_DataFieldsInfo_for_eigen_vector(i, Int, 5)
    make_DataFieldsInfo_for_eigen_vector(i, Int, 6)
#undef make_DataFieldsInfo_for_eigen_vector
}
//Eigen::Matrix4f
namespace armarx::introspection
{
    std::size_t DataFieldsInfo<Eigen::Matrix4f, void>::GetNumberOfFields()
    {
        return 16;
    }
    void DataFieldsInfo<Eigen::Matrix4f, void>::GetDataFieldAs(std::size_t i, const Eigen::Matrix4f& field, float& out)
    {
        ARMARX_CHECK_LESS(i, 16);
        out = field(i / 4, i % 4);
    }
    void DataFieldsInfo<Eigen::Matrix4f, void>::GetDataFieldAs(std::size_t i, const Eigen::Matrix4f& field, std::string& out)
    {
        ARMARX_CHECK_LESS(i, 16);
        out = to_string(field(i / 4, i % 4));
    }
    const std::type_info& DataFieldsInfo<Eigen::Matrix4f, void>::GetDataFieldType(std::size_t i)
    {
        return typeid(float);
    }
    const std::vector<std::string>& DataFieldsInfo<Eigen::Matrix4f, void>::GetFieldNames()
    {
        static const std::vector<std::string> result
        {
            "00", "01", "02", "03",
            "10", "11", "12", "13",
            "20", "21", "22", "23",
            "30", "31", "32", "33"
        };
        return result;
    }
    std::map<std::string, VariantBasePtr> DataFieldsInfo<Eigen::Matrix4f, void>::ToVariants(
        const Eigen::Matrix4f& value,
        const std::string& name,
        const IceUtil::Time& timestamp,
        const std::string& frame,
        const std::string& agent)
    {
        if (!frame.empty())
        {
            return {{name, {new TimedVariant(FramedPose{value, frame, agent}, timestamp)}}};
        }
        ARMARX_CHECK_EXPRESSION(agent.empty()) << "No frame but an agent given";
        return {{name, {new TimedVariant(Pose{value}, timestamp)}}};
    }
}
//Eigen::Quaternionf
namespace armarx::introspection
{
    std::size_t DataFieldsInfo<Eigen::Quaternionf, void>::GetNumberOfFields()
    {
        return 4;
    }
    void DataFieldsInfo<Eigen::Quaternionf, void>::GetDataFieldAs(std::size_t i, const Eigen::Quaternionf& field, float& out)
    {
        ARMARX_CHECK_LESS(i, 4);
        switch (i)
        {
            case 0:
                out = field.w();
                return;
            case 1:
                out = field.x();
                return;
            case 2:
                out = field.y();
                return;
            case 3:
                out = field.z();
                return;
        }
        throw std::logic_error
        {
            __FILE__ " : " + to_string(__LINE__) +
            " : Unreachable code reached"
        };

    }
    void DataFieldsInfo<Eigen::Quaternionf, void>::GetDataFieldAs(std::size_t i, const Eigen::Quaternionf& field, std::string& out)
    {
        ARMARX_CHECK_LESS(i, 4);
        switch (i)
        {
            case 0:
                out = to_string(field.w());
                return;
            case 1:
                out = to_string(field.x());
                return;
            case 2:
                out = to_string(field.y());
                return;
            case 3:
                out = to_string(field.z());
                return;
        }
        throw std::logic_error
        {
            __FILE__ " : " + to_string(__LINE__) +
            " : Unreachable code reached"
        };

    }

    const std::type_info& DataFieldsInfo<Eigen::Quaternionf, void>::GetDataFieldType(std::size_t i)
    {
        return typeid(float);
    }
    const std::vector<std::string>& DataFieldsInfo<Eigen::Quaternionf, void>::GetFieldNames()
    {
        static const std::vector<std::string> result{"qw", "qx", "qy", "qz"};
        return result;
    }

    std::map<std::string, VariantBasePtr> DataFieldsInfo<Eigen::Quaternionf, void>::ToVariants(
        const Eigen::Quaternionf& value,
        const std::string& name,
        const IceUtil::Time& timestamp,
        const std::string& frame,
        const std::string& agent)
    {
        if (!frame.empty())
        {
            return {{name, {new TimedVariant(FramedOrientation{value, frame, agent}, timestamp)}}};
        }
        ARMARX_CHECK_EXPRESSION(agent.empty()) << "No frame but an agent given";
        return {{name, {new TimedVariant(Quaternion{value}, timestamp)}}};
    }
}
//std::chrono::microseconds
namespace armarx::introspection
{
    std::size_t DataFieldsInfo<std::chrono::microseconds, void>::GetNumberOfFields()
    {
        return 1;
    }
    void DataFieldsInfo<std::chrono::microseconds, void>::GetDataFieldAs(std::size_t i, const std::chrono::microseconds& field, long& out)
    {
        ARMARX_CHECK_EQUAL(i, 0);
        out = field.count();
    }
    void DataFieldsInfo<std::chrono::microseconds, void>::GetDataFieldAs(std::size_t i, const std::chrono::microseconds& field, std::string& out)
    {
        ARMARX_CHECK_EQUAL(i, 0);
        out = to_string(field.count());
    }
    const std::type_info& GetDataFieldType(std::size_t i)
    {
        return typeid(long);
    }
    std::map<std::string, VariantBasePtr> DataFieldsInfo<std::chrono::microseconds, void>::ToVariants(
        std::chrono::microseconds value,
        const std::string& name,
        const IceUtil::Time& timestamp,
        const std::string& frame,
        const std::string& agent)
    {
        ARMARX_CHECK_EXPRESSION(frame.empty() && agent.empty()) << "There is no framed version of TimestampVariant";
        return {{name, {new TimedVariant(TimestampVariant{value.count()}, timestamp)}}};
    }

}
//IceUtil::Time
namespace armarx::introspection
{
    std::size_t DataFieldsInfo<IceUtil::Time, void>::GetNumberOfFields()
    {
        return 1;
    }
    void DataFieldsInfo<IceUtil::Time, void>::GetDataFieldAs(std::size_t i, const IceUtil::Time& field, long& out)
    {
        ARMARX_CHECK_EQUAL(i, 0);
        out = field.toMicroSeconds();
    }
    void DataFieldsInfo<IceUtil::Time, void>::GetDataFieldAs(std::size_t i, const IceUtil::Time& field, std::string& out)
    {
        ARMARX_CHECK_EQUAL(i, 0);
        out = to_string(field.toMicroSeconds());
    }
    const std::type_info& DataFieldsInfo<IceUtil::Time, void>::GetDataFieldType(std::size_t i)
    {
        return typeid(long);
    }
    std::map<std::string, VariantBasePtr> DataFieldsInfo<IceUtil::Time, void>::ToVariants(
        IceUtil::Time value,
        const std::string& name,
        const IceUtil::Time& timestamp,
        const std::string& frame,
        const std::string& agent)
    {
        ARMARX_CHECK_EXPRESSION(frame.empty() && agent.empty()) << "There is no framed version of TimestampVariant";
        return {{name, {new TimedVariant(TimestampVariant{value.toMicroSeconds()}, timestamp)}}};
    }
}
//JointStatus
namespace armarx::introspection
{
    std::size_t DataFieldsInfo<JointStatus, void>::GetNumberOfFields()
    {
        return 4;
    }
    void DataFieldsInfo<JointStatus, void>::GetDataFieldAs(
        std::size_t i, const JointStatus& field, std::string& out)
    {
        ARMARX_CHECK_LESS(i, 4);
        switch (i)
        {
            case 0:
                out = to_string(field.error);
                return;
            case 1:
                out = to_string(field.operation);
                return;
            case 2:
                out = to_string(field.enabled);
                return;
            case 3:
                out = to_string(field.emergencyStop);
                return;
        }
        throw std::logic_error
        {
            __FILE__ " : " + to_string(__LINE__) +
            " : Unreachable code reached"
        };
    }
    void DataFieldsInfo<JointStatus, void>::GetDataFieldAs(
        std::size_t i, const JointStatus& field, Ice::Int& out)
    {
        ARMARX_CHECK_EXPRESSION(i == 0 || i == 1);
        switch (i)
        {
            case 0:
                out = field.enabled;
                return;
            case 1:
                out = field.emergencyStop;
                return;
        }
        throw std::logic_error
        {
            __FILE__ " : " + to_string(__LINE__) +
            " : Unreachable code reached"
        };
    }
    void DataFieldsInfo<JointStatus, void>::GetDataFieldAs(
        std::size_t i, const JointStatus& field, bool& out)
    {
        ARMARX_CHECK_EXPRESSION(i == 2 || i == 3);
        switch (i)
        {
            case 2:
                out = static_cast<Ice::Int>(field.error);
                return;
            case 3:
                out = static_cast<Ice::Int>(field.operation);
                return;
        }
        throw std::logic_error
        {
            __FILE__ " : " + to_string(__LINE__) +
            " : Unreachable code reached"
        };
    }
    const std::type_info& DataFieldsInfo<JointStatus, void>::GetDataFieldType(std::size_t i)
    {
        ARMARX_CHECK_LESS(i, 4);
        switch (i)
        {
            case 0:
                return typeid(Ice::Int);
            case 1:
                return typeid(Ice::Int);
            case 2:
                return typeid(bool);
            case 3:
                return typeid(bool);
        }
        throw std::logic_error
        {
            __FILE__ " : " + to_string(__LINE__) +
            " : Unreachable code reached"
        };
    }

    const std::vector<std::string>& DataFieldsInfo<JointStatus, void>::GetFieldNames()
    {
        static const std::vector<std::string> result{"error", "operation", "enabled", "emergencyStop"};
        return result;
    }

    std::map<std::string, VariantBasePtr> DataFieldsInfo<JointStatus, void>::ToVariants(
        const JointStatus& value,
        const std::string& name,
        const IceUtil::Time& timestamp,
        const std::string& frame,
        const std::string& agent)
    {
        ARMARX_CHECK_EXPRESSION(frame.empty() && agent.empty()) << "There is no framed version for JointStatus";
        static const auto fnames = GetFieldNames();
        return
        {
            {name + fnames.at(0), {new TimedVariant{to_string(value.error), timestamp}}},
            {name + fnames.at(1), {new TimedVariant{to_string(value.operation), timestamp}}},
            {name + fnames.at(2), {new TimedVariant{value.enabled, timestamp}}},
            {name + fnames.at(3), {new TimedVariant{value.emergencyStop, timestamp}}}
        };
    }
}
