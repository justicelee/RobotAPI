/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "DataFieldsInfo.h"
namespace armarx::introspection
{

    template<class CommonBaseT, class ClassT>
    struct ClassMemberInfo;
    template<class CommonBaseT, class ClassT>
    struct ClassMemberInfoEntryConfigurator;

    template<class CommonBaseT>
    struct ClassMemberInfoEntry
    {
    public:
        using CommonBaseType = CommonBaseT;

        ClassMemberInfoEntry(ClassMemberInfoEntry&&) = default;
        ClassMemberInfoEntry(const ClassMemberInfoEntry&) = default;
        ClassMemberInfoEntry& operator=(ClassMemberInfoEntry&&) = default;
        ClassMemberInfoEntry& operator=(const ClassMemberInfoEntry&) = default;

        template<class ClassType, class MemberType>
        ClassMemberInfoEntry(const std::string& memberName, MemberType ClassType::* ptr):
            className {GetTypeString<ClassType>()},
            memberName {memberName},
            memberTypeName {GetTypeString<MemberType>()},
            numberOfFields {DataFieldsInfo<MemberType>::GetNumberOfFields()},
            fieldToType {MakeFieldToTypeFunctors<ClassType>(numberOfFields, ptr)},
            toVariants_ {MakeToVariantsFunctor<ClassType>(ptr)}
        {
            ARMARX_CHECK_NOT_EQUAL(0, numberOfFields);
            //field names
            {
                fieldNames.reserve(numberOfFields);
                if (numberOfFields == 1)
                {
                    fieldNames.emplace_back(memberName);
                }
                else
                {
                    for (auto& name : DataFieldsInfo<MemberType>::GetFieldNames())
                    {
                        fieldNames.emplace_back(memberName + "." + std::move(name));
                    }
                }
            }
        }

        //general
        const std::string& getClassName() const
        {
            return className;
        }
        const std::string& getMemberName() const
        {
            return memberName;
        }
        const std::string& getMemberTypeName() const
        {
            return memberTypeName;
        }
        //fields
        std::size_t getNumberOfFields() const
        {
            return numberOfFields;
        }
        const std::string& getFieldName(std::size_t i) const
        {
            ARMARX_CHECK_LESS(i, numberOfFields);
            return fieldNames.at(i);
        }
        void getDataFieldAs(std::size_t i, const CommonBaseType* ptr, bool&        out) const
        {
            ARMARX_CHECK_NOT_NULL(ptr);
            ARMARX_CHECK_LESS(i, numberOfFields);
            return fieldToType.at(i).toBool(ptr, out);
        }
        void getDataFieldAs(std::size_t i, const CommonBaseType* ptr, Ice::Byte&   out) const
        {
            ARMARX_CHECK_NOT_NULL(ptr);
            ARMARX_CHECK_LESS(i, numberOfFields);
            return fieldToType.at(i).toByte(ptr, out);
        }
        void getDataFieldAs(std::size_t i, const CommonBaseType* ptr, Ice::Short&  out) const
        {
            ARMARX_CHECK_NOT_NULL(ptr);
            ARMARX_CHECK_LESS(i, numberOfFields);
            return fieldToType.at(i).toShort(ptr, out);
        }
        void getDataFieldAs(std::size_t i, const CommonBaseType* ptr, Ice::Int&    out) const
        {
            ARMARX_CHECK_NOT_NULL(ptr);
            ARMARX_CHECK_LESS(i, numberOfFields);
            return fieldToType.at(i).toInt(ptr, out);
        }
        void getDataFieldAs(std::size_t i, const CommonBaseType* ptr, Ice::Long&   out) const
        {
            ARMARX_CHECK_NOT_NULL(ptr);
            ARMARX_CHECK_LESS(i, numberOfFields);
            return fieldToType.at(i).toLong(ptr, out);
        }
        void getDataFieldAs(std::size_t i, const CommonBaseType* ptr, Ice::Float&  out) const
        {
            ARMARX_CHECK_NOT_NULL(ptr);
            ARMARX_CHECK_LESS(i, numberOfFields);
            return fieldToType.at(i).toFloat(ptr, out);
        }
        void getDataFieldAs(std::size_t i, const CommonBaseType* ptr, Ice::Double& out) const
        {
            ARMARX_CHECK_NOT_NULL(ptr);
            ARMARX_CHECK_LESS(i, numberOfFields);
            return fieldToType.at(i).toDouble(ptr, out);
        }
        void getDataFieldAs(std::size_t i, const CommonBaseType* ptr, std::string& out) const
        {
            ARMARX_CHECK_NOT_NULL(ptr);
            ARMARX_CHECK_LESS(i, numberOfFields);
            return fieldToType.at(i).toString(ptr, out);
        }
        const std::type_info& getFieldTypeId(std::size_t i) const
        {
            ARMARX_CHECK_LESS(i, numberOfFields);
            return *fieldToType.at(i).typeId;
        }

        //variants
        std::map<std::string, VariantBasePtr> toVariants(const IceUtil::Time& timestamp, const CommonBaseType* ptr) const
        {
            return toVariants_(this, timestamp, ptr);
        }

    private:
        using ToVariantsFunctorType = std::function <
                                      std::map<std::string, VariantBasePtr>(
                                          const ClassMemberInfoEntry* thisptr,
                                          const IceUtil::Time&,
                                          const CommonBaseType*) >;
        struct FieldToType
        {
            template<class T>
            using FieldToFunctorType = std::function<void(const CommonBaseType*, T&)>;
            FieldToFunctorType<bool>        toBool;
            FieldToFunctorType<Ice::Byte>   toByte;
            FieldToFunctorType<Ice::Short>  toShort;
            FieldToFunctorType<Ice::Int>    toInt;
            FieldToFunctorType<Ice::Long>   toLong;
            FieldToFunctorType<Ice::Float>  toFloat;
            FieldToFunctorType<Ice::Double> toDouble;
            FieldToFunctorType<std::string> toString;
            const std::type_info*           typeId;
        };

        template<class ClassType, class MemberType, class MemberPtrClassType>
        static std::vector<FieldToType> MakeFieldToTypeFunctors(
            std::size_t numberOfFields,
            MemberType MemberPtrClassType::* ptr)
        {
            std::vector<FieldToType> result;
            result.reserve(numberOfFields);
            for (std::size_t i = 0; i < numberOfFields; ++i)
            {
                result.emplace_back();
                auto& fieldData = result.back();

#define make_getter(Type) [i, ptr](const CommonBaseType * ptrBase, Type & out)  \
    {                                                                           \
        const ClassType* cptr = dynamic_cast<const ClassType*>(ptrBase);        \
        ARMARX_CHECK_NOT_NULL(cptr);                                            \
        DataFieldsInfo<MemberType>::GetDataFieldAs(i, cptr->*ptr, out);         \
    }
                fieldData.toBool   = make_getter(bool);
                fieldData.toByte   = make_getter(Ice::Byte);
                fieldData.toShort  = make_getter(Ice::Short);
                fieldData.toInt    = make_getter(Ice::Int);
                fieldData.toLong   = make_getter(Ice::Long);
                fieldData.toFloat  = make_getter(Ice::Float);
                fieldData.toDouble = make_getter(Ice::Double);
                fieldData.toString = make_getter(std::string);
#undef make_getter
                fieldData.typeId   = &DataFieldsInfo<MemberType>::GetDataFieldType(i);
            }
            return result;
        }

        template<class ClassType, class MemberType, class MemberPtrClassType>
        static ToVariantsFunctorType MakeToVariantsFunctor(MemberType MemberPtrClassType::* ptr)
        {
            return [ptr](
                       const ClassMemberInfoEntry * thisptr,
                       const IceUtil::Time & timestamp,
                       const CommonBaseType * ptrBase)
            {
                const ClassType* cptr = dynamic_cast<const ClassType*>(ptrBase);
                ARMARX_CHECK_NOT_NULL(cptr);
                return DataFieldsInfo<MemberType>::ToVariants(
                           cptr->*ptr, thisptr->getMemberName(),
                           timestamp,
                           thisptr->variantReportFrame(), thisptr->variantReportAgent());
            };
        }

        template<class BaseType, class ClassT>
        friend class ClassMemberInfo;
        template<class BaseType, class ClassT>
        friend class ClassMemberInfoEntryConfigurator;

        const std::string className;
        const std::string memberName;
        const std::string memberTypeName;
        //elementar subparts
        const std::size_t numberOfFields;
        const std::vector<FieldToType> fieldToType;
        std::vector<std::string> fieldNames;
        //variants
        ToVariantsFunctorType toVariants_;
        bool  setVariantReportFrame_ {false};
        std::function<std::string()> variantReportFrame {[]{return "";}};
        std::function<std::string()> variantReportAgent {[]{return "";}};
    };

    template<class CommonBaseT, class ClassT>
    struct ClassMemberInfoEntryConfigurator
    {
        using ClassType = ClassT;
        using CommonBaseType = CommonBaseT;
        using Entry = ClassMemberInfoEntry<CommonBaseType>;

        ClassMemberInfoEntryConfigurator& setFieldNames(std::vector<std::string> fieldNames)
        {
            ARMARX_CHECK_EQUAL(fieldNames.size(), e.numberOfFields);
            e.fieldNames = std::move(fieldNames);
            return *this;
        }
        ClassMemberInfoEntryConfigurator& setVariantReportFrame(const std::string& agent, const std::string& frame)
        {
            e.setVariantReportFrame_ = true;
            e.variantReportFrame = [frame] {return frame;};
            e.variantReportAgent = [agent] {return agent;};
            return *this;
        }
        ClassMemberInfoEntryConfigurator& setVariantReportFrame(const std::string& agent, std::function<std::string()> frame)
        {
            e.setVariantReportFrame_ = true;
            e.variantReportFrame = std::move(frame);
            e.variantReportAgent = [agent] {return agent;};
            return *this;
        }
        ClassMemberInfoEntryConfigurator& setVariantReportFrame(std::function<std::string()> agent, const std::string& frame)
        {
            e.setVariantReportFrame_ = true;
            e.variantReportFrame = [frame] {return frame;};
            e.variantReportAgent = std::move(agent);
            return *this;
        }
        ClassMemberInfoEntryConfigurator& setVariantReportFrame(std::function<std::string()> agent, std::function<std::string()> frame)
        {
            e.setVariantReportFrame_ = true;
            e.variantReportFrame = std::move(frame);
            e.variantReportAgent = std::move(agent);
            return *this;
        }

        ClassMemberInfoEntryConfigurator& setVariantReportFunction(
            std::function<std::map<std::string, VariantBasePtr>(const IceUtil::Time&, const ClassType*)> f)
        {
            e.toVariants_ = [f](
                                const ClassMemberInfoEntry<CommonBaseType>* thisptr,
                                const IceUtil::Time & timestamp,
                                const CommonBaseType * ptrBase)
            {
                const ClassType* ptr = dynamic_cast<const ClassType*>(ptrBase);
                ARMARX_CHECK_NOT_NULL(ptr);
                return f(timestamp, ptr);
            };
            return *this;
        }

    private:
        friend struct ClassMemberInfo<CommonBaseType, ClassType>;
        ClassMemberInfoEntryConfigurator(Entry& e): e(e) {}
        Entry& e;
    };
}
