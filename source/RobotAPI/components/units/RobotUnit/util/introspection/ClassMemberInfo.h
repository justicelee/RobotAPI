/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/util/algorithm.h>

#include "ClassMemberInfoEntry.h"

namespace armarx::introspection
{
    /**
     * @brief
     */
    template<class CommonBaseT, class ClassT>
    struct ClassMemberInfo
    {
        using ClassType = ClassT;
        using CommonBaseType = CommonBaseT;
        static_assert(std::is_base_of<CommonBaseType, ClassType>::value, "The class has to inherit the common base class");
        using Entry = introspection::ClassMemberInfoEntry<CommonBaseType>;
        template<class T>
        using EntryConfigurator = introspection::ClassMemberInfoEntryConfigurator<CommonBaseType, T>;

        static const ClassMemberInfo<CommonBaseT, ClassT>& GetInstance();

        /// @brief add a member variable of the current class
        template<class MemberType>
        EntryConfigurator<ClassType> addMemberVariable(MemberType ClassType::* ptr, const std::string& name);

        /// @brief add all variables of a base class of the current class
        template<class BaseClassType>
        void addBaseClass();

        /// @brief Get the name of the current class
        static const std::string& GetClassName();
        /// @brief Get all entries for member variables
        static const KeyValueVector<std::string, Entry>& GetEntries();
        static std::size_t GetNumberOfDataFields();
        static void GetDataFieldAs(const ClassType* ptr, std::size_t i, bool&        out);
        static void GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Byte&   out);
        static void GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Short&  out);
        static void GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Int&    out);
        static void GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Long&   out);
        static void GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Float&  out);
        static void GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Double& out);
        static void GetDataFieldAs(const ClassType* ptr, std::size_t i, std::string& out);
        static const std::type_info& GetDataFieldType(std::size_t i);
        static std::vector<std::string> GetDataFieldNames();

        static std::map<std::string, VariantBasePtr> ToVariants(const IceUtil::Time& timestamp, const CommonBaseT* ptr);
    private:
        template<class T>
        static auto MakeConverter()
        {
            ARMARX_TRACE;
            std::vector<std::function<void(const ClassType*, T&)>> functions;

            for (std::size_t idxEntr = 0; idxEntr < GetEntries().size(); ++idxEntr)
            {
                for (std::size_t idxField = 0; idxField < GetEntries().at(idxEntr).getNumberOfFields(); ++idxField)
                {
                    functions.emplace_back(
                        [idxEntr, idxField](const ClassType * classptr, T & out)
                    {
                        const Entry& e = GetEntries().at(idxEntr);
                        e.getDataFieldAs(idxField, classptr, out);
                    });
                }
            }
            ARMARX_CHECK_EQUAL(functions.size(), GetNumberOfDataFields());
            return functions;
        }

    private:
        KeyValueVector<std::string, Entry> entries;
        std::set<std::string> addedBases;
    };
}


//implementation
namespace armarx::introspection
{
    template<class CommonBaseT, class ClassT>
    template<class MemberType>
    ClassMemberInfo<CommonBaseT, ClassT>::EntryConfigurator<ClassT> ClassMemberInfo<CommonBaseT, ClassT>::addMemberVariable(MemberType ClassType::* ptr, const std::string& name)
    {
        ARMARX_TRACE;
        ARMARX_CHECK_EQUAL(0, entries.count(name));
        entries.add(name, Entry(name, ptr));
        return entries.at(name);
    }

    template<class CommonBaseT, class ClassT>
    template<class BaseClassType>
    void ClassMemberInfo<CommonBaseT, ClassT>::addBaseClass()
    {
        ARMARX_TRACE;
        if (std::is_same<BaseClassType, CommonBaseType>::value)
        {
            return;
        }
        static_assert(std::is_base_of<CommonBaseType, BaseClassType>::value, "The base class has to inherit the common base class");
        static_assert(std::is_base_of<BaseClassType, ClassType>::value, "The base class has to be a base class");
        static_assert(!std::is_same<BaseClassType, ClassType>::value, "The base class has must not be the class");

        std::set<std::string> basesAddedInCall;
        if (addedBases.count(ClassMemberInfo<CommonBaseType, BaseClassType>::GetClassName()))
        {
            return;
        }
        for (const Entry& e : ClassMemberInfo<CommonBaseType, BaseClassType>::GetEntries().values())
        {
            if (!addedBases.count(e.getClassName()))
            {
                ARMARX_CHECK_EXPRESSION(
                    !entries.count(e.getMemberName())) <<
                                                       "Adding the base class '" << GetTypeString<BaseClassType>()
                                                       << "' adds an entry for the member '" << e.getMemberName()
                                                       << "' which already was added by class '" << entries.at(e.getMemberName()).getClassName() << "'";
                basesAddedInCall.emplace(e.getClassName());
                entries.add(e.getMemberName(), e);
            }
        }
        addedBases.insert(basesAddedInCall.begin(), basesAddedInCall.end());
    }

    template<class CommonBaseT, class ClassT>
    std::vector<std::string> ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldNames()
    {
        ARMARX_TRACE;
        std::vector<std::string> dataFieldNames;
        dataFieldNames.reserve(GetNumberOfDataFields());
        for (const Entry& e : GetEntries().values())
        {
            for (std::size_t i = 0; i < e.getNumberOfFields(); ++i)
            {
                dataFieldNames.emplace_back(e.getFieldName(i));
            }
        }
        ARMARX_CHECK_EQUAL(GetNumberOfDataFields(), dataFieldNames.size());
        return dataFieldNames;
    }

    template<class CommonBaseT, class ClassT>
    std::map<std::string, VariantBasePtr> ClassMemberInfo<CommonBaseT, ClassT>::ToVariants(const IceUtil::Time& timestamp, const CommonBaseT* ptr)
    {
        ARMARX_TRACE;
        std::map<std::string, VariantBasePtr> result;
        for (const auto& e : GetEntries().values())
        {
            mergeMaps(result, e.toVariants(timestamp, ptr), MergeMapsMode::OverrideNoValues);
        }
        return result;
    }

    template<class CommonBaseT, class ClassT>
    const ClassMemberInfo<CommonBaseT, ClassT>& ClassMemberInfo<CommonBaseT, ClassT>::GetInstance()
    {
        static const ClassMemberInfo<CommonBaseT, ClassT> i = ClassT::GetClassMemberInfo();
        return i;
    }

    template<class CommonBaseT, class ClassT>
    const std::string& ClassMemberInfo<CommonBaseT, ClassT>::GetClassName()
    {
        return GetTypeString<ClassType>();
    }

    template<class CommonBaseT, class ClassT>
    const KeyValueVector<std::string, ClassMemberInfoEntry<CommonBaseT>>& ClassMemberInfo<CommonBaseT, ClassT>::GetEntries()
    {
        return GetInstance().entries;
    }

    template<class CommonBaseT, class ClassT>
    std::size_t ClassMemberInfo<CommonBaseT, ClassT>::GetNumberOfDataFields()
    {
        ARMARX_TRACE;
        static const std::size_t numberOfDataFields = []
        {
            std::size_t n = 0;
            for (const Entry& e : GetEntries().values())
            {
                n += e.getNumberOfFields();
            }
            return n;
        }();
        return numberOfDataFields;
    }

    template<class CommonBaseT, class ClassT>
    void ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldAs(const ClassType* ptr, std::size_t i, bool&        out)
    {
        ARMARX_TRACE;
        static const auto convert = MakeConverter<bool>();
        ARMARX_CHECK_LESS(i, GetNumberOfDataFields());
        convert.at(i)(ptr, out);
    }
    template<class CommonBaseT, class ClassT>
    void ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Byte&   out)
    {
        ARMARX_TRACE;
        static const auto convert = MakeConverter<Ice::Byte>();
        ARMARX_CHECK_LESS(i, GetNumberOfDataFields());
        convert.at(i)(ptr, out);
    }
    template<class CommonBaseT, class ClassT>
    void ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Short&  out)
    {
        ARMARX_TRACE;
        static const auto convert = MakeConverter<Ice::Short>();
        ARMARX_CHECK_LESS(i, GetNumberOfDataFields());
        convert.at(i)(ptr, out);
    }
    template<class CommonBaseT, class ClassT>
    void ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Int&    out)
    {
        ARMARX_TRACE;
        static const auto convert = MakeConverter<Ice::Int>();
        ARMARX_CHECK_LESS(i, GetNumberOfDataFields());
        convert.at(i)(ptr, out);
    }
    template<class CommonBaseT, class ClassT>
    void ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Long&   out)
    {
        ARMARX_TRACE;
        static const auto convert = MakeConverter<Ice::Long>();
        ARMARX_CHECK_LESS(i, GetNumberOfDataFields());
        convert.at(i)(ptr, out);
    }
    template<class CommonBaseT, class ClassT>
    void ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Float&  out)
    {
        ARMARX_TRACE;
        static const auto convert = MakeConverter<Ice::Float>();
        ARMARX_CHECK_LESS(i, GetNumberOfDataFields());
        convert.at(i)(ptr, out);
    }
    template<class CommonBaseT, class ClassT>
    void ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldAs(const ClassType* ptr, std::size_t i, Ice::Double& out)
    {
        ARMARX_TRACE;
        static const auto convert = MakeConverter<Ice::Double>();
        ARMARX_CHECK_LESS(i, GetNumberOfDataFields());
        convert.at(i)(ptr, out);
    }
    template<class CommonBaseT, class ClassT>
    void ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldAs(const ClassType* ptr, std::size_t i, std::string& out)
    {
        ARMARX_TRACE;
        static const auto convert = MakeConverter<std::string>();
        ARMARX_CHECK_LESS(i, GetNumberOfDataFields());
        convert.at(i)(ptr, out);
    }

    template<class CommonBaseT, class ClassT>
    const std::type_info& ClassMemberInfo<CommonBaseT, ClassT>::GetDataFieldType(std::size_t i)
    {
        ARMARX_TRACE;
        static const auto convert = []
        {
            std::vector<const std::type_info*> data;

            for (std::size_t idxEntr = 0; idxEntr < GetEntries().size(); ++idxEntr)
            {
                const Entry& e = GetEntries().at(idxEntr);
                for (std::size_t idxField = 0; idxField < GetEntries().at(idxEntr).getNumberOfFields(); ++idxField)
                {
                    data.emplace_back(&e.getFieldTypeId(idxField));
                }
            }
            ARMARX_CHECK_EQUAL(data.size(), GetNumberOfDataFields());
            return data;
        }();
        ARMARX_CHECK_LESS(i, GetNumberOfDataFields());
        return *convert.at(i);
    }
}
