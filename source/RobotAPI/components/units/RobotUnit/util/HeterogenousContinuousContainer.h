/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/TemplateMetaProgramming.h>
#include <ArmarXCore/core/util/PropagateConst.h>

#include "HeterogenousContinuousContainerMacros.h"

#if __GNUC__< 5
namespace std
{
    inline void* align(size_t alignment, size_t bytes, void*& bufferPlace, size_t& bufferSpace) noexcept
    {
        const auto uiptr = reinterpret_cast<uintptr_t>(bufferPlace);
        const auto alignedPlace = (uiptr - 1u + alignment) & -alignment;
        const auto spaceRequired = alignedPlace - uiptr;
        if ((bytes + spaceRequired) > bufferSpace)
        {
            return nullptr;
        }
        else
        {
            bufferSpace -= spaceRequired;
            return bufferPlace = reinterpret_cast<void*>(alignedPlace);
        }
    }
}
#endif

namespace armarx::detail
{
    template<class Base>
    class HeterogenousContinuousContainerBase
    {
    public:
        bool empty() const
        {
            return begin_ == current_;
        }
        bool owning() const
        {
            return storage_ != nullptr;
        }
        std::size_t getRemainingStorageCapacity() const
        {
            return static_cast<const std::uint8_t*>(end_) -
                   static_cast<const std::uint8_t*>(current_);
        }
        std::size_t getStorageCapacity() const
        {
            return static_cast<const std::uint8_t*>(end_) -
                   static_cast<const std::uint8_t*>(begin_);
        }
        std::size_t getUsedStorageCapacity() const
        {
            return static_cast<const std::uint8_t*>(current_) -
                   static_cast<const std::uint8_t*>(begin_);
        }
        void assignStorage(void* begin, void* end)
        {
            ARMARX_CHECK_EXPRESSION(empty());
            ARMARX_CHECK_LESS_EQUAL(begin, end);
            storage_ = nullptr;
            begin_ = begin;
            current_ = begin;
            end_ = end;
        }
        void setStorageCapacity(std::size_t sz)
        {
            ARMARX_CHECK_EXPRESSION(empty());
            if (!sz)
            {
                assignStorage(nullptr, nullptr);
                return;
            }
            //replache with void* aligned_alloc( std::size_t alignment, std::size_t size ) @c++17
            sz += 63;
            storage_.reset(new std::uint8_t[sz]);
            begin_ = static_cast<void*>(storage_.get());
            current_ = begin_;
            end_ = static_cast<void*>(storage_.get() + sz);
        }
    protected:
        template<class Derived>
        Base* pushBack(const Derived* d)
        {
            static_assert(
                std::is_base_of<Base, Derived>::value,
                "HeterogenousContinuousContainerBase::pushBack: Derived must derive Base"
            );
            std::size_t space_ = getRemainingStorageCapacity();

            void* ptr = std::align(d->_alignof(), d->_sizeof(), current_, space_);

            if (!ptr)
            {
                return nullptr;
            }
            current_ = static_cast<void*>(static_cast<std::uint8_t*>(ptr) + d->_sizeof());
            return d->_placementCopyConstruct(ptr);
        }
        void clear()
        {
            current_ = begin_;
        }
    private:
        std::unique_ptr<std::uint8_t[]> storage_ {nullptr};
        void* begin_ {nullptr};
        void* current_ {nullptr};
        void* end_ {nullptr};
    };
}

namespace armarx
{
    template<class Base, bool UsePropagateConst = true>
    class HeterogenousContinuousContainer : public detail::HeterogenousContinuousContainerBase<Base>
    {
        using BaseContainer = detail::HeterogenousContinuousContainerBase<Base>;
    public:
        using ElementType = typename std::conditional <
                            UsePropagateConst,
                            PropagateConst<Base*>,
                            Base *
                            >::type;

        HeterogenousContinuousContainer() = default;
        HeterogenousContinuousContainer(HeterogenousContinuousContainer&&) = default;
        HeterogenousContinuousContainer(const HeterogenousContinuousContainer& other, bool compressElems = false)
        {
            this->setStorageCapacity(compressElems ? other.getUsedStorageCapacity() : other.getStorageCapacity());
            setElementCapacity(compressElems ? other.elements().size() : other.elements().capacity());
            for (auto& e : other.elements())
            {
                pushBack(e);
            }
        }

        HeterogenousContinuousContainer& operator=(HeterogenousContinuousContainer&&) = default;
        HeterogenousContinuousContainer& operator=(const HeterogenousContinuousContainer& other)
        {
            clear();
            setStorageCapacity(other.getStorageCapacity());
            setElementCapacity(other.elements().capacity());
            for (auto& e : other.elements())
            {
                pushBack(e);
            }
            return *this;
        }

        ~HeterogenousContinuousContainer()
        {
            clear();
        }

        std::size_t getElementCount() const
        {
            return elements_.size();
        }
        std::size_t getElementCapacity() const
        {
            return elements_.capacity();
        }
        std::size_t getRemainingElementCapacity() const
        {
            return getElementCapacity() - getElementCount();
        }

        void setElementCapacity(std::size_t cnt)
        {
            ARMARX_CHECK_EXPRESSION(this->empty());
            if (elements_.capacity() > cnt)
            {
                //force the capacity to reduce
                elements_ = std::vector<ElementType> {};
            }
            elements_.reserve(cnt);
        }

        template<class Derived>
        Base* pushBack(const Derived* d)
        {
            static_assert(
                std::is_base_of<Base, Derived>::value,
                "HeterogenousContinuousContainerBase::pushBack: Derived must derive Base"
            );
            ARMARX_CHECK_NOT_NULL(d);
            if (!getRemainingElementCapacity())
            {
                return nullptr;
            }
            Base* const ptr = BaseContainer::pushBack(d);
            if (ptr)
            {
                elements_.emplace_back(ptr);
            }
            return ptr;
        }
        template<class Derived>
        Base* pushBack(const PropagateConst<Derived*>& d)
        {
            return pushBack(*d);
        }
        template<class Derived>
        Base* pushBack(const Derived& d)
        {
            return pushBack(&d);
        }

        std::vector<ElementType>& elements()
        {
            return elements_;
        }
        const std::vector<ElementType>& elements() const
        {
            return elements_;
        }

        void clear()
        {
            for (auto& e : elements_)
            {
                e->~Base();
            }
            elements_.clear();
            BaseContainer::clear();
        }
    private:
        std::vector<ElementType> elements_;
    };

    template<class Base, bool UsePropagateConst = true>
    class HeterogenousContinuous2DContainer : public detail::HeterogenousContinuousContainerBase<Base>
    {
        using BaseContainer = detail::HeterogenousContinuousContainerBase<Base>;
    public:
        using ElementType = typename std::conditional <
                            UsePropagateConst,
                            PropagateConst<Base*>,
                            Base *
                            >::type;

        HeterogenousContinuous2DContainer() = default;
        HeterogenousContinuous2DContainer(HeterogenousContinuous2DContainer&&) = default;
        HeterogenousContinuous2DContainer(const HeterogenousContinuous2DContainer& other, bool compressElems = false)
        {
            setStorageCapacity(compressElems ? other.getUsedStorageCapacity() : other.getStorageCapacity());
            std::vector<std::size_t> elemCaps;
            elemCaps.reserve(other.elements().size());
            for (const auto& d1 : other.elements())
            {
                elemCaps.emplace_back(compressElems ? d1.size() : d1.capacity());
            }
            setElementCapacity(elemCaps);
            for (std::size_t i = 0; i < other.elements().size(); ++i)
            {
                for (auto& e : other.elements().at(i))
                {
                    pushBack(i, e);
                }
            }
        }

        HeterogenousContinuous2DContainer& operator=(HeterogenousContinuous2DContainer&&) = default;
        HeterogenousContinuous2DContainer& operator=(const HeterogenousContinuous2DContainer& other)
        {
            clear();
            setStorageCapacity(other.getStorageCapacity());
            std::vector<std::size_t> elemCaps;
            elemCaps.reserve(other.elements().size());
            for (const auto& d1 : other.elements())
            {
                elemCaps.emplace_back(d1.capacity());
            }
            setElementCapacity(elemCaps);
            for (std::size_t i = 0; i < other.elements().size(); ++i)
            {
                for (auto& e : other.elements().at(i))
                {
                    pushBack(i, e);
                }
            }
            return *this;
        }

        ~HeterogenousContinuous2DContainer()
        {
            clear();
        }

        std::size_t getElementCount(std::size_t d0) const
        {
            return elements_.at(d0).size();
        }
        std::size_t getElementCapacity(std::size_t d0) const
        {
            return elements_.at(d0).capacity();
        }
        std::size_t getRemainingElementCapacity(std::size_t d0) const
        {
            return getElementCapacity(d0) - getElementCount(d0);
        }

        void setElementCapacity(const std::vector<std::size_t>& cnt)
        {
            ARMARX_CHECK_EXPRESSION(this->empty());
            elements_.resize(cnt.size());
            for (std::size_t i = 0; i < cnt.size(); ++i)
            {
                if (elements_.at(i).capacity() > cnt)
                {
                    //force the capacity to reduce
                    elements_.at(i) = std::vector<ElementType> {};
                }
                elements_.at(i).reserve(cnt.at(i));
            }
        }

        template<class Derived>
        Base* pushBack(std::size_t d0, const Derived* d)
        {
            static_assert(
                std::is_base_of<Base, Derived>::value,
                "HeterogenousContinuousContainerBase::pushBack: Derived must derive Base"
            );
            ARMARX_CHECK_NOT_NULL(d);
            if (!getRemainingElementCapacity(d0))
            {
                return nullptr;
            }
            Base* const ptr = BaseContainer::pushBack(d);
            if (ptr)
            {
                elements_.at(d0).emplace_back(ptr);
            }
            return ptr;
        }
        template<class Derived>
        Base* pushBack(std::size_t d0, const PropagateConst<Derived*>& d)
        {
            return pushBack(d0, *d);
        }
        template<class Derived>
        Base* pushBack(std::size_t d0, const Derived& d)
        {
            return pushBack(d0, &d);
        }

        std::vector<std::vector<ElementType>>& elements()
        {
            return elements_;
        }
        const std::vector<std::vector<ElementType>>& elements() const
        {
            return elements_;
        }

        void clear()
        {
            for (auto& d1 : elements_)
            {
                for (auto& e : d1)
                {
                    e->~Base();
                }
                d1.clear();
            }
            BaseContainer::clear();
        }
    private:
        std::vector<std::vector<ElementType>> elements_;
    };
}
