/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

namespace Eigen
{
    template<typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
    class Matrix;


#define ax_eigen_fwd_make_matrix_and_vector(Type, TSuff, Size, SzSuff)      \
    typedef Matrix<Type, Size, Size, 0, Size, Size> Matrix##SzSuff##TSuff;  \
    typedef Matrix<Type, Size, 1   , 0, Size, 1   > Vector##SzSuff##TSuff;

#define ax_eigen_fwd_make_matrix_one_dynamic_dim(Type, TypeSuffix, Size)    \
    typedef Matrix<Type, Size, -1,0, Size, -1> Matrix##Size##X##TypeSuffix; \
    typedef Matrix<Type, -1, Size,0, -1, Size> Matrix##X##Size##TypeSuffix;

#define ax_eigen_fwd_make_matrix_and_vector_ALL_SIZES(Type, TypeSuffix) \
    ax_eigen_fwd_make_matrix_and_vector(Type, TypeSuffix, 2, 2)         \
    ax_eigen_fwd_make_matrix_and_vector(Type, TypeSuffix, 3, 3)         \
    ax_eigen_fwd_make_matrix_and_vector(Type, TypeSuffix, 4, 4)         \
    ax_eigen_fwd_make_matrix_and_vector(Type, TypeSuffix, 5, 5)         \
    ax_eigen_fwd_make_matrix_and_vector(Type, TypeSuffix, 6, 6)         \
    ax_eigen_fwd_make_matrix_and_vector(Type, TypeSuffix, -1, X)        \
    ax_eigen_fwd_make_matrix_one_dynamic_dim(Type, TypeSuffix, 2)       \
    ax_eigen_fwd_make_matrix_one_dynamic_dim(Type, TypeSuffix, 3)       \
    ax_eigen_fwd_make_matrix_one_dynamic_dim(Type, TypeSuffix, 4)       \
    ax_eigen_fwd_make_matrix_one_dynamic_dim(Type, TypeSuffix, 5)       \
    ax_eigen_fwd_make_matrix_one_dynamic_dim(Type, TypeSuffix, 6)

    ax_eigen_fwd_make_matrix_and_vector_ALL_SIZES(int,   i)
    ax_eigen_fwd_make_matrix_and_vector_ALL_SIZES(float, f)
    ax_eigen_fwd_make_matrix_and_vector_ALL_SIZES(double, d)

#undef ax_eigen_fwd_make_matrix_and_vector_ALL_SIZES
#undef ax_eigen_fwd_make_matrix_and_vector
#undef ax_eigen_fwd_make_matrix_one_dynamic_dim

    template<typename _Scalar, int _Options>
    class Quaternion;
    using Quaternionf = Quaternion<float, 0>;
    using Quaterniond = Quaternion<double, 0>;
}
