f << R"raw_str_delim(

def consume_file(fname, save=True, show=True):
#get data
    with open(fname, 'r') as f:
        data = [ x.split(' ') for x in f.read().split('\n') if x != '']
#plot
    from mpl_toolkits.axes_grid1 import host_subplot
    import mpl_toolkits.axisartist as AA
    import matplotlib.pyplot as plt

    pos = host_subplot(111, axes_class=AA.Axes)
    plt.subplots_adjust(right=0.75)
    vel = pos.twinx()
    acc = pos.twinx()


    pos.axhline(0, color='black')
    vel.axhline(0, color='black')
    acc.axhline(0, color='black')

    offset = 60
    acc.axis['right'] = acc.get_grid_helper().new_fixed_axis(loc='right',
    axes=acc,
    offset=(offset, 0))

    c={pos:'red', vel:'blue', acc:'green'}
    b={pos:0    , vel:0     , acc:0      }

    pos.set_xlabel('Time [s]')
    pos.set_ylabel('Position')
    vel.set_ylabel('Velocity')
    acc.set_ylabel('Acceleration')

    pos.axis['left'].label.set_color(c[pos])
    vel.axis['right'].label.set_color(c[vel])
    acc.axis['right'].label.set_color(c[acc])


    def get(name,factor=1):
        if name in data[0]:
            return [factor*float(x[data[0].index(name)]) for x in data[1:]]

    times=get('time')

    def add(plt,name,factor=1, clr=None, style='-'):
        d=get(name,factor)
        if d is None or [0]*len(d) == d:
            return
        if clr is None:
            clr=c[plt]
        plt.plot(times, d, style,color=clr)
        b[plt] = max([b[plt]]+[abs(x) for x in d])
        plt.set_ylim(-b[plt]*1.1, b[plt]*1.1)

    add(pos,'curpos',clr='red')
    add(pos,'targpos',clr='red', style='-.')
    add(pos,'posHi',clr='darkred', style='--')
    add(pos,'posLo',clr='darkred', style='--')
    add(pos,'posHiHard',clr='darkred', style='--')
    add(pos,'posLoHard',clr='darkred', style='--')
    add(pos,'brakingDist',clr='orange', style=':')
    add(pos,'posAfterBraking',clr='magenta', style=':')

    add(vel,'curvel',clr='blue')
    add(vel,'targvel',clr='blue', style='-.')
    add(vel,'maxv',clr='blue', style='--')
    add(vel,'maxv',factor=-1,clr='blue', style='--')

    add(acc,'curacc',clr='green')
    add(acc,'acc',clr='darkgreen', style='--')
    add(acc,'dec',clr='darkgreen', style='--')

    plt.draw()
    if save: plt.savefig(fname+'.svg')
    if show: plt.show()
    if not show: plt.close()


import sys
import os
def handle_path(p, show=True):
    if '.' == p:
        return
    if os.path.isfile(p):
        consume_file(p,show=show)
    if os.path.isdir(p):
        show=False
        for subdir, dirs, files in os.walk(p):
            for f in files:
                if not ".svg" in f:
                    handle_path(subdir+f,show=show)

if len(sys.argv) >= 2:
    handle_path(sys.argv[1])
else:
    handle_path('./')

  )raw_str_delim";
