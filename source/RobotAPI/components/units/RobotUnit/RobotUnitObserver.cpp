#include "RobotUnitObserver.h"

#include <ArmarXCore/observers/exceptions/user/InvalidDatafieldException.h>

namespace armarx
{

    RobotUnitObserver::RobotUnitObserver()
    {

    }

    void RobotUnitObserver::offerOrUpdateDataFieldsFlatCopy(const std::string& channelName, const StringVariantBaseMap& valueMap)
    {
        if (!existsChannel(channelName))
        {
            offerChannel(channelName, "");
        }
        try
        {
            // fastest way is to just try to set the datafields
            setDataFieldsFlatCopy(channelName, valueMap, true);
        }
        catch (exceptions::user::InvalidDataFieldException& e)
        {
            ARMARX_INFO << deactivateSpam() << "Creating datafields for channel " << channelName;
            // on failure: do it the slow way
            for (const auto& value : valueMap)
            {
                const std::string& datafieldName = value.first;

                if (!existsDataField(channelName, datafieldName))
                {
                    try
                    {
                        offerDataFieldWithDefault(channelName, datafieldName, *VariantPtr::dynamicCast(value.second), "");
                    }
                    catch (exceptions::user::DatafieldExistsAlreadyException& e)
                    {
                        setDataFieldFlatCopy(channelName, datafieldName, VariantPtr::dynamicCast(value.second));
                    }
                }
                else
                {
                    setDataFieldFlatCopy(channelName, datafieldName, VariantPtr::dynamicCast(value.second));
                }
            }
        }
    }

    void RobotUnitObserver::onInitObserver()
    {
    }

    void RobotUnitObserver::onConnectObserver()
    {
        offerChannel(sensorDevicesChannel, "Devices that provide sensor data");
        offerChannel(controlDevicesChannel, "Devices that are controlled by JointControllers");
        offerChannel(timingChannel, "Channel for timings of the different phases of the robot unit");
        offerChannel(additionalChannel, "Additional custom datafields");
    }

} // namespace armarx
