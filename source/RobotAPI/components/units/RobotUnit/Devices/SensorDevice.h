/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include"DeviceBase.h"
#include "../SensorValues/SensorValueBase.h"
#include "../util.h"
#include <IceUtil/Time.h>

namespace armarx::RobotUnitModule
{
    TYPEDEF_PTRS_HANDLE(Devices);
}

namespace armarx::SensorDeviceTags
{
    using namespace DeviceTags;
}

namespace armarx
{
    TYPEDEF_PTRS_SHARED(SensorDevice);
    /**
     * @brief This class represents some part of the robot providing sensor values.
     *
     * The \ref SensorValueBase "SensorValue" is accessed via \ref getSensorValue.
     *
     * A \ref SensorDevice could also represent a virtual sensor reporting some status of the software
     * (e.g. Timings):
     * Examples for \ref SensorValueBase "Sensorvalues" are:
     * \li Position, Velocity, Torque, Current, Temperature of an actuator
     * \li Forces and Torques of an Force-Torque-Sensor
     * \li Timings of the \ref ControlThread
     * \li Level of a battery power supply
     * \li 3d Velocity of a mobile Platform
     */
    class SensorDevice: public virtual DeviceBase
    {
    public:
        /// @brief A static const nullptr in case a const ref to a nullptr needs to be returned
        static const SensorDevicePtr NullPtr;
        /// @brief Create a SensorDevice with the given name
        SensorDevice(const std::string& name): DeviceBase {name} {}

        /// @return The SensorDevice's sensor value
        virtual const SensorValueBase* getSensorValue() const = 0;

        /// @return The SensorDevice's sensor value casted to the given type (may be nullptr)
        template<class T>
        const T* getSensorValue() const
        {
            return getSensorValue()->asA<const T*>();
        }

        /**
         * @brief Returns the \ref SensorValueBase "SensorValue's" type as a string.
         * @param withoutNamespaceSpecifier Whether namespace specifiers should be removed from the name.
         * @return The \ref SensorValueBase "SensorValues" type as a string.
         * @see getSensorValue
         */
        std::string getSensorValueType(bool withoutNamespaceSpecifier = false) const;

        /// @return The reporting frame name of this sensor (e.g. The frame some force torque values are in).
        /// This may be empty for virtual sensors e.g. execution time sensors.
        virtual std::string getReportingFrame() const;

        /**
         * @brief This is a hook for implementations to read the sensor value from a bus.
         * @param sensorValuesTimestamp The current timestamp
         * @param timeSinceLastIteration The time delta since the last call
         */
        virtual void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) {}

    private:
        friend class RobotUnitModule::Devices;
        /// @brief The owning \ref RobotUnit. (is set when this \ref SensorDevice is added to the \ref RobotUnit)
        std::atomic<RobotUnitModule::Devices*> owner {nullptr};
    };

    template<class SensorValueType>
    class SensorDeviceTemplate : public virtual SensorDevice
    {
        static_assert(std::is_base_of<SensorValueBase, SensorValueType>::value, "SensorValueType has to inherit SensorValueBase");
    public:
        SensorDeviceTemplate(const std::string& name): DeviceBase(name), SensorDevice(name) {}

        const SensorValueType* getSensorValue() const final override;

        SensorValueType sensorValue;
    };
}

//inline functions
namespace armarx
{
    inline std::string SensorDevice::getSensorValueType(bool withoutNamespaceSpecifier) const
    {
        return getSensorValue()->getSensorValueType(withoutNamespaceSpecifier);
    }

    inline std::string SensorDevice::getReportingFrame() const
    {
        return {};
    }

    template<class SensorValueType>
    inline const SensorValueType* SensorDeviceTemplate<SensorValueType>::getSensorValue() const
    {
        return &sensorValue;
    }
}

