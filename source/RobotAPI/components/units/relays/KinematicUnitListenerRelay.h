/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::KinematicUnitListenerRelay
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/util/noop.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>

namespace armarx
{
    class KinematicUnitListenerRelay :
        virtual public ManagedIceObject,
        virtual public KinematicUnitListener
    {
    public:
        using CallbackFunctionModes = std::function<void(const NameControlModeMap&, Ice::Long, bool)>;
        using CallbackFunctionState = std::function<void(const NameStatusMap&, Ice::Long, bool)>;
        using CallbackFunctionValue = std::function<void(const NameValueMap&, Ice::Long, bool)>;

        // KinematicUnitListener interface
        void reportControlModeChanged(const NameControlModeMap& map, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportControlModeChanged(map, time, changes);
        }
        void reportJointAngles(const NameValueMap& map, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportJointAngles(map, time, changes);
        }
        void reportJointVelocities(const NameValueMap& map, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportJointVelocities(map, time, changes);
        }
        void reportJointTorques(const NameValueMap& map, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportJointTorques(map, time, changes);
        }
        void reportJointAccelerations(const NameValueMap& map, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportJointAccelerations(map, time, changes);
        }
        void reportJointCurrents(const NameValueMap& map, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportJointCurrents(map, time, changes);
        }
        void reportJointMotorTemperatures(const NameValueMap& map, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportJointMotorTemperatures(map, time, changes);
        }
        void reportJointStatuses(const NameStatusMap& map, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportJointStatuses(map, time, changes);
        }

    protected:
        // ManagedIceObject interface
        void onInitComponent() override
        {
            usingTopic(topicName);
        }
        void onConnectComponent() override {}
        std::string getDefaultName() const override
        {
            return "KinematicUnitListenerRelay";
        }
    public:
        std::string topicName;
        CallbackFunctionModes callbackReportControlModeChanged     {noop<const NameControlModeMap&, Ice::Long, bool>};
        CallbackFunctionValue callbackReportJointAngles            {noop<const NameValueMap&, Ice::Long, bool>};
        CallbackFunctionValue callbackReportJointVelocities        {noop<const NameValueMap&, Ice::Long, bool>};
        CallbackFunctionValue callbackReportJointTorques           {noop<const NameValueMap&, Ice::Long, bool>};
        CallbackFunctionValue callbackReportJointAccelerations     {noop<const NameValueMap&, Ice::Long, bool>};
        CallbackFunctionValue callbackReportJointCurrents          {noop<const NameValueMap&, Ice::Long, bool>};
        CallbackFunctionValue callbackReportJointMotorTemperatures {noop<const NameValueMap&, Ice::Long, bool>};
        CallbackFunctionState callbackReportJointStatuses          {noop<const NameStatusMap&, Ice::Long, bool>};
    };
    using KinematicUnitListenerRelayPtr = IceInternal::Handle<KinematicUnitListenerRelay>;
}
