/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ForceTorqueUnitListenerRelay
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/util/noop.h>
#include <RobotAPI/interface/units/ForceTorqueUnit.h>

namespace armarx
{
    class ForceTorqueUnitListenerRelay :
        virtual public ManagedIceObject,
        virtual public ForceTorqueUnitListener
    {
    public:
        using CallbackFunctionSensorValues = std::function<void(const std::string&, const FramedDirectionBasePtr&, const FramedDirectionBasePtr&)>;

        // ForceTorqueUnitListener interface
        void reportSensorValues(const std::string& sensorNodeName, const FramedDirectionBasePtr& forces, const FramedDirectionBasePtr& torques, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportSensorValues(sensorNodeName, forces, torques);
        }
    protected:
        // ManagedIceObject interface
        void onInitComponent() override
        {
            usingTopic(topicName);
        }
        void onConnectComponent() override {}
        std::string getDefaultName() const override
        {
            return "ForceTorqueUnitListenerRelay";
        }
    public:
        std::string topicName;
        CallbackFunctionSensorValues callbackReportSensorValues {noop<const std::string&, const FramedDirectionBasePtr&, const FramedDirectionBasePtr&>};
    };
    using ForceTorqueUnitListenerRelayPtr = IceInternal::Handle<ForceTorqueUnitListenerRelay>;
}
