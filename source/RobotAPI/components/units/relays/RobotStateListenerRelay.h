/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::KinematicUnitListenerRelay
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/util/noop.h>
#include <RobotAPI/interface/core/RobotState.h>

namespace armarx
{
    class RobotStateListenerRelay :
        virtual public ManagedIceObject,
        virtual public RobotStateListenerInterface
    {
    public:
        using CallbackFunctionValue = std::function<void(const NameValueMap&, Ice::Long, bool)>;
        using CallbackFunctionPose  = std::function<void(const FramedPoseBasePtr&, Ice::Long, bool)>;

        virtual void reportJointValues(const NameValueMap& map, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent)
        {
            callbackReportJointValues(map, time, changes);
        }
        void reportGlobalRobotRootPose(const FramedPoseBasePtr& pose, Ice::Long time, bool changes, const Ice::Current& = Ice::emptyCurrent)
        {
            callbackReportGlobalRobotRootPose(pose, time, changes);
        }
    protected:
        // ManagedIceObject interface
        virtual void onInitComponent()
        {
            usingTopic(topicName);
        }
        virtual void onConnectComponent() {}
        virtual std::string getDefaultName() const
        {
            return "RobotStateListener";
        }
    public:
        std::string topicName;
        CallbackFunctionValue callbackReportJointValues         {noop<const NameValueMap&, Ice::Long, bool>};
        CallbackFunctionPose  callbackReportGlobalRobotRootPose {noop<const FramedPoseBasePtr&, Ice::Long, bool>};
    };
    using RobotStateListenerRelayPtr = IceInternal::Handle<RobotStateListenerRelay>;
}
