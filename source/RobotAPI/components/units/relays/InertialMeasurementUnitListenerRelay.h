/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::InertialMeasurementUnitListenerRelay
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/util/noop.h>
#include <RobotAPI/interface/units/InertialMeasurementUnit.h>

namespace armarx
{
    class InertialMeasurementUnitListenerRelay :
        virtual public ManagedIceObject,
        virtual public InertialMeasurementUnitListener
    {
    public:
        using CallbackFunctionSensorValues = std::function<void(const std::string&, const std::string&, const IMUData&, const TimestampBasePtr&)>;

        // InertialMeasurementUnitListener interface
        void reportSensorValues(const std::string& device, const std::string& name, const IMUData& values, const TimestampBasePtr& timestamp, const Ice::Current& = Ice::emptyCurrent) override
        {
            callbackReportSensorValues(device, name, values, timestamp);
        }
    protected:
        // ManagedIceObject interface
        void onInitComponent() override
        {
            usingTopic(topicName);
        }
        void onConnectComponent() override {}
        std::string getDefaultName() const override
        {
            return "InertialMeasurementUnitListenerRelay";
        }
    public:
        std::string topicName;
        CallbackFunctionSensorValues callbackReportSensorValues {noop<const std::string&, const std::string&, const IMUData&, const TimestampBasePtr&>};
    };
    using InertialMeasurementUnitListenerRelayPtr = IceInternal::Handle<InertialMeasurementUnitListenerRelay>;
}
