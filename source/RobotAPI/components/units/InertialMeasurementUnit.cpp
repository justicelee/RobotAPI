/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::InertialMeasurementUnit
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "InertialMeasurementUnit.h"


namespace armarx
{
    void InertialMeasurementUnit::onInitComponent()
    {
        offeringTopic(getProperty<std::string>("IMUTopicName").getValue());
        onInitIMU();
    }


    void InertialMeasurementUnit::onConnectComponent()
    {
        IMUTopicPrx = getTopic<InertialMeasurementUnitListenerPrx>(getProperty<std::string>("IMUTopicName").getValue());
        onStartIMU();
    }


    void InertialMeasurementUnit::onDisconnectComponent()
    {
    }


    void InertialMeasurementUnit::onExitComponent()
    {
        onExitIMU();
    }

    PropertyDefinitionsPtr InertialMeasurementUnit::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new InertialMeasurementUnitPropertyDefinitions(
                                          getConfigIdentifier()));
    }
}
