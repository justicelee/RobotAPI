/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <ArmarXCore/observers/Observer.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <mutex>

namespace armarx
{
    /**
     * \class LaserScannerUnitObserverPropertyDefinitions
     * \brief
     */
    class LaserScannerUnitObserverPropertyDefinitions:
        public ObserverPropertyDefinitions
    {
    public:
        LaserScannerUnitObserverPropertyDefinitions(std::string prefix):
            ObserverPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("LaserScannerTopicName", "LaserScans", "Name of the laser scan topic.");
        }
    };


    /**
     * \class LaserScannerUnitObserver
     * \ingroup RobotAPI-SensorActorUnits-observers
     * \brief Observer monitoring laser scanner values
     *
     * The LaserScannerUnitObserver monitors laser scanner values published by LaserScannerUnit-implementations.
     */
    class LaserScannerUnitObserver :
        virtual public Observer,
        virtual public LaserScannerUnitObserverInterface
    {
    public:
        LaserScannerUnitObserver() {}

        std::string getDefaultName() const override
        {
            return "LaserScannerUnitObserver";
        }
        void onInitObserver() override;
        void onConnectObserver() override;
        void onExitObserver() override;
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

        void reportSensorValues(const std::string& device, const std::string& name, const LaserScan& scan,
                                const TimestampBasePtr& timestamp, const Ice::Current& c) override;

    private:
        std::mutex dataMutex;

    };
}

