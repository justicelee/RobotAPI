/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Peter Kaiser <peter dot kaiser at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include "RobotAPI/components/units/SensorActorUnit.h"

#include <RobotAPI/interface/units/HapticUnit.h>

#include <string>

namespace armarx
{
    /**
     * \class HapticUnitPropertyDefinitions
     * \brief
     */
    class HapticUnitPropertyDefinitions : public ComponentPropertyDefinitions
    {
    public:
        HapticUnitPropertyDefinitions(std::string prefix) :
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("HapticTopicName", "HapticValues", "Name of the Haptic Topic.");
        }
    };

    /**
     * \defgroup Component-HapticUnit HapticUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Base unit for haptic sensors.
     */

    /**
     * @ingroup Component-HapticUnit
     * @brief The HapticUnit class
     */
    class HapticUnit :
        virtual public HapticUnitInterface,
        virtual public SensorActorUnit
    {
    public:
        std::string getDefaultName() const override
        {
            return "HapticUnit";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        virtual void onInitHapticUnit() = 0;
        virtual void onStartHapticUnit() = 0;
        virtual void onExitHapticUnit() = 0;

        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        HapticUnitListenerPrx hapticTopicPrx;
    };
}

