/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include "HandUnit.h"

namespace armarx
{
    /**
     * \class HandUnitSimulationPropertyDefinitions
     * \brief Defines all necessary properties for armarx::HandUnitSimulation
     */
    class HandUnitSimulationPropertyDefinitions:
        public HandUnitPropertyDefinitions
    {
    public:
        HandUnitSimulationPropertyDefinitions(std::string prefix):
            HandUnitPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the kinematic unit that should be used");
        }
    };


    /**
     * \class HandUnitSimulation
     * \ingroup RobotAPI-SensorActorUnits-simulation
     * \brief Simulates a robot hand.
     *
     * An instance of a HandUnitSimulation provides means to open, close, and shape hands.
     * It uses the HandUnitListener Ice interface to report updates of its current state
     */
    class HandUnitSimulation :
        virtual public HandUnit
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "HandUnitSimulation";
        }

        void onInitHandUnit() override;
        void onStartHandUnit() override;
        void onExitHandUnit() override;

        /**
         * Send command to the hand to form a specific shape position.
         *
         * \warning Not implemented yet!
         */
        void setShape(const std::string& shapeName, const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * \warning Not implemented yet!
         */
        void setJointAngles(const NameValueMap& jointAngles, const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * \see armarx::PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        KinematicUnitInterfacePrx kinematicUnitPrx;
    };
}

