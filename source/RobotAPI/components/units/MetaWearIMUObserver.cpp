/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MetaWearIMUObserver.h"


#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/observers/variant/TimestampVariant.h>



namespace armarx
{
    void MetaWearIMUObserver::onInitObserver()
    {
        usingTopic(getProperty<std::string>("MetaWearTopicName").getValue());

        offerConditionCheck("updated", new ConditionCheckUpdated());
        offerConditionCheck("larger", new ConditionCheckLarger());
        offerConditionCheck("equals", new ConditionCheckEquals());
        offerConditionCheck("smaller", new ConditionCheckSmaller());

        offeringTopic(getProperty<std::string>("DebugDrawerTopic").getValue());
    }



    void MetaWearIMUObserver::onConnectObserver()
    {
        debugDrawerPrx = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopic").getValue());
    }


    void MetaWearIMUObserver::onExitObserver()
    {

    }

    void armarx::MetaWearIMUObserver::reportIMUValues(const std::string& name, const MetaWearIMUData& data, const TimestampBasePtr& timestamp, const Ice::Current&)
    {
        std::unique_lock lock(dataMutex);

        if (!existsChannel(name))
        {
            offerChannel(name, "MetaWear IMU data");
        }
        offerVector3(name, "acceleration", data.acceleration);
        offerVector3(name, "gyro", data.gyro);
        offerVector3(name, "magnetic", data.magnetic);
        offerQuaternion(name, "orientationQuaternion", data.orientationQuaternion);

        updateChannel(name);

    }

    PropertyDefinitionsPtr MetaWearIMUObserver::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new MetaWearIMUObserverPropertyDefinitions(getConfigIdentifier()));
    }

    void MetaWearIMUObserver::offerVector3(const std::string& channelName, const std::string& dfName, const std::vector<float>& data)
    {
        if (data.size() == 3)
        {
            Vector3Ptr vec3 = new Vector3(data.at(0), data.at(1), data.at(2));
            offerOrUpdateDataField(channelName, dfName, vec3, dfName);
        }
        else if (data.size() != 0)
        {
            ARMARX_WARNING << "data." << dfName << ".size() != 3 && data." << dfName << ".size() != 0";
        }
    }

    void MetaWearIMUObserver::offerQuaternion(const std::string& channelName, const std::string& dfName, const std::vector<float>& data)
    {
        if (data.size() == 4)
        {
            QuaternionPtr quat = new Quaternion(data.at(0), data.at(1), data.at(2), data.at(3));
            offerOrUpdateDataField(channelName, dfName, quat, dfName);
        }
        else if (data.size() != 0)
        {
            ARMARX_WARNING << "data." << dfName << ".size() != 4 && data." << dfName << ".size() != 0";
        }
    }
}
