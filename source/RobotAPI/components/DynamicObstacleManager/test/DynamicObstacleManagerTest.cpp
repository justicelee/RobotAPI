/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6Skills::ArmarXObjects::DynamicObstacleManager
 * @author     Fabian PK ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE Armar6Skills::ArmarXObjects::DynamicObstacleManager

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include <Armar6Skills/components/DynamicObstacleManager/DynamicObstacleManager.h>

#include <iostream>

BOOST_AUTO_TEST_CASE(testExample)
{
    armarx::DynamicObstacleManager instance;

    BOOST_CHECK_EQUAL(true, true);
}
