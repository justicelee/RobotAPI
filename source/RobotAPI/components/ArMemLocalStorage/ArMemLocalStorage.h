/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemLocalStorage
 * @author     fabian.peller-konrad@kit.edu ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <map>

// Aron
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/interface/armem/legacy.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <RobotAPI/libraries/armem/legacy/ArMemBase.h>

namespace armarx
{

    namespace armem
    {
        class ArMemLocalStorage :
            virtual public armarx::Component,
            virtual public armarx::armem::ArMemBase,
            virtual public armarx::armem::ArMemLocalMemoryInterface
        {
        public:
            enum HasSegmentReturnType
            {
                Yes,
                No,
                InvalidState
            };

            typedef std::vector<ArMemCommitPtr> IndexedDatatypeStorage;
            typedef std::map<long, ArMemCommitPtr> TimestampedDataTypeStorage;
            typedef std::map<std::size_t, ArMemCommitPtr> HashedDataTypeStorage;

            ArMemLocalStorage();

            std::string getDefaultName() const override;

            HasSegmentReturnType hasSegment(const std::string&) const;
            long size(const std::string&) const;

            ArMemCommitPtr getLatestCommitFromSegment(const std::string&, const Ice::Current& = Ice::Current());
            ArMemCommitPtr getNextCommitFromSegmentForTimestamp(const std::string&, long, const Ice::Current& = Ice::Current());
            TimestampedArMemCommitList getAllCommitsBetweenTimestampsFromSegment(const std::string&, Ice::Long, Ice::Long, const Ice::Current& = Ice::Current());
            TimestampedArMemCommitList getAllCommitsFromSegment(const std::string&, const Ice::Current& = Ice::Current());

            std::string commit(const std::string& segment, long generatedTS, const std::vector<aron::data::AronDataPtr>&, const Ice::Current& = Ice::Current());
            std::string commit_single(const std::string& segment, long generatedTS, const aron::data::AronDataPtr&, const Ice::Current& = Ice::Current());

        protected:
            void onInitComponent() override;
            void onConnectComponent() override;
            void onDisconnectComponent() override;
            void onExitComponent() override;

            armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        private:

            void checkStorageIntegrity() const;
            void checkStorageIntegrityForSegment(const std::string&) const;

        private:
            std::map<std::string, IndexedDatatypeStorage> indexed_storage;
            std::map<std::string, TimestampedDataTypeStorage> timestamped_storage;
            std::map<std::string, HashedDataTypeStorage> hashed_storage;

            long num_segments;
            std::map<std::string, long> num_entries_per_segment;

            long maximum_segments;
            long maximum_entries_per_segment;
        };
    }
}
