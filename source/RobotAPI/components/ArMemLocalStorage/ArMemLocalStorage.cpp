/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemLocalStorage
 * @author     fabian.peller-konrad@kit.edu ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "ArMemLocalStorage.h"

// Boost
#include <boost/asio.hpp>

namespace armarx
{

    namespace armem
    {
        armarx::PropertyDefinitionsPtr ArMemLocalStorage::createPropertyDefinitions()
        {
            PropertyDefinitionsPtr defs(new ComponentPropertyDefinitions(getConfigIdentifier()));
            defs->optional(maximum_segments, "MaximumNumberOfSegments", "Maximum number of segments (<0 means infinite)");
            defs->optional(maximum_entries_per_segment, "MaximumEntriesPerSegment", "Maximum number of accepted datatypes per segment  (<0 means infinite)");
            return defs;
        }

        std::string ArMemLocalStorage::getDefaultName() const
        {
            return GenerateLocalMemoryObjectNameFromHostname(getMyHostname());
        }

        ArMemLocalStorage::ArMemLocalStorage() :
            armarx::Component(),
            armarx::armem::ArMemBase(),
            num_segments(0),
            num_entries_per_segment(),
            maximum_segments(-1),
            maximum_entries_per_segment(-1)
        {

        }

        void ArMemLocalStorage::onInitComponent()
        {

        }

        void ArMemLocalStorage::onConnectComponent()
        {

        }

        void ArMemLocalStorage::onDisconnectComponent()
        {

        }

        void ArMemLocalStorage::onExitComponent()
        {

        }

        ArMemLocalStorage::HasSegmentReturnType ArMemLocalStorage::hasSegment(const std::string& segment) const
        {
            auto index_pos = indexed_storage.find(segment);
            auto timestamped_pos = timestamped_storage.find(segment);
            auto hashed_pos = hashed_storage.find(segment);

            if (index_pos == indexed_storage.end() && timestamped_pos == timestamped_storage.end() && hashed_pos == hashed_storage.end())
            {
                return HasSegmentReturnType::No;
            }

            if (index_pos != indexed_storage.end() && timestamped_pos != timestamped_storage.end() && hashed_pos != hashed_storage.end())
            {
                return HasSegmentReturnType::Yes;
            }

            bool index_pos_end = (index_pos == indexed_storage.end());
            bool timestamped_pos_end = (timestamped_pos == timestamped_storage.end());
            bool hashed_pos_end = (hashed_pos == hashed_storage.end());
            ARMARX_ERROR << "The memory is in an inconsistent state. A segment was found in some containers but not in all! Resetting segment!\n"
                         << "The indexed storage " << (index_pos_end ? "does not contain " : "contains ") << " the segment, \n"
                         << "The timestamped storage " << (timestamped_pos_end ? "does not contain " : "contains ") << " the segment, \n"
                         << "The hashed storage " << (hashed_pos_end ? "does not contain " : "contains ") << " the segment, \n";
            return HasSegmentReturnType::InvalidState;
        }


        std::string ArMemLocalStorage::commit(const std::string& segment, long sentAt, const std::vector<aron::data::AronDataPtr>& data, const Ice::Current&)
        {
            // Check if segment already exists
            HasSegmentReturnType has_segment = hasSegment(segment);
            if (has_segment != HasSegmentReturnType::Yes)
            {
                // Need to allocate a new storage for a new segment
                ARMARX_WARNING << "A new segment '" + segment + "' registered to the memory. Allocating space for its members...";
                indexed_storage[segment] = IndexedDatatypeStorage();
                timestamped_storage[segment] = TimestampedDataTypeStorage();
                hashed_storage[segment] = HashedDataTypeStorage();

                if (has_segment != HasSegmentReturnType::InvalidState)
                {
                    // Otherwise we reassign the segment again.
                    num_segments++;
                }
            }

            IndexedDatatypeStorage& indexed_segment_storage = indexed_storage[segment];
            TimestampedDataTypeStorage& timestamped_segment_storage = timestamped_storage[segment];
            HashedDataTypeStorage& hashed_segment_storage = hashed_storage[segment];

            long now = IceUtil::Time::now().toMilliSeconds();
            if (sentAt > now)
            {
                ARMARX_WARNING << "Received an invalid timestamp. The timestamp when sending data to the segment '" + segment + "' is greater than the storage timestamp (The transmitted time needs to be in milliseconds [ms]). Please check!" ;
            }

            // Create new memory entry and save result
            ArMemCommitPtr entryPtr = ArMemCommitPtr(new ArMemCommit());
            entryPtr->producer = "TODO";
            entryPtr->stored_in_segment = segment;
            entryPtr->produce_timestamp_ms = sentAt;
            entryPtr->stored_in_segment = IceUtil::Time::now().toMilliSeconds();
            entryPtr->data = data;

            ARMARX_INFO << "Saving a new commit to segment '" + segment + "'";
            std::string string_to_hash = segment + "__" + std::to_string(now);
            size_t hash = std::hash<std::string> {}(string_to_hash);

            if (timestamped_segment_storage.find(now) != timestamped_segment_storage.end())
            {
                ARMARX_WARNING << "The segment '" + segment + "' already contains a value at timestamp " + std::to_string(now) + ". Overwriting value!";
            }
            if (hashed_segment_storage.find(hash) != hashed_segment_storage.end())
            {
                ARMARX_WARNING << "The segment '" + segment + "' already contains data at the commit hash " + std::to_string(hash) + ". Overwriting value!";
            }

            std::string hash_as_string = std::to_string(hash);
            ARMARX_DEBUG << "Storing new data with hash: " << hash_as_string;
            indexed_segment_storage.push_back(entryPtr);
            timestamped_segment_storage[now] = entryPtr;
            hashed_segment_storage[hash] = entryPtr;
            ARMARX_DEBUG << "Stored new data with hash: " << hash_as_string;
            ARMARX_DEBUG << "The current segment " << segment << " now contains " << indexed_segment_storage.size() << " elements!";
            return std::to_string(hash);
        }

        std::string ArMemLocalStorage::commit_single(const std::string& segment, long generatedTS, const aron::data::AronDataPtr& x, const Ice::Current& c)
        {
            return commit(segment, generatedTS, {x}, c);
        }

        void ArMemLocalStorage::checkStorageIntegrity() const
        {
            if (indexed_storage.size() != timestamped_storage.size() ||
                indexed_storage.size() != hashed_storage.size() ||
                timestamped_storage.size() != hashed_storage.size())
            {
                throw LocalException("The memory is in an invalid state. The storages have different sizes! Please Check!");
            }
            for (const auto& [segment, datatypeStorage] : indexed_storage)
            {
                checkStorageIntegrityForSegment(segment);
            }
        }

        void ArMemLocalStorage::checkStorageIntegrityForSegment(const std::string& segment) const
        {
            HasSegmentReturnType has_segment = hasSegment(segment);
            if (has_segment != HasSegmentReturnType::Yes)
            {
                throw LocalException("The memory is in an invalid state. Not all storages contain the segment '" + segment + "'! Please Check!");
            }
        }


        ArMemCommitPtr ArMemLocalStorage::getLatestCommitFromSegment(const std::string& segment, const Ice::Current&)
        {
            HasSegmentReturnType has_segment = hasSegment(segment);
            if (has_segment != HasSegmentReturnType::Yes)
            {
                return nullptr;
            }

            int index = indexed_storage[segment].size() - 1;
            if (index >= 0)
            {
                return indexed_storage[segment][index];
            }
            return nullptr;
        }

        ArMemCommitPtr ArMemLocalStorage::getNextCommitFromSegmentForTimestamp(const std::string&, Ice::Long, const Ice::Current&)
        {
            return nullptr;
        }

        TimestampedArMemCommitList ArMemLocalStorage::getAllCommitsBetweenTimestampsFromSegment(const std::string&, Ice::Long, Ice::Long, const Ice::Current&)
        {
            return {};
        }

        TimestampedArMemCommitList ArMemLocalStorage::getAllCommitsFromSegment(const std::string&, const Ice::Current&)
        {
            return {};
        }
    }
}
