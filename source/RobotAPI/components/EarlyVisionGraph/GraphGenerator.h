// *****************************************************************
// Filename:    GraphGenerator.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        08.07.2008
// *****************************************************************

#pragma once

// *****************************************************************
// includes
// *****************************************************************
#include "Base/DataStructures/Graph/SphericalGraph.h"
#include "Base/Math/MathTools.h"
#include "Math/Math3d.h"
#include <vector>
#include <float.h>

// *****************************************************************
// enums
// *****************************************************************
enum EPolyhedronType
{
    eTetrahedron,
    eOctahedron,
    eIcosahedron
};

// *****************************************************************
// implementation of class Face
// *****************************************************************
class Face
{
public:
    Face() {};
    Face(int n1, int n2, int n3)
    {
        set(n1, n2, n3);
    };
    ~Face() {};

    void set(int n1, int n2, int n3)
    {
        m_n1 = n1;
        m_n2 = n2 ;
        m_n3 = n3;
    };

    int m_n1;
    int m_n2;
    int m_n3;
};

// *****************************************************************
// implementation of CGraphGenerator
// *****************************************************************
class CGraphGenerator
{
public:
    static void generateGraph(CSphericalGraph* pPlainGraph, EPolyhedronType nBaseType, int nLevels, float fRadius);

private:
    static void initBasePolyhedron(EPolyhedronType nBaseType);
    static void generateTetrahedron();
    static void generateOctahedron();
    static void generateIcosahedron();
    static void subDivide();
    static void projectToSphere(float fRadius);
    static void buildNodesAndEdges(CSphericalGraph* pGraph);
    static int findVertex(Vec3d position, float fEpsilon);

private:
    CGraphGenerator() {};
    ~CGraphGenerator() {};

    static vector<Vec3d>    m_Vertices;
    static vector<Face> m_Faces;
};

