// *****************************************************************
// Filename:    IntensityGraph.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        12.06.2008
// *****************************************************************

#pragma once

// *****************************************************************
// includes
// *****************************************************************
#include "SphericalGraph.h"
#include <string>


// *****************************************************************
// structures
// *****************************************************************
// *****************************************************************
// definition of nodes
// *****************************************************************
class CIntensityNode : public CSGNode
{
public:
    CIntensityNode();
    CIntensityNode(TSphereCoord coord);
    ~CIntensityNode() override;

    void setIntensity(float fIntensity);
    float getIntensity();

    CSGNode* clone() override
    {
        CIntensityNode* copy = new CIntensityNode(m_Position);
        copy->setIndex(m_nIndex);
        copy->setIntensity(m_fIntensity);
        return copy;
    }

private:
    float   m_fIntensity;
};


// *****************************************************************
// definition of sensory egosphere
// *****************************************************************
class CIntensityGraph : public CSphericalGraph
{
public:
    CIntensityGraph();
    CIntensityGraph(const CIntensityGraph& prototype);
    //CIntensityGraph(int nNumberNodes);
    CIntensityGraph(std::string sFileName);
    ~CIntensityGraph() override;

    void reset();
    void set(float fIntensity);


    void graphToVec(std::vector<float>& vec);

    ///////////////////////////////
    // file io
    ///////////////////////////////
    bool read(std::istream& infile) override;
    bool readNode(std::istream& infile) override;
    bool readEdge(std::istream& infile) override;

    bool write(std::ostream& outfile) override;
    bool writeNode(std::ostream& outfile, int n) override;
    bool writeEdge(std::ostream& outfile, int e) override;


    // inherited
    CIntensityNode* getNewNode() override
    {
        return new CIntensityNode();
    }

    ///////////////////////////////
    // operators
    ///////////////////////////////
    CIntensityGraph& operator= (CIntensityGraph const& rhs);

};

