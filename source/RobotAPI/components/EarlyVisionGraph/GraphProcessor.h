// *****************************************************************
// Filename:    GraphProcessor.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        12.06.2007
// *****************************************************************

#pragma once

// *****************************************************************
// forward declarations
// *****************************************************************

// *****************************************************************
// includes
// *****************************************************************
#include "SphericalGraph.h"

// *****************************************************************
// namespace GraphProcessor
// *****************************************************************
namespace GraphProcessor
{
    // graph operations
    void copyGraphNodesAndEdges(CSphericalGraph* pSrcGraph, CSphericalGraph* pDstGraph);

    // graph coordinate transformations
    void transform(CSphericalGraph* pGraph, TTransform transform);
    void inverseTransform(CSphericalGraph* pGraph, TTransform transform);
    void invertPhi(CSphericalGraph* pGraph);
    void invertTheta(CSphericalGraph* pGraph);

    // edge operations
    int findEdge(CSphericalGraph* pGraph, int nIndex1, int nIndex2);
    bool isEdgePresent(CSphericalGraph* pGraph, int nIndex1, int nIndex2);

    // neighbour and node operations
    int findClosestNode(CSphericalGraph* pGraph, TSphereCoord coord);
    void findClosestNeighbours(CSphericalGraph* pGraph, int& s, int& t);
    int getDelaunayNeighbour(CSphericalGraph* pGraph, CSGEdge* pEdge);

    // circumcircle operations
    bool insideCircumcircle(CSphericalGraph* pGraph, int nIndex1, int nIndex2, int nIndex3, int nPointIndex, bool& bInside);
    bool getCircumcircle(CSphericalGraph* pGraph, int nIndex1, int nIndex2, int nIndex3, TSphereCoord& center, float& fRadius);
};

