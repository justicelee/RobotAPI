// *****************************************************************
// Filename:    GraphMap.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        12.06.2007
// *****************************************************************

#pragma once

// *****************************************************************
// includes
// *****************************************************************
#include "Base/DataStructures/Graph/IntensityGraph.h"
#include "Base/DataStructures/Graph/GraphPyramidLookupTable.h"
#include "Math/FloatMatrix.h"

// *****************************************************************
// enums
// *****************************************************************
enum EOperation
{
    eSet,
    eAdd,
    eMultiply,
    eMax
};

// *****************************************************************
// decleration of CGraphMap
// *****************************************************************
class CGraphMap
{
public:
    // construction / destruction
    CGraphMap(CIntensityGraph* pGraph, bool bUseLookup = false);
    ~CGraphMap();

    // getter / setter
    void setMap(CFloatMatrix* pMap, bool bUpdateGraph = true);
    CFloatMatrix* getMap();
    int getWidth();
    int getHeight();

    // manipulation
    void applyGaussian(EOperation nOperation, Vec3d position, float fAmplitude, float fVariance);
    void applyGaussian(EOperation nOperation, TSphereCoord coord, float fAmplitude, float fVariance);
    void applyGaussian(EOperation nOperation, int nNodeID, float fAmplitude, float fVariance);

    // update
    void updateGraph();
    void updateMap();

private:
    float evaluateGaussian(float fDist, float fAmplitude, float fVariance);
    void toGraph(); // update intensitygraph
    void toMap();   // update intensitymap

    CSphericalGraph*    m_pGraph;
    int         m_nNumberNodes;
    CFloatMatrix*       m_pMap;
    bool            m_bOwnMemory;

    int             m_nWidth;
    int             m_nHeight;

    bool            m_bUseLookup;
    CGraphPyramidLookupTable*   m_pLookupTable;
};


