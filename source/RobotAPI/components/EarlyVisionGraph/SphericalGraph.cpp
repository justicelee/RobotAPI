// *****************************************************************
// Filename:    AspectGraph.cpp
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        12.06.2007
// *****************************************************************


// *****************************************************************
// includes
// *****************************************************************
#include "SphericalGraph.h"
//#include "Base/Tools/DebugMemory.h"

// *****************************************************************
// construction / destruction
// *****************************************************************
CSphericalGraph::CSphericalGraph()
{
    clear();
}

CSphericalGraph::CSphericalGraph(const CSphericalGraph& prototype)
{
    *this = prototype;
}

CSphericalGraph::~CSphericalGraph()
{
    clear();
}

CSphericalGraph& CSphericalGraph::operator= (CSphericalGraph const& rhs)
{
    clear();

    // copy all nodes
    const TNodeList* nodes = &rhs.m_Nodes;

    int nNumberNodes = int(nodes->size());

    for (int n = 0 ; n < nNumberNodes ; n++)
    {
        addNode(nodes->at(n)->clone());
    }

    // copy all edges
    const TEdgeList* edges = &rhs.m_Edges;

    int nNumberEdges = int(edges->size());

    for (int e = 0 ; e < nNumberEdges ; e++)
    {
        CSGEdge* edge = edges->at(e);
        addEdge(edge->nIndex1, edge->nIndex2, edge->nLeftFace, edge->nRightFace);
    }

    return *this;
}


// *****************************************************************
// control
// *****************************************************************
void CSphericalGraph::clear()
{
    // clear edges
    std::vector<CSGEdge*>::iterator iter = m_Edges.begin();

    while (iter != m_Edges.end())
    {
        delete *iter;
        iter++;
    }

    m_Edges.clear();

    // clear nodes
    std::vector<CSGNode*>::iterator iter_node = m_Nodes.begin();

    while (iter_node != m_Nodes.end())
    {
        delete *iter_node;
        iter_node++;
    }

    m_Nodes.clear();
}

// *****************************************************************
// graph building
// *****************************************************************
// nodes
int CSphericalGraph::addNode(CSGNode* pNode)
{
    m_Nodes.push_back(pNode);
    pNode->setIndex(int(m_Nodes.size() - 1));

    return pNode->getIndex();
}

// edges
int CSphericalGraph::addEdge(int nIndex1, int nIndex2)
{
    return addEdge(nIndex1, nIndex2, -1, -1);
}

int CSphericalGraph::addEdge(int nIndex1, int nIndex2, int nLeftFace, int nRightFace)
{
    CSGEdge* pEdge = new CSGEdge;
    pEdge->nIndex1 = nIndex1;
    pEdge->nIndex2 = nIndex2;
    pEdge->nLeftFace = nLeftFace;
    pEdge->nRightFace = nRightFace;

    //  printf("Adding edge %d --> %d\n",nIndex1,nIndex2);

    // add to list of edges
    m_Edges.push_back(pEdge);

    // update node adjacency list
    addNodeAdjacency(nIndex1, nIndex2);
    addNodeAdjacency(nIndex2, nIndex1);

    return int(m_Edges.size()) - 1;
}

void CSphericalGraph::addNodeAdjacency(int nNode, int nAdjacency)
{
    if (nNode == nAdjacency)
    {
        printf("Error: adding node as adjacenca\n");
    }

    // check if present
    bool bPresent = false;

    for (int n : m_NodeAdjacency[nNode])
    {
        if (n == nAdjacency)
        {
            bPresent = true;
        }
    }

    if (!bPresent)
    {
        m_NodeAdjacency[nNode].push_back(nAdjacency);
    }
}

// *****************************************************************
// member access
// *****************************************************************
TEdgeList* CSphericalGraph::getEdges()
{
    return &m_Edges;
}

TNodeList* CSphericalGraph::getNodes()
{
    return &m_Nodes;
}

CSGEdge* CSphericalGraph::getEdge(int nEdgeIndex)
{
    return m_Edges.at(nEdgeIndex);
}

CSGNode* CSphericalGraph::getNode(int nIndex)
{
    return m_Nodes.at(nIndex);
}

std::vector<int>* CSphericalGraph::getNodeAdjacency(int nIndex)
{
    return &m_NodeAdjacency[nIndex];
}

// *****************************************************************
// file io
// *****************************************************************
bool CSphericalGraph::read(std::istream& infile)
{
    try
    {
        int nNumberNodes;

        // read number of nodes
        infile >> nNumberNodes;

        // read all nodes
        for (int n = 0 ; n < nNumberNodes ; n++)
        {
            readNode(infile);
        }

        // read number of edges
        int nNumberEdges;
        infile >> nNumberEdges;

        for (int e = 0 ; e < nNumberEdges ; e++)
        {
            readEdge(infile);
        }

    }
    catch (std::istream::failure&)
    {
        printf("ERROR: failed to write FeatureGraph to file\n");
        return false;
    }

    return true;
}

bool CSphericalGraph::readNode(std::istream& infile)
{
    CSGNode* pNewNode = (CSGNode*) getNewNode();

    int nIndex;
    TSphereCoord pos;

    infile >> nIndex;
    infile >> pos.fPhi;
    infile >> pos.fTheta;

    pNewNode->setPosition(pos);

    if (addNode(pNewNode) != nIndex)
    {
        printf("Error input file inconsistent\n");
        return false;
    }

    return true;
}



bool CSphericalGraph::readEdge(std::istream& infile)
{
    int nIndex1, nIndex2, nLeftFace, nRightFace;

    infile >> nIndex1;
    infile >> nIndex2;

    infile >> nLeftFace;
    infile >> nRightFace;

    addEdge(nIndex1, nIndex2, nLeftFace, nRightFace);
    return true;
}


bool CSphericalGraph::write(std::ostream& outfile)
{
    try
    {
        int nNumberNodes = int(m_Nodes.size());
        // write number of nodes
        outfile << nNumberNodes << std::endl;

        // write all nodes
        for (int n = 0 ; n < nNumberNodes ; n++)
        {
            writeNode(outfile, n);
        }

        // write number of edges
        int nNumberEdges = int(m_Edges.size());
        outfile << nNumberEdges << std::endl;

        for (int e = 0 ; e < nNumberEdges ; e++)
        {
            writeEdge(outfile, e);
        }

    }
    catch (std::ostream::failure&)
    {
        printf("ERROR: failed to write FeatureGraph to file\n");
        return false;
    }

    return true;
}

bool CSphericalGraph::writeNode(std::ostream& outfile, int n)
{
    CSGNode* pCurrentNode = m_Nodes.at(n);

    outfile << pCurrentNode->getIndex() << std::endl;
    outfile << pCurrentNode->getPosition().fPhi << std::endl;
    outfile << pCurrentNode->getPosition().fTheta << std::endl;

    outfile << std::endl;
    return true;
}

bool CSphericalGraph::writeEdge(std::ostream& outfile, int e)
{
    CSGEdge* pCurrentEdge = m_Edges.at(e);
    outfile << pCurrentEdge->nIndex1 << " ";
    outfile << pCurrentEdge->nIndex2 << " ";

    outfile << pCurrentEdge->nLeftFace << " ";
    outfile << pCurrentEdge->nRightFace << " ";

    outfile << std::endl;
    return true;
}
