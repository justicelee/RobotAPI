/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotHealth
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/util/CPPUtility/trace.h>

#include "RobotHealth.h"

#include <boost/regex.hpp>


namespace armarx
{
    void RobotHealth::onInitComponent()
    {
        ARMARX_TRACE;
        monitorHealthTask = new PeriodicTask<RobotHealth>(this, &RobotHealth::monitorHealthTaskClb, 10);
        defaultMaximumCycleTimeWarn = getProperty<int>("MaximumCycleTimeWarnMS").getValue();
        defaultMaximumCycleTimeErr = getProperty<int>("MaximumCycleTimeErrMS").getValue();
        usingTopic(getProperty<std::string>("RobotHealthTopicName").getValue());
        reportErrorsWithSpeech = getProperty<bool>("ReportErrorsWithSpeech").getValue();
        speechMinimumReportInterval = getProperty<int>("SpeechMinimumReportInterval").getValue();

        ARMARX_TRACE;
        //robotUnitRequired = getProperty<bool>("RobotUnitRequired").getValue();
        /*if(robotUnitRequired)
        {
            ARMARX_IMPORTANT << "RobotUnit is required";
            usingProxy(getProperty<std::string>("RobotUnitName").getValue());
        }*/


        //usingProxy(getProperty<std::string>("RemoteGuiName").getValue());
    }


    void RobotHealth::onConnectComponent()
    {
        ARMARX_TRACE;
        emergencyStopTopicPrx = getTopic<EmergencyStopListenerPrx>(getProperty<std::string>("EmergencyStopTopicName").getValue());
        //remoteGuiPrx = getProxy<RemoteGuiInterfacePrx>(getProperty<std::string>("RemoteGuiName").getValue());
        aggregatedRobotHealthTopicPrx = getTopic<AggregatedRobotHealthInterfacePrx>(getProperty<std::string>("AggregatedRobotHealthTopicName").getValue());
        textToSpeechTopic = getTopic<TextListenerInterfacePrx>(getProperty<std::string>("TextToSpeechTopicName").getValue());
        lastSpeechOutput = TimeUtil::GetTime();

        /*if(robotUnitRequired)
        {
            robotUnitPrx = getTopic<RobotUnitInterfacePrx>(getProperty<std::string>("RobotUnitName").getValue());
            robotUnitPrx->enableHeartbeat();
        }*/

        monitorHealthTask->start();
    }

    void RobotHealth::monitorHealthTaskClb()
    {
        ARMARX_TRACE;
        long now = TimeUtil::GetTime().toMicroSeconds();
        bool hasNewErr = false;

        RobotHealthState healthState = RobotHealthState::HealthOK;

        for (size_t i = 0; i < validEntries; i++)
        {
            ARMARX_TRACE;
            Entry& e = entries.at(i);
            long delta = std::max(now - e.lastUpdate, e.lastDelta);
            if (e.required && delta > e.maximumCycleTimeErr)
            {
                healthState = RobotHealthState::HealthError;
            }

            if (!e.enabled)
            {
                continue;
            }
            ARMARX_TRACE;


            if (delta > e.maximumCycleTimeErr)
            {
                ARMARX_TRACE;
                healthState = RobotHealthState::HealthError;
                if (e.isRunning)
                {
                    ARMARX_TRACE;
                    ARMARX_ERROR << deactivateSpam(0.1, e.name) << "Component " << e.name << " has died.";
                    if (reportErrorsWithSpeech && (TimeUtil::GetTime() - lastSpeechOutput).toSecondsDouble() > speechMinimumReportInterval)
                    {
                        ARMARX_TRACE;
                        lastSpeechOutput = TimeUtil::GetTime();

                        std::string name;

                        boost::smatch m;
                        boost::regex regex("^[a-zA-Z]+");
                        if (boost::regex_search(e.name, m, regex))
                        {
                            if (m.empty())
                            {
                                name = "Whatever";
                            }
                            else
                            {
                                name = m[0].str();
                            }
                        }

                        textToSpeechTopic->reportText("Oh no! Component " + name + " is no longer running.");
                    }
                    hasNewErr = true;
                    e.isRunning = false;
                }
            }
            else if (e.isRunning && delta > e.maximumCycleTimeWarn)
            {
                ARMARX_TRACE;
                ARMARX_WARNING << deactivateSpam(0.1, e.name) << "Component " << e.name << " is running too slow.";
                if (healthState == RobotHealthState::HealthOK)
                {
                    healthState = RobotHealthState::HealthWarning;
                }
            }

        }
        if (hasNewErr)
        {
            ARMARX_TRACE;
            emergencyStopTopicPrx->reportEmergencyStopState(EmergencyStopState::eEmergencyStopActive);
        }
        ARMARX_TRACE;
        aggregatedRobotHealthTopicPrx->aggregatedHeartbeat(healthState);
        /*if(allRequiredRunning && robotUnitPrx)
        {
            try
            {
                robotUnitPrx->sendHeartbeat();
            }
            catch(...)
            {}
        }*/
    }

    void RobotHealth::onDisconnectComponent()
    {
        //robotUnitPrx = nullptr;
        monitorHealthTask->stop();
    }


    void RobotHealth::onExitComponent()
    {

    }

    armarx::PropertyDefinitionsPtr RobotHealth::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new RobotHealthPropertyDefinitions(
                getConfigIdentifier()));
    }

    RobotHealth::Entry& RobotHealth::findOrCreateEntry(const std::string& name)
    {
        ARMARX_TRACE;
        for (size_t i = 0; i < entries.size(); i++)
        {
            if (entries.at(i).name == name)
            {
                return entries.at(i);
            }
        }
        std::unique_lock lock(mutex);
        ARMARX_INFO << "registering '" << name << "'";
        entries.emplace_back(name, defaultMaximumCycleTimeWarn * 1000, defaultMaximumCycleTimeErr * 1000);
        validEntries++;
        return entries.back();
    }



    void RobotHealth::heartbeat(const std::string& componentName, const RobotHealthHeartbeatArgs& args, const Ice::Current&)
    {
        ARMARX_TRACE;
        Entry& e = findOrCreateEntry(componentName);
        long now = TimeUtil::GetTime().toMicroSeconds();
        e.maximumCycleTimeWarn = args.maximumCycleTimeWarningMS * 1000;
        e.maximumCycleTimeErr = args.maximumCycleTimeErrorMS * 1000;
        if (!e.isRunning)
        {
            ARMARX_INFO << "'" << componentName << "' is now alive and running";
            e.lastDelta = 0;
        }
        else
        {
            e.lastDelta = now - e.lastUpdate;
            e.history.at(e.historyIndex) = e.lastDelta;
            e.historyIndex = (e.historyIndex + 1) % e.history.size();
        }
        ARMARX_TRACE;
        e.lastUpdate = now;
        e.isRunning = true;
        e.enabled = true;
        {
            std::unique_lock lock(e.messageMutex);
            e.message = args.message;
        }
    }

    void armarx::RobotHealth::unregister(const std::string& componentName, const Ice::Current&)
    {
        ARMARX_TRACE;
        Entry& e = findOrCreateEntry(componentName);
        e.isRunning = false;
        e.enabled = false;
        ARMARX_INFO << "unregistering " << componentName;
    }

    std::string RobotHealth::getSummary(const Ice::Current&)
    {
        ARMARX_TRACE;
        long now = TimeUtil::GetTime().toMicroSeconds();
        std::stringstream ss;
        for (size_t i = 0; i < validEntries; i++)
        {
            ARMARX_TRACE;
            Entry& e = entries.at(i);
            long delta = std::max(now - e.lastUpdate, e.lastDelta);

            long minDelta = delta;
            long maxDelta = delta;
            for (size_t i = 1; i < e.history.size(); i++)
            {
                long historicalDelta = e.history.at(i);
                if (historicalDelta < 0)
                {
                    break;
                }
                minDelta = std::min(minDelta, historicalDelta);
                maxDelta = std::max(maxDelta, historicalDelta);
            }

            ss << e.name;
            if (e.required)
            {
                ss << ", required";
            }

            if (!e.required && !e.enabled)
            {
                ss << ": disabled";
            }
            else
            {
                if (delta > e.maximumCycleTimeErr)
                {
                    ss << ": ERROR";
                }
                else if (delta > e.maximumCycleTimeWarn)
                {
                    ss << ": warning";
                }
                else
                {
                    ss << ": ok";
                }
                ss << " (" << (delta / 1000) << "ms, min: " << (minDelta / 1000) << "ms, max: " << (maxDelta / 1000) << "ms)";
            }
            ARMARX_TRACE;

            std::unique_lock lock(e.messageMutex);
            if (e.message.size())
            {
                ss << " - " << e.message;
            }

            ss << "\n";

        }
        return ss.str();
    }
}
