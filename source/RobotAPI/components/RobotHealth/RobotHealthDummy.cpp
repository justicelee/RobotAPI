/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotHealthDummy
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotHealthDummy.h"
#include <time.h>
#include <thread>

namespace armarx
{
    void RobotHealthDummy::onInitComponent()
    {
        dummyTask = new RunningTask<RobotHealthDummy>(this, &RobotHealthDummy::runTask);
        getProperty(sleepmode, "SleepMode");
    }


    void RobotHealthDummy::onConnectComponent()
    {
        robotHealthTopicPrx = getTopic<RobotHealthInterfacePrx>(getProperty<std::string>("RobotHealthTopicName").getValue());
        dummyTask->start();
    }

#define NANOSECS_PER_SEC  1000000000
    int RobotHealthDummy::NanoSleep(long nanosec)
    {
        struct timespec ts;
        ts.tv_sec = nanosec / NANOSECS_PER_SEC;
        ts.tv_nsec = (nanosec % NANOSECS_PER_SEC);
        return nanosleep(&ts, nullptr); // jitter up to +100ms!
    }

    void RobotHealthDummy::sleepwait(long microseconds)
    {
        long start = TimeUtil::GetTime().toMicroSeconds();
        auto end = start + microseconds;
        do
        {
            NanoSleep(1000);
        }
        while (TimeUtil::GetTime().toMicroSeconds() < end);
    }
    void RobotHealthDummy::yieldwait(long microseconds)
    {
        long start = TimeUtil::GetTime().toMicroSeconds();
        auto end = start + microseconds;
        do
        {
            std::this_thread::yield();
        }
        while (TimeUtil::GetTime().toMicroSeconds() < end); // jitter up to +25ms...
    }
    void RobotHealthDummy::busydwait(long microseconds)
    {
        long start = TimeUtil::GetTime().toMicroSeconds();
        auto end = start + microseconds;
        do
        {
            ;
        }
        while (TimeUtil::GetTime().toMicroSeconds() < end);
    }

    void RobotHealthDummy::runTask()
    {



        ARMARX_INFO << "starting rinning task";
        while (!dummyTask->isStopped())
        {
            long beforeTopicCall = TimeUtil::GetTime().toMicroSeconds();
            //ARMARX_INFO << "send heartbeat";
            robotHealthTopicPrx->heartbeat(getName(), RobotHealthHeartbeatArgs());
            long afterTopicCall = TimeUtil::GetTime().toMicroSeconds();
            if (sleepmode == "nanosleep")
            {
                NanoSleep(10 * 1000 * 1000);
            }
            else if (sleepmode == "sleepwait")
            {
                sleepwait(10 * 1000);
            }
            else if (sleepmode == "yieldwait")
            {
                yieldwait(10 * 1000);
            }
            else if (sleepmode == "busywait")
            {
                busydwait(10 * 1000);
            }
            else
            {
                throw LocalException("Unknown sleepmode.");
            }

            long afterSleep = TimeUtil::GetTime().toMicroSeconds();
            long topicCallDelay = afterTopicCall - beforeTopicCall;
            long sleepDelay = afterSleep - afterTopicCall;
            if (sleepDelay > 20000)
            {
                ARMARX_IMPORTANT << sleepmode << ": " << sleepDelay << "us";
            }
            if (topicCallDelay > 1000)
            {
                ARMARX_IMPORTANT << "topic: " << topicCallDelay << "us";
            }
        }
    }


    void RobotHealthDummy::onDisconnectComponent()
    {
        //ARMARX_IMPORTANT << "onDisconnectComponent";
        dummyTask->stop();
    }


    void RobotHealthDummy::onExitComponent()
    {
        //ARMARX_IMPORTANT << "onExitComponent";
        dummyTask->stop();
    }

    armarx::PropertyDefinitionsPtr RobotHealthDummy::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new RobotHealthDummyPropertyDefinitions(
                getConfigIdentifier()));
    }
}
