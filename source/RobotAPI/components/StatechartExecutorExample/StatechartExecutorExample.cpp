/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::StatechartExecutorExample
 * @author     Stefan Reither ( stefan dot reither at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StatechartExecutorExample.h"
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/RemoteGui/Storage.h>

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>

#include <ArmarXCore/statechart/StateParameter.h>


#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/interface/statechart/RemoteStateOffererIce.h>
#include <ArmarXCore/statechart/StateBase.h>
#include <ArmarXCore/statechart/StateUtilFunctions.h>
#include <ArmarXCore/statechart/StateUtil.h>
#include <ArmarXCore/statechart/State.h>

#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    DEFINEEVENT(EvAbort)

    StatechartExecutorExamplePropertyDefinitions::StatechartExecutorExamplePropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {

    }


    std::string StatechartExecutorExample::getDefaultName() const
    {
        return "StatechartExecutorExample";
    }


    void StatechartExecutorExample::onInitComponent()
    {
        usingProxy("SimpleStatechartExecutor");
        usingProxy("RemoteGuiProvider");
    }


    void StatechartExecutorExample::onConnectComponent()
    {
        getProxy(_statechartExecutor, "SimpleStatechartExecutor");

        getProxy(_remoteGuiPrx, "RemoteGuiProvider");
        _tabName = "StatechartExecutorExample";
        setupRemoteGuiWidget();
        _remoteGuiTab = RemoteGui::TabProxy(_remoteGuiPrx, _tabName);

        _remoteGuiTask = new RunningTask<StatechartExecutorExample>(this, &StatechartExecutorExample::runRemoteGui);
        _remoteGuiTask->start();
    }


    void StatechartExecutorExample::onDisconnectComponent()
    {

    }


    void StatechartExecutorExample::onExitComponent()
    {

    }


    armarx::PropertyDefinitionsPtr StatechartExecutorExample::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new StatechartExecutorExamplePropertyDefinitions(
                getConfigIdentifier()));
    }

    using namespace RemoteGui;
    void StatechartExecutorExample::setupRemoteGuiWidget()
    {
        auto grid = makeSimpleGridLayout("Layout").cols(3);

        WidgetPtr test0Button = makeButton("Test_normal");
        grid.addChild(test0Button);
        WidgetPtr test1Button = makeButton("Test_args");
        grid.addChild(test1Button);
        WidgetPtr test2Button = makeButton("Test_wrongProxy");
        grid.addChild(test2Button);

        auto hlayout = makeHBoxLayout("HLayout");
        hlayout.addChild(grid);

        WidgetPtr stopButton = makeButton("Stop").label("Stop!!");
        hlayout.addChild(stopButton);

        auto manualLayout = makeSimpleGridLayout("manualLayout").cols(2);
        WidgetPtr label1 = makeLabel("ProxyName").value("ProxyName");
        manualLayout.addChild(label1);
        WidgetPtr lineedit1 = makeLineEdit("ProxyEdit");
        manualLayout.addChild(lineedit1);
        WidgetPtr label2 = makeLabel("StateName").value("StateName");
        manualLayout.addChild(label2);
        WidgetPtr lineedit2 = makeLineEdit("StateEdit");
        manualLayout.addChild(lineedit2);
        WidgetPtr runManual = makeButton("Run");
        manualLayout.addChild(runManual);

        auto vlayout = makeVBoxLayout("VLayout");
        vlayout.addChildren({hlayout, manualLayout});

        _remoteGuiPrx->createTab(_tabName, vlayout);
    }

    void StatechartExecutorExample::runRemoteGui()
    {
        int cycleDurationMs = 20;
        CycleUtil c(cycleDurationMs);
        while (!_remoteGuiTask->isStopped())
        {
            _remoteGuiTab.receiveUpdates();

            if (_remoteGuiTab.getButton("Stop").clicked())
            {
                _statechartExecutor->stopImmediatly();

                auto output = _statechartExecutor->getSetOutputParameters();
                for (const auto& p : output)
                {
                    SingleVariantPtr var = SingleVariantPtr::dynamicCast(p.second);
                    ARMARX_INFO << p.first << ": " << var->get()->getOutputValueOnly();
                }
            }

            if (_remoteGuiTab.getButton("Test_normal").clicked())
            {
                std::string proxyName = "StatechartExecutionGroupRemoteStateOfferer";
                std::string stateName = "TestStateForStatechartExecution";


                StateParameterMap inputMap;
                PosePtr p = new Pose();
                //                StateParameterPtr param = StateParameter::create();
                //                param->value = new SingleVariant(p);
                //                param->set = true;
                //                inputMap["bla2"] = param;

                StateParameterPtr param2 = StateParameter::create();
                param2->value = new SingleVariant(1);
                param2->set = true;
                inputMap["intTest"] = param2;

                std::vector<PosePtr> v;
                v.push_back(p);
                StateParameterPtr param3 = StateParameter::create();
                param3->value = SingleTypeVariantList::FromStdVector(v);
                param3->set = true;
                inputMap["intListTest"] = param3;

                std::map<std::string, PosePtr> m;
                m["a"] = p;
                m["b"] = p;
                StateParameterPtr param4 = StateParameter::create();
                param4->value = StringValueMap::FromStdMap<PosePtr>(m);
                param4->set = true;
                inputMap["poseMapTest"] = param4;

                _statechartExecutor->ensureVariantLibrariesAreLoaded(inputMap);
                _statechartExecutor->startStatechart(proxyName, stateName, inputMap);

                //                StatechartExecutionResult result = _statechartExecutor->waitUntilStatechartExecutionIsFinished();
                //                ARMARX_INFO << VAROUT(result);
                //                StringVariantContainerBaseMap output = _statechartExecutor->getSetOutputParameters();


                //                int outInt = SingleVariantPtr::dynamicCast(output["OutputInt"])->get()->get<int>();
                //                ARMARX_INFO << VAROUT(outInt);

                //                PosePtr pose = SingleVariantPtr::dynamicCast(output["OutputPose"])->get()->get<Pose>();
                //                ARMARX_INFO << VAROUT(pose);
            }

            if (_remoteGuiTab.getButton("Test_args").clicked())
            {

            }

            if (_remoteGuiTab.getButton("Test_wrongProxy").clicked())
            {
                StringVariantContainerBaseMap output = _statechartExecutor->getSetOutputParameters();
                if (!output.empty())
                {
                    int outInt = SingleVariantPtr::dynamicCast(output["OutputInt"])->get()->get<int>();
                    ARMARX_INFO << VAROUT(outInt);
                }

            }

            if (_remoteGuiTab.getButtonClicked("Run"))
            {

            }

            _remoteGuiTab.sendUpdates();
            c.waitForCycleDuration();
        }
    }
}
