/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::GamepadControlUnit
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GamepadControlUnit.h"

#include <ArmarXCore/observers/variant/TimestampVariant.h>





namespace armarx
{
    void GamepadControlUnit::onInitComponent()
    {
        ARMARX_INFO << "oninit GamepadControlUnit";
        usingProxy(getProperty<std::string>("PlatformUnitName").getValue());
        usingTopic(getProperty<std::string>("GamepadTopicName").getValue());
        usingProxy("EmergencyStopMaster");

        scaleX = getProperty<float>("ScaleX").getValue();
        scaleY = getProperty<float>("ScaleY").getValue();
        scaleRotation = getProperty<float>("ScaleAngle").getValue();
        ARMARX_INFO << "oninit GamepadControlUnit end";


    }


    void GamepadControlUnit::onConnectComponent()
    {
        ARMARX_INFO << "onConnect GamepadControlUnit";
        platformUnitPrx = getProxy<PlatformUnitInterfacePrx>(getProperty<std::string>("PlatformUnitName").getValue());
        emergencyStop = getProxy<EmergencyStopMasterInterfacePrx>("EmergencyStopMaster");
    }


    void GamepadControlUnit::onDisconnectComponent()
    {

    }


    void GamepadControlUnit::onExitComponent()
    {
        ARMARX_INFO << "exit GamepadControlUnit";
    }

    armarx::PropertyDefinitionsPtr GamepadControlUnit::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new GamepadControlUnitPropertyDefinitions(
                getConfigIdentifier()));
    }

    void GamepadControlUnit::reportGamepadState(const std::string& device, const std::string& name, const GamepadData& data, const TimestampBasePtr& timestamp, const Ice::Current& c)
    {
        if (data.leftTrigger > 0)
        {
            emergencyStop->setEmergencyStopState(EmergencyStopState::eEmergencyStopActive);
            return;
        }
        else if (data.startButton)
        {
            emergencyStop->setEmergencyStopState(EmergencyStopState::eEmergencyStopInactive);
        }
        //scales are for the robot axis
        if (data.rightTrigger > 0)
        {
            platformUnitPrx->move(data.leftStickY  * scaleX, data. leftStickX  * scaleY, data.rightStickX * scaleRotation);
        }
        else
        {
            platformUnitPrx->move(0, 0, 0);
        }
        //ARMARX_INFO << "sending targets" << data.leftStickX* scaleX << " " << data.leftStickY* scaleY << " " << data.rightStickX* scaleRotation;
    }
}
