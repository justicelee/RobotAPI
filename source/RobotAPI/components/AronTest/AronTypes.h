#ifndef ARONTESTPRODUCER_ARON_TYPE_DEFINITION_INCLUDE_GUARD
#define ARONTESTPRODUCER_ARON_TYPE_DEFINITION_INCLUDE_GUARD

#include <string>
#include <vector>
#include <map>
#include <Eigen/Core>
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/AronWriter.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/AronReader.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/classWriters/AronDataNavigatorWriter/AronDataNavigatorWriter.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/classReaders/AronDataNavigatorReader/AronDataNavigatorReader.h>

namespace armarx
{
    class NaturalIKResult
    {
    public:
        std::vector<float> jointValues;
        bool reached;
        NaturalIKResult()
        {
        }

    public:
        void write(armarx::aron::io::AronWriter& w) const
        {
            w.writeStartObject();
            w.writeKey("jointValues");
            w.writeStartList();
            for (unsigned int jointValues_index = 0; jointValues_index < jointValues.size(); ++jointValues_index)
            {
                w.writeFloat(jointValues[jointValues_index]);
            }
            w.writeEndList();
            w.writeKey("reached");
            w.writeBool(reached);
            w.writeEndObject();
        }
        void read(armarx::aron::io::AronReader& r)
        {
            r.readStartObject();
            r.readKey("jointValues");
            r.readStartList();
            jointValues.clear();
            while (!r.readEndList())
            {
                float jointValues_iterator = 0;
                jointValues_iterator = r.readFloat();
                jointValues.push_back(jointValues_iterator);
            }
            r.readKey("reached");
            reached = r.readBool();
            r.readEndObject();
        }
        armarx::aron::data::AronDataPtr toAron() const
        {
            armarx::aron::io::AronDataNavigatorWriter writer;
            this->write(writer);
            return writer.getResult();
        }
        void fromAron(const armarx::aron::data::AronDataPtr& input)
        {
            armarx::aron::io::AronDataNavigatorReader reader(input);
            this->read(reader);
        }
    }; // class NaturalIKResult
} // namespace armarx

#endif // ARONTESTPRODUCER_ARON_TYPE_DEFINITION_INCLUDE_GUARD
