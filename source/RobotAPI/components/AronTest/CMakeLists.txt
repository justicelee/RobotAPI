armarx_component_set_name("AronTest")


set(COMPONENT_LIBS
    ArmarXCore
    ArmarXCoreInterfaces
    aron
)

set(SOURCES
    ./AronTest.cpp
)
set(HEADERS
    ./AronTest.h
    ./AronTypes.h
)


armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
#add_subdirectory(test)
