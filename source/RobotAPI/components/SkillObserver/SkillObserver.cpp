/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::SkillObserver
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/IceGridAdmin.h>
#include <ArmarXCore/core/IceManager.h>

#include "SkillObserver.h"


namespace armarx::skills
{
    armarx::PropertyDefinitionsPtr SkillObserver::createPropertyDefinitions()
    {
        PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());
        defs->topic<ProvidedSkillsTopicPrx>("SkillProviderTopic",
                                            "SkillProviderTopic",
                                            "Topic where skill providers provide their topics");
        return defs;
    }

    std::string SkillObserver::getDefaultName() const
    {
        return "SkillObserver";
    }

    void SkillObserver::onInitComponent()
    {
    }

    void SkillObserver::onConnectComponent()
    {
        if (!_update_thread.joinable())
        {
            _stop_update_thread = false;
            _update_thread = std::thread {[&]{update_task();}};
        }
    }

    void SkillObserver::onDisconnectComponent()
    {
    }

    void SkillObserver::onExitComponent()
    {
        _stop_update_thread = true;
        if (_update_thread.joinable())
        {
            _update_thread.join();
        }
    }

    ProviderSkillsSeq SkillObserver::getSkills(const Ice::Current&) const
    {
        std::lock_guard g{_skills_mutex};
        ProviderSkillsSeq s;
        for (const auto& [pr, info] : _skills)
        {
            s.emplace_back(info);
        }
        return s;
    }

    void SkillObserver::skillsProvided(const SkillProviderInterfacePrx& provider, const SkillDescriptionMap &skills, const Ice::Current&)
    {
        std::lock_guard g{_skills_mutex};
        ARMARX_CHECK_NOT_NULL(provider);
        auto& info = _skills[provider];
        info.provider          = provider;
        info.skills            = skills;
        info.pingTimestampUs   = timestamp();
        info.updateTimestampUs = info.pingTimestampUs;
        info.providerName      = info.provider->ice_getIdentity().name;
    }

    void SkillObserver::removeProvider(const SkillProviderInterfacePrx& provider, const Ice::Current&)
    {
        std::lock_guard g{_skills_mutex};
        _skills.erase(provider);
    }

    void SkillObserver::update_task()
    {
        while (!_stop_update_thread)
        {
            //locked
            {
                std::lock_guard g{_skills_mutex};
                if (_scan_for_skills)
                {
                    ARMARX_INFO << "scanning for skills...";
                    const auto iceGridSession = getArmarXManager()->getIceManager()->getIceGridSession();
                    IceGrid::AdminPrx admin = iceGridSession->getAdmin();
                    IceGrid::ObjectInfoSeq objects = admin->getAllObjectInfos("*");
                    ARMARX_INFO << "found " << objects.size() << " objects";
                    ProviderSkillsMap found_skills;
                    ProviderSkills pskills;
                    for (const auto& o : objects)
                    {
                        try
                        {
                            pskills.provider = SkillProviderInterfacePrx::checkedCast(o.proxy->ice_timeout(60));
                            if (pskills.provider)
                            {
                                pskills.skills            = pskills.provider->getSkills();
                                pskills.pingTimestampUs   = timestamp();
                                pskills.updateTimestampUs = pskills.pingTimestampUs;
                                pskills.providerName      = pskills.provider->ice_getIdentity().name;
                                found_skills[pskills.provider] = pskills;
                            }
                        }
                        catch (...)
                        {
                        }
                    }
                    _skills = std::move(found_skills);
                    ARMARX_INFO << "scanning for skills...done!";
                    _scan_for_skills = false;
                }
                else
                {
                    //ping objects
                    std::set<SkillProviderInterfacePrx> to_remove;
                    for (auto& [pr, info] : _skills)
                    {
                        try
                        {
                            pr->ice_ping();
                            info.pingTimestampUs   = timestamp();
                        }
                        catch (...)
                        {

                        }
                    }
                    if(!to_remove.empty())
                    {
                        ARMARX_INFO << "removing " << to_remove.size() << " skills (timeout on ping)";
                    }
                    for (const auto& p : to_remove)
                    {
                        _skills.erase(p);
                    }
                }
                //write stats
                {
                    std::size_t number_of_skills = 0;
                    for (const auto& [pr, info] : _skills)
                    {
                        number_of_skills += info.skills.size();
                    }
                    setDebugObserverDatafield("number_of_providers", _skills.size());
                    setDebugObserverDatafield("number_of_skills", number_of_skills);
                    sendDebugObserverBatch();
                }
            }
            std::this_thread::sleep_for(std::chrono::milliseconds{_update_thread_period_ms});
        }
    }

    long SkillObserver::timestamp()
    {
        return std::chrono::duration_cast<std::chrono::microseconds>(
                   std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    }

}
