/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::MoveJointSkillExample
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/IceGridAdmin.h>
#include <ArmarXCore/core/IceManager.h>

#include "MoveJointSkillExample.h"


namespace armarx::skills
{
    armarx::PropertyDefinitionsPtr MoveJointSkillExample::createPropertyDefinitions()
    {
        PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());
        defs->topic(_skill_topic,
                    "SkillProviderTopic",
                    "SkillProviderTopic",
                    "Topic where skill providers provide their skills");
        return defs;
    }

    std::string MoveJointSkillExample::getDefaultName() const
    {
        return "MoveJointSkillExample";
    }

    void MoveJointSkillExample::onInitComponent()
    {
        std::lock_guard l{_skills_mutex};
        _skills.emplace(
            "SetZeroPos",
            [&]
        {
            NameControlModeMap c;
            NameValueMap       t;
            for (const auto& j : getKinematicUnit()->getJoints())
            {
                c[j] = ControlMode::ePositionControl;
                t[j] = 0;
            }
            getKinematicUnit()->switchControlMode(c);
            getKinematicUnit()->setJointAngles(t);
            return false;
        }
        );
        _skills.emplace(
            "SetZeroVel",
            [&]
        {
            NameControlModeMap c;
            NameValueMap       t;
            for (const auto& j : getKinematicUnit()->getJoints())
            {
                c[j] = ControlMode::eVelocityControl;
                t[j] = 0;
            }
            getKinematicUnit()->switchControlMode(c);
            getKinematicUnit()->setJointVelocities(t);
            return false;
        }
        );
    }

    void MoveJointSkillExample::onConnectComponent()
    {
        std::lock_guard l{_skills_mutex};
        getProxy(_self_prx, -1);
        _skill_topic->skillsProvided(_self_prx, getSkills());
    }

    void MoveJointSkillExample::onDisconnectComponent()
    {
        std::lock_guard l{_skills_mutex};
        _skill_topic->removeProvider(_self_prx);
    }

    void MoveJointSkillExample::onExitComponent()
    {
    }

    SkillDescriptionMap MoveJointSkillExample::getSkills(const Ice::Current&) const
    {
        std::lock_guard l{_skills_mutex};
        SkillDescriptionMap s;
        for (const auto& [key, value] : _skills)
        {
            s[key];
        }
        return s;
    }

    SkillStatusMap MoveJointSkillExample::getSkillExecutionStatus(const Ice::Current&) const
    {
        std::lock_guard l{_skills_mutex};
        SkillStatusMap s;
        for (const auto& [key, value] : _skills)
        {
            s[key] = value.status();
        }
        return s;
    }

    void MoveJointSkillExample::executeSkill(const SkillParametrization& p, const Ice::Current&)
    {
        std::lock_guard l{_skills_mutex};
        ARMARX_CHECK_EXPRESSION(_skills.count(p.skillName));
        _skills.at(p.skillName).do_start();
    }

    SkillStatus MoveJointSkillExample::abortSkill(const std::string& skill, const Ice::Current&)
    {
        std::lock_guard l{_skills_mutex};
        if (_skills.count(skill))
        {
            _skills.at(skill).do_stop();
            return _skills.at(skill).status();
        }
        return {};
    }

    void MoveJointSkillExample::SkillExecData::do_start()
    {
        do_stop();
        _stop = false;
        _thread = std::thread
        {
            [&]
            {
                {
                    std::lock_guard l{_status_mutex};
                    _status.status = ExecutionStatus::Running;
                }
                try
                {
                    while (!_stop && f())
                    {
                        std::this_thread::sleep_for(std::chrono::milliseconds{100});
                    }
                    std::lock_guard l{_status_mutex};
                    _status.status = _stop ? ExecutionStatus::Aborted : ExecutionStatus::Succeeded;
                }
                catch (...)
                {
                    std::lock_guard l{_status_mutex};
                    _status.status = ExecutionStatus::Failed;
                }
            }
        };
    }

    void MoveJointSkillExample::SkillExecData::do_stop()
    {
        _stop = true;
        if (_thread.joinable())
        {
            _thread.join();
        }
    }

    MoveJointSkillExample::SkillExecData::~SkillExecData()
    {
        do_stop();
    }

    SkillStatus MoveJointSkillExample::SkillExecData::status() const
    {
        std::lock_guard l{_status_mutex};
        return _status;
    }


}
