#pragma once

#include <thread>
#include <mutex>

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/KinematicUnitComponentPlugin.h>

#include <RobotAPI/interface/skills/SkillObserverInterface.h>


namespace armarx::skills
{
    class MoveJointSkillExample :
        virtual public Component,
        virtual public SkillProviderInterface,
        virtual public KinematicUnitComponentPluginUser
    {
    public:
        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
    protected:

        void onInitComponent()       override;
        void onConnectComponent()    override;
        void onDisconnectComponent() override;
        void onExitComponent()       override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    public:
        SkillDescriptionMap getSkills(const Ice::Current& = Ice::emptyCurrent) const override;
        SkillStatusMap getSkillExecutionStatus(const Ice::Current& = Ice::emptyCurrent) const override;
        void executeSkill(const SkillParametrization& p, const Ice::Current& = Ice::emptyCurrent) override;
        SkillStatus abortSkill(const std::string& skill, const Ice::Current& = Ice::emptyCurrent) override;

    private:
        struct SkillExecData
        {
            std::function<bool()> f;

            SkillExecData(auto&& fnc)
                : f(std::move(fnc))
            {
            }
            void do_start();
            void do_stop();
            ~SkillExecData();
            SkillStatus status() const;
        private:
            std::thread        _thread;
            std::atomic_bool   _stop{false};
            SkillStatus        _status;
            mutable std::mutex _status_mutex;
        };

        SkillProviderInterfacePrx            _self_prx;
        mutable std::recursive_mutex         _skills_mutex;
        std::map<std::string, SkillExecData> _skills;
        ProvidedSkillsTopicPrx               _skill_topic;
    };
}
