/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObjectPoseProviderExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectPoseProviderExample.h"

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <ArmarXCore/core/time/CycleUtil.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>

namespace armarx
{
    ObjectPoseProviderExamplePropertyDefinitions::ObjectPoseProviderExamplePropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr ObjectPoseProviderExample::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ObjectPoseProviderExamplePropertyDefinitions(getConfigIdentifier());

        defs->topic(debugObserver);

        defs->optional(initialObjectIDs, "Objects", "Object IDs of objects to be tracked.")
        .map(simox::alg::join(initialObjectIDs, ", "), initialObjectIDs);

        return defs;
    }

    std::string ObjectPoseProviderExample::getDefaultName() const
    {
        return "ObjectPoseProviderExample";
    }

    void ObjectPoseProviderExample::onInitComponent()
    {
        for (const auto& initial : initialObjectIDs)
        {
            // -1 -> infinitely
            requestedObjects.requestObjects({ObjectID(initial)}, -1);
        }

        {
            providerInfo.objectType = objpose::ObjectTypeEnum::KnownObject;
            std::vector<ObjectInfo> objects = objectFinder.findAllObjectsOfDataset("KIT");
            for (const auto& obj : objects)
            {
                providerInfo.supportedObjects.push_back(armarx::toIce(obj.id()));
            }
        }
    }

    void ObjectPoseProviderExample::onConnectComponent()
    {
        poseEstimationTask = new SimpleRunningTask<>([this]()
        {
            this->poseEstimationTaskRun();
        });
        poseEstimationTask->start();
    }

    void ObjectPoseProviderExample::onDisconnectComponent()
    {
    }

    void ObjectPoseProviderExample::onExitComponent()
    {
    }

    objpose::ProviderInfo ObjectPoseProviderExample::getProviderInfo(const Ice::Current&)
    {
        return providerInfo;
    }

    objpose::provider::RequestObjectsOutput ObjectPoseProviderExample::requestObjects(
        const objpose::provider::RequestObjectsInput& input, const Ice::Current&)
    {
        ARMARX_INFO << "Requested object IDs for " << input.relativeTimeoutMS << " ms: "
                    << input.objectIDs;
        {
            std::scoped_lock lock(requestedObjectsMutex);
            requestedObjects.requestObjects(input.objectIDs, input.relativeTimeoutMS);
        }

        objpose::provider::RequestObjectsOutput output;
        // All requests are successful.
        for (const auto& id : input.objectIDs)
        {
            output.results[id].success = true;
        }
        return output;
    }

    void ObjectPoseProviderExample::poseEstimationTaskRun()
    {
        CycleUtil cycle(50);
        IceUtil::Time start = TimeUtil::GetTime();

        std::map<ObjectID, ObjectInfo> objectInfos;

        while (poseEstimationTask && !poseEstimationTask->isStopped())
        {
            IceUtil::Time now = TimeUtil::GetTime();
            float t = float((now - start).toSecondsDouble());

            objpose::RequestedObjects::Update update;
            {
                std::scoped_lock lock(requestedObjectsMutex);
                update = requestedObjects.updateRequestedObjects(now);
            }

            if (update.added.size() > 0 || update.removed.size() > 0)
            {
                ARMARX_INFO << "Added: " << update.added
                            << "Removed: " << update.removed;
            }

            int i = 0;
            armarx::objpose::data::ProvidedObjectPoseSeq poses;
            for (const ObjectID& id : update.current)
            {
                if (objectInfos.count(id) == 0)
                {
                    if (std::optional<ObjectInfo> info = objectFinder.findObject(id))
                    {
                        objectInfos.emplace(id, *info);
                    }
                    else
                    {
                        ARMARX_WARNING << "Could not find object class '" << id << "'.";
                    }
                }
                const ObjectInfo& info = objectInfos.at(id);

                armarx::objpose::data::ProvidedObjectPose& pose = poses.emplace_back();
                pose.providerName = getName();
                pose.objectType = objpose::ObjectTypeEnum::KnownObject;

                pose.objectID.dataset = info.id().dataset();
                pose.objectID.className = info.id().className();
                pose.objectID.instanceName = info.id().instanceName();

                Eigen::Vector3f pos = 200 * Eigen::Vector3f(std::sin(t - i), std::cos(t - i), i);
                Eigen::Quaternionf ori = Eigen::Quaternionf::Identity();
                pose.objectPose = new Pose(ori.toRotationMatrix(), pos);
                pose.objectPoseFrame = armarx::GlobalFrame;

                pose.confidence = 1.0;
                pose.timestampMicroSeconds = TimeUtil::GetTime().toMicroSeconds();

                i++;
            }

            ARMARX_VERBOSE << "Reporting " << poses.size() << " object poses";
            objectPoseTopic->reportObjectPoses(getName(), poses);

            cycle.waitForCycleDuration();
        }
    }
}
