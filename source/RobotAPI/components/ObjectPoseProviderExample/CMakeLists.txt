armarx_component_set_name("ObjectPoseProviderExample")


set(COMPONENT_LIBS
    ArmarXCore ArmarXCoreInterfaces  # for DebugObserverInterface
    # RobotAPICore RobotAPIInterfaces
    # RobotAPIComponentPlugins  # for ArViz and other plugins
    RobotAPIArmarXObjects ObjectPoseObserver
)

set(SOURCES
    ObjectPoseProviderExample.cpp
)
set(HEADERS
    ObjectPoseProviderExample.h
)


armarx_add_component("${SOURCES}" "${HEADERS}")

#find_package(MyLib QUIET)
#armarx_build_if(MyLib_FOUND "MyLib not available")
# all target_include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
#if(MyLib_FOUND)
#    target_include_directories(ObjectPoseProviderExample PUBLIC ${MyLib_INCLUDE_DIRS})
#endif()

# add unit tests
add_subdirectory(test)

#generate the application
armarx_generate_and_add_component_executable()
