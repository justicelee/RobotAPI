/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemExampleClient
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <RobotAPI/interface/armem/MemoryInterface.h>
#include <RobotAPI/interface/armem/MemoryNameSystemInterface.h>

#include <RobotAPI/libraries/armem/client/MemoryClientComponentPlugin.h>


namespace armarx
{
    /**
     * @class ArMemExampleClientPropertyDefinitions
     * @brief Property definitions of `ArMemExampleClient`.
     */
    class ArMemExampleClientPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ArMemExampleClientPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-ArMemExampleClient ArMemExampleClient
     * @ingroup RobotAPI-Components
     * A description of the component ArMemExampleClient.
     *
     * @class ArMemExampleClient
     * @ingroup Component-ArMemExampleClient
     * @brief Brief description of class ArMemExampleClient.
     *
     * Detailed description of class ArMemExampleClient.
     */
    class ArMemExampleClient :
        virtual public armarx::Component
        , virtual public armarx::armem::MemoryClientComponentPluginUser
        , virtual public LightweightRemoteGuiComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // LightweightRemoteGuiComponentPluginUser interface
    public:
        void RemoteGui_update() override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        armarx::DebugObserverInterfacePrx debugObserver;

        std::string memoryName = "Example";
        armem::MemoryInterfacePrx memory;

    };
}
