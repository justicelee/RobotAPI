/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemExampleClient
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArMemExampleClient.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/CycleUtil.h>

#include <RobotAPI/interface/aron.h>

#include <RobotAPI/libraries/armem/core/ice_conversions.h>
#include <RobotAPI/libraries/armem/client/MemoryWriter.h>


namespace armarx
{
    ArMemExampleClientPropertyDefinitions::ArMemExampleClientPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr ArMemExampleClient::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ArMemExampleClientPropertyDefinitions(getConfigIdentifier());

        defs->topic(debugObserver);

        defs->optional(memoryName, "mem.MemoryName", "Name of the memory to use.");
        // defs->component(memory, "ArMemExampleMemory");

        return defs;
    }


    std::string ArMemExampleClient::getDefaultName() const
    {
        return "ArMemExampleClient";
    }


    void ArMemExampleClient::onInitComponent()
    {
    }


    void ArMemExampleClient::onConnectComponent()
    {
        {
            ARMARX_IMPORTANT << "Waiting for memory '" << memoryName << "'.";
            armem::Time start = armem::Time::now();
            armem::data::WaitForMemoryInput input;
            input.name = memoryName;
            armem::data::WaitForMemoryResult result = memoryNameSystem->waitForMemory(input);
            if (result.success && result.proxy)
            {
                memory = result.proxy;
            }
            ARMARX_IMPORTANT << "Got memory '" << memoryName << "'"
                             << " (took " << armem::toStringMilliSeconds(armem::Time::now() - start) << ").";
        }
        ARMARX_CHECK_NOT_NULL(memory);

        armem::MemoryID providerID;
        if (true)
        {
            armem::data::AddSegmentInput input;
            input.coreSegmentName = "ExampleModality";
            input.providerSegmentName = "FancyMethodModality";

            ARMARX_IMPORTANT
                    << "Adding segment:"
                    << "\n- core segment:     \t'" << input.coreSegmentName << "'"
                    << "\n- provider segment: \t'" << input.providerSegmentName << "'"
                    ;

            armem::data::AddSegmentsResult results = memory->addSegments({input});
            armem::data::AddSegmentResult& result = results.at(0);

            ARMARX_INFO << "Output: "
                        << "\n- success:      \t" << result.success
                        << "\n- segmentID:    \t" << result.segmentID
                        << "\n- errorMessage: \t" << result.errorMessage
                        ;

            providerID = armem::MemoryID::fromString(result.segmentID);
        }

        armem::MemoryWriter writer(memory);

        armem::MemoryID entityID = providerID;
        entityID.entityName = "example_entity";

        armem::MemoryID snapshotID;
        {
            armem::Commit commit;
            armem::EntityUpdate& update = commit.updates.emplace_back();

            update.entityID = entityID;
            update.timeCreated = armem::Time::now();
            update.instancesData = { new aron::data::AronData(), new aron::data::AronData() };

            ARMARX_IMPORTANT
                    << "Committing:"
                    << "\n- entityID:     \t'" << update.entityID.str() << "'"
                    ;
            armem::CommitResult commitResult = writer.commit(commit);

            ARMARX_CHECK_EQUAL(commitResult.results.size(), 1);
            armem::EntityUpdateResult& result = commitResult.results.at(0);

            ARMARX_INFO << result;

            snapshotID = result.snapshotID;
        }
        {
            armem::Commit commit;
            for (int i = 0; i < 3; ++i)
            {
                armem::EntityUpdate& update = commit.updates.emplace_back();
                update.entityID = entityID;
                update.timeCreated = armem::Time::now() + armem::Time::seconds(i);
                for (int j = 0; j < i; ++j)
                {
                    update.instancesData.push_back(new aron::data::AronData());
                }
            }
            ARMARX_INFO << "Commiting " << commit.updates.size() << " more updates.";

            armem::CommitResult commitResult = writer.commit(commit);
            ARMARX_CHECK_EQUAL(commitResult.results.size(), commit.updates.size());
        }

        {
            armem::data::EntitySnapshotQuery query;
            query.snapshotID = snapshotID.str();

            ARMARX_IMPORTANT
                    << "Querying snapshot:"
                    << "\n- snapshotID:     \t'" << query.snapshotID << "'"
                    ;

            armem::data::EntitySnapshotQueryResultList results = memory->getEntitySnapshots({query});

            ARMARX_CHECK_EQUAL(results.size(), 1);
            armem::data::EntitySnapshotQueryResult& result = results.at(0);

            ARMARX_INFO << "Result: "
                        << "\n- success:       \t" << result.success
                        << "\n- # instances:   \t" << result.instances.size()
                        << "\n- time created:  \t" << armem::Time::microSeconds(result.timeCreatedMicroSeconds).toDateTime()
                        << "\n- error message: \t" << result.errorMessage
                        ;
        }

        if (true)
        {
            armem::data::EntitySnapshotQuery query;
            snapshotID.clearTimestamp();
            query.snapshotID = snapshotID.str();

            ARMARX_IMPORTANT
                    << "Querying latest snapshot:"
                    << "\n- entityID:     \t'" << query.snapshotID << "'"
                    ;

            armem::data::EntitySnapshotQueryResultList results = memory->getEntitySnapshots({query});

            ARMARX_CHECK_EQUAL(results.size(), 1);
            armem::data::EntitySnapshotQueryResult& result = results.at(0);

            ARMARX_INFO << "Result: "
                        << "\n- success:       \t" << result.success
                        << "\n- # instances:   \t" << result.instances.size()
                        << "\n- time created:  \t" << armem::Time::microSeconds(result.timeCreatedMicroSeconds).toDateTime()
                        << "\n- error message: \t" << result.errorMessage
                        ;
        }
    }


    void ArMemExampleClient::onDisconnectComponent()
    {
    }


    void ArMemExampleClient::onExitComponent()
    {
    }


    void ArMemExampleClient::RemoteGui_update()
    {

    }

}
