/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemMemoryNameSystem
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArMemMemoryNameSystem.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/armem/error/ArMemError.h>


namespace armarx
{
    ArMemMemoryNameSystemPropertyDefinitions::ArMemMemoryNameSystemPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr ArMemMemoryNameSystem::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ArMemMemoryNameSystemPropertyDefinitions(getConfigIdentifier());

        return defs;
    }


    std::string ArMemMemoryNameSystem::getDefaultName() const
    {
        return "ArMemMemoryNameSystem";
    }



    void ArMemMemoryNameSystem::onInitComponent()
    {
    }


    void ArMemMemoryNameSystem::onConnectComponent()
    {
        RemoteGui__createTab();
        RemoteGui_startRunningTask();
    }


    void ArMemMemoryNameSystem::onDisconnectComponent()
    {
    }


    void ArMemMemoryNameSystem::onExitComponent()
    {
    }


    armem::data::RegisterMemoryResult ArMemMemoryNameSystem::registerMemory(
        const armem::data::RegisterMemoryInput& input, const Ice::Current& c)
    {
        armem::data::RegisterMemoryResult result = Plugin::registerMemory(input, c);
        tab.rebuild = true;
        return result;
    }


    armem::data::RemoveMemoryResult ArMemMemoryNameSystem::removeMemory(
        const armem::data::RemoveMemoryInput& input, const Ice::Current& c)
    {
        armem::data::RemoveMemoryResult result = Plugin::removeMemory(input, c);
        tab.rebuild = true;
        return result;
    }



    // REMOTE GUI

    void ArMemMemoryNameSystem::RemoteGui__createTab()
    {
        using namespace armarx::RemoteGui::Client;

        std::scoped_lock lock(mnsMutex);
        GridLayout grid = mns.RemoteGui_buildInfoGrid();

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void ArMemMemoryNameSystem::RemoteGui_update()
    {
        if (tab.rebuild.exchange(false))
        {
            RemoteGui__createTab();
        }
    }

}
