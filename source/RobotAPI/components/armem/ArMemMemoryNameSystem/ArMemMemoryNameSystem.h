/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemMemoryNameSystem
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <RobotAPI/interface/armem/MemoryNameSystemInterface.h>

#include <RobotAPI/libraries/armem/mns/MemoryNameSystemComponentPlugin.h>


namespace armarx
{
    /**
     * @class ArMemMemoryNameSystemPropertyDefinitions
     * @brief Property definitions of `ArMemMemoryNameSystem`.
     */
    class ArMemMemoryNameSystemPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ArMemMemoryNameSystemPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-ArMemMemoryNameSystem ArMemMemoryNameSystem
     * @ingroup RobotAPI-Components
     * A description of the component ArMemMemoryNameSystem.
     *
     * @class ArMemMemoryNameSystem
     * @ingroup Component-ArMemMemoryNameSystem
     * @brief Brief description of class ArMemMemoryNameSystem.
     *
     * Detailed description of class ArMemMemoryNameSystem.
     */
    class ArMemMemoryNameSystem :
        virtual public armarx::Component
        , virtual public armem::MemoryNameSystemComponentPluginUser
        , virtual public LightweightRemoteGuiComponentPluginUser
    //  , virtual public armarx::ArVizComponentPluginUser
    {
        using Plugin = MemoryNameSystemComponentPluginUser;
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // MemoryNameSystemInterface interface
    public:
        armem::data::RegisterMemoryResult registerMemory(const armem::data::RegisterMemoryInput& input, const Ice::Current&) override;
        armem::data::RemoveMemoryResult removeMemory(const armem::data::RemoveMemoryInput& input, const Ice::Current&) override;
        // Others are inherited from MemoryNameSystemComponentPluginUser


        // LightweightRemoteGuiComponentPluginUser interface
    public:

        void RemoteGui__createTab();
        void RemoteGui_update() override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        struct Properties
        {

        };
        Properties p;


        struct RemoteGuiTab : RemoteGui::Client::Tab
        {
            std::atomic_bool rebuild = false;
        };
        RemoteGuiTab tab;

    };
}
