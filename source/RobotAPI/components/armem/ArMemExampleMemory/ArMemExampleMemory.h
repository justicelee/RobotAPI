/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <RobotAPI/interface/armem/MemoryInterface.h>

#include <RobotAPI/libraries/armem/memory/Memory.h>
#include <RobotAPI/libraries/armem/component/MemoryComponentPlugin.h>


namespace armarx
{
    /**
     * @class ArMemExampleMemoryPropertyDefinitions
     * @brief Property definitions of `ArMemExampleMemory`.
     */
    class ArMemExampleMemoryPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ArMemExampleMemoryPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-ArMemExampleMemory ArMemExampleMemory
     * @ingroup RobotAPI-Components
     * A description of the component ArMemExampleMemory.
     *
     * @class ArMemExampleMemory
     * @ingroup Component-ArMemExampleMemory
     * @brief Brief description of class ArMemExampleMemory.
     *
     * Detailed description of class ArMemExampleMemory.
     */
    class ArMemExampleMemory :
        virtual public armarx::Component
        , virtual public armem::MemoryInterface
        , virtual public armem::MemoryComponentPluginUser
        , virtual public LightweightRemoteGuiComponentPluginUser
    // , virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // WritingInterface interface
    public:
        armem::data::AddSegmentsResult addSegments(const armem::data::AddSegmentsInput& input, const Ice::Current&) override;
        armem::data::CommitResult commit(const armem::data::Commit& commit, const Ice::Current&) override;


        // ReadingInterface interface
    public:
        using MemoryComponentPluginUser::getEntitySnapshots;


        // LightweightRemoteGuiComponentPluginUser interface
    public:

        void RemoteGui__createTab();
        void RemoteGui_update() override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;


        struct Properties
        {
            struct CoreSegments
            {
                std::vector<std::string> defaultCoreSegments = { "ExampleModality", "ExampleData" };
                std::string _defaultSegmentsStr;
                bool addOnUsage = false;
            };

            CoreSegments core;
        };
        Properties p;


        struct RemoteGuiTab : RemoteGui::Client::Tab
        {
            std::atomic_bool rebuild = false;

            RemoteGui::Client::GroupBox memoryGroup;
        };
        RemoteGuiTab tab;

    };
}
