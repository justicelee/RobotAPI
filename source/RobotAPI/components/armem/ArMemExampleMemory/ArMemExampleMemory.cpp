/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArMemExampleMemory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/armem/error/ArMemError.h>
#include <RobotAPI/libraries/armem/component/MemoryRemoteGui.h>


namespace armarx
{
    ArMemExampleMemoryPropertyDefinitions::ArMemExampleMemoryPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr ArMemExampleMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ArMemExampleMemoryPropertyDefinitions(getConfigIdentifier());

        defs->topic(debugObserver);

        memory.name = "Example";
        defs->optional(memory.name, "memory.Name", "Name of this memory.");

        p.core._defaultSegmentsStr = simox::alg::join(p.core.defaultCoreSegments, ", ");
        defs->optional(p.core._defaultSegmentsStr, "core.DefaultSegments", "Core segments to add on start up.");
        defs->optional(p.core.addOnUsage, "core.AddOnUsage",
                       "If enabled, core segments are added when required by a new provider segment.");

        return defs;
    }


    std::string ArMemExampleMemory::getDefaultName() const
    {
        return "ArMemExampleMemory";
    }



    void ArMemExampleMemory::onInitComponent()
    {
        ARMARX_CHECK_POSITIVE(memory.name.size());

        bool trim = true;
        p.core.defaultCoreSegments = simox::alg::split(p.core._defaultSegmentsStr, ",", trim);
        p.core._defaultSegmentsStr.clear();

        memory.addCoreSegments(p.core.defaultCoreSegments);
    }


    void ArMemExampleMemory::onConnectComponent()
    {
        RemoteGui__createTab();
        RemoteGui_startRunningTask();
    }


    void ArMemExampleMemory::onDisconnectComponent()
    {
    }


    void ArMemExampleMemory::onExitComponent()
    {
    }



    // WRITING

    armem::data::AddSegmentsResult ArMemExampleMemory::addSegments(const armem::data::AddSegmentsInput& input, const Ice::Current&)
    {
        armem::data::AddSegmentsResult result = MemoryComponentPluginUser::addSegments(input, p.core.addOnUsage);
        tab.rebuild = true;
        return result;
    }


    armem::data::CommitResult ArMemExampleMemory::commit(const armem::data::Commit& commit, const Ice::Current&)
    {
        armem::data::CommitResult result = MemoryComponentPluginUser::commit(commit);
        tab.rebuild = true;
        return result;
    }


    // READING

    // Inherited from Plugin



    // REMOTE GUI

    void ArMemExampleMemory::RemoteGui__createTab()
    {
        using namespace armarx::RemoteGui::Client;

        armem::MemoryRemoteGui mrg;
        {
            std::scoped_lock lock(memoryMutex);
            tab.memoryGroup = mrg.makeGroupBox(memory);
        }

        VBoxLayout root = {tab.memoryGroup, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void ArMemExampleMemory::RemoteGui_update()
    {
        if (tab.rebuild.exchange(false))
        {
            RemoteGui__createTab();
        }
    }

}
