#include <RobotAPI/components/ArViz/ArVizStorage.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

int main(int argc, char* argv[])
{
    return armarx::runSimpleComponentApp < armarx::ArVizStorage > (argc, argv, "ArVizStorage");
}
