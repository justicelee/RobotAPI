/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArVizExample
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArVizExample.h"



#include <Eigen/Eigen>

#include <SimoxUtility/color/cmaps.h>

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/components/ArViz/Client/Client.h>


namespace armarx
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(ArVizExample);

    void ArVizExample::onInitComponent()
    {
        offeringTopicFromProperty("ArVizTopicName");
    }

    void ArVizExample::onConnectComponent()
    {
        task = new RunningTask<ArVizExample>(this, &ArVizExample::update);
        task->start();
    }


    void ArVizExample::onDisconnectComponent()
    {
        task->stop();
        task = nullptr;
    }


    void fillTestLayer(viz::Layer& layer, double timeInSeconds)
    {
        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.x() = 100.0f * std::sin(timeInSeconds);

            viz::Box box = viz::Box("box")
                           .position(pos)
                           .color(viz::Color::red())
                           .size(Eigen::Vector3f(100.0f, 100.0f, 100.0f));

            bool toggleVisibility = (static_cast<int>(timeInSeconds) % 2 == 0);
            box.visible(toggleVisibility);

            layer.add(box);
        }
        {
            const float delta = 20. * std::sin(timeInSeconds);
            layer.add(viz::Ellipsoid{"ellipsoid"}
                      .position(Eigen::Vector3f{0, 100, 150})
                      .color(viz::Color::blue())
                      .axisLengths(Eigen::Vector3f{70.f + delta, 70.f - delta, 30.f}));
        }
        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.y() = 100.0f * std::sin(timeInSeconds);
            pos.x() = 150.0f;

            viz::Cylinder cyl = viz::Cylinder("cylinder")
                                .position(pos)
                                .color(viz::Color::green())
                                .radius(50.0f)
                                .height(100.0f);

            layer.add(cyl);
        }

        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.z() = 100.0f * std::sin(timeInSeconds);
            pos.x() = -150.0f;

            viz::Pose pose = viz::Pose("pose")
                             .position(pos)
                             .scale(1.0f);

            layer.add(pose);
        }
        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.z() = +300.0f;

            viz::Text text = viz::Text("text")
                             .text("Test Text")
                             .scale(4.0f)
                             .position(pos)
                             .color(viz::Color::black());

            layer.add(text);
        }
        {
            viz::Arrow arrow = viz::Arrow("arrow");

            float modTime = std::fmod(timeInSeconds, 2.0 * M_PI);
            arrow.length(200.0f + 100.0f * std::sin(modTime));

            Eigen::AngleAxisf dirRot(modTime, Eigen::Vector3f::UnitZ());
            Eigen::Vector3f direction = dirRot * Eigen::Vector3f::UnitX();
            arrow.direction(direction);


            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.z() = +300.0f;
            pos.x() = -500.0f;
            arrow.position(pos);
            arrow.color(viz::Color::blue());

            layer.add(arrow);
        }


    }


    void fillExampleLayer(viz::Layer& layer, double timeInSeconds)
    {
        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.z() = +300.0f;

            viz::ArrowCircle circle = viz::ArrowCircle("circle")
                                      .position(pos)
                                      .radius(100.0f)
                                      .width(10.0f)
                                      .color(viz::Color::fromRGBA(255, 0, 255));

            float modTime = std::fmod(timeInSeconds, 2.0 * M_PI);
            circle.completion(std::sin(modTime));

            layer.add(circle);
        }
        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.z() = +1000.0f;

            viz::Polygon poly = viz::Polygon("poly")
                                .position(pos)
                                .color(viz::Color::fromRGBA(0, 128, 255, 128))
                                .lineColor(viz::Color::fromRGBA(0, 0, 255))
                                .lineWidth(1.0f);

            float t = 1.0f + std::sin(timeInSeconds);
            float offset = 50.0f * t;
            poly.addPoint(Eigen::Vector3f{-200.0f - offset, -200.0f - offset, 0.0f});
            poly.addPoint(Eigen::Vector3f{-200.0f, +200.0f, 0.0f});
            poly.addPoint(Eigen::Vector3f{+200.0f + offset, +200.0f + offset, 0.0f});
            poly.addPoint(Eigen::Vector3f{+200.0f, -200.0f, 0.0f});

            layer.add(poly);
        }
        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.z() = +1500.0f;

            viz::Polygon poly = viz::Polygon("poly2")
                                .position(pos)
                                .color(viz::Color::fromRGBA(255, 128, 0, 128))
                                .lineWidth(0.0f);

            float t = 1.0f + std::sin(timeInSeconds);
            float offset = 20.0f * t;
            poly.addPoint(Eigen::Vector3f{-100.0f - offset, -100.0f - offset, 0.0f});
            poly.addPoint(Eigen::Vector3f{-100.0f, +100.0f, 0.0f});
            poly.addPoint(Eigen::Vector3f{+100.0f + offset, +100.0f + offset, 0.0f});
            poly.addPoint(Eigen::Vector3f{+100.0f, -100.0f, 0.0f});

            layer.add(poly);
        }
        {
            armarx::Vector3f vertices[] =
            {
                {-100.0f, -100.0f, 0.0f},
                {-100.0f, +100.0f, 0.0f},
                {+100.0f, +100.0f, 0.0f},
                {+100.0f, +100.0f, 200.0f},
            };
            std::size_t verticesSize = sizeof(vertices) / sizeof(vertices[0]);

            armarx::viz::data::Color colors[] =
            {
                {255, 255, 0, 0},
                {255, 0, 255, 0},
                {255, 0, 0, 255},
            };
            std::size_t colorsSize = sizeof(colors) / sizeof(colors[0]);

            viz::data::Face faces[] =
            {
                {
                    0, 1, 2,
                    0, 1, 2,
                },
                {
                    1, 2, 3,
                    0, 1, 2,
                },
            };
            std::size_t facesSize = sizeof(faces) / sizeof(faces[0]);

            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.z() = +1000.0f;
            pos.x() = -500.0f;

            viz::Mesh mesh = viz::Mesh("mesh")
                             .position(pos)
                             .vertices(vertices, verticesSize)
                             .colors(colors, colorsSize)
                             .faces(faces, facesSize);

            layer.add(mesh);
        }
        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.x() = 500.0f;

            viz::Robot robot = viz::Robot("robot")
                               .position(pos)
                               .file("Armar6RT", "Armar6RT/robotmodel/Armar6-SH/Armar6-SH.xml");

            // Full model
            if (true)
            {
                robot.useFullModel();
            }
            else
            {
                robot.useCollisionModel()
                .overrideColor(viz::Color::fromRGBA(0, 255, 128, 128));
            }

            float value = 0.5f * (1.0f + std::sin(timeInSeconds));
            robot.joint("ArmR2_Sho1", value);
            robot.joint("ArmR3_Sho2", value);

            layer.add(robot);
        }
    }


    void fillPermanentLayer(viz::Layer& layer)
    {
        viz::Box box = viz::Box("permBox")
                       .position(Eigen::Vector3f(2000.0f, 0.0f, 0.0f))
                       .size(Eigen::Vector3f(200.0f, 200.0f, 200.0f))
                       .color(viz::Color::fromRGBA(255, 165, 0));

        layer.add(box);
    }


    void fillPointsLayer(viz::Layer& layer, double timeInSeconds)
    {
        viz::PointCloud pc = viz::PointCloud("points")
                             .position(Eigen::Vector3f(2000.0f, 0.0f, 400.0f))
                             .transparency(0.0f);

        viz::ColoredPoint p;
        p.color = viz::Color::fromRGBA(255, 255, 0, 255);
        for (int x = -200; x <= 200; ++x)
        {
            p.x = 2.0f * x;
            double phase = timeInSeconds + x / 50.0;
            double heightT = std::max(0.0, std::min(0.5 * (1.0 + std::sin(phase)), 1.0));
            for (int y = -200; y <= 200; ++y)
            {
                p.y = 2.0f * y;
                p.z = 100.0 * heightT;

                p.color.g = 255.0 * heightT;
                p.color.b = 255.0 * (1.0 - heightT);
                pc.addPoint(p);
            }
        }

        layer.add(pc);
    }


    void fillObjectsLayer(viz::Layer& layer, double timeInSeconds)
    {
        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.x() = 100.0f * std::sin(timeInSeconds);
            pos.y() = 1000.0f;

            viz::Object sponge = viz::Object("sponge")
                                 .position(pos)
                                 .file("ArmarXObjects", "ArmarXObjects/SecondHands/Screwbox_big/Screwbox_big.xml");

            layer.add(sponge);
        }

        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            pos.x() = 300.0f + 100.0f * std::sin(timeInSeconds);
            pos.y() = 1000.0f;

            viz::Object spraybottle = viz::Object("spraybottle")
                                      .position(pos)
                                      .file("ArmarXObjects", "ArmarXObjects/SecondHands/Spraybottle/Spraybottle.xml");

            layer.add(spraybottle);
        }
    }


    void fillDisAppearingLayer(viz::Layer& layer, double timeInSeconds)
    {
        int time = int(timeInSeconds);

        const Eigen::Vector3f at = {-400, 0, 100};
        const Eigen::Vector3f dir = {-150, 0, 0};

        layer.add(viz::Box("box_always").position(at).size(100).color(simox::Color::azure()));
        layer.add(viz::Text("text_seconds").position(at + Eigen::Vector3f(0, 0, 100))
                  .orientation(Eigen::AngleAxisf(float(M_PI), Eigen::Vector3f::UnitZ())
                               * Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f::UnitZ(), - Eigen::Vector3f::UnitY()))
                  .text(std::to_string(time % 3)).scale(5).color(simox::Color::black()));

        switch (time % 3)
        {
            case 0:
                layer.add(viz::Sphere("sphere_0_1").position(at + 1.0 * dir).radius(50)
                          .color(simox::Color::purple()));
            // fallthrough
            case 1:
                layer.add(viz::Sphere("sphere_1").position(at + 2.0 * dir).radius(50)
                          .color(simox::Color::pink()));
                break;
            case 2:
                layer.add(viz::Cylinder("cylinder_2").position(at + 3.0 * dir).radius(50).height(100)
                          .color(simox::Color::turquoise()));
                break;
        }
    }


    void fillManyElementsLayer(viz::Layer& layer, double timeInSeconds)
    {
        const Eigen::Vector3f at = {-800, 0, 500};
        const float size = 5;
        const float dist = 10;

        const double period = 10;
        const float angle = float(2 * M_PI * std::fmod(timeInSeconds, period) / period);

        const int num = 10;
        float cf = 1.f / num;  // Color factor
        for (int x = 0; x < num; ++x)
        {
            for (int y = 0; y < num; ++y)
            {
                for (int z = 0; z < num; ++z)
                {
                    std::stringstream ss;
                    ss << "box_" << x << "_" << y << "_" << z;
                    layer.add(viz::Box(ss.str()).position(at + dist * Eigen::Vector3f(x, y, z))
                              .orientation(Eigen::AngleAxisf(angle, Eigen::Vector3f::UnitZ()).toRotationMatrix())
                              .size(size).color(simox::Color(cf * x, cf * y, cf * z)));
                }
            }
        }
    }

    void fillColorMapsLayer(viz::Layer& layer, double timeInSeconds)
    {
        (void) timeInSeconds;
        namespace E = Eigen;

        const E::Vector3f at(-500, -500, 1500);
        const E::Quaternionf ori = E::Quaternionf::FromTwoVectors(E::Vector3f::UnitZ(), - E::Vector3f::UnitY());
        const E::Vector2f size(200, 20);
        const E::Vector2i num(64, 2);

        int index = 0;
        for (const auto& pair : simox::color::cmaps::Named::all())
        {
            std::string const& name = pair.first;
            const simox::color::ColorMap& cmap = pair.second;

            Eigen::Vector3f shift(0, 0, - 1.5f * index * size.y());
            viz::Mesh mesh = viz::Mesh(name + "_mesh").position(at + shift).orientation(ori)
                             .grid2D(size, num, [&cmap](size_t, size_t, const E::Vector3f & p)
            {
                return viz::Color(cmap((p.x() + 100.f) / 200.f));
            });

            layer.add(mesh);

            layer.add(viz::Text(name + "_text").text(name)
                      .position(at + shift + E::Vector3f(2 + size.x() / 2, -2, 2 - size.y() / 2))
                      .orientation(ori).scale(1.5).color(simox::Color::black()));

            index++;
        }

    }


    void ArVizExample::update()
    {
        // This example uses the member `arviz` provided by armarx::ArVizComponentPluginUser.
        {
            // Alternatively, you can instantiate a new client in a component like this: */
            viz::Client arviz(*this);
            // The plugin also offers a helper function if you need to create new clients:
            arviz = createArVizClient();
        }

        /*
         * General Usage Scheme:
         * 1. Create a layer (using the client).
         * 2. Create elements and add them to the layer.
         * 3. Commit layers (using the client). This is the only network call.
         */

        viz::Layer testLayer = arviz.layer("Test");
        viz::Layer exampleLayer = arviz.layer("Example");
        viz::Layer pointsLayer = arviz.layer("Points");
        viz::Layer objectsLayer = arviz.layer("Objects");
        viz::Layer disAppearingLayer = arviz.layer("DisAppearing");


        // These layers are not updated in the loop.
        {
            viz::Layer permanentLayer = arviz.layer("Permanent");
            fillPermanentLayer(permanentLayer);
            arviz.commit(permanentLayer);
        }
        if (getProperty<bool>("layers.ManyElements"))
        {
            viz::Layer manyElementsLayer = arviz.layer("ManyElements");
            fillManyElementsLayer(manyElementsLayer, 0);
            arviz.commit(manyElementsLayer);
        }
        {
            viz::Layer colorMapsLayer = arviz.layer("ColorMaps");
            fillColorMapsLayer(colorMapsLayer, 0);
            arviz.commit(colorMapsLayer);
        }


        CycleUtil c(20);
        while (!task->isStopped())
        {
            double timeInSeconds = TimeUtil::GetTime().toSecondsDouble();

            testLayer.clear();
            fillTestLayer(testLayer, timeInSeconds);
            exampleLayer.clear();
            fillExampleLayer(exampleLayer, timeInSeconds);
            pointsLayer.clear();
            fillPointsLayer(pointsLayer, timeInSeconds);
            objectsLayer.clear();
            fillObjectsLayer(objectsLayer, timeInSeconds);
            disAppearingLayer.clear();
            fillDisAppearingLayer(disAppearingLayer, timeInSeconds);

            arviz.commit({testLayer, exampleLayer, pointsLayer, objectsLayer, disAppearingLayer});

            c.waitForCycleDuration();
        }
    }

    void ArVizExample::onExitComponent()
    {

    }

    armarx::PropertyDefinitionsPtr ArVizExample::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new ArVizExamplePropertyDefinitions(
                getConfigIdentifier()));
    }
}

