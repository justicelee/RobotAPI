armarx_component_set_name("ArVizExample")

set(COMPONENT_LIBS
    ArmarXCore
    DecoupledSingleComponent
    ArViz
    RobotAPIComponentPlugins  # For ArVizComponentPluginUser
)

set(SOURCES
    ArVizExample.cpp
    main.cpp
)
set(HEADERS
    ArVizExample.h
)

armarx_add_component_executable("${SOURCES}" "${HEADERS}")
