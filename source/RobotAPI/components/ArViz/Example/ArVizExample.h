/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArVizExample
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/ArViz.h>

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>


namespace armarx
{
    /**
     * @class ArVizExamplePropertyDefinitions
     */
    class ArVizExamplePropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ArVizExamplePropertyDefinitions(std::string prefix) :
            armarx::ComponentPropertyDefinitions(prefix)
        {
            // In this example, this is automatically done by deriving the component
            // from armarx::ArVizComponentPluginUser.
            if (false)
            {
                defineOptionalProperty<std::string>("ArVizTopicName", "ArVizTopic", "Layer updates are sent over this topic.");
            }


            defineOptionalProperty<bool>("layers.ManyElements", false,
                                         "Show a layer with a lot of elements (used for testing, may impact performance).");
        }
    };

    /**
     * @defgroup Component-ArVizExample ArVizExample
     * @ingroup RobotAPI-Components
     * A description of the component ArVizExample.
     *
     * @class ArVizExample
     * @ingroup Component-ArVizExample
     * @brief Brief description of class ArVizExample.
     *
     * Detailed description of class ArVizExample.
     */
    class ArVizExample :
        virtual public armarx::Component,
    // Deriving from armarx::ArVizComponentPluginUser adds necessary properties
    // and provides a ready-to-use ArViz client.
        virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override
        {
            return "ArVizExample";
        }

        void onInitComponent() override;
        void onConnectComponent() override;

        void onDisconnectComponent() override;
        void onExitComponent() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        void update();

    private:
        RunningTask<ArVizExample>::pointer_type task;
    };
}
