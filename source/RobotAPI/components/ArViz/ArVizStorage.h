/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArViz
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "Coin/Visualizer.h"

#include <RobotAPI/interface/ArViz.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <mutex>
#include <atomic>
#include <condition_variable>
#include <filesystem>


namespace armarx
{
    /**
     * @class ArVizPropertyDefinitions
     * @brief
     */
    class ArVizPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ArVizPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("TopicName", "ArVizTopic", "Layer updates are sent over this topic.");
            defineOptionalProperty<int>("MaxHistorySize", 1000, "How many layer updates are saved in the history until they are compressed")
            .setMin(0);
            defineOptionalProperty<std::string>("HistoryPath", "RobotAPI/ArVizStorage", "Destination path where the history are serialized to");
        }
    };

    /**
     * @defgroup Component-ArViz ArViz
     * @ingroup RobotAPI-Components
     * A description of the component ArViz.
     *
     * @class ArViz
     * @ingroup Component-ArViz
     * @brief Brief description of class ArViz.
     *
     * Detailed description of class ArViz.
     */
    class ArVizStorage
        : virtual public armarx::Component
        , virtual public armarx::viz::StorageAndTopicInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ArVizStorage";
        }

        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // Topic interface
        void updateLayers(viz::data::LayerUpdateSeq const& updates, const Ice::Current&) override;

        // StorageInterface interface
        viz::data::LayerUpdates pullUpdatesSince(Ice::Long revision, const Ice::Current&) override;
        std::string startRecording(std::string const& prefix, const Ice::Current&) override;
        void stopRecording(const Ice::Current&) override;
        viz::data::RecordingSeq getAllRecordings(const Ice::Current&) override;
        viz::data::RecordingBatch getRecordingBatch(const std::string&, Ice::Long, const Ice::Current&) override;

    private:
        void record();

        void recordBatch(viz::data::RecordingBatch& batch);

    private:
        std::string topicName;
        int maxHistorySize = 100;
        std::filesystem::path historyPath;

        std::mutex historyMutex;

        std::vector<viz::data::TimestampedLayerUpdate> currentState;
        // We ignore the history for now
        std::vector<viz::data::TimestampedLayerUpdate> history;
        long revision = 0;

        std::mutex recordingMutex;
        std::filesystem::path recordingPath;
        std::vector<viz::data::TimestampedLayerUpdate> recordingInitialState;
        std::vector<viz::data::RecordingBatch> recordingBuffer;
        viz::data::Recording recordingMetaData;
        std::condition_variable recordingCondition;
        RunningTask<ArVizStorage>::pointer_type recordingTask;
    };
}
