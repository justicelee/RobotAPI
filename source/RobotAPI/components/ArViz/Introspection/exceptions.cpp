#include "exceptions.h"

#include <sstream>

#include <SimoxUtility/algorithm/string.h>


namespace armarx::viz::error
{

    ArvizReflectionError::ArvizReflectionError(const std::string& msg) : std::runtime_error(msg)
    {}

    NoTypeNameEntryInJsonObject::NoTypeNameEntryInJsonObject(
        const std::string& missingKey, const nlohmann::json& j) :
        ArvizReflectionError(makeMsg(missingKey, j))
    {
    }

    static std::string getAvailableKeys(const nlohmann::json& j)
    {
        std::stringstream ss;
        for (const auto& item : j.items())
        {
            ss << "\n- " << item.key();
        }
        return ss.str();
    }

    std::string NoTypeNameEntryInJsonObject::makeMsg(const std::string& missingKey, const nlohmann::json& j)
    {
        std::stringstream ss;
        ss << "No type name entry with key '" << missingKey << "' in JSON object.\n";
        if (j.is_object())
        {
            ss << "Available keys: " << getAvailableKeys(j);
        }
        else
        {
            ss << "JSON document is not an object, but a " << j.type_name() << ".";
        }
        return ss.str();
    }


    TypeNameEntryAlreadyInJsonObject::TypeNameEntryAlreadyInJsonObject(
        const std::string& key, const std::string& typeName, const nlohmann::json& j) :
        ArvizReflectionError(makeMsg(key, typeName, j))
    {
    }

    std::string TypeNameEntryAlreadyInJsonObject::makeMsg(
        const std::string& key, const std::string& typeName, const nlohmann::json& j)
    {
        std::stringstream ss;
        ss << "Key '" << key << "' already used in JSON object "
           << "when trying to store the type name '" << typeName << "'.\n";
        ss << "Used keys:" << getAvailableKeys(j);
        return ss.str();
    }


    TypeNameMismatch::TypeNameMismatch(const std::string& typeInJson, const std::string& typeOfObject) :
        ArvizReflectionError(makeMsg(typeInJson, typeOfObject))
    {
    }

    std::string TypeNameMismatch::makeMsg(const std::string& typeInJson, const std::string& typeOfObject)
    {
        std::stringstream ss;
        ss << "Type stored JSON (" << typeInJson << ") does not match the type of passed object ("
           << typeOfObject << ").";
        return ss.str();
    }


    NoSerializerForType::NoSerializerForType(const std::string& typeName) :
        ArvizReflectionError("No registered serializer for type '" + typeName + "'.")
    {}

    SerializerAlreadyRegisteredForType::SerializerAlreadyRegisteredForType(
        const std::string& typeName, const std::vector<std::string>& acceptedTypes) :
        ArvizReflectionError(makeMsg(typeName, acceptedTypes))
    {}

    std::string SerializerAlreadyRegisteredForType::makeMsg(
        const std::string& typeName, const std::vector<std::string>& acceptedTypes)
    {
        std::stringstream ss;
        ss << "There is already a registered serializer for type '" + typeName + "'.";
        if (!acceptedTypes.empty())
        {
            ss << "\nAccepted types:\n" + simox::alg::join(acceptedTypes, "\n- ");
        }
        return ss.str();
    }

}

