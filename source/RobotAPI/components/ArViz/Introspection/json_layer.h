#pragma once

#include <SimoxUtility/json.h>

#include <RobotAPI/interface/ArViz/Component.h>


namespace armarx::viz::data
{

    void to_json(nlohmann::json& j, const LayerUpdate& update);
    void from_json(const nlohmann::json& j, LayerUpdate& update);

    void to_json(nlohmann::json& j, const LayerUpdates& update);
    void from_json(const nlohmann::json& j, LayerUpdates& update);

}
