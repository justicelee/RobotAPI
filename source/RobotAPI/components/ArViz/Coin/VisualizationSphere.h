#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>
#include <Inventor/nodes/SoSphere.h>

namespace armarx::viz::coin
{
    struct VisualizationSphere : TypedElementVisualization<SoSphere>
    {
        using ElementType = data::ElementSphere;

        bool update(ElementType const& element)
        {
            node->radius = element.radius;

            return true;
        }
    };
}
