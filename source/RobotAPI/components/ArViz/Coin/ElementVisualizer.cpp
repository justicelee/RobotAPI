#include <SimoxUtility/EigenStdVector.h>

#include "ElementVisualizer.h"

#include <RobotAPI/components/ArViz/IceConversions.h>

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>

namespace armarx::viz::coin
{
    ElementVisualization::ElementVisualization()
    {
        // We do everything in millimeters...
        units = new SoUnits();
        units->units = SoUnits::MILLIMETERS;

        transform = new SoTransform;
        material = new SoMaterial;

        separator = new SoSeparator;
        separator->addChild(units);
        separator->addChild(transform);
        separator->addChild(material);
    }

    void ElementVisualization::updateBase(data::Element const& element)
    {
        auto& p = element.pose;
        transform->translation.setValue(p.x, p.y, p.z);
        transform->rotation.setValue(p.qx, p.qy, p.qz, p.qw);
        transform->scaleFactor.setValue(element.scale, element.scale, element.scale);

        auto color = element.color;
        const float conv = 1.0f / 255.0f;
        SbColor coinColor(conv * color.r, conv * color.g, conv * color.b);
        material->ambientColor.setValue(coinColor);
        material->diffuseColor.setValue(coinColor);
        material->transparency.setValue(1.0f - conv * color.a);

        // This enables textured meshes to be displayed with transparency
        bool overrideMaterial = (element.flags & data::ElementFlags::OVERRIDE_MATERIAL);
        material->setOverride(overrideMaterial);
    }

    std::unique_ptr<ElementVisualization> ElementVisualizer::create(const data::Element& element)
    {
        std::unique_ptr<ElementVisualization> result(createDerived());
        update(element, result.get());

        return result;
    }

    bool ElementVisualizer::update(data::Element const& element, ElementVisualization* visu)
    {
        visu->updateBase(element);
        return updateDerived(element, visu);
    }
}

