#include <regex>
#include <fstream>

#include "VisualizationRobot.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>


namespace armarx::viz::coin
{
    namespace
    {
        VirtualRobot::RobotPtr loadRobot(std::string const& project, std::string const& filename)
        {
            VirtualRobot::RobotPtr  result;

            if (filename.empty())
            {
                ARMARX_INFO << deactivateSpam() << "No filename provided for robot.";
                return result;
            }

            std::string fullFilename;
            // Suppress warnings when full path is specified (is discouraged, but may happen).
            if (project.empty() && std::filesystem::path(filename).is_absolute())
            {
                ARMARX_INFO << deactivateSpam()
                            << "You specified the absolute path to the robot file:"
                            << "\n\t'" << filename << "'"
                            << "\nConsider specifying the containing ArmarX package and relative data path instead to "
                            << "improve portability to other systems.";
            }
            // We need to always check that the file is readable otherwise, VirtualRobot::RobotIO::loadRobot crashes
            ArmarXDataPath::FindPackageAndAddDataPath(project);
            if (!ArmarXDataPath::SearchReadableFile(filename, fullFilename))
            {
                ARMARX_INFO << deactivateSpam()
                            << "Unable to find readable file for name: " << filename;
                return result;
            }

            try
            {
                // Always load full model. Visualization can choose between full and collision model
                VirtualRobot::RobotIO::RobotDescription loadMode = VirtualRobot::RobotIO::eFull;
                // This check is always true since it does not compare the flag to anything ==> if (true)
                // Deactivated for now
                //                if (data::ModelDrawStyle::COLLISION)
                //                {
                //                    loadMode = VirtualRobot::RobotIO::eCollisionModel;
                //                }
                ARMARX_INFO << "Loading robot from " << fullFilename;
                result = VirtualRobot::RobotIO::loadRobot(fullFilename, loadMode);
                if (result)
                {
                    result->setThreadsafe(false);
                    // Do we want to propagate joint values? Probably not...
                    // Closing the hand on the real robot could be implemented on another level
                    result->setPropagatingJointValuesEnabled(false);
                }
                else
                {
                    ARMARX_WARNING << "Could not load robot from file: " << fullFilename
                                   << "\nReason: loadRobot() returned nullptr";
                }
            }
            catch (std::exception const& ex)
            {
                ARMARX_WARNING << "Could not load robot from file: " << fullFilename
                               << "\nReason: " << ex.what();
            }

            return result;
        }

        static std::vector<LoadedRobot> robotcache;

        LoadedRobot getRobotFromCache(std::string const& project, std::string const& filename)
        {
            // We can use a global variable, since this code is only executed in the GUI thread

            LoadedRobot result;

            for (LoadedRobot const& loaded : robotcache)
            {
                if (loaded.project == project && loaded.filename == filename)
                {
                    ARMARX_DEBUG << "loading robot from chace " << VAROUT(project) << ", " << VAROUT(filename);
                    result = loaded;
                    //do not scale the robot and do not clone meshes if scaling = 1
                    result.robot = loaded.robot->clone(nullptr, 1, true);
                    return result;
                }
            }

            ARMARX_DEBUG << "loading robot from file  " << VAROUT(project) << ", " << VAROUT(filename);
            result.project = project;
            result.filename = filename;
            result.robot = loadRobot(project, filename);

            robotcache.push_back(result);

            return result;
        }
    }

    bool VisualizationRobot::update(ElementType const& element)
    {
        IceUtil::Time time_start = IceUtil::Time::now();
        (void) time_start;

        bool robotChanged = loaded.project != element.project || loaded.filename != element.filename;
        if (robotChanged)
        {
            // The robot file changed, so reload the robot
            loaded = getRobotFromCache(element.project, element.filename);
        }
        if (!loaded.robot)
        {
            ARMARX_WARNING << deactivateSpam(10)
                           << "Robot will not visualized since it could not be loaded."
                           << "\nID: " << element.id
                           << "\nProject: " << element.project
                           << "\nFilename: " << element.filename;
            return true;
        }
        //IceUtil::Time time_load = IceUtil::Time::now();

        bool drawStyleChanged = loadedDrawStyle != element.drawStyle;
        if (robotChanged || drawStyleChanged)
        {
            recreateVisualizationNodes(element.drawStyle);
            loadedDrawStyle = element.drawStyle;
        }
        //IceUtil::Time time_style = IceUtil::Time::now();

        // Set pose, configuration and so on

        auto& robot = *loaded.robot;

        // We do not need to update the global pose in the robot
        // Since we only visualize the results, we can simply use the
        // pose from the Element base class.

        // Eigen::Matrix4f pose = defrost(element.pose);
        // robot.setGlobalPose(pose, false);

        robot.setJointValues(element.jointValues);
        //IceUtil::Time time_set = IceUtil::Time::now();

        if (loadedDrawStyle & data::ModelDrawStyle::OVERRIDE_COLOR)
        {
            if (loadedColor.r != element.color.r
                || loadedColor.g != element.color.g
                || loadedColor.b != element.color.b
                || loadedColor.a != element.color.a)
            {
                int numChildren = node->getNumChildren();
                for (int i = 0; i < numChildren; i++)
                {
                    SoSeparator* nodeSep = static_cast<SoSeparator*>(node->getChild(i));
                    // The first entry must be a SoMaterial (see recreateVisualizationNodes)
                    SoMaterial* m = dynamic_cast<SoMaterial*>(nodeSep->getChild(0));
                    if (!m)
                    {
                        ARMARX_WARNING << "Error at node with index: " << i;
                        continue;
                    }

                    auto color = element.color;
                    const float conv = 1.0f / 255.0f;
                    float a = color.a * conv;
                    SbColor coinColor(conv * color.r, conv * color.g, conv * color.b);
                    m->diffuseColor = coinColor;
                    m->ambientColor = coinColor;
                    m->transparency = 1.0f - a;
                    m->setOverride(true);
                }
                loadedColor = element.color;
            }
        }
        //IceUtil::Time time_color = IceUtil::Time::now();

        // Setting the joint angles takes > 0.5 ms! This is unexpected...
        //        ARMARX_INFO << "Total:   " << (time_color - time_start).toMilliSecondsDouble()
        //                    << "\nLoad:  " << (time_load - time_start).toMilliSecondsDouble()
        //                    << "\nStyle: " << (time_style - time_load).toMilliSecondsDouble()
        //                    << "\nSet:   " << (time_set - time_style).toMilliSecondsDouble()
        //                    << "\nColor: " << (time_color - time_set).toMilliSecondsDouble();

        return true;
    }

    void VisualizationRobot::recreateVisualizationNodes(int drawStyle)
    {
        VirtualRobot::SceneObject::VisualizationType visuType = VirtualRobot::SceneObject::Full;
        if (drawStyle & data::ModelDrawStyle::COLLISION)
        {
            visuType = VirtualRobot::SceneObject::Collision;
        }

        node->removeAllChildren();

        auto& robot = *loaded.robot;
        for (size_t i = 0; i < robot.getRobotNodes().size(); ++i)
        {
            VirtualRobot::RobotNodePtr robNode = robot.getRobotNodes()[i];
            SoSeparator* nodeSep = new SoSeparator;

            // This material is used to color the nodes individually
            // We require it to be the first node in the separator for updates
            SoMaterial* nodeMat = new SoMaterial;
            nodeMat->setOverride(false);
            nodeSep->addChild(nodeMat);

            auto robNodeVisu = robNode->getVisualization<VirtualRobot::CoinVisualization>(visuType);
            if (robNodeVisu)
            {
                SoNode* sepRobNode = robNodeVisu->getCoinVisualization();

                if (sepRobNode)
                {
                    nodeSep->addChild(sepRobNode);
                }
            }

            node->addChild(nodeSep);
        }
    }

    void clearRobotCache()
    {
        robotcache.clear();
    }
}
