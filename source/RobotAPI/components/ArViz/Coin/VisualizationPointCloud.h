#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoPointSet.h>

namespace armarx::viz::coin
{
    struct VisualizationPointCloud : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementPointCloud;

        VisualizationPointCloud()
        {
            pclMat = new SoMaterial;

            SoMaterialBinding* pclMatBind = new SoMaterialBinding;
            pclMatBind->value = SoMaterialBinding::PER_PART;

            pclCoords = new SoCoordinate3;
            pclStye = new SoDrawStyle;

            node->addChild(pclMat);
            node->addChild(pclMatBind);
            node->addChild(pclCoords);
            node->addChild(pclStye);
            node->addChild(new SoPointSet);
        }

        bool update(ElementType const& element)
        {
            auto& pcl = element.points;

            colors.clear();
            colors.reserve(pcl.size());
            coords.clear();
            coords.reserve(pcl.size());

            const float conv = 1.0f / 255.0f;
            for (auto& point : pcl)
            {
                float r = point.color.r * conv;
                float g = point.color.g * conv;
                float b = point.color.b * conv;
                colors.emplace_back(r, g, b);
                coords.emplace_back(point.x, point.y, point.z);
            }
            pclMat->diffuseColor.setValues(0, colors.size(), colors.data());
            pclMat->ambientColor.setValues(0, colors.size(), colors.data());
            pclMat->transparency = element.transparency;

            pclCoords->point.setValues(0, coords.size(), coords.data());
            pclCoords->point.setNum(coords.size());

            pclStye->pointSize = element.pointSizeInPixels;

            return true;
        }

        SoMaterial* pclMat;
        SoCoordinate3* pclCoords;
        SoDrawStyle* pclStye;

        std::vector<SbColor> colors;
        std::vector<SbVec3f> coords;
    };
}
