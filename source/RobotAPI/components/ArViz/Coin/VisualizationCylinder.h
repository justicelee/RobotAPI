#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>
#include <Inventor/nodes/SoCylinder.h>

namespace armarx::viz::coin
{
    struct VisualizationCylinder : TypedElementVisualization<SoCylinder>
    {
        using ElementType = data::ElementCylinder;

        bool update(ElementType const& element)
        {
            node->radius = element.radius;
            node->height = element.height;

            return true;
        }
    };
}
