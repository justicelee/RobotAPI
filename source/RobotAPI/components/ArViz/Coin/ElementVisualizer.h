#pragma once

#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoUnits.h>
#include <Inventor/nodes/SoTransform.h>

#include <memory>

#include <RobotAPI/interface/ArViz/Elements.h>

namespace armarx::viz::data
{
    class Element;
}

namespace armarx::viz::coin
{
    struct ElementVisualization
    {
        ElementVisualization();

        virtual ~ElementVisualization() = default;

        void updateBase(data::Element const& element);

        SoSeparator* separator;
        SoUnits* units;
        SoTransform* transform;
        SoMaterial* material;
        bool wasUpdated = true;
        bool visible = true;
    };

    class ElementVisualizer
    {
    public:
        virtual ~ElementVisualizer() = default;

        std::unique_ptr<ElementVisualization> create(data::Element const& element);
        bool update(data::Element const& element, ElementVisualization* visu);

        virtual ElementVisualization* createDerived() = 0;
        virtual bool updateDerived(data::Element const& element, ElementVisualization* data) = 0;
    };


    template <typename NodeT>
    struct TypedElementVisualization : ElementVisualization
    {
        using NodeType = NodeT;

        TypedElementVisualization()
        {
            node = new NodeType;
            node->ref();
        }

        ~TypedElementVisualization()
        {
            node->unref();
        }

        NodeType* node;
    };

    template <typename DataT>
    class TypedElementVisualizer : public ElementVisualizer
    {
    public:
        using DataType = DataT;
        using ElementType = typename DataType::ElementType;


        DataType* createDerived() final
        {
            DataType* result = new DataType;
            result->separator->addChild(result->node);
            return result;
        }

        bool updateDerived(data::Element const& element_, ElementVisualization* data_) final
        {
            auto const& element = static_cast<ElementType const&>(element_);
            auto* data = dynamic_cast<DataType*>(data_);

            if (data)
            {
                bool hidden = (element.flags & armarx::viz::data::ElementFlags::HIDDEN);
                if (hidden)
                {
                    if (data->visible)
                    {
                        data->separator->removeChild(data->node);
                        data->visible = false;
                    }
                }
                else if (!data->visible)
                {
                    data->visible = true;
                    data->separator->addChild(data->node);
                }

                // We want to call  update with the correctly downcasted arguments
                return data->update(element);
            }
            return false;
        }
    };
}
