#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationNode.h>

namespace armarx::viz::coin
{
    struct VisualizationArrowCircle : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementArrowCircle;

        bool update(ElementType const& element)
        {
            int rings = 32;

            float completion = std::min<float>(1.0f, std::max(-1.0f, element.completion));
            int sign = completion >= 0 ? 1 : -1;
            float torusCompletion = completion - 1.0f / rings * sign;
            if (torusCompletion * sign < 0)
            {
                torusCompletion = 0;
            }
            auto color = element.color;
            const float conv = 1.0f / 255.0f;
            float r = color.r * conv;
            float g = color.g * conv;
            float b = color.b * conv;
            float a = color.a * conv;

            auto torusNode = VirtualRobot::CoinVisualizationFactory().createTorus(
                                 element.radius, element.width, torusCompletion,
                                 r, g, b, 1.0f - a,
                                 8, rings);
            SoNode* torus = dynamic_cast<VirtualRobot::CoinVisualizationNode&>(*torusNode).getCoinVisualization();


            float angle0 = (float)(rings - 2) / rings * 2 * M_PI * completion;
            float x0 = element.radius * cos(angle0);
            float y0 = element.radius * sin(angle0);
            float angle1 = (float)(rings - 1) / rings * 2 * M_PI * completion;

            SoSeparator* subSep = new SoSeparator();
            SoTransform* tr = new SoTransform;
            tr->translation.setValue(x0, y0, 0);

            tr->rotation.setValue(SbVec3f(0, 0, 1), angle1);
            subSep->addChild(tr);

            subSep->addChild(VirtualRobot::CoinVisualizationFactory::CreateArrow(
                                 Eigen::Vector3f::UnitY()*sign, 0, element.width,
                                 VirtualRobot::VisualizationFactory::Color(r, g, b, 1.0f - a)));

            node->removeAllChildren();
            node->addChild(torus);
            node->addChild(subSep);

            return true;
        }
    };
}
