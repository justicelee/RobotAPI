#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>
#include <Inventor/nodes/SoCube.h>

namespace armarx::viz::coin
{
    struct VisualizationBox : TypedElementVisualization<SoCube>
    {
        using ElementType = data::ElementBox;

        bool update(ElementType const& element)
        {
            node->width = element.size.e0;
            node->height = element.size.e1;
            node->depth = element.size.e2;

            return true;
        }
    };
}
