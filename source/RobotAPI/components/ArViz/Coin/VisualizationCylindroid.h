#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationNode.h>

namespace armarx::viz::coin
{
    struct VisualizationCylindroid : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementCylindroid;

        bool update(ElementType const& element)
        {
            auto color = element.color;
            constexpr float conv = 1.0f / 255.0f;
            const float r = color.r * conv;
            const float g = color.g * conv;
            const float b = color.b * conv;
            const float a = color.a * conv;

            VirtualRobot::VisualizationNodePtr cylindroid_node;
            {
                // Params.
                SoMaterial* mat = new SoMaterial;
                mat->diffuseColor.setValue(r, g, b);
                mat->ambientColor.setValue(r, g, b);
                mat->transparency.setValue(1. - a);

                SoSeparator* res = new SoSeparator();
                res->ref();
                SoUnits* u = new SoUnits();
                u->units = SoUnits::MILLIMETERS;
                res->addChild(u);
                res->addChild(VirtualRobot::CoinVisualizationFactory::CreateCylindroid(
                                  element.axisLengths.e0, element.axisLengths.e1, element.height,
                                  mat));

                cylindroid_node.reset(new VirtualRobot::CoinVisualizationNode(res));
                res->unref();
            }

            SoNode* cylindroid = dynamic_cast<VirtualRobot::CoinVisualizationNode&>(
                                     *cylindroid_node).getCoinVisualization();

            node->removeAllChildren();
            node->addChild(cylindroid);

            return true;
        }
    };
}
