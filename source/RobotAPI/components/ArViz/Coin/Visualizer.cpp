#include "Visualizer.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/util/CPPUtility/GetTypeString.h>

#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/nodes/SoUnits.h>
#include <thread>

#include "VisualizationRobot.h"

namespace armarx::viz
{
    struct CoinVisualizerWrapper : IceUtil::Shared
    {
        class CoinVisualizer* this_;

        void onUpdateSuccess(data::LayerUpdates const& updates)
        {
            this_->onUpdateSuccess(updates);
        }

        void onUpdateFailure(Ice::Exception const& ex)
        {
            this_->onUpdateFailure(ex);
        }
    };

    static const char* toString(CoinVisualizerState state)
    {
        switch (state)
        {
            case CoinVisualizerState::STOPPED:
                return "STOPPED";
            case CoinVisualizerState::STARTING:
                return "STARTING";
            case CoinVisualizerState::RUNNING:
                return "RUNNING";
            case CoinVisualizerState::STOPPING:
                return "STOPPING";
        }
        return "UNKNOWN";
    }

    static void updateVisualizationCB(void* data, SoSensor* sensor)
    {
        auto* visu = static_cast<CoinVisualizer*>(data);
        visu->update();
    }

    struct TimedBlock
    {
        TimedBlock(const char* function)
            : start(IceUtil::Time::now())
            , function(function)
        {
        }

        ~TimedBlock()
        {
            IceUtil::Time diff = IceUtil::Time::now() - start;
            // Horrible: We need to plot this actually
            ARMARX_INFO << "Time '" << function << "': "
                        << diff.toMilliSecondsDouble() << " ms";
        }

    private:
        IceUtil::Time start;
        const char* function;
    };

    CoinVisualizer::CoinVisualizer()
    {
        registerVisualizationTypes();

        callbackData = new CoinVisualizerWrapper;
        callbackData->this_ = this;
        callback = newCallback_StorageInterface_pullUpdatesSince(callbackData,
                   &CoinVisualizerWrapper::onUpdateSuccess,
                   &CoinVisualizerWrapper::onUpdateFailure);
        root = new SoSeparator;

        timerSensor = new SoTimerSensor(updateVisualizationCB, this);

        float cycleTimeMS = 33.0f;
        timerSensor->setInterval(SbTime(cycleTimeMS / 1000.0f));
    }

    CoinVisualizer::~CoinVisualizer()
    {
        // We need to clear the caches while Coin is still initialized
        coin::clearRobotCache();
    }


    void CoinVisualizer::startAsync(StorageInterfacePrx const& storage)
    {
        std::unique_lock<std::mutex> lock(stateMutex);
        if (state != CoinVisualizerState::STOPPED)
        {
            ARMARX_WARNING << "Unexpected state of visualizer\n"
                           << "Expected: STOPPED\n"
                           << "But got: " << toString(state);
            return;
        }
        state = CoinVisualizerState::STARTING;
        stateStorage = storage;

        SoSensorManager* sensor_mgr = SoDB::getSensorManager();
        sensor_mgr->insertTimerSensor(timerSensor);
    }

    void CoinVisualizer::stop()
    {
        if (state == CoinVisualizerState::STOPPED)
        {
            return;
        }

        SoSensorManager* sensor_mgr = SoDB::getSensorManager();
        state = CoinVisualizerState::STOPPING;
        while (state != CoinVisualizerState::STOPPED)
        {
            sensor_mgr->processTimerQueue();
            usleep(1000);
        }
        sensor_mgr->removeTimerSensor(timerSensor);

    }

    CoinVisualizer_ApplyTiming CoinVisualizer::apply(data::LayerUpdate const& update)
    {
        IceUtil::Time time_start = IceUtil::Time::now();

        CoinVisualizer_ApplyTiming timing;
        timing.layerName = update.name;

        auto layerID = std::make_pair(update.component, update.name);
        auto layerIt = layers.find(layerID);

        if (layerIt == layers.end())
        {
            // Create a new layer
            SoSeparator* coinNode = new SoSeparator;
            coinNode->ref();
            root->addChild(coinNode);

            layerIt = layers.emplace(layerID, CoinLayer(coinNode)).first;
        }

        IceUtil::Time time_addLayer = IceUtil::Time::now();
        timing.addLayer = time_addLayer - time_start;

        // Add or update the elements in the update
        CoinLayer& layer = layerIt->second;
        for (auto& updatedElementPtr : update.elements)
        {
            if (!updatedElementPtr)
            {
                ARMARX_WARNING << deactivateSpam(10) << "Element is null in layer "
                               << update.component << "/" << update.name;
                continue;
            }
            data::Element const& updatedElement = *updatedElementPtr;

            std::type_index elementType = typeid(updatedElement);
            size_t visuIndex;
            size_t visuSize = elementVisualizersTypes.size();
            for (visuIndex = 0; visuIndex < visuSize; ++visuIndex)
            {
                if (elementVisualizersTypes[visuIndex] == elementType)
                {
                    break;
                }
            }
            if (visuIndex >= visuSize)
            {
                ARMARX_WARNING << deactivateSpam(1)
                               << "No visualizer for element type found: "
                               << armarx::GetTypeString(elementType);
                continue;
            }
            coin::ElementVisualizer* visualizer = elementVisualizers[visuIndex].get();

            auto oldElementIter = layer.elements.find(updatedElement.id);
            if (oldElementIter != layer.elements.end())
            {
                // Element already exists
                coin::ElementVisualization& oldElement = *oldElementIter->second.visu;
                oldElement.wasUpdated = true;

                bool updated = visualizer->update(updatedElement, &oldElement);

                if (updated)
                {
                    layer.elements[updatedElement.id].data = updatedElementPtr;
                    continue;
                }
                else
                {
                    // Types are different, so we delete the old element and create a new one
                    layer.node->removeChild(oldElement.separator);
                }
            }

            auto elementVisu = visualizer->create(updatedElement);
            if (elementVisu->separator)
            {
                layer.node->addChild(elementVisu->separator);
                layer.elements[updatedElement.id] = CoinLayerElement{updatedElementPtr, std::move(elementVisu)};
            }
            else
            {
                std::string typeName = armarx::GetTypeString(elementType);
                ARMARX_WARNING << deactivateSpam(typeName, 1)
                               << "CoinElementVisualizer returned null for type: " << typeName << "\n"
                               << "You need to register a visualizer for each type in ArViz/Coin/Visualizer.cpp";
            }
        }

        IceUtil::Time time_updates = IceUtil::Time::now();
        timing.updateElements = time_updates - time_addLayer;

        // Remove the elements which were not contained in the update
        for (auto iter = layer.elements.begin(); iter != layer.elements.end();)
        {
            auto& elementVisu = *iter->second.visu;
            if (elementVisu.wasUpdated)
            {
                elementVisu.wasUpdated = false;
                ++iter;
            }
            else
            {
                layer.node->removeChild(elementVisu.separator);
                iter = layer.elements.erase(iter);
            }
        }

        IceUtil::Time time_remove = IceUtil::Time::now();
        timing.removeElements = time_remove - time_updates;

        emitLayerUpdated(layerID, layer);

        IceUtil::Time time_end = IceUtil::Time::now();
        timing.total = time_end - time_start;

        return timing;
    }

    void CoinVisualizer::update()
    {
        switch (state)
        {
            case CoinVisualizerState::STARTING:
            {
                std::unique_lock<std::mutex> lock(stateMutex);
                storage = stateStorage;
                root->removeAllChildren();
                layers.clear();
                pulledUpdates.revision = 0;
                pulledUpdates.updates.clear();
                updateResult = CoinVisualizerUpdateResult::SUCCESS;
                state = CoinVisualizerState::RUNNING;
            }
            break;

            case CoinVisualizerState::RUNNING:
                break;

            case CoinVisualizerState::STOPPING:
            {
                // After we have reached the STOPPED state, we know that updates are no longer running
                state = CoinVisualizerState::STOPPED;
                return;
            }
            break;

            case CoinVisualizerState::STOPPED:
                return;
        }

        switch (updateResult)
        {
            case CoinVisualizerUpdateResult::SUCCESS:
            {
                IceUtil::Time time_start = IceUtil::Time::now();
                CoinVisualizer_UpdateTiming timing;

                // We should restart the pull for updates so it can run in parallel
                data::LayerUpdates currentUpdates = pulledUpdates;
                updateResult = CoinVisualizerUpdateResult::WAITING;
                storage->begin_pullUpdatesSince(currentUpdates.revision, callback);

                auto layerIDsBefore = getLayerIDs();

                IceUtil::Time time_pull = IceUtil::Time::now();
                timing.pull = time_pull - time_start;

                timing.applies.reserve(currentUpdates.updates.size());
                for (data::LayerUpdate const& update : currentUpdates.updates)
                {
                    auto& applyTiming = timing.applies.emplace_back(apply(update));
                    timing.applyTotal.add(applyTiming);
                }
                IceUtil::Time time_apply = IceUtil::Time::now();
                timing.applyTotal.total = time_apply - time_pull;

                auto layerIDsAfter = getLayerIDs();
                if (layerIDsAfter != layerIDsBefore)
                {
                    emitLayersChanged(layerIDsAfter);
                }
                IceUtil::Time time_layersChanged = IceUtil::Time::now();
                timing.layersChanged = time_layersChanged - time_apply;

                IceUtil::Time time_end = IceUtil::Time::now();
                timing.total = time_end - time_start;

                {
                    // Copy the timing result
                    std::lock_guard lock(timingMutex);
                    timing.counter = lastTiming.counter + 1;
                    lastTiming = std::move(timing);
                }
            }
            break;
            case CoinVisualizerUpdateResult::WAITING:
            {
                // Still waiting for result
            } break;
            case CoinVisualizerUpdateResult::FAILURE:
            {
                std::unique_lock<std::mutex> lock(stateMutex);
                storage = nullptr;
                state = CoinVisualizerState::STOPPED;
            }
            break;
        }

    }

    void CoinVisualizer::onUpdateSuccess(const data::LayerUpdates& updates)
    {
        pulledUpdates = updates;
        updateResult = CoinVisualizerUpdateResult::SUCCESS;
    }

    void CoinVisualizer::onUpdateFailure(const Ice::Exception& ex)
    {
        ARMARX_WARNING << "Lost connection to ArVizStorage\n"
                       << ex.what();
        updateResult = CoinVisualizerUpdateResult::FAILURE;
    }

    void CoinVisualizer::showLayer(CoinLayerID const& id, bool visible)
    {
        auto iter = layers.find(id);
        if (iter == layers.end())
        {
            return;
        }

        viz::CoinLayer& layer = iter->second;
        int childIndex = root->findChild(layer.node);
        if (childIndex < 0)
        {
            // Layer is currently not visible
            if (visible)
            {
                root->addChild(layer.node);
            }
        }
        else
        {
            // Layer is currently visible
            if (!visible)
            {
                root->removeChild(childIndex);
            }
        }
    }

    CoinVisualizer_UpdateTiming CoinVisualizer::getTiming()
    {
        std::lock_guard lock(timingMutex);
        return lastTiming;
    }

    std::vector<CoinLayerID> CoinVisualizer::getLayerIDs()
    {
        std::vector<CoinLayerID> result;
        result.reserve(layers.size());
        for (auto& entry : layers)
        {
            result.push_back(entry.first);
        }
        return result;
    }

    void CoinVisualizer::emitLayersChanged(std::vector<CoinLayerID> const& layerIDs)
    {
        if (layersChangedCallback)
        {
            layersChangedCallback(layerIDs);
        }
    }

    void CoinVisualizer::emitLayerUpdated(const CoinLayerID& layerID, const CoinLayer& layer)
    {
        for (auto& callback : layerUpdatedCallbacks)
        {
            if (callback)
            {
                callback(layerID, layer);
            }
        }
    }
}
