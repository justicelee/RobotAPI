#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationNode.h>

namespace armarx::viz::coin
{
    struct VisualizationEllipsoid : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementEllipsoid;

        bool update(ElementType const& element)
        {
            auto color = element.color;
            constexpr float conv = 1.0f / 255.0f;
            const float r = color.r * conv;
            const float g = color.g * conv;
            const float b = color.b * conv;
            const float a = color.a * conv;

            VirtualRobot::VisualizationNodePtr ellipsoid_node;
            {
                // Params.
                SoMaterial* mat = new SoMaterial;
                mat->diffuseColor.setValue(r, g, b);
                mat->ambientColor.setValue(r, g, b);
                mat->transparency.setValue(1. - a);
                const bool show_axes = false;  // Do not show axes.  If needed, draw a Pose instead.

                SoSeparator* res = new SoSeparator();
                res->ref();
                SoUnits* u = new SoUnits();
                u->units = SoUnits::MILLIMETERS;
                res->addChild(u);
                res->addChild(VirtualRobot::CoinVisualizationFactory::CreateEllipse(
                                  element.axisLengths.e0, element.axisLengths.e1,
                                  element.axisLengths.e2, mat, show_axes));
                ellipsoid_node.reset(new VirtualRobot::CoinVisualizationNode(res));
                res->unref();
            }

            SoNode* ellipsoid = dynamic_cast<VirtualRobot::CoinVisualizationNode&>(
                                    *ellipsoid_node).getCoinVisualization();

            node->removeAllChildren();
            node->addChild(ellipsoid);

            return true;
        }
    };
}
