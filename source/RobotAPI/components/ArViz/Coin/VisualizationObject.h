#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <VirtualRobot/VirtualRobot.h>

namespace armarx::viz::coin
{
    struct LoadedObject
    {
        std::string project;
        std::string filename;
        VirtualRobot::ManipulationObjectPtr object;
    };

    struct VisualizationObject : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementObject;

        bool update(ElementType const& element);

        void recreateVisualizationNodes(int drawStyle);

        LoadedObject loaded;
        int loadedDrawStyle = data::ModelDrawStyle::ORIGINAL;
    };

    void clearObjectCache();
}
