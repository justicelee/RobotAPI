#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Component.h>

#include <Inventor/nodes/SoSeparator.h>

#include <map>
#include <unordered_map>
#include <memory>
#include <mutex>
#include <typeindex>

#include <IceUtil/Shared.h>

namespace armarx::viz
{
    using CoinLayerID = std::pair<std::string, std::string>;


    struct CoinLayerElement
    {
        data::ElementPtr data;
        std::unique_ptr<coin::ElementVisualization> visu;
    };


    struct CoinLayer
    {
        CoinLayer(SoSeparator* node)
            : node(node)
        {
            // Increase ref-count, so we can add/remove this node without it being deleted
            // This is needed for showing/hiding layers
            node->ref();
        }

        CoinLayer(CoinLayer&& other)
            : node(other.node), elements(std::move(other.elements))
        {
            other.node = nullptr;
        }

        ~CoinLayer()
        {
            if (node)
            {
                node->unref();
            }
        }

        CoinLayer(CoinLayer const&) = delete;
        void operator= (CoinLayer const&) = delete;

        void operator= (CoinLayer&& other)
        {
            node = other.node;
            other.node = nullptr;
            elements = std::move(other.elements);
        }

        SoSeparator* node = nullptr;
        std::map<std::string, CoinLayerElement> elements;
    };

    enum class CoinVisualizerState
    {
        STOPPED,
        STARTING,
        RUNNING,
        STOPPING
    };

    enum class CoinVisualizerUpdateResult
    {
        SUCCESS,
        WAITING,
        FAILURE,
    };

    struct CoinVisualizer_ApplyTiming
    {
        std::string layerName;

        IceUtil::Time addLayer = IceUtil::Time::seconds(0);
        IceUtil::Time updateElements = IceUtil::Time::seconds(0);
        IceUtil::Time removeElements = IceUtil::Time::seconds(0);
        IceUtil::Time total = IceUtil::Time::seconds(0);

        void add(CoinVisualizer_ApplyTiming const& other)
        {
            addLayer += other.addLayer;
            updateElements += other.updateElements;
            removeElements += other.removeElements;
            total += other.total;
        }
    };

    struct CoinVisualizer_UpdateTiming
    {
        std::vector<CoinVisualizer_ApplyTiming> applies;

        CoinVisualizer_ApplyTiming applyTotal;

        IceUtil::Time pull = IceUtil::Time::seconds(0);
        IceUtil::Time layersChanged = IceUtil::Time::seconds(0);
        IceUtil::Time total = IceUtil::Time::seconds(0);

        int counter = 0;
    };

    struct CoinVisualizerWrapper;

    class CoinVisualizer
    {
    public:
        CoinVisualizer();

        ~CoinVisualizer();

        void registerVisualizationTypes();

        void startAsync(StorageInterfacePrx const& storage);

        void stop();

        CoinVisualizer_ApplyTiming apply(data::LayerUpdate const& update);

        void update();

        template <typename ElementVisuT>
        void registerVisualizerFor()
        {
            using ElementT = typename ElementVisuT::ElementType;
            using VisualizerT = coin::TypedElementVisualizer<ElementVisuT>;
            // Map element type to visualizer
            elementVisualizersTypes.emplace_back(typeid(ElementT));
            elementVisualizers.push_back(std::make_unique<VisualizerT>());
        }

        void showLayer(CoinLayerID const& id, bool visible);

        CoinVisualizer_UpdateTiming getTiming();

        std::vector<CoinLayerID> getLayerIDs();
        void emitLayersChanged(std::vector<CoinLayerID> const& layerIDs);
        void emitLayerUpdated(CoinLayerID const& layerID, CoinLayer const& layer);

        void onUpdateSuccess(data::LayerUpdates const& updates);
        void onUpdateFailure(Ice::Exception const& ex);
        IceUtil::Handle<CoinVisualizerWrapper> callbackData;
        armarx::viz::Callback_StorageInterface_pullUpdatesSincePtr callback;

        std::mutex storageMutex;
        viz::StorageInterfacePrx storage;

        SoTimerSensor* timerSensor = nullptr;
        std::map<CoinLayerID, CoinLayer> layers;

        std::vector<std::type_index> elementVisualizersTypes;
        std::vector<std::unique_ptr<coin::ElementVisualizer>> elementVisualizers;

        SoSeparator* root = nullptr;

        std::atomic<CoinVisualizerUpdateResult> updateResult{CoinVisualizerUpdateResult::SUCCESS};
        data::LayerUpdates pulledUpdates;

        std::mutex stateMutex;
        std::atomic<CoinVisualizerState> state{CoinVisualizerState::STOPPED};
        viz::StorageInterfacePrx stateStorage;

        std::function<void(std::vector<CoinLayerID> const&)> layersChangedCallback;

        /// A layer's data has changed.
        std::vector<std::function<void(CoinLayerID const& layerID, CoinLayer const& layer)>> layerUpdatedCallbacks;

        std::mutex timingMutex;
        CoinVisualizer_UpdateTiming lastTiming;
    };
}
