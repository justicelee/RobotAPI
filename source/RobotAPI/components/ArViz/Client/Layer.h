#pragma once

#include "Elements.h"

#include <RobotAPI/interface/ArViz/Component.h>

#include <ArmarXCore/interface/core/BasicVectorTypes.h>

namespace armarx::viz
{

    struct Layer
    {
        Layer() = default;

        Layer(std::string const& component, std::string const& name)
        {
            data_.component = component;
            data_.name = name;
            data_.action = data::Layer_CREATE_OR_UPDATE;
        }

        void clear()
        {
            data_.elements.clear();
        }

        template <typename ElementT>
        void add(ElementT const& element)
        {
            data_.elements.push_back(element.data_);
        }

        template <typename ElementT>
        void add(std::vector<ElementT> const& elements)
        {
            for (const auto& e : elements)
            {
                add(e);
            }
        }

        template<class...Ts>
        std::enable_if_t < sizeof...(Ts) != 1 > add(Ts&& ...elems)
        {
            (add(std::forward<Ts>(elems)), ...);
        }

        data::LayerUpdate data_;
    };

}
