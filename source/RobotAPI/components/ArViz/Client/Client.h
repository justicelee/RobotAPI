#pragma once

#include <functional>
#include <vector>

#include "Layer.h"

#include <ArmarXCore/core/Component.h>


namespace armarx::viz
{

    class Client
    {
    public:
        Client() = default;

        Client(armarx::Component& component, std::string topicNameProperty = "ArVizTopicName")
        {
            componentName = component.getName();
            component.getTopicFromProperty(_topic, topicNameProperty);
        }
        Client(ManagedIceObject& obj, const std::string& topicName = "ArVizTopic")
        {
            componentName = obj.getName();
            obj.getTopic(_topic, topicName);
        }

        static Client createFromTopic(const std::string arvizNamespace, armarx::viz::Topic::ProxyType topic)
        {
            Client client;
            client.componentName = arvizNamespace;
            client._topic = topic;
            return client;
        }

        static Client createForGuiPlugin(armarx::Component& component, std::string const& topicName = "ArVizTopic")
        {
            Client client;
            std::string name = component.getName();
            std::size_t dashPos = name.find('-');
            if (dashPos != std::string::npos)
            {
                name = name.substr(0, dashPos);
            }
            client.componentName = name;
            component.getTopic(client._topic, topicName);
            return client;
        }

        // ////////////////////////////////////////////////////////////////// //
        //layer
        Layer layer(std::string const& name)
        {
            return Layer(componentName, name);
        }

        template<class...Ts>
        Layer layerContaining(std::string const& name, Ts&& ...elems)
        {
            auto l = layer(name);
            l.add(std::forward<Ts>(elems)...);
            return l;
        }
        // ////////////////////////////////////////////////////////////////// //
        //enqueue
        void enqueueLayer(const Layer& l)
        {
            updates.emplace_back(l.data_);
        }
        void enqueueLayer(const Layer* l)
        {
            updates.emplace_back(l->data_);
        }
        void enqueueLayer(std::initializer_list<Layer> const& layers)
        {
            for (const auto& l : layers)
            {
                enqueueLayer(l);
            }
        }
        void enqueueLayer(const std::vector<const Layer*>& layers)
        {
            for (const auto& l : layers)
            {
                enqueueLayer(l);
            }
        }
        void enqueueLayer(const std::vector<Layer>& layers)
        {
            for (const auto& l : layers)
            {
                enqueueLayer(l);
            }
        }
        template<class...Ts>
        void enqueueLayerContaining(std::string const& name, Ts&& ...elems)
        {
            enqueueLayer(layerContaining(name, std::forward<Ts>(elems)...));
        }
        // ////////////////////////////////////////////////////////////////// //
        //commit
        void commit(std::initializer_list<Layer> const& layers)
        {
            enqueueLayer(layers);
            commit();
        }

        void commit(const std::vector<const Layer*>& layers)
        {
            enqueueLayer(layers);
            commit();
        }

        void commit(const std::vector<Layer>& layers)
        {
            enqueueLayer(layers);
            commit();
        }

        template<class...Ts>
        std::enable_if_t < (std::is_same_v<Layer, Ts>&& ...) >
        commit(Ts const& ...layers)
        {
            (enqueueLayer(layers), ...);
            _topic->updateLayers(updates);
            updates.clear();
        }

        template<class...Ts>
        void commitLayerContaining(std::string const& name, Ts&& ...elems)
        {
            commit(layerContaining(name, std::forward<Ts>(elems)...));
        }

        const armarx::viz::TopicPrx& topic() const
        {
            return _topic;
        }
    private:
        std::string componentName;
        armarx::viz::TopicPrx _topic;

        data::LayerUpdateSeq updates;
    };

}
