#pragma once

#pragma once

#include <SimoxUtility/color/GlasbeyLUT.h>
#include <SimoxUtility/math/convert/rpy_to_quat.h>

#include <RobotAPI/interface/ArViz/Elements.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <string>

#include "Color.h"


namespace armarx::viz
{
    using data::ColoredPoint;

    // Move the ice datatypes into their own namespace
    template <typename DerivedT, typename ElementT>
    class ElementOps
    {
    public:
        ElementOps(std::string const& id)
            : data_(new ElementT)
        {
            data_->id = id;
        }

        DerivedT& id(const std::string& id)
        {
            data_->id = id;

            return *static_cast<DerivedT*>(this);
        }

        // TODO: add more overloads
        DerivedT& position(float x, float y, float z)
        {
            auto& pose = data_->pose;
            pose.x = x;
            pose.y = y;
            pose.z = z;
            return *static_cast<DerivedT*>(this);
        }
        DerivedT& position(Eigen::Vector3f const& pos)
        {
            return position(pos.x(), pos.y(), pos.z());
        }

        DerivedT& orientation(Eigen::Quaternionf const& ori)
        {
            auto& pose = data_->pose;
            pose.qw = ori.w();
            pose.qx = ori.x();
            pose.qy = ori.y();
            pose.qz = ori.z();

            return *static_cast<DerivedT*>(this);
        }
        DerivedT& orientation(Eigen::Matrix3f const& ori)
        {
            return orientation(Eigen::Quaternionf(ori));
        }
        DerivedT& orientation(float r, float p, float y)
        {
            return orientation(simox::math::rpy_to_quat(r, p, y));
        }

        DerivedT& pose(Eigen::Matrix4f const& pose)
        {
            return position(pose.block<3, 1>(0, 3)).orientation(pose.block<3, 3>(0, 0));
        }

        DerivedT& pose(Eigen::Vector3f const& position, Eigen::Quaternionf const& orientation)
        {
            return this->position(position).orientation(orientation);
        }

        DerivedT& pose(Eigen::Vector3f const& position, Eigen::Matrix3f const& orientation)
        {
            return this->position(position).orientation(orientation);
        }

        Eigen::Matrix4f pose() const
        {
            auto& p = data_->pose;
            Eigen::Matrix4f m = Eigen::Matrix4f::Identity();
            m(0, 3) = p.x;
            m(1, 3) = p.y;
            m(2, 3) = p.z;
            m.topLeftCorner<3, 3>() = Eigen::Quaternionf{p.qw, p.qx, p.qy, p.qz}.toRotationMatrix();
            return m;
        }

        DerivedT& transformPose(Eigen::Matrix4f const& p)
        {
            return pose(p * pose());
        }

        DerivedT& color(Color color)
        {
            data_->color = color;

            return *static_cast<DerivedT*>(this);
        }

        template<class...Ts>
        DerivedT& color(Ts&& ...ts)
        {
            return color({std::forward<Ts>(ts)...});
        }

        DerivedT& colorGlasbeyLUT(std::size_t id, int alpha = 255)
        {
            return color(Color::fromRGBA(simox::color::GlasbeyLUT::at(id, alpha)));
        }

        DerivedT& overrideMaterial(bool value)
        {
            if (value)
            {
                data_->flags |= data::ElementFlags::OVERRIDE_MATERIAL;
            }
            else
            {
                data_->flags &= ~data::ElementFlags::OVERRIDE_MATERIAL;
            }

            return *static_cast<DerivedT*>(this);
        }

        DerivedT& scale(float scale)
        {
            data_->scale = scale;

            return *static_cast<DerivedT*>(this);
        }

        DerivedT& hide()
        {
            data_->flags |= data::ElementFlags::HIDDEN;

            return *static_cast<DerivedT*>(this);
        }

        DerivedT& show()
        {
            data_->flags &= ~data::ElementFlags::HIDDEN;

            return *static_cast<DerivedT*>(this);
        }

        DerivedT& visible(bool visible)
        {
            if (visible)
            {
                return show();
            }
            else
            {
                return hide();
            }
        }


        IceInternal::Handle<ElementT> data_;
    };

}
