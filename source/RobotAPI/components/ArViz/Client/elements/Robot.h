#pragma once

#include "ElementOps.h"


namespace armarx::viz
{

    class Robot : public ElementOps<Robot, data::ElementRobot>
    {
    public:
        using ElementOps::ElementOps;

        Robot& file(std::string const& project, std::string const& filename)
        {
            data_->project = project;
            data_->filename = filename;

            return *this;
        }

        Robot& useCollisionModel()
        {
            data_->drawStyle |= data::ModelDrawStyle::COLLISION;

            return *this;
        }

        Robot& useFullModel()
        {
            data_->drawStyle &= ~data::ModelDrawStyle::COLLISION;

            return *this;
        }

        Robot& overrideColor(Color c)
        {
            data_->drawStyle |= data::ModelDrawStyle::OVERRIDE_COLOR;

            return color(c);
        }

        Robot& useOriginalColor()
        {
            data_->drawStyle &= ~data::ModelDrawStyle::OVERRIDE_COLOR;

            return *this;
        }

        Robot& joint(std::string const& name, float value)
        {
            data_->jointValues[name] = value;

            return *this;
        }

        Robot& joints(std::map<std::string, float> const& values)
        {
            data_->jointValues = values;

            return *this;
        }

    };

}
