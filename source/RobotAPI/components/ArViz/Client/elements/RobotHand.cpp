#include "RobotHand.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::viz
{

    RobotHand& RobotHand::fileBySide(const std::string& side, RobotInfoNodePtr robotInfo)
    {
        ARMARX_CHECK_NOT_NULL(robotInfo);
        RobotNameHelperPtr nh = RobotNameHelper::Create(robotInfo, nullptr);
        return this->fileBySide(side, *nh);
    }

    RobotHand& RobotHand::fileBySide(const std::string& side, const RobotNameHelper& nameHelper)
    {
        RobotNameHelper::Arm arm = nameHelper.getArm(side);
        return this->fileByArm(arm);
    }

    RobotHand& RobotHand::fileByArm(const RobotNameHelper::Arm& arm)
    {
        this->arm = arm;
        this->file(arm.getHandModelPackage(), arm.getHandModelPath());
        return *this;
    }

    RobotHand& RobotHand::fileByArm(const RobotNameHelper::RobotArm& arm)
    {
        return this->fileByArm(arm.getArm());
    }

    RobotHand& RobotHand::tcpPose(const Eigen::Matrix4f& tcpPose, VirtualRobot::RobotPtr robot)
    {
        ARMARX_CHECK(arm) << "Set RobotHand::side() before setting the TCP pose.";
        RobotNameHelper::RobotArm robotArm = arm->addRobot(robot);
        this->pose(tcpPose * robotArm.getTcp2HandRootTransform());
        return *this;
    }
}




