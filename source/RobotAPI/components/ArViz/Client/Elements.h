#pragma once

#include <RobotAPI/interface/ArViz/Elements.h>

#include <SimoxUtility/EigenStdVector.h>
#include <SimoxUtility/math/normal/normal_to_mat4.h>
#include <SimoxUtility/math/convert/rpy_to_mat3f.h>
#include <SimoxUtility/math/pose/transform.h>
#include <SimoxUtility/shapes/OrientedBoxBase.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <string>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "Color.h"
#include "elements/ElementOps.h"
#include "elements/Mesh.h"
#include "elements/PointCloud.h"
#include "elements/Robot.h"
//#include "elements/RobotHand.h"  // Not included by default (exposes additional headers).

// The has_member macro causes compile errors if *any* other header uses
// the identifier has_member. Boost.Thread does, so this causes compile
// errors down the line.
// Offending file: simox/SimoxUtility/meta/has_member_macros/has_member.hpp
#ifdef has_member
#undef has_member
#endif


namespace Eigen
{
    using Hyperplane3f = Hyperplane<float, 3>;
}

namespace armarx
{
    ///@see <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
    class ObjectID;
}

namespace armarx::viz
{
    using data::ColoredPoint;

    struct Convert
    {
        static Eigen::Quaternionf directionToQuaternion(Eigen::Vector3f dir)
        {
            dir = dir.normalized();
            Eigen::Vector3f naturalDir = Eigen::Vector3f::UnitY();
            Eigen::Vector3f cross = naturalDir.cross(dir);
            float dot = naturalDir.dot(dir);
            float angle = std::acos(dot);
            if (cross.squaredNorm() < 1.0e-12)
            {
                // Directions are almost colinear ==> Do no rotation
                cross = Eigen::Vector3f::UnitX();
                if (dot < 0)
                {
                    angle = M_PI;
                }
                else
                {
                    angle = 0.0f;
                }
            }
            Eigen::Vector3f axis = cross.normalized();
            Eigen::Quaternionf ori(Eigen::AngleAxisf(angle, axis));

            return ori;
        }
    };


    // Move the ice datatypes into their own namespace

    class Box : public ElementOps<Box, data::ElementBox>
    {
    public:
        using ElementOps::ElementOps;

        Box& size(Eigen::Vector3f const& s)
        {
            data_->size.e0 = s.x();
            data_->size.e1 = s.y();
            data_->size.e2 = s.z();

            return *this;
        }

        Box& size(float s)
        {
            return size(Eigen::Vector3f(s, s, s));
        }

        Box& set(const simox::OrientedBoxBase<float>& b)
        {
            size(b.dimensions());
            return pose(b.transformation_centered());
        }
        Box& set(const simox::OrientedBoxBase<double>& b)
        {
            size(b.dimensions<float>());
            return pose(b.transformation_centered<float>());
        }

        template<class T>
        Box(const std::string& name, const simox::OrientedBoxBase<T>& b)
            : ElementOps(name)
        {
            set(b);
        }

        template<class T>
        Box(
            const simox::OrientedBoxBase<T>& b,
            const std::string& name = std::to_string(std::chrono::high_resolution_clock::now().time_since_epoch().count())
        )
            : Box(name, b)
        {}
    };


    class Cylinder : public ElementOps<Cylinder, data::ElementCylinder>
    {
    public:
        using ElementOps::ElementOps;

        Cylinder& radius(float r)
        {
            data_->radius = r;

            return *this;
        }

        Cylinder& height(float h)
        {
            data_->height = h;

            return *this;
        }

        Cylinder& fromTo(Eigen::Vector3f from, Eigen::Vector3f to)
        {
            position((to + from) / 2);
            orientation(Convert::directionToQuaternion((to - from).normalized()));
            height((to - from).norm());

            return *this;
        }
    };


    class Cylindroid : public ElementOps<Cylindroid, data::ElementCylindroid>
    {
    public:
        Cylindroid(const std::string& name) :
            ElementOps(name)
        {
            data_->curvature.e0 = 1;
            data_->curvature.e1 = 1;
        }

        Cylindroid& height(float height)
        {
            data_->height = height;

            return *this;
        }

        Cylindroid& axisLengths(const Eigen::Vector2f& axisLengths)
        {
            data_->axisLengths.e0 = axisLengths.x();
            data_->axisLengths.e1 = axisLengths.y();

            return *this;
        }

        Cylindroid& curvature(const Eigen::Vector2f& curvature)
        {
            // Warning:  Custom curvatures are not yet supported by the visualization backend and
            // are thus ignored.
            data_->curvature.e0 = curvature.x();
            data_->curvature.e1 = curvature.y();

            return *this;
        }
    };


    class Sphere : public ElementOps<Sphere, data::ElementSphere>
    {
    public:
        using ElementOps::ElementOps;

        Sphere& radius(float r)
        {
            data_->radius = r;

            return *this;
        }
    };


    class Ellipsoid : public ElementOps<Ellipsoid, data::ElementEllipsoid>
    {
    public:
        Ellipsoid(const std::string& name) :
            ElementOps(name)
        {
            data_->curvature.e0 = 1;
            data_->curvature.e1 = 1;
            data_->curvature.e2 = 1;
        }

        Ellipsoid& axisLengths(const Eigen::Vector3f& axisLengths)
        {
            data_->axisLengths.e0 = axisLengths.x();
            data_->axisLengths.e1 = axisLengths.y();
            data_->axisLengths.e2 = axisLengths.z();

            return *this;
        }

        Ellipsoid& curvature(const Eigen::Vector3f& curvature)
        {
            // Warning:  Custom curvatures are not yet supported by the visualization backend and
            // are thus ignored.
            data_->curvature.e0 = curvature.x();
            data_->curvature.e1 = curvature.y();
            data_->curvature.e2 = curvature.z();

            return *this;
        }
    };


    class Pose : public ElementOps<Pose, data::ElementPose>
    {
    public:
        using ElementOps::ElementOps;
    };


    class Text : public ElementOps<Text, data::ElementText>
    {
    public:
        using ElementOps::ElementOps;

        Text& text(std::string const& t)
        {
            data_->text = t;

            return *this;
        }
    };


    class Arrow : public ElementOps<Arrow, data::ElementArrow>
    {
    public:
        using ElementOps::ElementOps;

        Arrow& direction(Eigen::Vector3f dir)
        {
            return orientation(Convert::directionToQuaternion(dir));
        }

        Arrow& length(float l)
        {
            data_->length = l;

            return *this;
        }

        Arrow& width(float w)
        {
            data_->width = w;

            return *this;
        }

        Arrow& fromTo(Eigen::Vector3f from, Eigen::Vector3f to)
        {
            position(from);
            direction((to - from).normalized());
            length((to - from).norm());

            return *this;
        }
    };


    class ArrowCircle : public ElementOps<ArrowCircle, data::ElementArrowCircle>
    {
    public:
        using ElementOps::ElementOps;

        ArrowCircle& normal(Eigen::Vector3f dir)
        {
            Eigen::Vector3f naturalDir = Eigen::Vector3f::UnitZ();
            Eigen::Vector3f cross = naturalDir.cross(dir);
            float angle = std::acos(naturalDir.dot(dir));
            if (cross.squaredNorm() < 1.0e-12f)
            {
                // Directions are almost colinear ==> Do no rotation
                cross = Eigen::Vector3f::UnitX();
                angle = 0.0f;
            }
            Eigen::Vector3f axis = cross.normalized();
            Eigen::Quaternionf ori(Eigen::AngleAxisf(angle, axis));

            return orientation(ori);
        }

        ArrowCircle& radius(float r)
        {
            data_->radius = r;

            return *this;
        }


        ArrowCircle& width(float w)
        {
            data_->width = w;

            return *this;
        }

        ArrowCircle& completion(float c)
        {
            data_->completion = c;

            return *this;
        }
    };


    class Polygon : public ElementOps<Polygon, data::ElementPolygon>
    {
    public:
        using ElementOps::ElementOps;

        Polygon& clear()
        {
            data_->points.clear();
            return *this;
        }

        Polygon& lineColor(Color color)
        {
            data_->lineColor = color;

            return *this;
        }

        template<class...Ts>
        Polygon& lineColor(Ts&& ...ts)
        {
            return lineColor({std::forward<Ts>(ts)...});
        }

        Polygon& lineColorGlasbeyLUT(std::size_t id, int alpha = 255)
        {
            return lineColor(Color::fromRGBA(simox::color::GlasbeyLUT::at(id, alpha)));
        }

        Polygon& lineWidth(float w)
        {
            data_->lineWidth = w;

            return *this;
        }

        Polygon& points(std::vector<Eigen::Vector3f> const& ps)
        {
            auto& points = data_->points;
            points.clear();
            points.reserve(ps.size());
            for (auto& p : ps)
            {
                points.push_back(armarx::Vector3f{p.x(), p.y(), p.z()});
            }

            return *this;
        }

        Polygon& addPoint(Eigen::Vector3f p)
        {
            data_->points.push_back(armarx::Vector3f{p.x(), p.y(), p.z()});

            return *this;
        }

        /**
         * @brief Add points representing a plane as rectangle.
         * @param plane The plane.
         * @param at Center of rectangle, is projected onto plane.
         * @param size Extents of rectangle.
         */
        Polygon& plane(Eigen::Hyperplane3f plane, Eigen::Vector3f at, Eigen::Vector2f size)
        {
            const Eigen::Quaternionf ori = Eigen::Quaternionf::FromTwoVectors(
                                               Eigen::Vector3f::UnitZ(), plane.normal());
            return this->plane(plane.projection(at), ori, size);
        }

        /**
         * @brief Add points representing the XY-plane of the given coordinate system as rectangle.
         * @param center The rectangle center.
         * @param orientation The orientation of the coordinate system.
         * @param size The XY-size of the rectangle.
         */
        Polygon& plane(Eigen::Vector3f center, Eigen::Quaternionf orientation, Eigen::Vector2f size)
        {
            const Eigen::Vector3f x = 0.5f * size.x() * (orientation * Eigen::Vector3f::UnitX());
            const Eigen::Vector3f y = 0.5f * size.y() * (orientation * Eigen::Vector3f::UnitY());

            addPoint(center + x + y);
            addPoint(center - x + y);
            addPoint(center - x - y);
            addPoint(center + x - y);

            return *this;
        }

        Polygon& circle(Eigen::Vector3f center, Eigen::Vector3f normal, float radius, std::size_t tessellation = 64)
        {
            const Eigen::Matrix4f pose = simox::math::normal_pos_to_mat4(normal, center);
            ARMARX_CHECK_GREATER_EQUAL(tessellation, 3);

            const float angle = 2 * M_PI / tessellation;
            const Eigen::Matrix3f rot = simox::math::rpy_to_mat3f(0, 0, angle);

            Eigen::Vector3f lastLocalPoint = Eigen::Vector3f::UnitX() * radius;
            addPoint(simox::math::transform_position(pose, lastLocalPoint));
            while (--tessellation)
            {
                const Eigen::Vector3f localPoint = rot * lastLocalPoint;
                addPoint(simox::math::transform_position(pose, localPoint));
                lastLocalPoint = localPoint;
            }
            return *this;
        }
    };


    class Object : public ElementOps<Object, data::ElementObject>
    {
    private:
        static const std::string DefaultObjectsPackage;

    public:
        using ElementOps::ElementOps;

        Object& file(std::string const& project, std::string const& filename)
        {
            data_->project = project;
            data_->filename = filename;

            return *this;
        }
        Object& file(std::string const& filename)
        {
            return file("", filename);
        }

        /**
         * @brief Set the file so it could be found using `armarx::ObjectFinder` (also on remote machine).
         * @param objectID The object ID, see <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
         * @param objectsPackage The objects package ("ArmarXObjects" by default)
         * @see <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
         */
        Object& fileByObjectFinder(const armarx::ObjectID& objectID, const std::string& objectsPackage = DefaultObjectsPackage);
        Object& fileByObjectFinder(const std::string& objectID, const std::string& objectsPackage = DefaultObjectsPackage);


        Object& useCollisionModel()
        {
            data_->drawStyle |= data::ModelDrawStyle::COLLISION;

            return *this;
        }

        Object& useFullModel()
        {
            data_->drawStyle &= ~data::ModelDrawStyle::COLLISION;

            return *this;
        }

        Object& overrideColor(Color c)
        {
            data_->drawStyle |= data::ModelDrawStyle::OVERRIDE_COLOR;

            return color(c);
        }

        Object& useOriginalColor()
        {
            data_->drawStyle &= ~data::ModelDrawStyle::OVERRIDE_COLOR;

            return *this;
        }

    };

}
