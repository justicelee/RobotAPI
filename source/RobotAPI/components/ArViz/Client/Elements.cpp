#include "Elements.h"

#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectInfo.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>


namespace armarx::viz
{

    const std::string Object::DefaultObjectsPackage = armarx::ObjectFinder::DefaultObjectsPackageName;

    Object& Object::fileByObjectFinder(const std::string& objectID, const std::string& objectsPackage)
    {
        return this->fileByObjectFinder(armarx::ObjectID(objectID), objectsPackage);
    }

    Object& Object::fileByObjectFinder(const armarx::ObjectID& objectID, const std::string& objectsPackage)
    {
        ObjectInfo info(objectsPackage, "", objectID);
        armarx::PackageFileLocation file = info.simoxXML();
        return this->file(file.package, file.relativePath);
    }
}


