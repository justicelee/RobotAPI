/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObjectPoseObserver
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXObjects::ObjectPoseObserver

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include <RobotAPI/components/ObjectPoseObserver/ObjectPoseObserver.h>

#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <iostream>

using namespace armarx;


BOOST_AUTO_TEST_CASE(test_from_to_OOBB)
{
    Eigen::Vector3f pos(-100, -200, -300);
    Eigen::Matrix3f ori = Eigen::AngleAxisf(1.0, Eigen::Vector3f(1, 2, 3).normalized()).toRotationMatrix();
    Eigen::Vector3f extents(40, 50, 60);

    armarx::objpose::Box box;
    box.position = new Vector3(pos);
    box.orientation = new Quaternion(ori);
    box.extents = new Vector3(extents);


    const float prec = 1e-3;

    simox::OrientedBoxf oobb;
    armarx::objpose::fromIce(box, oobb);
    ARMARX_CHECK_LESS_EQUAL((oobb.center() - pos).norm(), prec);
    ARMARX_CHECK(oobb.rotation().isApprox(ori, prec));
    ARMARX_CHECK_LESS_EQUAL((oobb.dimensions() - extents).norm(), prec);

    armarx::objpose::Box boxOut;
    armarx::objpose::toIce(boxOut, oobb);

    Eigen::Vector3f posOut = Vector3Ptr::dynamicCast(boxOut.position)->toEigen();
    Eigen::Matrix3f oriOut = QuaternionPtr::dynamicCast(boxOut.orientation)->toEigen();
    Eigen::Vector3f extentsOut = Vector3Ptr::dynamicCast(boxOut.extents)->toEigen();

    ARMARX_CHECK_LESS_EQUAL((posOut - pos).norm(), prec);
    ARMARX_CHECK(oriOut.isApprox(ori, prec));
    ARMARX_CHECK_LESS_EQUAL((extentsOut - extents).norm(), prec);
}
