#include "ObjectPoseClientPlugin.h"

namespace armarx::plugins
{
    void ObjectPoseClientPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(PROPERTY_NAME)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(PROPERTY_NAME),
                "ObjectPoseObserver",
                "Name of the object pose observer.");
        }
    }

    void ObjectPoseClientPlugin::preOnInitComponent()
    {
        parent<Component>().usingProxyFromProperty(makePropertyName(PROPERTY_NAME));
    }

    void ObjectPoseClientPlugin::preOnConnectComponent()
    {
        parent<ObjectPoseClientPluginUser>().objectPoseObserver = createObjectPoseObserver();
    }

    objpose::ObjectPoseObserverInterfacePrx ObjectPoseClientPlugin::createObjectPoseObserver()
    {
        return parent<Component>().getProxyFromProperty<objpose::ObjectPoseObserverInterfacePrx>(makePropertyName(PROPERTY_NAME));
    }

    const ObjectFinder& ObjectPoseClientPlugin::setObjectFinderPath(const std::string& path)
    {
        _finder.setPath(path);
        return _finder;
    }

    const ObjectFinder& ObjectPoseClientPlugin::getObjectFinder() const
    {
        return _finder;
    }
}

namespace armarx
{
    ObjectPoseClientPluginUser::ObjectPoseClientPluginUser()
    {
        addPlugin(plugin);
    }

    objpose::ObjectPoseObserverInterfacePrx ObjectPoseClientPluginUser::createObjectPoseObserver()
    {
        return plugin->createObjectPoseObserver();
    }

    objpose::ObjectPoseSeq ObjectPoseClientPluginUser::getObjectPoses()
    {
        if (!objectPoseObserver)
        {
            ARMARX_WARNING << "No object pose observer.";
            return {};
        }
        return objpose::fromIce(objectPoseObserver->getObjectPoses());
    }

    plugins::ObjectPoseClientPlugin& ObjectPoseClientPluginUser::getObjectPoseClientPlugin()
    {
        return *plugin;
    }
    const plugins::ObjectPoseClientPlugin& ObjectPoseClientPluginUser::getObjectPoseClientPlugin() const
    {
        return *plugin;
    }

    const ObjectFinder& ObjectPoseClientPluginUser::getObjectFinder() const
    {
        return plugin->getObjectFinder();
    }
}

