/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObjectPoseObserver
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectPoseObserver.h"

#include <SimoxUtility/algorithm/get_map_keys_values.h>
#include <SimoxUtility/math/pose/pose.h>
#include <SimoxUtility/meta/EnumNames.hpp>
#include <SimoxUtility/shapes/OrientedBox.h>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotConfig.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>

namespace armarx
{

    ObjectPoseObserverPropertyDefinitions::ObjectPoseObserverPropertyDefinitions(std::string prefix) :
        armarx::ObserverPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr ObjectPoseObserver::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(new ObjectPoseObserverPropertyDefinitions(getConfigIdentifier()));

        defs->defineOptionalProperty<std::string>("ObjectPoseTopicName", "ObjectPoseTopic", "Name of the Object Pose Topic.");

        defs->optional(visu.enabled, "visu.enabled", "Enable or disable visualization of objects.");
        defs->optional(visu.inGlobalFrame, "visu.inGlobalFrame", "If true, show global poses. If false, show poses in robot frame.");
        defs->optional(visu.alpha, "visu.alpha", "Alpha of objects (1 = solid, 0 = transparent).");
        defs->optional(visu.oobbs, "visu.oobbs", "Enable showing oriented bounding boxes.");
        defs->optional(visu.objectFrames, "visu.objectFrames", "Enable showing object frames.");
        defs->optional(visu.objectFramesScale, "visu.objectFramesScale", "Scaling of object frames.");
        defs->optional(calibration.robotNode, "calibration.robotNode", "Robot node which can be calibrated");
        defs->optional(calibration.offset, "calibration.offset", "Offset for the node to be calibrated");

        return defs;
    }

    std::string ObjectPoseObserver::getDefaultName() const
    {
        return "ObjectPoseObserver";
    }

    void ObjectPoseObserver::onInitObserver()
    {
        usingTopicFromProperty("ObjectPoseTopicName");
    }

    void ObjectPoseObserver::onConnectObserver()
    {
        // onConnect can be called multiple times, but addRobot will fail if called more than once with the same ID
        // So we need to always make sure to guard a call to addRobot
        if (!RobotState::hasRobot("robot"))
        {
            robot = RobotState::addRobot("robot", VirtualRobot::RobotIO::RobotDescription::eStructure);
        }

        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }

    void ObjectPoseObserver::onDisconnectComponent()
    {
    }

    void ObjectPoseObserver::onExitObserver()
    {
    }


    void ObjectPoseObserver::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        tab.visuEnabled.setValue(visu.enabled);
        tab.visuInGlobalFrame.setValue(visu.inGlobalFrame);
        tab.visuAlpha.setRange(0, 1.0);
        tab.visuAlpha.setValue(visu.alpha);
        tab.visuOOBBs.setValue(visu.oobbs);
        tab.visuObjectFrames.setValue(visu.objectFrames);
        {
            float max = 10000;
            tab.visuObjectFramesScale.setRange(0, max);
            tab.visuObjectFramesScale.setDecimals(2);
            tab.visuObjectFramesScale.setSteps(int(10 * max));
            tab.visuObjectFramesScale.setValue(visu.objectFramesScale);
        }

        GroupBox visuGroup;
        {
            GridLayout grid;
            int row = 0;
            grid.add(Label("Enabled"), {row, 0}).add(tab.visuEnabled, {row, 1});
            row++;
            grid.add(Label("Global Frame"), {row, 0}).add(tab.visuInGlobalFrame, {row, 1});
            row++;
            grid.add(Label("Alpha"), {row, 0}).add(tab.visuAlpha, {row, 1}, {1, 3});
            row++;
            grid.add(Label("OOBB"), {row, 0}).add(tab.visuOOBBs, {row, 1});
            row++;
            grid.add(Label("Object Frames"), {row, 0}).add(tab.visuObjectFrames, {row, 1});
            grid.add(Label("Scale:"), {row, 2}).add(tab.visuObjectFramesScale, {row, 3});
            row++;

            visuGroup.setLabel("Visualization");
            visuGroup.addChild(grid);
        }

        VBoxLayout root = {visuGroup, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }

    void ObjectPoseObserver::RemoteGui_update()
    {
        // Non-atomic variables need to be guarded by a mutex if accessed by multiple threads
        std::scoped_lock lock(dataMutex);

        visu.enabled = tab.visuEnabled.getValue();
        visu.inGlobalFrame = tab.visuInGlobalFrame.getValue();
        visu.alpha = tab.visuAlpha.getValue();
        visu.oobbs = tab.visuOOBBs.getValue();
        visu.objectFrames = tab.visuObjectFrames.getValue();
        visu.objectFramesScale = tab.visuObjectFramesScale.getValue();
    }


    void ObjectPoseObserver::reportProviderAvailable(const std::string& providerName, const objpose::ProviderInfo& info, const Ice::Current&)
    {
        updateProviderInfo(providerName, info);
    }


    void ObjectPoseObserver::reportObjectPoses(
        const std::string& providerName, const objpose::data::ProvidedObjectPoseSeq& providedPoses, const Ice::Current&)
    {
        ARMARX_VERBOSE << "Received object poses from '" << providerName << "'.";

        objpose::ObjectPoseSeq objectPoses;
        {
            std::scoped_lock lock(dataMutex);
            RobotState::synchronizeLocalClone(robot);

            if (robot->hasRobotNode(calibration.robotNode))
            {
                VirtualRobot::RobotNodePtr robotNode = robot->getRobotNode(calibration.robotNode);
                float value = robotNode->getJointValue();
                robotNode->setJointValue(value + calibration.offset);
            }

            for (const objpose::data::ProvidedObjectPose& provided : providedPoses)
            {
                objpose::ObjectPose& pose = objectPoses.emplace_back();
                pose.fromProvidedPose(provided, robot);
                if (pose.objectID.dataset().empty())
                {
                    // Try to find the data set. (It might be good to cache this.)
                    if (std::optional<ObjectInfo> objectInfo = objectFinder.findObject(pose.objectID))
                    {
                        pose.objectID = { objectInfo->dataset(), pose.objectID.className(), pose.objectID.instanceName() };
                    }
                }
                if (!provided.localOOBB)
                {
                    pose.localOOBB = getObjectOOBB(pose.objectID);
                }
            }
        }

        {
            std::scoped_lock lock(dataMutex);
            this->objectPoses[providerName] = objectPoses;
            handleProviderUpdate(providerName);
        }
    }


    void ObjectPoseObserver::updateProviderInfo(const std::string& providerName, const objpose::ProviderInfo& info)
    {
        if (!info.proxy)
        {
            ARMARX_WARNING << "Received availability signal by provider '" << providerName << "' "
                           << "with invalid provider proxy.\nIgnoring provider '" << providerName << "'.";
            return;
        }
        {
            std::scoped_lock lock(dataMutex);
            std::stringstream ss;
            for (const auto& id : info.supportedObjects)
            {
                ss << "- " << id << "\n";
            }
            ARMARX_VERBOSE << "Provider '" << providerName << "' available.\n"
                           << "Supported objects: \n" << ss.str();
            providers[providerName] = info;

            if (updateCounters.count(providerName) == 0)
            {
                updateCounters[providerName] = 0;
            }
        }

        if (!existsChannel(providerName))
        {
            offerChannel(providerName, "Channel of provider '" + providerName + "'.");
        }
        offerOrUpdateDataField(providerName, "objectType", objpose::ObjectTypeEnumNames.to_name(info.objectType), "The object type (known or unknown)");
        offerOrUpdateDataField(providerName, "numSupportedObjects", int(info.supportedObjects.size()), "Number of requestable objects.");
    }


    objpose::data::ObjectPoseSeq ObjectPoseObserver::getObjectPoses(const Ice::Current&)
    {
        // IceUtil::Time start = IceUtil::Time::now();
        std::scoped_lock lock(dataMutex);
        // ARMARX_INFO << "Locking took " << (IceUtil::Time::now() - start).toMilliSeconds() << " ms";
        // start = IceUtil::Time::now();
        bool synchronized = false;
        objpose::data::ObjectPoseSeq result;
        for (auto& [name, poses] : objectPoses)
        {
            toIceWithAttachments(poses, robot, result, synchronized);
        }
        // ARMARX_INFO << "getObjectPoses() took " << (IceUtil::Time::now() - start).toMilliSeconds() << " ms";
        return result;
    }

    objpose::data::ObjectPoseSeq ObjectPoseObserver::getObjectPosesByProvider(const std::string& providerName, const Ice::Current&)
    {
        std::scoped_lock lock(dataMutex);
        bool synchronized = false;
        objpose::data::ObjectPoseSeq result;
        toIceWithAttachments(objectPoses.at(providerName), robot, result, synchronized);
        return result;
    }


    objpose::observer::RequestObjectsOutput ObjectPoseObserver::requestObjects(
        const objpose::observer::RequestObjectsInput& input, const Ice::Current&)
    {
        std::map<std::string, objpose::provider::RequestObjectsInput> providerRequests;
        std::map<std::string, objpose::ObjectPoseProviderPrx> proxies;

        objpose::observer::RequestObjectsOutput output;

        auto updateProxy = [&](const std::string & providerName)
        {
            if (proxies.count(providerName) == 0)
            {
                if (auto it = providers.find(providerName); it != providers.end())
                {
                    proxies[providerName] = it->second.proxy;
                }
                else
                {
                    ARMARX_ERROR << "No proxy for provider ' " << providerName << "'.";
                    proxies[providerName] = nullptr;
                }
            }
        };

        if (input.provider.size() > 0)
        {
            providerRequests[input.provider] = input.request;
            updateProxy(input.provider);
        }
        else
        {
            std::scoped_lock lock(dataMutex);
            for (const auto& objectID : input.request.objectIDs)
            {
                bool found = true;
                for (const auto& [providerName, info] : providers)
                {
                    // ToDo: optimize look up.
                    if (std::find(info.supportedObjects.begin(), info.supportedObjects.end(), objectID) != info.supportedObjects.end())
                    {
                        providerRequests[providerName].objectIDs.push_back(objectID);
                        updateProxy(providerName);
                        break;
                    }
                }
                if (!found)
                {
                    ARMARX_ERROR << "Did not find a provider for " << objectID << ".";
                    output.results[objectID].providerName = "";
                }
            }
        }

        for (const auto& [providerName, request] : providerRequests)
        {
            if (objpose::ObjectPoseProviderPrx proxy = proxies.at(providerName); proxy)
            {
                ARMARX_INFO << "Requesting " << request.objectIDs.size() << " objects by provider '"
                            << providerName << "' for " << request.relativeTimeoutMS << " ms.";
                objpose::provider::RequestObjectsOutput providerOutput = proxy->requestObjects(request);

                int successful = 0;
                for (const auto& [objectID, result] : providerOutput.results)
                {
                    objpose::observer::ObjectRequestResult& res = output.results[objectID];
                    res.providerName = providerName;
                    res.result = result;
                    successful += int(result.success);
                }
                ARMARX_INFO << successful << " of " << request.objectIDs.size() << " object requests successful.";
            }
        }
        return output;
    }

    objpose::ProviderInfoMap ObjectPoseObserver::getAvailableProvidersInfo(const Ice::Current&)
    {
        std::scoped_lock lock(dataMutex);
        return providers;
    }

    Ice::StringSeq ObjectPoseObserver::getAvailableProviderNames(const Ice::Current&)
    {
        std::scoped_lock lock(dataMutex);
        return simox::get_keys(providers);
    }

    objpose::ProviderInfo ObjectPoseObserver::getProviderInfo(const std::string& providerName, const Ice::Current&)
    {
        try
        {
            return providers.at(providerName);
        }
        catch (const std::out_of_range&)
        {
            std::stringstream ss;
            ss << "No provider with name '" << providerName << "' available.\n";
            ss << "Available are:\n";
            for (const auto& [name, _] : providers)
            {
                ss << "- '" << name << "'\n";
            }
            throw std::out_of_range(ss.str());
        }
    }

    bool ObjectPoseObserver::hasProvider(const std::string& providerName, const Ice::Current&)
    {
        std::scoped_lock lock(dataMutex);
        return providers.count(providerName) > 0;
    }



    objpose::AttachObjectToRobotNodeOutput ObjectPoseObserver::attachObjectToRobotNode(
        const objpose::AttachObjectToRobotNodeInput& input, const Ice::Current&)
    {
        std::scoped_lock lock(dataMutex);

        objpose::AttachObjectToRobotNodeOutput output;
        output.success = false;  // We are not successful until proven otherwise.

        ObjectID objectID = armarx::fromIce(input.objectID);

        if (input.agentName != "" && input.agentName != robot->getName())
        {
            ARMARX_WARNING << "Tried to attach object " << objectID << " to unknown agent '" << input.agentName << "'."
                           << "\n(You can leave the agent name empty if there is only one agent.)\n"
                           << "\nKnown agents: " << std::vector<std::string> {robot->getName()};
            return output;
        }
        VirtualRobot::RobotPtr agent = this->robot;

        if (!agent->hasRobotNode(input.frameName))
        {
            ARMARX_WARNING << "Tried to attach object " << objectID << " to unknown node '" << input.frameName
                           << "' of agent '" << agent->getName() << "'.";
            return output;
        }
        std::string frameName = input.frameName;


        // Find object pose.
        objpose::ObjectPose* currentObjectPose = findObjectPose(input.providerName, objectID);
        if (!currentObjectPose)
        {
            ARMARX_WARNING << "Tried to attach object " << objectID << " to node '" << frameName
                           << "' of agent '" << agent->getName() << "', but object is currently not provided.";
            return output;
        }

        objpose::ObjectAttachmentInfo info;
        info.agentName = agent->getName();
        info.frameName = frameName;

        if (input.poseInFrame)
        {
            info.poseInFrame = PosePtr::dynamicCast(input.poseInFrame)->toEigen();
        }
        else
        {
            RobotState::synchronizeLocalClone(robot);

            armarx::FramedPose framed(currentObjectPose->objectPoseGlobal, armarx::GlobalFrame, agent->getName());
            if (frameName == armarx::GlobalFrame)
            {
                info.poseInFrame = framed.toGlobalEigen(robot);
            }
            else
            {
                framed.changeFrame(robot, info.frameName);
                info.poseInFrame = framed.toEigen();
            }
        }
        attachments[std::make_pair(currentObjectPose->providerName, objectID)] = info;

        ARMARX_INFO << "Attached object " << objectID << " by provider '" << currentObjectPose->providerName << "' "
                    << "to node '" << info.frameName << "' of agent '" << info.agentName << "'.\n"
                    << "Object pose in frame: \n" << info.poseInFrame;

        output.success = true;
        output.attachment = new objpose::data::ObjectAttachmentInfo();
        output.attachment->frameName = info.frameName;
        output.attachment->agentName = info.agentName;
        output.attachment->poseInFrame = new Pose(info.poseInFrame);

        return output;
    }

    objpose::DetachObjectFromRobotNodeOutput ObjectPoseObserver::detachObjectFromRobotNode(
        const objpose::DetachObjectFromRobotNodeInput& input, const Ice::Current&)
    {
        ObjectID objectID = armarx::fromIce(input.objectID);
        std::string providerName = input.providerName;

        std::optional<objpose::ObjectAttachmentInfo> attachment;
        {
            std::scoped_lock lock(dataMutex);

            // Remove from latest pose (if it was cached).
            objpose::ObjectPose* objectPose = findObjectPose(input.providerName, objectID);
            if (objectPose)
            {
                objectPose->attachment = std::nullopt;
            }

            if (providerName.empty() && objectPose)
            {
                providerName = objectPose->providerName;
            }
            // Remove from attachment map.
            if (input.providerName.size() > 0)
            {
                auto it = attachments.find(std::make_pair(input.providerName, objectID));
                if (it != attachments.end())
                {
                    attachment = it->second;
                    attachments.erase(it);
                }
            }
            else
            {
                // Search for entry with matching object ID.
                for (auto it = attachments.begin(); it != attachments.end(); ++it)
                {
                    const ObjectID& id = it->first.second;
                    if (id == objectID)
                    {
                        attachment = it->second;
                        attachments.erase(it);
                        break;
                    }
                }
            }
        }

        objpose::DetachObjectFromRobotNodeOutput output;
        output.wasAttached = bool(attachment);
        if (attachment)
        {
            ARMARX_INFO << "Detached object " << objectID << " by provider '" << providerName << "' from robot node '"
                        << attachment->frameName << "' of agent '" << attachment->agentName << "'.";
        }
        else
        {
            ARMARX_INFO << "Tried to detach object " << objectID << " by provider '" << providerName << "' "
                        << "from robot node, but it was not attached.";
        }

        return output;
    }

    objpose::DetachAllObjectsFromRobotNodesOutput ObjectPoseObserver::detachAllObjectsFromRobotNodes(const Ice::Current&)
    {
        objpose::DetachAllObjectsFromRobotNodesOutput output;
        output.numDetached = 0;

        {
            std::scoped_lock lock(dataMutex);

            // Clear attachment map.
            size_t num = attachments.size();
            attachments.clear();
            output.numDetached = int(num);

            // Remove from poses (if it was cached).
            for (auto& [prov, poses] : objectPoses)
            {
                for (auto& pose : poses)
                {
                    pose.attachment = std::nullopt;
                }
            }
        }
        ARMARX_INFO << "Detached all objects (" << output.numDetached << ") from robot nodes.";

        return output;
    }


    objpose::AgentFramesSeq ObjectPoseObserver::getAttachableFrames(const Ice::Current&)
    {
        std::scoped_lock lock(dataMutex);

        objpose::AgentFramesSeq output;
        std::vector<VirtualRobot::RobotPtr> agents = { robot };
        for (VirtualRobot::RobotPtr agent : agents)
        {
            objpose::AgentFrames& frames = output.emplace_back();
            frames.agent = agent->getName();
            frames.frames = agent->getRobotNodeNames();
        }
        return output;
    }



    void ObjectPoseObserver::handleProviderUpdate(const std::string& providerName)
    {
        // Initialized to 0 on first access.
        updateCounters[providerName]++;
        if (providers.count(providerName) == 0)
        {
            providers[providerName] = objpose::ProviderInfo();
        }

        if (!existsChannel(providerName))
        {
            offerChannel(providerName, "Channel of provider '" + providerName + "'.");
        }
        offerOrUpdateDataField(providerName, "updateCounter", Variant(updateCounters.at(providerName)), "Counter incremented for each update.");
        offerOrUpdateDataField(providerName, "objectCount", Variant(int(objectPoses.at(providerName).size())), "Number of provided object poses.");

        if (visu.enabled)
        {
            visProviderUpdate(providerName);
        }
    }


    void ObjectPoseObserver::visProviderUpdate(const std::string& providerName)
    {
        viz::Layer layer = arviz.layer(providerName);

        bool synchronized = false;

        for (objpose::ObjectPose& objectPose : objectPoses.at(providerName))
        {
            const armarx::ObjectID id = objectPose.objectID;
            std::string key = id.str();

            Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();

            if (isAttachedCached(objectPose))
            {
                if (!synchronized)
                {
                    RobotState::synchronizeLocalClone(robot);
                    synchronized = true;
                }
                objpose::ObjectPose updated = applyAttachment(objectPose, robot);
                pose = visu.inGlobalFrame ? updated.objectPoseGlobal : updated.objectPoseRobot;
            }
            else
            {
                pose = visu.inGlobalFrame ? objectPose.objectPoseGlobal : objectPose.objectPoseRobot;
            }

            {
                viz::Object object = viz::Object(key).pose(pose);
                if (std::optional<ObjectInfo> objectInfo = objectFinder.findObject(id))
                {
                    object.file(objectInfo->package(), objectInfo->simoxXML().relativePath);
                }
                else
                {
                    object.fileByObjectFinder(id);
                }
                if (visu.alpha < 1)
                {
                    object.overrideColor(simox::Color::white().with_alpha(visu.alpha));
                }
                layer.add(object);
            }

            if (visu.oobbs && objectPose.localOOBB)
            {
                const simox::OrientedBoxf& oobb = *objectPose.localOOBB;
                layer.add(viz::Box(key + " OOBB").set(oobb.transformed(pose))
                          .color(simox::Color::lime(255, 64)));
            }
            if (visu.objectFrames)
            {
                layer.add(viz::Pose(key + " Pose").pose(pose).scale(visu.objectFramesScale));
            }
        }

        arviz.commit(layer);
    }

    objpose::ObjectPoseProviderPrx ObjectPoseObserver::getProviderProxy(const std::string& providerName)
    {
        return getProxy<objpose::ObjectPoseProviderPrx>(providerName, false, "", false);
    }

    std::optional<simox::OrientedBoxf> ObjectPoseObserver::getObjectOOBB(const ObjectID& id)
    {
        if (auto it = oobbCache.find(id); it != oobbCache.end())
        {
            return it->second;
        }

        // Try to get OOBB from repository.
        std::optional<simox::OrientedBoxf> oobb;
        if (std::optional<ObjectInfo> objectInfo = objectFinder.findObject(id))
        {
            try
            {
                oobb = objectInfo->loadOOBB();
            }
            catch (const std::ios_base::failure& e)
            {
                // Give up - no OOBB information.
                ARMARX_WARNING << "Could not get OOBB of object " << id << ".\n- " << e.what();
                oobb = std::nullopt;
            }
        }
        oobbCache[id] = oobb;  // Store result in any case.
        return oobb;
    }

    objpose::ObjectPose* ObjectPoseObserver::findObjectPose(const std::string& providerName, ObjectID& objectID)
    {
        objpose::ObjectPose* pose = nullptr;
        if (!providerName.empty())
        {
            pose = findObjectPoseByID(objectPoses.at(providerName), objectID);
        }
        else
        {
            for (auto& [_, poses] : objectPoses)
            {
                pose = findObjectPoseByID(poses, objectID);
                if (pose)
                {
                    break;
                }
            }
        }
        return pose;
    }

    objpose::ObjectPose* ObjectPoseObserver::findObjectPoseByID(objpose::ObjectPoseSeq& objectPoses, const ObjectID& id)
    {
        for (objpose::ObjectPose& pose : objectPoses)
        {
            if (pose.objectID == id)
            {
                return &pose;
            }
        }
        return nullptr;
    }

    bool ObjectPoseObserver::isAttachedCached(objpose::ObjectPose& objectPose) const
    {
        if (!objectPose.attachment)
        {
            auto it = attachments.find(std::make_pair(objectPose.providerName, objectPose.objectID));
            if (it != attachments.end())
            {
                objectPose.attachment = it->second;
            }
        }
        return bool(objectPose.attachment);
    }

    objpose::ObjectPose ObjectPoseObserver::applyAttachment(
        const objpose::ObjectPose& objectPose, VirtualRobot::RobotPtr agent)
    {
        ARMARX_CHECK(objectPose.attachment);

        objpose::ObjectPose updated = objectPose;
        const objpose::ObjectAttachmentInfo& attachment = *updated.attachment;

        ARMARX_CHECK_EQUAL(attachment.agentName, agent->getName());

        VirtualRobot::RobotNodePtr robotNode = agent->getRobotNode(attachment.frameName);
        ARMARX_CHECK_NOT_NULL(robotNode);

        // Overwrite object pose
        updated.objectPoseRobot = robotNode->getPoseInRootFrame() * attachment.poseInFrame;
        updated.objectPoseGlobal = agent->getGlobalPose() * updated.objectPoseRobot;

        updated.robotPose = agent->getGlobalPose();
        updated.robotConfig = agent->getConfig()->getRobotNodeJointValueMap();

        return updated;
    }


    void ObjectPoseObserver::toIceWithAttachments(
        objpose::ObjectPoseSeq& objectPoses, VirtualRobot::RobotPtr agent,
        objpose::data::ObjectPoseSeq& result, bool& synchronized)
    {
        for (objpose::ObjectPose& pose : objectPoses)
        {
            if (isAttachedCached(pose))
            {
                if (!synchronized)  // Synchronize only once.
                {
                    RobotState::synchronizeLocalClone(agent);
                    synchronized = true;
                }
                result.push_back(applyAttachment(pose, agent).toIce());
            }
            else
            {
                result.push_back(pose.toIce());
            }
        }
    }

}

