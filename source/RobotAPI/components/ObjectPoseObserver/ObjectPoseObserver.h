/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObjectPoseObserver
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/observers/Observer.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <RobotAPI/interface/objectpose/ObjectPoseObserver.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>

#define ICE_CURRENT_ARG const Ice::Current& = Ice::emptyCurrent



namespace armarx
{
    /**
     * @class ObjectPoseObserverPropertyDefinitions
     * @brief Property definitions of `ObjectPoseObserver`.
     */
    class ObjectPoseObserverPropertyDefinitions :
        public ObserverPropertyDefinitions
    {
    public:
        ObjectPoseObserverPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-ObjectPoseObserver ObjectPoseObserver
     * @ingroup RobotAPI-Components
     * A description of the component ObjectPoseObserver.
     *
     * @class ObjectPoseObserver
     * @ingroup Component-ObjectPoseObserver
     * @brief Brief description of class ObjectPoseObserver.
     *
     * Detailed description of class ObjectPoseObserver.
     */
    class ObjectPoseObserver :
        virtual public Observer
        , virtual public objpose::ObjectPoseObserverInterface
        , virtual public armarx::RobotStateComponentPluginUser
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser

    {
    public:
        using RobotState = armarx::RobotStateComponentPluginUser;

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // ObjectPoseTopic interface
    public:
        void reportProviderAvailable(const std::string& providerName, const objpose::ProviderInfo& info, ICE_CURRENT_ARG) override;
        void reportObjectPoses(const std::string& providerName, const objpose::data::ProvidedObjectPoseSeq& objectPoses, ICE_CURRENT_ARG) override;

        // ObjectPoseObserverInterface interface
    public:

        // OBJECT POSES

        objpose::data::ObjectPoseSeq getObjectPoses(ICE_CURRENT_ARG) override;
        objpose::data::ObjectPoseSeq getObjectPosesByProvider(const std::string& providerName, ICE_CURRENT_ARG) override;

        // PROVIDER INFORMATION

        bool hasProvider(const std::string& providerName, ICE_CURRENT_ARG) override;
        objpose::ProviderInfo getProviderInfo(const std::string& providerName, ICE_CURRENT_ARG) override;
        Ice::StringSeq getAvailableProviderNames(ICE_CURRENT_ARG) override;
        objpose::ProviderInfoMap getAvailableProvidersInfo(ICE_CURRENT_ARG) override;


        // REQUESTING

        objpose::observer::RequestObjectsOutput requestObjects(const objpose::observer::RequestObjectsInput& input, ICE_CURRENT_ARG) override;

        // ATTACHING

        objpose::AttachObjectToRobotNodeOutput attachObjectToRobotNode(const objpose::AttachObjectToRobotNodeInput& input, ICE_CURRENT_ARG) override;
        objpose::DetachObjectFromRobotNodeOutput detachObjectFromRobotNode(const objpose::DetachObjectFromRobotNodeInput& input, ICE_CURRENT_ARG) override;
        objpose::DetachAllObjectsFromRobotNodesOutput detachAllObjectsFromRobotNodes(ICE_CURRENT_ARG) override;

        objpose::AgentFramesSeq getAttachableFrames(ICE_CURRENT_ARG) override;


        // Remote GUI
        void createRemoteGuiTab();
        void RemoteGui_update() override;


    protected:

        void onInitObserver() override;
        void onConnectObserver() override;

        void onDisconnectComponent() override;
        void onExitObserver() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:

        void updateProviderInfo(const std::string& providerName, const objpose::ProviderInfo& info);
        void handleProviderUpdate(const std::string& providerName);
        void visProviderUpdate(const std::string& providerName);

        objpose::ObjectPoseProviderPrx getProviderProxy(const std::string& providerName);

        std::optional<simox::OrientedBoxf> getObjectOOBB(const ObjectID& id);


        objpose::ObjectPose* findObjectPose(const std::string& providerName, ObjectID& objectID);
        static objpose::ObjectPose* findObjectPoseByID(objpose::ObjectPoseSeq& objectPoses, const ObjectID& id);


        bool isAttachedCached(objpose::ObjectPose& objectPose) const;
        static objpose::ObjectPose applyAttachment(const objpose::ObjectPose& objectPose, VirtualRobot::RobotPtr robot);

        void toIceWithAttachments(objpose::ObjectPoseSeq& objectPoses, VirtualRobot::RobotPtr agent,
                                  objpose::data::ObjectPoseSeq& result, bool& synchronized);


    private:

        std::mutex dataMutex;

        VirtualRobot::RobotPtr robot;


        objpose::ProviderInfoMap providers;

        std::map<std::string, objpose::ObjectPoseSeq> objectPoses;
        std::map<std::string, int> updateCounters;

        std::map<std::pair<std::string, ObjectID>, objpose::ObjectAttachmentInfo> attachments;


        ObjectFinder objectFinder;
        /// Caches results of attempts to retrieve the OOBB from ArmarXObjects.
        std::map<ObjectID, std::optional<simox::OrientedBoxf>> oobbCache;

        struct Visu
        {
            bool enabled = false;
            bool inGlobalFrame = true;
            float alpha = 1.0;
            bool oobbs = false;
            bool objectFrames = false;
            float objectFramesScale = 1.0;
        };
        Visu visu;

        struct Calibration
        {
            std::string robotNode = "Neck_2_Pitch";
            float offset = 0.0f;
        };
        Calibration calibration;


        struct RemoteGuiTab : RemoteGui::Client::Tab
        {
            RemoteGui::Client::CheckBox visuEnabled;
            RemoteGui::Client::CheckBox visuInGlobalFrame;
            RemoteGui::Client::FloatSlider visuAlpha;
            RemoteGui::Client::CheckBox visuOOBBs;
            RemoteGui::Client::CheckBox visuObjectFrames;
            RemoteGui::Client::FloatSpinBox visuObjectFramesScale;
        };
        RemoteGuiTab tab;

    };

}

#undef ICE_CURRENT_ARG
