armarx_component_set_name("ObjectPoseObserver")


set(COMPONENT_LIBS
    ArmarXCore ArmarXCoreInterfaces
    ArmarXGuiComponentPlugins
    RobotAPIArmarXObjects RobotAPIComponentPlugins
    ArViz

    ${PROJECT_NAME}Interfaces
)

set(SOURCES
    ObjectPoseObserver.cpp

    plugins/ObjectPoseProviderPlugin.cpp
    plugins/ObjectPoseClientPlugin.cpp
    plugins/RequestedObjects.cpp
)
set(HEADERS
    ObjectPoseObserver.h

    plugins/ObjectPoseProviderPlugin.h
    plugins/ObjectPoseClientPlugin.h
    plugins/RequestedObjects.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")


# add unit tests
add_subdirectory(test)

# generate the application
armarx_generate_and_add_component_executable()
