/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemGlobalStorage
 * @author     fabian.peller-konrad@kit.edu ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArMemGlobalStorage.h"

// STD/STL
#include <algorithm>

// Boost
#include <boost/asio.hpp>

// Simox
#include <SimoxUtility/algorithm/string.h>

// ArmarX
#include <RobotAPI/interface/armem/legacy.h>

namespace armarx
{
    namespace armem
    {

        armarx::PropertyDefinitionsPtr ArMemGlobalStorage::createPropertyDefinitions()
        {
            PropertyDefinitionsPtr defs{new ComponentPropertyDefinitions{getConfigIdentifier()}};

            defs->optional(local_memory_names, "LocalMemoryNames", "The objectnames of the local memories, comma separated");
            return defs;
        }

        ArMemGlobalStorage::ArMemGlobalStorage():
            armarx::Component(),
            armarx::armem::ArMemBase(),
            armarx::armem::ArMemGlobalMemoryResolver()
        {
        }

        std::string ArMemGlobalStorage::getDefaultName() const
        {
            return "ArMemGlobalStorage";
        }


        void ArMemGlobalStorage::onInitComponent()
        {
            for (const std::string& name : simox::alg::split(local_memory_names, ","))
            {
                usingProxy(GenerateLocalMemoryObjectNameFromHostname(name));
            }
        }


        void ArMemGlobalStorage::onConnectComponent()
        {
            for (const std::string& hostname : simox::alg::split(local_memory_names, ","))
            {
                ArMemLocalMemoryInterfacePrx localMemoryPrx = getProxy<ArMemLocalMemoryInterfacePrx>(GenerateLocalMemoryObjectNameFromHostname(hostname));
                local_memories[hostname] = localMemoryPrx;
            }
        }


        void ArMemGlobalStorage::onDisconnectComponent()
        {

        }


        void ArMemGlobalStorage::onExitComponent()
        {

        }

        std::string ArMemGlobalStorage::getHostnameOfCurrentMachine(const Ice::Current&)
        {
            return getMyHostname();
        }

        ArMemLocalMemoryInterfacePrx ArMemGlobalStorage::getMemoryOfCurrentMachine(const Ice::Current& c)
        {
            return getMemoryForHostname(getMyHostname(), c);
        }

        ArMemLocalMemoryInterfacePrx ArMemGlobalStorage::getMemoryForHostname(const std::string& hostname, const Ice::Current&)
        {
            if (local_memories.find(hostname) == local_memories.end())
            {
                throw LocalException("The local memory of host '" + hostname + "' does not exist!. Could not return proxy!");
            }
            return local_memories[hostname];
        }

        void ArMemGlobalStorage::dynamicallyRegisterNewLocalMemory(const std::string& hostname, const Ice::Current&)
        {
            if (local_memories.find(hostname) == local_memories.end())
            {
                ArMemLocalMemoryInterfacePrx localMemoryPrx = getProxy<ArMemLocalMemoryInterfacePrx>(GenerateLocalMemoryObjectNameFromHostname(hostname));
                local_memories[hostname] = localMemoryPrx;
            }
        }

        void ArMemGlobalStorage::exportDataOfAllMemoriesToLocation(const std::string&, const Ice::Current&)
        {
            // TODO!! Needs Aron-JSON Export?
        }
    }
}
