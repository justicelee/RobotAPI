/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArMemGlobalStorage
 * @author     fabian.peller-konrad@kit.edu ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <map>

// Aron
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/interface/armem/legacy.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <RobotAPI/libraries/armem/legacy/ArMemBase.h>

namespace armarx
{
    namespace armem
    {
        class ArMemGlobalStorage :
            virtual public armarx::Component,
            virtual public armarx::armem::ArMemBase,
            virtual public armarx::armem::ArMemGlobalMemoryResolver
        {
        public:
            ArMemGlobalStorage();

            std::string getDefaultName() const override;

            std::string getHostnameOfCurrentMachine(const Ice::Current& = Ice::Current());
            ArMemLocalMemoryInterfacePrx getMemoryOfCurrentMachine(const Ice::Current& = Ice::Current());
            ArMemLocalMemoryInterfacePrx getMemoryForHostname(const std::string&, const Ice::Current& = Ice::Current());

            void dynamicallyRegisterNewLocalMemory(const std::string&, const Ice::Current& = Ice::Current());

            void exportDataOfAllMemoriesToLocation(const std::string&, const Ice::Current& = Ice::Current());

        protected:
            void onInitComponent() override;
            void onConnectComponent() override;
            void onDisconnectComponent() override;
            void onExitComponent() override;
            armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        private:
            std::string local_memory_names;
            std::map<std::string, ArMemLocalMemoryInterfacePrx> local_memories;
        };
    }
}
