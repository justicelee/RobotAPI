/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::KITProstheticHandUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <RobotAPI/components/units/HandUnit.h>
#include <RobotAPI/drivers/KITProstheticHandDriver/BLEProthesisInterface.h>

namespace armarx
{
    /**
     * @class KITProstheticHandUnitPropertyDefinitions
     * @brief
     */
    class KITProstheticHandUnitPropertyDefinitions:
        public armarx::HandUnitPropertyDefinitions
    {
    public:
        KITProstheticHandUnitPropertyDefinitions(std::string prefix):
            armarx::HandUnitPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("MAC", "The KIT Prosthetic Hand's Bluetooth MAC");
            defineOptionalProperty<std::string>("RemoteGuiName", "", "Name of the remote gui");
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the debug observer that should be used");
        }
    };

    /**
     * @defgroup Component-KITProstheticHandUnit KITProstheticHandUnit
     * @ingroup RobotAPI-Components
     * A description of the component KITProstheticHandUnit.
     *
     * @class KITProstheticHandUnit
     * @ingroup Component-KITProstheticHandUnit
     * @brief Brief description of class KITProstheticHandUnit.
     *
     * Detailed description of class KITProstheticHandUnit.
     */
    class KITProstheticHandUnit :
        virtual public armarx::HandUnit
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "KITProstheticHandUnit";
        }

        void onInitHandUnit() override;
        void onStartHandUnit() override;
        void onExitHandUnit() override;
        void setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current& = Ice::emptyCurrent) override;
        NameValueMap getCurrentJointValues(const Ice::Current&) override;

        void addShape(const std::string& name, const std::map<std::string, float>& shape);
        void addShapeName(const std::string& name);


        void setShape(const std::string& shapeName, const Ice::Current& c = Ice::emptyCurrent) override;

    protected:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
    private:
        std::unique_ptr<BLEProthesisInterface> _driver;
        std::map<std::string, std::map<std::string, float>> _shapes;

        DebugObserverInterfacePrx _debugObserver;
        //gui
        RemoteGuiInterfacePrx _remoteGuiPrx;
        SimplePeriodicTask<>::pointer_type _guiTask;
        RemoteGui::TabProxy _guiTab;
        float _lastGuiValueThumb{0};
        float _lastGuiValueFingers{0};
    };
}
