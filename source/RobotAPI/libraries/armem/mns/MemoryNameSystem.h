#pragma once

#include <condition_variable>
#include <mutex>

#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/interface/armem/MemoryInterface.h>
#include <RobotAPI/interface/armem/MemoryNameSystemInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include "../core/Time.h"


namespace armarx::armem
{

    class MemoryNameSystem : armarx::Logging
    {
    public:

        MemoryNameSystem(const std::string& logTag = "MemoryNameSystem");


        /**
         * @brief Register a new memory or update an existing entry.
         *
         * Causes threads waiting in `waitForMemory()` to resume if the respective
         * memory was added.
         */
        data::RegisterMemoryResult registerMemory(const data::RegisterMemoryInput& input);
        /**
         * @brief Remove a memory entry.
         */
        data::RemoveMemoryResult removeMemory(const data::RemoveMemoryInput& input);

        /**
         * @brief Gets a memory entry, if it is available.
         */
        data::ResolveMemoryNameResult resolveMemoryName(const data::ResolveMemoryNameInput& input);

        /**
         * @brief Blocks until the specified memory is available, returning its proxy.
         */
        data::WaitForMemoryResult waitForMemory(const data::WaitForMemoryInput& input);


        /// Indicates whether a memory entry for that name exists.
        bool hasMemory(const std::string& memoryName) const;

        /// Builds a RemoteGui grid containing information about registered memories.
        armarx::RemoteGui::Client::GridLayout RemoteGui_buildInfoGrid();


    public:

        /// Information about a memory entry.
        struct MemoryInfo
        {
            std::string name;
            MemoryInterfacePrx proxy;
            Time timeRegistered;
        };

        /// The registered memories.
        std::map<std::string, MemoryInfo> memoryMap;


        /// Mutex for `waitForMemoryCond`.
        std::mutex waitForMemoryMutex;
        /// Condition variable used by `waitForMemory()`.
        std::condition_variable waitForMemoryCond;

    };

}
