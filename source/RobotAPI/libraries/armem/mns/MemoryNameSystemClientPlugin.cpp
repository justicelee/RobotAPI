#include "MemoryNameSystemClientPlugin.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error.h"



namespace armarx::armem::plugins
{

    MemoryNameSystemClientPlugin::~MemoryNameSystemClientPlugin()
    {}


    void MemoryNameSystemClientPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(PROPERTY_MNS_NAME_NAME)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(PROPERTY_MNS_NAME_NAME),
                PROPERTY_MNS_NAME_DEFAULT,
                "Name of the Memory Name System (MNS) component.");
        }
        if (!properties->hasDefinition(makePropertyName(PROPERTY_MNS_ENABLED_NAME)))
        {
            properties->defineOptionalProperty<bool>(
                makePropertyName(PROPERTY_MNS_ENABLED_NAME),
                PROPERTY_MNS_ENABLED_DEFAULT,
                "Whether to use (and depend on) the Memory Name System (MNS)."
                "\nSet to false to use this memory as a stand-alone.");
        }
    }


    void MemoryNameSystemClientPlugin::preOnInitComponent()
    {
        if (isMemoryNameSystemEnabled())
        {
            parent().usingProxy(getMemoryNameSystemName());
        }
    }

    void MemoryNameSystemClientPlugin::preOnConnectComponent()
    {
        if (isMemoryNameSystemEnabled())
        {
            parent<MemoryNameSystemClientPluginUser>().memoryNameSystem = getMemoryNameSystem();
        }
    }


    bool MemoryNameSystemClientPlugin::isMemoryNameSystemEnabled()
    {
        return parentDerives<Component>() ?
               parent<Component>().getProperty<bool>(makePropertyName(PROPERTY_MNS_ENABLED_NAME)) :
               PROPERTY_MNS_ENABLED_DEFAULT;
    }

    std::string MemoryNameSystemClientPlugin::getMemoryNameSystemName()
    {
        return parentDerives<Component>() ?
               parent<Component>().getProperty<std::string>(makePropertyName(PROPERTY_MNS_NAME_NAME)) :
               std::string{PROPERTY_MNS_NAME_DEFAULT};
    }

    MemoryNameSystemInterfacePrx MemoryNameSystemClientPlugin::getMemoryNameSystem()
    {
        return isMemoryNameSystemEnabled() && parentDerives<Component>()
               ? parent<Component>().getProxy<MemoryNameSystemInterfacePrx>(getMemoryNameSystemName())
               : nullptr;
    }

}


namespace armarx::armem
{

}
