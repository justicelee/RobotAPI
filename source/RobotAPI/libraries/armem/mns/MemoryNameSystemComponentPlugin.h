#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/armem/MemoryInterface.h>
#include <RobotAPI/interface/armem/MemoryNameSystemInterface.h>

#include "MemoryNameSystem.h"


namespace armarx::armem::plugins
{

    class MemoryNameSystemComponentPlugin : public ComponentPlugin
    {
    public:

        using ComponentPlugin::ComponentPlugin;


    };

}


namespace armarx::armem
{

    /**
     * @brief Utility for connecting a Memory to Ice.
     */
    class MemoryNameSystemComponentPluginUser :
        virtual public ManagedIceObject
        , virtual public MemoryNameSystemInterface
    {
    public:

        MemoryNameSystemComponentPluginUser();

        // MemoryNameSystemInterface interface
    public:
        armem::data::RegisterMemoryResult registerMemory(const armem::data::RegisterMemoryInput& input, const Ice::Current& = Ice::emptyCurrent) override;
        armem::data::RemoveMemoryResult removeMemory(const armem::data::RemoveMemoryInput& input, const Ice::Current& = Ice::emptyCurrent) override;
        armem::data::ResolveMemoryNameResult resolveMemoryName(const armem::data::ResolveMemoryNameInput& input, const Ice::Current& = Ice::emptyCurrent) override;
        armem::data::WaitForMemoryResult waitForMemory(const armem::data::WaitForMemoryInput& input, const Ice::Current& = Ice::emptyCurrent) override;


    public:


        std::mutex mnsMutex;
        armem::MemoryNameSystem mns;


    private:

        armem::plugins::MemoryNameSystemComponentPlugin* plugin = nullptr;


    };


}
