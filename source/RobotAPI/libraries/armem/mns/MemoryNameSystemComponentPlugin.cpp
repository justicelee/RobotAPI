#include "MemoryNameSystemComponentPlugin.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/CycleUtil.h>

#include "../error.h"


namespace armarx::armem::plugins
{

}


namespace armarx::armem
{

    MemoryNameSystemComponentPluginUser::MemoryNameSystemComponentPluginUser()
    {
        addPlugin(plugin);
    }


    armem::data::RegisterMemoryResult MemoryNameSystemComponentPluginUser::registerMemory(const armem::data::RegisterMemoryInput& input, const Ice::Current&)
    {
        std::scoped_lock lock(mnsMutex);
        armem::data::RegisterMemoryResult result = mns.registerMemory(input);
        return result;
    }


    armem::data::RemoveMemoryResult MemoryNameSystemComponentPluginUser::removeMemory(const armem::data::RemoveMemoryInput& input, const Ice::Current&)
    {
        std::scoped_lock lock(mnsMutex);
        armem::data::RemoveMemoryResult result = mns.removeMemory(input);
        return result;
    }


    armem::data::ResolveMemoryNameResult MemoryNameSystemComponentPluginUser::resolveMemoryName(const armem::data::ResolveMemoryNameInput& input, const Ice::Current&)
    {
        std::scoped_lock lock(mnsMutex);
        armem::data::ResolveMemoryNameResult result = mns.resolveMemoryName(input);
        return result;
    }


    armem::data::WaitForMemoryResult MemoryNameSystemComponentPluginUser::waitForMemory(const armem::data::WaitForMemoryInput& input, const Ice::Current&)
    {
        // No lock - this call blocks internally.
        return mns.waitForMemory(input);
    }

}
