#include "MemoryNameSystem.h"


namespace armarx::armem
{

    MemoryNameSystem::MemoryNameSystem(const std::string& logTag)
    {
        armarx::Logging::setTag(logTag);
    }


    armem::data::RegisterMemoryResult MemoryNameSystem::registerMemory(const armem::data::RegisterMemoryInput& input)
    {
        armem::data::RegisterMemoryResult result;

        if (!input.proxy)
        {
            result.success = false;
            std::stringstream ss;
            ss << "Could not register the memory '" << input.name << "'."
               << "\nGiven proxy is null."
               << "\nIf you want to remove a memory, use `removeMemory()`.";
            result.errorMessage = ss.str();
            return result;
        }

        auto it = memoryMap.find(input.name);
        if (it == memoryMap.end())
        {
            it = memoryMap.emplace(input.name, MemoryInfo{}).first;
        }
        else if (!input.existOk)
        {
            result.success = false;
            std::stringstream ss;
            ss << "Could not register the memory '" << input.name << "'."
               << "\nMemory '" << input.name << "' is already registered. "
               << "\nIf this is ok, set 'existOk' to true when registering the memory.";
            result.errorMessage = ss.str();
            return result;
        }

        MemoryInfo& info = it->second;
        info.name = input.name;
        info.proxy = input.proxy;
        info.timeRegistered = armem::Time::now();
        ARMARX_DEBUG << "Registered memory '" << info.name << "'.";

        {
            std::unique_lock lock(waitForMemoryMutex);
            waitForMemoryCond.notify_all();
        }

        result.success = true;
        return result;
    }


    armem::data::RemoveMemoryResult MemoryNameSystem::removeMemory(const armem::data::RemoveMemoryInput& input)
    {
        armem::data::RemoveMemoryResult result;

        if (auto it = memoryMap.find(input.name); it != memoryMap.end())
        {
            result.success = true;
            memoryMap.erase(it);

            ARMARX_DEBUG << "Removed memory '" << input.name << "'.";
        }
        else if (!input.notExistOk)
        {
            result.success = false;
            std::stringstream ss;
            ss << "Could not remove the memory '" << input.name << "."
               << "\nMemory '" << input.name << "' is not registered. "
               << "\nIf this is ok, set 'notExistOk' to true when removing the memory.";
            result.errorMessage = ss.str();
        }

        return result;
    }


    armem::data::ResolveMemoryNameResult MemoryNameSystem::resolveMemoryName(const armem::data::ResolveMemoryNameInput& input)
    {
        armem::data::ResolveMemoryNameResult result;
        try
        {
            MemoryInfo& info = memoryMap.at(input.name);

            result.success = true;
            result.proxy = info.proxy;

            ARMARX_DEBUG << "Resolved memory name '" << input.name << "'.";
        }
        catch (const std::out_of_range&)
        {
            result.success = false;
            std::stringstream ss;
            ss << "Could not resolve the memory name '" << input.name << "'."
               << "\nMemory '" << input.name << "' is not registered.";
            result.errorMessage = ss.str();
        }

        return result;
    }


    data::WaitForMemoryResult MemoryNameSystem::waitForMemory(const data::WaitForMemoryInput& input)
    {
        data::ResolveMemoryNameInput resInput;
        resInput.name = input.name;

        Time start = Time::now();
        data::ResolveMemoryNameResult resResult;
        {
            std::unique_lock lock(waitForMemoryMutex);
            auto pred = [this, resInput, &resResult]()
            {
                resResult = resolveMemoryName(resInput);
                return resResult.success;
            };
            if (input.timeoutMilliSeconds >= 0)
            {
                waitForMemoryCond.wait_for(lock, std::chrono::milliseconds(input.timeoutMilliSeconds), pred);
            }
            else
            {
                waitForMemoryCond.wait(lock, pred);
            }
        }

        armem::data::WaitForMemoryResult result;
        result.success = resResult.success;

        if (resResult.success)
        {
            ARMARX_CHECK(resResult.proxy);
            result.proxy = resResult.proxy;
        }
        else
        {
            ARMARX_CHECK((Time::now() - start).toMilliSeconds() > input.timeoutMilliSeconds)
                    << (Time::now() - start).toMilliSeconds() << " > " << input.timeoutMilliSeconds;

            std::stringstream ss;
            ss << "Timeout (" << input.timeoutMilliSeconds << " ms) while waiting for memory '" << input.name << "'.";
            if (resResult.errorMessage.size() > 0)
            {
                ss << "\n" << resResult.errorMessage;
            }
            result.errorMessage = ss.str();
        }

        return result;
    }


    bool MemoryNameSystem::hasMemory(const std::string& memoryName) const
    {
        return memoryMap.count(memoryName) > 0;
    }


    armarx::RemoteGui::Client::GridLayout MemoryNameSystem::RemoteGui_buildInfoGrid()
    {
        using namespace armarx::RemoteGui::Client;

        GridLayout grid;

        int row = 0;
        grid.add(Label("Memory Name"), {row, 0})
        .add(Label("Component Name"), {row, 1})
        .add(Label("Registration Time"), {row, 2})
        ;
        row++;

        for (const auto& [name, info] : memoryMap)
        {
            ARMARX_CHECK_EQUAL(name, info.name);
            grid
            .add(Label(name), {row, 0})
            .add(Label(info.proxy->ice_getIdentity().name), {row, 1})
            .add(Label(armem::toDateTimeMilliSeconds(info.timeRegistered, 0)), {row, 2})
            ;

            row++;
        }

        return grid;
    }

}

