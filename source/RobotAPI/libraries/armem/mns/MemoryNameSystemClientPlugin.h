#pragma once

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/armem/MemoryNameSystemInterface.h>


namespace armarx::armem
{
    class MemoryNameSystemClientPluginUser;
}


namespace armarx::armem::plugins
{

    /**
     * @brief A base plugin offering optional access and dependency
     * to the Memory Name System (MNS).
     */
    class MemoryNameSystemClientPlugin : public ComponentPlugin
    {
    public:

        using ComponentPlugin::ComponentPlugin;
        virtual ~MemoryNameSystemClientPlugin() override;


        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        void preOnInitComponent() override;
        void preOnConnectComponent() override;


        /**
         * @brief Indicate whether the Memory Name System (MNS) is enabled.
         */
        bool isMemoryNameSystemEnabled();
        /**
         * @brief Get the name of the MNS component.
         */
        std::string getMemoryNameSystemName();

        /**
         * @brief Get the MNS proxy.
         * @return The MNS proxy when MNS is enabled, nullptr when MNS is disabled.
         */
        MemoryNameSystemInterfacePrx getMemoryNameSystem();


    private:

        static constexpr const char* PROPERTY_MNS_ENABLED_NAME = "mns.MemoryNameSystemEnabled";
        static constexpr const bool PROPERTY_MNS_ENABLED_DEFAULT = true;

        static constexpr const char* PROPERTY_MNS_NAME_NAME = "mns.MemoryNameSystemName";
        static constexpr const char* PROPERTY_MNS_NAME_DEFAULT = "ArMemMemoryNameSystem";

    };


    /**
     * @brief Base class for users of the `MemoryNameSystemClientPlugin`.
     * This is itself not a usable plugin user (hence still in the plugins namespace).
     */
    class MemoryNameSystemClientPluginUser
    {
    public:

        /// Only set when enabled.
        MemoryNameSystemInterfacePrx memoryNameSystem = nullptr;

    };


}
