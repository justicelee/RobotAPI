#pragma once

#include <RobotAPI/interface/armem/MemoryInterface.h>

#include "../memory/Memory.h"


namespace armarx::armem
{


    /**
     * @brief Helps connecting a Memory to Ice.
     *
     * This involves conversion of ice types to C++ types as well as
     * catchin exceptions and converting them to error messages
     */
    class IceMemory
    {
    public:

        /// Construct an IceMemory from an existing Memory.
        IceMemory(Memory* memory = nullptr);


        armem::data::AddSegmentResult addSegment(
            const armem::data::AddSegmentInput& input, bool addCoreSegments = false);

        armem::data::AddSegmentsResult addSegments(
            const armem::data::AddSegmentsInput& input, bool addCoreSegments = false);


        armem::data::CommitResult commit(const armem::data::Commit& commitIce);
        armem::CommitResult commit(const armem::InternalCommit& commit);


    public:

        Memory* memory;


    };


}
