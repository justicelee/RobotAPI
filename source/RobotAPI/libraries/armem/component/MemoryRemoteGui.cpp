#include "MemoryRemoteGui.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem
{

    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const Memory& memory) const
    {
        GroupBox group;
        group.setLabel("Memory '" + memory.name + "'");

        for (const auto& [name, coreSegment] : memory.coreSegments)
        {
            group.addChild(makeGroupBox(*coreSegment));
        }

        return group;
    }



    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const CoreSegment& coreSegment) const
    {
        GroupBox group;
        group.setLabel("Core Segment '" + coreSegment.name + "'");

        for (const auto& [name, providerSegment] : coreSegment.providerSegments)
        {
            group.addChild(makeGroupBox(*providerSegment));
        }

        return group;
    }



    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const ProviderSegment& providerSegment) const
    {
        GroupBox group;
        group.setLabel("Provider Segment '" + providerSegment.name + "'");

        for (const auto& [name, entity] : providerSegment.entities)
        {
            group.addChild(makeGroupBox(*entity));
        }

        return group;
    }



    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const Entity& entity) const
    {
        GroupBox group;
        group.setLabel("Entity '" + entity.name + "'");

        if (int(entity.history.size()) <= maxHistorySize)
        {
            for (const auto& [time, snapshot] : entity.history)
            {
                group.addChild(makeGroupBox(*snapshot));
            }
        }
        else
        {
            int margin = 2;
            auto it = entity.history.begin();
            auto rit = entity.history.end();
            --rit;
            for (int i = 0; i < margin; ++i, ++it)
            {
                group.addChild(makeGroupBox(*it->second));
                --rit;
            }
            group.addChild(Label("..."));
            for (; rit != entity.history.end(); ++rit)
            {
                group.addChild(makeGroupBox(*rit->second));
            }
        }

        return group;
    }



    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const EntitySnapshot& snapshot) const
    {
        GroupBox group;
        std::stringstream ss;
        ss << "t = " << armem::toDateTimeMilliSeconds(snapshot.time);
        group.setLabel(ss.str());

        for (const auto& instance : snapshot.instances)
        {
            std::stringstream ss;
            ss << "Instance #" << instance->index;
            Label instanceLabel(ss.str());
            group.addChild(instanceLabel);
        }

        return group;
    }

}
