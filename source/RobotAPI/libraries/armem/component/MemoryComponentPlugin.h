#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/armem/MemoryInterface.h>
#include <RobotAPI/interface/armem/MemoryNameSystemInterface.h>

#include "../memory/Memory.h"
#include "../mns/MemoryNameSystemClientPlugin.h"
#include "IceMemory.h"


namespace armarx::armem
{
    class MemoryComponentPluginUser;
}


namespace armarx::armem::plugins
{

    class MemoryComponentPlugin : public MemoryNameSystemClientPlugin
    {
    public:

        using MemoryNameSystemClientPlugin::MemoryNameSystemClientPlugin;
        virtual ~MemoryComponentPlugin() override;


        void postOnConnectComponent() override;
        void preOnDisconnectComponent() override;


        /**
         * @brief Register the parent component in the MNS.
         * Called before onConnect() if MNS is enabled.
         */
        data::RegisterMemoryResult registerMemory(MemoryComponentPluginUser& parent);
        /**
         * @brief Remove the parent component from the MNS.
         * Called before onDisconnect() if MNS is enabled.
         */
        data::RemoveMemoryResult removeMemory(MemoryComponentPluginUser& parent);

    };

}


namespace armarx::armem
{

    /**
     * @brief Utility for connecting a Memory component to Ice.
     */
    class MemoryComponentPluginUser :
        virtual public ManagedIceObject
        , virtual public MemoryInterface
        , virtual public plugins::MemoryNameSystemClientPluginUser
    {
    public:

        MemoryComponentPluginUser();
        virtual ~MemoryComponentPluginUser() override = default;


        // WritingInterface interface
        virtual armem::data::AddSegmentsResult addSegments(const armem::data::AddSegmentsInput& input, const Ice::Current& = Ice::emptyCurrent) override;
        armem::data::AddSegmentsResult addSegments(const armem::data::AddSegmentsInput& input, bool addCoreSegments);

        virtual armem::data::CommitResult commit(const armem::data::Commit& commit, const Ice::Current& = Ice::emptyCurrent) override;


        // ReadingInterface interface
        virtual armem::data::EntitySnapshotQueryResultList getEntitySnapshots(const armem::data::EntitySnapshotQueryList&, const Ice::Current& = Ice::emptyCurrent) override;


    public:

        std::mutex memoryMutex;

        /// The actual memory.
        Memory memory;

        /// Helps connecting `memory` to ice. Used to handle Ice callbacks.
        IceMemory iceMemory { &memory };


    private:

        armem::plugins::MemoryComponentPlugin* plugin = nullptr;


    };


}
