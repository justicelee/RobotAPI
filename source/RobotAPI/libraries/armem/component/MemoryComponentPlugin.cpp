#include "MemoryComponentPlugin.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error.h"

#include "IceMemory.h"


namespace armarx::armem::plugins
{

    MemoryComponentPlugin::~MemoryComponentPlugin()
    {}

    void MemoryComponentPlugin::postOnConnectComponent()
    {
        MemoryComponentPluginUser& parent = this->parent<MemoryComponentPluginUser>();
        if (isMemoryNameSystemEnabled() && parent.memoryNameSystem)
        {
            registerMemory(parent);
        }
    }


    void MemoryComponentPlugin::preOnDisconnectComponent()
    {
        MemoryComponentPluginUser& parent = this->parent<MemoryComponentPluginUser>();
        if (isMemoryNameSystemEnabled() && parent.memoryNameSystem)
        {
            removeMemory(parent);
        }
    }


    data::RegisterMemoryResult MemoryComponentPlugin::registerMemory(MemoryComponentPluginUser& parent)
    {
        data::RegisterMemoryInput input;
        input.name = parent.memory.name;
        input.proxy = MemoryInterfacePrx::checkedCast(parent.getProxy());
        ARMARX_CHECK_NOT_NULL(input.proxy);
        data::RegisterMemoryResult result = parent.memoryNameSystem->registerMemory(input);
        if (result.success)
        {
            ARMARX_DEBUG << "Registered memory '" << input.name << "' in the Memory Name System (MNS).";
        }
        else
        {
            ARMARX_WARNING << "Failed to register this memory in the Memory Name System (MNS):"
                           << "\n" << result.errorMessage;
        }
        return result;
    }


    data::RemoveMemoryResult MemoryComponentPlugin::removeMemory(MemoryComponentPluginUser& parent)
    {
        data::RemoveMemoryResult result;
        try
        {
            data::RemoveMemoryInput input;
            input.name = parent.memory.name;
            result = parent.memoryNameSystem->removeMemory(input);
            if (result.success)
            {
                ARMARX_DEBUG << "Removed memory '" << input.name << "' from the Memory Name System (MNS).";
            }
            else
            {
                ARMARX_WARNING << "Failed to remove this memory in the Memory Name System (MNS):"
                               << "\n" << result.errorMessage;
            }
        }
        catch (const Ice::NotRegisteredException&)
        {
            // It's ok, the MNS is gone.
            result.success = false;
            result.errorMessage = "Memory Name System is gone.";
        }
        return result;
    }

}


namespace armarx::armem
{

    MemoryComponentPluginUser::MemoryComponentPluginUser()
    {
        addPlugin(plugin);
    }


    // WRITING

    armem::data::AddSegmentsResult MemoryComponentPluginUser::addSegments(const armem::data::AddSegmentsInput& input, const Ice::Current&)
    {
        bool addCoreSegmentOnUsage = false;
        return addSegments(input, addCoreSegmentOnUsage);
    }

    armem::data::AddSegmentsResult MemoryComponentPluginUser::addSegments(const armem::data::AddSegmentsInput& input, bool addCoreSegments)
    {
        std::scoped_lock lock(memoryMutex);
        armem::data::AddSegmentsResult result = iceMemory.addSegments(input, addCoreSegments);
        return result;
    }


    armem::data::CommitResult MemoryComponentPluginUser::commit(const armem::data::Commit& commitIce, const Ice::Current&)
    {
        std::scoped_lock lock(memoryMutex);
        return iceMemory.commit(commitIce);
    }



    // READING

    armem::data::EntitySnapshotQueryResultList MemoryComponentPluginUser::getEntitySnapshots(
        const armem::data::EntitySnapshotQueryList& queries, const Ice::Current&)
    {
        std::scoped_lock lock(memoryMutex);

        armem::data::EntitySnapshotQueryResultList results;

        for (const auto& query : queries)
        {
            armem::data::EntitySnapshotQueryResult& result = results.emplace_back();
            armem::MemoryID id = armem::MemoryID::fromString(query.snapshotID);

            try
            {
                armem::EntitySnapshot& snapshot = memory.getEntitySnapshot(id);
                result.success = true;
                result.timeCreatedMicroSeconds = snapshot.time.toMicroSeconds();
                for (const auto& instance : snapshot.instances)
                {
                    result.instances.push_back(instance->data);
                }
            }
            catch (const armem::error::ArMemError& e)
            {
                result.success = false;
                result.errorMessage = e.what();
            }
        }

        return results;
    }


}
