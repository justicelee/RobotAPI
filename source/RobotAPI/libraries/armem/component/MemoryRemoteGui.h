#pragma once

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include "../memory/Memory.h"


namespace armarx::armem
{

    /**
     * @brief Utility for memory Remote Guis.
     */
    class MemoryRemoteGui
    {
    public:
        using GroupBox = armarx::RemoteGui::Client::GroupBox;
        using Label = armarx::RemoteGui::Client::Label;

        GroupBox makeGroupBox(const Memory& memory) const;
        GroupBox makeGroupBox(const CoreSegment& coreSegment) const;
        GroupBox makeGroupBox(const ProviderSegment& providerSegment) const;
        GroupBox makeGroupBox(const Entity& entity) const;
        GroupBox makeGroupBox(const EntitySnapshot& entitySnapshot) const;



        int maxHistorySize = 10;

    };


}
