#include "IceMemory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error.h"
#include "../memory/ice_conversions.h"


namespace armarx::armem
{

    IceMemory::IceMemory(Memory* memory) : memory(memory)
    {
    }


    armem::data::AddSegmentResult IceMemory::addSegment(const armem::data::AddSegmentInput& input, bool addCoreSegments)
    {
        ARMARX_DEBUG << "Adding segment '" << input.coreSegmentName << "/" << input.providerSegmentName << "'.";
        ARMARX_CHECK_NOT_NULL(memory);

        armem::data::AddSegmentResult output;

        armem::CoreSegment* coreSegment = nullptr;
        try
        {
            coreSegment = &memory->getCoreSegment(input.coreSegmentName);
        }
        catch (const armem::error::MissingEntry& e)
        {
            if (addCoreSegments)
            {
                coreSegment = &memory->addCoreSegment(input.coreSegmentName);
            }
            else
            {
                output.success = false;
                output.errorMessage = e.what();
                return output;
            }
        }
        ARMARX_CHECK_NOT_NULL(coreSegment);

        if (input.providerSegmentName.size() > 0)
        {
            try
            {
                coreSegment->addProviderSegment(input.providerSegmentName);
            }
            catch (const armem::error::StorageEntryAlreadyExists&)
            {
                // This is ok.
                if (input.clearWhenExists)
                {
                    ProviderSegment& provider = coreSegment->getProviderSegment(input.providerSegmentName);
                    provider.clear();
                }
            }
        }

        armem::MemoryID segmentID;
        segmentID.memoryName = memory->name;
        segmentID.coreSegmentName = input.coreSegmentName;
        segmentID.providerSegmentName = input.providerSegmentName;

        output.success = true;
        output.segmentID = segmentID.str();
        return output;
    }


    armem::data::AddSegmentsResult IceMemory::addSegments(const armem::data::AddSegmentsInput& input, bool addCoreSegments)
    {
        ARMARX_CHECK_NOT_NULL(memory);

        armem::data::AddSegmentsResult output;
        for (const auto& i : input)
        {
            output.push_back(addSegment(i, addCoreSegments));
        }
        return output;
    }


    armem::data::CommitResult IceMemory::commit(const armem::data::Commit& commitIce)
    {
        ARMARX_CHECK_NOT_NULL(memory);

        armem::Time timeArrived = armem::Time::now();

        armem::InternalCommit commit;
        armem::fromIce(commitIce, commit, timeArrived);

        armem::CommitResult result = this->commit(commit);
        armem::data::CommitResult resultIce;
        armem::toIce(resultIce, result);

        return resultIce;
    }


    armem::CommitResult IceMemory::commit(const armem::InternalCommit& commit)
    {
        armem::CommitResult commitResult;
        for (const auto& update : commit.updates)
        {
            armem::EntityUpdateResult& result = commitResult.results.emplace_back();
            try
            {
                armem::MemoryID snapshotID = memory->update(update);

                result.success = true;
                result.snapshotID = snapshotID;
                result.timeArrived = update.timeArrived;
            }
            catch (const armem::error::ArMemError& e)
            {
                result.success = false;
                result.errorMessage = e.what();
            }
        }
        return commitResult;
    }

}
