/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include "../memory/Memory.h"
#include "../error/ArMemError.h"

#include <iostream>


namespace armem = armarx::armem;
namespace aron = armarx::aron;


BOOST_AUTO_TEST_CASE(test_time_to_string)
{
    // 1: seconds, 2: milliseconds, 3: microseconds
    armem::Time time = armem::Time::microSeconds(111111345789);

    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time), "111111345.789 ms");
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time, 0), "111111345 ms");
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time, 1), "111111345.7 ms");
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time, 2), "111111345.78 ms");
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time, 3), "111111345.789 ms");

    BOOST_CHECK_EQUAL(armem::toStringMicroSeconds(time), "111111345789 " "\u03BC" "s");

    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time), "1970-01-02 07:51:51.345");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 0), "1970-01-02 07:51:51");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 3), "1970-01-02 07:51:51.345");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 6), "1970-01-02 07:51:51.345789");
}


BOOST_AUTO_TEST_CASE(test_segment_setup)
{
    armem::InternalEntityUpdate update;

    armem::MemoryPtr memory = std::make_unique<armem::Memory>("Memory");
    BOOST_CHECK_EQUAL(memory->name, "Memory");
    {
        update.entityID = armem::MemoryID::fromString("OtherMemory/SomeSegment");
        BOOST_CHECK_THROW(memory->update(update), armem::error::StorageNameMismatch);
        update.entityID = armem::MemoryID::fromString("Memory/MissingSegment");
        BOOST_CHECK_THROW(memory->update(update), armem::error::MissingEntry);
    }

    armem::CoreSegment& coreSegment = memory->addCoreSegment("ImageRGB");
    BOOST_CHECK_EQUAL(coreSegment.name, "ImageRGB");
    BOOST_CHECK(memory->hasCoreSegment(coreSegment.name));
    {
        update.entityID = armem::MemoryID::fromString("Memory/OtherCoreSegment");
        BOOST_CHECK_THROW(coreSegment.update(update), armem::error::StorageNameMismatch);
        update.entityID = armem::MemoryID::fromString("Memory/ImageRGB/MissingProvider");
        BOOST_CHECK_THROW(coreSegment.update(update), armem::error::MissingEntry);
    }

    armem::ProviderSegment& providerSegment = coreSegment.addProviderSegment("SomeRGBImageProvider");
    BOOST_CHECK_EQUAL(providerSegment.name, "SomeRGBImageProvider");
    BOOST_CHECK(coreSegment.hasProviderSegment(providerSegment.name));
    {
        update.entityID = armem::MemoryID::fromString("Memory/ImageRGB/OtherRGBImageProvider");
        BOOST_CHECK_THROW(providerSegment.update(update), armem::error::StorageNameMismatch);
    }


    // A successful update.

    update.entityID = armem::MemoryID::fromString("Memory/ImageRGB/SomeRGBImageProvider/image");
    update.instancesData = { new aron::data::AronData(), new aron::data::AronData() };
    update.timeCreated = armem::Time::milliSeconds(1000);
    BOOST_CHECK_NO_THROW(providerSegment.update(update));

    BOOST_CHECK_EQUAL(providerSegment.entities.size(), 1);
    BOOST_CHECK(providerSegment.hasEntity("image"));
    BOOST_CHECK(!providerSegment.hasEntity("other_image"));

    armem::Entity& entity = providerSegment.getEntity("image");
    BOOST_CHECK_EQUAL(entity.name, "image");
    BOOST_CHECK_EQUAL(entity.history.size(), 1);
    BOOST_CHECK_EQUAL(entity.history.count(update.timeCreated), 1);

    armem::EntitySnapshot* entitySnapshot = entity.history.at(update.timeCreated).get();
    BOOST_CHECK_EQUAL(entitySnapshot->instances.size(), update.instancesData.size());


    // Another update (on memory).

    update.instancesData = { new aron::data::AronData() };
    update.timeCreated = armem::Time::milliSeconds(2000);
    memory->update(update);
    BOOST_CHECK_EQUAL(entity.history.size(), 2);
    BOOST_CHECK_EQUAL(entity.history.count(update.timeCreated), 1);
    BOOST_CHECK_EQUAL(entity.history.at(update.timeCreated)->instances.size(), update.instancesData.size());


    // A third update (on entity).
    update.instancesData = { new aron::data::AronData() };
    update.timeCreated = armem::Time::milliSeconds(3000);
    entity.update(update);
    BOOST_CHECK_EQUAL(entity.history.size(), 3);

}



BOOST_AUTO_TEST_CASE(test_history_size_in_entity)
{
    armem::EntityPtr entity = std::make_unique<armem::Entity>("entity");

    armem::InternalEntityUpdate update;
    update.entityID.entityName = entity->name;

    // With unlimited history.
    update.timeCreated = armem::Time::milliSeconds(1000);
    entity->update(update);
    update.timeCreated = armem::Time::milliSeconds(2000);
    entity->update(update);
    update.timeCreated = armem::Time::milliSeconds(3000);
    entity->update(update);
    BOOST_CHECK_EQUAL(entity->history.size(), 3);

    // Now with maximum history size.
    entity->setMaxHistorySize(2);
    BOOST_CHECK_EQUAL(entity->history.size(), 2);
    BOOST_CHECK_EQUAL(entity->history.count(armem::Time::milliSeconds(1000)), 0);
    BOOST_CHECK_EQUAL(entity->history.count(armem::Time::milliSeconds(2000)), 1);
    BOOST_CHECK_EQUAL(entity->history.count(armem::Time::milliSeconds(3000)), 1);


    update.timeCreated = armem::Time::milliSeconds(4000);
    entity->update(update);
    BOOST_CHECK_EQUAL(entity->history.size(), 2);
    BOOST_CHECK_EQUAL(entity->history.count(armem::Time::milliSeconds(2000)), 0);
    BOOST_CHECK_EQUAL(entity->history.count(armem::Time::milliSeconds(3000)), 1);
    BOOST_CHECK_EQUAL(entity->history.count(armem::Time::milliSeconds(4000)), 1);

    // Disable maximum history size.
    entity->setMaxHistorySize(-1);

    update.timeCreated = armem::Time::milliSeconds(5000);
    entity->update(update);
    BOOST_CHECK_EQUAL(entity->history.size(), 3);
    BOOST_CHECK_EQUAL(entity->history.count(armem::Time::milliSeconds(3000)), 1);
    BOOST_CHECK_EQUAL(entity->history.count(armem::Time::milliSeconds(4000)), 1);
    BOOST_CHECK_EQUAL(entity->history.count(armem::Time::milliSeconds(5000)), 1);
}


BOOST_AUTO_TEST_CASE(test_history_size_in_provider_segment)
{
    armem::ProviderSegment providerSegment("SomeRGBImageProvider");

    armem::InternalEntityUpdate update;
    update.entityID.providerSegmentName = providerSegment.name;

    std::vector<std::string> entityNames = { "A", "B" };

    // Fill entities and histories with unlimited size.
    for (const auto& name : entityNames)
    {
        update.entityID.entityName = name;

        update.timeCreated = armem::Time::milliSeconds(1000);
        providerSegment.update(update);
        update.timeCreated = armem::Time::milliSeconds(2000);
        providerSegment.update(update);
        update.timeCreated = armem::Time::milliSeconds(3000);
        providerSegment.update(update);
    }
    update.entityID.entityName = entityNames.back();
    update.timeCreated = armem::Time::milliSeconds(4000);
    providerSegment.update(update);

    BOOST_CHECK_EQUAL(providerSegment.getEntity("A").history.size(), 3);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("B").history.size(), 4);


    // Employ maximum history size.
    providerSegment.setMaxHistorySize(3);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("A").history.size(), 3);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("B").history.size(), 3);

    providerSegment.setMaxHistorySize(2);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("A").history.size(), 2);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("B").history.size(), 2);

    providerSegment.setMaxHistorySize(3);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("A").history.size(), 2);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("B").history.size(), 2);

    // Add new entity.
    providerSegment.setMaxHistorySize(2);

    update.entityID.entityName = "C";
    update.timeCreated = armem::Time::milliSeconds(1000);
    providerSegment.update(update);
    update.timeCreated = armem::Time::milliSeconds(2000);
    providerSegment.update(update);
    update.timeCreated = armem::Time::milliSeconds(3000);
    providerSegment.update(update);

    // Check correctly inherited history size.
    BOOST_CHECK_EQUAL(providerSegment.getEntity("C").maxHistorySize, 2);
    // Check actual history size.
    BOOST_CHECK_EQUAL(providerSegment.getEntity("C").history.size(), 2);

    // Remove maximum.
    providerSegment.setMaxHistorySize(-1);

    entityNames.push_back("C");
    for (const auto& name : entityNames)
    {
        update.entityID.entityName = name;
        update.timeCreated = armem::Time::milliSeconds(5000);
        providerSegment.update(update);
        BOOST_CHECK_EQUAL(providerSegment.getEntity(name).history.size(), 3);
    }
}
