#pragma once

#include <map>
#include <memory>
#include <string>

#include "EntityStorage.h"
#include "ProviderSegment.h"


namespace armarx::armem
{

    /**
     * @brief Data of a core segment containing multiple provider segments.
     */
    class CoreSegment : public EntityStorage
    {
    public:

        using EntityStorage::EntityStorage;


        bool hasProviderSegment(const std::string& name) const;
        ProviderSegment& getProviderSegment(const std::string& name);

        Entity& getEntity(const MemoryID& id) override;


        ProviderSegment& addProviderSegment(const std::string& name);

        virtual MemoryID update(const InternalEntityUpdate& update) override;
        using EntityStorage::update;

        void clear() override;


    public:

        std::map<std::string, ProviderSegmentPtr> providerSegments;

    };

    using CoreSegmentPtr = std::unique_ptr<CoreSegment>;

}
