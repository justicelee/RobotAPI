#include "EntityStorage.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error/ArMemError.h"


namespace armarx::armem
{

    EntityStorage::EntityStorage()
    {
    }

    EntityStorage::EntityStorage(const std::string& name) : name(name)
    {
    }

    std::vector<MemoryID> EntityStorage::update(const InternalCommit& commit)
    {
        std::vector<MemoryID> ids;
        for (const auto& update : commit.updates)
        {
            ids.push_back(this->update(update));
        }
        return ids;
    }

    EntitySnapshot& EntityStorage::getEntitySnapshot(const MemoryID& id)
    {
        Entity& entity = getEntity(id);

        if (id.hasTimestamp())
        {
            return entity.getSnapshot(id);
        }
        else
        {
            return entity.getLatestSnapshot();
        }
    }

    void EntityStorage::checkStorageName(const std::string& expectedName, const std::string& actualName,
                                         const std::string& storageTerm)
    {
        if (expectedName != actualName)
        {
            throw armem::error::StorageNameMismatch(expectedName, storageTerm, actualName);
        }
    }
}
