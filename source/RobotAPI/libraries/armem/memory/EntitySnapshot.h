#pragma once

#include <memory>
#include <vector>

#include "../core/Time.h"
#include "../core/MemoryID.h"

#include "EntityData.h"


namespace armarx::armem
{

    /**
     * @brief Data of an entity at one point in time.
     */
    class EntitySnapshot
    {
    public:

        EntitySnapshot() = default;


        void update(const InternalEntityUpdate& update);


        /**
         * @brief Get the given instance.
         * @param index The instance's index.
         * @return The instance.
         * @throw `armem::error::MissingEntry` If the given index is invalid.
         */
        EntityData& getInstance(int index);
        /**
         * @brief Get the given instance.
         * @param index The instance's index.
         * @return The instance.
         * @throw `armem::error::MissingEntry` If the given index is invalid.
         * @throw `armem::error::InvalidMemoryID` If memory ID does not have an instance index.
         */
        EntityData& getInstance(MemoryID id);


    public:

        Time time;
        std::vector<EntityDataPtr> instances;

    };

    using EntitySnapshotPtr = std::unique_ptr<EntitySnapshot>;


}
