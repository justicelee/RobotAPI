#pragma once

#include <map>
#include <memory>
#include <string>

#include "Entity.h"
#include "EntityStorage.h"


namespace armarx::armem
{

    /**
     * @brief Data of a provider segment containing multiple entities.
     */
    class ProviderSegment : public EntityStorage
    {
    public:

        using EntityStorage::EntityStorage;


        bool hasEntity(const std::string& name) const
        {
            return entities.count(name) > 0;
        }
        Entity& getEntity(const std::string& name);
        Entity& getEntity(const MemoryID& id) override;

        /**
         * @brief Updates an entity's history.
         *
         * Missing entity entries are added before updating.
         */
        virtual MemoryID update(const InternalEntityUpdate& update) override;
        using EntityStorage::update;

        virtual void clear() override;


        /**
         * @brief Sets the maximum history size of entities in this segment.
         * This affects all current entities as well as new ones.
         * @see Entity::setMaxHistorySize()
         */
        void setMaxHistorySize(long maxSize);


    public:

        std::map<std::string, EntityPtr> entities;

        /**
         * @brief Maximum size of entity histories.
         * @see Entity::maxHstorySize
         */
        long maxHistorySize = -1;

    };

    using ProviderSegmentPtr = std::unique_ptr<ProviderSegment>;

}
