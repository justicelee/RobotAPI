#pragma once

#include <map>
#include <memory>
#include <string>

#include "../core/Time.h"

#include "EntitySnapshot.h"


namespace armarx::armem
{

    /**
     * @brief Data of an entity over a period of time.
     */
    class Entity
    {
    public:

        Entity();
        Entity(const std::string& name);

        bool hasSnapshot(Time time) const;
        EntitySnapshot& getSnapshot(Time time);
        EntitySnapshot& getSnapshot(const MemoryID& id);

        /**
         * @brief Return the snapshot with the most recent timestamp.
         * @return The latest snapshot.
         */
        EntitySnapshot& getLatestSnapshot();


        /**
         * @brief Add the given update to this entity's history.
         * @param update The update.
         * @return The snapshot ID of the update.
         */
        MemoryID update(const InternalEntityUpdate& update);

        /**
         * @brief Sets the maximum history size.
         *
         * The current history is truncated if necessary.
         */
        void setMaxHistorySize(long maxSize);


    private:

        /// If maximum size is set, ensure `history`'s is not higher.
        void truncateHistoryToSize();
        void checkEntityName(const std::string& name) const;


    public:

        std::string name;
        std::map<Time, EntitySnapshotPtr> history;

        /**
         * @brief Maximum size of `history`
         *
         * If negative, the size of `history` is not limited.
         */
        long maxHistorySize = -1;
        // ToDo: Add max age;
        // ToDo in future: keep/remove predicate

    };

    using EntityPtr = std::unique_ptr<Entity>;

}
