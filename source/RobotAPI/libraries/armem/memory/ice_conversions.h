#pragma once


#include "../core/ice_conversions.h"

#include "InternalCommit.h"


namespace armarx::armem
{

    void fromIce(const data::Commit& ice, InternalCommit& commit, Time timeArrived);

    void fromIce(const data::EntityUpdate& ice, InternalEntityUpdate& update, Time timeArrived);


}
