#include "ProviderSegment.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error/ArMemError.h"


namespace armarx::armem
{

    Entity& ProviderSegment::getEntity(const std::string& name)
    {
        auto it = this->entities.find(name);
        if (it != entities.end())
        {
            ARMARX_CHECK_NOT_NULL(it->second);
            return *it->second;
        }
        else
        {
            throw error::MissingEntry("entity", name, "provider segment", this->name);
        }
    }

    Entity& ProviderSegment::getEntity(const MemoryID& id)
    {
        checkStorageName(id.providerSegmentName, this->name, "provider segment");
        return getEntity(id.entityName);
    }

    MemoryID ProviderSegment::update(const InternalEntityUpdate& update)
    {
        checkStorageName(update.entityID.providerSegmentName, this->name, "provider segment");

        auto it = this->entities.find(update.entityID.providerSegmentName);
        if (it == entities.end())
        {
            // Add entity entry.
            it = entities.emplace(update.entityID.entityName,
                                  std::make_unique<Entity>(update.entityID.entityName)).first;
            it->second->setMaxHistorySize(maxHistorySize);
        }
        // Update entity.
        return it->second->update(update);
    }

    void ProviderSegment::clear()
    {
        entities.clear();
    }

    void ProviderSegment::setMaxHistorySize(long maxSize)
    {
        this->maxHistorySize = maxSize;
        for (auto& [name, entity] : entities)
        {
            entity->setMaxHistorySize(maxSize);
        }
    }

}
