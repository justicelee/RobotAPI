#pragma once

#include <memory>

#include <RobotAPI/interface/aron.h>

#include "../core/Time.h"

#include "InternalCommit.h"


namespace armarx::armem
{

    /**
     * @brief Metadata of an entity instance.
     */
    struct EntityMetadata
    {
        /// Time when this value was created.
        Time timeCreated;
        /// Time when this value was sent to the memory.
        Time timeSent;
        /// Time when this value has arrived at the memory.
        Time timeArrived;

        /// An optional confidence, may be used for things like decay.
        float confidence = 1.0;
    };


    /**
     * @brief Data of a single entity instance.
     */
    struct EntityData
    {
        int index;

        aron::data::AronDataPtr data;
        EntityMetadata metadata;

        /**
         * @brief Fill `*this` with the update's values.
         * @param update The update.
         * @param index The instances index.
         */
        void update(const InternalEntityUpdate& update, int index);

    };

    using EntityDataPtr = std::unique_ptr<EntityData>;

}
