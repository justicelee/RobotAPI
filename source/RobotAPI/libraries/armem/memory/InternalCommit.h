#pragma once

#include <vector>

#include "../core/Commit.h"


namespace armarx::armem
{

    /**
     * @brief An update received by a memory.
     *
     * For use inside a memory.
     */
    struct InternalEntityUpdate : public EntityUpdate
    {
        Time timeArrived = Time::microSeconds(-1);
    };


    struct InternalCommit
    {
        std::vector<InternalEntityUpdate> updates;
    };


}
