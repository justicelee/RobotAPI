#include "ice_conversions.h"


namespace armarx
{

    void armem::fromIce(const data::Commit& ice, InternalCommit& commit, Time timeArrived)
    {
        commit.updates.clear();
        for (const auto& ice_update : ice.updates)
        {
            InternalEntityUpdate& update = commit.updates.emplace_back(InternalEntityUpdate());
            fromIce(ice_update, update, timeArrived);
        }
    }


    void armem::fromIce(const data::EntityUpdate& ice, InternalEntityUpdate& update, Time timeArrived)
    {
        armem::fromIce(ice, update);
        update.timeArrived = timeArrived;
    }


}


