#include "CoreSegment.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error/ArMemError.h"


namespace armarx::armem
{


    bool CoreSegment::hasProviderSegment(const std::string& name) const
    {
        return providerSegments.count(name) > 0;
    }

    ProviderSegment& CoreSegment::getProviderSegment(const std::string& name)
    {
        auto it = this->providerSegments.find(name);
        if (it != providerSegments.end())
        {
            ARMARX_CHECK_NOT_NULL(it->second);
            return *it->second;
        }
        else
        {
            throw armem::error::MissingEntry("provider segment", name, "core segment", this->name);
        }
    }

    Entity& CoreSegment::getEntity(const MemoryID& id)
    {
        checkStorageName(id.coreSegmentName, this->name, "core segment");
        return getProviderSegment(id.providerSegmentName).getEntity(id);
    }


    ProviderSegment& CoreSegment::addProviderSegment(const std::string& name)
    {
        if (hasProviderSegment(name))
        {
            throw armem::error::StorageEntryAlreadyExists("provider segment", name, "core segment", this->name);
        }
        auto it = providerSegments.emplace(name, std::make_unique<ProviderSegment>(name)).first;
        ARMARX_CHECK_NOT_NULL(it->second);
        return *it->second;
    }


    MemoryID CoreSegment::update(const InternalEntityUpdate& update)
    {
        checkStorageName(update.entityID.coreSegmentName, this->name, "core segment");

        auto it = this->providerSegments.find(update.entityID.providerSegmentName);
        if (it != providerSegments.end())
        {
            return it->second->update(update);
        }
        else
        {
            throw armem::error::MissingEntry("provider segment", update.entityID.providerSegmentName,
                                             "core segment", name);
        }
    }

    void CoreSegment::clear()
    {
        providerSegments.clear();
    }

}
