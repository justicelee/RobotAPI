#include "EntityData.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem
{

    void EntityData::update(const InternalEntityUpdate& update, int index)
    {
        ARMARX_CHECK_FITS_SIZE(index, update.instancesData.size());

        this->index = index;
        this->data = update.instancesData.at(size_t(index));

        this->metadata.confidence = update.confidence;

        this->metadata.timeCreated = update.timeCreated;
        this->metadata.timeSent = update.timeSent;
        this->metadata.timeArrived = update.timeArrived;
    }

}
