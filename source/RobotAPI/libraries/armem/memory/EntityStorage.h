#pragma once

#include "InternalCommit.h"

#include "Entity.h"
#include "EntitySnapshot.h"


namespace armarx::armem
{

    /**
     * @brief A container of entities at some point in the hierarchy.
     *
     * Can be updated by multiple entity updates.
     */
    class EntityStorage
    {
    public:

        EntityStorage();
        EntityStorage(const std::string& name);

        virtual ~EntityStorage() = default;


        /**
         * @brief Store all updates in `commit`.
         * @param commit The commit.
         * @return The resulting memory IDs.
         */
        std::vector<MemoryID> update(const InternalCommit& commit);
        /**
         * @brief Store the given update.
         * @param update The update.
         * @return The resulting entity snapshot's ID.
         */
        virtual MemoryID update(const InternalEntityUpdate& update) = 0;

        /// Clear this storage of any elements.
        virtual void clear() = 0;


        /**
         * @brief Retrieve an entity.
         * @param id The entity ID.
         * @return The entity.
         * @throw An exception deriving from `armem::error::ArMemError` if the entity is missing.
         */
        virtual Entity& getEntity(const MemoryID& id) = 0;

        /**
         * @brief Retrieve an entity snapshot.
         *
         * Uses `getEntity()` to retrieve the respective entity.
         *
         * @param id The snapshot ID.
         * @return The entity snapshot.
         * @throw An exception deriving from `armem::error::ArMemError` if the snapshot is missing.
         */
        virtual EntitySnapshot& getEntitySnapshot(const MemoryID& id);


    protected:

        /// Throw an `armem::error::StorageNameMismatch` if `expectedName != actualName`.
        static void checkStorageName(const std::string& expectedName, const std::string& actualName,
                                     const std::string& storageTerm);

    public:

        /// The storage's name.
        std::string name;

    };


}
