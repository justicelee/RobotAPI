#include "EntitySnapshot.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error.h"


namespace armarx::armem
{


    void EntitySnapshot::update(const InternalEntityUpdate& update)
    {
        time = update.timeCreated;

        instances.clear();
        for (int i = 0; i < int(update.instancesData.size()); ++i)
        {
            EntityDataPtr& data = instances.emplace_back(std::make_unique<EntityData>());
            data->update(update, i);
        }
    }


    EntityData& EntitySnapshot::getInstance(int index)
    {
        size_t si = size_t(index);
        if (index >= 0 && si < instances.size())
        {
            return *instances[si];
        }
        else
        {
            throw armem::error::MissingEntry("instance", std::to_string(index),
                                             "entity snapshot", toDateTimeMilliSeconds(time));
        }
    }


    EntityData& EntitySnapshot::getInstance(MemoryID id)
    {
        if (!id.hasInstanceIndex())
        {
            throw armem::error::InvalidMemoryID(id, "ID has no instance index.");
        }
        return getInstance(id.instanceIndex);
    }

}
