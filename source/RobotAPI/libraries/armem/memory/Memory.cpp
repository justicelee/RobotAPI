#include "Memory.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error/ArMemError.h"


namespace armarx::armem
{

    bool Memory::hasCoreSegment(const std::__cxx11::string& name) const
    {
        return coreSegments.count(name) > 0;
    }

    CoreSegment& Memory::getCoreSegment(const std::string& name)
    {
        auto it = this->coreSegments.find(name);
        if (it != coreSegments.end())
        {
            ARMARX_CHECK_NOT_NULL(it->second);
            return *it->second;
        }
        else
        {
            throw armem::error::MissingEntry("core segment", name, "memory", this->name);
        }
    }


    Entity& Memory::getEntity(const MemoryID& id)
    {
        checkStorageName(id.memoryName, this->name, "memory");
        return getCoreSegment(id.coreSegmentName).getEntity(id);
    }


    CoreSegment& Memory::addCoreSegment(const std::string& name)
    {
        if (coreSegments.count(name) > 0)
        {
            throw armem::error::StorageEntryAlreadyExists("core segment", name, "memory", this->name);
        }
        auto it = coreSegments.emplace(name, std::make_unique<CoreSegment>(name)).first;
        ARMARX_CHECK_NOT_NULL(it->second);
        return *it->second;
    }

    std::vector<CoreSegment*> Memory::addCoreSegments(const std::vector<std::string>& names)
    {
        std::vector<CoreSegment*> segments;
        for (const auto& name : names)
        {
            try
            {
                segments.push_back(&addCoreSegment(name));
            }
            catch (const armem::error::StorageEntryAlreadyExists& e)
            {
                ARMARX_INFO << e.what() << "\nIgnoring multiple addition.";
            }
        }
        return segments;
    }


    MemoryID Memory::update(const InternalEntityUpdate& update)
    {
        checkStorageName(update.entityID.memoryName, this->name, "memory");

        auto it = this->coreSegments.find(update.entityID.coreSegmentName);
        if (it != coreSegments.end())
        {
            return it->second->update(update);
        }
        else
        {
            throw armem::error::MissingEntry("core segment", update.entityID.coreSegmentName,
                                             "memory", name);
        }
    }

    void Memory::clear()
    {
        coreSegments.clear();
    }


}
