#include "Entity.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error.h"


namespace armarx::armem
{

    Entity::Entity()
    {
    }

    Entity::Entity(const std::string& name) : name(name)
    {
    }

    bool Entity::hasSnapshot(Time time) const
    {
        return history.count(time) > 0;
    }

    EntitySnapshot& Entity::getSnapshot(Time time)
    {
        auto it = history.find(time);
        if (it != history.end())
        {
            ARMARX_CHECK_NOT_NULL(it->second);
            return *it->second;
        }
        else
        {
            throw error::MissingEntry("entity snapshot", toDateTimeMilliSeconds(time),
                                      "entity", this->name);
        }
    }

    EntitySnapshot& Entity::getSnapshot(const MemoryID& id)
    {
        checkEntityName(id.entityName);
        return getSnapshot(id.timestamp);
    }

    EntitySnapshot& Entity::getLatestSnapshot()
    {
        if (history.empty())
        {
            throw armem::error::EntityHistoryEmpty(name, "when getting the latest snapshot.");
        }
        ARMARX_CHECK_NOT_NULL(history.rbegin()->second);
        return *history.rbegin()->second;
    }


    MemoryID Entity::update(const InternalEntityUpdate& update)
    {
        checkEntityName(update.entityID.entityName);

        auto it = history.find(update.timeCreated);
        if (it == history.end())
        {
            // Insert into history.
            it = history.emplace(update.timeCreated, std::make_unique<EntitySnapshot>()).first;

            truncateHistoryToSize();
        }
        // Update entry.
        ARMARX_CHECK_NOT_NULL(it->second);
        it->second->update(update);

        MemoryID snapshotID = update.entityID;
        snapshotID.timestamp = it->second->time;
        return snapshotID;
    }


    void Entity::setMaxHistorySize(long maxSize)
    {
        this->maxHistorySize = maxSize;
        truncateHistoryToSize();
    }

    void Entity::truncateHistoryToSize()
    {
        if (maxHistorySize >= 0)
        {
            while (history.size() > size_t(maxHistorySize))
            {
                history.erase(history.begin());
            }
            ARMARX_CHECK_LESS_EQUAL(history.size(), maxHistorySize);
        }
    }

    void Entity::checkEntityName(const std::string& name) const
    {
        if (name != this->name)
        {
            throw armem::error::StorageNameMismatch(name, "entity", this->name);
        }
    }

}
