#pragma once

#include <map>
#include <memory>
#include <string>

#include "CoreSegment.h"
#include "EntityStorage.h"


namespace armarx::armem
{

    /**
     * @brief Data of a memory consisting of multiple core segments.
     */
    class Memory : public EntityStorage
    {
    public:

        using EntityStorage::EntityStorage;


        bool hasCoreSegment(const std::string& name) const;
        CoreSegment& getCoreSegment(const std::string& name);

        Entity& getEntity(const MemoryID& id) override;


        CoreSegment& addCoreSegment(const std::string& name);
        /**
         * @brief Add multiple core segments.
         * @param The core segment names.
         * @return The core segments. The contained pointers are never null.
         */
        std::vector<CoreSegment*> addCoreSegments(const std::vector<std::string>& names);

        virtual MemoryID update(const InternalEntityUpdate& update) override;
        using EntityStorage::update;

        void clear() override;


    public:

        std::map<std::string, CoreSegmentPtr> coreSegments;


    };

    using MemoryPtr = std::unique_ptr<Memory>;


}
