#include "ArMemError.h"

#include <sstream>

#include <SimoxUtility/algorithm/string/string_tools.h>


namespace
{

    std::string capitalize(const std::string& string)
    {
        return simox::alg::to_upper(string.substr(0, 1)) + string.substr(1);
    }

}

namespace armarx::armem::error
{


    ArMemError::ArMemError(const std::string& msg) : std::runtime_error(msg)
    {
    }


    StorageNameMismatch::StorageNameMismatch(const std::string& expectedName,
            const std::string& ownTerm, const std::string& storageName) :
        ArMemError(makeMsg(expectedName, ownTerm, storageName))
    {}

    std::string StorageNameMismatch::makeMsg(
        const std::string& expectedName, const std::string& storageTerm, const std::string& storageName)
    {
        std::stringstream ss;
        ss << "Name '" << expectedName << "' does not match name of " << storageTerm << " '" << storageName << "'.";
        return ss.str();
    }


    StorageEntryAlreadyExists::StorageEntryAlreadyExists(const std::string& existingTerm, const std::string& existingName, const std::string& ownTerm, const std::string& ownName) :
        ArMemError(makeMsg(existingTerm, existingName, ownTerm, ownName))
    {
    }

    std::string StorageEntryAlreadyExists::makeMsg(const std::string& existingTerm, const std::string& existingName, const std::string& ownTerm, const std::string& ownName)
    {
        std::stringstream ss;
        ss << capitalize(existingTerm) << " with name '" << existingName << "' "
           << " already exists in " << ownTerm << " '" << ownName << "'.";
        return ss.str();
    }


    MissingEntry::MissingEntry(const std::string& missingTerm, const std::string& missingName,
                               const std::string& ownTerm, const std::string& ownName) :
        ArMemError(makeMsg(missingTerm, missingName, ownTerm, ownName))
    {
    }

    std::string MissingEntry::makeMsg(const std::string& missingTerm, const std::string& missingName,
                                      const std::string& ownTerm, const std::string& ownName)
    {
        std::stringstream ss;
        ss << "No " << missingTerm << " with name '" << missingName << "' "
           << "in " << ownTerm << " '" << ownName << "'.";
        return ss.str();
    }



    ParseIntegerError::ParseIntegerError(std::string string, std::string semanticName) :
        ArMemError(makeMsg(string, semanticName))
    {
    }

    std::string ParseIntegerError::makeMsg(std::string string, std::string semanticName)
    {
        std::stringstream ss;
        ss << "Failed to parse " << semanticName << " '" << string << "' as integer.";
        return ss.str();
    }



    InvalidMemoryID::InvalidMemoryID(const MemoryID& id, const std::string& message) :
        ArMemError(makeMsg(id, message))
    {
    }

    std::string InvalidMemoryID::makeMsg(const MemoryID& id, const std::string& message)
    {
        std::stringstream ss;
        ss << "Invalid memory ID " << id << ": " << message;
        return ss.str();
    }



    EntityHistoryEmpty::EntityHistoryEmpty(const std::string& entityName, const std::string& message) :
        ArMemError(makeMsg(entityName, message))
    {
    }

    std::string EntityHistoryEmpty::makeMsg(const std::string& entityName, const std::string& message)
    {
        std::stringstream ss;
        ss << "History of entity '" << entityName << "' is empty";
        if (message.size() > 0)
        {
            ss << " " << message;
        }
        else
        {
            ss << ".";
        }
        return ss.str();
    }

}
