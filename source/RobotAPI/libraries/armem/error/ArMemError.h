#pragma once

#include <stdexcept>

#include "../core/MemoryID.h"


namespace armarx::armem::error
{

    /**
     * @brief Base class for all exceptions thrown by the armem library.
     */
    class ArMemError : public std::runtime_error
    {
    public:

        ArMemError(const std::string& msg);

    };


    /**
     * @brief Indicates that a name in a given ID does not match a storage's own name.
     */
    class StorageNameMismatch : public ArMemError
    {
    public:

        StorageNameMismatch(const std::string& expectedName,
                            const std::string& storageTerm, const std::string& storageName);

        static std::string makeMsg(const std::string& expectedName,
                                   const std::string& storageTerm, const std::string& storageName);

    };


    /**
     * @brief Indicates that a name in a given ID does not match a storage's own name.
     */
    class StorageEntryAlreadyExists : public ArMemError
    {
    public:

        StorageEntryAlreadyExists(const std::string& existingTerm, const std::string& existingName,
                                  const std::string& ownTerm, const std::string& ownName);

        static std::string makeMsg(const std::string& existingTerm, const std::string& existingName,
                                   const std::string& ownTerm, const std::string& ownName);

    };


    /**
     * @brief Indicates that a storage did not have an entry under a given name.
     */
    class MissingEntry : public ArMemError
    {
    public:
        MissingEntry(const std::string& missingTerm, const std::string& missingName,
                     const std::string& ownTerm, const std::string& ownName);

        static std::string makeMsg(const std::string& missingTerm, const std::string& missingName,
                                   const std::string& ownTerm, const std::string& ownName);
    };


    /**
     * @brief Indicates that a storage did not have an entry under a given name.
     */
    class ParseIntegerError : public ArMemError
    {
    public:
        ParseIntegerError(std::string string, std::string semanticName);

        static std::string makeMsg(std::string string, std::string semanticName);
    };



    /**
     * @brief Indicates that a memory ID is invalid, e.g. does not contain necessary information.
     */
    class InvalidMemoryID : public ArMemError
    {
    public:

        InvalidMemoryID(const MemoryID& id, const std::string& message);

        static std::string makeMsg(const MemoryID& id, const std::string& message);

    };


    /**
     * @brief Indicates that an entity's history was queried, but is empty.
     */
    class EntityHistoryEmpty : public ArMemError
    {
    public:

        EntityHistoryEmpty(const std::string& entityName, const std::string& message = "");

        static std::string makeMsg(const std::string& entityName, const std::string& message = "");

    };


}

