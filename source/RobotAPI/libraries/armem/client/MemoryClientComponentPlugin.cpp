#include "MemoryClientComponentPlugin.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error.h"


namespace armarx::armem::plugins
{

    MemoryClientComponentPlugin::~MemoryClientComponentPlugin()
    {
    }

}


namespace armarx::armem
{

    MemoryClientComponentPluginUser::MemoryClientComponentPluginUser()
    {
        addPlugin(plugin);
    }

}
