#include "MemoryWriter.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../error.h"


namespace armarx::armem
{

    MemoryWriter::MemoryWriter(WritingInterfacePrx memory) : memory(memory)
    {
    }

    data::AddSegmentResult MemoryWriter::addSegment(const data::AddSegmentInput& input)
    {
        data::AddSegmentsResult results = addSegments({input});
        ARMARX_CHECK_EQUAL(results.size(), 1);
        return results.at(0);
    }

    data::AddSegmentsResult MemoryWriter::addSegments(const data::AddSegmentsInput& inputs)
    {
        ARMARX_CHECK_NOT_NULL(memory);
        data::AddSegmentsResult results = memory->addSegments(inputs);
        ARMARX_CHECK_EQUAL(results.size(), inputs.size());
        return results;
    }


    CommitResult MemoryWriter::commit(Commit commit)
    {
        ARMARX_CHECK_NOT_NULL(memory);

        Time timeSent = armem::Time::now();
        for (EntityUpdate& update : commit.updates)
        {
            update.timeSent = timeSent;
        }

        armem::data::Commit commitIce;
        armem::toIce(commitIce, commit);

        armem::data::CommitResult resultIce;
        try
        {
            resultIce = memory->commit(commitIce);
        }
        catch (const Ice::NotRegisteredException& e)
        {
            armem::CommitResult result;
            for (const auto& _ : commit.updates)
            {
                (void) _;
                armem::EntityUpdateResult& r = result.results.emplace_back();
                r.success = false;
                r.errorMessage = "Memory component not registered.\n" + std::string(e.what());
            }
            return result;
        }

        armem::CommitResult result;
        armem::fromIce(resultIce, result);

        return result;
    }


    EntityUpdateResult MemoryWriter::commit(const EntityUpdate& update)
    {
        armem::Commit commit;
        commit.updates.push_back(update);

        armem::CommitResult result = this->commit(commit);
        ARMARX_CHECK_EQUAL(result.results.size(), 1);
        return result.results.at(0);
    }

}
