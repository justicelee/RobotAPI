#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/armem/MemoryInterface.h>
#include <RobotAPI/interface/armem/MemoryNameSystemInterface.h>

#include "../mns/MemoryNameSystemClientPlugin.h"
#include "MemoryReader.h"
#include "MemoryWriter.h"



namespace armarx::armem::plugins
{

    class MemoryClientComponentPlugin : public MemoryNameSystemClientPlugin
    {
    public:

        using MemoryNameSystemClientPlugin::MemoryNameSystemClientPlugin;
        virtual ~MemoryClientComponentPlugin() override;


    };

}


namespace armarx::armem
{

    /**
     * @brief Utility for connecting a Memory to Ice.
     */
    class MemoryClientComponentPluginUser :
        virtual public ManagedIceObject
        , virtual public plugins::MemoryNameSystemClientPluginUser
    {
    public:

        MemoryClientComponentPluginUser();


    private:

        armem::plugins::MemoryClientComponentPlugin* plugin = nullptr;


    };


}
