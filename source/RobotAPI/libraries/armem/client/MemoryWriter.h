#pragma once

#include <RobotAPI/interface/armem/WritingInterface.h>

#include <RobotAPI/libraries/armem/core/ice_conversions.h>


namespace armarx::armem
{

    /**
     * @brief Helps sending data to a memory.
     */
    class MemoryWriter
    {
    public:

        /**
         * @brief Construct a memory writer.
         * @param memory The memory proxy.
         */
        MemoryWriter(WritingInterfacePrx memory = nullptr);


        data::AddSegmentResult addSegment(const data::AddSegmentInput& input);
        data::AddSegmentsResult addSegments(const data::AddSegmentsInput& input);

        /**
         * @brief Writes a `Commit` to the memory.
         */
        CommitResult commit(Commit commit);
        EntityUpdateResult commit(const EntityUpdate& update);



    public:

        WritingInterfacePrx memory;

    };

}
