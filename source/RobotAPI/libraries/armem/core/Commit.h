#pragma once

#include <vector>

#include <RobotAPI/interface/aron.h>

#include "../core/MemoryID.h"
#include "../core/Time.h"


namespace armarx::armem
{

    /**
     * @brief An update of an entity for a specific point in time.
     */
    struct EntityUpdate
    {
        /// The entity's ID.
        MemoryID entityID;

        /// The entity data.
        std::vector<aron::data::AronDataPtr> instancesData;

        /**
         * @brief Time when this entity update was created (e.g. time of image recording).
         * This is the key of the entity's history.
         */
        Time timeCreated = Time::microSeconds(-1);


        // OPTIONAL

        /// An optional confidence, may be used for things like decay.
        float confidence = 1.0;


        // OPTIONAL

        /**
         * @brief Time when this update was sent to the memory.
         *
         * Set automatically when sending the commit.
         */
        Time timeSent = Time::microSeconds(-1);
    };


    /**
     * @brief Result of an `EntityUpdate`.
     */
    struct EntityUpdateResult
    {
        bool success = false;

        MemoryID snapshotID;
        Time timeArrived = Time::microSeconds(-1);

        std::string errorMessage;

        friend std::ostream& operator<<(std::ostream& os, const EntityUpdateResult rhs);
    };



    /**
     * @brief An update sent to the memory.
     */
    struct Commit
    {

        /**
         * @brief The entity updates.
         *
         * May contain updates of multiple entities at different
         * points in time.
         */
        std::vector<EntityUpdate> updates;

    };

    /**
     * @brief Result of a `Commit`.
     */
    struct CommitResult
    {
        std::vector<EntityUpdateResult> results;
    };


}
