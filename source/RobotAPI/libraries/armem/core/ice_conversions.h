#pragma once


#include <RobotAPI/interface/armem/MemoryInterface.h>

#include "Commit.h"


namespace armarx::armem
{

    void fromIce(const data::Commit& ice, Commit& commit);
    void toIce(data::Commit& ice, const Commit& commit);

    void fromIce(const data::CommitResult& ice, CommitResult& result);
    void toIce(data::CommitResult& ice, const CommitResult& result);


    void fromIce(const data::EntityUpdate& ice, EntityUpdate& update);
    void toIce(data::EntityUpdate& ice, const EntityUpdate& update);

    void fromIce(const data::EntityUpdateResult& ice, EntityUpdateResult& result);
    void toIce(data::EntityUpdateResult& ice, const EntityUpdateResult& result);

}
