#include "Commit.h"


namespace armarx::armem
{
    std::ostream& operator<<(std::ostream& os, const EntityUpdateResult rhs)
    {
        return os << "Entity update result: "
               << "\n- success:       \t" << (rhs.success ? "true" : "false")
               << "\n- snapshotID:    \t" << rhs.snapshotID
               << "\n- time arrived:  \t" << rhs.timeArrived.toDateTime()
               << "\n- error message: \t" << rhs.errorMessage
               ;
    }
}
