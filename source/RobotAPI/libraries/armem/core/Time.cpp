#include "Time.h"

#include <cmath>


namespace armarx
{


    std::string armem::toStringMilliSeconds(const Time& time, int decimals)
    {
        std::stringstream ss;
        ss << time.toMilliSeconds();
        if (decimals > 0)
        {
            int div = int(std::pow(10, 3 - decimals));
            ss << "." << (time.toMicroSeconds() % 1000) / div;
        }
        ss << " ms";
        return ss.str();
    }


    std::string armem::toStringMicroSeconds(const Time& time)
    {
        static const char* mu = "\u03BC";
        std::stringstream ss;
        ss << time.toMicroSeconds() << " " << mu << "s";
        return ss.str();
    }


    std::string armem::toDateTimeMilliSeconds(const Time& time, int decimals)
    {
        std::stringstream ss;
        ss << time.toString("%Y-%m-%d %H:%M:%S");
        if (decimals > 0)
        {
            int div = int(std::pow(10, 6 - decimals));
            ss << "." << (time.toMicroSeconds() % int(1e6)) / div;
        }

        return ss.str();
    }

}


