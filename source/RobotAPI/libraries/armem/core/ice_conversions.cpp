#include "ice_conversions.h"


namespace armarx
{

    void armem::fromIce(const data::Commit& ice, Commit& commit)
    {
        commit.updates.clear();
        for (const auto& ice_update : ice.updates)
        {
            EntityUpdate& update = commit.updates.emplace_back();
            fromIce(ice_update, update);
        }
    }

    void armem::toIce(data::Commit& ice, const Commit& commit)
    {
        ice.updates.clear();
        for (const auto& update : commit.updates)
        {
            data::EntityUpdate& ice_update = ice.updates.emplace_back();
            toIce(ice_update, update);
        }
    }


    void armem::fromIce(const data::CommitResult& ice, CommitResult& result)
    {
        result.results.clear();
        for (const auto& ice_res : ice.results)
        {
            EntityUpdateResult& res = result.results.emplace_back();
            fromIce(ice_res, res);
        }
    }

    void armem::toIce(data::CommitResult& ice, const CommitResult& result)
    {
        ice.results.clear();
        for (const auto& res : result.results)
        {
            data::EntityUpdateResult& ice_res = ice.results.emplace_back();
            toIce(ice_res, res);
        }
    }


    void armem::fromIce(const data::EntityUpdate& ice, EntityUpdate& update)
    {
        update.entityID = MemoryID::fromString(ice.entityID);
        update.instancesData = ice.instancesData;
        update.timeCreated = Time::microSeconds(ice.timeCreatedMicroSeconds);

        update.confidence = ice.confidence;
        update.timeSent = Time::microSeconds(ice.timeSentMicroSeconds);
    }

    void armem::toIce(data::EntityUpdate& ice, const EntityUpdate& update)
    {
        ice.entityID = update.entityID.str();
        ice.instancesData = update.instancesData;
        ice.timeCreatedMicroSeconds = update.timeCreated.toMicroSeconds();

        ice.confidence = update.confidence;
        ice.timeSentMicroSeconds = update.timeSent.toMicroSeconds();
    }


    void armem::fromIce(const data::EntityUpdateResult& ice, EntityUpdateResult& result)
    {
        result.success = ice.success;
        result.snapshotID = MemoryID::fromString(ice.snapshotID);
        result.timeArrived = Time::microSeconds(ice.timeArrivedMicroSeconds);
        result.errorMessage = ice.errorMessage;
    }

    void armem::toIce(data::EntityUpdateResult& ice, const EntityUpdateResult& result)
    {
        ice.success = result.success;
        ice.snapshotID = result.snapshotID.str();
        ice.timeArrivedMicroSeconds = result.timeArrived.toMicroSeconds();
        ice.errorMessage = result.errorMessage;
    }

}


