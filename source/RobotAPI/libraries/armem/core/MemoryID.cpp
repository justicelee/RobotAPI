#include "MemoryID.h"

#include <filesystem>

#include "../error/ArMemError.h"


namespace armarx::armem
{

    std::string MemoryID::str() const
    {
        std::filesystem::path p;

        std::vector<std::string> items = getItems();
        for (const auto& item : items)
        {
            p /= item;
        }

        return p;
    }


    MemoryID MemoryID::fromString(const std::string& string)
    {
        MemoryID id;

        std::filesystem::path p = string;

        auto it = p.begin();
        if (it == p.end())
        {
            return id;
        }
        id.memoryName = *it;

        if (++it == p.end())
        {
            return id;
        }
        id.coreSegmentName = *it;

        if (++it == p.end())
        {
            return id;
        }
        id.providerSegmentName = *it;

        if (++it == p.end())
        {
            return id;
        }
        id.entityName = *it;

        if (++it == p.end())
        {
            return id;
        }
        id.timestamp = timestampFromStr(*it);

        if (++it == p.end())
        {
            return id;
        }
        id.instanceIndex = instanceIndexFromStr(*it);

        return id;
    }

    std::vector<std::string> MemoryID::getItems() const
    {
        std::vector<std::string> items;

        items.push_back(memoryName);

        if (!hasCoreSegmentName())
        {
            return items;
        }
        items.push_back(coreSegmentName);

        if (!hasProviderSegmentName())
        {
            return items;

        }
        items.push_back(providerSegmentName);

        if (!hasEntityName())
        {
            return items;

        }
        items.push_back(entityName);

        if (!hasTimestamp())
        {
            return items;

        }
        items.push_back(timestampStr());

        if (!hasInstanceIndex())
        {
            return items;

        }
        items.push_back(instanceIndexStr());

        return items;
    }

    std::vector<std::string> MemoryID::getAllItems() const
    {
        return
        {
            memoryName, coreSegmentName, providerSegmentName, entityName,
            timestampStr(), instanceIndexStr()
        };
    }

    std::string MemoryID::timestampStr() const
    {
        return hasTimestamp() ? std::to_string(timestamp.toMicroSeconds()) : "";
    }

    std::string MemoryID::instanceIndexStr() const
    {
        return hasInstanceIndex() ? std::to_string(instanceIndex) : "";
    }

    IceUtil::Time MemoryID::timestampFromStr(const std::string& string)
    {
        return Time::microSeconds(parseInteger(string, "timestamp"));
    }

    int MemoryID::instanceIndexFromStr(const std::string& string)
    {
        return int(parseInteger(string, "instance index"));
    }

    long long MemoryID::parseInteger(const std::string& string, const std::string& semanticName)
    {
        try
        {
            return std::stoll(string);
        }
        catch (const std::invalid_argument&)
        {
            throw error::ParseIntegerError(string, semanticName);
        }
        catch (const std::out_of_range&)
        {
            throw error::ParseIntegerError(string, semanticName);
        }
    }

    std::ostream& operator<<(std::ostream& os, const MemoryID id)
    {
        return os << "'" << id.str() << "'";
    }

}
