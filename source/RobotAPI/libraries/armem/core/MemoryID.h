#pragma once

#include <string>
#include <vector>

#include "Time.h"


namespace armarx::armem
{

    /**
     * @brief A memory ID.
     *
     * Structure:
     * `MemoryName/CoreSegmentName/ProviderSegmentName/EntityName/Timestamp/InstanceIndex`
     *
     * Example:
     * `VisionMemory/RGBImages/PrimesenseRGB/image/1245321323/0`
     */
    class MemoryID
    {
    public:

        std::string memoryName = "";
        std::string coreSegmentName = "";
        std::string providerSegmentName = "";
        std::string entityName = "";
        Time timestamp = Time::microSeconds(-1);
        int instanceIndex = -1;


        std::string str() const;
        static MemoryID fromString(const std::string& string);


        std::vector<std::string> getItems() const;
        std::vector<std::string> getAllItems() const;


        bool hasMemoryName() const
        {
            return memoryName.size() > 0;
        }
        bool hasCoreSegmentName() const
        {
            return coreSegmentName.size() > 0;
        }
        bool hasProviderSegmentName() const
        {
            return providerSegmentName.size() > 0;
        }
        bool hasEntityName() const
        {
            return entityName.size() > 0;
        }
        bool hasTimestamp() const
        {
            return timestamp.toMicroSeconds() >= 0;
        }
        void clearTimestamp()
        {
            timestamp = Time::microSeconds(-1);
        }
        bool hasInstanceIndex() const
        {
            return instanceIndex >= 0;
        }
        void clearInstanceIndex()
        {
            instanceIndex = -1;
        }


        std::string timestampStr() const;
        std::string instanceIndexStr() const;

        static Time timestampFromStr(const std::string& timestamp);
        static int instanceIndexFromStr(const std::string& index);

        friend std::ostream& operator<<(std::ostream& os, const MemoryID id);

    private:

        static long long parseInteger(const std::string& string, const std::string& semanticName);

    };

}
