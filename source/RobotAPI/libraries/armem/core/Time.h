#pragma once

#include <IceUtil/Time.h>


namespace armarx::armem
{

    using Time = IceUtil::Time;

    /**
     * @brief Returns `time` as e.g. "123456789.012 ms".
     * @param decimals How many sub-millisecond decimals to include.
     */
    std::string toStringMilliSeconds(const Time& time, int decimals = 3);

    /**
     * @brief Returns `time` as e.g. "123456789012 `mu`s".
     * The output string contains the actual greek letter `mu`.
     */
    std::string toStringMicroSeconds(const Time& time);

    /**
     * @brief Returns `time`as e.g. "2020-11-16 17:01:54.123".
     * @param decimals How many sub-second decimals to include.
     */
    std::string toDateTimeMilliSeconds(const Time& time, int decimals = 6);

}
