/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "ArMemBase.h"


namespace armarx
{
    namespace armem
    {
        const std::string ArMemBase::DEFAULT_LOCAL_MEMORY_NAME_PREFIX = "ArMemLocalStorage";

        ArMemBase::ArMemBase()
        {
            my_hostname = GetMyHostname();
        }

        std::string ArMemBase::GenerateLocalMemoryObjectNameFromHostname(const std::string& hostname)
        {
            return DEFAULT_LOCAL_MEMORY_NAME_PREFIX + "__" + hostname;
        }

        std::string ArMemBase::getMyHostname() const
        {
            return my_hostname;
        }

        std::string ArMemBase::GetMyHostname()
        {
            boost::asio::io_service io_service;
            return boost::asio::ip::host_name();
        }
    }
}
