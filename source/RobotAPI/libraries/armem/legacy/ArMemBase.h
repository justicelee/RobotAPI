/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// Boost
#include <boost/asio.hpp>


namespace armarx
{
    namespace armem
    {

        class ArMemBase;
        typedef std::shared_ptr<ArMemBase> ArMemBasePtr;

        class ArMemBase
        {

        public:
            ArMemBase();

            std::string getMyHostname() const;
            static std::string GetMyHostname();

            static std::string GenerateLocalMemoryObjectNameFromHostname(const std::string&);

        private:
            static const std::string DEFAULT_LOCAL_MEMORY_NAME_PREFIX;
            std::string my_hostname;
        };

    }
}
