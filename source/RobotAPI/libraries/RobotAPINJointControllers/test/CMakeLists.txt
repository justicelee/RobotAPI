
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore RobotAPINJointControllers)
 
armarx_add_test(RobotAPINJointsControllerTest RobotAPINJointsControllerTest.cpp "${LIBS}")
