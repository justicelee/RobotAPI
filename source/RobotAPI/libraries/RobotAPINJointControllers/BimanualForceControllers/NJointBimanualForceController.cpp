#include "NJointBimanualForceController.h"

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>

namespace armarx
{
    NJointControllerRegistration<NJointBimanualForceController> registrationControllerNJointBimanualForceController("NJointBimanualForceController");

    NJointBimanualForceController::NJointBimanualForceController(const RobotUnitPtr& robUnit, const armarx::NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&)
    {
        ARMARX_INFO << "Preparing ... bimanual ";
        useSynchronizedRtRobot();
        cfg = NJointBimanualForceControllerConfigPtr::dynamicCast(config);
        //        ARMARX_CHECK_EXPRESSION(prov);
        //        RobotUnitPtr robotUnit = RobotUnitPtr::dynamicCast(prov);
        //        ARMARX_CHECK_EXPRESSION(robotUnit);
        leftRNS = rtGetRobot()->getRobotNodeSet("LeftArm");

        for (size_t i = 0; i < leftRNS->getSize(); ++i)
        {
            std::string jointName = leftRNS->getNode(i)->getName();
            leftJointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            leftTargets.push_back(ct->asA<ControlTarget1DoFActuatorTorque>());
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();

            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }


            leftVelocitySensors.push_back(velocitySensor);
            leftPositionSensors.push_back(positionSensor);

        };

        rightRNS = rtGetRobot()->getRobotNodeSet("RightArm");

        for (size_t i = 0; i < rightRNS->getSize(); ++i)
        {
            std::string jointName = rightRNS->getNode(i)->getName();
            rightJointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            rightTargets.push_back(ct->asA<ControlTarget1DoFActuatorTorque>());

            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();

            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }

            rightVelocitySensors.push_back(velocitySensor);
            rightPositionSensors.push_back(positionSensor);

        };


        //        const SensorValueBase* svlf = prov->getSensorValue("FT L");
        const SensorValueBase* svlf = robUnit->getSensorDevice("FT L")->getSensorValue();
        leftForceTorque = svlf->asA<SensorValueForceTorque>();
        //        const SensorValueBase* svrf = prov->getSensorValue("FT R");
        const SensorValueBase* svrf = robUnit->getSensorDevice("FT R")->getSensorValue();
        rightForceTorque = svrf->asA<SensorValueForceTorque>();

        leftIK.reset(new VirtualRobot::DifferentialIK(leftRNS, leftRNS->getRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));
        rightIK.reset(new VirtualRobot::DifferentialIK(rightRNS, rightRNS->getRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));


        TaskSpaceDMPControllerConfig taskSpaceDMPConfig;
        taskSpaceDMPConfig.motionTimeDuration = cfg->timeDuration;
        taskSpaceDMPConfig.DMPKernelSize = cfg->kernelSize;
        taskSpaceDMPConfig.DMPMode = cfg->dmpMode;
        taskSpaceDMPConfig.DMPStyle = cfg->dmpType;
        taskSpaceDMPConfig.DMPAmplitude = 1.0;
        taskSpaceDMPConfig.phaseStopParas.goDist = cfg->phaseDist0;
        taskSpaceDMPConfig.phaseStopParas.backDist = cfg->phaseDist1;
        taskSpaceDMPConfig.phaseStopParas.Kpos = cfg->phaseKpPos;
        taskSpaceDMPConfig.phaseStopParas.Dpos = 0;
        taskSpaceDMPConfig.phaseStopParas.Kori = cfg->phaseKpOri;
        taskSpaceDMPConfig.phaseStopParas.Dori = 0;
        taskSpaceDMPConfig.phaseStopParas.mm2radi = cfg->posToOriRatio;
        taskSpaceDMPConfig.phaseStopParas.maxValue = cfg->phaseL;
        taskSpaceDMPConfig.phaseStopParas.slop = cfg->phaseK;



        objectDMP.reset(new TaskSpaceDMPController("boxDMP", taskSpaceDMPConfig, false));
        ARMARX_IMPORTANT << "dmp finieshed";

        tcpLeft = leftRNS->getTCP();
        tcpRight = rightRNS->getTCP();

        KpImpedance.resize(cfg->KpImpedance.size());
        ARMARX_CHECK_EQUAL(cfg->KpImpedance.size(), 6);

        for (int i = 0; i < KpImpedance.size(); ++i)
        {
            KpImpedance(i) = cfg->KpImpedance.at(i);
        }

        KdImpedance.resize(cfg->KdImpedance.size());
        ARMARX_CHECK_EQUAL(cfg->KdImpedance.size(), 6);

        for (int i = 0; i < KdImpedance.size(); ++i)
        {
            KdImpedance(i) = cfg->KdImpedance.at(i);
        }

        KpAdmittance.resize(cfg->KpAdmittance.size());
        ARMARX_CHECK_EQUAL(cfg->KpImpedance.size(), 6);

        for (int i = 0; i < KpAdmittance.size(); ++i)
        {
            KpAdmittance(i) = cfg->KpAdmittance.at(i);
        }

        KdAdmittance.resize(cfg->KdAdmittance.size());
        ARMARX_CHECK_EQUAL(cfg->KdAdmittance.size(), 6);

        for (int i = 0; i < KdAdmittance.size(); ++i)
        {
            KdAdmittance(i) = cfg->KdAdmittance.at(i);
        }

        KmAdmittance.resize(cfg->KmAdmittance.size());
        ARMARX_CHECK_EQUAL(cfg->KmAdmittance.size(), 6);

        for (int i = 0; i < KmAdmittance.size(); ++i)
        {
            KmAdmittance(i) = cfg->KmAdmittance.at(i);
        }

        leftDesiredJointValues.resize(leftTargets.size());
        ARMARX_CHECK_EQUAL(cfg->leftDesiredJointValues.size(), leftTargets.size());

        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            leftDesiredJointValues(i) = cfg->leftDesiredJointValues.at(i);
        }

        rightDesiredJointValues.resize(rightTargets.size());
        ARMARX_CHECK_EQUAL(cfg->rightDesiredJointValues.size(), rightTargets.size());

        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            rightDesiredJointValues(i) = cfg->rightDesiredJointValues.at(i);
        }

        KmPID.resize(cfg->KmPID.size());
        ARMARX_CHECK_EQUAL(cfg->KmPID.size(), 6);

        for (int i = 0; i < KmPID.size(); ++i)
        {
            KmPID(i) = cfg->KmPID.at(i);
        }



        modifiedAcc.setZero(12);
        modifiedTwist.setZero(12);
        ARMARX_INFO << "got controller params";


        boxInitialPose = VirtualRobot::MathTools::quat2eigen4f(cfg->boxInitialPose[4], cfg->boxInitialPose[5], cfg->boxInitialPose[6], cfg->boxInitialPose[3]);
        for (int i = 0; i < 3; ++i)
        {
            boxInitialPose(i, 3) = cfg->boxInitialPose[i];
        }

        NJointBimanualForceControllerSensorData initSensorData;
        initSensorData.deltaT = 0;
        initSensorData.currentTime = 0;
        initSensorData.currentPose = boxInitialPose;
        initSensorData.currentTwist.setZero();
        controllerSensorData.reinitAllBuffers(initSensorData);


        NJointBimanualForceControllerInterfaceData initInterfaceData;
        initInterfaceData.currentLeftPose = tcpLeft->getPoseInRootFrame();
        initInterfaceData.currentRightPose = tcpRight->getPoseInRootFrame();
        interfaceData.reinitAllBuffers(initInterfaceData);

        leftInitialPose = boxInitialPose;
        leftInitialPose(0, 3) -= cfg->boxWidth;
        rightInitialPose = boxInitialPose;
        rightInitialPose(0, 3) += cfg->boxWidth;

        forcePIDControllers.resize(12);
        for (size_t i = 0; i < 6; i++)
        {
            forcePIDControllers.at(i).reset(new PIDController(cfg->forceP[i], cfg->forceI[i], cfg->forceD[i], cfg->forcePIDLimits[i]));
            forcePIDControllers.at(i + 6).reset(new PIDController(cfg->forceP[i], cfg->forceI[i], cfg->forceD[i], cfg->forcePIDLimits[i]));
            forcePIDControllers.at(i)->reset();
            forcePIDControllers.at(i + 6)->reset();
        }

        // filter
        filterCoeff = cfg->filterCoeff;
        ARMARX_IMPORTANT << "filter coeff.: " << filterCoeff;
        filteredOldValue.setZero(12);

        // static compensation
        massLeft = cfg->massLeft;
        CoMVecLeft << cfg->CoMVecLeft[0],  cfg->CoMVecLeft[1],  cfg->CoMVecLeft[2];
        forceOffsetLeft << cfg->forceOffsetLeft[0],  cfg->forceOffsetLeft[1],  cfg->forceOffsetLeft[2];
        torqueOffsetLeft << cfg->torqueOffsetLeft[0],  cfg->torqueOffsetLeft[1],  cfg->torqueOffsetLeft[2];

        massRight = cfg->massRight;
        CoMVecRight << cfg->CoMVecRight[0],  cfg->CoMVecRight[1],  cfg->CoMVecRight[2];
        forceOffsetRight << cfg->forceOffsetRight[0],  cfg->forceOffsetRight[1],  cfg->forceOffsetRight[2];
        torqueOffsetRight << cfg->torqueOffsetRight[0],  cfg->torqueOffsetRight[1],  cfg->torqueOffsetRight[2];

        sensorFrame2TcpFrameLeft.setZero();
        sensorFrame2TcpFrameRight.setZero();

        NJointBimanualForceControlData initData;
        initData.boxPose = boxInitialPose;
        initData.boxTwist.setZero(6);
        reinitTripleBuffer(initData);

        firstLoop = true;
        ARMARX_INFO << "left initial pose: \n" << leftInitialPose  << "\n right initial pose: \n" << rightInitialPose;

        ARMARX_IMPORTANT << "targetwrench is: " << cfg->targetWrench;
        ARMARX_IMPORTANT << "finished construction!";

        dmpStarted = false;

        targetWrench.setZero(cfg->targetWrench.size());
        for (size_t i = 0; i < cfg->targetWrench.size(); ++i)
        {
            targetWrench(i) = cfg->targetWrench[i];
        }



    }

    std::string NJointBimanualForceController::getClassName(const Ice::Current&) const
    {
        return "NJointBimanualForceController";
    }

    //    void NJointBimanualForceController::rtPreActivateController()
    //    {
    //        //        modifiedLeftPose = tcpLeft->getPoseInRootFrame();
    //        //        modifiedRightPose = tcpRight->getPoseInRootFrame();
    //        //        Eigen::Matrix4f leftSensorFrame = leftRNS->getRobot()->getRobotNode("ArmL8_Wri2")->getPoseInRootFrame();
    //        //        Eigen::Matrix4f rightSensorFrame = rightRNS->getRobot()->getRobotNode("ArmR8_Wri2")->getPoseInRootFrame();
    //        //        sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) = modifiedLeftPose.block<3, 3>(0, 0).transpose() * leftSensorFrame.block<3, 3>(0, 0);
    //        //        sensorFrame2TcpFrameRight.block<3, 3>(0, 0) = modifiedRightPose.block<3, 3>(0, 0).transpose() * rightSensorFrame.block<3, 3>(0, 0);
    //        //        CoMVecLeft = sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) * CoMVecLeft;
    //        //        CoMVecRight = sensorFrame2TcpFrameRight.block<3, 3>(0, 0) * CoMVecRight;
    //    }


    void NJointBimanualForceController::controllerRun()
    {
        if (!controllerSensorData.updateReadBuffer() || !dmpStarted)
        {
            return;
        }

        double deltaT = controllerSensorData.getReadBuffer().deltaT;
        Eigen::Matrix4f currentPose = controllerSensorData.getReadBuffer().currentPose;
        Eigen::VectorXf currentTwist = controllerSensorData.getReadBuffer().currentTwist;

        if (objectDMP->canVal < 1e-8)
        {
            finished = true;
        }

        objectDMP->flow(deltaT, currentPose, currentTwist);

        LockGuardType guard {controlDataMutex};
        getWriterControlStruct().boxPose = objectDMP->getTargetPoseMat();
        getWriterControlStruct().boxTwist = objectDMP->getTargetVelocity();
        writeControlStruct();
    }




    void NJointBimanualForceController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (firstLoop)
        {
            modifiedLeftPose = tcpLeft->getPoseInRootFrame();
            modifiedRightPose = tcpRight->getPoseInRootFrame();
            modifiedLeftPose.block<3, 1>(0, 3) = modifiedLeftPose.block<3, 1>(0, 3) * 0.001;
            modifiedRightPose.block<3, 1>(0, 3) = modifiedRightPose.block<3, 1>(0, 3) * 0.001;

            Eigen::Matrix4f leftSensorFrame = leftRNS->getRobot()->getRobotNode("ArmL8_Wri2")->getPoseInRootFrame();
            Eigen::Matrix4f rightSensorFrame = rightRNS->getRobot()->getRobotNode("ArmR8_Wri2")->getPoseInRootFrame();
            leftSensorFrame.block<3, 1>(0, 3) = leftSensorFrame.block<3, 1>(0, 3) * 0.001;
            rightSensorFrame.block<3, 1>(0, 3) = rightSensorFrame.block<3, 1>(0, 3) * 0.001;

            sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) = modifiedLeftPose.block<3, 3>(0, 0).transpose() * leftSensorFrame.block<3, 3>(0, 0);
            sensorFrame2TcpFrameRight.block<3, 3>(0, 0) = modifiedRightPose.block<3, 3>(0, 0).transpose() * rightSensorFrame.block<3, 3>(0, 0);
            CoMVecLeft = sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) * CoMVecLeft;
            CoMVecRight = sensorFrame2TcpFrameRight.block<3, 3>(0, 0) * CoMVecRight;
            firstLoop = false;
            ARMARX_INFO << "modified left pose:\n " << modifiedLeftPose;
            ARMARX_INFO << "modified right pose:\n " << modifiedRightPose;
        }
        double deltaT = timeSinceLastIteration.toSecondsDouble();


        // grasp matrix
        Eigen::Vector3f rToBoxCoM;
        rToBoxCoM << cfg->boxWidth, 0, 0;
        Eigen::Matrix4f currentLeftPose = tcpLeft->getPoseInRootFrame();
        Eigen::Matrix4f currentRightPose = tcpRight->getPoseInRootFrame();

        interfaceData.getWriteBuffer().currentLeftPose = currentLeftPose;
        interfaceData.getWriteBuffer().currentRightPose = currentRightPose;
        interfaceData.commitWrite();

        Eigen::VectorXf currentTargetWrench = targetWrench;
        if (fabs(currentLeftPose(0, 3) - currentRightPose(0, 3)) < 0.8 * 2 *  cfg->boxWidth)
        {
            currentTargetWrench.setZero();
        }
        currentLeftPose.block<3, 1>(0, 3) = 0.001 * currentLeftPose.block<3, 1>(0, 3);
        currentRightPose.block<3, 1>(0, 3) = 0.001 * currentRightPose.block<3, 1>(0, 3);
        Eigen::MatrixXf graspMatrix;
        graspMatrix.setZero(6, 12);
        graspMatrix.block<6, 6>(0, 0) = Eigen::MatrixXf::Identity(6, 6);
        graspMatrix.block<6, 6>(0, 6) = Eigen::MatrixXf::Identity(6, 6);
        Eigen::Vector3f r = - currentLeftPose.block<3, 3>(0, 0) * rToBoxCoM;
        graspMatrix(4, 2) = -r(0);
        graspMatrix(3, 2) = r(1);
        graspMatrix(3, 1) = -r(2);
        graspMatrix(4, 0) = r(2);
        graspMatrix(5, 0) = -r(1);
        graspMatrix(5, 1) = r(0);
        r = currentRightPose.block<3, 3>(0, 0) * rToBoxCoM;
        graspMatrix(4, 8) = -r(0);
        graspMatrix(3, 8) = r(1);
        graspMatrix(3, 7) = -r(2);
        graspMatrix(4, 6) = r(2);
        graspMatrix(5, 6) = -r(1);
        graspMatrix(5, 7) = r(0);
        // projection of grasp matrix
        Eigen::MatrixXf pinvG = leftIK->computePseudoInverseJacobianMatrix(graspMatrix, 0);
        Eigen::MatrixXf G_range = pinvG * graspMatrix;
        Eigen::MatrixXf PG = Eigen::MatrixXf::Identity(12, 12) - G_range;

        // box pose
        Eigen::Matrix4f boxCurrentPose = currentLeftPose;
        boxCurrentPose.block<3, 1>(0, 3) = 0.5 * (currentLeftPose.block<3, 1>(0, 3) + currentRightPose.block<3, 1>(0, 3));
        Eigen::VectorXf boxCurrentTwist;
        boxCurrentTwist.setZero(6);

        // cartesian vel controller
        Eigen::MatrixXf I = Eigen::MatrixXf::Identity(leftTargets.size(), leftTargets.size());

        Eigen::MatrixXf jacobiL = leftIK->getJacobianMatrix(tcpLeft, VirtualRobot::IKSolver::CartesianSelection::All);

        Eigen::VectorXf leftqpos;
        Eigen::VectorXf leftqvel;
        leftqpos.resize(leftPositionSensors.size());
        leftqvel.resize(leftVelocitySensors.size());
        for (size_t i = 0; i < leftVelocitySensors.size(); ++i)
        {
            leftqpos(i) = leftPositionSensors[i]->position;
            leftqvel(i) = leftVelocitySensors[i]->velocity;
        }

        Eigen::MatrixXf jacobiR = rightIK->getJacobianMatrix(tcpRight, VirtualRobot::IKSolver::CartesianSelection::All);

        // jacobiL used in L304
        jacobiL.block<3, 8>(0, 0) = 0.001 * jacobiL.block<3, 8>(0, 0);
        jacobiR.block<3, 8>(0, 0) = 0.001 * jacobiR.block<3, 8>(0, 0);

        Eigen::VectorXf rightqpos;
        Eigen::VectorXf rightqvel;
        rightqpos.resize(rightPositionSensors.size());
        rightqvel.resize(rightVelocitySensors.size());

        for (size_t i = 0; i < rightVelocitySensors.size(); ++i)
        {
            rightqpos(i) = rightPositionSensors[i]->position;
            rightqvel(i) = rightVelocitySensors[i]->velocity;
        }

        // what is the unit of jacobiL, 0.001?
        Eigen::VectorXf currentLeftTwist = jacobiL * leftqvel;
        Eigen::VectorXf currentRightTwist = jacobiR * rightqvel;
        Eigen::VectorXf currentTwist(12);
        currentTwist << currentLeftTwist, currentRightTwist;
        Eigen::MatrixXf pinvGraspMatrixT = leftIK->computePseudoInverseJacobianMatrix(graspMatrix.transpose(), 0);
        boxCurrentTwist = pinvGraspMatrixT * currentTwist;




        controllerSensorData.getWriteBuffer().currentPose = boxCurrentPose;
        controllerSensorData.getWriteBuffer().currentTwist = boxCurrentTwist;
        controllerSensorData.getWriteBuffer().deltaT = deltaT;
        controllerSensorData.getWriteBuffer().currentTime += deltaT;
        controllerSensorData.commitWrite();




        Eigen::Matrix4f boxPose = rtGetControlStruct().boxPose;
        Eigen::VectorXf boxTwist = rtGetControlStruct().boxTwist;

        Eigen::VectorXf leftJointControlWrench;
        Eigen::VectorXf rightJointControlWrench;



        //Todo: calculate desired wrench from required box pose
        //        Eigen::VectorXf boxPoseError(6);
        //        Eigen::Matrix3f diffMat = boxPose.block<3, 3>(0, 0) * boxCurrentPose.block<3, 3>(0, 0).transpose();
        //        boxPoseError.tail(3) = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
        //        boxPoseError.head(3) = boxPose.block<3, 1>(0, 3) - boxCurrentPose.block<3, 1>(0, 3);
        //        Eigen::VectorXf computedBoxWrench(6);
        //        computedBoxWrench = KpAdmittance.cwiseProduct(boxPoseError);// + KdAdmittance.cwiseProduct(boxTwist - boxCurrentTwist);
        //        Eigen::VectorXf wrenchDMP = graspMatrix.transpose() * computedBoxWrench;
        //        wrenchDMP.setZero();
        Eigen::VectorXf twistDMP = graspMatrix.transpose() * boxTwist;
        Eigen::VectorXf deltaInitialPose = deltaT * twistDMP;
        leftInitialPose.block<3, 1>(0, 3)  = leftInitialPose.block<3, 1>(0, 3) + deltaInitialPose.block<3, 1>(0, 0);
        rightInitialPose.block<3, 1>(0, 3) = rightInitialPose.block<3, 1>(0, 3) + deltaInitialPose.block<3, 1>(6, 0);
        leftInitialPose.block<3, 3>(0, 0) = VirtualRobot::MathTools::rpy2eigen3f(deltaInitialPose(3), deltaInitialPose(4), deltaInitialPose(5)) * leftInitialPose.block<3, 3>(0, 0);
        rightInitialPose.block<3, 3>(0, 0) = VirtualRobot::MathTools::rpy2eigen3f(deltaInitialPose(9), deltaInitialPose(10), deltaInitialPose(11)) * rightInitialPose.block<3, 3>(0, 0);



        // static compensation
        Eigen::Vector3f gravity;
        gravity << 0.0, 0.0, -9.8;
        Eigen::Vector3f localGravityLeft = currentLeftPose.block<3, 3>(0, 0).transpose() * gravity;
        Eigen::Vector3f localForceVecLeft = massLeft * localGravityLeft;
        Eigen::Vector3f localTorqueVecLeft = CoMVecLeft.cross(localForceVecLeft);

        Eigen::Vector3f localGravityRight = currentRightPose.block<3, 3>(0, 0).transpose() * gravity;
        Eigen::Vector3f localForceVecRight = massRight * localGravityRight;
        Eigen::Vector3f localTorqueVecRight = CoMVecRight.cross(localForceVecRight);

        // mapping of measured wrenches
        Eigen::VectorXf wrenchesMeasured(12);
        wrenchesMeasured << leftForceTorque->force - forceOffsetLeft, leftForceTorque->torque - torqueOffsetLeft, rightForceTorque->force - forceOffsetRight, rightForceTorque->torque - torqueOffsetRight;
        for (size_t i = 0; i < 12; i++)
        {
            wrenchesMeasured(i) = (1 - filterCoeff) * wrenchesMeasured(i) + filterCoeff * filteredOldValue(i);
        }
        filteredOldValue = wrenchesMeasured;
        wrenchesMeasured.block<3, 1>(0, 0) = sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(0, 0) - localForceVecLeft;
        wrenchesMeasured.block<3, 1>(3, 0) = sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(3, 0) - localTorqueVecLeft;
        wrenchesMeasured.block<3, 1>(6, 0) = sensorFrame2TcpFrameRight.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(6, 0) - localForceVecRight;
        wrenchesMeasured.block<3, 1>(9, 0) = sensorFrame2TcpFrameRight.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(9, 0) - localTorqueVecRight;
        if (wrenchesMeasured.norm() < cfg->forceThreshold)
        {
            wrenchesMeasured.setZero();
        }

        // PID force controller
        //        Eigen::VectorXf wrenchesConstrainedInLocal(12);
        //        wrenchesConstrainedInLocal.block<3, 1>(0, 0) = currentLeftPose.block<3, 3>(0, 0).transpose() * wrenchesConstrained.block<3, 1>(0, 0);
        //        wrenchesConstrainedInLocal.block<3, 1>(3, 0) = currentLeftPose.block<3, 3>(0, 0).transpose() * wrenchesConstrained.block<3, 1>(3, 0);
        //        wrenchesConstrainedInLocal.block<3, 1>(6, 0) = currentRightPose.block<3, 3>(0, 0).transpose() * wrenchesConstrained.block<3, 1>(6, 0);
        //        wrenchesConstrainedInLocal.block<3, 1>(9, 0) = currentRightPose.block<3, 3>(0, 0).transpose() * wrenchesConstrained.block<3, 1>(9, 0);
        Eigen::VectorXf forcePID(12);
        //        Eigen::VectorXf forcePIDControlValue(12);
        //        for (size_t i = 0; i < 12; i++)
        //        {
        //            forcePIDControllers[i]->update(deltaT, wrenchesConstrainedInLocal(i), cfg->targetWrench[i]);
        //            forcePIDControllers[i]->update(deltaT, wrenchesMeasured(i), cfg->targetWrench[i]);
        //            forcePIDControlValue(i) = forcePIDControllers[i]->getControlValue();
        //            forcePID(i) = - forcePIDControllers[i]->getControlValue();

        //        }
        for (size_t i = 0; i < 6; i++)
        {
            forcePID(i) = cfg->forceP[i] * (currentTargetWrench(i) - wrenchesMeasured(i));
            forcePID(i + 6) = cfg->forceP[i] * (currentTargetWrench(i + 6) - wrenchesMeasured(i + 6));
        }
        Eigen::VectorXf forcePIDInRoot(12);
        forcePIDInRoot.block<3, 1>(0, 0) = currentLeftPose.block<3, 3>(0, 0) * forcePID.block<3, 1>(0, 0);
        forcePIDInRoot.block<3, 1>(3, 0) = currentLeftPose.block<3, 3>(0, 0) * forcePID.block<3, 1>(3, 0);
        forcePIDInRoot.block<3, 1>(6, 0) = currentRightPose.block<3, 3>(0, 0) * forcePID.block<3, 1>(6, 0);
        forcePIDInRoot.block<3, 1>(9, 0) = currentRightPose.block<3, 3>(0, 0) * forcePID.block<3, 1>(9, 0);

        //        forcePIDInRoot = PG * forcePIDInRoot;
        Eigen::VectorXf forcePIDInRootForDebug = forcePIDInRoot;

        Eigen::VectorXf wrenchesMeasuredInRoot(12);
        wrenchesMeasuredInRoot.block<3, 1>(0, 0) = currentLeftPose.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(0, 0);
        wrenchesMeasuredInRoot.block<3, 1>(3, 0) = currentLeftPose.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(3, 0);
        wrenchesMeasuredInRoot.block<3, 1>(6, 0) = currentRightPose.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(6, 0);
        wrenchesMeasuredInRoot.block<3, 1>(9, 0) = currentRightPose.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(9, 0);
        Eigen::VectorXf wrenchesConstrained = PG * wrenchesMeasuredInRoot;
        //        wrenchesConstrained.setZero();




        // admittance
        Eigen::VectorXf poseError(12);
        poseError.block<3, 1>(0, 0) = leftInitialPose.block<3, 1>(0, 3) - modifiedLeftPose.block<3, 1>(0, 3);
        Eigen::Matrix3f diffMat = leftInitialPose.block<3, 3>(0, 0) * modifiedLeftPose.block<3, 3>(0, 0).transpose();
        poseError.block<3, 1>(3, 0) = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
        poseError.block<3, 1>(6, 0) = rightInitialPose.block<3, 1>(0, 3) - modifiedRightPose.block<3, 1>(0, 3);
        diffMat = rightInitialPose.block<3, 3>(0, 0) * modifiedRightPose.block<3, 3>(0, 0).transpose();
        poseError.block<3, 1>(9, 0) = VirtualRobot::MathTools::eigen3f2rpy(diffMat);

        Eigen::VectorXf acc;
        Eigen::VectorXf twist;
        twist.setZero(12);
        acc.setZero(12);
        for (size_t i = 0; i < 6; i++)
        {
            //            acc(i) = KpAdmittance(i) * poseError(i) - KdAdmittance(i) * modifiedTwist(i) + wrenchDMP(i) - KmAdmittance(i) * wrenchesConstrained(i);
            //            acc(i + 6) = KpAdmittance(i) * poseError(i + 6) - KdAdmittance(i) * modifiedTwist(i + 6) + wrenchDMP(i + 6) - KmAdmittance(i) * wrenchesConstrained(i + 6);
            acc(i) = KpAdmittance(i) * poseError(i) - KdAdmittance(i) * modifiedTwist(i) - KmAdmittance(i) * wrenchesConstrained(i) - KmPID(i) * forcePIDInRoot(i);
            acc(i + 6) = KpAdmittance(i) * poseError(i + 6) - KdAdmittance(i) * modifiedTwist(i + 6) - KmAdmittance(i) * wrenchesConstrained(i + 6) - KmPID(i) * forcePIDInRoot(i + 6);
        }
        twist = modifiedTwist + 0.5 * deltaT * (acc + modifiedAcc);
        Eigen::VectorXf deltaPose = 0.5 * deltaT * (twist + modifiedTwist);
        modifiedAcc = acc;
        modifiedTwist = twist;

        modifiedLeftPose.block<3, 1>(0, 3)  = modifiedLeftPose.block<3, 1>(0, 3) + deltaPose.block<3, 1>(0, 0);
        modifiedRightPose.block<3, 1>(0, 3) = modifiedRightPose.block<3, 1>(0, 3) + deltaPose.block<3, 1>(6, 0);
        modifiedLeftPose.block<3, 3>(0, 0) = VirtualRobot::MathTools::rpy2eigen3f(deltaPose(3), deltaPose(4), deltaPose(5)) * modifiedLeftPose.block<3, 3>(0, 0);
        modifiedRightPose.block<3, 3>(0, 0) = VirtualRobot::MathTools::rpy2eigen3f(deltaPose(9), deltaPose(10), deltaPose(11)) * modifiedRightPose.block<3, 3>(0, 0);

        //        for (size_t i = 0; i < 6; i++)
        //        {
        //            poseError(i) = (wrenchDMP(i) + wrenchesConstrained(i)) / KpAdmittance(i);
        //            poseError(i + 6) = (wrenchDMP(i + 6) + wrenchesConstrained(i + 6)) / KpAdmittance(i);
        //        }
        //        modifiedLeftPose.block<3, 1>(0, 3)  = leftInitialPose.block<3, 1>(0, 3) + poseError.block<3, 1>(0, 0);
        //        modifiedRightPose.block<3, 1>(0, 3) = rightInitialPose.block<3, 1>(0, 3) + poseError.block<3, 1>(6, 0);
        //        modifiedLeftPose.block<3, 3>(0, 0) =  VirtualRobot::MathTools::rpy2eigen3f(poseError(3), poseError(4), poseError(5)) * leftInitialPose.block<3, 3>(0, 0);
        //        modifiedRightPose.block<3, 3>(0, 0) = VirtualRobot::MathTools::rpy2eigen3f(poseError(9), poseError(10), poseError(11)) * rightInitialPose.block<3, 3>(0, 0) * ;

        // impedance control
        diffMat = modifiedLeftPose.block<3, 3>(0, 0) * currentLeftPose.block<3, 3>(0, 0).transpose();
        poseError.block<3, 1>(0, 0) = modifiedLeftPose.block<3, 1>(0, 3) - currentLeftPose.block<3, 1>(0, 3);
        poseError.block<3, 1>(3, 0) = VirtualRobot::MathTools::eigen3f2rpy(diffMat);

        diffMat = modifiedRightPose.block<3, 3>(0, 0) * currentRightPose.block<3, 3>(0, 0).transpose();
        poseError.block<3, 1>(6, 0) = modifiedRightPose.block<3, 1>(0, 3) - currentRightPose.block<3, 1>(0, 3);
        poseError.block<3, 1>(9, 0) = VirtualRobot::MathTools::eigen3f2rpy(diffMat);

        Eigen::VectorXf forceImpedance(12);
        for (size_t i = 0; i < 6; i++)
        {
            forceImpedance(i) = KpImpedance(i) * poseError(i) - KdImpedance(i) * currentTwist(i);
            forceImpedance(i + 6) = KpImpedance(i) * poseError(i + 6) - KdImpedance(i) * currentTwist(i + 6);
        }



        // nullspace
        Eigen::VectorXf leftNullspaceTorque = cfg->knull * (leftDesiredJointValues - leftqpos) - cfg->dnull * leftqvel;
        Eigen::VectorXf rightNullspaceTorque = cfg->knull * (rightDesiredJointValues - rightqpos) - cfg->dnull * rightqvel;

        float lambda = 1;

        //        forcePIDInRoot.setZero();
        forcePIDInRoot.setZero();
        leftJointControlWrench = forceImpedance.head(6) + forcePIDInRoot.head(6);
        rightJointControlWrench = forceImpedance.tail(6) + forcePIDInRoot.tail(6);
        Eigen::MatrixXf jtpinvL = leftIK->computePseudoInverseJacobianMatrix(jacobiL.transpose(), lambda);
        Eigen::MatrixXf jtpinvR = rightIK->computePseudoInverseJacobianMatrix(jacobiR.transpose(), lambda);
        Eigen::VectorXf leftJointDesiredTorques = jacobiL.transpose() * leftJointControlWrench + (I - jacobiL.transpose() * jtpinvL) * leftNullspaceTorque;
        Eigen::VectorXf rightJointDesiredTorques = jacobiR.transpose() * rightJointControlWrench + (I - jacobiR.transpose() * jtpinvR) * rightNullspaceTorque;



        // torque limit
        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            float desiredTorque =   leftJointDesiredTorques(i);

            if (isnan(desiredTorque))
            {
                desiredTorque = 0;
            }

            desiredTorque = (desiredTorque >  cfg->torqueLimit) ? cfg->torqueLimit : desiredTorque;
            desiredTorque = (desiredTorque < -cfg->torqueLimit) ? -cfg->torqueLimit : desiredTorque;

            debugOutputData.getWriteBuffer().desired_torques[leftJointNames[i]] = leftJointDesiredTorques(i);

            leftTargets.at(i)->torque = desiredTorque;
        }


        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            float desiredTorque = rightJointDesiredTorques(i);

            if (isnan(desiredTorque))
            {
                desiredTorque = 0;
            }

            desiredTorque = (desiredTorque >   cfg->torqueLimit) ?  cfg->torqueLimit : desiredTorque;
            desiredTorque = (desiredTorque < - cfg->torqueLimit) ? - cfg->torqueLimit : desiredTorque;

            debugOutputData.getWriteBuffer().desired_torques[rightJointNames[i]] = rightJointDesiredTorques(i);

            rightTargets.at(i)->torque = desiredTorque;
        }
        //        debugOutputData.getWriteBuffer().leftControlSignal_x = leftJointControlWrench(0);



        debugOutputData.getWriteBuffer().forceImpedance = forceImpedance;
        debugOutputData.getWriteBuffer().poseError = poseError;
        debugOutputData.getWriteBuffer().wrenchesConstrained = wrenchesConstrained;
        debugOutputData.getWriteBuffer().wrenchesMeasuredInRoot = wrenchesMeasuredInRoot;
        //        debugOutputData.getWriteBuffer().wrenchDMP = wrenchDMP;
        //        debugOutputData.getWriteBuffer().computedBoxWrench = computedBoxWrench;

        debugOutputData.getWriteBuffer().modifiedPoseRight_x = modifiedRightPose(0, 3);
        debugOutputData.getWriteBuffer().modifiedPoseRight_y = modifiedRightPose(1, 3);
        debugOutputData.getWriteBuffer().modifiedPoseRight_z = modifiedRightPose(2, 3);

        debugOutputData.getWriteBuffer().currentPoseLeft_x = currentLeftPose(0, 3);
        debugOutputData.getWriteBuffer().currentPoseLeft_y = currentLeftPose(1, 3);
        debugOutputData.getWriteBuffer().currentPoseLeft_z = currentLeftPose(2, 3);



        debugOutputData.getWriteBuffer().modifiedPoseLeft_x = modifiedLeftPose(0, 3);
        debugOutputData.getWriteBuffer().modifiedPoseLeft_y = modifiedLeftPose(1, 3);
        debugOutputData.getWriteBuffer().modifiedPoseLeft_z = modifiedLeftPose(2, 3);

        debugOutputData.getWriteBuffer().currentPoseRight_x = currentRightPose(0, 3);
        debugOutputData.getWriteBuffer().currentPoseRight_y = currentRightPose(1, 3);
        debugOutputData.getWriteBuffer().currentPoseRight_z = currentRightPose(2, 3);


        debugOutputData.getWriteBuffer().dmpBoxPose_x = boxPose(0, 3);
        debugOutputData.getWriteBuffer().dmpBoxPose_y = boxPose(1, 3);
        debugOutputData.getWriteBuffer().dmpBoxPose_z = boxPose(2, 3);

        debugOutputData.getWriteBuffer().dmpTwist_x = boxTwist(0);
        debugOutputData.getWriteBuffer().dmpTwist_y = boxTwist(1);
        debugOutputData.getWriteBuffer().dmpTwist_z = boxTwist(2);
        debugOutputData.getWriteBuffer().rx = r(0);
        debugOutputData.getWriteBuffer().ry = r(1);
        debugOutputData.getWriteBuffer().rz = r(2);

        debugOutputData.getWriteBuffer().modifiedTwist_lx = twistDMP(0);
        debugOutputData.getWriteBuffer().modifiedTwist_ly = twistDMP(1);
        debugOutputData.getWriteBuffer().modifiedTwist_lz = twistDMP(2);
        debugOutputData.getWriteBuffer().modifiedTwist_rx = twistDMP(6);
        debugOutputData.getWriteBuffer().modifiedTwist_ry = twistDMP(7);
        debugOutputData.getWriteBuffer().modifiedTwist_rz = twistDMP(8);

        debugOutputData.getWriteBuffer().forcePID = forcePIDInRootForDebug;

        debugOutputData.commitWrite();

    }

    void NJointBimanualForceController::learnDMPFromFiles(const Ice::StringSeq& fileNames, const Ice::Current&)
    {
        objectDMP->learnDMPFromFiles(fileNames);
    }


    void NJointBimanualForceController::setGoals(const Ice::DoubleSeq& goals, const Ice::Current& ice)
    {
        LockGuardType guard(controllerMutex);
        objectDMP->setGoalPoseVec(goals);

    }


    void NJointBimanualForceController::runDMP(const Ice::DoubleSeq& goals, const Ice::Current&)
    {
        while (!interfaceData.updateReadBuffer())
        {
            usleep(1000);
        }

        Eigen::Matrix4f leftPose = interfaceData.getUpToDateReadBuffer().currentLeftPose;
        Eigen::Matrix4f rightPose = interfaceData.getUpToDateReadBuffer().currentRightPose;

        VirtualRobot::MathTools::Quaternion boxOri = VirtualRobot::MathTools::eigen4f2quat(leftPose);
        Eigen::Vector3f boxPosi = (leftPose.block<3, 1>(0, 3) + rightPose.block<3, 1>(0, 3)) / 2;

        std::vector<double> boxInitialPose;
        for (size_t i = 0; i < 3; ++i)
        {
            boxInitialPose.push_back(boxPosi(i) * 0.001); //Important: mm -> m
        }
        boxInitialPose.push_back(boxOri.w);
        boxInitialPose.push_back(boxOri.x);
        boxInitialPose.push_back(boxOri.y);
        boxInitialPose.push_back(boxOri.z);

        virtualtimer = cfg->timeDuration;
        objectDMP->prepareExecution(boxInitialPose, goals);

        finished = false;
        dmpStarted = true;
    }

    void NJointBimanualForceController::setViaPoints(Ice::Double u, const Ice::DoubleSeq& viapoint, const Ice::Current&)
    {
        //        LockGuardType guard(controllerMutex);
        ARMARX_INFO << "setting via points ";
        objectDMP->setViaPose(u, viapoint);

    }

    void NJointBimanualForceController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& debugObs)
    {

        StringVariantBaseMap datafields;
        auto values = debugOutputData.getUpToDateReadBuffer().desired_torques;
        for (auto& pair : values)
        {
            datafields[pair.first] = new Variant(pair.second);
        }

        Eigen::VectorXf forceImpedance = debugOutputData.getUpToDateReadBuffer().forceImpedance;
        for (int i = 0; i < forceImpedance.rows(); ++i)
        {
            std::stringstream ss;
            ss << i;
            std::string data_name = "forceImpedance_" + ss.str();
            datafields[data_name] = new Variant(forceImpedance(i));
        }

        Eigen::VectorXf forcePID = debugOutputData.getUpToDateReadBuffer().forcePID;
        for (int i = 0; i < forcePID.rows(); ++i)
        {
            std::stringstream ss;
            ss << i;
            std::string data_name = "forcePID_" + ss.str();
            datafields[data_name] = new Variant(forcePID(i));
        }


        Eigen::VectorXf poseError = debugOutputData.getUpToDateReadBuffer().poseError;
        for (int i = 0; i < poseError.rows(); ++i)
        {
            std::stringstream ss;
            ss << i;
            std::string data_name = "poseError_" + ss.str();
            datafields[data_name] = new Variant(poseError(i));
        }

        Eigen::VectorXf wrenchesConstrained = debugOutputData.getUpToDateReadBuffer().wrenchesConstrained;
        for (int i = 0; i < wrenchesConstrained.rows(); ++i)
        {
            std::stringstream ss;
            ss << i;
            std::string data_name = "wrenchesConstrained_" + ss.str();
            datafields[data_name] = new Variant(wrenchesConstrained(i));
        }

        Eigen::VectorXf wrenchesMeasuredInRoot = debugOutputData.getUpToDateReadBuffer().wrenchesMeasuredInRoot;
        for (int i = 0; i < wrenchesMeasuredInRoot.rows(); ++i)
        {
            std::stringstream ss;
            ss << i;
            std::string data_name = "wrenchesMeasuredInRoot_" + ss.str();
            datafields[data_name] = new Variant(wrenchesMeasuredInRoot(i));
        }


        //        Eigen::VectorXf wrenchDMP = debugOutputData.getUpToDateReadBuffer().wrenchDMP;
        //        for (size_t i = 0; i < wrenchDMP.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "wrenchDMP_" + ss.str();
        //            datafields[data_name] = new Variant(wrenchDMP(i));
        //        }

        //        Eigen::VectorXf computedBoxWrench = debugOutputData.getUpToDateReadBuffer().computedBoxWrench;
        //        for (size_t i = 0; i < computedBoxWrench.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "computedBoxWrench_" + ss.str();
        //            datafields[data_name] = new Variant(computedBoxWrench(i));
        //        }


        datafields["modifiedPoseRight_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedPoseRight_x);
        datafields["modifiedPoseRight_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedPoseRight_y);
        datafields["modifiedPoseRight_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedPoseRight_z);
        datafields["currentPoseLeft_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPoseLeft_x);
        datafields["currentPoseLeft_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPoseLeft_y);
        datafields["currentPoseLeft_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPoseLeft_z);


        datafields["modifiedPoseLeft_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedPoseLeft_x);
        datafields["modifiedPoseLeft_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedPoseLeft_y);
        datafields["modifiedPoseLeft_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedPoseLeft_z);

        datafields["currentPoseRight_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPoseRight_x);
        datafields["currentPoseRight_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPoseRight_y);
        datafields["currentPoseRight_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPoseRight_z);
        datafields["dmpBoxPose_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().dmpBoxPose_x);
        datafields["dmpBoxPose_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().dmpBoxPose_y);
        datafields["dmpBoxPose_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().dmpBoxPose_z);
        datafields["dmpTwist_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().dmpTwist_x);
        datafields["dmpTwist_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().dmpTwist_y);
        datafields["dmpTwist_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().dmpTwist_z);

        datafields["modifiedTwist_lx"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedTwist_lx);
        datafields["modifiedTwist_ly"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedTwist_ly);
        datafields["modifiedTwist_lz"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedTwist_lz);
        datafields["modifiedTwist_rx"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedTwist_rx);
        datafields["modifiedTwist_ry"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedTwist_ry);
        datafields["modifiedTwist_rz"] = new Variant(debugOutputData.getUpToDateReadBuffer().modifiedTwist_rz);

        datafields["rx"] = new Variant(debugOutputData.getUpToDateReadBuffer().rx);
        datafields["ry"] = new Variant(debugOutputData.getUpToDateReadBuffer().ry);
        datafields["rz"] = new Variant(debugOutputData.getUpToDateReadBuffer().rz);


        debugObs->setDebugChannel("BimanualForceController", datafields);
    }

    void NJointBimanualForceController::onInitNJointController()
    {
        ARMARX_INFO << "init ...";
        runTask("NJointBimanualForceController", [&]
        {
            CycleUtil c(1);
            getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted);
            while (getState() == eManagedIceObjectStarted)
            {
                if (isControllerActive())
                {
                    controllerRun();
                }
                c.waitForCycleDuration();
            }
        });
    }

    void NJointBimanualForceController::onDisconnectNJointController()
    {
        ARMARX_INFO << "stopped ...";
    }
}

