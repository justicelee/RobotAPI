#include "NJointBimanualCCDMPController.h"

namespace armarx
{
    NJointControllerRegistration<NJointBimanualCCDMPController> registrationControllerNJointBimanualCCDMPController("NJointBimanualCCDMPController");

    NJointBimanualCCDMPController::NJointBimanualCCDMPController(armarx::NJointControllerDescriptionProviderInterfacePtr prov, const armarx::NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&)
    {
        ARMARX_INFO << "Preparing ... ";
        cfg = NJointBimanualCCDMPControllerConfigPtr::dynamicCast(config);
        ARMARX_CHECK_EXPRESSION(prov);
        RobotUnitPtr robotUnit = RobotUnitPtr::dynamicCast(prov);
        ARMARX_CHECK_EXPRESSION(robotUnit);

        leftRNS = robotUnit->getRtRobot()->getRobotNodeSet("LeftArm");
        rnsLeftBody = robotUnit->getRtRobot()->getRobotNodeSet("LeftArmCol");
        for (size_t i = 0; i < leftRNS->getSize(); ++i)
        {
            std::string jointName = leftRNS->getNode(i)->getName();

            leftJointNames.push_back(jointName);
            ControlTargetBase* ct = prov->getControlTarget(jointName, ControlModes::Torque1DoF);
            const SensorValueBase* sv = prov->getSensorValue(jointName);
            leftTargets.push_back(ct->asA<ControlTarget1DoFActuatorTorque>());
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
            const SensorValue1DoFActuatorAcceleration* accelerationSensor = sv->asA<SensorValue1DoFActuatorAcceleration>();

            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }

            if (!accelerationSensor)
            {
                ARMARX_WARNING << "No accelerationSensor available for " << jointName;
            }


            leftVelocitySensors.push_back(velocitySensor);
            leftPositionSensors.push_back(positionSensor);
            leftAccelerationSensors.push_back(accelerationSensor);

        };



        rnsRightBody = robotUnit->getRtRobot()->getRobotNodeSet("RightArmCol");

        rightRNS = robotUnit->getRtRobot()->getRobotNodeSet("RightArm");
        for (size_t i = 0; i < rightRNS->getSize(); ++i)
        {
            std::string jointName = rightRNS->getNode(i)->getName();
            rightJointNames.push_back(jointName);
            ControlTargetBase* ct = prov->getControlTarget(jointName, ControlModes::Torque1DoF);
            const SensorValueBase* sv = prov->getSensorValue(jointName);
            rightTargets.push_back(ct->asA<ControlTarget1DoFActuatorTorque>());
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
            const SensorValue1DoFActuatorAcceleration* accelerationSensor = sv->asA<SensorValue1DoFActuatorAcceleration>();


            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }
            if (!accelerationSensor)
            {
                ARMARX_WARNING << "No accelerationSensor available for " << jointName;
            }

            rightVelocitySensors.push_back(velocitySensor);
            rightPositionSensors.push_back(positionSensor);
            rightAccelerationSensors.push_back(accelerationSensor);

        };


        const SensorValueBase* svlf = prov->getSensorValue("FT L");
        leftForceTorque = svlf->asA<SensorValueForceTorque>();
        const SensorValueBase* svrf = prov->getSensorValue("FT R");
        rightForceTorque = svrf->asA<SensorValueForceTorque>();

        leftIK.reset(new VirtualRobot::DifferentialIK(leftRNS, leftRNS->getRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));
        rightIK.reset(new VirtualRobot::DifferentialIK(rightRNS, rightRNS->getRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));


        TaskSpaceDMPControllerConfig taskSpaceDMPConfig;
        taskSpaceDMPConfig.motionTimeDuration = cfg->timeDuration;
        taskSpaceDMPConfig.DMPKernelSize = cfg->kernelSize;
        taskSpaceDMPConfig.DMPMode = cfg->dmpMode;
        taskSpaceDMPConfig.DMPStyle = cfg->dmpType;
        taskSpaceDMPConfig.DMPAmplitude = 1.0;
        taskSpaceDMPConfig.phaseStopParas.goDist = cfg->phaseDist0;
        taskSpaceDMPConfig.phaseStopParas.backDist = cfg->phaseDist1;
        taskSpaceDMPConfig.phaseStopParas.Kpos = cfg->phaseKpPos;
        taskSpaceDMPConfig.phaseStopParas.Dpos = 0;
        taskSpaceDMPConfig.phaseStopParas.Kori = cfg->phaseKpOri;
        taskSpaceDMPConfig.phaseStopParas.Dori = 0;
        taskSpaceDMPConfig.phaseStopParas.mm2radi = cfg->posToOriRatio;
        taskSpaceDMPConfig.phaseStopParas.maxValue = cfg->phaseL;
        taskSpaceDMPConfig.phaseStopParas.slop = cfg->phaseK;


        TaskSpaceDMPControllerPtr leftLeader(new TaskSpaceDMPController("leftLeader", taskSpaceDMPConfig));
        TaskSpaceDMPControllerPtr leftFollower(new TaskSpaceDMPController("leftFollower", taskSpaceDMPConfig));
        TaskSpaceDMPControllerPtr rightLeader(new TaskSpaceDMPController("rightLeader", taskSpaceDMPConfig));
        TaskSpaceDMPControllerPtr rightFollower(new TaskSpaceDMPController("rightFollower", taskSpaceDMPConfig));

        leftGroup.push_back(leftLeader);
        leftGroup.push_back(rightFollower);

        rightGroup.push_back(rightLeader);
        rightGroup.push_back(leftFollower);

        bothLeaderGroup.push_back(leftLeader);
        bothLeaderGroup.push_back(rightLeader);


        tcpLeft = leftRNS->getTCP();
        tcpRight = rightRNS->getTCP();
        NJointBimanualCCDMPControllerSensorData initSensorData;
        initSensorData.deltaT = 0;
        initSensorData.currentTime = 0;
        initSensorData.currentLeftPose = leftRNS->getTCP()->getPoseInRootFrame();
        initSensorData.currentRightPose = rightRNS->getTCP()->getPoseInRootFrame();
        controllerSensorData.reinitAllBuffers(initSensorData);

        leaderName = "both";

        NJointBimanualCCDMPControllerControlData initData;
        initData.leftTargetVel.resize(6);
        initData.leftTargetVel.setZero();
        initData.rightTargetVel.resize(6);
        initData.rightTargetVel.setZero();
        reinitTripleBuffer(initData);

        leftDesiredJointValues.resize(leftTargets.size());
        ARMARX_CHECK_EQUAL(cfg->leftDesiredJointValues.size(), leftTargets.size());

        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            leftDesiredJointValues(i) = cfg->leftDesiredJointValues.at(i);
        }

        rightDesiredJointValues.resize(rightTargets.size());
        ARMARX_CHECK_EQUAL(cfg->rightDesiredJointValues.size(), rightTargets.size());

        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            rightDesiredJointValues(i) = cfg->rightDesiredJointValues.at(i);
        }

        KoriFollower = cfg->KoriFollower;
        KposFollower = cfg->KposFollower;

        kpos << cfg->Kpos[0], cfg->Kpos[1], cfg->Kpos[2];
        dpos << cfg->Dpos[0], cfg->Dpos[1], cfg->Dpos[2];
        kori << cfg->Kori[0], cfg->Kori[1], cfg->Kori[2];
        dori << cfg->Dori[0], cfg->Dori[1], cfg->Dori[2];


        knull = cfg->knull;
        dnull = cfg->dnull;

        torqueLimit = cfg->torqueLimit;

        kpf.resize(12);
        kif.resize(12);
        forceC_des.resize(12);

        ARMARX_CHECK_EQUAL(cfg->Kpf.size(), kpf.rows());
        ARMARX_CHECK_EQUAL(cfg->Kif.size(), kif.rows());
        ARMARX_CHECK_EQUAL(cfg->DesiredForce.size(), forceC_des.rows());


        for (size_t i = 0; i < 12; i++)
        {
            kpf(i) = cfg->Kpf.at(i);
            kif(i) = cfg->Kif.at(i);
            forceC_des(i) = cfg->DesiredForce.at(i);
        }

        boxWidth = cfg->BoxWidth;

        filtered_leftForce.setZero();
        filtered_leftTorque.setZero();
        filtered_rightForce.setZero();
        filtered_rightTorque.setZero();

        //        offset_leftForce.setZero();
        //        offset_leftTorque.setZero();
        //        offset_rightForce.setZero();
        //        offset_rightTorque.setZero();

        offset_leftForce(0) = -4.34;
        offset_leftForce(1) = -8.21;
        offset_leftForce(2) = 15.59;

        offset_leftTorque(0) = 1.42;
        offset_leftTorque(1) = 0.29;
        offset_leftTorque(2) = 0.09;

        offset_rightForce(0) = 2.88;
        offset_rightForce(1) = 11.15;
        offset_rightForce(2) = -20.18;

        offset_rightTorque(0) = -1.83;
        offset_rightTorque(1) = 0.34;
        offset_rightTorque(2) = -0.01;


        filterTimeConstant = cfg->FilterTimeConstant;

        ftPIDController.resize(12);
        for (size_t i = 0; i < ftPIDController.size(); i++)
        {
            ftPIDController[i].reset(new PIDController(kpf(i), kif(i), 0.0, 10, 10));
        }

        isForceCompensateDone = false;

    }

    std::string NJointBimanualCCDMPController::getClassName(const Ice::Current&) const
    {
        return "NJointBimanualCCDMPController";
    }

    void NJointBimanualCCDMPController::controllerRun()
    {
        if (!controllerSensorData.updateReadBuffer())
        {
            return;
        }

        double deltaT = controllerSensorData.getReadBuffer().deltaT;


        Eigen::VectorXf leftTargetVel;
        leftTargetVel.resize(6);
        leftTargetVel.setZero();
        Eigen::VectorXf rightTargetVel;
        rightTargetVel.resize(6);
        rightTargetVel.setZero();

        std::vector<TaskSpaceDMPControllerPtr > currentControlGroup;
        Eigen::Matrix4f currentLeaderPose;
        Eigen::Matrix4f currentFollowerPose;
        Eigen::VectorXf currentLeaderTwist;
        Eigen::VectorXf currentFollowerTwist;
        if (leaderName == "Left")
        {
            currentControlGroup = leftGroup;
            currentLeaderPose = controllerSensorData.getReadBuffer().currentLeftPose;
            currentFollowerPose = controllerSensorData.getReadBuffer().currentRightPose;
            currentLeaderTwist = controllerSensorData.getReadBuffer().currentLeftTwist;
            currentFollowerTwist = controllerSensorData.getReadBuffer().currentRightTwist;
        }
        else if (leaderName == "right")
        {
            currentControlGroup = rightGroup;
            currentLeaderPose = controllerSensorData.getReadBuffer().currentRightPose;
            currentFollowerPose = controllerSensorData.getReadBuffer().currentLeftPose;
            currentLeaderTwist = controllerSensorData.getReadBuffer().currentRightTwist;
            currentFollowerTwist = controllerSensorData.getReadBuffer().currentLeftTwist;
        }
        else
        {
            currentControlGroup = bothLeaderGroup;

            TaskSpaceDMPControllerPtr leaderDMPleft = currentControlGroup[0];
            TaskSpaceDMPControllerPtr leaderDMPright = currentControlGroup[1];
            leaderDMPleft->flow(deltaT, controllerSensorData.getReadBuffer().currentLeftPose, controllerSensorData.getReadBuffer().currentLeftTwist);
            leaderDMPright->flow(deltaT, controllerSensorData.getReadBuffer().currentRightPose, controllerSensorData.getReadBuffer().currentRightTwist);

            Eigen::VectorXf leftTargetVel = leaderDMPleft->getTargetVelocity();
            Eigen::Matrix4f leftTargetPose = leaderDMPleft->getTargetPoseMat();
            Eigen::VectorXf rightTargetVel = leaderDMPright->getTargetVelocity();
            Eigen::Matrix4f rightTargetPose = leaderDMPright->getTargetPoseMat();

            virtualtimer = leaderDMPleft->canVal;
            LockGuardType guard {controlDataMutex};
            getWriterControlStruct().leftTargetVel = leftTargetVel;
            getWriterControlStruct().rightTargetVel = rightTargetVel;
            getWriterControlStruct().leftTargetPose = leftTargetPose;
            getWriterControlStruct().rightTargetPose = rightTargetPose;
            writeControlStruct();

            return;
        }

        TaskSpaceDMPControllerPtr leaderDMP = currentControlGroup[0];
        TaskSpaceDMPControllerPtr followerDMP = currentControlGroup[1];


        leaderDMP->flow(deltaT, currentLeaderPose, currentLeaderTwist);

        Eigen::Matrix4f currentFollowerLocalPose;
        currentFollowerLocalPose.block<3, 3>(0, 0) = currentFollowerPose.block<3, 3>(0, 0) * currentLeaderPose.block<3, 3>(0, 0).inverse();
        currentFollowerLocalPose.block<3, 1>(0, 3) = currentFollowerLocalPose.block<3, 3>(0, 0) * (currentFollowerPose.block<3, 1>(0, 3) - currentLeaderPose.block<3, 1>(0, 3));
        followerDMP->flow(deltaT, currentFollowerPose, currentFollowerTwist);

        Eigen::VectorXf leaderTargetVel = leaderDMP->getTargetVelocity();
        Eigen::Matrix4f leaderTargetPose = leaderDMP->getTargetPoseMat();
        Eigen::Matrix4f followerLocalTargetPose = followerDMP->getTargetPoseMat();
        std::vector<double> followerLocalTargetPoseVec = followerDMP->getTargetPose();

        Eigen::Matrix4f followerTargetPose;
        followerTargetPose.block<3, 3>(0, 0) = followerLocalTargetPose.block<3, 3>(0, 0) * currentLeaderPose.block<3, 3>(0, 0);
        followerTargetPose.block<3, 1>(0, 3) = currentLeaderPose.block<3, 3>(0, 0) * followerLocalTargetPose.block<3, 1>(0, 3) + currentLeaderPose.block<3, 1>(0, 3);


        Eigen::VectorXf followerLocalTargetVel = followerDMP->getTargetVelocity();
        Eigen::VectorXf followerTargetVel = followerLocalTargetVel;
        //        followerTargetVel.block<3, 1>(0, 0) = currentLeaderPose.block<3, 3>(0, 0) * followerLocalTargetVel.block<3, 1>(0, 0) + leaderTargetVel.block<3, 1>(0, 0);
        followerTargetVel.block<3, 1>(0, 0) = KposFollower * (followerTargetPose.block<3, 1>(0, 3) - currentFollowerPose.block<3, 1>(0, 3));


        Eigen::Matrix3f followerDiffMat =  followerTargetPose.block<3, 3>(0, 0) * currentFollowerPose.block<3, 3>(0, 0).inverse();
        Eigen::Vector3f followerDiffRPY = KoriFollower * VirtualRobot::MathTools::eigen3f2rpy(followerDiffMat);
        followerTargetVel(3) = followerDiffRPY(0);
        followerTargetVel(4) = followerDiffRPY(1);
        followerTargetVel(5) = followerDiffRPY(2);

        virtualtimer = leaderDMP->canVal;


        std::vector<double> leftDMPTarget;
        std::vector<double> rightDMPTarget;

        Eigen::Matrix4f leftTargetPose;
        Eigen::Matrix4f rightTargetPose;

        if (leaderName == "Left")
        {
            leftTargetVel = leaderTargetVel;
            rightTargetVel = followerTargetVel;

            leftDMPTarget = leaderDMP->getTargetPose();
            rightDMPTarget = followerLocalTargetPoseVec;


            leftTargetPose = leaderTargetPose;
            rightTargetPose = followerLocalTargetPose;
        }
        else if (leaderName == "right")
        {
            rightTargetVel = leaderTargetVel;
            leftTargetVel = followerTargetVel;

            rightDMPTarget = leaderDMP->getTargetPose();
            leftDMPTarget = followerLocalTargetPoseVec;

            rightTargetPose = leaderTargetPose;
            leftTargetPose = followerLocalTargetPose;

        }





        //        debugOutputData.getWriteBuffer().latestTargetVelocities["LeftArm_xvel"] = leftTargetVel(0);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["LeftArm_yvel"] = leftTargetVel(1);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["LeftArm_zvel"] = leftTargetVel(2);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["LeftArm_rollvel"] = leftTargetVel(3);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["LeftArm_pitchvel"] = leftTargetVel(4);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["LeftArm_yawvel"] = leftTargetVel(5);

        //        debugOutputData.getWriteBuffer().latestTargetVelocities["RightArm_xvel"] = rightTargetVel(0);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["RightArm_yvel"] = rightTargetVel(1);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["RightArm_zvel"] = rightTargetVel(2);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["RightArm_rollvel"] = rightTargetVel(3);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["RightArm_pitchvel"] = rightTargetVel(4);
        //        debugOutputData.getWriteBuffer().latestTargetVelocities["RightArm_yawvel"] = rightTargetVel(5);


        //        debugOutputData.getWriteBuffer().dmpTargets["LeftArm_dmp_x"] = leftDMPTarget[0];
        //        debugOutputData.getWriteBuffer().dmpTargets["LeftArm_dmp_y"] = leftDMPTarget[1];
        //        debugOutputData.getWriteBuffer().dmpTargets["LeftArm_dmp_z"] = leftDMPTarget[2];
        //        debugOutputData.getWriteBuffer().dmpTargets["LeftArm_dmp_qw"] = leftDMPTarget[3];
        //        debugOutputData.getWriteBuffer().dmpTargets["LeftArm_dmp_qx"] = leftDMPTarget[4];
        //        debugOutputData.getWriteBuffer().dmpTargets["LeftArm_dmp_qy"] = leftDMPTarget[5];
        //        debugOutputData.getWriteBuffer().dmpTargets["LeftArm_dmp_qz"] = leftDMPTarget[6];

        //        debugOutputData.getWriteBuffer().dmpTargets["RightArm_dmp_x"] = rightDMPTarget[0];
        //        debugOutputData.getWriteBuffer().dmpTargets["RightArm_dmp_y"] = rightDMPTarget[1];
        //        debugOutputData.getWriteBuffer().dmpTargets["RightArm_dmp_z"] = rightDMPTarget[2];
        //        debugOutputData.getWriteBuffer().dmpTargets["RightArm_dmp_qw"] = rightDMPTarget[3];
        //        debugOutputData.getWriteBuffer().dmpTargets["RightArm_dmp_qx"] = rightDMPTarget[4];
        //        debugOutputData.getWriteBuffer().dmpTargets["RightArm_dmp_qy"] = rightDMPTarget[5];
        //        debugOutputData.getWriteBuffer().dmpTargets["RightArm_dmp_qz"] = rightDMPTarget[6];

        //        std::vector<double> currentLeftPoseVec = leaderDMP->eigen4f2vec(controllerSensorData.getReadBuffer().currentLeftPose);
        //        std::vector<double> currentRightPoseVec = leaderDMP->eigen4f2vec(controllerSensorData.getReadBuffer().currentRightPose);


        //        debugOutputData.getWriteBuffer().currentPose["leftPose_x"] = currentLeftPoseVec[0];
        //        debugOutputData.getWriteBuffer().currentPose["leftPose_y"] = currentLeftPoseVec[1];
        //        debugOutputData.getWriteBuffer().currentPose["leftPose_z"] = currentLeftPoseVec[2];
        //        debugOutputData.getWriteBuffer().currentPose["leftPose_qw"] = currentLeftPoseVec[3];
        //        debugOutputData.getWriteBuffer().currentPose["leftPose_qx"] = currentLeftPoseVec[4];
        //        debugOutputData.getWriteBuffer().currentPose["leftPose_qy"] = currentLeftPoseVec[5];
        //        debugOutputData.getWriteBuffer().currentPose["leftPose_qz"] = currentLeftPoseVec[6];

        //        debugOutputData.getWriteBuffer().currentPose["rightPose_x"] = currentRightPoseVec[0];
        //        debugOutputData.getWriteBuffer().currentPose["rightPose_y"] = currentRightPoseVec[1];
        //        debugOutputData.getWriteBuffer().currentPose["rightPose_z"] = currentRightPoseVec[2];
        //        debugOutputData.getWriteBuffer().currentPose["rightPose_qw"] = currentRightPoseVec[3];
        //        debugOutputData.getWriteBuffer().currentPose["rightPose_qx"] = currentRightPoseVec[4];
        //        debugOutputData.getWriteBuffer().currentPose["rightPose_qy"] = currentRightPoseVec[5];
        //        debugOutputData.getWriteBuffer().currentPose["rightPose_qz"] = currentRightPoseVec[6];


        //        debugOutputData.getWriteBuffer().leaderCanVal = leaderDMP->debugData.canVal;
        //        debugOutputData.getWriteBuffer().leadermpcFactor =  leaderDMP->debugData.mpcFactor;
        //        debugOutputData.getWriteBuffer().leadererror = leaderDMP->debugData.poseError;
        //        debugOutputData.getWriteBuffer().leaderposError = leaderDMP->debugData.posiError;
        //        debugOutputData.getWriteBuffer().leaderoriError = leaderDMP->debugData.oriError;

        //        debugOutputData.getWriteBuffer().followerCanVal = followerDMP->debugData.canVal;
        //        debugOutputData.getWriteBuffer().followermpcFactor =  followerDMP->debugData.mpcFactor;
        //        debugOutputData.getWriteBuffer().followererror = followerDMP->debugData.poseError;
        //        debugOutputData.getWriteBuffer().followerposError = followerDMP->debugData.posiError;
        //        debugOutputData.getWriteBuffer().followeroriError = followerDMP->debugData.oriError;

        //        debugOutputData.commitWrite();







        LockGuardType guard {controlDataMutex};
        getWriterControlStruct().leftTargetVel = leftTargetVel;
        getWriterControlStruct().rightTargetVel = rightTargetVel;
        getWriterControlStruct().leftTargetPose = leftTargetPose;
        getWriterControlStruct().rightTargetPose = rightTargetPose;
        writeControlStruct();
    }

    Eigen::VectorXf NJointBimanualCCDMPController::getControlWrench(const Eigen::VectorXf& tcptwist, const Eigen::Matrix4f& currentPose, const Eigen::Matrix4f& targetPose)
    {
        //        Eigen::Vector3f currentTCPLinearVelocity;
        //        currentTCPLinearVelocity << 0.001 * tcptwist(0),   0.001 * tcptwist(1), 0.001 * tcptwist(2);
        //        Eigen::Vector3f currentTCPAngularVelocity;
        //        currentTCPAngularVelocity << tcptwist(3),   tcptwist(4),  tcptwist(5);

        //        Eigen::Vector3f currentTCPPosition = currentPose.block<3,1>(0,3);
        //        Eigen::Vector3f desiredPosition = targetPose.block<3,1>(0,3);
        //        Eigen::Vector3f tcpForces = 0.001 * kpos.cwiseProduct(desiredPosition - currentTCPPosition);
        //        Eigen::Vector3f tcpDesiredForce = tcpForces - dpos.cwiseProduct(currentTCPLinearVelocity);

        //        Eigen::Matrix3f currentRotMat = currentPose.block<3, 3>(0, 0);
        //        Eigen::Matrix3f diffMat = targetPose.block<3,3>(0,0) * currentRotMat.inverse();
        //        Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen3f2rpy(diffMat);

        //        Eigen::Vector3f tcpDesiredTorque = kori.cwiseProduct(rpy) - dori.cwiseProduct(currentTCPAngularVelocity);
        //        Eigen::Vector6f tcpDesiredWrench;
        //        tcpDesiredWrench <<   0.001 * tcpDesiredForce, tcpDesiredTorque;

        //        return tcpDesiredWrench;

    }



    void NJointBimanualCCDMPController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        double deltaT = timeSinceLastIteration.toSecondsDouble();
        controllerSensorData.getWriteBuffer().currentLeftPose = tcpLeft->getPoseInRootFrame();
        controllerSensorData.getWriteBuffer().currentRightPose = tcpRight->getPoseInRootFrame();
        controllerSensorData.getWriteBuffer().deltaT = deltaT;
        controllerSensorData.getWriteBuffer().currentTime += deltaT;


        //        if(controllerSensorData.getWriteBuffer().currentTime > 0.3 && !isForceCompensateDone)
        //        {
        //            offset_leftForce = filtered_leftForce;
        //            offset_leftTorque = filtered_leftTorque;
        //            offset_rightForce = filtered_rightForce;
        //            offset_rightTorque = filtered_rightTorque;
        //            isForceCompensateDone = true;


        //            filtered_leftForce.setZero();
        //            filtered_leftTorque.setZero();
        //            filtered_rightForce.setZero();
        //            filtered_rightTorque.setZero();
        //        }

        //        if(controllerSensorData.getWriteBuffer().currentTime <= 0.3)
        //        {
        //            // torque limit
        //            for (size_t i = 0; i < leftTargets.size(); ++i)
        //            {
        //                leftTargets.at(i)->torque = 0;
        //            }

        //            for (size_t i = 0; i < rightTargets.size(); ++i)
        //            {
        //                rightTargets.at(i)->torque = 0;
        //            }

        //            return;
        //        }

        // cartesian vel controller
        Eigen::MatrixXf I = Eigen::MatrixXf::Identity(leftTargets.size(), leftTargets.size());

        Eigen::MatrixXf jacobiL = leftIK->getJacobianMatrix(tcpLeft, VirtualRobot::IKSolver::CartesianSelection::All);
        jacobiL.block<3, 8>(0, 0) = 0.001 * jacobiL.block<3, 8>(0, 0);

        Eigen::VectorXf leftqpos;
        Eigen::VectorXf leftqvel;
        leftqpos.resize(leftPositionSensors.size());
        leftqvel.resize(leftVelocitySensors.size());
        for (size_t i = 0; i < leftVelocitySensors.size(); ++i)
        {
            leftqpos(i) = leftPositionSensors[i]->position;
            leftqvel(i) = leftVelocitySensors[i]->velocity;
        }

        Eigen::MatrixXf jacobiR = rightIK->getJacobianMatrix(tcpRight, VirtualRobot::IKSolver::CartesianSelection::All);
        jacobiR.block<3, 8>(0, 0) = 0.001 * jacobiR.block<3, 8>(0, 0);


        Eigen::VectorXf rightqpos;
        Eigen::VectorXf rightqvel;
        rightqpos.resize(rightPositionSensors.size());
        rightqvel.resize(rightVelocitySensors.size());

        for (size_t i = 0; i < rightVelocitySensors.size(); ++i)
        {
            rightqpos(i) = rightPositionSensors[i]->position;
            rightqvel(i) = rightVelocitySensors[i]->velocity;
        }

        Eigen::VectorXf currentLeftTwist = jacobiL * leftqvel;
        Eigen::VectorXf currentRightTwist = jacobiR * rightqvel;


        controllerSensorData.getWriteBuffer().currentLeftTwist = currentLeftTwist;
        controllerSensorData.getWriteBuffer().currentRightTwist = currentRightTwist;
        controllerSensorData.commitWrite();


        Eigen::Matrix4f leftTargetPose = rtGetControlStruct().leftTargetPose;
        Eigen::Matrix4f rightTargetPose = rtGetControlStruct().rightTargetPose;
        Eigen::VectorXf leftTargetVel = rtGetControlStruct().leftTargetVel;
        Eigen::VectorXf rightTargetVel = rtGetControlStruct().rightTargetVel;
        Eigen::Matrix4f leftCurrentPose = tcpLeft->getPoseInRootFrame();
        Eigen::Matrix4f rightCurrentPose = tcpRight->getPoseInRootFrame();

        // Todo: force control
        int nl = leftRNS->getSize();
        int nr = rightRNS->getSize();
        int n = nl + nr;
        // GraspMatrix

        //        Eigen::Matrix4f poseLeftInRightCoordinate = tcpRight->toLocalCoordinateSystem(tcpLeft->getGlobalPose());
        //        Eigen::Vector3f positionLeftInRightCoordinate;
        //        positionLeftInRightCoordinate << poseLeftInRightCoordinate(0,3), poseLeftInRightCoordinate(1,3), poseLeftInRightCoordinate(2,3);
        //        boxWidth = 0.001 * 0.5 * positionLeftInRightCoordinate.norm();


        Eigen::Vector3f localDistanceCoM;
        localDistanceCoM << 0.5 * boxWidth, 0, 0;


        Eigen::Vector3f rl = -leftCurrentPose.block<3, 3>(0, 0) * localDistanceCoM; //tcpLeft->getRobot()->getRootNode()->toLocalCoordinateSystem(tcpLeft->toGlobalCoordinateSystemVec(localDistanceCoM));
        Eigen::Vector3f rr = rightCurrentPose.block<3, 3>(0, 0) * localDistanceCoM;
        Eigen::MatrixXf leftGraspMatrix = Eigen::MatrixXf::Identity(6, 6);
        Eigen::MatrixXf rightGraspMatrix = Eigen::MatrixXf::Identity(6, 6);
        leftGraspMatrix.block<3, 3>(3, 0) << 0, -rl(2), rl(1),
                              rl(2), 0, -rl(0),
                              -rl(1), rl(0), 0;
        rightGraspMatrix.block<3, 3>(3, 0) << 0, -rr(2), rr(1),
                               rr(2), 0, -rr(0),
                               -rr(1), rr(0), 0;
        Eigen::MatrixXf graspMatrix;
        graspMatrix.resize(6, 12);
        graspMatrix << leftGraspMatrix, rightGraspMatrix;

        // constraint Jacobain and projection matrix
        Eigen::MatrixXf jacobi;
        jacobi.resize(12, n);
        jacobi.setZero(12, n);
        jacobi.block < 6, 8 > (0, 0) = jacobiL;
        jacobi.block < 6, 8 > (6, 8) = jacobiR;

        Eigen::MatrixXf pinvGraspMatrix = leftIK->computePseudoInverseJacobianMatrix(graspMatrix, 1);
        Eigen::MatrixXf proj2GraspNullspace = Eigen::MatrixXf::Identity(12, 12) - pinvGraspMatrix * graspMatrix;
        Eigen::MatrixXf jacobiConstrained = proj2GraspNullspace * jacobi;

        Eigen::MatrixXf pinvJacobiC = leftIK->computePseudoInverseJacobianMatrix(jacobiConstrained, 1);
        Eigen::MatrixXf projection = Eigen::MatrixXf::Identity(n, n) - pinvJacobiC * jacobiConstrained;



        // calculate inertia matrix
        Eigen::MatrixXf leftInertialMatrix;
        Eigen::MatrixXf rightInertialMatrix;
        leftInertialMatrix.setZero(nl, nl);
        rightInertialMatrix.setZero(nr, nr);

        for (size_t i = 0; i < rnsLeftBody->getNodeNames().size(); ++i)
        {
            VirtualRobot::RobotNodePtr rnbody = rnsLeftBody->getNode(i);
            VirtualRobot::RobotNodePtr rnjoint = leftRNS->getNode(i + 1);

            Eigen::MatrixXf jacobipos_rn = 0.001 * leftIK->getJacobianMatrix(rnjoint, VirtualRobot::IKSolver::CartesianSelection::Position);
            Eigen::MatrixXf jacobiori_rn = leftIK->getJacobianMatrix(rnjoint, VirtualRobot::IKSolver::CartesianSelection::Orientation);


            float m = rnbody->getMass();
            Eigen::Matrix3f InertiaMatrix = rnbody->getInertiaMatrix();
            leftInertialMatrix += m * jacobipos_rn.transpose() * jacobipos_rn + jacobiori_rn.transpose() * InertiaMatrix * jacobiori_rn;
        }

        for (size_t i = 0; i < rnsRightBody->getNodeNames().size(); ++i)
        {
            VirtualRobot::RobotNodePtr rnbody = rnsRightBody->getNode(i);
            VirtualRobot::RobotNodePtr rnjoint = rightRNS->getNode(i + 1);

            Eigen::MatrixXf jacobipos_rn = 0.001 * rightIK->getJacobianMatrix(rnjoint, VirtualRobot::IKSolver::CartesianSelection::Position);
            Eigen::MatrixXf jacobiori_rn = rightIK->getJacobianMatrix(rnjoint, VirtualRobot::IKSolver::CartesianSelection::Orientation);


            float m = rnbody->getMass();
            Eigen::Matrix3f InertiaMatrix = rnbody->getInertiaMatrix();

            rightInertialMatrix += m * jacobipos_rn.transpose() * jacobipos_rn + jacobiori_rn.transpose() * InertiaMatrix * jacobiori_rn;
        }
        Eigen::MatrixXf M = Eigen::MatrixXf::Zero(n, n);
        M.topLeftCorner(nl, nl) = leftInertialMatrix;
        M.bottomRightCorner(nr, nr) = rightInertialMatrix;

        Eigen::MatrixXf Mc = M + projection * M - M * projection;


        // force filter and measure
        double filterFactor = 1 / ((filterTimeConstant / deltaT) + 1);

        filtered_leftForce = (1 - filterFactor) * filtered_leftForce + filterFactor * (leftForceTorque->force - offset_leftForce);
        filtered_leftTorque = (1 - filterFactor) * filtered_leftTorque + filterFactor * (leftForceTorque->torque - offset_leftTorque);
        filtered_rightForce = (1 - filterFactor) * filtered_rightForce + filterFactor * (rightForceTorque->force - offset_rightForce);
        filtered_rightTorque = (1 - filterFactor) * filtered_rightForce + filterFactor * (rightForceTorque->torque - offset_rightTorque);

        Eigen::VectorXf filteredForce;
        filteredForce.resize(12);
        filteredForce << filtered_leftForce, filtered_leftTorque, filtered_rightForce, filtered_rightTorque;
        filteredForce.block<3, 1>(0, 0) = leftCurrentPose.block<3, 3>(0, 0) * filteredForce.block<3, 1>(0, 0);
        filteredForce.block<3, 1>(3, 0) = leftCurrentPose.block<3, 3>(0, 0) * filteredForce.block<3, 1>(3, 0);
        filteredForce.block<3, 1>(6, 0) = rightCurrentPose.block<3, 3>(0, 0) * filteredForce.block<3, 1>(6, 0);
        filteredForce.block<3, 1>(9, 0) = rightCurrentPose.block<3, 3>(0, 0) * filteredForce.block<3, 1>(9, 0);


        // calculate force control part
        Eigen::VectorXf constrainedForceMeasure = proj2GraspNullspace  * filteredForce;

        constrainedForceMeasure.block<3, 1>(0, 0) = leftCurrentPose.block<3, 3>(0, 0).transpose() * constrainedForceMeasure.block<3, 1>(0, 0);
        constrainedForceMeasure.block<3, 1>(3, 0) = leftCurrentPose.block<3, 3>(0, 0).transpose() * constrainedForceMeasure.block<3, 1>(3, 0);
        constrainedForceMeasure.block<3, 1>(6, 0) = rightCurrentPose.block<3, 3>(0, 0).transpose() * constrainedForceMeasure.block<3, 1>(6, 0);
        constrainedForceMeasure.block<3, 1>(9, 0) = rightCurrentPose.block<3, 3>(0, 0).transpose() * constrainedForceMeasure.block<3, 1>(9, 0);
        Eigen::VectorXf forceConstrained = Eigen::VectorXf::Zero(12);
        for (size_t i = 0; i < 12; i++)
        {
            ftPIDController[i]->update(constrainedForceMeasure(i), forceC_des(i));
            forceConstrained(i) = ftPIDController[i]->getControlValue() + forceC_des(i);
            //            forceConstrained(i) = forceC_des(i) + kpf(i) * (forceC_des(i) - filtered_leftForce(i)); // TODO: add PID controller
            //            forceConstrained(i + 3) = forceC_des(i + 3) + kpf(i + 3) * (forceC_des(i + 3) - filtered_leftTorque(i)); // TODO: add PID controller
            //            forceConstrained(i + 6) = forceC_des(i + 6) + kpf(i + 6) * (forceC_des(i + 6) - filtered_rightForce(i)); // TODO: add PID controller
            //            forceConstrained(i + 9) = forceC_des(i + 9) + kpf(i + 9) * (forceC_des(i + 9) - filtered_rightTorque(i)); // TODO: add PID controller
        }
        forceConstrained.block<3, 1>(0, 0) = leftCurrentPose.block<3, 3>(0, 0) * forceConstrained.block<3, 1>(0, 0);
        forceConstrained.block<3, 1>(3, 0) = leftCurrentPose.block<3, 3>(0, 0) * forceConstrained.block<3, 1>(3, 0);
        forceConstrained.block<3, 1>(6, 0) = rightCurrentPose.block<3, 3>(0, 0) * forceConstrained.block<3, 1>(6, 0);
        forceConstrained.block<3, 1>(9, 0) = rightCurrentPose.block<3, 3>(0, 0) * forceConstrained.block<3, 1>(9, 0);






        // unconstrained space controller
        Eigen::Vector6f leftJointControlWrench;
        {
            Eigen::Vector3f targetTCPLinearVelocity;
            targetTCPLinearVelocity << 0.001 * leftTargetVel(0), 0.001 * leftTargetVel(1), 0.001 * leftTargetVel(2);

            Eigen::Vector3f currentTCPLinearVelocity;
            currentTCPLinearVelocity <<  currentLeftTwist(0),   currentLeftTwist(1),  currentLeftTwist(2);
            Eigen::Vector3f currentTCPAngularVelocity;
            currentTCPAngularVelocity << currentLeftTwist(3),   currentLeftTwist(4),  currentLeftTwist(5);
            Eigen::Vector3f currentTCPPosition = leftCurrentPose.block<3, 1>(0, 3);
            Eigen::Vector3f desiredPosition = leftTargetPose.block<3, 1>(0, 3);

            Eigen::Vector3f tcpDesiredForce = 0.001 * kpos.cwiseProduct(desiredPosition - currentTCPPosition) + dpos.cwiseProduct(targetTCPLinearVelocity - currentTCPLinearVelocity);
            Eigen::Matrix3f currentRotMat = leftCurrentPose.block<3, 3>(0, 0);
            Eigen::Matrix3f diffMat = leftTargetPose.block<3, 3>(0, 0) * currentRotMat.inverse();
            Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
            Eigen::Vector3f tcpDesiredTorque = kori.cwiseProduct(rpy) - dori.cwiseProduct(currentTCPAngularVelocity);
            leftJointControlWrench <<  tcpDesiredForce, tcpDesiredTorque;
        }


        Eigen::Vector6f rightJointControlWrench;
        {
            Eigen::Vector3f targetTCPLinearVelocity;
            targetTCPLinearVelocity << 0.001 * rightTargetVel(0), 0.001 * rightTargetVel(1), 0.001 * rightTargetVel(2);

            Eigen::Vector3f currentTCPLinearVelocity;
            currentTCPLinearVelocity <<  currentRightTwist(0),  currentRightTwist(1), currentRightTwist(2);
            Eigen::Vector3f currentTCPAngularVelocity;
            currentTCPAngularVelocity << currentRightTwist(3),   currentRightTwist(4),  currentRightTwist(5);
            Eigen::Vector3f currentTCPPosition = rightCurrentPose.block<3, 1>(0, 3);
            Eigen::Vector3f desiredPosition = rightTargetPose.block<3, 1>(0, 3);

            Eigen::Vector3f tcpDesiredForce = 0.001 * kpos.cwiseProduct(desiredPosition - currentTCPPosition) - dpos.cwiseProduct(targetTCPLinearVelocity - currentTCPLinearVelocity);
            Eigen::Matrix3f currentRotMat = rightCurrentPose.block<3, 3>(0, 0);
            Eigen::Matrix3f diffMat = rightTargetPose.block<3, 3>(0, 0) * currentRotMat.inverse();
            Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
            Eigen::Vector3f tcpDesiredTorque = kori.cwiseProduct(rpy) - dori.cwiseProduct(currentTCPAngularVelocity);
            rightJointControlWrench <<   tcpDesiredForce, tcpDesiredTorque;
        }

        Eigen::VectorXf forceUnconstrained(12);
        forceUnconstrained << leftJointControlWrench, rightJointControlWrench;

        Eigen::VectorXf unConstrainedForceMeasure = pinvGraspMatrix * graspMatrix * filteredForce;

        Eigen::MatrixXf taskSpaceInertia = (jacobi * M.inverse() *  jacobi.transpose()).inverse();



        forceUnconstrained = taskSpaceInertia * forceUnconstrained ;//+ unConstrainedForceMeasure) - unConstrainedForceMeasure;

        Eigen::VectorXf leftNullspaceTorque = knull * (leftDesiredJointValues - leftqpos) - dnull * leftqvel;
        Eigen::VectorXf rightNullspaceTorque = knull * (rightDesiredJointValues - rightqpos) - dnull * rightqvel;
        float lambda = 2;

        Eigen::MatrixXf jtpinvL = leftIK->computePseudoInverseJacobianMatrix(jacobiL.transpose(), lambda);
        Eigen::MatrixXf jtpinvR = leftIK->computePseudoInverseJacobianMatrix(jacobiR.transpose(), lambda);
        forceUnconstrained.head(8) = forceUnconstrained.head(8) + (I - jacobiL.transpose() * jtpinvL) * leftNullspaceTorque;
        forceUnconstrained.tail<8>() = forceUnconstrained.tail<8>() + (I - jacobiR.transpose() * jtpinvR) * rightNullspaceTorque;


        //
        ////        Eigen::VectorXf leftJointDesiredTorques = jacobiL.transpose() * leftJointControlWrench + (I - jacobiL.transpose() * jtpinvL) * leftNullspaceTorque;
        //        Eigen::VectorXf leftJointDesiredTorques = jacobiL.transpose() * forceUnconstrained.head(6) + (I - jacobiL.transpose() * jtpinvL) * leftNullspaceTorque;

        //
        ////        Eigen::VectorXf rightJointDesiredTorques = jacobiR.transpose() * rightJointControlWrench + (I - jacobiR.transpose() * jtpinvR) * rightNullspaceTorque;
        //        Eigen::VectorXf rightJointDesiredTorques = jacobiR.transpose() * forceUnconstrained.tail<6>() + (I - jacobiR.transpose() * jtpinvR) * rightNullspaceTorque;

        //        Eigen::VectorXf torqueUnconstrained(16);
        //        torqueUnconstrained << leftJointDesiredTorques, rightJointDesiredTorques;

        // constrained space control

        //        Eigen::MatrixXf mu = (Eigen::MatrixXf::Identity(n, n) - projection) * M * Mc.inverse();
        //        Eigen::VectorXf qacc;
        //        qacc.resize(n);
        //        for (size_t i = 0; i < leftAccelerationSensors.size(); ++i)
        //        {
        //            qacc(i) = 0.001 * leftAccelerationSensors[i]->acceleration;
        //        }

        //        for (size_t i = 0; i < rightAccelerationSensors.size(); ++i)
        //        {
        //            qacc(i + leftAccelerationSensors.size()) = 0.001 * rightAccelerationSensors[i]->acceleration;
        //        }


        //        Eigen::VectorXf torqueConstrained = mu * (torqueUnconstrained + (Eigen::MatrixXf::Identity(n, n) - projection) * qacc) - jacobiConstrained.transpose() * forceConstrained;


        // all torques



        //        Eigen::VectorXf torqueInput = projection * torqueUnconstrained + projection * torqueConstrained;
        //        Eigen::VectorXf torqueInput = torqueUnconstrained + jacobi.transpose() * (Eigen::MatrixXf::Identity(12,12) - proj2GraspNullspace) * forceConstrained;
        //        Eigen::VectorXf torqueInput = torqueUnconstrained + jacobi.transpose() * forceConstrained;
        Eigen::VectorXf torqueInput = jacobi.transpose() * (forceUnconstrained + forceConstrained);

        Eigen::VectorXf leftJointDesiredTorques = torqueInput.head(nl);
        Eigen::VectorXf rightJointDesiredTorques = torqueInput.block<8, 1>(8, 0);

        // torque limit
        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            float desiredTorque = leftJointDesiredTorques(i);

            if (isnan(desiredTorque))
            {
                desiredTorque = 0;
            }

            desiredTorque = (desiredTorque >  torqueLimit) ? torqueLimit : desiredTorque;
            desiredTorque = (desiredTorque < -torqueLimit) ? -torqueLimit : desiredTorque;

            debugDataInfo.getWriteBuffer().desired_torques[leftJointNames[i]] = leftJointDesiredTorques(i);

            leftTargets.at(i)->torque = desiredTorque;
        }



        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            float desiredTorque = rightJointDesiredTorques(i);

            if (isnan(desiredTorque))
            {
                desiredTorque = 0;
            }

            desiredTorque = (desiredTorque >  torqueLimit) ? torqueLimit : desiredTorque;
            desiredTorque = (desiredTorque < -torqueLimit) ? -torqueLimit : desiredTorque;

            debugDataInfo.getWriteBuffer().desired_torques[rightJointNames[i]] = rightJointDesiredTorques(i);

            rightTargets.at(i)->torque = desiredTorque;
        }

        //        debugDataInfo.getWriteBuffer().desiredForce_x = tcpDesiredForce(0);
        //        debugDataInfo.getWriteBuffer().desiredForce_y = tcpDesiredForce(1);
        //        debugDataInfo.getWriteBuffer().desiredForce_z = tcpDesiredForce(2);
        //        debugDataInfo.getWriteBuffer().leftTcpDesiredTorque_x = tcpDesiredTorque(0);
        //        debugDataInfo.getWriteBuffer().leftTcpDesiredTorque_y = tcpDesiredTorque(1);
        //        debugDataInfo.getWriteBuffer().tcpDesiredTorque_z = tcpDesiredTorque(2);
        //        debugDataInfo.getWriteBuffer().quatError = errorAngle;
        //        debugDataInfo.getWriteBuffer().posiError = posiError;
        debugDataInfo.getWriteBuffer().leftTargetPose_x = leftTargetPose(0, 3);
        debugDataInfo.getWriteBuffer().leftTargetPose_y = leftTargetPose(1, 3);
        debugDataInfo.getWriteBuffer().leftTargetPose_z = leftTargetPose(2, 3);
        debugDataInfo.getWriteBuffer().leftCurrentPose_x = leftCurrentPose(0, 3);
        debugDataInfo.getWriteBuffer().leftCurrentPose_y = leftCurrentPose(1, 3);
        debugDataInfo.getWriteBuffer().leftCurrentPose_z = leftCurrentPose(2, 3);


        debugDataInfo.getWriteBuffer().rightTargetPose_x = rightTargetPose(0, 3);
        debugDataInfo.getWriteBuffer().rightTargetPose_y = rightTargetPose(1, 3);
        debugDataInfo.getWriteBuffer().rightTargetPose_z = rightTargetPose(2, 3);

        debugDataInfo.getWriteBuffer().rightCurrentPose_x = rightCurrentPose(0, 3);
        debugDataInfo.getWriteBuffer().rightCurrentPose_y = rightCurrentPose(1, 3);
        debugDataInfo.getWriteBuffer().rightCurrentPose_z = rightCurrentPose(2, 3);


        for (size_t i = 0 ; i < 12; ++i)
        {
            std::stringstream ss;
            ss << i;
            std::string name = "forceConstrained_" + ss.str();
            debugDataInfo.getWriteBuffer().constrained_force[name] = filteredForce(i);
        }


        debugDataInfo.commitWrite();



        //        Eigen::VectorXf leftTargetVel = rtGetControlStruct().leftTargetVel;
        //        float normLinearVelocity = leftTargetVel.block<3, 1>(0, 0).norm();
        //        if (normLinearVelocity > cfg->maxLinearVel)
        //        {
        //            leftTargetVel.block<3, 1>(0, 0) = cfg->maxLinearVel * leftTargetVel.block<3, 1>(0, 0) / normLinearVelocity;
        //        }
        //        float normAngularVelocity = leftTargetVel.block<3, 1>(3, 0).norm();
        //        if (normAngularVelocity > cfg->maxAngularVel)
        //        {
        //            leftTargetVel.block<3, 1>(3, 0) = cfg->maxAngularVel * leftTargetVel.block<3, 1>(3, 0) / normAngularVelocity;
        //        }

        //        Eigen::VectorXf leftNullSpaceJointVelocity = cfg->knull * (leftDesiredJointValues - leftqpos);
        //        Eigen::MatrixXf jtpinvL = leftIK->computePseudoInverseJacobianMatrix(jacobiL, leftIK->getJacobiRegularization(VirtualRobot::IKSolver::CartesianSelection::All));
        //        Eigen::VectorXf jnvL = jtpinvL * leftTargetVel + (I - jtpinvL * jacobiL) * leftNullSpaceJointVelocity;

        //        for (size_t i = 0; i < leftTargets.size(); ++i)
        //        {
        //            leftTargets.at(i)->velocity = jnvL(i);
        //        }

        //        Eigen::VectorXf rightTargetVel = rtGetControlStruct().rightTargetVel;
        //        normLinearVelocity = rightTargetVel.block<3, 1>(0, 0).norm();
        //        if (normLinearVelocity > cfg->maxLinearVel)
        //        {
        //            rightTargetVel.block<3, 1>(0, 0) = cfg->maxLinearVel * rightTargetVel.block<3, 1>(0, 0) / normLinearVelocity;
        //        }
        //        normAngularVelocity = leftTargetVel.block<3, 1>(3, 0).norm();
        //        if (normAngularVelocity > cfg->maxAngularVel)
        //        {
        //            rightTargetVel.block<3, 1>(3, 0) = cfg->maxAngularVel * rightTargetVel.block<3, 1>(3, 0) / normAngularVelocity;
        //        }
        //        Eigen::VectorXf rightNullSpaceJointVelocity = cfg->knull * (rightDesiredJointValues - rightqpos);
        //        Eigen::MatrixXf jtpinvR = rightIK->computePseudoInverseJacobianMatrix(jacobiR, rightIK->getJacobiRegularization(VirtualRobot::IKSolver::CartesianSelection::All));
        //        Eigen::VectorXf jnvR = jtpinvR * rightTargetVel + (I - jtpinvR * jacobiR) * rightNullSpaceJointVelocity;

        //        for (size_t i = 0; i < rightTargets.size(); ++i)
        //        {
        //            rightTargets.at(i)->velocity = jnvR(i);
        //        }
    }

    void NJointBimanualCCDMPController::learnDMPFromFiles(const std::string& name, const Ice::StringSeq& fileNames, const Ice::Current&)
    {
        if (name == "LeftLeader")
        {
            leftGroup.at(0)->learnDMPFromFiles(fileNames);
        }
        else if (name == "LeftFollower")
        {
            rightGroup.at(1)->learnDMPFromFiles(fileNames);
        }
        else if (name == "RightLeader")
        {
            rightGroup.at(0)->learnDMPFromFiles(fileNames);
        }
        else
        {
            leftGroup.at(1)->learnDMPFromFiles(fileNames);
        }
    }


    void NJointBimanualCCDMPController::setViaPoints(Ice::Double u, const Ice::DoubleSeq& viapoint, const Ice::Current&)
    {
        LockGuardType guard(controllerMutex);
        if (leaderName == "Left")
        {
            leftGroup.at(0)->setViaPose(u, viapoint);
        }
        else
        {
            rightGroup.at(0)->setViaPose(u, viapoint);
        }
    }

    void NJointBimanualCCDMPController::setGoals(const Ice::DoubleSeq& goals, const Ice::Current& ice)
    {
        LockGuardType guard(controllerMutex);
        if (leaderName == "Left")
        {
            leftGroup.at(0)->setGoalPoseVec(goals);
        }
        else
        {
            rightGroup.at(0)->setGoalPoseVec(goals);
        }

    }

    void NJointBimanualCCDMPController::changeLeader(const Ice::Current&)
    {
        LockGuardType guard(controllerMutex);

        if (leaderName == "Left")
        {
            leaderName = "Right";
            rightGroup.at(0)->canVal = virtualtimer;
            rightGroup.at(1)->canVal = virtualtimer;
        }
        else
        {
            leaderName = "Left";
            leftGroup.at(0)->canVal = virtualtimer;
            leftGroup.at(1)->canVal = virtualtimer;
        }

    }





    void NJointBimanualCCDMPController::runDMP(const Ice::DoubleSeq& leftGoals, const Ice::DoubleSeq& rightGoals, const Ice::Current&)
    {
        virtualtimer = cfg->timeDuration;

        Eigen::Matrix4f leftPose = tcpLeft->getPoseInRootFrame();
        Eigen::Matrix4f rightPose = tcpRight->getPoseInRootFrame();

        leftGroup.at(0)->prepareExecution(leftGroup.at(0)->eigen4f2vec(leftPose), leftGoals);
        rightGroup.at(0)->prepareExecution(rightGroup.at(0)->eigen4f2vec(rightPose), rightGoals);


        ARMARX_INFO << "leftgroup goal local pose: " << getLocalPose(leftGoals, rightGoals);

        leftGroup.at(1)->prepareExecution(getLocalPose(leftPose, rightPose), getLocalPose(leftGoals, rightGoals));
        rightGroup.at(1)->prepareExecution(getLocalPose(rightPose, leftPose), getLocalPose(rightGoals, leftGoals));

        controllerTask->start();
    }

    Eigen::Matrix4f NJointBimanualCCDMPController::getLocalPose(const Eigen::Matrix4f& newCoordinate, const Eigen::Matrix4f& globalTargetPose)
    {
        Eigen::Matrix4f localPose;
        localPose.block<3, 3>(0, 0) = globalTargetPose.block<3, 3>(0, 0) * newCoordinate.block<3, 3>(0, 0).inverse();
        localPose.block<3, 1>(0, 3) = localPose.block<3, 3>(0, 0) * (globalTargetPose.block<3, 1>(0, 3) - newCoordinate.block<3, 1>(0, 3));

        return localPose;
    }


    void NJointBimanualCCDMPController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& debugObs)
    {

        StringVariantBaseMap datafields;
        auto values = debugDataInfo.getUpToDateReadBuffer().desired_torques;
        for (auto& pair : values)
        {
            datafields[pair.first] = new Variant(pair.second);
        }

        auto constrained_force = debugDataInfo.getUpToDateReadBuffer().constrained_force;
        for (auto& pair : constrained_force)
        {
            datafields[pair.first] = new Variant(pair.second);
        }


        datafields["leftTargetPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftTargetPose_x);
        datafields["leftTargetPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftTargetPose_y);
        datafields["leftTargetPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftTargetPose_z);
        datafields["rightTargetPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightTargetPose_x);
        datafields["rightTargetPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightTargetPose_y);
        datafields["rightTargetPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightTargetPose_z);


        datafields["leftCurrentPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftCurrentPose_x);
        datafields["leftCurrentPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftCurrentPose_y);
        datafields["leftCurrentPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftCurrentPose_z);
        datafields["rightCurrentPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightCurrentPose_x);
        datafields["rightCurrentPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightCurrentPose_y);
        datafields["rightCurrentPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightCurrentPose_z);

        //        StringVariantBaseMap datafields;
        //        auto values = debugOutputData.getUpToDateReadBuffer().latestTargetVelocities;
        //        for (auto& pair : values)
        //        {
        //            datafields[pair.first] = new Variant(pair.second);
        //        }

        //        auto dmpTargets = debugOutputData.getUpToDateReadBuffer().dmpTargets;
        //        for (auto& pair : dmpTargets)
        //        {
        //            datafields[pair.first] = new Variant(pair.second);
        //        }

        //        auto currentPose = debugOutputData.getUpToDateReadBuffer().currentPose;
        //        for (auto& pair : currentPose)
        //        {
        //            datafields[pair.first] = new Variant(pair.second);
        //        }

        //        datafields["leaderCanVal"] = new Variant(debugOutputData.getUpToDateReadBuffer().leaderCanVal);
        //        datafields["leadermpcFactor"] = new Variant(debugOutputData.getUpToDateReadBuffer().leadermpcFactor);
        //        datafields["leaderError"] = new Variant(debugOutputData.getUpToDateReadBuffer().leadererror);
        //        datafields["leaderposError"] = new Variant(debugOutputData.getUpToDateReadBuffer().leaderposError);
        //        datafields["leaderoriError"] = new Variant(debugOutputData.getUpToDateReadBuffer().leaderoriError);

        //        datafields["followerCanVal"] = new Variant(debugOutputData.getUpToDateReadBuffer().followerCanVal);
        //        datafields["followermpcFactor"] = new Variant(debugOutputData.getUpToDateReadBuffer().followermpcFactor);
        //        datafields["followerError"] = new Variant(debugOutputData.getUpToDateReadBuffer().followererror);
        //        datafields["followerposError"] = new Variant(debugOutputData.getUpToDateReadBuffer().followerposError);
        //        datafields["followeroriError"] = new Variant(debugOutputData.getUpToDateReadBuffer().followeroriError);

        debugObs->setDebugChannel("DMPController", datafields);
    }

    void NJointBimanualCCDMPController::onInitComponent()
    {
        ARMARX_INFO << "init ...";
        controllerTask = new PeriodicTask<NJointBimanualCCDMPController>(this, &NJointBimanualCCDMPController::controllerRun, 0.3);
    }

    void NJointBimanualCCDMPController::onDisconnectComponent()
    {
        controllerTask->stop();
        ARMARX_INFO << "stopped ...";
    }



}
