#include "NJointTaskSpaceAdaptiveDMPController.h"

#include <ArmarXCore/core/ArmarXObjectScheduler.h>

namespace armarx
{
    NJointControllerRegistration<NJointTaskSpaceAdaptiveDMPController> registrationControllerNJointTaskSpaceAdaptiveDMPController("NJointTaskSpaceAdaptiveDMPController");

    NJointTaskSpaceAdaptiveDMPController::NJointTaskSpaceAdaptiveDMPController(const RobotUnitPtr& robotUnit, const armarx::NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&)
    {
        ARMARX_INFO << "creating impedance dmp controller";
        cfg = NJointTaskSpaceAdaptiveDMPControllerConfigPtr::dynamicCast(config);
        useSynchronizedRtRobot();
        rns = rtGetRobot()->getRobotNodeSet(cfg->nodeSetName);
        ARMARX_CHECK_EXPRESSION(rns) << cfg->nodeSetName;

        for (size_t i = 0; i < rns->getSize(); ++i)
        {
            std::string jointName = rns->getNode(i)->getName();
            jointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            targets.push_back(ct->asA<ControlTarget1DoFActuatorTorque>());
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();

            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }

            velocitySensors.push_back(velocitySensor);
            positionSensors.push_back(positionSensor);
        };


        const SensorValueBase* svlf = robotUnit->getSensorDevice(cfg->forceSensorName)->getSensorValue();
        forceSensor = svlf->asA<SensorValueForceTorque>();

        tcp =  rns->getTCP();
        ik.reset(new VirtualRobot::DifferentialIK(rns, rtGetRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));
        numOfJoints = targets.size();
        // set DMP
        double phaseL = 1000;
        double phaseK = 1000;
        double phaseDist0 = 10000;
        double phaseDist1 = 10000;
        double posToOriRatio = 10;

        TaskSpaceDMPControllerConfig taskSpaceDMPConfig;
        taskSpaceDMPConfig.motionTimeDuration = cfg->timeDuration;
        taskSpaceDMPConfig.DMPKernelSize = cfg->kernelSize;
        taskSpaceDMPConfig.DMPMode = cfg->dmpMode;
        taskSpaceDMPConfig.DMPStyle = cfg->dmpType;
        taskSpaceDMPConfig.DMPAmplitude = 1.0;
        taskSpaceDMPConfig.phaseStopParas.goDist = phaseDist0;
        taskSpaceDMPConfig.phaseStopParas.backDist = phaseDist1;
        taskSpaceDMPConfig.phaseStopParas.Kpos = 0;
        taskSpaceDMPConfig.phaseStopParas.Dpos = 0;
        taskSpaceDMPConfig.phaseStopParas.Kori = 0;
        taskSpaceDMPConfig.phaseStopParas.Dori = 0;
        taskSpaceDMPConfig.phaseStopParas.mm2radi = posToOriRatio;
        taskSpaceDMPConfig.phaseStopParas.maxValue = phaseL;
        taskSpaceDMPConfig.phaseStopParas.slop = phaseK;

        dmpCtrl.reset(new TaskSpaceDMPController("DMPController", taskSpaceDMPConfig, false));
        finished = false;

        useNullSpaceJointDMP = cfg->useNullSpaceJointDMP;
        nullSpaceJointDMPPtr.reset(new DMP::UMIDMP(100));

        isNullSpaceJointDMPLearned = false;

        defaultNullSpaceJointValues.resize(targets.size());
        ARMARX_CHECK_EQUAL(cfg->defaultNullSpaceJointValues.size(), targets.size());

        for (size_t i = 0; i < targets.size(); ++i)
        {
            defaultNullSpaceJointValues(i) = cfg->defaultNullSpaceJointValues.at(i);
        }


        kpos << cfg->Kpos[0], cfg->Kpos[1], cfg->Kpos[2];
        dpos << cfg->Dpos[0], cfg->Dpos[1], cfg->Dpos[2];
        kori << cfg->Kori[0], cfg->Kori[1], cfg->Kori[2];
        dori << cfg->Dori[0], cfg->Dori[1], cfg->Dori[2];


        ARMARX_CHECK_EQUAL(cfg->Knull.size(), targets.size());
        ARMARX_CHECK_EQUAL(cfg->Dnull.size(), targets.size());

        knull.setZero(targets.size());
        dnull.setZero(targets.size());

        for (size_t i = 0; i < targets.size(); ++i)
        {
            knull(i) = cfg->Knull.at(i);
            dnull(i) = cfg->Dnull.at(i);
        }

        torqueLimit = cfg->torqueLimit;
        timeDuration = cfg->timeDuration;

        NJointTaskSpaceAdaptiveDMPControllerInterfaceData initInterfaceData;
        initInterfaceData.currentTcpPose = Eigen::Matrix4f::Identity();
        initInterfaceData.currentForce = Eigen::Vector3f::Zero();
        initInterfaceData.currentVel.setZero(6);
        interfaceData.reinitAllBuffers(initInterfaceData);

        NJointTaskSpaceAdaptiveDMPControllerSensorData initControllerSensorData;
        initControllerSensorData.currentPose = Eigen::Matrix4f::Identity();
        initControllerSensorData.currentTime = 0;
        initControllerSensorData.deltaT = 0;
        initControllerSensorData.currentTwist.setZero();
        controllerSensorData.reinitAllBuffers(initControllerSensorData);

        //initialize control parameters
        Eigen::VectorXf KpImpedance;
        KpImpedance.setZero(6);

        for (int i = 0; i < 3; ++i)
        {
            KpImpedance(i) = cfg->Kpos.at(i);
            KpImpedance(i + 3) = cfg->Kori.at(i);
        }

        Eigen::VectorXf KdImpedance;
        KdImpedance.setZero(6);

        for (int i = 0; i < 3; ++i)
        {
            KdImpedance(i) = cfg->Dpos.at(i);
            KdImpedance(i + 3) = cfg->Dori.at(i);

        }

        Inferface2rtData initInt2rtData;
        initInt2rtData.KpImpedance = KpImpedance;
        initInt2rtData.KdImpedance = KdImpedance;
        initInt2rtData.Knull = knull;
        initInt2rtData.Dnull = dnull;
        interface2rtBuffer.reinitAllBuffers(initInt2rtData);


        Interface2CtrlData initInt2CtrlData;
        initInt2CtrlData.canVal = 1.0;
        interface2CtrlBuffer.reinitAllBuffers(initInt2CtrlData);

        firstRun = true;
        forceOffset.setZero(3);
        filteredForce.setZero(3);

        ARMARX_INFO << "Finished controller constructor ";
    }

    std::string NJointTaskSpaceAdaptiveDMPController::getClassName(const Ice::Current&) const
    {
        return "NJointTaskSpaceAdaptiveDMPController";

    }


    void NJointTaskSpaceAdaptiveDMPController::rtPreActivateController()
    {
        NJointTaskSpaceAdaptiveDMPControllerControlData initData;
        initData.targetPose = tcp->getPoseInRootFrame();
        initData.targetVel.resize(6);
        initData.targetVel.setZero();
        initData.desiredNullSpaceJointValues = defaultNullSpaceJointValues;
        reinitTripleBuffer(initData);


    }


    void NJointTaskSpaceAdaptiveDMPController::controllerRun()
    {


        if (!dmpCtrl)
        {
            return;
        }

        if (!controllerSensorData.updateReadBuffer())
        {
            return;
        }


        double deltaT = 0.001; //controllerSensorData.getReadBuffer().deltaT;
        Eigen::Matrix4f currentPose = controllerSensorData.getReadBuffer().currentPose;
        Eigen::VectorXf currentTwist = controllerSensorData.getReadBuffer().currentTwist;

        if (interface2CtrlBuffer.updateReadBuffer())
        {
            dmpCtrl->canVal = interface2CtrlBuffer.getUpToDateReadBuffer().canVal;
        }

        if (!started)
        {
            LockGuardType guard {controlDataMutex};
            getWriterControlStruct().desiredNullSpaceJointValues = defaultNullSpaceJointValues;
            getWriterControlStruct().targetVel.setZero(6);
            getWriterControlStruct().targetPose = currentPose;
            getWriterControlStruct().canVal = 1.0;
            getWriterControlStruct().mpcFactor = 0.0;
            writeControlStruct();
        }
        else
        {
            if (stopped)
            {

                LockGuardType guard {controlDataMutex};
                getWriterControlStruct().desiredNullSpaceJointValues = defaultNullSpaceJointValues;
                getWriterControlStruct().targetVel.setZero(6);
                getWriterControlStruct().targetPose = oldPose;
                getWriterControlStruct().canVal = dmpCtrl->canVal;
                getWriterControlStruct().mpcFactor = dmpCtrl->debugData.mpcFactor;
                writeControlStruct();
            }
            else
            {
                if (dmpCtrl->canVal < 1e-8)
                {
                    finished = true;
                    LockGuardType guard {controlDataMutex};
                    getWriterControlStruct().targetVel.setZero();
                    writeControlStruct();
                    return;
                }

                dmpCtrl->flow(deltaT, currentPose, currentTwist);

                Eigen::VectorXf desiredNullSpaceJointValues(jointNames.size());
                if (useNullSpaceJointDMP && isNullSpaceJointDMPLearned)
                {
                    DMP::DVec targetJointState;
                    currentJointState = nullSpaceJointDMPPtr->calculateDirectlyVelocity(currentJointState, dmpCtrl->canVal / timeDuration, deltaT / timeDuration, targetJointState);

                    if (targetJointState.size() == jointNames.size())
                    {
                        for (size_t i = 0; i < targetJointState.size(); ++i)
                        {
                            desiredNullSpaceJointValues(i) = targetJointState[i];
                        }
                    }
                    else
                    {
                        desiredNullSpaceJointValues = defaultNullSpaceJointValues;
                    }
                }
                else
                {
                    desiredNullSpaceJointValues = defaultNullSpaceJointValues;
                }

                LockGuardType guard {controlDataMutex};
                getWriterControlStruct().desiredNullSpaceJointValues = desiredNullSpaceJointValues;
                getWriterControlStruct().targetVel = dmpCtrl->getTargetVelocity();
                getWriterControlStruct().targetPose = dmpCtrl->getTargetPoseMat();
                getWriterControlStruct().canVal = dmpCtrl->canVal;
                getWriterControlStruct().mpcFactor = dmpCtrl->debugData.mpcFactor;

                writeControlStruct();
            }
        }
    }

    void NJointTaskSpaceAdaptiveDMPController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        double deltaT = timeSinceLastIteration.toSecondsDouble();
        Eigen::Matrix4f currentPose = tcp->getPoseInRootFrame();

        Eigen::MatrixXf jacobi = ik->getJacobianMatrix(tcp, VirtualRobot::IKSolver::CartesianSelection::All);

        Eigen::VectorXf qpos(positionSensors.size());
        Eigen::VectorXf qvel(velocitySensors.size());
        for (size_t i = 0; i < positionSensors.size(); ++i)
        {
            qpos(i) = positionSensors[i]->position;
            qvel(i) = velocitySensors[i]->velocity;
        }

        Eigen::VectorXf currentTwist = jacobi * qvel;

        controllerSensorData.getWriteBuffer().currentPose = currentPose;
        controllerSensorData.getWriteBuffer().currentTwist = currentTwist;
        controllerSensorData.getWriteBuffer().deltaT = deltaT;
        controllerSensorData.getWriteBuffer().currentTime += deltaT;
        controllerSensorData.commitWrite();


        Eigen::Matrix4f targetPose;
        Eigen::VectorXf targetVel;
        Eigen::VectorXf desiredNullSpaceJointValues;
        if (firstRun || !started)
        {
            firstRun = false;
            targetPose = currentPose;
            targetVel.setZero(6);
            desiredNullSpaceJointValues = defaultNullSpaceJointValues;
            forceOffset = 0.1 * forceOffset + 0.9 * forceSensor->force;
        }
        else
        {
            filteredForce = (1 - cfg->filterCoeff) * filteredForce + cfg->filterCoeff * (forceSensor->force - forceOffset);
            targetPose = rtGetControlStruct().targetPose;
            targetVel = rtGetControlStruct().targetVel;
            desiredNullSpaceJointValues = rtGetControlStruct().desiredNullSpaceJointValues;
        }

        interfaceData.getWriteBuffer().currentTcpPose = currentPose;
        interfaceData.getWriteBuffer().currentForce = filteredForce;
        interfaceData.getWriteBuffer().currentVel = currentTwist;
        interfaceData.commitWrite();


        kpos = interface2rtBuffer.getUpToDateReadBuffer().KpImpedance.head(3);
        kori = interface2rtBuffer.getUpToDateReadBuffer().KpImpedance.tail(3);
        dpos = interface2rtBuffer.getUpToDateReadBuffer().KdImpedance.head(3);
        dori = interface2rtBuffer.getUpToDateReadBuffer().KdImpedance.tail(3);
        knull = interface2rtBuffer.getUpToDateReadBuffer().Knull;
        dnull = interface2rtBuffer.getUpToDateReadBuffer().Dnull;

        /* calculate torques in meter */
        jacobi.block(0, 0, 3, numOfJoints) = 0.001 * jacobi.block(0, 0, 3, numOfJoints); // convert mm to m
        Eigen::Vector6f jointControlWrench;
        {
            Eigen::Vector3f targetTCPLinearVelocity;
            targetTCPLinearVelocity << 0.001 * targetVel(0), 0.001 * targetVel(1), 0.001 * targetVel(2);
            Eigen::Vector3f currentTCPLinearVelocity;
            currentTCPLinearVelocity << 0.001 * currentTwist(0),  0.001 * currentTwist(1),  0.001 * currentTwist(2);
            Eigen::Vector3f currentTCPPosition = currentPose.block<3, 1>(0, 3);
            Eigen::Vector3f desiredPosition = targetPose.block<3, 1>(0, 3);
            Eigen::Vector3f tcpDesiredForce = 0.001 * kpos.cwiseProduct(desiredPosition - currentTCPPosition) + dpos.cwiseProduct(targetTCPLinearVelocity - currentTCPLinearVelocity);

            Eigen::Vector3f currentTCPAngularVelocity;
            currentTCPAngularVelocity << currentTwist(3),   currentTwist(4),  currentTwist(5);
            Eigen::Matrix3f currentRotMat = currentPose.block<3, 3>(0, 0);
            Eigen::Matrix3f diffMat = targetPose.block<3, 3>(0, 0) * currentRotMat.inverse();
            Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
            Eigen::Vector3f tcpDesiredTorque = kori.cwiseProduct(rpy) - dori.cwiseProduct(currentTCPAngularVelocity);
            jointControlWrench <<  tcpDesiredForce, tcpDesiredTorque;
        }

        Eigen::MatrixXf I = Eigen::MatrixXf::Identity(targets.size(), targets.size());

        Eigen::VectorXf nullspaceTorque = knull.cwiseProduct(desiredNullSpaceJointValues - qpos) - dnull.cwiseProduct(qvel);
        Eigen::MatrixXf jtpinv = ik->computePseudoInverseJacobianMatrix(jacobi.transpose(), 2.0);
        Eigen::VectorXf jointDesiredTorques = jacobi.transpose() * jointControlWrench + (I - jacobi.transpose() * jtpinv) * nullspaceTorque;

        // torque limit
        ARMARX_CHECK_EXPRESSION(!targets.empty());
        ARMARX_CHECK_LESS(targets.size(), 1000);
        for (size_t i = 0; i < targets.size(); ++i)
        {
            float desiredTorque = jointDesiredTorques(i);

            if (isnan(desiredTorque))
            {
                desiredTorque = 0;
            }

            desiredTorque = (desiredTorque >  torqueLimit) ? torqueLimit : desiredTorque;
            desiredTorque = (desiredTorque < -torqueLimit) ? -torqueLimit : desiredTorque;

            debugOutputData.getWriteBuffer().desired_torques[jointNames[i]] = jointDesiredTorques(i);
            debugOutputData.getWriteBuffer().desired_nullspaceJoint[jointNames[i]] = desiredNullSpaceJointValues(i);

            targets.at(i)->torque = desiredTorque;
            if (!targets.at(i)->isValid())
            {
                ARMARX_INFO << deactivateSpam(1) << "Torque controller target is invalid - setting to zero! set value: " << targets.at(i)->torque;
                targets.at(i)->torque = 0.0f;
            }
        }


        debugOutputData.getWriteBuffer().forceDesired_x = jointControlWrench(0);
        debugOutputData.getWriteBuffer().forceDesired_y = jointControlWrench(1);
        debugOutputData.getWriteBuffer().forceDesired_z = jointControlWrench(2);
        debugOutputData.getWriteBuffer().forceDesired_rx = jointControlWrench(3);
        debugOutputData.getWriteBuffer().forceDesired_ry = jointControlWrench(4);
        debugOutputData.getWriteBuffer().forceDesired_rz = jointControlWrench(5);

        debugOutputData.getWriteBuffer().impedanceKp_x = kpos(0);
        debugOutputData.getWriteBuffer().impedanceKp_y = kpos(1);
        debugOutputData.getWriteBuffer().impedanceKp_z = kpos(2);
        debugOutputData.getWriteBuffer().impedanceKp_rx = kori(0);
        debugOutputData.getWriteBuffer().impedanceKp_ry = kori(1);
        debugOutputData.getWriteBuffer().impedanceKp_rz = kori(2);

        debugOutputData.getWriteBuffer().forceInRoot_x = filteredForce(0);
        debugOutputData.getWriteBuffer().forceInRoot_y = filteredForce(1);
        debugOutputData.getWriteBuffer().forceInRoot_z = filteredForce(2);
        //        debugOutputData.getWriteBuffer().torqueInRoot_x = filteredForce(3);
        //        debugOutputData.getWriteBuffer().torqueInRoot_y = filteredForce(4);
        //        debugOutputData.getWriteBuffer().torqueInRoot_z = filteredForce(5);

        debugOutputData.getWriteBuffer().vel_x = currentTwist(0);
        debugOutputData.getWriteBuffer().vel_y = currentTwist(1);
        debugOutputData.getWriteBuffer().vel_z = currentTwist(2);

        //        debugOutputData.getWriteBuffer().currentCanVal = rtGetControlStruct().canVal;
        //        debugOutputData.getWriteBuffer().mpcfactor = rtGetControlStruct().mpcFactor;

        debugOutputData.getWriteBuffer().targetPose_x = targetPose(0, 3);
        debugOutputData.getWriteBuffer().targetPose_y = targetPose(1, 3);
        debugOutputData.getWriteBuffer().targetPose_z = targetPose(2, 3);
        VirtualRobot::MathTools::Quaternion targetQuat = VirtualRobot::MathTools::eigen4f2quat(targetPose);
        debugOutputData.getWriteBuffer().targetPose_qw = targetQuat.w;
        debugOutputData.getWriteBuffer().targetPose_qx = targetQuat.x;
        debugOutputData.getWriteBuffer().targetPose_qy = targetQuat.y;
        debugOutputData.getWriteBuffer().targetPose_qz = targetQuat.z;

        debugOutputData.getWriteBuffer().currentPose_x = currentPose(0, 3);
        debugOutputData.getWriteBuffer().currentPose_y = currentPose(1, 3);
        debugOutputData.getWriteBuffer().currentPose_z = currentPose(2, 3);
        VirtualRobot::MathTools::Quaternion currentQuat = VirtualRobot::MathTools::eigen4f2quat(currentPose);
        debugOutputData.getWriteBuffer().currentPose_qw = currentQuat.w;
        debugOutputData.getWriteBuffer().currentPose_qx = currentQuat.x;
        debugOutputData.getWriteBuffer().currentPose_qy = currentQuat.y;
        debugOutputData.getWriteBuffer().currentPose_qz = currentQuat.z;
        debugOutputData.getWriteBuffer().deltaT = deltaT;

        debugOutputData.commitWrite();

    }


    void NJointTaskSpaceAdaptiveDMPController::learnDMPFromFiles(const Ice::StringSeq& fileNames, const Ice::Current&)
    {
        dmpCtrl->learnDMPFromFiles(fileNames);
        ARMARX_INFO << "Learned DMP ... ";
    }

    void NJointTaskSpaceAdaptiveDMPController::setViaPoints(Ice::Double u, const Ice::DoubleSeq& viapoint, const Ice::Current&)
    {
        LockGuardType guard(controllerMutex);
        ARMARX_INFO << "setting via points ";
        dmpCtrl->setViaPose(u, viapoint);

    }

    void NJointTaskSpaceAdaptiveDMPController::setGoals(const Ice::DoubleSeq& goals, const Ice::Current& ice)
    {
        dmpCtrl->setGoalPoseVec(goals);

    }

    void NJointTaskSpaceAdaptiveDMPController::setCanVal(double canVal, const Ice::Current&)
    {
        LockGuardType guard(int2ctrlMutex);
        interface2CtrlBuffer.getWriteBuffer().canVal = canVal;
        interface2CtrlBuffer.commitWrite();
    }

    void NJointTaskSpaceAdaptiveDMPController::setUseNullSpaceJointDMP(bool useJointDMP, const Ice::Current&)
    {
        useNullSpaceJointDMP = useJointDMP;
    }


    void NJointTaskSpaceAdaptiveDMPController::setDefaultJointValues(const Ice::FloatSeq& desiredJointVals, const Ice::Current&)
    {
        ARMARX_CHECK_EQUAL(cfg->defaultNullSpaceJointValues.size(), targets.size());

        for (size_t i = 0; i < targets.size(); ++i)
        {
            defaultNullSpaceJointValues(i) = desiredJointVals.at(i);
        }
    }

    void NJointTaskSpaceAdaptiveDMPController::learnJointDMPFromFiles(const std::string& fileName, const Ice::FloatSeq& currentJVS, const Ice::Current&)
    {
        DMP::Vec<DMP::SampledTrajectoryV2 > trajs;
        DMP::DVec ratios;
        DMP::SampledTrajectoryV2 traj;
        traj.readFromCSVFile(fileName);
        traj = DMP::SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
        if (traj.dim() != jointNames.size())
        {
            isNullSpaceJointDMPLearned = false;
            return;
        }

        DMP::DVec goal;
        goal.resize(traj.dim());
        currentJointState.resize(traj.dim());

        for (size_t i = 0; i < goal.size(); ++i)
        {
            goal.at(i) = traj.rbegin()->getPosition(i);
            currentJointState.at(i).pos = currentJVS.at(i);
            currentJointState.at(i).vel = 0;
        }

        trajs.push_back(traj);
        nullSpaceJointDMPPtr->learnFromTrajectories(trajs);

        // prepare exeuction of joint dmp
        nullSpaceJointDMPPtr->prepareExecution(goal, currentJointState, 1.0, 1.0);
        ARMARX_INFO << "prepared nullspace joint dmp";
        isNullSpaceJointDMPLearned = true;
    }


    void NJointTaskSpaceAdaptiveDMPController::resetDMP(const Ice::Current&)
    {
        if (started)
        {
            ARMARX_INFO << "Cannot reset running DMP";
        }
        firstRun = true;
    }

    void NJointTaskSpaceAdaptiveDMPController::setKdImpedance(const Ice::FloatSeq& value, const Ice::Current&)
    {
        Eigen::VectorXf setpoint;
        setpoint.setZero(value.size());
        ARMARX_CHECK_EQUAL(value.size(), 6);

        for (size_t i = 0; i < value.size(); ++i)
        {
            setpoint(i) = value.at(i);
        }

        LockGuardType guard {interfaceDataMutex};
        interface2rtBuffer.getWriteBuffer().KdImpedance = setpoint;
        interface2rtBuffer.commitWrite();
    }

    void NJointTaskSpaceAdaptiveDMPController::setKpImpedance(const Ice::FloatSeq& value, const Ice::Current&)
    {
        Eigen::VectorXf setpoint;
        setpoint.setZero(value.size());
        ARMARX_CHECK_EQUAL(value.size(), 6);

        for (size_t i = 0; i < value.size(); ++i)
        {
            setpoint(i) = value.at(i);
        }

        LockGuardType guard {interfaceDataMutex};
        interface2rtBuffer.getWriteBuffer().KpImpedance = setpoint;
        interface2rtBuffer.commitWrite();

    }

    void NJointTaskSpaceAdaptiveDMPController::setKpNull(const Ice::FloatSeq& value, const Ice::Current&)
    {
        Eigen::VectorXf setpoint;
        setpoint.setZero(value.size());
        ARMARX_CHECK_EQUAL(value.size(), rns->getSize());

        for (size_t i = 0; i < value.size(); ++i)
        {
            setpoint(i) = value.at(i);
        }

        LockGuardType guard {interfaceDataMutex};
        interface2rtBuffer.getWriteBuffer().Knull = setpoint;
        interface2rtBuffer.commitWrite();

    }

    void NJointTaskSpaceAdaptiveDMPController::setKdNull(const Ice::FloatSeq& value, const Ice::Current&)
    {
        Eigen::VectorXf setpoint;
        setpoint.setZero(value.size());
        ARMARX_CHECK_EQUAL(value.size(), rns->getSize());

        for (size_t i = 0; i < value.size(); ++i)
        {
            setpoint(i) = value.at(i);
        }

        LockGuardType guard {interfaceDataMutex};
        interface2rtBuffer.getWriteBuffer().Dnull = setpoint;
        interface2rtBuffer.commitWrite();

    }

    Ice::FloatSeq NJointTaskSpaceAdaptiveDMPController::getForce(const Ice::Current&)
    {
        Eigen::Vector3f force = interfaceData.getUpToDateReadBuffer().currentForce;
        std::vector<float> forceVec = {force(0), force(1), force(2)};
        return forceVec;
    }

    Ice::FloatSeq NJointTaskSpaceAdaptiveDMPController::getVelocityInMM(const Ice::Current&)
    {
        Eigen::VectorXf currentVel = interfaceData.getUpToDateReadBuffer().currentVel;
        std::vector<float> tvelvec = {currentVel(0), currentVel(1), currentVel(2), currentVel(3), currentVel(4), currentVel(5)};
        return tvelvec;
    }

    void NJointTaskSpaceAdaptiveDMPController::stopDMP(const Ice::Current&)
    {
        oldPose = interfaceData.getUpToDateReadBuffer().currentTcpPose;
        stopped = true;
    }

    void NJointTaskSpaceAdaptiveDMPController::resumeDMP(const Ice::Current&)
    {
        stopped = false;
    }


    void NJointTaskSpaceAdaptiveDMPController::removeAllViaPoints(const Ice::Current&)
    {
        LockGuardType guard {controllerMutex};
        dmpCtrl->removeAllViaPoints();
    }


    void NJointTaskSpaceAdaptiveDMPController::runDMPWithTime(const Ice::DoubleSeq& goals, Ice::Double timeDuration, const Ice::Current&)
    {
        firstRun = true;
        while (firstRun)
        {
            usleep(100);
        }

        Eigen::Matrix4f pose = interfaceData.getUpToDateReadBuffer().currentTcpPose;
        dmpCtrl->prepareExecution(dmpCtrl->eigen4f2vec(pose), goals);

        dmpCtrl->canVal = timeDuration;
        dmpCtrl->config.motionTimeDuration = timeDuration;

        finished = false;

        if (isNullSpaceJointDMPLearned && useNullSpaceJointDMP)
        {
            ARMARX_INFO << "Using Null Space Joint DMP";
        }

        started = true;
        stopped = false;
        //        controllerTask->start();
    }

    void NJointTaskSpaceAdaptiveDMPController::runDMP(const Ice::DoubleSeq& goals, const Ice::Current&)
    {
        firstRun = true;
        while (firstRun)
        {
            usleep(100);
        }

        Eigen::Matrix4f pose = interfaceData.getUpToDateReadBuffer().currentTcpPose;
        dmpCtrl->prepareExecution(dmpCtrl->eigen4f2vec(pose), goals);

        finished = false;

        if (isNullSpaceJointDMPLearned && useNullSpaceJointDMP)
        {
            ARMARX_INFO << "Using Null Space Joint DMP";
        }

        started = true;
        stopped = false;
        //        controllerTask->start();
    }



    void NJointTaskSpaceAdaptiveDMPController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& debugObs)
    {
        StringVariantBaseMap datafields;
        auto values = debugOutputData.getUpToDateReadBuffer().desired_torques;
        for (auto& pair : values)
        {
            datafields["torqueDesired_" + pair.first] = new Variant(pair.second);
        }

        auto values_null = debugOutputData.getUpToDateReadBuffer().desired_nullspaceJoint;
        for (auto& pair : values_null)
        {
            datafields["nullspaceDesired_" + pair.first] = new Variant(pair.second);
        }

        datafields["canVal"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentCanVal);
        datafields["mpcfactor"] = new Variant(debugOutputData.getUpToDateReadBuffer().mpcfactor);
        datafields["targetPose_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().targetPose_x);
        datafields["targetPose_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().targetPose_y);
        datafields["targetPose_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().targetPose_z);
        datafields["targetPose_qw"] = new Variant(debugOutputData.getUpToDateReadBuffer().targetPose_qw);
        datafields["targetPose_qx"] = new Variant(debugOutputData.getUpToDateReadBuffer().targetPose_qx);
        datafields["targetPose_qy"] = new Variant(debugOutputData.getUpToDateReadBuffer().targetPose_qy);
        datafields["targetPose_qz"] = new Variant(debugOutputData.getUpToDateReadBuffer().targetPose_qz);

        datafields["impedanceKp_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().impedanceKp_x);
        datafields["impedanceKp_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().impedanceKp_y);
        datafields["impedanceKp_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().impedanceKp_z);
        datafields["impedanceKp_rx"] = new Variant(debugOutputData.getUpToDateReadBuffer().impedanceKp_rx);
        datafields["impedanceKp_ry"] = new Variant(debugOutputData.getUpToDateReadBuffer().impedanceKp_ry);
        datafields["impedanceKp_rz"] = new Variant(debugOutputData.getUpToDateReadBuffer().impedanceKp_rz);

        datafields["currentPose_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPose_x);
        datafields["currentPose_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPose_y);
        datafields["currentPose_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPose_z);
        datafields["currentPose_qw"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPose_qw);
        datafields["currentPose_qx"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPose_qx);
        datafields["currentPose_qy"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPose_qy);
        datafields["currentPose_qz"] = new Variant(debugOutputData.getUpToDateReadBuffer().currentPose_qz);

        datafields["forceDesired_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().forceDesired_x);
        datafields["forceDesired_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().forceDesired_y);
        datafields["forceDesired_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().forceDesired_z);
        datafields["forceDesired_rx"] = new Variant(debugOutputData.getUpToDateReadBuffer().forceDesired_rx);
        datafields["forceDesired_ry"] = new Variant(debugOutputData.getUpToDateReadBuffer().forceDesired_ry);
        datafields["forceDesired_rz"] = new Variant(debugOutputData.getUpToDateReadBuffer().forceDesired_rz);

        datafields["forceInRoot_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().forceInRoot_x);
        datafields["forceInRoot_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().forceInRoot_y);
        datafields["forceInRoot_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().forceInRoot_z);

        datafields["vel_x"] = new Variant(debugOutputData.getUpToDateReadBuffer().vel_x);
        datafields["vel_y"] = new Variant(debugOutputData.getUpToDateReadBuffer().vel_y);
        datafields["vel_z"] = new Variant(debugOutputData.getUpToDateReadBuffer().vel_z);

        datafields["deltaT"] = new Variant(debugOutputData.getUpToDateReadBuffer().deltaT);

        std::string channelName = cfg->nodeSetName + "_TaskSpaceImpedanceControl";
        debugObs->setDebugChannel(channelName, datafields);
    }

    void NJointTaskSpaceAdaptiveDMPController::onInitNJointController()
    {
        ARMARX_INFO << "init ...";
        //        controllerTask = new PeriodicTask<NJointTaskSpaceAdaptiveDMPController>(this, &NJointTaskSpaceAdaptiveDMPController::controllerRun, 1);
        runTask("NJointTaskSpaceAdaptiveDMPController", [&]
        {
            CycleUtil c(1);
            getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted);
            while (getState() == eManagedIceObjectStarted)
            {
                if (isControllerActive())
                {
                    controllerRun();
                }
                c.waitForCycleDuration();
            }
        });
    }

    void NJointTaskSpaceAdaptiveDMPController::onDisconnectNJointController()
    {
        //        controllerTask->stop();
    }



}
