#include "NJointBimanualCCDMPVelocityController.h"

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

namespace armarx
{
    NJointControllerRegistration<NJointBimanualCCDMPVelocityController> registrationControllerNJointBimanualCCDMPVelocityController("NJointBimanualCCDMPVelocityController");

    NJointBimanualCCDMPVelocityController::NJointBimanualCCDMPVelocityController(const RobotUnitPtr& robotUnit, const armarx::NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&)
    {
        ARMARX_INFO << "Preparing ... ";
        cfg = NJointBimanualCCDMPVelocityControllerConfigPtr::dynamicCast(config);

        ARMARX_CHECK_EXPRESSION(robotUnit);
        useSynchronizedRtRobot();

        leftRNS = rtGetRobot()->getRobotNodeSet("LeftArm");
        leftNullSpaceCoefs.resize(leftRNS->getSize());
        leftNullSpaceCoefs.setOnes();

        for (size_t i = 0; i < leftRNS->getSize(); ++i)
        {
            std::string jointName = leftRNS->getNode(i)->getName();

            if (leftRNS->getNode(i)->isLimitless())
            {
                leftNullSpaceCoefs(i) = 1;
            }

            leftJointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Velocity1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            leftTargets.push_back(ct->asA<ControlTarget1DoFActuatorVelocity>());
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
            const SensorValue1DoFActuatorAcceleration* accelerationSensor = sv->asA<SensorValue1DoFActuatorAcceleration>();

            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }

            if (!accelerationSensor)
            {
                ARMARX_WARNING << "No accelerationSensor available for " << jointName;
            }


            leftVelocitySensors.push_back(velocitySensor);
            leftPositionSensors.push_back(positionSensor);
            leftAccelerationSensors.push_back(accelerationSensor);

        };
        rightRNS = rtGetRobot()->getRobotNodeSet("RightArm");

        rightNullSpaceCoefs.resize(rightRNS->getSize());
        rightNullSpaceCoefs.setOnes();
        for (size_t i = 0; i < rightRNS->getSize(); ++i)
        {
            std::string jointName = rightRNS->getNode(i)->getName();

            if (rightRNS->getNode(i)->isLimitless())
            {
                rightNullSpaceCoefs(i) = 1;
            }

            rightJointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Velocity1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            rightTargets.push_back(ct->asA<ControlTarget1DoFActuatorVelocity>());
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
            const SensorValue1DoFActuatorAcceleration* accelerationSensor = sv->asA<SensorValue1DoFActuatorAcceleration>();


            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }
            if (!accelerationSensor)
            {
                ARMARX_WARNING << "No accelerationSensor available for " << jointName;
            }

            rightVelocitySensors.push_back(velocitySensor);
            rightPositionSensors.push_back(positionSensor);
            rightAccelerationSensors.push_back(accelerationSensor);

        };


        const SensorValueBase* svlf = useSensorValue("FT L");
        leftForceTorque = svlf->asA<SensorValueForceTorque>();
        const SensorValueBase* svrf = useSensorValue("FT R");
        rightForceTorque = svrf->asA<SensorValueForceTorque>();

        leftIK.reset(new VirtualRobot::DifferentialIK(leftRNS, rtGetRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));
        rightIK.reset(new VirtualRobot::DifferentialIK(rightRNS, rtGetRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));


        leftCtrl.reset(new CartesianVelocityController(leftRNS, leftRNS->getTCP()));
        rightCtrl.reset(new CartesianVelocityController(rightRNS, rightRNS->getTCP()));

        TaskSpaceDMPControllerConfig taskSpaceDMPConfig;
        taskSpaceDMPConfig.motionTimeDuration = cfg->timeDuration;
        taskSpaceDMPConfig.DMPKernelSize = cfg->kernelSize;
        taskSpaceDMPConfig.DMPMode = cfg->dmpMode;
        taskSpaceDMPConfig.DMPStyle = cfg->dmpType;
        taskSpaceDMPConfig.DMPAmplitude = 1.0;


        TaskSpaceDMPControllerPtr leftLeader(new TaskSpaceDMPController("leftLeader", taskSpaceDMPConfig, false));
        TaskSpaceDMPControllerPtr leftFollower(new TaskSpaceDMPController("leftFollower", taskSpaceDMPConfig, false));
        TaskSpaceDMPControllerPtr rightLeader(new TaskSpaceDMPController("rightLeader", taskSpaceDMPConfig, false));
        TaskSpaceDMPControllerPtr rightFollower(new TaskSpaceDMPController("rightFollower", taskSpaceDMPConfig, false));
        leftJointDMP.reset(new DMP::UMIDMP(10));
        rightJointDMP.reset(new DMP::UMIDMP(10));
        isLeftJointLearned = false;
        isRightJointLearned = false;

        started = false;
        isDMPRun = false;

        leftGroup.push_back(leftLeader);
        leftGroup.push_back(rightFollower);

        rightGroup.push_back(rightLeader);
        rightGroup.push_back(leftFollower);

        bothLeaderGroup.push_back(leftLeader);
        bothLeaderGroup.push_back(rightLeader);


        tcpLeft = leftRNS->getTCP();
        tcpRight = rightRNS->getTCP();


        leaderName = cfg->defautLeader;


        leftDesiredJointValues.resize(leftTargets.size());
        ARMARX_CHECK_EQUAL(cfg->leftDesiredJointValues.size(), leftTargets.size());

        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            leftDesiredJointValues(i) = cfg->leftDesiredJointValues.at(i);
        }

        rightDesiredJointValues.resize(rightTargets.size());
        ARMARX_CHECK_EQUAL(cfg->rightDesiredJointValues.size(), rightTargets.size());

        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            rightDesiredJointValues(i) = cfg->rightDesiredJointValues.at(i);
        }


        leftKpos << cfg->leftKpos[0], cfg->leftKpos[1], cfg->leftKpos[2];
        leftDpos << cfg->leftDpos[0], cfg->leftDpos[1], cfg->leftDpos[2];
        leftKori << cfg->leftKori[0], cfg->leftKori[1], cfg->leftKori[2];
        leftDori << cfg->leftDori[0], cfg->leftDori[1], cfg->leftDori[2];

        rightKpos << cfg->rightKpos[0], cfg->rightKpos[1], cfg->rightKpos[2];
        rightDpos << cfg->rightDpos[0], cfg->rightDpos[1], cfg->rightDpos[2];
        rightKori << cfg->rightKori[0], cfg->rightKori[1], cfg->rightKori[2];
        rightDori << cfg->rightDori[0], cfg->rightDori[1], cfg->rightDori[2];

        knull = cfg->knull;
        dnull = cfg->dnull;


        timeDuration = cfg->timeDuration;

        maxLinearVel = cfg->maxLinearVel;
        maxAngularVel = cfg->maxAngularVel;

        NJointBimanualCCDMPVelocityControllerInterfaceData initInterfaceData;
        initInterfaceData.currentLeftPose = Eigen::Matrix4f::Identity();
        initInterfaceData.currentRightPose = Eigen::Matrix4f::Identity();
        initInterfaceData.currentLeftJointVals.setZero(leftTargets.size());
        initInterfaceData.currentRightJointVals.setZero(rightTargets.size());
        interfaceData.reinitAllBuffers(initInterfaceData);

        NJointBimanualCCDMPVelocityControllerSensorData initSensorData;
        initSensorData.deltaT = 0;
        initSensorData.currentTime = 0;
        initSensorData.currentLeftPose = Eigen::Matrix4f::Identity();
        initSensorData.currentRightPose = Eigen::Matrix4f::Identity();
        controllerSensorData.reinitAllBuffers(initSensorData);
    }

    std::string NJointBimanualCCDMPVelocityController::getClassName(const Ice::Current&) const
    {
        return "NJointBimanualCCDMPVelocityController";
    }

    void NJointBimanualCCDMPVelocityController::rtPreActivateController()
    {
        NJointBimanualCCDMPVelocityControllerControlData initData;
        initData.leftTargetVel.resize(6);
        initData.leftTargetVel.setZero();
        initData.rightTargetVel.resize(6);
        initData.rightTargetVel.setZero();
        initData.leftTargetPose = tcpLeft->getPoseInRootFrame();
        initData.rightTargetPose = tcpRight->getPoseInRootFrame();
        initData.virtualTime = cfg->timeDuration;
        reinitTripleBuffer(initData);
    }


    void NJointBimanualCCDMPVelocityController::controllerRun()
    {
        if (!started)
        {
            return;
        }

        if (!controllerSensorData.updateReadBuffer())
        {
            return;
        }
        double deltaT = controllerSensorData.getReadBuffer().deltaT;


        Eigen::VectorXf leftTargetVel;
        leftTargetVel.resize(6);
        leftTargetVel.setZero();
        Eigen::VectorXf rightTargetVel;
        rightTargetVel.resize(6);
        rightTargetVel.setZero();
        Eigen::Matrix4f leftTargetPose;
        Eigen::Matrix4f rightTargetPose;

        std::vector<TaskSpaceDMPControllerPtr > currentControlGroup;
        Eigen::Matrix4f currentLeaderPose;
        Eigen::Matrix4f currentFollowerPose;
        Eigen::VectorXf currentLeaderTwist;
        Eigen::VectorXf currentFollowerTwist;



        // get desired joint values
        if (leaderName == "Both")
        {
            currentControlGroup = bothLeaderGroup;

            TaskSpaceDMPControllerPtr leaderDMPleft = currentControlGroup[0];
            TaskSpaceDMPControllerPtr leaderDMPright = currentControlGroup[1];
            leaderDMPleft->flow(deltaT, controllerSensorData.getReadBuffer().currentLeftPose, controllerSensorData.getReadBuffer().currentLeftTwist);
            leaderDMPright->flow(deltaT, controllerSensorData.getReadBuffer().currentRightPose, controllerSensorData.getReadBuffer().currentRightTwist);

            leftTargetVel = leaderDMPleft->getTargetVelocity();
            leftTargetPose = leaderDMPleft->getTargetPoseMat();
            rightTargetVel = leaderDMPright->getTargetVelocity();
            rightTargetPose = leaderDMPright->getTargetPoseMat();

            virtualtimer = leaderDMPleft->canVal;
        }
        else
        {
            if (leaderName == "Left")
            {
                currentControlGroup = leftGroup;
                currentLeaderPose = controllerSensorData.getReadBuffer().currentLeftPose;
                currentFollowerPose = controllerSensorData.getReadBuffer().currentRightPose;
                currentLeaderTwist = controllerSensorData.getReadBuffer().currentLeftTwist;
                currentFollowerTwist = controllerSensorData.getReadBuffer().currentRightTwist;
            }
            else if (leaderName == "Right")
            {
                currentControlGroup = rightGroup;
                currentLeaderPose = controllerSensorData.getReadBuffer().currentRightPose;
                currentFollowerPose = controllerSensorData.getReadBuffer().currentLeftPose;
                currentLeaderTwist = controllerSensorData.getReadBuffer().currentRightTwist;
                currentFollowerTwist = controllerSensorData.getReadBuffer().currentLeftTwist;
            }

            TaskSpaceDMPControllerPtr leaderDMP = currentControlGroup[0];
            TaskSpaceDMPControllerPtr followerDMP = currentControlGroup[1];
            virtualtimer = leaderDMP->canVal;

            if (virtualtimer < 1e-8)
            {
                finished = true;
                started = false;
                isDMPRun = false;
            }


            leaderDMP->flow(deltaT, currentLeaderPose, currentLeaderTwist);

            Eigen::Matrix4f currentFollowerLocalPose;
            currentFollowerLocalPose.block<3, 3>(0, 0) = currentLeaderPose.block<3, 3>(0, 0).inverse() * currentFollowerPose.block<3, 3>(0, 0);
            currentFollowerLocalPose.block<3, 1>(0, 3) = currentLeaderPose.block<3, 3>(0, 0).inverse() * (currentFollowerPose.block<3, 1>(0, 3) - currentLeaderPose.block<3, 1>(0, 3));
            followerDMP->flow(deltaT, currentFollowerLocalPose, currentFollowerTwist);

            Eigen::VectorXf leaderTargetVel = leaderDMP->getTargetVelocity();
            Eigen::Matrix4f leaderTargetPose = leaderDMP->getTargetPoseMat();
            Eigen::Matrix4f followerLocalTargetPose = followerDMP->getTargetPoseMat();
            std::vector<double> followerLocalTargetPoseVec = followerDMP->getTargetPose();

            Eigen::Matrix4f followerTargetPose;
            followerTargetPose.block<3, 3>(0, 0) = currentLeaderPose.block<3, 3>(0, 0) * followerLocalTargetPose.block<3, 3>(0, 0);
            followerTargetPose.block<3, 1>(0, 3) = currentLeaderPose.block<3, 3>(0, 0) * followerLocalTargetPose.block<3, 1>(0, 3) + currentLeaderPose.block<3, 1>(0, 3);


            Eigen::VectorXf followerLocalTargetVel = followerDMP->getTargetVelocity();
            Eigen::VectorXf followerTargetVel = followerLocalTargetVel;
            followerTargetVel.setZero();

            std::vector<double> leftDMPTarget;
            std::vector<double> rightDMPTarget;

            if (leaderName == "Left")
            {
                leftTargetVel = leaderTargetVel;
                rightTargetVel = followerTargetVel;
                leftDMPTarget = leaderDMP->getTargetPose();
                rightDMPTarget = followerLocalTargetPoseVec;
                leftTargetPose = leaderTargetPose;
                rightTargetPose = followerTargetPose;
            }
            else if (leaderName == "Right")
            {
                rightTargetVel = leaderTargetVel;
                leftTargetVel = followerTargetVel;
                rightDMPTarget = leaderDMP->getTargetPose();
                leftDMPTarget = followerLocalTargetPoseVec;
                rightTargetPose = leaderTargetPose;
                leftTargetPose = followerTargetPose;
            }
        }


        Eigen::VectorXf leftDesiredJoint = leftDesiredJointValues;
        Eigen::VectorXf rightDesiredJoint = rightDesiredJointValues;
        if (isLeftJointLearned)
        {
            DMP::DVec targetJointState;
            currentLeftJointState = leftJointDMP->calculateDirectlyVelocity(currentLeftJointState, virtualtimer / timeDuration, deltaT / timeDuration, targetJointState);
            for (size_t i = 0; i < targetJointState.size(); ++i)
            {
                leftDesiredJoint(i) = targetJointState[i];
            }
        }

        if (isRightJointLearned)
        {
            DMP::DVec targetJointState;
            currentRightJointState = rightJointDMP->calculateDirectlyVelocity(currentRightJointState, virtualtimer / timeDuration, deltaT / timeDuration, targetJointState);
            for (size_t i = 0; i < targetJointState.size(); ++i)
            {
                rightDesiredJoint(i) = targetJointState[i];
            }
        }

        LockGuardType guard {controlDataMutex};
        getWriterControlStruct().leftTargetVel = leftTargetVel;
        getWriterControlStruct().rightTargetVel = rightTargetVel;
        getWriterControlStruct().leftTargetPose = leftTargetPose;
        getWriterControlStruct().rightTargetPose = rightTargetPose;
        getWriterControlStruct().leftDesiredJoint = leftDesiredJoint;
        getWriterControlStruct().rightDesiredJoint = rightDesiredJoint;
        getWriterControlStruct().virtualTime = virtualtimer;
        writeControlStruct();
        isDMPRun = true;
    }

    Eigen::VectorXf NJointBimanualCCDMPVelocityController::getControlWrench(const Eigen::VectorXf& tcptwist, const Eigen::Matrix4f& currentPose, const Eigen::Matrix4f& targetPose)
    {
        return Eigen::Vector6f::Zero();
    }



    void NJointBimanualCCDMPVelocityController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        double deltaT = timeSinceLastIteration.toSecondsDouble();

        controllerSensorData.getWriteBuffer().currentLeftPose = tcpLeft->getPoseInRootFrame();
        controllerSensorData.getWriteBuffer().currentRightPose = tcpRight->getPoseInRootFrame();
        controllerSensorData.getWriteBuffer().deltaT = deltaT;
        controllerSensorData.getWriteBuffer().currentTime += deltaT;

        // cartesian vel controller
        Eigen::MatrixXf I = Eigen::MatrixXf::Identity(leftTargets.size(), leftTargets.size());

        Eigen::MatrixXf jacobiL = leftIK->getJacobianMatrix(tcpLeft, VirtualRobot::IKSolver::CartesianSelection::All);

        Eigen::VectorXf leftqpos;
        Eigen::VectorXf leftqvel;
        leftqpos.resize(leftPositionSensors.size());
        leftqvel.resize(leftVelocitySensors.size());
        for (size_t i = 0; i < leftVelocitySensors.size(); ++i)
        {
            leftqpos(i) = leftPositionSensors[i]->position;
            leftqvel(i) = leftVelocitySensors[i]->velocity;
        }

        Eigen::MatrixXf jacobiR = rightIK->getJacobianMatrix(tcpRight, VirtualRobot::IKSolver::CartesianSelection::All);
        Eigen::VectorXf rightqpos;
        Eigen::VectorXf rightqvel;
        rightqpos.resize(rightPositionSensors.size());
        rightqvel.resize(rightVelocitySensors.size());

        for (size_t i = 0; i < rightVelocitySensors.size(); ++i)
        {
            rightqpos(i) = rightPositionSensors[i]->position;
            rightqvel(i) = rightVelocitySensors[i]->velocity;
        }

        Eigen::VectorXf currentLeftTwist = jacobiL * leftqvel;
        Eigen::VectorXf currentRightTwist = jacobiR * rightqvel;

        controllerSensorData.getWriteBuffer().currentLeftTwist = currentLeftTwist;
        controllerSensorData.getWriteBuffer().currentRightTwist = currentRightTwist;
        controllerSensorData.commitWrite();


        interfaceData.getWriteBuffer().currentLeftPose = tcpLeft->getPoseInRootFrame();
        interfaceData.getWriteBuffer().currentRightPose = tcpRight->getPoseInRootFrame();
        interfaceData.getWriteBuffer().currentLeftJointVals = leftqpos;
        interfaceData.getWriteBuffer().currentRightJointVals = rightqpos;
        interfaceData.commitWrite();

        if (!isDMPRun)
        {
            for (size_t i = 0; i < leftTargets.size(); ++i)
            {
                leftTargets.at(i)->velocity = 0;
            }
            for (size_t i = 0; i < rightTargets.size(); ++i)
            {
                rightTargets.at(i)->velocity = 0;
            }
        }
        else
        {
            Eigen::Matrix4f leftTargetPose = rtGetControlStruct().leftTargetPose;
            Eigen::Matrix4f rightTargetPose = rtGetControlStruct().rightTargetPose;
            double virtualtime = rtGetControlStruct().virtualTime;
            Eigen::Matrix4f leftCurrentPose = tcpLeft->getPoseInRootFrame();
            Eigen::Matrix4f rightCurrentPose = tcpRight->getPoseInRootFrame();
            Eigen::VectorXf leftTargetVel = rtGetControlStruct().leftTargetVel;
            Eigen::VectorXf rightTargetVel = rtGetControlStruct().rightTargetVel;
            Eigen::VectorXf leftDesiredJoint = rtGetControlStruct().leftDesiredJoint;
            Eigen::VectorXf rightDesiredJoint = rtGetControlStruct().rightDesiredJoint;

            // generate joint control signals
            Eigen::VectorXf leftCartesianTarget(6);
            {
                Eigen::Vector3f targetTCPLinearVelocity;
                targetTCPLinearVelocity << leftTargetVel(0), leftTargetVel(1), leftTargetVel(2);
                Eigen::Vector3f currentTCPLinearVelocity;
                currentTCPLinearVelocity <<  currentLeftTwist(0), currentLeftTwist(1), currentLeftTwist(2);
                Eigen::Vector3f currentTCPPosition = leftCurrentPose.block<3, 1>(0, 3);
                Eigen::Vector3f desiredPosition = leftTargetPose.block<3, 1>(0, 3);
                Eigen::Vector3f posVel = leftKpos.cwiseProduct(desiredPosition - currentTCPPosition) + leftDpos.cwiseProduct(targetTCPLinearVelocity - currentTCPLinearVelocity);


                Eigen::Vector3f currentTCPAngularVelocity;
                currentTCPAngularVelocity << currentLeftTwist(3),   currentLeftTwist(4),  currentLeftTwist(5);
                Eigen::Matrix3f currentRotMat = leftCurrentPose.block<3, 3>(0, 0);
                Eigen::Matrix3f diffMat = leftTargetPose.block<3, 3>(0, 0) * currentRotMat.inverse();
                Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
                Eigen::Vector3f oriVel = leftKori.cwiseProduct(rpy) - leftDori.cwiseProduct(currentTCPAngularVelocity);

                float normLinearVelocity = posVel.norm();
                if (normLinearVelocity > maxLinearVel)
                {
                    posVel = maxLinearVel * posVel / normLinearVelocity;
                }
                float normAngularVelocity = oriVel.norm();
                if (normAngularVelocity > maxAngularVel)
                {
                    oriVel = maxAngularVel * oriVel / normAngularVelocity;
                }

                leftCartesianTarget << posVel, oriVel;
            }

            Eigen::VectorXf rightCartesianTarget(6);
            {
                Eigen::Vector3f targetTCPLinearVelocity;
                targetTCPLinearVelocity << rightTargetVel(0), rightTargetVel(1), rightTargetVel(2);
                Eigen::Vector3f currentTCPLinearVelocity;
                currentTCPLinearVelocity <<  currentRightTwist(0), currentRightTwist(1), currentRightTwist(2);
                Eigen::Vector3f currentTCPPosition = rightCurrentPose.block<3, 1>(0, 3);
                Eigen::Vector3f desiredPosition = rightTargetPose.block<3, 1>(0, 3);
                Eigen::Vector3f posVel = rightKpos.cwiseProduct(desiredPosition - currentTCPPosition) + rightDpos.cwiseProduct(targetTCPLinearVelocity - currentTCPLinearVelocity);


                Eigen::Vector3f currentTCPAngularVelocity;
                currentTCPAngularVelocity << currentRightTwist(3),   currentRightTwist(4),  currentRightTwist(5);
                Eigen::Matrix3f currentRotMat = rightCurrentPose.block<3, 3>(0, 0);
                Eigen::Matrix3f diffMat = rightTargetPose.block<3, 3>(0, 0) * currentRotMat.inverse();
                Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
                Eigen::Vector3f oriVel = rightKori.cwiseProduct(rpy) - rightDori.cwiseProduct(currentTCPAngularVelocity);

                float normLinearVelocity = posVel.norm();
                if (normLinearVelocity > maxLinearVel)
                {
                    posVel = maxLinearVel * posVel / normLinearVelocity;
                }
                float normAngularVelocity = oriVel.norm();
                if (normAngularVelocity > maxAngularVel)
                {
                    oriVel = maxAngularVel * oriVel / normAngularVelocity;
                }

                rightCartesianTarget << posVel, oriVel;

            }

            Eigen::VectorXf leftNullspaceVel = knull * (leftDesiredJoint - leftqpos) - dnull * leftqvel;
            Eigen::VectorXf rightNullspaceVel = knull * (rightDesiredJoint - rightqpos) - dnull * rightqvel;
            Eigen::VectorXf desiredLeftVels = leftCtrl->calculate(leftCartesianTarget, leftNullspaceVel, VirtualRobot::IKSolver::CartesianSelection::All);
            Eigen::VectorXf desiredRightVels = rightCtrl->calculate(rightCartesianTarget, rightNullspaceVel, VirtualRobot::IKSolver::CartesianSelection::All);

            // torque limit
            for (size_t i = 0; i < leftTargets.size(); ++i)
            {

                float desiredVel = desiredLeftVels[i];

                if (fabs(desiredVel) > cfg->maxJointVel)
                {
                    desiredVel = cfg->maxJointVel * desiredVel / fabs(desiredVel);
                }

                leftTargets.at(i)->velocity = desiredVel;
                debugDataInfo.getWriteBuffer().desired_velocities[leftJointNames[i]] = desiredVel;

            }

            for (size_t i = 0; i < rightTargets.size(); ++i)
            {
                float desiredVel = desiredRightVels[i];

                if (fabs(desiredVel) > cfg->maxJointVel)
                {
                    desiredVel = cfg->maxJointVel * desiredVel / fabs(desiredVel);
                }

                rightTargets.at(i)->velocity = desiredVel;
                debugDataInfo.getWriteBuffer().desired_velocities[rightJointNames[i]] = desiredVel;
            }


            debugDataInfo.getWriteBuffer().leftControlSignal_x = leftCartesianTarget(0);
            debugDataInfo.getWriteBuffer().leftControlSignal_y = leftCartesianTarget(1);
            debugDataInfo.getWriteBuffer().leftControlSignal_z = leftCartesianTarget(2);
            debugDataInfo.getWriteBuffer().leftControlSignal_ro = leftCartesianTarget(3);
            debugDataInfo.getWriteBuffer().leftControlSignal_pi = leftCartesianTarget(4);
            debugDataInfo.getWriteBuffer().leftControlSignal_ya = leftCartesianTarget(5);

            debugDataInfo.getWriteBuffer().rightControlSignal_x = rightCartesianTarget(0);
            debugDataInfo.getWriteBuffer().rightControlSignal_y = rightCartesianTarget(1);
            debugDataInfo.getWriteBuffer().rightControlSignal_z = rightCartesianTarget(2);
            debugDataInfo.getWriteBuffer().rightControlSignal_ro = rightCartesianTarget(3);
            debugDataInfo.getWriteBuffer().rightControlSignal_pi = rightCartesianTarget(4);
            debugDataInfo.getWriteBuffer().rightControlSignal_ya = rightCartesianTarget(5);

            debugDataInfo.getWriteBuffer().leftTargetPose_x = leftTargetPose(0, 3);
            debugDataInfo.getWriteBuffer().leftTargetPose_y = leftTargetPose(1, 3);
            debugDataInfo.getWriteBuffer().leftTargetPose_z = leftTargetPose(2, 3);
            debugDataInfo.getWriteBuffer().leftCurrentPose_x = leftCurrentPose(0, 3);
            debugDataInfo.getWriteBuffer().leftCurrentPose_y = leftCurrentPose(1, 3);
            debugDataInfo.getWriteBuffer().leftCurrentPose_z = leftCurrentPose(2, 3);


            debugDataInfo.getWriteBuffer().rightTargetPose_x = rightTargetPose(0, 3);
            debugDataInfo.getWriteBuffer().rightTargetPose_y = rightTargetPose(1, 3);
            debugDataInfo.getWriteBuffer().rightTargetPose_z = rightTargetPose(2, 3);

            debugDataInfo.getWriteBuffer().rightCurrentPose_x = rightCurrentPose(0, 3);
            debugDataInfo.getWriteBuffer().rightCurrentPose_y = rightCurrentPose(1, 3);
            debugDataInfo.getWriteBuffer().rightCurrentPose_z = rightCurrentPose(2, 3);
            debugDataInfo.getWriteBuffer().virtualTime = virtualtime;

            debugDataInfo.commitWrite();
        }

    }

    void NJointBimanualCCDMPVelocityController::learnDMPFromFiles(const std::string& name, const Ice::StringSeq& fileNames, const Ice::Current&)
    {
        if (name == "LeftLeader")
        {
            leftGroup.at(0)->learnDMPFromFiles(fileNames);
        }
        else if (name == "LeftFollower")
        {
            rightGroup.at(1)->learnDMPFromFiles(fileNames);
        }
        else if (name == "RightLeader")
        {
            rightGroup.at(0)->learnDMPFromFiles(fileNames);
        }
        else if (name == "RightFollower")
        {
            leftGroup.at(1)->learnDMPFromFiles(fileNames);
        }
        else if (name == "LeftJoint")
        {
            DMP::Vec<DMP::SampledTrajectoryV2 > trajs;
            DMP::SampledTrajectoryV2 traj;
            std::string absPath;
            ArmarXDataPath::getAbsolutePath(fileNames.at(0), absPath);
            traj.readFromCSVFile(absPath);
            traj = DMP::SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
            trajs.push_back(traj);
            leftJointDMP->learnFromTrajectories(trajs);
        }
        else if (name == "RightJoint")
        {
            DMP::Vec<DMP::SampledTrajectoryV2 > trajs;
            DMP::SampledTrajectoryV2 traj;
            std::string absPath;
            ArmarXDataPath::getAbsolutePath(fileNames.at(0), absPath);
            traj.readFromCSVFile(absPath);
            traj = DMP::SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
            trajs.push_back(traj);
            rightJointDMP->learnFromTrajectories(trajs);
        }
        else
        {
            ARMARX_ERROR << "The given name is not supported by CCDMP, the supported names contain ";
        }

    }

    void NJointBimanualCCDMPVelocityController::learnDMPFromBothFiles(const Ice::StringSeq& leftFiles, const Ice::StringSeq& rightFiles, const Ice::Current&)
    {
        ARMARX_CHECK_EQUAL(leftFiles.size(), rightFiles.size());

        DMP::Vec<DMP::SampledTrajectoryV2 > leftFollowerTrajs;
        DMP::Vec<DMP::SampledTrajectoryV2 > rightFollowerTrajs;

        for (size_t i = 0; i < leftFiles.size(); ++i)
        {
            DMP::SampledTrajectoryV2 leftFollowerTraj = DMP::RobotHelpers::getRelativeTrajFromFiles(leftFiles[i], rightFiles[i]);
            leftFollowerTrajs.push_back(leftFollowerTraj);
            DMP::SampledTrajectoryV2 rightFollowerTraj = DMP::RobotHelpers::getRelativeTrajFromFiles(rightFiles[i], leftFiles[i]);
            rightFollowerTrajs.push_back(rightFollowerTraj);
        }

        leftGroup.at(0)->learnDMPFromFiles(leftFiles, cfg->initRatio);
        leftGroup.at(1)->learnDMPFromSampledTrajectory(leftFollowerTrajs, cfg->initRatio);
        rightGroup.at(0)->learnDMPFromFiles(rightFiles, cfg->initRatio);
        rightGroup.at(1)->learnDMPFromSampledTrajectory(rightFollowerTrajs, cfg->initRatio);
    }

    void NJointBimanualCCDMPVelocityController::setRatios(const Ice::DoubleSeq& ratios, const Ice::Current&)
    {
        leftGroup.at(0)->setRatios(ratios);
        leftGroup.at(1)->setRatios(ratios);
        rightGroup.at(0)->setRatios(ratios);
        rightGroup.at(1)->setRatios(ratios);
    }


    void NJointBimanualCCDMPVelocityController::setViaPoints(Ice::Double u, const Ice::DoubleSeq& viapoint, const Ice::Current&)
    {
        LockGuardType guard(controllerMutex);
        if (leaderName == "Left")
        {
            leftGroup.at(0)->setViaPose(u, viapoint);
        }
        else
        {
            rightGroup.at(0)->setViaPose(u, viapoint);
        }
    }

    void NJointBimanualCCDMPVelocityController::setGoals(const Ice::DoubleSeq& goals, const Ice::Current& ice)
    {
        LockGuardType guard(controllerMutex);
        if (leaderName == "Left")
        {
            leftGroup.at(0)->setGoalPoseVec(goals);
        }
        else
        {
            rightGroup.at(0)->setGoalPoseVec(goals);
        }

    }

    void NJointBimanualCCDMPVelocityController::changeLeader(const Ice::Current&)
    {
        LockGuardType guard(controllerMutex);

        if (leaderName == "Left")
        {
            leaderName = "Right";
            rightGroup.at(0)->canVal = virtualtimer;
            rightGroup.at(1)->canVal = virtualtimer;
        }
        else
        {
            leaderName = "Left";
            leftGroup.at(0)->canVal = virtualtimer;
            leftGroup.at(1)->canVal = virtualtimer;
        }

    }


    void NJointBimanualCCDMPVelocityController::runDMP(const Ice::DoubleSeq& leftGoals, const Ice::DoubleSeq& rightGoals, const Ice::Current&)
    {
        ARMARX_INFO << "start running ccdmp";
        while (!interfaceData.updateReadBuffer())
        {
            usleep(100);
        }
        Eigen::Matrix4f leftPose = interfaceData.getReadBuffer().currentLeftPose;
        Eigen::Matrix4f rightPose = interfaceData.getReadBuffer().currentRightPose;
        Eigen::VectorXf leftJointVals = interfaceData.getReadBuffer().currentLeftJointVals;
        Eigen::VectorXf rightJointVals = interfaceData.getReadBuffer().currentRightJointVals;

        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            DMP::DMPState jstate;
            jstate.pos = leftJointVals(i);
            jstate.vel = 0;
            currentLeftJointState.push_back(jstate);
        }

        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            DMP::DMPState jstate;
            jstate.pos = rightJointVals(i);
            jstate.vel = 0;
            currentRightJointState.push_back(jstate);
        }

        virtualtimer = cfg->timeDuration;
        leftGroup.at(0)->prepareExecution(leftGroup.at(0)->eigen4f2vec(leftPose), leftGoals);
        rightGroup.at(0)->prepareExecution(rightGroup.at(0)->eigen4f2vec(rightPose), rightGoals);


        ARMARX_INFO << "leftgroup goal local pose: " << getLocalPose(leftGoals, rightGoals);

        leftGroup.at(1)->prepareExecution(getLocalPose(leftPose, rightPose), getLocalPose(leftGoals, rightGoals));
        rightGroup.at(1)->prepareExecution(getLocalPose(rightPose, leftPose), getLocalPose(rightGoals, leftGoals));

        finished = false;
        started = true;
    }

    Eigen::Matrix4f NJointBimanualCCDMPVelocityController::getLocalPose(const Eigen::Matrix4f& newCoordinate, const Eigen::Matrix4f& globalTargetPose)
    {
        Eigen::Matrix4f localPose = Eigen::Matrix4f::Identity();

        localPose.block<3, 3>(0, 0) = newCoordinate.block<3, 3>(0, 0).inverse() * globalTargetPose.block<3, 3>(0, 0);
        localPose.block<3, 1>(0, 3) = newCoordinate.block<3, 3>(0, 0).inverse() * (globalTargetPose.block<3, 1>(0, 3) - newCoordinate.block<3, 1>(0, 3));


        return localPose;
    }


    void NJointBimanualCCDMPVelocityController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& debugObs)
    {

        StringVariantBaseMap datafields;
        auto values = debugDataInfo.getUpToDateReadBuffer().desired_velocities;
        for (auto& pair : values)
        {
            datafields[pair.first] = new Variant(pair.second);
        }

        auto constrained_force = debugDataInfo.getUpToDateReadBuffer().constrained_force;
        for (auto& pair : constrained_force)
        {
            datafields[pair.first] = new Variant(pair.second);
        }


        datafields["leftTargetPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftTargetPose_x);
        datafields["leftTargetPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftTargetPose_y);
        datafields["leftTargetPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftTargetPose_z);
        datafields["rightTargetPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightTargetPose_x);
        datafields["rightTargetPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightTargetPose_y);
        datafields["rightTargetPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightTargetPose_z);


        datafields["leftCurrentPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftCurrentPose_x);
        datafields["leftCurrentPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftCurrentPose_y);
        datafields["leftCurrentPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftCurrentPose_z);
        datafields["rightCurrentPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightCurrentPose_x);
        datafields["rightCurrentPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightCurrentPose_y);
        datafields["rightCurrentPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightCurrentPose_z);

        datafields["leftControlSignal_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_x);
        datafields["leftControlSignal_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_y);
        datafields["leftControlSignal_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_z);
        datafields["leftControlSignal_ro"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_ro);
        datafields["leftControlSignal_pi"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_pi);
        datafields["leftControlSignal_ya"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_ya);


        datafields["rightControlSignal_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_x);
        datafields["rightControlSignal_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_y);
        datafields["rightControlSignal_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_z);
        datafields["rightControlSignal_ro"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_ro);
        datafields["rightControlSignal_pi"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_pi);
        datafields["rightControlSignal_ya"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_ya);

        datafields["virtual_timer"] = new Variant(debugDataInfo.getUpToDateReadBuffer().virtualTime);


        debugObs->setDebugChannel("CCDMPVelocityController", datafields);
    }

    void NJointBimanualCCDMPVelocityController::onInitNJointController()
    {
        ARMARX_INFO << "init ...";
        started = false;
        runTask("NJointBimanualCCDMPVelocityController", [&]
        {
            CycleUtil c(1);
            getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted);
            while (getState() == eManagedIceObjectStarted)
            {
                if (isControllerActive())
                {
                    controllerRun();
                }
                c.waitForCycleDuration();
            }
        });
        ARMARX_INFO << "Finished init ...";

    }

    void NJointBimanualCCDMPVelocityController::onDisconnectNJointController()
    {
        ARMARX_INFO << "stopped ...";
    }



}
