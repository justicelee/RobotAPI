#include "NJointBimanualCCDMPController.h"

namespace armarx
{
    NJointControllerRegistration<NJointBimanualCCDMPController> registrationControllerNJointBimanualCCDMPController("NJointBimanualCCDMPController");

    NJointBimanualCCDMPController::NJointBimanualCCDMPController(const RobotUnitPtr& robotUnit, const armarx::NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&)
    {
        ARMARX_INFO << "Preparing ... ";
        cfg = NJointBimanualCCDMPControllerConfigPtr::dynamicCast(config);

        ARMARX_CHECK_EXPRESSION(robotUnit);
        useSynchronizedRtRobot();

        leftRNS = rtGetRobot()->getRobotNodeSet("LeftArm");
        leftNullSpaceCoefs.resize(leftRNS->getSize());
        leftNullSpaceCoefs.setOnes();

        for (size_t i = 0; i < leftRNS->getSize(); ++i)
        {
            std::string jointName = leftRNS->getNode(i)->getName();

            if (leftRNS->getNode(i)->isLimitless())
            {
                leftNullSpaceCoefs(i) = 1;
            }

            leftJointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            leftTargets.push_back(ct->asA<ControlTarget1DoFActuatorTorque>());
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
            const SensorValue1DoFActuatorAcceleration* accelerationSensor = sv->asA<SensorValue1DoFActuatorAcceleration>();

            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }

            if (!accelerationSensor)
            {
                ARMARX_WARNING << "No accelerationSensor available for " << jointName;
            }


            leftVelocitySensors.push_back(velocitySensor);
            leftPositionSensors.push_back(positionSensor);
            leftAccelerationSensors.push_back(accelerationSensor);

        };
        rightRNS = rtGetRobot()->getRobotNodeSet("RightArm");

        rightNullSpaceCoefs.resize(rightRNS->getSize());
        rightNullSpaceCoefs.setOnes();
        for (size_t i = 0; i < rightRNS->getSize(); ++i)
        {
            std::string jointName = rightRNS->getNode(i)->getName();

            if (rightRNS->getNode(i)->isLimitless())
            {
                rightNullSpaceCoefs(i) = 1;
            }

            rightJointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            rightTargets.push_back(ct->asA<ControlTarget1DoFActuatorTorque>());
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
            const SensorValue1DoFActuatorAcceleration* accelerationSensor = sv->asA<SensorValue1DoFActuatorAcceleration>();


            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }
            if (!accelerationSensor)
            {
                ARMARX_WARNING << "No accelerationSensor available for " << jointName;
            }

            rightVelocitySensors.push_back(velocitySensor);
            rightPositionSensors.push_back(positionSensor);
            rightAccelerationSensors.push_back(accelerationSensor);

        };


        const SensorValueBase* svlf = useSensorValue("FT L");
        leftForceTorque = svlf->asA<SensorValueForceTorque>();
        const SensorValueBase* svrf = useSensorValue("FT R");
        rightForceTorque = svrf->asA<SensorValueForceTorque>();

        leftIK.reset(new VirtualRobot::DifferentialIK(leftRNS, rtGetRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));
        rightIK.reset(new VirtualRobot::DifferentialIK(rightRNS, rtGetRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));


        TaskSpaceDMPControllerConfig taskSpaceDMPConfig;
        taskSpaceDMPConfig.motionTimeDuration = cfg->timeDuration;
        taskSpaceDMPConfig.DMPKernelSize = cfg->kernelSize;
        taskSpaceDMPConfig.DMPMode = cfg->dmpMode;
        taskSpaceDMPConfig.DMPStyle = cfg->dmpType;
        taskSpaceDMPConfig.DMPAmplitude = 1.0;
        taskSpaceDMPConfig.phaseStopParas.goDist = cfg->phaseDist0;
        taskSpaceDMPConfig.phaseStopParas.backDist = cfg->phaseDist1;
        taskSpaceDMPConfig.phaseStopParas.Kpos = cfg->phaseKpPos;
        taskSpaceDMPConfig.phaseStopParas.Dpos = 0;
        taskSpaceDMPConfig.phaseStopParas.Kori = cfg->phaseKpOri;
        taskSpaceDMPConfig.phaseStopParas.Dori = 0;
        taskSpaceDMPConfig.phaseStopParas.mm2radi = cfg->posToOriRatio;
        taskSpaceDMPConfig.phaseStopParas.maxValue = cfg->phaseL;
        taskSpaceDMPConfig.phaseStopParas.slop = cfg->phaseK;


        TaskSpaceDMPControllerPtr leftLeader(new TaskSpaceDMPController("leftLeader", taskSpaceDMPConfig, false));
        TaskSpaceDMPControllerPtr leftFollower(new TaskSpaceDMPController("leftFollower", taskSpaceDMPConfig, false));
        TaskSpaceDMPControllerPtr rightLeader(new TaskSpaceDMPController("rightLeader", taskSpaceDMPConfig, false));
        TaskSpaceDMPControllerPtr rightFollower(new TaskSpaceDMPController("rightFollower", taskSpaceDMPConfig, false));

        leftGroup.push_back(leftLeader);
        leftGroup.push_back(rightFollower);

        rightGroup.push_back(rightLeader);
        rightGroup.push_back(leftFollower);

        bothLeaderGroup.push_back(leftLeader);
        bothLeaderGroup.push_back(rightLeader);


        tcpLeft = leftRNS->getTCP();
        tcpRight = rightRNS->getTCP();


        leaderName = cfg->defautLeader;


        leftDesiredJointValues.resize(leftTargets.size());
        ARMARX_CHECK_EQUAL(cfg->leftDesiredJointValues.size(), leftTargets.size());

        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            leftDesiredJointValues(i) = cfg->leftDesiredJointValues.at(i);
        }

        rightDesiredJointValues.resize(rightTargets.size());
        ARMARX_CHECK_EQUAL(cfg->rightDesiredJointValues.size(), rightTargets.size());

        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            rightDesiredJointValues(i) = cfg->rightDesiredJointValues.at(i);
        }


        leftKpos << cfg->leftKpos[0], cfg->leftKpos[1], cfg->leftKpos[2];
        leftDpos << cfg->leftDpos[0], cfg->leftDpos[1], cfg->leftDpos[2];
        leftKori << cfg->leftKori[0], cfg->leftKori[1], cfg->leftKori[2];
        leftDori << cfg->leftDori[0], cfg->leftDori[1], cfg->leftDori[2];

        rightKpos << cfg->rightKpos[0], cfg->rightKpos[1], cfg->rightKpos[2];
        rightDpos << cfg->rightDpos[0], cfg->rightDpos[1], cfg->rightDpos[2];
        rightKori << cfg->rightKori[0], cfg->rightKori[1], cfg->rightKori[2];
        rightDori << cfg->rightDori[0], cfg->rightDori[1], cfg->rightDori[2];

        knull = cfg->knull;
        dnull = cfg->dnull;


        torqueLimit = cfg->torqueLimit;


        maxLinearVel = cfg->maxLinearVel;
        maxAngularVel = cfg->maxAngularVel;

        torqueFactor = 1.0;
        startReduceTorque = cfg->startReduceTorque;

        NJointBimanualCCDMPControllerInterfaceData initInterfaceData;
        initInterfaceData.currentLeftPose = Eigen::Matrix4f::Identity();
        initInterfaceData.currentRightPose = Eigen::Matrix4f::Identity();
        interfaceData.reinitAllBuffers(initInterfaceData);

        NJointBimanualCCDMPControllerSensorData initSensorData;
        initSensorData.deltaT = 0;
        initSensorData.currentTime = 0;
        initSensorData.currentLeftPose = Eigen::Matrix4f::Identity();
        initSensorData.currentRightPose = Eigen::Matrix4f::Identity();
        controllerSensorData.reinitAllBuffers(initSensorData);

    }

    std::string NJointBimanualCCDMPController::getClassName(const Ice::Current&) const
    {
        return "NJointBimanualCCDMPController";
    }

    void NJointBimanualCCDMPController::rtPreActivateController()
    {
        NJointBimanualCCDMPControllerControlData initData;
        initData.leftTargetVel.resize(6);
        initData.leftTargetVel.setZero();
        initData.rightTargetVel.resize(6);
        initData.rightTargetVel.setZero();
        initData.leftTargetPose = tcpLeft->getPoseInRootFrame();
        initData.rightTargetPose = tcpRight->getPoseInRootFrame();
        initData.virtualTime = cfg->timeDuration;
        reinitTripleBuffer(initData);
    }


    void NJointBimanualCCDMPController::controllerRun()
    {
        if (!controllerSensorData.updateReadBuffer())
        {
            return;
        }

        double deltaT = controllerSensorData.getReadBuffer().deltaT;


        Eigen::VectorXf leftTargetVel;
        leftTargetVel.resize(6);
        leftTargetVel.setZero();
        Eigen::VectorXf rightTargetVel;
        rightTargetVel.resize(6);
        rightTargetVel.setZero();

        std::vector<TaskSpaceDMPControllerPtr > currentControlGroup;
        Eigen::Matrix4f currentLeaderPose;
        Eigen::Matrix4f currentFollowerPose;
        Eigen::VectorXf currentLeaderTwist;
        Eigen::VectorXf currentFollowerTwist;
        if (leaderName == "Left")
        {
            currentControlGroup = leftGroup;
            currentLeaderPose = controllerSensorData.getReadBuffer().currentLeftPose;
            currentFollowerPose = controllerSensorData.getReadBuffer().currentRightPose;
            currentLeaderTwist = controllerSensorData.getReadBuffer().currentLeftTwist;
            currentFollowerTwist = controllerSensorData.getReadBuffer().currentRightTwist;
        }
        else if (leaderName == "right")
        {
            currentControlGroup = rightGroup;
            currentLeaderPose = controllerSensorData.getReadBuffer().currentRightPose;
            currentFollowerPose = controllerSensorData.getReadBuffer().currentLeftPose;
            currentLeaderTwist = controllerSensorData.getReadBuffer().currentRightTwist;
            currentFollowerTwist = controllerSensorData.getReadBuffer().currentLeftTwist;
        }
        else
        {
            currentControlGroup = bothLeaderGroup;

            TaskSpaceDMPControllerPtr leaderDMPleft = currentControlGroup[0];
            TaskSpaceDMPControllerPtr leaderDMPright = currentControlGroup[1];
            leaderDMPleft->flow(deltaT, controllerSensorData.getReadBuffer().currentLeftPose, controllerSensorData.getReadBuffer().currentLeftTwist);
            leaderDMPright->flow(deltaT, controllerSensorData.getReadBuffer().currentRightPose, controllerSensorData.getReadBuffer().currentRightTwist);

            Eigen::VectorXf leftTargetVel = leaderDMPleft->getTargetVelocity();
            Eigen::Matrix4f leftTargetPose = leaderDMPleft->getTargetPoseMat();
            Eigen::VectorXf rightTargetVel = leaderDMPright->getTargetVelocity();
            Eigen::Matrix4f rightTargetPose = leaderDMPright->getTargetPoseMat();

            virtualtimer = leaderDMPleft->canVal;
            LockGuardType guard {controlDataMutex};
            getWriterControlStruct().leftTargetVel = leftTargetVel;
            getWriterControlStruct().rightTargetVel = rightTargetVel;
            getWriterControlStruct().leftTargetPose = leftTargetPose;
            getWriterControlStruct().rightTargetPose = rightTargetPose;
            writeControlStruct();

            return;
        }

        TaskSpaceDMPControllerPtr leaderDMP = currentControlGroup[0];
        TaskSpaceDMPControllerPtr followerDMP = currentControlGroup[1];
        virtualtimer = leaderDMP->canVal;

        if (virtualtimer < 1e-8)
        {
            finished = true;
        }


        leaderDMP->flow(deltaT, currentLeaderPose, currentLeaderTwist);

        Eigen::Matrix4f currentFollowerLocalPose;
        currentFollowerLocalPose.block<3, 3>(0, 0) = currentLeaderPose.block<3, 3>(0, 0).inverse() * currentFollowerPose.block<3, 3>(0, 0);
        currentFollowerLocalPose.block<3, 1>(0, 3) = currentLeaderPose.block<3, 3>(0, 0).inverse() * (currentFollowerPose.block<3, 1>(0, 3) - currentLeaderPose.block<3, 1>(0, 3));
        followerDMP->flow(deltaT, currentFollowerLocalPose, currentFollowerTwist);

        Eigen::VectorXf leaderTargetVel = leaderDMP->getTargetVelocity();
        Eigen::Matrix4f leaderTargetPose = leaderDMP->getTargetPoseMat();
        Eigen::Matrix4f followerLocalTargetPose = followerDMP->getTargetPoseMat();
        std::vector<double> followerLocalTargetPoseVec = followerDMP->getTargetPose();

        Eigen::Matrix4f followerTargetPose;
        followerTargetPose.block<3, 3>(0, 0) = currentLeaderPose.block<3, 3>(0, 0) * followerLocalTargetPose.block<3, 3>(0, 0);
        followerTargetPose.block<3, 1>(0, 3) = currentLeaderPose.block<3, 3>(0, 0) * followerLocalTargetPose.block<3, 1>(0, 3) + currentLeaderPose.block<3, 1>(0, 3);


        Eigen::VectorXf followerLocalTargetVel = followerDMP->getTargetVelocity();
        Eigen::VectorXf followerTargetVel = followerLocalTargetVel;
        followerTargetVel.setZero();

        //        followerTargetVel.block<3, 1>(0, 0) = currentLeaderPose.block<3, 3>(0, 0) * followerLocalTargetVel.block<3, 1>(0, 0) + leaderTargetVel.block<3, 1>(0, 0);
        //        followerTargetVel.block<3, 1>(0, 0) = KposFollower * (followerTargetPose.block<3, 1>(0, 3) - currentFollowerPose.block<3, 1>(0, 3));
        //        Eigen::Matrix3f followerDiffMat =  followerTargetPose.block<3, 3>(0, 0) * currentFollowerPose.block<3, 3>(0, 0).inverse();
        //        Eigen::Vector3f followerDiffRPY = KoriFollower * VirtualRobot::MathTools::eigen3f2rpy(followerDiffMat);
        //        followerTargetVel(3) = followerDiffRPY(0);
        //        followerTargetVel(4) = followerDiffRPY(1);
        //        followerTargetVel(5) = followerDiffRPY(2);





        std::vector<double> leftDMPTarget;
        std::vector<double> rightDMPTarget;

        Eigen::Matrix4f leftTargetPose;
        Eigen::Matrix4f rightTargetPose;

        //        float leftKratio = 1.0;
        //        float rightKratio = 1.0;

        if (leaderName == "Left")
        {
            leftTargetVel = leaderTargetVel;
            rightTargetVel = followerTargetVel;

            leftDMPTarget = leaderDMP->getTargetPose();
            rightDMPTarget = followerLocalTargetPoseVec;


            leftTargetPose = leaderTargetPose;
            rightTargetPose = followerTargetPose;


        }
        else if (leaderName == "right")
        {
            rightTargetVel = leaderTargetVel;
            leftTargetVel = followerTargetVel;

            rightDMPTarget = leaderDMP->getTargetPose();
            leftDMPTarget = followerLocalTargetPoseVec;

            rightTargetPose = leaderTargetPose;
            leftTargetPose = followerTargetPose;

        }

        LockGuardType guard {controlDataMutex};
        getWriterControlStruct().leftTargetVel = leftTargetVel;
        getWriterControlStruct().rightTargetVel = rightTargetVel;
        getWriterControlStruct().leftTargetPose = leftTargetPose;
        getWriterControlStruct().rightTargetPose = rightTargetPose;
        getWriterControlStruct().virtualTime = virtualtimer;

        writeControlStruct();
    }

    Eigen::VectorXf NJointBimanualCCDMPController::getControlWrench(const Eigen::VectorXf& tcptwist, const Eigen::Matrix4f& currentPose, const Eigen::Matrix4f& targetPose)
    {
        //        Eigen::Vector3f currentTCPLinearVelocity;
        //        currentTCPLinearVelocity << 0.001 * tcptwist(0),   0.001 * tcptwist(1), 0.001 * tcptwist(2);
        //        Eigen::Vector3f currentTCPAngularVelocity;
        //        currentTCPAngularVelocity << tcptwist(3),   tcptwist(4),  tcptwist(5);

        //        Eigen::Vector3f currentTCPPosition = currentPose.block<3,1>(0,3);
        //        Eigen::Vector3f desiredPosition = targetPose.block<3,1>(0,3);
        //        Eigen::Vector3f tcpForces = 0.001 * kpos.cwiseProduct(desiredPosition - currentTCPPosition);
        //        Eigen::Vector3f tcpDesiredForce = tcpForces - dpos.cwiseProduct(currentTCPLinearVelocity);

        //        Eigen::Matrix3f currentRotMat = currentPose.block<3, 3>(0, 0);
        //        Eigen::Matrix3f diffMat = targetPose.block<3,3>(0,0) * currentRotMat.inverse();
        //        Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen3f2rpy(diffMat);

        //        Eigen::Vector3f tcpDesiredTorque = kori.cwiseProduct(rpy) - dori.cwiseProduct(currentTCPAngularVelocity);
        //        Eigen::Vector6f tcpDesiredWrench;
        //        tcpDesiredWrench <<   0.001 * tcpDesiredForce, tcpDesiredTorque;

        //        return tcpDesiredWrench;
        return Eigen::Vector6f::Zero();
    }



    void NJointBimanualCCDMPController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        double deltaT = timeSinceLastIteration.toSecondsDouble();



        interfaceData.getWriteBuffer().currentLeftPose = tcpLeft->getPoseInRootFrame();
        interfaceData.getWriteBuffer().currentRightPose = tcpRight->getPoseInRootFrame();
        interfaceData.commitWrite();

        controllerSensorData.getWriteBuffer().currentLeftPose = tcpLeft->getPoseInRootFrame();
        controllerSensorData.getWriteBuffer().currentRightPose = tcpRight->getPoseInRootFrame();
        controllerSensorData.getWriteBuffer().deltaT = deltaT;
        controllerSensorData.getWriteBuffer().currentTime += deltaT;

        // cartesian vel controller
        Eigen::MatrixXf I = Eigen::MatrixXf::Identity(leftTargets.size(), leftTargets.size());

        Eigen::MatrixXf jacobiL = leftIK->getJacobianMatrix(tcpLeft, VirtualRobot::IKSolver::CartesianSelection::All);

        Eigen::VectorXf leftqpos;
        Eigen::VectorXf leftqvel;
        leftqpos.resize(leftPositionSensors.size());
        leftqvel.resize(leftVelocitySensors.size());
        for (size_t i = 0; i < leftVelocitySensors.size(); ++i)
        {
            leftqpos(i) = leftPositionSensors[i]->position;
            leftqvel(i) = leftVelocitySensors[i]->velocity;
        }

        Eigen::MatrixXf jacobiR = rightIK->getJacobianMatrix(tcpRight, VirtualRobot::IKSolver::CartesianSelection::All);


        Eigen::VectorXf rightqpos;
        Eigen::VectorXf rightqvel;
        rightqpos.resize(rightPositionSensors.size());
        rightqvel.resize(rightVelocitySensors.size());

        for (size_t i = 0; i < rightVelocitySensors.size(); ++i)
        {
            rightqpos(i) = rightPositionSensors[i]->position;
            rightqvel(i) = rightVelocitySensors[i]->velocity;
        }

        Eigen::VectorXf currentLeftTwist = jacobiL * leftqvel;
        Eigen::VectorXf currentRightTwist = jacobiR * rightqvel;

        controllerSensorData.getWriteBuffer().currentLeftTwist = currentLeftTwist;
        controllerSensorData.getWriteBuffer().currentRightTwist = currentRightTwist;
        controllerSensorData.commitWrite();


        jacobiL.block<3, 8>(0, 0) = 0.001 * jacobiL.block<3, 8>(0, 0);
        jacobiR.block<3, 8>(0, 0) = 0.001 * jacobiR.block<3, 8>(0, 0);


        Eigen::Matrix4f leftTargetPose = rtGetControlStruct().leftTargetPose;
        Eigen::Matrix4f rightTargetPose = rtGetControlStruct().rightTargetPose;
        double virtualtime = rtGetControlStruct().virtualTime;
        Eigen::Matrix4f leftCurrentPose = tcpLeft->getPoseInRootFrame();
        Eigen::Matrix4f rightCurrentPose = tcpRight->getPoseInRootFrame();

        Eigen::VectorXf leftTargetVel = rtGetControlStruct().leftTargetVel;
        float normLinearVelocity = leftTargetVel.block<3, 1>(0, 0).norm();
        if (normLinearVelocity > maxLinearVel)
        {
            leftTargetVel.block<3, 1>(0, 0) = maxLinearVel * leftTargetVel.block<3, 1>(0, 0) / normLinearVelocity;
        }
        float normAngularVelocity = leftTargetVel.block<3, 1>(3, 0).norm();
        if (normAngularVelocity > maxAngularVel)
        {
            leftTargetVel.block<3, 1>(3, 0) = maxAngularVel * leftTargetVel.block<3, 1>(3, 0) / normAngularVelocity;
        }

        Eigen::VectorXf rightTargetVel = rtGetControlStruct().rightTargetVel;
        normLinearVelocity = rightTargetVel.block<3, 1>(0, 0).norm();
        if (normLinearVelocity > maxLinearVel)
        {
            rightTargetVel.block<3, 1>(0, 0) = maxLinearVel * rightTargetVel.block<3, 1>(0, 0) / normLinearVelocity;
        }
        normAngularVelocity = leftTargetVel.block<3, 1>(3, 0).norm();
        if (normAngularVelocity > maxAngularVel)
        {
            rightTargetVel.block<3, 1>(3, 0) = maxAngularVel * rightTargetVel.block<3, 1>(3, 0) / normAngularVelocity;
        }



        // unconstrained space controller
        Eigen::Vector6f leftJointControlWrench;
        {


            Eigen::Vector3f targetTCPLinearVelocity;
            targetTCPLinearVelocity << 0.001 * leftTargetVel(0), 0.001 * leftTargetVel(1), 0.001 * leftTargetVel(2);

            Eigen::Vector3f currentTCPLinearVelocity;
            currentTCPLinearVelocity <<  0.001 * currentLeftTwist(0),  0.001 * currentLeftTwist(1),   0.001 * currentLeftTwist(2);
            Eigen::Vector3f currentTCPAngularVelocity;
            currentTCPAngularVelocity << currentLeftTwist(3),   currentLeftTwist(4),  currentLeftTwist(5);
            Eigen::Vector3f currentTCPPosition = leftCurrentPose.block<3, 1>(0, 3);
            Eigen::Vector3f desiredPosition = leftTargetPose.block<3, 1>(0, 3);

            Eigen::Vector3f tcpDesiredForce = 0.001 * leftKpos.cwiseProduct(desiredPosition - currentTCPPosition) + leftDpos.cwiseProduct(targetTCPLinearVelocity - currentTCPLinearVelocity);
            Eigen::Matrix3f currentRotMat = leftCurrentPose.block<3, 3>(0, 0);
            Eigen::Matrix3f diffMat = leftTargetPose.block<3, 3>(0, 0) * currentRotMat.inverse();
            Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
            Eigen::Vector3f tcpDesiredTorque = leftKori.cwiseProduct(rpy) - leftDori.cwiseProduct(currentTCPAngularVelocity);
            leftJointControlWrench <<  tcpDesiredForce, tcpDesiredTorque;
        }

        Eigen::Vector6f rightJointControlWrench;
        {

            Eigen::Vector3f targetTCPLinearVelocity;
            targetTCPLinearVelocity << 0.001 * rightTargetVel(0), 0.001 * rightTargetVel(1), 0.001 * rightTargetVel(2);

            Eigen::Vector3f currentTCPLinearVelocity;
            currentTCPLinearVelocity <<   0.001 * currentRightTwist(0),   0.001 * currentRightTwist(1),  0.001 * currentRightTwist(2);
            Eigen::Vector3f currentTCPAngularVelocity;
            currentTCPAngularVelocity << currentRightTwist(3),   currentRightTwist(4),  currentRightTwist(5);
            Eigen::Vector3f currentTCPPosition = rightCurrentPose.block<3, 1>(0, 3);
            Eigen::Vector3f desiredPosition = rightTargetPose.block<3, 1>(0, 3);

            Eigen::Vector3f tcpDesiredForce = 0.001 * rightKpos.cwiseProduct(desiredPosition - currentTCPPosition) + rightDpos.cwiseProduct(targetTCPLinearVelocity - currentTCPLinearVelocity);
            Eigen::Matrix3f currentRotMat = rightCurrentPose.block<3, 3>(0, 0);
            Eigen::Matrix3f diffMat = rightTargetPose.block<3, 3>(0, 0) * currentRotMat.inverse();
            Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
            Eigen::Vector3f tcpDesiredTorque = rightKori.cwiseProduct(rpy) - rightDori.cwiseProduct(currentTCPAngularVelocity);
            rightJointControlWrench <<   tcpDesiredForce, tcpDesiredTorque;
        }



        //        Eigen::VectorXf leftJointLimitAvoidance(leftRNS->getSize());
        //        for (size_t i = 0; i < leftRNS->getSize(); i++)
        //        {
        //            VirtualRobot::RobotNodePtr rn = leftRNS->getNode(i);
        //            if (rn->isLimitless())
        //            {
        //                leftJointLimitAvoidance(i) = 0;
        //            }
        //            else
        //            {
        //                float f = math::MathUtils::ILerp(rn->getJointLimitLo(), rn->getJointLimitHi(), rn->getJointValue());
        //                leftJointLimitAvoidance(i) = cos(f * M_PI);
        //            }
        //        }
        //        Eigen::VectorXf leftNullspaceTorque = knull * leftJointLimitAvoidance - dnull * leftqvel;
        //        Eigen::VectorXf rightJointLimitAvoidance(rightRNS->getSize());
        //        for (size_t i = 0; i < rightRNS->getSize(); i++)
        //        {
        //            VirtualRobot::RobotNodePtr rn = rightRNS->getNode(i);
        //            if (rn->isLimitless())
        //            {
        //                rightJointLimitAvoidance(i) = 0;
        //            }
        //            else
        //            {
        //                float f = math::MathUtils::ILerp(rn->getJointLimitLo(), rn->getJointLimitHi(), rn->getJointValue());
        //                rightJointLimitAvoidance(i) = cos(f * M_PI);
        //            }
        //        }
        //        Eigen::VectorXf rightNullspaceTorque = knull * rightJointLimitAvoidance - dnull * rightqvel;


        Eigen::VectorXf leftNullspaceTorque = knull * (leftDesiredJointValues - leftqpos) - dnull * leftqvel;
        Eigen::VectorXf rightNullspaceTorque = knull * (rightDesiredJointValues - rightqpos) - dnull * rightqvel;

        float lambda = 2;

        Eigen::MatrixXf jtpinvL = leftIK->computePseudoInverseJacobianMatrix(jacobiL.transpose(), lambda);
        Eigen::MatrixXf jtpinvR = rightIK->computePseudoInverseJacobianMatrix(jacobiR.transpose(), lambda);
        Eigen::VectorXf leftJointDesiredTorques = jacobiL.transpose() * leftJointControlWrench + (I - jacobiL.transpose() * jtpinvL) * leftNullSpaceCoefs.cwiseProduct(leftNullspaceTorque);
        Eigen::VectorXf rightJointDesiredTorques = jacobiR.transpose() * rightJointControlWrench + (I - jacobiR.transpose() * jtpinvR) * rightNullSpaceCoefs.cwiseProduct(rightNullspaceTorque);



        // torque limit
        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            float desiredTorque =   leftJointDesiredTorques(i);

            if (isnan(desiredTorque))
            {
                desiredTorque = 0;
            }

            desiredTorque = (desiredTorque >  torqueLimit) ? torqueLimit : desiredTorque;
            desiredTorque = (desiredTorque < -torqueLimit) ? -torqueLimit : desiredTorque;

            debugDataInfo.getWriteBuffer().desired_torques[leftJointNames[i]] = desiredTorque;

            leftTargets.at(i)->torque = desiredTorque;
        }



        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            float desiredTorque = rightJointDesiredTorques(i);

            if (isnan(desiredTorque))
            {
                desiredTorque = 0;
            }

            desiredTorque = (desiredTorque >  torqueLimit) ? torqueLimit : desiredTorque;
            desiredTorque = (desiredTorque < -torqueLimit) ? -torqueLimit : desiredTorque;

            debugDataInfo.getWriteBuffer().desired_torques[rightJointNames[i]] = desiredTorque;

            rightTargets.at(i)->torque = desiredTorque;
        }
        debugDataInfo.getWriteBuffer().leftControlSignal_x = leftJointControlWrench(0);

        debugDataInfo.getWriteBuffer().leftControlSignal_x = leftJointControlWrench(0);
        debugDataInfo.getWriteBuffer().leftControlSignal_y = leftJointControlWrench(1);
        debugDataInfo.getWriteBuffer().leftControlSignal_z = leftJointControlWrench(2);
        debugDataInfo.getWriteBuffer().leftControlSignal_ro = leftJointControlWrench(3);
        debugDataInfo.getWriteBuffer().leftControlSignal_pi = leftJointControlWrench(4);
        debugDataInfo.getWriteBuffer().leftControlSignal_ya = leftJointControlWrench(5);

        debugDataInfo.getWriteBuffer().rightControlSignal_x = rightJointControlWrench(0);
        debugDataInfo.getWriteBuffer().rightControlSignal_y = rightJointControlWrench(1);
        debugDataInfo.getWriteBuffer().rightControlSignal_z = rightJointControlWrench(2);
        debugDataInfo.getWriteBuffer().rightControlSignal_ro = rightJointControlWrench(3);
        debugDataInfo.getWriteBuffer().rightControlSignal_pi = rightJointControlWrench(4);
        debugDataInfo.getWriteBuffer().rightControlSignal_ya = rightJointControlWrench(5);
        //        debugDataInfo.getWriteBuffer().leftTcpDesiredTorque_x = tcpDesiredTorque(0);
        //        debugDataInfo.getWriteBuffer().leftTcpDesiredTorque_y = tcpDesiredTorque(1);
        //        debugDataInfo.getWriteBuffer().tcpDesiredTorque_z = tcpDesiredTorque(2);
        //        debugDataInfo.getWriteBuffer().quatError = errorAngle;
        //        debugDataInfo.getWriteBuffer().posiError = posiError;
        debugDataInfo.getWriteBuffer().leftTargetPose_x = leftTargetPose(0, 3);
        debugDataInfo.getWriteBuffer().leftTargetPose_y = leftTargetPose(1, 3);
        debugDataInfo.getWriteBuffer().leftTargetPose_z = leftTargetPose(2, 3);
        debugDataInfo.getWriteBuffer().leftCurrentPose_x = leftCurrentPose(0, 3);
        debugDataInfo.getWriteBuffer().leftCurrentPose_y = leftCurrentPose(1, 3);
        debugDataInfo.getWriteBuffer().leftCurrentPose_z = leftCurrentPose(2, 3);


        debugDataInfo.getWriteBuffer().rightTargetPose_x = rightTargetPose(0, 3);
        debugDataInfo.getWriteBuffer().rightTargetPose_y = rightTargetPose(1, 3);
        debugDataInfo.getWriteBuffer().rightTargetPose_z = rightTargetPose(2, 3);

        debugDataInfo.getWriteBuffer().rightCurrentPose_x = rightCurrentPose(0, 3);
        debugDataInfo.getWriteBuffer().rightCurrentPose_y = rightCurrentPose(1, 3);
        debugDataInfo.getWriteBuffer().rightCurrentPose_z = rightCurrentPose(2, 3);
        debugDataInfo.getWriteBuffer().virtualTime = virtualtime;

        debugDataInfo.commitWrite();




        //        Eigen::VectorXf leftNullSpaceJointVelocity = cfg->knull * (leftDesiredJointValues - leftqpos);
        //        Eigen::MatrixXf jtpinvL = leftIK->computePseudoInverseJacobianMatrix(jacobiL, leftIK->getJacobiRegularization(VirtualRobot::IKSolver::CartesianSelection::All));
        //        Eigen::VectorXf jnvL = jtpinvL * leftTargetVel + (I - jtpinvL * jacobiL) * leftNullSpaceJointVelocity;

        //        for (size_t i = 0; i < leftTargets.size(); ++i)
        //        {
        //            leftTargets.at(i)->velocity = jnvL(i);
        //        }
        //        Eigen::VectorXf rightNullSpaceJointVelocity = cfg->knull * (rightDesiredJointValues - rightqpos);
        //        Eigen::MatrixXf jtpinvR = rightIK->computePseudoInverseJacobianMatrix(jacobiR, rightIK->getJacobiRegularization(VirtualRobot::IKSolver::CartesianSelection::All));
        //        Eigen::VectorXf jnvR = jtpinvR * rightTargetVel + (I - jtpinvR * jacobiR) * rightNullSpaceJointVelocity;

        //        for (size_t i = 0; i < rightTargets.size(); ++i)
        //        {
        //            rightTargets.at(i)->velocity = jnvR(i);
        //        }
    }

    void NJointBimanualCCDMPController::learnDMPFromFiles(const std::string& name, const Ice::StringSeq& fileNames, const Ice::Current&)
    {
        if (name == "LeftLeader")
        {
            leftGroup.at(0)->learnDMPFromFiles(fileNames);
        }
        else if (name == "LeftFollower")
        {
            rightGroup.at(1)->learnDMPFromFiles(fileNames);
        }
        else if (name == "RightLeader")
        {
            rightGroup.at(0)->learnDMPFromFiles(fileNames);
        }
        else
        {
            leftGroup.at(1)->learnDMPFromFiles(fileNames);
        }
    }


    void NJointBimanualCCDMPController::setViaPoints(Ice::Double u, const Ice::DoubleSeq& viapoint, const Ice::Current&)
    {
        LockGuardType guard(controllerMutex);
        if (leaderName == "Left")
        {
            leftGroup.at(0)->setViaPose(u, viapoint);
        }
        else
        {
            rightGroup.at(0)->setViaPose(u, viapoint);
        }
    }

    void NJointBimanualCCDMPController::setGoals(const Ice::DoubleSeq& goals, const Ice::Current& ice)
    {
        LockGuardType guard(controllerMutex);
        if (leaderName == "Left")
        {
            leftGroup.at(0)->setGoalPoseVec(goals);
        }
        else
        {
            rightGroup.at(0)->setGoalPoseVec(goals);
        }

    }

    void NJointBimanualCCDMPController::changeLeader(const Ice::Current&)
    {
        LockGuardType guard(controllerMutex);

        if (leaderName == "Left")
        {
            leaderName = "Right";
            rightGroup.at(0)->canVal = virtualtimer;
            rightGroup.at(1)->canVal = virtualtimer;
        }
        else
        {
            leaderName = "Left";
            leftGroup.at(0)->canVal = virtualtimer;
            leftGroup.at(1)->canVal = virtualtimer;
        }

    }





    void NJointBimanualCCDMPController::runDMP(const Ice::DoubleSeq& leftGoals, const Ice::DoubleSeq& rightGoals, const Ice::Current&)
    {

        while (!interfaceData.updateReadBuffer())
        {
            usleep(100);
        }
        Eigen::Matrix4f leftPose = interfaceData.getReadBuffer().currentLeftPose;
        Eigen::Matrix4f rightPose = interfaceData.getReadBuffer().currentRightPose;

        ARMARX_IMPORTANT << VAROUT(leftPose);
        ARMARX_IMPORTANT << VAROUT(rightPose);

        virtualtimer = cfg->timeDuration;

        leftGroup.at(0)->prepareExecution(leftGroup.at(0)->eigen4f2vec(leftPose), leftGoals);
        rightGroup.at(0)->prepareExecution(rightGroup.at(0)->eigen4f2vec(rightPose), rightGoals);


        ARMARX_INFO << "leftgroup goal local pose: " << getLocalPose(leftGoals, rightGoals);

        leftGroup.at(1)->prepareExecution(getLocalPose(leftPose, rightPose), getLocalPose(leftGoals, rightGoals));
        rightGroup.at(1)->prepareExecution(getLocalPose(rightPose, leftPose), getLocalPose(rightGoals, leftGoals));

        finished = false;
        controllerTask->start();
    }

    Eigen::Matrix4f NJointBimanualCCDMPController::getLocalPose(const Eigen::Matrix4f& newCoordinate, const Eigen::Matrix4f& globalTargetPose)
    {
        Eigen::Matrix4f localPose = Eigen::Matrix4f::Identity();

        localPose.block<3, 3>(0, 0) = newCoordinate.block<3, 3>(0, 0).inverse() * globalTargetPose.block<3, 3>(0, 0);
        localPose.block<3, 1>(0, 3) = newCoordinate.block<3, 3>(0, 0).inverse() * (globalTargetPose.block<3, 1>(0, 3) - newCoordinate.block<3, 1>(0, 3));


        return localPose;
    }



    void NJointBimanualCCDMPController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& debugObs)
    {

        StringVariantBaseMap datafields;
        auto values = debugDataInfo.getUpToDateReadBuffer().desired_torques;
        for (auto& pair : values)
        {
            datafields[pair.first] = new Variant(pair.second);
        }

        auto constrained_force = debugDataInfo.getUpToDateReadBuffer().constrained_force;
        for (auto& pair : constrained_force)
        {
            datafields[pair.first] = new Variant(pair.second);
        }


        datafields["leftTargetPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftTargetPose_x);
        datafields["leftTargetPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftTargetPose_y);
        datafields["leftTargetPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftTargetPose_z);
        datafields["rightTargetPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightTargetPose_x);
        datafields["rightTargetPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightTargetPose_y);
        datafields["rightTargetPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightTargetPose_z);


        datafields["leftCurrentPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftCurrentPose_x);
        datafields["leftCurrentPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftCurrentPose_y);
        datafields["leftCurrentPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftCurrentPose_z);
        datafields["rightCurrentPose_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightCurrentPose_x);
        datafields["rightCurrentPose_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightCurrentPose_y);
        datafields["rightCurrentPose_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightCurrentPose_z);

        datafields["leftControlSignal_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_x);
        datafields["leftControlSignal_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_y);
        datafields["leftControlSignal_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_z);
        datafields["leftControlSignal_ro"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_ro);
        datafields["leftControlSignal_pi"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_pi);
        datafields["leftControlSignal_ya"] = new Variant(debugDataInfo.getUpToDateReadBuffer().leftControlSignal_ya);


        datafields["rightControlSignal_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_x);
        datafields["rightControlSignal_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_y);
        datafields["rightControlSignal_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_z);
        datafields["rightControlSignal_ro"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_ro);
        datafields["rightControlSignal_pi"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_pi);
        datafields["rightControlSignal_ya"] = new Variant(debugDataInfo.getUpToDateReadBuffer().rightControlSignal_ya);

        datafields["virtual_timer"] = new Variant(debugDataInfo.getUpToDateReadBuffer().virtualTime);


        debugObs->setDebugChannel("DMPController", datafields);
    }

    void NJointBimanualCCDMPController::onInitNJointController()
    {
        ARMARX_INFO << "init ...";
        controllerTask = new PeriodicTask<NJointBimanualCCDMPController>(this, &NJointBimanualCCDMPController::controllerRun, 0.3);
    }

    void NJointBimanualCCDMPController::onDisconnectNJointController()
    {
        controllerTask->stop();
        ARMARX_INFO << "stopped ...";
    }



}
