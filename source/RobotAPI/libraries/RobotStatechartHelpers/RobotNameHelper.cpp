/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotNameHelper.h"

#include <ArmarXCore/core/util/StringHelpers.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include <Eigen/Dense>

namespace armarx
{
    void RobotNameHelper::writeRobotInfoNode(
        const RobotInfoNodePtr& n,
        std::ostream& str,
        std::size_t indent,
        char indentChar)
    {
        const std::string ind(4 * indent, indentChar);
        if (!n)
        {
            str << ind << "nullptr\n";
            return;
        }
        str << ind << n->name << ", profile = " << n->profile << ", value " << n->value << '\n';
        for (const auto c : n->children)
        {
            writeRobotInfoNode(c, str, indent + 1);
        }
    }
    const RobotInfoNodePtr& RobotNameHelper::getRobotInfoNodePtr() const
    {
        return robotInfo;
    }

    const std::string RobotNameHelper::LocationLeft = "Left";
    const std::string RobotNameHelper::LocationRight = "Right";

    RobotNameHelper::RobotNameHelper(const RobotInfoNodePtr& robotInfo, const StatechartProfilePtr& profile)
        : root(Node(robotInfo)), robotInfo(robotInfo)
    {
        ARMARX_TRACE;
        StatechartProfilePtr p = profile;
        while (p && !p->isRoot())
        {
            profiles.emplace_back(p->getName());
            p = p->getParent();
        }
        profiles.emplace_back(""); // for matching the default/root

    }

    std::string RobotNameHelper::select(const std::string& path) const
    {
        ARMARX_TRACE;
        Node node = root;
        for (const std::string& p : Split(path, "/"))
        {
            node = node.select(p, profiles);
        }
        if (!node.isValid())
        {
            std::stringstream s;
            s << "RobotNameHelper::select: path " + path + " not found\nrobotInfo:\n";
            writeRobotInfoNode(robotInfo, s);
            throw std::runtime_error(s.str());
        }
        return node.value();
    }

    RobotNameHelperPtr RobotNameHelper::Create(const RobotInfoNodePtr& robotInfo, const StatechartProfilePtr& profile)
    {
        return RobotNameHelperPtr(new RobotNameHelper(robotInfo, profile));
    }

    RobotNameHelper::Arm RobotNameHelper::getArm(const std::string& side) const
    {
        return Arm(shared_from_this(), side);
    }

    RobotNameHelper::RobotArm RobotNameHelper::getRobotArm(const std::string& side, const VirtualRobot::RobotPtr& robot) const
    {
        return RobotArm(Arm(shared_from_this(), side), robot);
    }

    std::shared_ptr<const RobotNameHelper> RobotNameHelper::Arm::getRobotNameHelper() const
    {
        return rnh;
    }

    RobotNameHelper::Node::Node(const RobotInfoNodePtr& robotInfo)
        : robotInfo(robotInfo)
    {

    }

    std::string RobotNameHelper::Node::value()
    {
        checkValid();
        return robotInfo->value;
    }

    RobotNameHelper::Node RobotNameHelper::Node::select(const std::string& name, const std::vector<std::string>& profiles)
    {
        ARMARX_TRACE;
        if (!isValid())
        {
            return Node(nullptr);
        }
        std::map<std::string, RobotInfoNodePtr> matches;
        for (const RobotInfoNodePtr& child : robotInfo->children)
        {
            if (child->name == name)
            {
                matches[child->profile] = child;
            }
        }
        for (const std::string& p : profiles)
        {
            if (matches.count(p))
            {
                return matches.at(p);
            }
        }
        return Node(nullptr);
    }

    bool RobotNameHelper::Node::isValid()
    {
        return robotInfo ? true : false;
    }

    void RobotNameHelper::Node::checkValid()
    {
        if (!isValid())
        {

            std::stringstream s;
            s << "RobotNameHelper::Node nullptr exception\nrobotInfo:\n";
            writeRobotInfoNode(robotInfo, s);
            throw std::runtime_error(s.str());
        }
    }


    std::string RobotNameHelper::Arm::getSide() const
    {
        return side;
    }

    std::string RobotNameHelper::Arm::getKinematicChain() const
    {
        ARMARX_TRACE;
        return select("KinematicChain");
    }

    std::string RobotNameHelper::Arm::getTorsoKinematicChain() const
    {
        ARMARX_TRACE;
        return select("TorsoKinematicChain");
    }

    std::string RobotNameHelper::Arm::getTCP() const
    {
        ARMARX_TRACE;
        return select("TCP");
    }

    std::string RobotNameHelper::Arm::getForceTorqueSensor() const
    {
        ARMARX_TRACE;
        return select("ForceTorqueSensor");
    }

    std::string RobotNameHelper::Arm::getEndEffector() const
    {
        ARMARX_TRACE;
        return select("EndEffector");
    }

    std::string RobotNameHelper::Arm::getMemoryHandName() const
    {
        ARMARX_TRACE;
        return select("MemoryHandName");
    }

    std::string RobotNameHelper::Arm::getHandControllerName() const
    {
        ARMARX_TRACE;
        return select("HandControllerName");
    }

    std::string RobotNameHelper::Arm::getHandRootNode() const
    {
        ARMARX_TRACE;
        return select("HandRootNode");
    }

    std::string RobotNameHelper::Arm::getHandModelPath() const
    {
        ARMARX_TRACE;
        return select("HandModelPath");
    }

    std::string RobotNameHelper::Arm::getAbsoluteHandModelPath() const
    {
        ArmarXDataPath::FindPackageAndAddDataPath(getHandModelPackage());
        auto path = getHandModelPath();
        return ArmarXDataPath::getAbsolutePath(path, path) ?
               path : "";
    }

    std::string RobotNameHelper::Arm::getHandModelPackage() const
    {
        ARMARX_TRACE;
        return select("HandModelPackage");
    }

    std::string RobotNameHelper::Arm::getPalmCollisionModel() const
    {
        ARMARX_TRACE;
        return select("PalmCollisionModel");
    }

    RobotNameHelper::RobotArm RobotNameHelper::Arm::addRobot(const VirtualRobot::RobotPtr& robot) const
    {
        ARMARX_TRACE;
        return RobotArm(*this, robot);
    }

    RobotNameHelper::Arm::Arm(const std::shared_ptr<const RobotNameHelper>& rnh, const std::string& side)
        : rnh(rnh), side(side)
    {

    }

    std::string RobotNameHelper::Arm::select(const std::string& path) const
    {
        ARMARX_TRACE;
        return rnh->select(side + "Arm/" + path);
    }

    std::string RobotNameHelper::RobotArm::getSide() const
    {
        ARMARX_TRACE;
        return arm.getSide();
    }

    VirtualRobot::RobotNodeSetPtr RobotNameHelper::RobotArm::getKinematicChain() const
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(robot);
        return robot->getRobotNodeSet(arm.getKinematicChain());
    }

    VirtualRobot::RobotNodeSetPtr RobotNameHelper::RobotArm::getTorsoKinematicChain() const
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(robot);
        return robot->getRobotNodeSet(arm.getTorsoKinematicChain());
    }

    VirtualRobot::RobotNodePtr RobotNameHelper::RobotArm::getTCP() const
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(robot);
        return robot->getRobotNode(arm.getTCP());
    }

    VirtualRobot::RobotNodePtr RobotNameHelper::RobotArm::getPalmCollisionModel() const
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(robot);
        return robot->getRobotNode(arm.getPalmCollisionModel());
    }

    VirtualRobot::RobotNodePtr RobotNameHelper::RobotArm::getHandRootNode() const
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(robot);
        return robot->getRobotNode(arm.getHandRootNode());
    }

    Eigen::Matrix4f RobotNameHelper::RobotArm::getTcp2HandRootTransform() const
    {
        ARMARX_TRACE;
        const auto tcp = getTCP();
        ARMARX_CHECK_NOT_NULL(tcp);
        const auto hand = getHandRootNode();
        ARMARX_CHECK_NOT_NULL(hand);
        return tcp->getPoseInRootFrame().inverse() * hand->getPoseInRootFrame();
    }

    const VirtualRobot::RobotPtr& RobotNameHelper::RobotArm::getRobot() const
    {
        return robot;
    }

    const RobotNameHelper::Arm& RobotNameHelper::RobotArm::getArm() const
    {
        return arm;
    }

    RobotNameHelper::RobotArm::RobotArm(const Arm& arm, const VirtualRobot::RobotPtr& robot)
        : arm(arm), robot(robot)
    {
        ARMARX_CHECK_NOT_NULL(robot);
    }

    const std::vector<std::string>& RobotNameHelper::getProfiles() const
    {
        return profiles;
    }
}
