/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>

namespace armarx
{
    class KinematicUnitHelper;
    using KinematicUnitHelperPtr = std::shared_ptr<KinematicUnitHelper>;

    class KinematicUnitHelper
    {
    public:
        KinematicUnitHelper(const KinematicUnitInterfacePrx& kinUnit);

        void setJointAngles(const NameValueMap& jointAngles);
        void setJointVelocities(const NameValueMap& jointVelocities);
        void setJointTorques(const NameValueMap& jointTorques);
        static NameControlModeMap MakeControlModes(const NameValueMap& jointValues, ControlMode controlMode);
        static std::string ControlModeToString(ControlMode controlMode)
        {
            std::string state;
            switch (controlMode)
            {
                case eDisabled:
                    state = "Disabled";
                    break;

                case eUnknown:
                    state = "Unknown";
                    break;

                case ePositionControl:
                    state = "Position";
                    break;

                case eVelocityControl:
                    state = "Velocity";
                    break;

                case eTorqueControl:
                    state = "Torque";
                    break;


                case ePositionVelocityControl:
                    state = "Position + Velocity";
                    break;

                default:
                    //show the value of the mode so it can be implemented
                    state = std::string("nyi Mode: " + std::to_string(controlMode));
                    break;
            }
            return state;
        }
    private:
        KinematicUnitInterfacePrx kinUnit;
    };
}
