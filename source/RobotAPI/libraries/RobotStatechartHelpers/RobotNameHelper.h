/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/core/RobotState.h>

#include <VirtualRobot/Robot.h>

namespace armarx
{
    using RobotNameHelperPtr = std::shared_ptr<class RobotNameHelper>;
    using StatechartProfilePtr = std::shared_ptr<class StatechartProfile>;

    class RobotNameHelper : public std::enable_shared_from_this<RobotNameHelper>
    {
    public:
        static void writeRobotInfoNode(
            const RobotInfoNodePtr& n,
            std::ostream& str,
            std::size_t indent = 0,
            char indentChar = ' ');

        class Node
        {
        public:
            Node(const RobotInfoNodePtr& robotInfo);
            std::string value();

            Node select(const std::string& name, const std::vector<std::string>& profiles);
            bool isValid();
            void checkValid();

        private:
            RobotInfoNodePtr robotInfo;
        };

        static const std::string LocationLeft;
        static const std::string LocationRight;

        struct RobotArm;
        struct Arm
        {
            friend class RobotNameHelper;
        public:

            std::string getSide() const;
            std::string getKinematicChain() const;
            std::string getTorsoKinematicChain() const;
            std::string getTCP() const;
            std::string getForceTorqueSensor() const;
            std::string getEndEffector() const;
            std::string getMemoryHandName() const;
            std::string getHandControllerName() const;
            std::string getHandRootNode() const;
            std::string getHandModelPath() const;
            std::string getAbsoluteHandModelPath() const;
            std::string getHandModelPackage() const;
            std::string getPalmCollisionModel() const;
            RobotArm addRobot(const VirtualRobot::RobotPtr& robot) const;

            Arm(const std::shared_ptr<const RobotNameHelper>& rnh, const std::string& side);
            Arm() = default;
            Arm(Arm&&) = default;
            Arm(const Arm&) = default;
            Arm& operator=(Arm&&) = default;
            Arm& operator=(const Arm&) = default;

            std::shared_ptr<const RobotNameHelper> getRobotNameHelper() const;

            std::string select(const std::string& path) const;
        private:

            std::shared_ptr<const RobotNameHelper> rnh;
            std::string side;
        };

        struct RobotArm
        {
            friend class RobotNameHelper;
            friend class Arm;
        public:
            std::string getSide() const;
            VirtualRobot::RobotNodeSetPtr getKinematicChain() const;
            VirtualRobot::RobotNodeSetPtr getTorsoKinematicChain() const;
            VirtualRobot::RobotNodePtr getTCP() const;
            VirtualRobot::RobotNodePtr getHandRootNode() const;
            VirtualRobot::RobotNodePtr getPalmCollisionModel() const;
            Eigen::Matrix4f getTcp2HandRootTransform() const;
            const Arm& getArm() const;
            const VirtualRobot::RobotPtr& getRobot() const;

            RobotArm(const Arm& arm, const VirtualRobot::RobotPtr& robot);
            RobotArm() = default;
            RobotArm(RobotArm&&) = default;
            RobotArm(const RobotArm&) = default;
            RobotArm& operator=(RobotArm&&) = default;
            RobotArm& operator=(const RobotArm&) = default;
        private:

            Arm arm;
            VirtualRobot::RobotPtr robot;
        };

        std::string select(const std::string& path) const;

        static RobotNameHelperPtr Create(const RobotInfoNodePtr& robotInfo, const StatechartProfilePtr& profile);

        RobotNameHelper(const RobotInfoNodePtr& robotInfo, const StatechartProfilePtr& profile = nullptr);
        RobotNameHelper(RobotNameHelper&&) = default;
        RobotNameHelper(const RobotNameHelper&) = default;
        RobotNameHelper& operator=(RobotNameHelper&&) = default;
        RobotNameHelper& operator=(const RobotNameHelper&) = default;

        Arm getArm(const std::string& side) const;
        RobotArm getRobotArm(const std::string& side, const VirtualRobot::RobotPtr& robot) const;
        const std::vector<std::string>& getProfiles() const;
        const RobotInfoNodePtr& getRobotInfoNodePtr() const;
    private:
        RobotNameHelper& self()
        {
            return *this;
        }

        Node root;
        std::vector<std::string> profiles;
        RobotInfoNodePtr robotInfo;
    };
}
