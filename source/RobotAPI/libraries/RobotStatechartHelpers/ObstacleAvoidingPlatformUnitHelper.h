/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObstacleAvoidingPlatformUnit
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// Eigen
#include <Eigen/Core>

// Simox
#include <VirtualRobot/Nodes/RobotNode.h>

// RobotAPI
#include <RobotAPI/components/units/ObstacleAvoidingPlatformUnit/ObstacleAvoidingPlatformUnit.h>


namespace armarx
{

    class ObstacleAvoidingPlatformUnitHelper
    {

    public:

        ObstacleAvoidingPlatformUnitHelper(
            armarx::PlatformUnitInterfacePrx platform_unit,
            VirtualRobot::RobotPtr robot,
            float pos_reached_threshold,
            float ori_reached_threshold);

        virtual
        ~ObstacleAvoidingPlatformUnitHelper();

        void
        setTarget(const Eigen::Vector2f& target_pos, float target_ori);

        void
        update();

        bool
        isFinalTargetReached()
        const;

    private:

        struct target
        {
            Eigen::Vector2f pos;
            float ori;
        };

        armarx::PlatformUnitInterfacePrx m_platform_unit;

        VirtualRobot::RobotPtr m_robot;

        target m_target;

        float m_pos_reached_threshold;
        float m_ori_reached_threshold;

    };

}
