#pragma once

#include <QWidget>
#include <QSettings>

#include <VirtualRobot/RobotNodeSet.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/SpinBoxToVector.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/SpinBoxToPose.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>

#include <RobotAPI/libraries/RobotAPINJointControllerWidgets/ui_CartesianImpedanceControllerConfigWidget.h>

namespace armarx
{
    class CartesianImpedanceControllerConfigWidget : public QWidget
    {
    public:
        CartesianImpedanceControllerConfigWidget(QWidget* parent = nullptr);

        void loadSettings(QSettings* settings, const QString& prefix = "");

        void saveSettings(QSettings* settings, const QString& prefix = "");

        void on_pushButtonNullspaceUpdateJoints_clicked();
        void on_pushButtonNullspaceSend_clicked();
        void on_pushButtonSettingsSend_clicked();

        NJointTaskSpaceImpedanceControlConfigPtr
        readFullCFG(const Eigen::Vector3f& targPos, const Eigen::Quaternionf& targOri) const;
        NJointTaskSpaceImpedanceControlRuntimeConfig
        readRuntimeCFG() const;
        std::tuple<Eigen::VectorXf, Eigen::VectorXf, Eigen::VectorXf>
        readNullspaceCFG() const;

        ///if null -> send buttons deactivated
        void setController(const NJointTaskSpaceImpedanceControlInterfacePrx& prx);
        void setRNS(const VirtualRobot::RobotNodeSetPtr& rns);

        Ui::CartesianImpedanceControllerConfigWidget ui;
        SpinBoxToVector<QDoubleSpinBox, 3>      kxyz;
        SpinBoxToVector<QDoubleSpinBox, 3>      krpy;
        SpinBoxToVector<QDoubleSpinBox, 3>      dxyz;
        SpinBoxToVector<QDoubleSpinBox, 3>      drpy;
        SpinBoxToVector<QDoubleSpinBox>         jointValues;
        SpinBoxToVector<QDoubleSpinBox>         jointKnull;
        SpinBoxToVector<QDoubleSpinBox>         jointDnull;

    private:
        NJointTaskSpaceImpedanceControlInterfacePrx _controller;
        VirtualRobot::RobotNodeSetPtr               _rns;
    };
}
