/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RobotAPIObjectFactories.h"

#include "Trajectory.h"
#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/LinkedPose.h>
#include <RobotAPI/libraries/core/observerfilters/PoseMedianFilter.h>
#include <RobotAPI/libraries/core/observerfilters/OffsetFilter.h>
#include <RobotAPI/libraries/core/observerfilters/MatrixFilters.h>
#include <RobotAPI/libraries/core/observerfilters/PoseMedianOffsetFilter.h>
#include <RobotAPI/libraries/core/observerfilters/PoseAverageFilter.h>
#include <RobotAPI/libraries/core/observerfilters/MedianDerivativeFilterV3.h>
#include <RobotAPI/libraries/core/OrientedPoint.h>
#include <RobotAPI/libraries/core/FramedOrientedPoint.h>
#include <RobotAPI/libraries/core/CartesianPositionController.h>


using namespace armarx;
using namespace armarx::ObjectFactories;

ObjectFactoryMap RobotAPIObjectFactories::getFactories()
{
    ObjectFactoryMap map;

    add<Vector2>(map);
    add<Vector3>(map);
    add<FramedDirection>(map);
    add<LinkedDirection>(map);
    add<Quaternion>(map);
    add<Pose>(map);
    add<FramedPose>(map);
    add<FramedOrientation>(map);
    add<FramedPosition>(map);
    add<LinkedPose>(map);

    add<armarx::OrientedPointBase, armarx::OrientedPoint>(map);
    add<armarx::FramedOrientedPointBase, armarx::FramedOrientedPoint>(map);
    add<armarx::CartesianPositionControllerConfigBase, armarx::CartesianPositionControllerConfig>(map);

    add<armarx::PoseMedianFilterBase, armarx::filters::PoseMedianFilter>(map);
    add<armarx::PoseAverageFilterBase, armarx::filters::PoseAverageFilter>(map);
    add<armarx::PoseMedianOffsetFilterBase, armarx::filters::PoseMedianOffsetFilter>(map);
    add<armarx::MedianDerivativeFilterV3Base, armarx::filters::MedianDerivativeFilterV3>(map);
    add<armarx::OffsetFilterBase, armarx::filters::OffsetFilter>(map);
    add<armarx::MatrixMaxFilterBase, armarx::filters::MatrixMaxFilter>(map);
    add<armarx::MatrixMinFilterBase, armarx::filters::MatrixMinFilter>(map);
    add<armarx::MatrixAvgFilterBase, armarx::filters::MatrixAvgFilter>(map);
    add<armarx::MatrixPercentileFilterBase, armarx::filters::MatrixPercentileFilter>(map);
    add<armarx::MatrixPercentilesFilterBase, armarx::filters::MatrixPercentilesFilter>(map);
    add<armarx::MatrixCumulativeFrequencyFilterBase, armarx::filters::MatrixCumulativeFrequencyFilter>(map);
    add<armarx::TrajectoryBase, armarx::Trajectory>(map);

    return map;
}
