/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Sonja Marahrens (sonja dot marahrens)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "JointVelocityRamp.h"
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{
    JointVelocityRamp::JointVelocityRamp()
    { }

    void JointVelocityRamp::setState(const Eigen::VectorXf& state)
    {
        this->state = state;
    }

    Eigen::VectorXf JointVelocityRamp::update(const Eigen::VectorXf& target, float dt)
    {
        if (dt <= 0)
        {
            return state;
        }

        Eigen::VectorXf delta = target - state;

        float factor = delta.norm() / maxAcceleration / dt;
        factor = std::max(factor, 1.f);

        state += delta / factor;
        //    ARMARX_IMPORTANT << "JointRamp state " << state;
        return state;

    }

    void JointVelocityRamp::setMaxAccelaration(float maxAcceleration)
    {
        this->maxAcceleration = maxAcceleration;

    }

    float JointVelocityRamp::getMaxAccelaration()
    {
        return maxAcceleration;
    }
}
