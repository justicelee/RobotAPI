/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "RobotPool.h"

#include <VirtualRobot/CollisionDetection/CollisionChecker.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

namespace armarx
{

    RobotPool::RobotPool(VirtualRobot::RobotPtr robot, size_t defaultSize) :
        baseRobot(robot)
    {
        std::vector<VirtualRobot::RobotPtr> tempVec;
        for (size_t i = 0; i < defaultSize; ++i)
        {
            tempVec.push_back(getRobot());
        }
    }

    VirtualRobot::RobotPtr RobotPool::getRobot(int inflation)
    {
        std::scoped_lock lock(mutex);
        for (auto& r : robots[inflation])
        {
            if (r.use_count() == 1)
            {
                return r;
            }
        }
        auto newRobot = baseRobot->clone(baseRobot->getName(), VirtualRobot::CollisionCheckerPtr(new VirtualRobot::CollisionChecker()));
        newRobot->inflateCollisionModel(inflation);
        ARMARX_CHECK_EQUAL(newRobot.use_count(), 1);
        ARMARX_DEBUG << "created new robot clone n with inflation " << inflation;
        robots[inflation].push_back(newRobot);
        return newRobot;
    }

    size_t RobotPool::getPoolSize() const
    {
        std::scoped_lock lock(mutex);
        size_t size = 0;
        for (auto& pair : robots)
        {
            size += pair.second.size();
        }
        return size;
    }

    size_t RobotPool::getRobotsInUseCount() const
    {
        std::scoped_lock lock(mutex);
        size_t count = 0;
        for (auto& pair : robots)
        {
            for (auto& r : pair.second)
            {
                if (r.use_count() > 1)
                {
                    count++;
                }
            }
        }
        return count;
    }

    size_t RobotPool::clean()
    {
        std::scoped_lock lock(mutex);
        size_t count = 0;
        for (auto& pair : robots)
        {
            std::vector<VirtualRobot::RobotPtr> newList;
            for (auto& r : pair.second)
            {
                if (r.use_count() > 1)
                {
                    newList.push_back(r);
                }
                else
                {
                    count++;
                }
            }
            pair.second = newList;
        }
        return count;
    }

} // namespace armarx
