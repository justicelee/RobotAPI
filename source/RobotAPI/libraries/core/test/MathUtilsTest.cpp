/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE RobotAPI::PIDController::Test
#define ARMARX_BOOST_TEST
#include <RobotAPI/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include "../math/MathUtils.h"
#include "../math/ColorUtils.h"
#include "../FramedPose.h"
using namespace armarx;

void check_close(float a, float b, float tolerance)
{
    if (fabs(a - b) > tolerance)
    {
        ARMARX_ERROR << "fabs(a-b) > tolerance. a=" << a << " b=" << b << " tolerance=" << tolerance;
        BOOST_FAIL("");
    }
}

BOOST_AUTO_TEST_CASE(eigenOutputTest)
{
    Eigen::Vector3f v;
    v << 10, 1000, -10;
    std::cout << "v:\n" << v << std::endl;

    armarx::Vector3 v2(v);
    std::cout << "v2:\n" << v2.output() << std::endl;


    FramedPose p(Eigen::Matrix4f::Identity(), "", "");
    p.position->x = 100;
    ARMARX_INFO << "pose:\n" << p.output();
}


BOOST_AUTO_TEST_CASE(fmodTest)
{
    float min = -22.0f / 180.f * M_PI;
    float max = 200.0f / 180.f * M_PI;
    std::vector<float> values = { 190.0f, 185, min, max, 0};
    for (auto value : values)
    {
        value /= 180.f / M_PI;
        std::vector<float> offsets = { -2 * M_PI, 0, 2 * M_PI};

        for (auto offset : offsets)
        {
            float center = (max + min) / 2;

            float result = armarx::math::MathUtils::angleModX(value + offset, center);
            ARMARX_INFO << VAROUT(result) << " value: " << value + offset;
            check_close(result, value, 0.001);
        }
    }

}

BOOST_AUTO_TEST_CASE(ColorUtilTest)
{
    auto testColor = [](DrawColor24Bit rgb)
    {
        auto hsv = colorutils::RgbToHsv(rgb);
        ARMARX_INFO << VAROUT(rgb.r) << VAROUT(rgb.g) << VAROUT(rgb.b) << " --> " << VAROUT(hsv.h) << VAROUT(hsv.s) << VAROUT(hsv.v);
    };
    testColor({255, 0, 0});
    testColor({0, 255, 0});
    testColor({0, 0, 255});
    testColor({0, 20, 0});
}
