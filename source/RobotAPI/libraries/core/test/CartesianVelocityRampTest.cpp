/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE RobotAPI::CartesianVelocityRamp::Test
#define ARMARX_BOOST_TEST
#include <RobotAPI/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include "../CartesianVelocityRamp.h"
#include <RobotAPI/libraries/core/EigenHelpers.h>

using namespace armarx;



BOOST_AUTO_TEST_CASE(testBoth)
{
    Eigen::VectorXf state = MakeVectorXf(0, 0, 0, 0, 0, 0);
    float dt = 0.1f;

    CartesianVelocityRamp c;
    c.setState(state, VirtualRobot::IKSolver::CartesianSelection::All);
    c.setMaxPositionAcceleration(100);
    c.setMaxOrientationAcceleration(1);

    Eigen::VectorXf target = MakeVectorXf(100, 0, 0, 0, 0, 0);
    state = c.update(target, dt);

    Eigen::VectorXf expected = MakeVectorXf(10, 0, 0, 0, 0, 0);
    BOOST_CHECK_LE((state - expected).norm(), 0.01f);

}
