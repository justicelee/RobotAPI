/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE RobotAPI::CartesianVelocityController::Test
#define ARMARX_BOOST_TEST
#include <RobotAPI/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include "../CartesianVelocityControllerWithRamp.h"

using namespace armarx;

BOOST_AUTO_TEST_CASE(CartesianVelocityRampTest)
{
    Eigen::VectorXf state(6);
    state << 0, 0, 0, 0, 0, 0;
    float dt = 0.1f;

    CartesianVelocityRamp c;
    c.setState(state, VirtualRobot::IKSolver::CartesianSelection::All);
    c.setMaxPositionAcceleration(100);
    c.setMaxOrientationAcceleration(1);

    Eigen::VectorXf target(6);
    target << 200, 0, 0, 3, 0, 0;



    for (size_t i = 0; i < 30; i++)
    {
        state = c.update(target, dt);
        ARMARX_IMPORTANT << "state:" << state.transpose();
    }

}


BOOST_AUTO_TEST_CASE(test1)
{
    const std::string project = "RobotAPI";
    armarx::CMakePackageFinder finder(project);

    if (!finder.packageFound())
    {
        ARMARX_ERROR_S << "ArmarX Package " << project << " has not been found!";
    }
    else
    {
        armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
    }
    std::string fn = "RobotAPI/robots/Armar3/ArmarIII.xml";
    ArmarXDataPath::getAbsolutePath(fn, fn);
    std::string robotFilename = fn;
    VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(robotFilename, VirtualRobot::RobotIO::eStructure);
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet("HipYawRightArm");


    Eigen::VectorXf currentjointVel(8);
    currentjointVel << 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0;


    Eigen::VectorXf targetTcpVel(6);
    targetTcpVel << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;

    CartesianVelocityControllerWithRampPtr h(new CartesianVelocityControllerWithRamp(rns, currentjointVel, VirtualRobot::IKSolver::CartesianSelection::All, 100, 1, 0.1));
    Eigen::MatrixXf jacobi = h->controller.ik->getJacobianMatrix(rns->getTCP(), VirtualRobot::IKSolver::CartesianSelection::All);
    ARMARX_IMPORTANT << "Initial TCP Vel: " << (jacobi * currentjointVel).transpose();


    float dt = 0.01f;

    for (size_t n = 0; n < 300; n++)
    {
        //ARMARX_IMPORTANT << rns->getJointValues();
        Eigen::VectorXf vel = h->calculate(targetTcpVel, 2, dt);
        ARMARX_IMPORTANT << "jv: " << vel.transpose();
        Eigen::VectorXf tcpVel = (h->controller.jacobi * vel);
        ARMARX_IMPORTANT << "cv: " << tcpVel.transpose();
        for (size_t i = 0; i < rns->getSize(); i++)
        {
            rns->getNode(i)->setJointValue(rns->getNode(i)->getJointValue() + vel(i) * dt);
        }
    }
    /*
    Eigen::VectorXf tcpVel = (h->jacobi * vel).transpose();
    ARMARX_IMPORTANT << tcpVel;
    BOOST_CHECK_LE((targetTcpVel - tcpVel).norm(), 0.01f);
    */

}
