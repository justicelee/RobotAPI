/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/observers/filters/AverageFilter.h>

#include <RobotAPI/libraries/core/FramedPose.h>



namespace armarx::filters
{

    class PoseAverageFilter :
        public ::armarx::PoseAverageFilterBase,
        public AverageFilter
    {
    public:
        PoseAverageFilter(int windowSize = 11)
        {
            this->windowFilterSize = windowSize;
        }

        // DatafieldFilterBase interface
    public:
        VariantBasePtr calculate(const Ice::Current& c) const override;

        /**
         * @brief This filter supports: Vector3, FramedDirection, FramedPosition
         * @return List of VariantTypes
         */
        ParameterTypeList getSupportedTypes(const Ice::Current& c) const override
        {
            ParameterTypeList result = AverageFilter::getSupportedTypes(c);
            result.push_back(VariantType::Vector3);
            result.push_back(VariantType::FramedDirection);
            result.push_back(VariantType::FramedPosition);
            return result;
        }
    };

}


