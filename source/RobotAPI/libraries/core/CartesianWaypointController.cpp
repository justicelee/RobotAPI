/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Raphael Grimm (raphael dot grimm at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/core/CartesianVelocityController.h>

#include "CartesianWaypointController.h"

#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/MathTools.h>
#include <cfloat>

namespace armarx
{
    CartesianWaypointController::CartesianWaypointController(
        const VirtualRobot::RobotNodeSetPtr& rns,
        const Eigen::VectorXf& currentJointVelocity,
        float maxPositionAcceleration,
        float maxOrientationAcceleration,
        float maxNullspaceAcceleration,
        const VirtualRobot::RobotNodePtr& tcp,
        const VirtualRobot::RobotNodePtr& referenceFrame
    ) :
        ctrlCartesianVelWithRamps
    {
        rns,
        currentJointVelocity,
        VirtualRobot::IKSolver::CartesianSelection::All,
        maxPositionAcceleration,
        maxOrientationAcceleration,
        maxNullspaceAcceleration,
        tcp ? tcp : rns->getTCP()
    },
    ctrlCartesianPos2Vel(tcp ? tcp : rns->getTCP(), referenceFrame),
                         currentWaypointIndex(0)
    {
        ARMARX_CHECK_NOT_NULL(rns);
        _out = Eigen::VectorXf::Zero(rns->getSize());
        _jnv = Eigen::VectorXf::Zero(rns->getSize());
    }

    const Eigen::VectorXf& CartesianWaypointController::calculate(float dt)
    {
        //calculate cartesian velocity + some management stuff
        if (!isLastWaypoint() && isCurrentTargetNear())
        {
            currentWaypointIndex++;
        }
        cartesianVelocity = ctrlCartesianPos2Vel.calculate(getCurrentTarget(), VirtualRobot::IKSolver::All) + feedForwardVelocity;

        if (autoClearFeedForward)
        {
            clearFeedForwardVelocity();
        }

        //calculate joint velocity
        if (nullSpaceControlEnabled)
        {
            //avoid joint limits
            _jnv = KpJointLimitAvoidance * ctrlCartesianVelWithRamps.controller.calculateJointLimitAvoidance() +
                   ctrlCartesianVelWithRamps.controller.calculateNullspaceVelocity(
                       cartesianVelocity,
                       jointLimitAvoidanceScale,
                       VirtualRobot::IKSolver::All
                   );
        }
        else
        {
            //don't avoid joint limits
            _jnv *= 0;
        }
        _out = ctrlCartesianVelWithRamps.calculate(cartesianVelocity, _jnv, dt);
        return _out;
    }

    void CartesianWaypointController::setWaypoints(const std::vector<Eigen::Matrix4f>& waypoints)
    {
        this->waypoints = waypoints;
        currentWaypointIndex = 0;
    }

    void CartesianWaypointController::swapWaypoints(std::vector<Eigen::Matrix4f>& waypoints)
    {
        std::swap(this->waypoints, waypoints);
        currentWaypointIndex = 0;
    }

    void CartesianWaypointController::addWaypoint(const Eigen::Matrix4f& waypoint)
    {
        this->waypoints.push_back(waypoint);
    }

    void CartesianWaypointController::setTarget(const Eigen::Matrix4f& target)
    {
        waypoints.clear();
        waypoints.push_back(target);
        currentWaypointIndex = 0;
    }

    void CartesianWaypointController::setFeedForwardVelocity(const Eigen::Vector6f& feedForwardVelocity)
    {
        this->feedForwardVelocity = feedForwardVelocity;
    }

    void CartesianWaypointController::setFeedForwardVelocity(const Eigen::Vector3f& feedForwardVelocityPos, const Eigen::Vector3f& feedForwardVelocityOri)
    {
        feedForwardVelocity.block<3, 1>(0, 0) = feedForwardVelocityPos;
        feedForwardVelocity.block<3, 1>(3, 0) = feedForwardVelocityOri;
    }

    void CartesianWaypointController::clearFeedForwardVelocity()
    {
        feedForwardVelocity = Eigen::Vector6f::Zero();
    }

    float CartesianWaypointController::getPositionError() const
    {
        return ctrlCartesianPos2Vel.getPositionError(getCurrentTarget());
    }

    float CartesianWaypointController::getOrientationError() const
    {
        return ctrlCartesianPos2Vel.getOrientationError(getCurrentTarget());
    }

    bool CartesianWaypointController::isCurrentTargetReached() const
    {
        return getPositionError() < thresholdPositionReached && getOrientationError() < thresholdOrientationReached;
    }

    bool CartesianWaypointController::isCurrentTargetNear() const
    {
        return getPositionError() < thresholdPositionNear && getOrientationError() < thresholdOrientationNear;
    }

    bool CartesianWaypointController::isFinalTargetReached() const
    {
        return isLastWaypoint() && isCurrentTargetReached();
    }

    bool CartesianWaypointController::isFinalTargetNear() const
    {
        return isLastWaypoint() && isCurrentTargetNear();
    }

    bool CartesianWaypointController::isLastWaypoint() const
    {
        return currentWaypointIndex + 1 >= waypoints.size();
    }

    const Eigen::Matrix4f& CartesianWaypointController::getCurrentTarget() const
    {
        return waypoints.at(currentWaypointIndex);
    }

    const Eigen::Vector3f CartesianWaypointController::getCurrentTargetPosition() const
    {
        return ::math::Helpers::GetPosition(waypoints.at(currentWaypointIndex));
    }

    size_t CartesianWaypointController::skipToClosestWaypoint(float rad2mmFactor)
    {
        float dist = FLT_MAX;
        size_t minIndex = 0;
        for (size_t i = 0; i < waypoints.size(); i++)
        {
            float posErr = ctrlCartesianPos2Vel.getPositionError(waypoints.at(i));
            float oriErr = ctrlCartesianPos2Vel.getOrientationError(waypoints.at(i));
            float d = posErr + oriErr * rad2mmFactor;
            if (d < dist)
            {
                minIndex = i;
                dist = d;
            }
        }
        currentWaypointIndex = minIndex;
        return currentWaypointIndex;
    }

    void CartesianWaypointController::setNullSpaceControl(bool enabled)
    {
        nullSpaceControlEnabled = enabled;
    }

    std::string CartesianWaypointController::getStatusText()
    {
        std::stringstream ss;
        ss.precision(2);
        ss << std::fixed << "Waypoint: " << (currentWaypointIndex + 1) << "/" << waypoints.size() << " distance: " << getPositionError() << " mm " << VirtualRobot::MathTools::rad2deg(getOrientationError()) << " deg";
        return ss.str();
    }

    void CartesianWaypointController::setConfig(const CartesianWaypointControllerConfig& cfg)
    {
        KpJointLimitAvoidance           = cfg.kpJointLimitAvoidance;
        jointLimitAvoidanceScale        = cfg.jointLimitAvoidanceScale;

        thresholdOrientationNear        = cfg.thresholdOrientationNear;
        thresholdOrientationReached     = cfg.thresholdOrientationReached;
        thresholdPositionNear           = cfg.thresholdPositionNear;
        thresholdPositionReached        = cfg.thresholdPositionReached;

        ctrlCartesianPos2Vel.maxOriVel  = cfg.maxOriVel;
        ctrlCartesianPos2Vel.maxPosVel  = cfg.maxPosVel;
        ctrlCartesianPos2Vel.KpOri      = cfg.kpOri;
        ctrlCartesianPos2Vel.KpPos      = cfg.kpPos;

        ctrlCartesianVelWithRamps.setMaxAccelerations(
            cfg.maxPositionAcceleration,
            cfg.maxOrientationAcceleration,
            cfg.maxNullspaceAcceleration
        );
    }
    void CartesianWaypointController::setCurrentJointVelocity(const Eigen::Ref<const Eigen::VectorXf>& currentJointVelocity)
    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
        ctrlCartesianVelWithRamps.setCurrentJointVelocity(currentJointVelocity);
#pragma GCC diagnostic pop
    }
}
