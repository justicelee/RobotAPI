/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::core::PIDController
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "MultiDimPIDController.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/observers/filters/rtfilters/RTFilterBase.h>

#include <Eigen/Core>

#include <memory>
#include <mutex>

namespace armarx
{
    class PIDController :
        public Logging
    {
    public:
        PIDController() = default;
        PIDController(PIDController&& o);
        PIDController(const PIDController& o);
        PIDController& operator=(PIDController&& o);
        PIDController& operator=(const PIDController& o);

        PIDController(float Kp,
                      float Ki,
                      float Kd,
                      double maxControlValue = std::numeric_limits<double>::max(),
                      double maxDerivation = std::numeric_limits<double>::max(),
                      bool limitless = false, bool threadSafe = true);
        void update(double deltaSec, double measuredValue, double targetValue);
        void update(double measuredValue, double targetValue);
        void update(double measuredValue);
        void setTarget(double newTarget);
        void setMaxControlValue(double newMax);
        double getControlValue() const;

        void reset();
        //    protected:
        float Kp, Ki, Kd;
        double integral;
        double maxIntegral = std::numeric_limits<double>::max();
        double conditionalIntegralErrorTreshold = std::numeric_limits<double>::max(); // anti-windup
        double derivative;
        double previousError;
        double processValue;
        double target;
        IceUtil::Time lastUpdateTime;
        double controlValue;
        double controlValueDerivation;
        double maxControlValue;
        double maxDerivation;
        bool firstRun;
        bool limitless;
        bool threadSafe = true;
        rtfilters::RTFilterBasePtr differentialFilter;
        rtfilters::RTFilterBasePtr pdOutputFilter;
    private:
        using ScopedRecursiveLock = std::unique_lock<std::recursive_mutex>;
        using ScopedRecursiveLockPtr = std::unique_ptr<ScopedRecursiveLock>;
        ScopedRecursiveLockPtr getLock() const;
        mutable std::recursive_mutex mutex;
    };
    using PIDControllerPtr = std::shared_ptr<PIDController>;


}

