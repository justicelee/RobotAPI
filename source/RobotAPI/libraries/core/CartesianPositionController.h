/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/IK/DifferentialIK.h>
#include <VirtualRobot/Robot.h>
#include <Eigen/Dense>

#include <RobotAPI/interface/core/CartesianPositionControllerConfig.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace armarx
{
    class CartesianPositionController;
    using CartesianPositionControllerPtr = std::shared_ptr<CartesianPositionController>;

    // This is a P-controller. In: Target pose, out Cartesian velocity.
    class CartesianPositionController
    {
    public:
        CartesianPositionController(const VirtualRobot::RobotNodePtr& tcp,
                                    const VirtualRobot::RobotNodePtr& referenceFrame = nullptr);

        CartesianPositionController(CartesianPositionController&&) = default;
        CartesianPositionController& operator=(CartesianPositionController&&) = default;

        Eigen::VectorXf calculate(const Eigen::Matrix4f& targetPose, VirtualRobot::IKSolver::CartesianSelection mode) const;
        Eigen::VectorXf calculatePos(const Eigen::Vector3f& targetPos) const;

        float getPositionError(const Eigen::Matrix4f& targetPose) const;
        float getOrientationError(const Eigen::Matrix4f& targetPose) const;
        static float GetPositionError(const Eigen::Matrix4f& targetPose,
                                      const VirtualRobot::RobotNodePtr& tcp,
                                      const VirtualRobot::RobotNodePtr& referenceFrame = nullptr);
        static float GetOrientationError(const Eigen::Matrix4f& targetPose,
                                         const VirtualRobot::RobotNodePtr& tcp,
                                         const VirtualRobot::RobotNodePtr& referenceFrame = nullptr);
        static bool Reached(const Eigen::Matrix4f& targetPose,
                            const VirtualRobot::RobotNodePtr& tcp,
                            bool checkOri,
                            float thresholdPosReached,
                            float thresholdOriReached,
                            const VirtualRobot::RobotNodePtr& referenceFrame = nullptr);
        bool reached(const Eigen::Matrix4f& targetPose, VirtualRobot::IKSolver::CartesianSelection mode, float thresholdPosReached, float thresholdOriReached);

        Eigen::Vector3f getPositionDiff(const Eigen::Matrix4f& targetPose) const;
        Eigen::Vector3f getPositionDiffVec3(const Eigen::Vector3f& targetPosition) const;
        Eigen::Vector3f getOrientationDiff(const Eigen::Matrix4f& targetPose) const;
        VirtualRobot::RobotNodePtr getTcp() const;

        float KpPos = 1;
        float KpOri = 1;
        float maxPosVel = -1;
        float maxOriVel = -1;


    private:
        VirtualRobot::RobotNodePtr tcp;
        VirtualRobot::RobotNodePtr referenceFrame;
    };
}
namespace armarx::VariantType
{
    // variant types
    const VariantTypeId CartesianPositionControllerConfig = Variant::addTypeName("::armarx::CartesianPositionControllerConfig");
}

namespace armarx
{
    class CartesianPositionControllerConfig : virtual public CartesianPositionControllerConfigBase
    {
    public:
        CartesianPositionControllerConfig();

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const override
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& = ::Ice::Current()) const override
        {
            return new CartesianPositionControllerConfig(*this);
        }
        std::string output(const Ice::Current& c = ::Ice::Current()) const override;
        VariantTypeId getType(const Ice::Current& = ::Ice::Current()) const override
        {
            return VariantType::CartesianPositionControllerConfig;
        }
        bool validate(const Ice::Current& = ::Ice::Current()) override
        {
            return true;
        }

        friend std::ostream& operator<<(std::ostream& stream, const CartesianPositionControllerConfig& rhs)
        {
            stream << "CartesianPositionControllerConfig: " << std::endl << rhs.output() << std::endl;
            return stream;
        }

    public: // serialization
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override;

    };

    typedef IceInternal::Handle<CartesianPositionControllerConfig> CartesianPositionControllerConfigPtr;
}

