/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <math.h>
#include <Eigen/Eigen>

namespace armarx::math
{
    class SVD
    {
    private:
        Eigen::JacobiSVD<Eigen::MatrixXf> svd;
    public:
        Eigen::MatrixXf matrixU;
        Eigen::MatrixXf matrixV;
        Eigen::VectorXf singularValues;
        SVD(Eigen::MatrixXf matrix)
            : svd(matrix, Eigen::ComputeThinU | Eigen::ComputeThinV)
        {
            matrixU = svd.matrixU();
            matrixV = svd.matrixV();
            singularValues = svd.singularValues();
        }

        Eigen::Vector3f getLeftSingularVector3D(int nr)
        {
            return matrixU.block<3, 1>(0, nr);
        }
        float getLeftSingularValue(int nr)
        {
            return singularValues(nr);
        }

    };
}


