/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author      ()
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

namespace armarx::math
{
    class TimeSeriesUtils;
    using TimeSeriesUtilsPtr = std::shared_ptr<TimeSeriesUtils>;


    class TimeSeriesUtils
    {
    public:
        enum class BorderMode
        {
            Nearest
        };
        TimeSeriesUtils();

        static std::vector<float> Resample(const std::vector<float>& timestamps, const std::vector<float>& data, const std::vector<float>& newTimestamps);
        static std::vector<float> ApplyFilter(const std::vector<float>& data, const std::vector<float>& filter, BorderMode mode);
        static std::vector<float> ApplyGaussianFilter(const std::vector<float>& data, float sigma, float sampleTime, BorderMode mode);
        static std::vector<float> CreateGaussianFilter(const float sigma, float sampleTime, float truncate = 4);
        static std::vector<float> MakeTimestamps(float start, float end, size_t count);

    private:
    };
}


