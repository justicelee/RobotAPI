/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <math.h>

namespace armarx::math
{
    class Trigonometry
    {
    public:
        static float Deg2RadF(const float angle)
        {
            return angle / 180.0f * M_PI;
        }
        static double Deg2RadD(const double angle)
        {
            return angle / 180.0 * M_PI;
        }
        static float Rad2DegF(const float rad)
        {
            return rad / M_PI * 180.0f;
        }
        static double Rad2DegD(const float rad)
        {
            return rad / M_PI * 180.0;
        }

        static double GetAngleFromVectorXY(const Eigen::Vector3f& vector)
        {
            return atan2(vector(1), vector(0));
        }
    };
}


