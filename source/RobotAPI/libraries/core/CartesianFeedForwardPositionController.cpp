/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianFeedForwardPositionController.h"
#include <RobotAPI/libraries/core/math/MathUtils.h>

namespace armarx
{
    CartesianFeedForwardPositionController::CartesianFeedForwardPositionController(const VirtualRobot::RobotNodePtr& tcp)
        : tcp(tcp)
    {

    }

    Eigen::VectorXf CartesianFeedForwardPositionController::calculate(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t, VirtualRobot::IKSolver::CartesianSelection mode)
    {
        int posLen = mode & VirtualRobot::IKSolver::Position ? 3 : 0;
        int oriLen = mode & VirtualRobot::IKSolver::Orientation ? 3 : 0;
        Eigen::VectorXf cartesianVel(posLen + oriLen);

        if (posLen)
        {
            Eigen::Vector3f targetPos = trajectory->GetPosition(t);
            Eigen::Vector3f errPos = targetPos - tcp->getPositionInRootFrame();
            Eigen::Vector3f posVel =  errPos * KpPos;
            if (enableFeedForward)
            {
                posVel += trajectory->GetPositionDerivative(t);
            }

            if (maxPosVel > 0)
            {
                posVel = math::MathUtils::LimitTo(posVel, maxPosVel);
            }
            cartesianVel.block<3, 1>(0, 0) = posVel;
        }

        if (oriLen)
        {
            Eigen::Matrix3f targetOri = trajectory->GetOrientation(t).toRotationMatrix();
            Eigen::Matrix3f tcpOri = tcp->getPoseInRootFrame().block<3, 3>(0, 0);
            Eigen::Matrix3f oriDir = targetOri * tcpOri.inverse();
            Eigen::AngleAxisf aa(oriDir);
            Eigen::Vector3f errOri = aa.axis() * aa.angle();
            Eigen::Vector3f oriVel = errOri * KpOri;
            if (enableFeedForward)
            {
                oriVel += trajectory->GetOrientationDerivative(t);
            }

            if (maxOriVel > 0)
            {
                oriVel = math::MathUtils::LimitTo(oriVel, maxOriVel);
            }
            cartesianVel.block<3, 1>(posLen, 0) = oriVel;
        }
        return cartesianVel;
    }

    float CartesianFeedForwardPositionController::getPositionError(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t)
    {
        Eigen::Vector3f targetPos = trajectory->GetPosition(t);
        Eigen::Vector3f errPos = targetPos - tcp->getPositionInRootFrame();
        return errPos.norm();
    }

    float CartesianFeedForwardPositionController::getOrientationError(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t)
    {
        Eigen::Matrix3f targetOri = trajectory->GetOrientation(t).toRotationMatrix();
        Eigen::Matrix3f tcpOri = tcp->getPoseInRootFrame().block<3, 3>(0, 0);
        Eigen::Matrix3f oriDir = targetOri * tcpOri.inverse();
        Eigen::AngleAxisf aa(oriDir);
        return aa.angle();
    }

    Eigen::Vector3f CartesianFeedForwardPositionController::getPositionDiff(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t)
    {
        Eigen::Vector3f targetPos = trajectory->GetPosition(t);
        return targetPos - tcp->getPositionInRootFrame();

    }

    Eigen::Vector3f CartesianFeedForwardPositionController::getOrientationDiff(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t)
    {
        Eigen::Matrix3f targetOri = trajectory->GetOrientation(t).toRotationMatrix();
        Eigen::Matrix3f tcpOri = tcp->getPoseInRootFrame().block<3, 3>(0, 0);
        Eigen::Matrix3f oriDir = targetOri * tcpOri.inverse();
        Eigen::AngleAxisf aa(oriDir);
        return aa.axis() * aa.angle();
    }
}
