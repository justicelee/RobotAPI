/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PIDController.h"
#include "Trajectory.h"
#pragma once

namespace armarx
{

    class TrajectoryController
    {
    public:
        TrajectoryController(const TrajectoryPtr& traj, float kp, float ki = 0.0f, float kd = 0.0f, bool threadSafe = true, float maxIntegral = std::numeric_limits<float>::max());
        const Eigen::VectorXf& update(double deltaT, const Eigen::VectorXf& currentPosition);
        //const MultiDimPIDControllerPtr& getPid() const;
        //void setPid(const MultiDimPIDControllerPtr& value);

        double getCurrentTimestamp() const;

        const TrajectoryPtr& getTraj() const;

        static void UnfoldLimitlessJointPositions(TrajectoryPtr traj);
        static void FoldLimitlessJointPositions(TrajectoryPtr traj);

        const Eigen::VectorXf& getCurrentError() const;

        const Eigen::VectorXf& getPositions() const;

    protected:
        TrajectoryPtr traj;
        MultiDimPIDControllerPtr pid;
        //std::vector<PIDControllerPtr> pids;
        double currentTimestamp;
        Eigen::VectorXf positions;
        Eigen::VectorXf veloctities;
        Eigen::VectorXf currentError;

    };
    using TrajectoryControllerPtr = std::shared_ptr<TrajectoryController>;

} // namespace armarx

