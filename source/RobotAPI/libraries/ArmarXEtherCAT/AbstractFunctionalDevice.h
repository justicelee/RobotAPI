#pragma once

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/libraries/ArmarXEtherCAT/VirtualDeviceFactory.h>

namespace armarx
{
    class AbstractFunctionalDevice;
    using AbstractFunctionalDevicePtr = std::shared_ptr<AbstractFunctionalDevice>;

    class AbstractFunctionalDevice : public std::enable_shared_from_this<AbstractFunctionalDevice>
    {
    public:
        AbstractFunctionalDevice(DefaultRapidXmlReaderNode configNode) :
            node(configNode),
            initialized(false)
        {
            //just to be sure, sometimes strange things happen when don't do it
            node = configNode;
        }
        virtual ~AbstractFunctionalDevice() {}

        virtual const DefaultRapidXmlReaderNode getNode() const;

        virtual bool isInitialized() const;

        /**
         * This converts the sensor data that arrive from the bus into floats and copys them they can be published via a DataUnit.
         */
        virtual void initData() = 0;
        virtual void execute() {}


        std::string getClassName() const;

    protected:
        template <typename Base, typename constructorArg, typename SharedPointer>
        friend class AbstractFactoryMethod;
        std::string className;
        DefaultRapidXmlReaderNode node;
        bool initialized = false;
    };
}


