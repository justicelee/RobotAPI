#pragma once

#include <stdint.h>
#include <memory>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include "SlaveIdentifier.h"


namespace armarx
{
    class EtherCAT;

#define DEFAULT_VENDOR_ID 0

    class AbstractSlave;
    using AbstractSlavePtr = std::shared_ptr<AbstractSlave>;

    class AbstractSlave : public armarx::Logging
    {

    public:
        AbstractSlave(const armarx::SlaveIdentifier slaveIdentifier, uint16_t slaveNumber);
        virtual ~AbstractSlave() {}
        /**
         * This is called after EtherCAT Bus is PreOp Mode. This is where the PDO Mapping can be configured for the slave.
         */
        virtual void doMappings() = 0;

        /**
         * This gets triggered by the bus controller before it will start the control loop.
         * If a slave needs more preparation than just getting in EtherCAT Op-Mode this should be done here.
         * So slaves can assume that the EtherCAT state machine is in Op-Mode so PDO's are available.
         * Attention!!! This needs to be implemented cooperative
         * @return true if the prepare is finished an don't needs to be called again
         */
        virtual bool prepare() = 0;
        /**
         * This method gets triggered by the Bus Controller, this function hast to be implemented cooperative.
         * The Bus controller will guarantee that the process data will be update before each call.
         */
        virtual void execute() = 0;

        /**
         * This gets triggered by the bus Controller before it will close the EtherCAT bus.
         * So if the device need to do something before to get in a safe state, this can be done here.
         * Attention!!! This needs to be implemented cooperative
         * @return if slave is shut down
         */
        virtual bool shutdown()  = 0;

        virtual void setInputPDO(void* ptr) = 0;

        virtual void setOutputPDO(void* ptr) = 0;

        /**
         * This gets called between the SafeOp an the Op state of the bus at the initizialisation
         */
        virtual void prepareForOp() = 0;
        /**
         * @brief This gets called after prepareForOp() was called. This is useful if prepareForOp()
         * executes a long running initialization and needs to be done before the slave goes into op
         */
        virtual void finishPreparingForOp() {}

        virtual void prepareForSafeOp() {}
        virtual void finishPreparingForSafeOp() {}

        /**
         * This function indicates if there is a error or Problem with this slave. It should not
         * @return true if there is an error/problem with this slave otherwise false;
         */
        virtual bool hasError() = 0;
        virtual bool isEmergencyStopActive() const
        {
            return false;
        }
        virtual bool recoverFromEmergencyStop()
        {
            return true;
        }

        /**
         * This tries to clear oder fix the errors or problems of the slave or just gives detailed information about the problem.
         * If hasError == false this function does nothing.
         * @return true if the function is trying to recover the slave or there is no error, false is send if this just reports the info
         */
        virtual bool handleErrors();

        /*uint32_t getVendorID() const ;

        uint32_t getSerialNumber() const;*/

        uint16_t getSlaveNumber() const;
        const SlaveIdentifier& getSlaveIdentifier() const;

        virtual bool hasPDOMapping() const = 0;

    private:
        const armarx::SlaveIdentifier slaveIdentifier;
        /*const uint32_t vendorID;
        const uint32_t serialNumber;*/
        const uint16_t slaveNumber;

    };

    template<class InputT, class OutputT>
    class AbstractSlaveWithInputOutput : public AbstractSlave
    {
    public:
        using AbstractSlave::AbstractSlave;

        void setInputPDO(void* ptr) override
        {
            const auto ptrAsInt = reinterpret_cast<std::uint64_t>(ptr);
            ARMARX_CHECK_EXPRESSION(
                (ptrAsInt % alignof(InputT)) == 0) <<
                                                   "\nThe alignment is wrong!\nIt has to be "
                                                   << alignof(InputT) << ", but the data is aligned with "
                                                   << ptrAsInt % alignof(std::max_align_t)
                                                   << "!\nThis is an offset of " << (ptrAsInt % alignof(InputT))
                                                   << " bytes!\nThe datatype is " << GetTypeString<InputT>()
                                                   << "\nIts size is " << sizeof(InputT)
                                                   << " bytes";
            inputs = static_cast<InputT*>(ptr);
        }
        void setOutputPDO(void* ptr) override
        {
            const auto ptrAsInt = reinterpret_cast<std::uint64_t>(ptr);
            ARMARX_CHECK_EXPRESSION(
                (ptrAsInt % alignof(OutputT)) == 0) <<
                                                    "\nThe alignment is wrong!\nIt has to be "
                                                    << alignof(OutputT) << ", but the data is aligned with "
                                                    << ptrAsInt % alignof(std::max_align_t)
                                                    << "!\nThis is an offset of " << (ptrAsInt % alignof(OutputT))
                                                    << " bytes!\nThe datatype is " << GetTypeString<OutputT>()
                                                    << "\nIts size is " << sizeof(OutputT)
                                                    << " bytes";
            outputs = static_cast<OutputT*>(ptr);
        }
        bool hasPDOMapping() const final override
        {
            return true;
        }
        OutputT* getOutputsPtr()
        {
            return outputs;
        }
        InputT* getInputsPtr()
        {
            return inputs;
        }
    protected:
        InputT* inputs{nullptr};
        OutputT* outputs;

    };

}

