#include "EtherCATDeviceFactory.h"
#include "AbstractFunctionalDevice.h"

namespace armarx
{




    EtherCATDeviceFactory::EtherCATDeviceFactory()
    {

    }

    const std::vector<AbstractSlavePtr>& EtherCATDeviceFactory::getSlaves() const
    {
        return slaves;
    }

    const std::vector<ControlDevicePtr>& EtherCATDeviceFactory::getCtrlDevs() const
    {
        return ctrlDevs;
    }

    const std::vector<SensorDevicePtr>& EtherCATDeviceFactory::getSensDevs() const
    {
        return sensDevs;
    }

    void EtherCATDeviceFactory::addControlDevice(ControlDevicePtr dev)
    {
        ctrlDevs.push_back(dev);
    }

    void EtherCATDeviceFactory::addSensorDevice(SensorDevicePtr dev)
    {
        sensDevs.push_back(dev);
    }

    void EtherCATDeviceFactory::addSlave(const AbstractSlavePtr& slave)
    {
        this->slaves.push_back(slave);
    }

} // namespace armarx
