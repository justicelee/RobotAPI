#pragma once
#include "ArmarXEtherCATFwd.h"
#include <ArmarXCore/core/system/AbstractFactoryMethod.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include "SlaveIdentifier.h"



namespace armarx
{
    using AbstractFunctionalDevicePtr = std::shared_ptr<class AbstractFunctionalDevice>;

    using DeviceContainerPtr = std::shared_ptr<class DeviceContainer>;

    using ControlDevicePtr = std::shared_ptr<class ControlDevice>;
    using SensorDevicePtr = std::shared_ptr<class SensorDevice>;

    using EtherCATFactoryArgs = std::tuple<EtherCAT*, uint16_t, DeviceContainerPtr>;

    class EtherCATDeviceFactory :
        public AbstractFactoryMethod<EtherCATDeviceFactory, EtherCATFactoryArgs, std::shared_ptr<EtherCATDeviceFactory>>
    {
    public:
        EtherCATDeviceFactory();
        const std::vector<AbstractSlavePtr>& getSlaves() const;
        const std::vector<ControlDevicePtr>& getCtrlDevs() const;
        const std::vector<SensorDevicePtr>& getSensDevs() const;

        void addSlave(const AbstractSlavePtr& slave);
        void addControlDevice(ControlDevicePtr dev);
        void addSensorDevice(SensorDevicePtr dev);
    private:
        std::vector<AbstractSlavePtr> slaves;
        std::vector<ControlDevicePtr> ctrlDevs;
        std::vector<SensorDevicePtr> sensDevs;
    };

} // namespace armarx

