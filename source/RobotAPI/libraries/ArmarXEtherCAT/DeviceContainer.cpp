/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "DeviceContainer.h"
#include "AbstractFunctionalDevice.h"

#include <ArmarXCore/core/rapidxml/wrapper/MultiNodeRapidXMLReader.h>
#include <VirtualRobot/Robot.h>


namespace armarx
{

    size_t DeviceContainer::load(const MultiNodeRapidXMLReader& rootNodeConfigs, const VirtualRobot::RobotPtr& robot)
    {
        //        sleep(5);
        size_t addedDevices = 0;
        //        auto children = robot->getRobotNodes();
        //        auto getSceneObject = [&](const std::string & name)
        //        {
        //            for (auto& obj : children)
        //            {
        //                if (obj->getName() == name)
        //                {
        //                    return VirtualRobot::SceneObjectPtr(obj);
        //                }
        //            }
        //            for (auto& s : robot->getSensors())
        //            {
        //                if (s->getName() == name)
        //                {
        //                    return  VirtualRobot::SceneObjectPtr(s);
        //                }
        //            }
        //            return VirtualRobot::SceneObjectPtr();
        //        };
        //rootNode = rootNodeConfig;
        ARMARX_DEBUG << "Device factories: " << VirtualDeviceFactory::getAvailableClasses();
        auto defaultNode = DefaultRapidXmlReaderNode(rootNodeConfigs.nodes("DefaultConfiguration"));
        for (auto& node : rootNodeConfigs.nodes(nullptr))
        {
            try
            {


                if (node.name() == "DefaultConfiguration" || node.name() == "include" || node.name().empty())
                {
                    continue;
                }
                auto name = node.attribute_value("name");
                ARMARX_DEBUG << "Handling: " << node.name() << " name: " << name;
                //            auto obj = getSceneObject(name);
                //            ARMARX_CHECK_EXPRESSION(obj) << name;
                auto tuple = std::make_tuple(node, defaultNode, robot);
                auto instance = VirtualDeviceFactory::fromName(node.name(), tuple);
                if (!instance)
                {
                    ARMARX_WARNING << "No factory found for virtual device " << node.name();
                }
                else
                {
                    ARMARX_VERBOSE << "Created instance of type " << node.name();
                    devices.push_back(instance);
                    addedDevices++;
                }
            }
            catch (...)
            {
                handleExceptions();
            }
        }
        return addedDevices;

    }

    std::vector<AbstractFunctionalDevicePtr> DeviceContainer::getAllInitializedFunctionalDevices() const
    {
        std::vector<AbstractFunctionalDevicePtr> returnList;
        for (AbstractFunctionalDevicePtr& device : getAllFunctionalDevices())
        {
            if (device && device->isInitialized())
            {
                returnList.push_back(device);
            }
        }
        return returnList;
    }

    std::vector<AbstractFunctionalDevicePtr> DeviceContainer::getAllUninitializedFunctionalDevices() const
    {
        std::vector<AbstractFunctionalDevicePtr> returnList;
        for (AbstractFunctionalDevicePtr& device : getAllFunctionalDevices())
        {
            if (device && !device->isInitialized())
            {
                returnList.push_back(device);
            }
        }
        return returnList;
    }

    std::vector<AbstractFunctionalDevicePtr> DeviceContainer::getAllFunctionalDevices() const
    {
        return devices;
    }

} // namespace armarx
