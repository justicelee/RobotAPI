/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "EtherCATRTUnit.h"
#include "DeviceContainer.h"
#include <chrono>
#include <thread>
#include <sstream>
#include <sched.h>
#include <syscall.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include <boost/algorithm/clamp.hpp>

#include <VirtualRobot/Tools/Gravity.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/rapidxml/wrapper/MultiNodeRapidXMLReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>

#include <RobotAPI/components/units/RobotUnit/Units/RobotUnitSubUnit.h>
#include <RobotAPI/components/units/RobotUnit/util/RtTiming.h>



#include <RobotAPI/libraries/ArmarXEtherCAT/EtherCAT.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syscall.h>
#include <RobotAPI/components/units/RobotUnit/util/RtTiming.h>
#include <RobotAPI/libraries/core/Pose.h>


using namespace armarx;



#define MAX_SAFE_STACK (8*1024) /* The maximum stack size which is
                                   guaranteed safe to access without
                                   faulting */



#define NSEC_PER_SEC    (1000*1000*1000) /* The number of nsecs per sec. */



EtherCATRTUnit::EtherCATRTUnit() :
    rtLoopTime(-1),
    deviceContainerPtr(nullptr)
{

}

void EtherCATRTUnit::componentPropertiesUpdated(const std::set<std::string>& changedProperties)
{
    if (changedProperties.count("checkErrorCountersOnEtherCATError"))
    {
        auto val = getProperty<bool>("checkErrorCountersOnEtherCATError").getValue();
        ARMARX_INFO << "Changing checkErrorCountersOnEtherCATError to " << val;
        EtherCAT::getBus().setCheckErrorCountersOnWKCError(val);
    }
}


void EtherCATRTUnit::onInitRobotUnit()
{
    ARMARX_TRACE;
    rtWarningFactor = getProperty<float>("RTLoopTimingCheckToleranceFactor").getValue();
    rtLoopTime = getProperty<int>("RTLoopFrequency").getValue();


    ARMARX_INFO << "Locking memory";

    if (mlockall(MCL_CURRENT | MCL_FUTURE) == -1)
    {
        ARMARX_WARNING << "Could nock lock memory (mlockall failed)";
        //::exit(-2);
    }

    ARMARX_DEBUG << "Pre-fault our stack";
    unsigned char dummy[MAX_SAFE_STACK];
    memset(dummy, 0, MAX_SAFE_STACK);

    ARMARX_INFO << "EtherCATRTUnit initializing now";
    publishRobot = rtGetRobot()->clone();
    ARMARX_CHECK_EXPRESSION(rtGetRobot()->hasRobotNodeSet("RobotJoints"));
    ARMARX_CHECK_EXPRESSION(rtGetRobot()->hasRobotNodeSet("RobotCol"));
    rtRobotJointSet = rtGetRobot()->getRobotNodeSet("RobotJoints");
    rtRobotBodySet = rtGetRobot()->getRobotNodeSet("RobotCol");
    ARMARX_CHECK_NOT_NULL(rtRobotJointSet);
    ARMARX_CHECK_NOT_NULL(rtRobotBodySet);
    for (auto& node : *rtRobotJointSet)
    {
        node->setEnforceJointLimits(false);
    }
    std::stringstream massesInfo;
    for (int i = 0; i < (int)rtRobotBodySet->getSize(); ++i)
    {
        auto node = rtRobotBodySet->getNode(i);
        massesInfo << "\t" << node->getName() << ": " << node->getMass() << " kg\n";
    }
    ARMARX_DEBUG << "Masses info: " << massesInfo.str();

    //setting the bus
    EtherCAT& bus = EtherCAT::getBus();
    bus.setMainUnitPtr(this);
    bus.setDeviceContainerPtr(deviceContainerPtr);
    bus.setCheckErrorCountersOnWKCError(getProperty<bool>("checkErrorCountersOnEtherCATError").getValue());
    checkErrorCountersOnStartupFlag = (getProperty<bool>("checkErrorCountersOnStartup").getValue());
    checkErrorCountersOnStartupDelayMS = (getProperty<long>("checkErrorCountersOnStartupDelayMS").getValue());

}

void EtherCATRTUnit::onConnectRobotUnit()
{
    ARMARX_INFO << "EtherCATRTUnit connects now";
    dd = getTopic<DebugDrawerInterfacePrx>("DebugDrawerUpdates");


    startRTThread();
    while (this->getRobotUnitState() < RobotUnitState::Running)
    {
        usleep(100000);
    }






}

void EtherCATRTUnit::onDisconnectRobotUnit()
{
    ARMARX_INFO << "EtherCATRTUnit disconnects now";
}

void EtherCATRTUnit::onExitRobotUnit()
{
    ARMARX_INFO << "EtherCATRTUnit is exiting now ";

    /* close the latency_target_fd if it's open */
    if (latency_target_fd >= 0)
    {
        close(latency_target_fd);
    }
    ARMARX_INFO << "EtherCATRTUnit exit complete";
}

void EtherCATRTUnit::initializeKinematicUnit()
{
    using IfaceT = KinematicUnitInterface;

    auto guard = getGuard();
    throwIfStateIsNot(RobotUnitState::InitializingUnits, __FUNCTION__);
    //check if unit is already added
    if (getUnit(IfaceT::ice_staticId()))
    {
        return;
    }

    auto unit = createKinematicSubUnit(getIceProperties()->clone(), ControlModes::Position1DoF,
                                       getProperty<bool>("UseTorqueVelocityModeAsDefault").getValue() ? ControlModes::VelocityTorque : ControlModes::Velocity1DoF);
    //add
    if (unit)
    {
        addUnit(std::move(unit));
    }
}

void EtherCATRTUnit::startRTThread()
{
    ARMARX_INFO << "starting control task";
    //task->start();
    if (rtTask.joinable())
    {
        rtTask.join();
    }

    rtTask = std::thread
    {
        [this] {
            taskRunning = true;
            try
            {
                this->rtRun();
            }
            catch (...)
            {
                ARMARX_FATAL << "RT Thread caused an uncaught exception:\n" << GetHandledExceptionString();
            }
        }
    };

}

void EtherCATRTUnit::joinControlThread()
{
    ARMARX_INFO << "stopping control task";
    taskRunning = false;
    //    EtherCAT& bus = EtherCAT::getBus();
    //    bus.switchBusOFF();
    rtTask.join();
    ARMARX_INFO << "rt task stopped";
}

void EtherCATRTUnit::publish(StringVariantBaseMap debugObserverMap, StringVariantBaseMap timingMap)
{
    RobotUnit::publish(std::move(debugObserverMap), std::move(timingMap));

    //    for (auto& name : getSensorDeviceNames())
    //    {
    //        auto data = getSensorDevice(name)->getSensorValue()->asA<SensorValue1DoFActuatorPosition>();
    //        if (!data)
    //        {
    //            continue;
    //        }
    //        publishRobot->getRobotNode(name)->setJointValueNoUpdate(data->position);
    //    }
    //    publishRobot->applyJointValues();
    //    for (auto& name : getSensorDeviceNames())
    //    {
    //        auto data = getSensorDevice(name)->getSensorValue()->asA<SensorValue1DoFActuatorTorque>();
    //        if (!data)
    //        {
    //            continue;
    //        }
    //        auto node = publishRobot->getRobotNode(name);
    //        ARMARX_CHECK_EXPRESSION(node);
    //        auto joint = boost::dynamic_pointer_cast<VirtualRobot::RobotNodeRevolute>(node);
    //        ARMARX_CHECK_EXPRESSION(joint);
    //        Vector3Ptr pos = new Vector3(joint->getGlobalPosition());
    //        Vector3Ptr axis = new Vector3(joint->getJointRotationAxis());
    //        float percent = data->torque / 4.0f;
    //        if (std::abs(percent) > 0.1)
    //        {
    //            getDebugDrawerProxy()->setCircleArrowVisu("TorqueEstimation", name, pos, axis, 100, percent, 5, DrawColor {0, 1, 0, 1});
    //        }
    //        else
    //        {
    //            getDebugDrawerProxy()->removeCircleVisu("TorqueEstimation", name);
    //        }

    //    }
}

armarx::PropertyDefinitionsPtr EtherCATRTUnit::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new EtherCATRTUnitPropertyDefinitions(
            getConfigIdentifier()));
}

EtherCATRTUnit::~EtherCATRTUnit()
{
    ARMARX_INFO << "DTOR of EtherCATRTUnit";
}


void EtherCATRTUnit::rtRun()
{
    ARMARX_INFO << "Control task running";
    EtherCAT& bus = EtherCAT::getBus();

    ARMARX_ON_SCOPE_EXIT
    {
        ARMARX_INFO << "Leaving rtRun scope";
        //switching off the bus and be sure that the bus will receive
        bus.switchBusOFF();
        bus.updateBus();
        if (getProperty<int>("SocketFileDescriptor").isSet() && getProperty<int>("SocketFileDescriptor").getValue() > 0)
        {
            ARMARX_INFO << "Closing raw socket with FD " << getProperty<int>("SocketFileDescriptor").getValue();
            int ret = close(getProperty<int>("SocketFileDescriptor").getValue());
            if (ret)
            {
                ARMARX_INFO << "Failed to close raw socket file handle: " << ret << " errno: " << strerror(errno);
            }
        }
    };


    bool busStartSucceeded = false;
    if (getProperty<bool>("StartEtherCATBus").getValue())
    {
        try
        {
            ARMARX_DEBUG << "initBus";
            busStartSucceeded = initBusRTThread();
        }
        catch (...)
        {
            ARMARX_ERROR << "init ethercat bus failed with exception: " << armarx::GetHandledExceptionString();
            throw;
        }

        ARMARX_INFO << "initBus finished with: " << busStartSucceeded;
    }
    else
    {
        ARMARX_IMPORTANT << "EtherCAT Bus disabled in properties - not starting it";
    }

    if (deviceContainerPtr->getAllInitializedFunctionalDevices().empty())
    {
        ARMARX_WARNING << "No functional devices found - shutting down";
        return;
    }
    ARMARX_DEBUG << "Getting list of uninitialized devices";
    Ice::StringSeq uninitializedDevices;
    for (AbstractFunctionalDevicePtr& device : deviceContainerPtr->getAllUninitializedFunctionalDevices())
    {
        std::shared_ptr<DeviceBase> deviceBase = std::dynamic_pointer_cast<DeviceBase>(device);
        if (deviceBase)
        {
            uninitializedDevices.push_back(deviceBase->getDeviceName());
        }
        else
        {
            uninitializedDevices.push_back("Unkown Device");
        }
    }
    if (!uninitializedDevices.empty())
    {
        ARMARX_WARNING << "Configured but not found or disabled devices: " << uninitializedDevices;
    }


    bool initializationDone = false;
    bool initializationFailed = false;
    ARMARX_DEBUG << "Async init starting now";
    std::thread unitInitTask = std::thread
    {
        [&, this] {
            try
            {
                finishDeviceInitialization();
                //                rtReadSensorDeviceValues(TimeUtil::GetTime(), IceUtil::Time::microSeconds(1)); // initialize sensor values
                initializeDefaultUnits();
                ARMARX_IMPORTANT << "Setting up custom Units";
                finishUnitInitialization();
                ARMARX_IMPORTANT << "Finishing setting up custom Units...DONE";
                ARMARX_INFO << "Sleeping a moment to let everything settle in";
                usleep(500000);
                initializationDone = true;
            }
            catch (...)
            {
                ARMARX_FATAL << "Shutting down - RobotUnit Init Thread caused an uncaught exception:\n" << GetHandledExceptionString();
                initializationFailed = true;

            }
        }
    };
    unitInitTask.detach();
    if (initializationFailed)
    {
        return;
    }
    if (busStartSucceeded)
    {
        elevateThreadPriority(RT_THREAD_PRIORITY);
        //        set_latency_target();

        //setting the timestamps for the pdo update, at the moment we only have on
        currentPDOUpdateTimestamp = TimeUtil::GetTime();
        CycleUtil cycleStats(IceUtil::Time::microSeconds(rtLoopTime));
        cycleStats.setBusyWaitShare(0.1);
        while (!initializationDone)
        {
            //            auto realDeltaT = currentPDOUpdateTimestamp - lastPDOUpdateTimestamp;
            //            auto cappedDeltaT = IceUtil::Time::microSeconds(boost::algorithm::clamp(realDeltaT.toMicroSeconds(), 1, 2000)); // TODO: Make property
            bus.updateBus(false);
            //            rtReadSensorDeviceValues(currentPDOUpdateTimestamp, cappedDeltaT);
            //            lastPDOUpdateTimestamp = currentPDOUpdateTimestamp;
            //            currentPDOUpdateTimestamp = TimeUtil::GetTime();
            cycleStats.waitForCycleDuration();
            ARMARX_INFO << deactivateSpam(1) << "Waiting for unit initialization!";
        }
        ARMARX_IMPORTANT << "RobotUnit is now ready";
    }
    else
    {

        if (!busStartSucceeded && getProperty<bool>("StartEtherCATBus").getValue())
        {
            ARMARX_WARNING << "Bus was not started!";
        }
    }

    ARMARX_DEBUG << "Setting up gravity calculation";
    // init data structs for gravity calculation
    for (size_t i = 0; i < rtRobotJointSet->getSize(); i++)
    {
        auto selectedJoint = getSensorDevice(rtRobotJointSet->getNode(i)->getName());
        if (selectedJoint)
        {
            auto sensorValue = const_cast<SensorValue1DoFGravityTorque*>(selectedJoint->getSensorValue()->asA<SensorValue1DoFGravityTorque>());
            ARMARX_DEBUG << "will calculate gravity for robot node " << rtRobotJointSet->getNode(i)->getName();
            nodeJointDataVec.push_back(std::make_pair(rtRobotJointSet->getNode(i), sensorValue));
        }
        else
        {
            ARMARX_INFO << "Joint " << rtRobotJointSet->getNode(i)->getName() << " not found";
            nodeJointDataVec.push_back(std::make_pair(rtRobotJointSet->getNode(i), nullptr));
        }
    }

    //    unitInitTask.join();
    controlLoopRTThread();
    //    //switching off the bus and be sure that the bus will receive
    //    bus.switchBusOFF();
    //    bus.updateBus();

    while (getObjectScheduler() && !getObjectScheduler()->isTerminationRequested())
    {
        ARMARX_INFO << deactivateSpam() << "Waiting for shutdown";
        usleep(10000);
    }
}

void EtherCATRTUnit::icePropertiesInitialized()
{
    RobotUnit::icePropertiesInitialized();

}

MultiNodeRapidXMLReader EtherCATRTUnit::ReadHardwareConfigFile(std::string busConfigFilePath, std::string rootNodeName)
{
    if (!ArmarXDataPath::getAbsolutePath(busConfigFilePath, busConfigFilePath))
    {
        throw LocalException("could not find BusConfigFilePath: ") << busConfigFilePath;
    }
    ARMARX_INFO_S << "read the hw config from " << busConfigFilePath;

    //reading the config for the Bus and create all the robot objects in the robot container
    auto busConfigFilePathDir = std::filesystem::path(busConfigFilePath).parent_path();
    auto rapidXmlReaderPtr = RapidXmlReader::FromFile(busConfigFilePath);
    auto rootNode = rapidXmlReaderPtr->getRoot(rootNodeName.c_str());
    MultiNodeRapidXMLReader multiNode({rootNode});
    for (RapidXmlReaderNode& includeNode  : rootNode.nodes("include"))
    {
        auto relPath = includeNode.attribute_value("file");
        std::filesystem::path filepath = (busConfigFilePathDir / relPath);
        if (!std::filesystem::exists(filepath))
        {
            std::string absPath;
            if (!ArmarXDataPath::getAbsolutePath(relPath, absPath))
            {
                throw LocalException("Could not find config file at path ") << relPath;
            }
        }
        multiNode.addNode(RapidXmlReader::FromFile(filepath.string())->getRoot(rootNodeName.c_str()));
    }
    return multiNode;
}

bool EtherCATRTUnit::initBusRTThread()
{
    // init EtherCAT
    EtherCAT& bus = EtherCAT::getBus();
    bus.setSocketFileDescriptor(getProperty<int>("SocketFileDescriptor").getValue());
    bus.setIfName(getProperty<std::string>("EthercatInterfaceName"));
    if (!bus.switchBusON())
    {
        ARMARX_INFO << "Starting bus failed!! - closing\n";
        return false;
    }
    for (auto& ctrl : bus.getCtrlDevs())
    {
        addControlDevice(ctrl);
    }
    for (auto& sens : bus.getSensDevs())
    {
        addSensorDevice(sens);
    }
    ARMARX_INFO << "EtherCAT bus is started";
    return true;
}

void EtherCATRTUnit::controlLoopRTThread()
{
    EtherCAT& bus = EtherCAT::getBus();
    try
    {
        finishControlThreadInitialization();

        int pid = syscall(SYS_gettid);
        ARMARX_IMPORTANT << "pinning thread " << pid << " to cpu #0";
        cpu_set_t mask;
        CPU_ZERO(&mask);
        CPU_SET(0, &mask);
        int retval = sched_setaffinity(pid, sizeof(mask), &mask);
        //        int retval = system(("taskset -pc 0 " + to_string(pid)).c_str());
        if (retval != 0)
        {
            ARMARX_ERROR << "Failed to pin thread " << pid << " to cpu #0";
        }
        cpu_set_t mask2;
        CPU_ZERO(&mask2);
        CPU_SET(0, &mask2);
        sched_getaffinity(pid, sizeof(mask2), &mask2);
        ARMARX_INFO << "Thread Pinning after matches mask: " << CPU_EQUAL(&mask, &mask2);

        //bus.setControlLoopRunning(true);
        //        rtLoopStartTime = TimeUtil::GetTime();
        //to not get any strange start values
        currentPDOUpdateTimestamp = armarx::rtNow();
        CycleUtil cycleKeeper(IceUtil::Time::microSeconds(rtLoopTime));
        cycleKeeper.setBusyWaitShare(1.0f);
        ARMARX_CHECK_EXPRESSION(rtGetRobot());
        ARMARX_CHECK_EXPRESSION(rtRobotJointSet);
        ARMARX_CHECK_EXPRESSION(rtRobotBodySet);
        ARMARX_CHECK_EXPRESSION(rtRobotJointSet->getSize() > 0);
        VirtualRobot::Gravity gravity(rtGetRobot(), rtRobotJointSet, rtRobotBodySet);

        //#if 0
        std::vector<float> gravityValues(rtRobotJointSet->getSize());
        ARMARX_CHECK_EQUAL(rtRobotJointSet->getSize(), nodeJointDataVec.size());
        //#endif

        ARMARX_INFO << "RT control loop started";
        EmergencyStopState lastSoftwareEmergencyStopState = rtGetEmergencyStopState();

        //        rtLoopStartTime = TimeUtil::GetTime();
        auto lastMonoticTimestamp = armarx::rtNow();
        auto currentMonotonicTimestamp = lastMonoticTimestamp;
        currentPDOUpdateTimestamp = armarx::rtNow();

        IceUtil::Time startTimestamp = armarx::rtNow();

        for (; taskRunning; ++ _iterationCount)
        {
            const IceUtil::Time loopStartTime = armarx::rtNow();
            rtGetRTThreadTimingsSensorDevice().rtMarkRtLoopStart();

            if (checkErrorCountersOnStartupFlag && (loopStartTime - startTimestamp).toMilliSeconds() > checkErrorCountersOnStartupDelayMS)
            {
                checkErrorCountersOnStartupFlag = false;
                bus.readErrorCounters();
            }
            if (checkErrorCountersTriggerFlag)
            {
                checkErrorCountersTriggerFlag = false;
                bus.readErrorCounters();
            }

            auto realDeltaT = currentMonotonicTimestamp - lastMonoticTimestamp;
            auto cappedDeltaT = IceUtil::Time::microSeconds(boost::algorithm::clamp<long>(realDeltaT.toMicroSeconds(), 1, 2000)); // TODO: Make property
            //            TIMING_START(load);
            //            if (controllerLoaded)
            if (rtIsCalibrating())
            {
                rtCalibrateActivateControlers();
                rtSwitchControllerSetup(SwitchControllerMode::RTThread);
                rtResetAllTargets();
                _calibrationStatus = rtCalibrate();
                rtHandleInvalidTargets();
                rtRunJointControllers(currentPDOUpdateTimestamp, cappedDeltaT);
            }
            else
            {
                RT_TIMING_START(RunControllers);
                RT_TIMING_START(SwitchControllers);
                rtSwitchControllerSetup();
                RT_TIMING_CEND(SwitchControllers, 0.3 * rtWarningFactor);
                RT_TIMING_START(ResettingTargets);
                rtResetAllTargets();
                RT_TIMING_CEND(ResettingTargets, 0.3 * rtWarningFactor);
                RT_TIMING_START(RunNJointControllers);
                rtRunNJointControllers(currentPDOUpdateTimestamp, cappedDeltaT);
                RT_TIMING_CEND(RunNJointControllers, 0.3 * rtWarningFactor);
                RT_TIMING_START(CheckInvalidTargets);
                rtHandleInvalidTargets();
                RT_TIMING_CEND(CheckInvalidTargets, 0.3 * rtWarningFactor);

                RT_TIMING_START(RunJointControllers);
                rtRunJointControllers(currentPDOUpdateTimestamp, cappedDeltaT);
                RT_TIMING_CEND(RunJointControllers, 0.3 * rtWarningFactor);
                RT_TIMING_CEND(RunControllers, 0.3 * rtWarningFactor);
            }

            //bus update
            rtGetRTThreadTimingsSensorDevice().rtMarkRtBusSendReceiveStart();
            RT_TIMING_START(updateBus);
            currentPDOUpdateTimestamp = TimeUtil::GetTime();
            if (bus.isBusInOperationalMode())
            {
                // error correction
                auto currentSoftwareEmergencyStopState = rtGetEmergencyStopState();
                if (lastSoftwareEmergencyStopState == EmergencyStopState::eEmergencyStopActive && currentSoftwareEmergencyStopState == EmergencyStopState::eEmergencyStopInactive)
                {
                    //                    bus.rebootBus();
                }
                lastSoftwareEmergencyStopState = currentSoftwareEmergencyStopState;

                bus.updateBus();
                if (bus.isEmergencyStopActive())
                {
                    rtSetEmergencyStopState(EmergencyStopState::eEmergencyStopActive);
                }
            }
            RT_TIMING_CEND(updateBus, 0.7 * rtWarningFactor);

            rtGetRTThreadTimingsSensorDevice().rtMarkRtBusSendReceiveEnd();

            RT_TIMING_START(ReadSensorValues);
            rtReadSensorDeviceValues(currentPDOUpdateTimestamp, cappedDeltaT);
            RT_TIMING_CEND(ReadSensorValues, 0.7 * rtWarningFactor);
            lastMonoticTimestamp = currentMonotonicTimestamp;
            currentMonotonicTimestamp = armarx::rtNow();


            RT_TIMING_START(Publish);
            rtUpdateSensorAndControlBuffer(currentPDOUpdateTimestamp, cappedDeltaT);
            RT_TIMING_CEND(Publish, 0.15 * rtWarningFactor);

            RT_TIMING_START(ComputeGravityTorques);
            gravity.computeGravityTorque(gravityValues);
            size_t i = 0;
            for (auto& node : nodeJointDataVec)
            {
                auto torque = gravityValues.at(i);
                if (node.second)
                {
                    node.second->gravityTorque = -torque;
                }

                i++;
            }
            RT_TIMING_CEND(ComputeGravityTorques, 0.2 * rtWarningFactor);

            //            copyEtherCATBufferOut();

            rtGetRTThreadTimingsSensorDevice().rtMarkRtLoopPreSleep();

            const auto loopPreSleepTime = armarx::rtNow();
            RT_TIMING_START(RTLoopWaiting);
            cycleKeeper.waitForCycleDuration();
            RT_TIMING_CEND(RTLoopWaiting, rtLoopTime * 1.3 * rtWarningFactor);
            const auto loopPostSleepTime = armarx::rtNow();

            const auto loopDuration = armarx::rtNow() - loopStartTime;
            if (loopDuration.toMicroSeconds() > (rtLoopTime + 500) * rtWarningFactor)
            {
                const SensorValueRTThreadTimings* sval = rtGetRTThreadTimingsSensorDevice().getSensorValue()->asA<SensorValueRTThreadTimings>();
                ARMARX_CHECK_NOT_NULL(sval);
                ARMARX_WARNING << "RT loop is under 1kHz control frequency:\n"
                               << "-- cycle time PDO timestamp " << realDeltaT.toMicroSeconds() << " µs\n"
                               << "-- cycle time loop          " << loopDuration.toMicroSeconds() << " µs\n"
                               << "-- sleep                    " << (loopPostSleepTime - loopPreSleepTime).toMicroSecondsDouble() << " µs\n"

                               << "-- thread timing sensor value: \n"

                               << "---- rtSwitchControllerSetup        Duration " << sval->rtSwitchControllerSetupDuration.toMicroSecondsDouble() << " µs\t"
                               <<                               " RoundTripTime " << sval->rtSwitchControllerSetupRoundTripTime.toMicroSecondsDouble() << " µs\n"

                               << "---- rtRunNJointControllers         Duration " << sval->rtRunNJointControllersDuration.toMicroSecondsDouble() << " µs\t"
                               <<                               " RoundTripTime " << sval->rtRunNJointControllersRoundTripTime.toMicroSecondsDouble() << " µs\n"

                               << "---- rtHandleInvalidTargets         Duration " << sval->rtHandleInvalidTargetsDuration.toMicroSecondsDouble() << " µs\t"
                               <<                               " RoundTripTime " << sval->rtHandleInvalidTargetsRoundTripTime.toMicroSecondsDouble() << " µs\n"

                               << "---- rtRunJointControllers          Duration " << sval->rtRunJointControllersDuration.toMicroSecondsDouble() << " µs\t"
                               <<                               " RoundTripTime " << sval->rtRunJointControllersRoundTripTime.toMicroSecondsDouble() << " µs\n"

                               << "---- rtBusSendReceive               Duration " << sval->rtBusSendReceiveDuration.toMicroSecondsDouble() << " µs\t"
                               <<                               " RoundTripTime " << sval->rtBusSendReceiveRoundTripTime.toMicroSecondsDouble() << " µs\n"

                               << "---- rtReadSensorDeviceValues       Duration " << sval->rtReadSensorDeviceValuesDuration.toMicroSecondsDouble() << " µs\t"
                               <<                               " RoundTripTime " << sval->rtReadSensorDeviceValuesRoundTripTime.toMicroSecondsDouble() << " µs\n"

                               << "---- rtUpdateSensorAndControlBuffer Duration " << sval->rtUpdateSensorAndControlBufferDuration.toMicroSecondsDouble() << " µs\t"
                               <<                               " RoundTripTime " << sval->rtUpdateSensorAndControlBufferRoundTripTime.toMicroSecondsDouble() << " µs\n"

                               << "---- rtResetAllTargets              Duration " << sval->rtResetAllTargetsDuration.toMicroSecondsDouble() << " µs\t"
                               <<                               " RoundTripTime " << sval->rtResetAllTargetsRoundTripTime.toMicroSecondsDouble() << " µs\n"

                               << "---- rtLoop                         Duration " << sval->rtLoopDuration.toMicroSecondsDouble() << " µs\t"
                               <<                               " RoundTripTime " << sval->rtLoopRoundTripTime.toMicroSecondsDouble() << " µs\n";
            }
        }
        ARMARX_IMPORTANT << "RT loop stopped";
        ARMARX_INFO << "Execution stats: Average: " << cycleKeeper.getAverageDuration().toMilliSecondsDouble()
                    << " max: " << cycleKeeper.getMaximumDuration().toMilliSecondsDouble()
                    << " min: " << cycleKeeper.getMinimumDuration().toMilliSecondsDouble();
        //switching off the bus and be sure that the bus will receive

    }
    catch (UserException& e)
    {
        ARMARX_FATAL << "exception in control thread!"
                     << "\nwhat:\n" << e.what()
                     << "\nreason:\n" << e.reason
                     << "\n\tname: " << e.ice_id()
                     << "\n\tfile: " << e.ice_file()
                     << "\n\tline: " << e.ice_line()
                     << "\n\tstack: " << e.ice_stackTrace()
                     << std::flush;
        //TODO emergency stop
    }
    catch (Ice::Exception& e)
    {
        ARMARX_FATAL << "exception in control thread!\nwhat:\n"
                     << e.what()
                     << "\n\tname: " << e.ice_id()
                     << "\n\tfile: " << e.ice_file()
                     << "\n\tline: " << e.ice_line()
                     << "\n\tstack: " << e.ice_stackTrace()
                     << std::flush;
        //TODO emergency stop
    }
    catch (std::exception& e)
    {
        ARMARX_FATAL << "exception in control thread!\nwhat:\n" << e.what() << std::flush;
        //TODO emergency stop
    }
    catch (...)
    {
        ARMARX_FATAL << "exception in control thread!" << std::flush;
        //TODO emergency stop
    }
    ARMARX_INFO << "Leaving control loop function";
}

DeviceContainerPtr EtherCATRTUnit::getDeviceContainerPtr() const
{
    return deviceContainerPtr;
}

void EtherCATRTUnit::setDeviceContainerPtr(const DeviceContainerPtr& value)
{
    deviceContainerPtr = value;
}



void EtherCATRTUnit::elevateThreadPriority(int priority)
{
    int pid = syscall(SYS_gettid);
    ARMARX_INFO << "Priority before: " << sched_getscheduler(pid);
    struct sched_param param;
    param.sched_priority = priority;
    if (sched_setscheduler(LogSender::getThreadId(), SCHED_FIFO | SCHED_RESET_ON_FORK, &param) == -1)
    {
        ARMARX_WARNING << ("sched_setscheduler failed");
        //::exit(-1);
    }
    ARMARX_INFO << "Priority: " << sched_getscheduler(pid);

}

/* Latency trick
 * if the file /dev/cpu_dma_latency exists,
 * open it and write a zero into it. This will tell
 * the power management system not to transition to
 * a high cstate (in fact, the system acts like idle=poll)
 * When the fd to /dev/cpu_dma_latency is closed, the behavior
 * goes back to the system default.
 *
 * Taken from rt-tests.
 */
void EtherCATRTUnit::set_latency_target(int32_t latency_target_value)
{

    struct stat s;
    int err;
    errno = 0;
    err = stat("/dev/cpu_dma_latency", &s);
    if (err == -1)
    {
        ARMARX_WARNING << "WARN: stat /dev/cpu_dma_latency failed";
        return;
    }
    errno = 0;
    latency_target_fd = open("/dev/cpu_dma_latency", O_RDWR);
    if (latency_target_fd == -1)
    {
        ARMARX_WARNING << "WARN: open /dev/cpu_dma_latency failed: " << strerror(errno);
        return;
    }
    errno = 0;
    err = write(latency_target_fd, &latency_target_value, 4);
    if (err < 1)
    {
        ARMARX_WARNING << "# error setting cpu_dma_latency to latency_target_value!";
        close(latency_target_fd);
        return;
    }
    ARMARX_INFO <<  "# /dev/cpu_dma_latency set to " << latency_target_value << " µs\n";
}



