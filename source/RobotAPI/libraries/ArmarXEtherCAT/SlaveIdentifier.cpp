/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SlaveIdentifier.h"

using namespace armarx;

SlaveIdentifier::SlaveIdentifier(const RapidXmlReaderNode& node)
{
    ARMARX_TRACE;
    ARMARX_DEBUG << "node valid " << node.is_valid()
                 << "\npaths: " << node.getChildPaths();
    ARMARX_CHECK_EXPRESSION(
        (
            node.has_node("VendorID") &&
            node.has_node("ProductID") &&
            node.has_node("Serial")
        ) ||
        node.has_node("Identifier")
    );
    if (node.has_node("Identifier"))
    {
        const auto inode = node.first_node("Identifier");
        ARMARX_CHECK_EXPRESSION(
            inode.has_node("VendorID") &&
            inode.has_node("ProductID") &&
            inode.has_node("Serial")
        ) << "paths: " << inode.getChildPaths();
        VendorID  = inode.first_node("VendorID").value_as_uint32();
        ProductID = inode.first_node("ProductID").value_as_uint32();
        Serial    = inode.first_node("Serial").value_as_uint32();
    }
    else
    {
        VendorID  = node.first_node("VendorID").value_as_uint32();
        ProductID = node.first_node("ProductID").value_as_uint32();
        Serial    = node.first_node("Serial").value_as_uint32();
    }
}

SlaveIdentifier::SlaveIdentifier(uint32_t VendorID, uint32_t ProductID, uint32_t Serial, const std::string& humanName)
    : VendorID(VendorID), ProductID(ProductID), Serial(Serial), humanName(humanName)
{

}
