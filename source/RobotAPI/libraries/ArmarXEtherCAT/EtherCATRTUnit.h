/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include <array>
#include <thread>

//EVAL some stuff for logging
#include <sstream>
#include <fstream>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/util/TripleBuffer.h>
#include <ArmarXCore/core/rapidxml/wrapper/MultiNodeRapidXMLReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/components/units/SensorActorUnit.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include "ArmarXEtherCATFwd.h"

#define RT_THREAD_PRIORITY (49) /* we use 49 as the PRREMPT_RT use 50
                            as the priority of kernel tasklets
                            and interrupt handler by default */


namespace armarx
{

    /**
     * @class EtherCATRTUnitPropertyDefinitions
     * @brief
     */
    class EtherCATRTUnitPropertyDefinitions:
        public armarx::RobotUnitPropertyDefinitions
    {
    public:
        EtherCATRTUnitPropertyDefinitions(std::string prefix):
            armarx::RobotUnitPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
            defineRequiredProperty<std::string>("BusConfigFilePath", "Location of the BusConfigFile");
            defineOptionalProperty<int>("SocketFileDescriptor", 777, "Socketfiledescriptor on which the ethercat connection is running");
            defineOptionalProperty<std::string>("EthercatInterfaceName", "", "Name of the ethercat socket. If set to non-empty string, this will be used over SocketFileDescriptor");
            defineOptionalProperty<bool>("StartEtherCATBus", true, "Whether or not to start the EtherCAT Bus. Useful if only the Dynamixels should be used.");

            defineOptionalProperty<bool>("UseTorqueVelocityModeAsDefault", false, "If true, the KinematicUnit will use TorqueVelocity mode for velocity mode");
            defineOptionalProperty<int>("RTLoopFrequency", 1000, "Desired frequency of real-time control loop");
            defineOptionalProperty<float>("RTLoopTimingCheckToleranceFactor", 1.0f, "Factor by which the timing checks are multiplied. Higher value -> less warning outputs");
            defineOptionalProperty<bool>("checkErrorCountersOnEtherCATError", false, "If true, the EtherCAT bus will be checked for receive errors on bus error. This is slow; it should not be active in normal usage.", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<bool>("checkErrorCountersOnStartup", false, "If true all error counter registers are read on bus startup.");
            defineOptionalProperty<long>("checkErrorCountersOnStartupDelayMS", 1000, "Delay for the initial error counter check in ms.");


            defineOptionalProperty<bool>("VisualizeTorques", false, "If true, EtherCATRTUnit::publish will draw joint torques on the debug drawer");
        }
    };

    /**
     * @defgroup Component-EtherCATRTUnit EtherCATRTUnit
     * @ingroup RobotAPI-Components
     * A description of the component EtherCATRTUnit.
     *
     * @class EtherCATRTUnit
     * @ingroup Component-EtherCATRTUnit
     * @brief Brief description of class EtherCATRTUnit.
     *
     * Detailed description of class EtherCATRTUnit.
     */
    class EtherCATRTUnit :
        virtual public Logging,
        virtual public RobotUnit
    {
    public:
        EtherCATRTUnit();
        ~EtherCATRTUnit() override;

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "EtherCATRTUnit";
        }


        void elevateThreadPriority(int priority);
        DeviceContainerPtr getDeviceContainerPtr() const;

    protected:
        void setDeviceContainerPtr(const DeviceContainerPtr& value);

        void onInitRobotUnit() override;
        void onConnectRobotUnit() override;
        void onDisconnectRobotUnit() override;
        void onExitRobotUnit() override;

        void initializeKinematicUnit() override;

        void joinControlThread() override;

        void publish(armarx::StringVariantBaseMap debugObserverMap = {}, armarx::StringVariantBaseMap timingMap = {}) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
        void icePropertiesInitialized() override;

        void componentPropertiesUpdated(const std::set<std::string>& changedProperties) override;

    protected:
        static MultiNodeRapidXMLReader ReadHardwareConfigFile(std::string hardwareConfigFilePath, std::string rootNodeName);

        //all the stuff to run the rt Thread
        void startRTThread();

        //        void stopRTThread();

        /** the run method of the rt thread */
        virtual void rtRun();

        bool initBusRTThread();

        void controlLoopRTThread();

        enum class CalibrationStatus
        {
            Calibrating, Done
        };
        /**
         * @brief Allows to switch controllers while calibrating
         *
         * use
         * rtSetJointController(JointController* c, std::size_t index)
         * to switch controllers
         */
        virtual void rtCalibrateActivateControlers()
        {
        }
        /**
         * @brief Hook to add calibration code
         *
         * This function is called in the rt loop! So you should not take too long!
         *
         * read sensors and write targets
         * while calibrating return CalibrationStatus::Calibrating
         * if done return CalibrationStatus::Done
         *
         * @return Whether done or still calibrating
         */
        virtual CalibrationStatus rtCalibrate()
        {
            return CalibrationStatus::Done;
        }
        bool rtIsCalibrating() const
        {
            return _calibrationStatus == CalibrationStatus::Calibrating;
        }
        std::uintmax_t getIterationCount()
        {
            return _iterationCount;
        }
    private:
        CalibrationStatus _calibrationStatus = CalibrationStatus::Calibrating;
        std::atomic_uintmax_t _iterationCount = 0;
    protected:

        void computeInertiaTorque();
        DebugDrawerInterfacePrx dd;

        std::thread rtTask;
        std::atomic_bool taskRunning  {false};
        std::atomic_int rtLoopTime{1000};
        float rtWarningFactor{1};


        //timestamps for the pdo updates
        IceUtil::Time currentPDOUpdateTimestamp;

        VirtualRobot::RobotPtr publishRobot;
        DeviceContainerPtr deviceContainerPtr;

        VirtualRobot::RobotNodeSetPtr rtRobotJointSet, rtRobotBodySet;
        std::vector<std::pair<VirtualRobot::RobotNodePtr, SensorValue1DoFGravityTorque*>> nodeJointDataVec;
        int latency_target_fd = -1;

        void set_latency_target(int32_t latency_target_value = 0);

        IceUtil::Time getControlThreadTargetPeriod() const override
        {
            return IceUtil::Time::microSeconds(rtLoopTime);
        }

        std::atomic_bool checkErrorCountersTriggerFlag = false;
        std::atomic_bool checkErrorCountersOnStartupFlag = false;
        std::atomic_long checkErrorCountersOnStartupDelayMS = 0;
    };
}


