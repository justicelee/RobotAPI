/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/components/units/RobotUnit/util/RtTiming.h>

namespace armarx
{
    template<typename T>
    class LinearConvertedValue
    {
    public:
        LinearConvertedValue()
        {
            raw = nullptr;
            offset = factor = 0;
            this->offsetBeforeFactor = true;
        }

        /**
         * @brief init
         * @param raw
         * @param node
         * @param defaultValue
         * @param offsetBeforeFactor if true the offset is added before multiplying with factor. If false: the other way around.
         * @param nameForDebugging This name is printend in case an error is encountered (Its only purpose is to ease debugging)
         */
        void init(T* raw, const DefaultRapidXmlReaderNode& node, float defaultValue = std::nan("1"), bool offsetBeforeFactor = true, const char* nameForDebugging = "")
        {
            float factor = node.attribute_as_float("factor");
            float offset = node.attribute_as_float("offset");
            init(raw, factor, offset, defaultValue, offsetBeforeFactor, nameForDebugging);
        }

        void init(T* raw, float factor, float offset, float defaultValue = std::nan("1"), bool offsetBeforeFactor = true, const char* nameForDebugging = "")
        {
            const auto rawAsInt = reinterpret_cast<std::uint64_t>(raw);
            ARMARX_CHECK_EXPRESSION(
                (rawAsInt % alignof(T)) == 0) <<
                                              "\nThe alignment is wrong!\nIt has to be "
                                              << alignof(T) << ", but the data is aligned with "
                                              << rawAsInt % alignof(std::max_align_t)
                                              << "!\nThis is an offset of " << (rawAsInt % alignof(T))
                                              << " bytes!\nThe datatype is " << GetTypeString<T>()
                                              << "\nIts size is " << sizeof(T)
                                              << "\nraw = " << raw
                                              << " bytes\nThe name is " << nameForDebugging;
            this->offsetBeforeFactor = offsetBeforeFactor;
            this->factor = factor;
            this->offset = offset;
            this->raw = raw;
            if (!std::isnan(defaultValue))
            {
                value = defaultValue;
                write();
            }
            else
            {
                value = 0;
                read();
            }
        }

        void read()
        {
            if (offsetBeforeFactor)
            {
                value = ((*raw) + offset) * factor;
            }
            else
            {
                value = (*raw) * factor + offset;
            }
        }

        void write()
        {
            if (offsetBeforeFactor)
            {
                *raw = (T)((value / factor) - offset);
            }
            else
            {
                *raw = (T)((value) - offset) / factor;
            }
        }

        float value;

        T getRaw() const
        {
            return *raw;
        }

        float getFactor() const
        {
            return factor;
        }

        float getOffset() const
        {
            return offset;
        }

        bool getOffsetBeforeFactor() const
        {
            return offsetBeforeFactor;
        }

    private:
        T* raw;
        float offset, factor;
        bool offsetBeforeFactor ;
    };

    class AbstractData;
    using AbstractDataPtr = std::shared_ptr<AbstractData>;


    class AbstractData
    {
    public:
        AbstractData();
        virtual ~AbstractData();
        virtual void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) = 0;
        virtual void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) = 0;

    private:
    };

}

