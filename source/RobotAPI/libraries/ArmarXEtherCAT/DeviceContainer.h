/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once
#include <vector>
#include <memory>
#include <VirtualRobot/VirtualRobot.h>

namespace armarx
{
    class MultiNodeRapidXMLReader;
    using AbstractFunctionalDevicePtr = std::shared_ptr<class AbstractFunctionalDevice>;

    class DeviceContainer
    {
    public:
        size_t load(const MultiNodeRapidXMLReader& rootNodeConfigs, const VirtualRobot::RobotPtr& robot);
        template <typename Type>
        std::vector<std::shared_ptr<Type>> getDevicesOfType() const
        {
            std::vector<std::shared_ptr<Type>> results;
            for (auto& dev : devices)
            {
                auto castedDev = std::dynamic_pointer_cast<Type>(dev);
                if (castedDev)
                {
                    results.push_back(castedDev);
                }

            }
            return results;
        }
        std::vector<AbstractFunctionalDevicePtr> getAllInitializedFunctionalDevices() const;
        std::vector<AbstractFunctionalDevicePtr> getAllUninitializedFunctionalDevices() const;
        virtual std::vector<AbstractFunctionalDevicePtr> getAllFunctionalDevices() const;
    protected:
        std::vector<AbstractFunctionalDevicePtr> devices;
    };



} // namespace armarx

