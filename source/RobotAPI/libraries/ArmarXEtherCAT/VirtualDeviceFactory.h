/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/system/AbstractFactoryMethod.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Robot.h>


namespace armarx
{
    using AbstractFunctionalDevicePtr = std::shared_ptr<class AbstractFunctionalDevice>;

    using VirtualDeviceFactoryArgs = std::tuple<RapidXmlReaderNode,  armarx::DefaultRapidXmlReaderNode, VirtualRobot::RobotPtr>;

    class VirtualDeviceFactory :
        public AbstractFactoryMethod<VirtualDeviceFactory, VirtualDeviceFactoryArgs, AbstractFunctionalDevicePtr>
    {
    public:
        template <typename ObjectType>
        static VirtualDeviceFactory::SharedPointerType createInstance(VirtualDeviceFactoryArgs args)
        {
            return VirtualDeviceFactory::SharedPointerType(std::make_shared<ObjectType>(std::get<0>(args), std::get<1>(args), std::get<2>(args)));
        }
    };


}
