/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

namespace armarx
{
    class SlaveIdentifier;
    using SlaveIdentifierPtr = std::shared_ptr<SlaveIdentifier>;

    class SlaveIdentifier
    {
    public:
        SlaveIdentifier(const RapidXmlReaderNode& node);
        SlaveIdentifier(uint32_t VendorID, uint32_t ProductID, uint32_t Serial, const std::string& humanName);

        uint32_t VendorID;
        uint32_t ProductID;
        uint32_t Serial;
        std::string humanName;

        friend std::ostream& operator<<(std::ostream& stream, const SlaveIdentifier& rhs)
        {
            stream << "Name: " << rhs.humanName << " Product ID: " << rhs.ProductID << " Serial: " << rhs.Serial << " VendorID: " << rhs.VendorID;
            return stream;
        }
    };

    class DXLIdentifier
    {
    public:
        DXLIdentifier(const RapidXmlReaderNode& node)
        {
            dxlID = node.first_node("DynamixelID").value_as_uint32();
        }

        DXLIdentifier(uint8_t dxl_id, const std::string& humanName) :
            dxlID(dxl_id),
            humanName(humanName)
        {

        }

        uint8_t dxlID;
        std::string humanName;


        friend std::ostream& operator<<(std::ostream& stream, const DXLIdentifier& rhs)
        {
            stream << "Name: " << rhs.humanName << " dxlID: " << (int)rhs.dxlID;
            return stream;
        }
    };
}

