//
// Created by swarowsky on 21.12.16.
//

#include "AbstractSlave.h"
#include "EtherCAT.h"

using namespace armarx;

AbstractSlave::AbstractSlave(const SlaveIdentifier slaveIdentifier, uint16_t slaveNumber)
    : slaveIdentifier(slaveIdentifier), slaveNumber(slaveNumber)
{

}


///**
// * Returns the Vendor ID of the slave
// * @return the vendor id of the slave
// */
//uint32_t AbstractSlave::getVendorID() const
//{
//    return vendorID;
//}

/**
 * This returns the slave number of the slave on the bus +1 because slave 0 is the master
 * @return index in ec_slave array
 */
uint16_t AbstractSlave::getSlaveNumber() const
{
    return slaveNumber;
}

//uint32_t AbstractSlave::getSerialNumber() const
//{
//    return serialNumber;
//}

bool AbstractSlave::handleErrors()
{
    bool retVal;
    retVal = !hasError();
    return retVal;
}

const SlaveIdentifier& AbstractSlave::getSlaveIdentifier() const
{
    return slaveIdentifier;
}


