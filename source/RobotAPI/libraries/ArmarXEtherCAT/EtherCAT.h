#pragma once
#include "ArmarXEtherCATFwd.h"

#include "AbstractFunctionalDevice.h"

#include <IceUtil/Time.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <sstream>
#include <atomic>
#include <mutex>


/**
 * The actual size of the mapped prozess image will be smaller but with 4096 we are quite
 * confident that we will have enough space
 */
#define IOmapSize 4096

/**  master -> slave */
#define RX_MAPPING_INDEX 0x1C12
/**  slave -> master */
#define TX_MAPPING_INDEX 0x1C13

/** The ESC of the EtherCAT Hub just give a device type not much more so we identify them via der device type an ingnore them */
#define ETHTERCAT_HUB_DEVICE_TYPE 0x1

/// This defenitions are to make the switching on and off of the bus clear
#define ON true
#define OFF false

//DEBUG
//#define RTTIME_TEST


namespace armarx
{
    using ControlDevicePtr = std::shared_ptr<class ControlDevice>;
    using SensorDevicePtr = std::shared_ptr<class SensorDevice>;


    class RobotUnit;
    class EtherCAT
    {
    public:
        static EtherCAT& getBus();

        void setDeviceContainerPtr(const DeviceContainerPtr& deviceContainerPtr);

        void setMainUnitPtr(RobotUnit* mainUnitPtr);

        void setSocketFileDescriptor(int socketFileDescriptor);

        void setIfName(const std::string& ifname);

        bool getVendorID(uint16_t slave, uint32_t& vendorID) const;

        /**
         * With this method one can check if the bus is started to operational mode.
         * This means you can use PDO's if bus is not in Operational mode pdo's not avaliable and can cause segmentation faults
         * @return true if control loop runs
         */
        bool isBusInOperationalMode();

        /**
         * With this method on can check if bus hat already mapped the PDO
         * @return
         */
        bool isPDOMapped() const;

        bool updateBus(bool doErrorHandling = true);

        void switchBusOFF();

        bool switchBusON();

        void deactivateCOECA(AbstractSlave* slave);

        void* getIOMap() const;
        std::array<char, IOmapSize>& getIOMapBuffer();

        int getMappedSize();
        AbstractSlavePtr getSlaveAtIndex(uint16_t slaveIndex) const;
        std::vector<AbstractSlavePtr> getSlaves();

        bool getProductCode(uint16_t slave, uint32_t& productCode) const;

        bool getSerialNumber(uint16_t slave, uint32_t& serialNumber) const;

        /**
         * This indicates if there is an error on the bus, maybe a slaved died or someting else, most times it
         * would be the best way to shut down the robot. But this is only an error, we are still able to communicate with
         * the bus. For a complete fail there is the indication isBusDead
         * @see EtherCAT::getIsBusDead()
         * @return TURE if there is a error on the bus
         */
        const std::atomic_bool& getError() const;

        /**
         * This indicates if the bus is completly dead, may power was switched off.
         * For just getting errors on the bus ther is the indication error
         * @see EtherCAT::getError()
         * @return TRUE if the bus is noch reachable anymore
         */
        const std::atomic_bool& getIsBusDead() const;

        //read and writes
        bool writeByteBuffer(unsigned char* buf, int buflen, uint16_t slave, uint16_t index, uint8_t subindex,
                             bool CompleteAccess = false);

        bool writeSDO(uint16_t slave, int8_t value, uint16_t index, uint8_t subindex, bool CompleteAccess = false);

        bool writeSDO(uint16_t slave, uint8_t value, uint16_t index, uint8_t subindex, bool CompleteAccess = false);

        bool writeSDO(uint16_t slave, uint16_t value, uint16_t index, uint8_t subindex, bool CompleteAccess = false);

        bool writeSDO(uint16_t slave, uint32_t value, uint16_t index, uint8_t subindex, bool CompleteAccess = false);

        bool writeSDO(uint16_t slave, int32_t value, uint16_t index, uint8_t subindex, bool CompleteAccess = false);

        bool readByteBuffer(uint16_t slave, uint16_t index, uint8_t subindex, unsigned  char* buf, int buflen, bool CompleteAccess = false);

        bool readSDO(uint16_t slave, uint16_t index, uint8_t subindex, uint8_t& value) const;

        bool readSDO(uint16_t slave, uint16_t index, uint8_t subindex, int8_t& value) const;

        bool readSDO(uint16_t slave, uint16_t index, uint8_t subindex, uint16_t& value, bool CompleteAccess = false) const;

        bool readSDO(uint16_t slave, uint16_t index, uint8_t subindex, int16_t& value) const;

        bool readSDO(uint16_t slave, uint16_t index, uint8_t subindex, int32_t& value) const;

        bool readSDO(uint16_t slave, uint16_t index, uint8_t subindex, uint32_t& value) const;

        bool goToSafeOp();
        static std::string EtherCATStateToString(uint16_t state);
        bool isEmergencyStopActive() const;

        bool rebootBus();

        static const char* EC_StateToString(uint16_t state);
        static const char* EC_ErrorTypeToString(uint16_t errorType);

        std::vector<ControlDevicePtr> getCtrlDevs() const;

        std::vector<SensorDevicePtr> getSensDevs() const;

        bool getCheckErrorCountersOnWKCError() const;
        void setCheckErrorCountersOnWKCError(bool value);

        void readErrorCounters();
    protected:

        int ecx_APRD_with_error_handling(uint16_t ADP, uint16_t ADO, uint16_t length, void* data, int timeout, uint16_t slaveIndex, const std::string& name, int port);
    private:
        //Hiding the constructor to avoid illegal creation of the Bus (singelton pattern)
        EtherCAT();
        ~EtherCAT();

        //avoid coping the object (singelton pattern)
        EtherCAT(const EtherCAT&);
        EtherCAT& operator=(const EtherCAT&);


        //starting and closing the bus should only be done via the switch on an off methods


        bool startBus(bool createDevices);
        void closeBus();

        void updatePDO();

        bool generalSDOWrite(uint16_t slave, uint16_t index, uint8_t subindex, int buflen, void* buf, bool ca);

        bool generalSDORead(uint16_t slave, uint16_t index, uint8_t subindex, int buflen, void* buf, bool ca = false) const;


        bool busShouldBeRunning() const;

        std::pair<std::vector<ControlDevicePtr>, std::vector<SensorDevicePtr> > createDevices();

        bool setPDOMappings();

        void printALStatusError(uint16_t slave);

        void errorHandling();


        std::vector<ControlDevicePtr> ctrlDevs;
        std::vector<SensorDevicePtr> sensDevs;


        //members
        /** the expected working counter that will be calculated at the bus initialisation */
        int expectedWKC;
        /** this indicates if the EtherCAT bus is on, will change to FALSE when the modul stopps */
        std::atomic_bool isSDOWorking;
        //! Socketfiledescriptor on which the ethercat connection is running
        int socketFileDescriptor = -1;
        /** @brief IOmap the IO map where the process data are mapped in */
        alignas(alignof(std::max_align_t)) std::array<char, IOmapSize> IOmap;
        /**  @brief switchON_OFF this flag is used to switch the bus off an close it */
        std::atomic_bool switchON_OFF;
        /** current Bus group we are working on */
        int currentGroup;
        /** indicates if there is an Error */
        std::atomic_bool error;

        /** List of all Slaves */
        std::vector<AbstractSlavePtr> slaves;
        /** flag indicates if the bus is started complete and all slaves are in EtherOP Mode*/
        std::atomic_bool busInOperationalMode;
        /** the working counter from the last transmission */
        int lastWorkCounter;
        int noErrorIterations = 0;
        bool firstErrorCheck = true;

        /** List of all functional/internal Units */
        std::vector<AbstractFunctionalDevicePtr> functionalDevices;


        int actualMappedSize;

        std::atomic_bool checkErrorCountersOnWKCError;

        /** Indicates if the bus got broken and is not recoverable, so we don't need to switch it down correct it already went away...*/
        std::atomic_bool isBusDead;

        std::atomic_bool pdoMapped;

        DeviceContainerPtr deviceContainerPtr;
        RobotUnit* mainUnitPtr;

        std::string ifname;
        mutable std::mutex etherCatMutex;
        bool emergencyStopWasActivated = false;
        IceUtil::Time busUpdateLastUpdateTime;
        IceUtil::Time ermergencyStopRecoverStartpoint;


    };



}

