﻿#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

#pragma GCC diagnostic pop

#include <ethercat.h>
#include <ctime>
#include <chrono>
#include <thread>
#include <iomanip>

#include <ArmarXCore/core/logging/Logging.h>
//#include <Armar6RT/units/Armar6Unit/FunctionalDevices/Joint.h>
//#include "Armar6RT/units/Armar6Unit/BusSlaves/SensorBoard.h"
//#include "Armar6RT/units/Armar6Unit/BusSlaves/Elmo.h"
//#include "Armar6RT/units/Armar6Unit/BusSlaves/FTSensorSlave.h"
//#include <Armar6RT/units/Armar6Unit/BusSlaves/EtherCATHub.h>
#include "EtherCAT.h"
#include <RobotAPI/components/units/RobotUnit/util/RtTiming.h>
//#include "RtRobotContainer.h"
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include "AbstractSlave.h"
#include "DeviceContainer.h"
#include "EtherCATDeviceFactory.h"
//include all soem stuff here so no one should be able to use soem functions unintentionally because he has to include some first
#define EC_VER1
#include <ethercattype.h>
#include <nicdrv.h>
#include <osal.h>
#include <osal_defs.h>
#include <ethercatmain.h>
#include <ethercatbase.h>
#include <ArmarXCore/core/util/OnScopeExit.h>
#include <ArmarXCore/util/CPPUtility/Iterator.h>

//EtherCAT definitions only used for the ethercat stuff
#define SDO_READ_TIMEOUT 100000
#define SDO_WRITE_TIMEOUT 50000
#define EC_TIMEOUTMON 2000

using namespace armarx;




#define DEFAULT_ECAT_GROUP 0


/**
 * This returns the one and only Bus object.
 * An implements the singelton pattern
 * @return The Bus instance
 */
EtherCAT& EtherCAT::getBus()
{
    static EtherCAT _instance;
    return _instance;
}

/**
 * default constructor, is privat an only be used from EtherCAT::getBus(), because of singelton.
 * @see EtherCAT::getBus()
 */
EtherCAT::EtherCAT() : expectedWKC(-1),
    isSDOWorking(false),
    switchON_OFF(OFF),
    currentGroup(DEFAULT_ECAT_GROUP),
    error(false),
    busInOperationalMode(false),
    lastWorkCounter(0),
    functionalDevices(),
    actualMappedSize(0),
    isBusDead(false),
    pdoMapped(false),
    deviceContainerPtr(nullptr),
    mainUnitPtr(nullptr)
{
    checkErrorCountersOnWKCError = false;
    //writing zeros to the IOMap
    for (size_t i = 0; i < IOmapSize; ++i)
    {
        IOmap[i] = 0;
    }
    //for the start we don't need the recovery part to be working
    ec_group[currentGroup].docheckstate = FALSE;
}

/**
 * This starts the bus by connection to the socket and initialize all the slaves, you will not be able to use the bus without calling this method before.
 * @param [IN] ifname the socket the bus should connect to
 * @return true if the bus could be started, false if something went wrong
 */
bool EtherCAT::startBus(bool createDevices)
{
    ARMARX_TRACE;
    //if the bus runs already do nothing
    if (isSDOWorking)
    {
        return true;
    }

    if (socketFileDescriptor == -1)
    {
        ARMARX_WARNING << "socketFileDescriptor is -1 - did you forget to set it?";
        error = true;
        return false;
    }

    if (!ifname.empty())
    {
        ARMARX_IMPORTANT << "ec_init(" << ifname << ")";
        if (!ec_init(ifname.c_str()))
        {
            ARMARX_WARNING << "Could not init etherCAT on " << ifname << "\nExecute as root\n";
            error = true;
            //end here there is nothing we can do
            return false;
        }
    }
    else if (socketFileDescriptor != -1)
    {
        ARMARX_INFO << "Using socketFileDescriptor " << socketFileDescriptor << " to open raw socket";
        //initialise SOEM, open socket
        if (!ec_init_wsock(socketFileDescriptor))
        {
            ARMARX_WARNING << "No socket connection on " << socketFileDescriptor << "\nExecute as root\n";
            error = true;
            //end here there is nothing we can do
            return false;
        }
    }
    else
    {
        ARMARX_WARNING << "Either socketFileDescriptor or ifname need to be set";
        error = true;
        //end here there is nothing we can do
        return false;
    }


    //We succeed
    ARMARX_INFO << "Started SOEM with socketFileDescriptor: " << socketFileDescriptor << std::endl;

    //config Bus to switch to to Pre-OP
    if (ec_config_init(FALSE) <= 0)
    {
        ARMARX_WARNING << "No slaves found! - close socket\n";
        //stop soem
        ec_close();
        isSDOWorking = false;
        return false;
    }

    ARMARX_TRACE;
    //wait to be sure
    osal_usleep(500000);
    //we are up and running for SDO's
    isSDOWorking = true;
    //slaves should be in PreOp mode now
    ARMARX_INFO << ec_slavecount << " slaves found and set to PreOp";

    std::vector<ControlDevicePtr> ctrlDevs;
    std::vector<SensorDevicePtr> sensDevs;

    ////prepare Devices to be read to switch to Safe-Op
    if (createDevices)
    {
        if (slaves.size() > 0)
        {
            ARMARX_ERROR << "Devices are already created - stopping starting bus";
            return false;
        }
        std::tie(ctrlDevs, sensDevs) = this->createDevices();

        if (slaves.size() < 1)
        {
            ARMARX_WARNING << "There are no usable devices on the bus!";
            return false;
        }
        ARMARX_INFO << "Devices where created";
        for (std::shared_ptr<AbstractSlave>& slave : slaves)
        {
            slave->doMappings();
        }
    }
    else if (slaves.size() < 1)
    {
        ARMARX_ERROR << "No Slaves configured - stopping bus start up";
        return false;
    }




    ARMARX_TRACE;
    for (auto slave : slaves)
    {
        slave->prepareForSafeOp();
    }
    ARMARX_INFO << "Finishing Preparing for safe op now";
    for (auto slave : slaves)
    {
        slave->finishPreparingForSafeOp();
    }

    osal_usleep(500000);

    ///// going to SAFE_OP
    //do the mapping
    ARMARX_INFO << "Mapping....";
    actualMappedSize = ec_config_map(IOmap.data());
    ARMARX_INFO << "Going to safe op now";
    //wait to get all slaves to SAFE-OP
    ec_statecheck(0, EC_STATE_SAFE_OP, EC_TIMEOUTSTATE * 4);
    ARMARX_INFO << "IOmapping done, size: " << actualMappedSize << " - all Slaves are in SAFE-OP now\n";

    //calculating Workcounter after mapping to have an error indication later
    expectedWKC = (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC;
    ARMARX_INFO << "Calculated workcounter: " << expectedWKC << std::endl;

    ///give the devices her mapping
    if (!setPDOMappings())
    {
        ARMARX_ERROR << "Couldn't map the PDO, may the the pc is under to much load. "
                     "Check if there are other performance hungry programs running.\n"
                     "Or just try to start again";
        return false;
    }

    ///give the slaves some time to prepare some stuff
    for (auto slave : slaves)
    {
        slave->prepareForOp();
        //        updateBus();
    }


    for (auto slave : slaves)
    {
        slave->finishPreparingForOp();
    }


    /// init functional devices
    functionalDevices = deviceContainerPtr->getAllInitializedFunctionalDevices();
    ARMARX_INFO << "Found " << functionalDevices.size() << " meta devices";
    /// setting the data pointer in the functional devices
    for (AbstractFunctionalDevicePtr& device : functionalDevices)
    {
        ARMARX_INFO << "init for device type '" << device->getClassName() << "'";
        device->initData();
    }

    this->ctrlDevs = ctrlDevs;
    this->sensDevs = sensDevs;


    pdoMapped = true;

    //// switching to OP-Mode
    ec_slave[0].state = EC_STATE_OPERATIONAL;
    //send one valid process data to make outputs in slaves happy
    ec_send_processdata();
    ec_receive_processdata(EC_TIMEOUTRET);

    ARMARX_TRACE;
    //request all slaves to transit to OP-Mode
    int stateRet = ec_writestate(0);
    if (stateRet == 1)
    {
        ARMARX_WARNING << " ec_writestate FAILED - " << stateRet << std::endl;
    }
    else
    {
        ARMARX_INFO << "ec_writestate WORKING\n";
    }
    int chk = 40;
    // wait for all slaves to reach OP state
    // send the pdo at least one time, so the slaves see we are there up and running
    do
    {
        ec_send_processdata();
        ec_receive_processdata(EC_TIMEOUTRET);
        ec_statecheck(0, EC_STATE_OPERATIONAL, 50000);
        ARMARX_INFO << deactivateSpam(3) << "Waiting for slaves to reach operational state";
    }
    while (chk-- && (ec_slave[0].state != EC_STATE_OPERATIONAL));

    //check if we succeeded
    if (ec_slave[0].state != EC_STATE_OPERATIONAL)
    {
        ARMARX_ERROR << "Not all slaves reached operational state. Slaves that are not in operational state:" << std::endl;
        ec_readstate();
        //looking for errors
        for (uint16 i = 1; i <= ec_slavecount; i++)
        {
            if (ec_slave[i].state != EC_STATE_OPERATIONAL)
            {
                printALStatusError(i);
            }
        }

        //returning with an error
        return false;
    }
    ARMARX_TRACE;
    ARMARX_INFO << "All Slaves in OP now!" << std::endl;


    //preparing devices to run
    size_t slaveReadyCounter = 0;
    while (switchON_OFF == ON && slaveReadyCounter != slaves.size())
    {
        slaveReadyCounter = 0;
        std::string missingSlaves;
        for (AbstractSlavePtr& slave : slaves)
        {
            if (slave->prepare())
            {
                slaveReadyCounter++;
            }
            else
            {
                missingSlaves += slave->getSlaveIdentifier().humanName + ", ";
            }
        }
        ARMARX_INFO << deactivateSpam(1) << "Waiting for " << (slaves.size() - slaveReadyCounter) << "/" << slaves.size() << " slaves to get ready: " << missingSlaves;
        updatePDO();
    }
    ARMARX_TRACE;
    ARMARX_DEBUG << "PDOs updated.";
    std::stringstream slaveInfo;
    for (AbstractSlavePtr& slave : slaves)
    {
        slaveInfo << "#" << slave->getSlaveNumber() << ": " << slave->getSlaveIdentifier().humanName << "\n";
    }
    ARMARX_VERBOSE << "Used slaves: \n" << slaveInfo.str();
    //check if the bus was put up in op mode or if it was switched off
    if (switchON_OFF == OFF)
    {
        return false;
    }

    //all slaves ar in Op - not only EtherCAT Op also DS 402 for the elmos
    busInOperationalMode = true;
    busUpdateLastUpdateTime = IceUtil::Time::now(IceUtil::Time::Monotonic);
    //if we get here all went good,
    return true;
}

/**
 * This checks if the bus was already started an if not it will print an error message.
 * With this method you don't need to spread the error message all over the code. Just using isStared gives the same result
 * but without any message.
 * @see isWorking
 * @return true if bus was already started othewise false
 */
bool EtherCAT::busShouldBeRunning() const
{
    if (!isSDOWorking)
    {
        ARMARX_INFO << "Bus isn't started yet! So the bus is not available !!!! \n”";
        return false;
    }
    return true;
}

std::string ec_errorToString(ec_errort const&  error)
{
    std::stringstream result;
    result << VAROUT(error.Etype) << "\n" << VAROUT(error.Signal) << "\n"
           << VAROUT(error.Slave) << "\n"
           << VAROUT(error.Index) << "\n"
           << VAROUT(error.SubIdx) << "\n"
           << "\n";
    return result.str();
}

/**
 * This sets the pointers of the PDO mappings for the devices>
 */
bool EtherCAT::setPDOMappings()
{
    ARMARX_TRACE;
    bool retVal = true;
    int byteSum = 0;
    for (std::shared_ptr<AbstractSlave>& slave : slaves)
    {
        ARMARX_INFO << "Checking mapping for slave " << slave->getSlaveNumber() << " name: " << slave->getSlaveIdentifier().humanName;
        if (!slave->hasPDOMapping())
        {
            ARMARX_INFO << "No mapping needed for " << slave->getSlaveIdentifier().humanName;
            continue;
        }
        byteSum += ec_slave[slave->getSlaveNumber()].Obytes + ec_slave[slave->getSlaveNumber()].Ibytes;
        if (ec_slave[slave->getSlaveNumber()].outputs == nullptr || ec_slave[slave->getSlaveNumber()].inputs == nullptr)
        {
            ARMARX_ERROR << "There is a nullpointer in the Mapping of Slave " << slave->getSlaveNumber();

            ARMARX_IMPORTANT << "current Slave" << slave->getSlaveNumber();
            ARMARX_IMPORTANT << "slaveCount: " << ec_slavecount;
            //            ARMARX_IMPORTANT << "size in: " << sizeof(ELMO_in_t) << " size out: " << sizeof(ELMO_out_t);
            ARMARX_IMPORTANT << "iomap ptr: 0x" << std::hex << &IOmap << std::dec;
            ARMARX_IMPORTANT << "sn:" << slave->getSlaveNumber() << std::flush;
            ARMARX_IMPORTANT << "out: 0x" << std::hex << (long)(ec_slave[slave->getSlaveNumber()].outputs) << std::dec
                             << std::flush;
            ARMARX_IMPORTANT << "in: 0x" << std::hex << (long)(ec_slave[slave->getSlaveNumber()].inputs) << std::dec
                             << std::flush;
            ARMARX_IMPORTANT << "in diff:  " << (long)(ec_slave[slave->getSlaveNumber()].inputs - ec_slave[0].outputs)
                             << std::flush;
            ARMARX_IMPORTANT << "-------------------------------------------------------";
            ec_errort error_type;
            while (ec_poperror(&error_type))
            {
                ARMARX_WARNING << "SOEM ERROR: " << ec_errorToString(error_type);
            }
            retVal = false;
        }
        else
        {
            //setting pdo mappings slave inputs are master outputs and vice versa
            slave->setInputPDO(ec_slave[slave->getSlaveNumber()].outputs);
            slave->setOutputPDO(ec_slave[slave->getSlaveNumber()].inputs);
        }
    }
    ARMARX_VERBOSE << "PDO size: " << byteSum;
    return retVal;
}

/**
 * Creates the Slave devices and adds them to the slaves list.
 * @see EtherCAT::slaves
 */
std::pair<std::vector<ControlDevicePtr>, std::vector<SensorDevicePtr>> EtherCAT::createDevices()
{
    ARMARX_TRACE;
    std::set<ControlDevicePtr> ctrlDevs;
    std::set<SensorDevicePtr> sensDevs;
    if (deviceContainerPtr == nullptr)
    {
        throw LocalException("no robot Container set! set a robotContainer before you start the bus!");
    }
    //dump infos about the device container
    {
        ARMARX_DEBUG << "device container:\n#functional devices"
                     << deviceContainerPtr->getAllFunctionalDevices().size()
                     << ARMARX_STREAM_PRINTER
        {
            const auto devs = deviceContainerPtr->getAllFunctionalDevices();
            for (const auto& [idx, dev] : MakeIndexedContainer(devs))
            {
                out << "\n    idx " << idx
                    << " initialized " << dev->isInitialized()
                    << " class: " << dev->getClassName()
                    << "\n        cfg nodes:";
                for (const auto& c : dev->getNode().getChildPaths())
                {
                    out << "\n            " << c;
                }
            }
        };
    }

    auto etherCATFactoryNames = EtherCATDeviceFactory::getAvailableClasses();
    ARMARX_INFO << "etherCATFactoryNames: " << etherCATFactoryNames;
    std::map<std::string, std::vector<uint16_t>> devicesOfType;
    for (uint16_t currentSlaveIndex = 1; currentSlaveIndex <= ec_slavecount; currentSlaveIndex++)
    {
        ARMARX_TRACE;
        const std::string messageSlaveIdentifier = "[Slave index: " + std::to_string(currentSlaveIndex) + "] ";
        //DEBUG for EtherCAT HUB
        //check the device type to identify the EtherCAT Hub
        //        uint32 deviceType = ETHTERCAT_HUB_DEVICE_TYPE;
        //uint32 vendorID = 0;
        ARMARX_INFO << messageSlaveIdentifier << '\n'
                    << "device type: " << std::hex << "0x" << ec_slave[currentSlaveIndex].Dtype   << std::dec << " (dec: " << ec_slave[currentSlaveIndex].Dtype   << ")\n"
                    << "itype:       " << std::hex << "0x" << ec_slave[currentSlaveIndex].Itype   << std::dec << " (dec: " << ec_slave[currentSlaveIndex].Itype   << ")\n"
                    << "eep_id:      " << std::hex << "0x" << ec_slave[currentSlaveIndex].eep_id  << std::dec << " (dec: " << ec_slave[currentSlaveIndex].eep_id  << ")\n"
                    << "eep_man:     " << std::hex << "0x" << ec_slave[currentSlaveIndex].eep_man << std::dec << " (dec: " << ec_slave[currentSlaveIndex].eep_man << ")\n"
                    << "eep_rev:     " << std::hex << "0x" << ec_slave[currentSlaveIndex].eep_rev << std::dec << " (dec: " << ec_slave[currentSlaveIndex].eep_rev << ")\n";
        ARMARX_INFO << messageSlaveIdentifier << "CoE details " << (int)(ec_slave[currentSlaveIndex].mbx_proto & ECT_MBXPROT_COE);

        bool foundDevice = false;
        try
        {
            for (auto& facName : etherCATFactoryNames)
            {
                ARMARX_INFO << "Trying factory " << facName;
                auto device = EtherCATDeviceFactory::fromName(facName, std::make_tuple(this, currentSlaveIndex, deviceContainerPtr));
                ARMARX_INFO << "Device is " << device.get();
                if (device)
                {
                    devicesOfType[device->getClassName()].emplace_back(currentSlaveIndex);

                    auto newSlaves = device->getSlaves();
                    ARMARX_INFO << "Found device of type: " << device->getClassName() << " with " << newSlaves.size() << " slaves";
                    ARMARX_CHECK_GREATER(newSlaves.size(), 0);
                    currentSlaveIndex += newSlaves.size() - 1;
                    slaves.insert(slaves.end(), newSlaves.begin(), newSlaves.end());
                    ctrlDevs.insert(device->getCtrlDevs().begin(), device->getCtrlDevs().end());
                    sensDevs.insert(device->getSensDevs().begin(), device->getSensDevs().end());
                    foundDevice = true;
                    break;
                }
            }
        }
        catch (LocalException& e)
        {
            ARMARX_WARNING
                    << "error during slave creation: " << messageSlaveIdentifier << e.getReason();
            continue;
        }
        if (!foundDevice)
        {
            devicesOfType["<NO FACTORY FOUND>"].emplace_back(currentSlaveIndex);
            ARMARX_ERROR << "Could not find a corresponding factory for " << messageSlaveIdentifier << " product id: " << (int)ec_slave[currentSlaveIndex].eep_id <<
                         "vendor id: " << (int)ec_slave[currentSlaveIndex].eep_man;
        }
    }
    ARMARX_INFO << "Summary of devices and factory types:\n" << ARMARX_STREAM_PRINTER
    {
        for (const auto& [factoryName, ids] : devicesOfType)
        {
            out << "---- " << factoryName << ": #" << ids.size() << " (ids:";
            for (auto id : ids)
            {
                out << ' ' << id;
            }
            out << ")\n";
        }
    }
            << '\n' << slaves.size() << " usable slave devices are initialized!" << std::endl;
    return {std::vector<ControlDevicePtr>(ctrlDevs.begin(), ctrlDevs.end()), std::vector<SensorDevicePtr>(sensDevs.begin(), sensDevs.end())};
}


void EtherCAT::setSocketFileDescriptor(int socketFileDescriptor)
{
    this->socketFileDescriptor = socketFileDescriptor;
}

void EtherCAT::setIfName(const std::string& ifname)
{
    this->ifname = ifname;
}

/**
 * This updates all information on the bus, so all commands will be send to the Bus and all sensor and other monitored
 * values will be recived from the bus.
 * To run this the bus fist hast to be switched on, otherwise it will return false.
 * @see EtherCAT::switchBusOFF()
 * @see EtherCAT::switchBusON()
 * @return true if the loop was stop correct, false if there was an error or loop cloudn't even started.
 */
bool EtherCAT::updateBus(bool doErrorHandling)
{
    ARMARX_TRACE;

    if (!isSDOWorking)
    {
        ARMARX_INFO << "Control loop couldn't start - bus is not switched on\n";
        return false;
    }
    else if (switchON_OFF == OFF)
    {
        if (!isSDOWorking)
        {
            ARMARX_WARNING << "Could not update bus because it switched off - closing bus (again?)";
        }
        closeBus();
        return false;
    }

    if (switchON_OFF == ON)
    {

        // handle emergency stop release
        bool emergencyStopActive = isEmergencyStopActive();
        auto now = IceUtil::Time::now();
        auto recoverTriggerAge = (now - ermergencyStopRecoverStartpoint);

        // the elmo sometimes go from emergency stop state back into fault state for a moment - the time variable is the fix for that
        if ((!emergencyStopActive && emergencyStopWasActivated) ||  recoverTriggerAge.toSecondsDouble() < 2.0)
        {
            if (recoverTriggerAge.toSecondsDouble() > 2)
            {
                ermergencyStopRecoverStartpoint = now;
            }
            bool recovered = true;
            for (AbstractSlavePtr& slave : slaves)
            {
                recovered &= slave->recoverFromEmergencyStop();
            }
            if (recovered)
            {
                ARMARX_RT_LOGF_IMPORTANT("Recovered!");
                emergencyStopWasActivated = false;
            }
        }
        else if (emergencyStopActive && !emergencyStopWasActivated)
        {
            ARMARX_RT_LOGF_IMPORTANT("EMERGENCY STOP ACTIVE");
            emergencyStopWasActivated = emergencyStopActive;
        }



        //execute every slave
        for (AbstractSlavePtr& slave : slaves)
        {

            //try to clear error if there exist some, the rest of the slaves can run normal
            if (!emergencyStopActive && slave->hasError())
            {
                //if the errors can't be fixed we will switch of the bus with an error
                if (doErrorHandling && !slave->handleErrors())
                {
                    error = true;
                }
                //ARMARX_WARNING << "slave: " << slave->getSlaveNumber() << " had error";
            }
            else
            {
                slave->execute();
            }
        }

        auto delay = (IceUtil::Time::now(IceUtil::Time::Monotonic) - busUpdateLastUpdateTime);
        if (delay.toMilliSecondsDouble() > 40)
        {
            ARMARX_RT_LOGF_WARN("Update bus was not called for a long time: %d ms", delay.toMilliSecondsDouble()).deactivateSpam(5);
        }

        //send an receive process data
        //        RT_TIMING_START(UpdatePDO);
        updatePDO();
        //        RT_TIMING_CEND(UpdatePDO, 0.7);
        busUpdateLastUpdateTime = IceUtil::Time::now(IceUtil::Time::Monotonic);


        //error handling
        if (doErrorHandling)
        {
            errorHandling();
        }
    }

    return true;
}

/**
 * This sets the flag to switch off the bus.
 * If the bus is already set to switch off this will has no effect.
 * @see EtherCAT::closeBus()
 */
void EtherCAT::switchBusOFF()
{
    if (switchON_OFF == OFF)
    {
        ARMARX_VERBOSE << deactivateSpam(1) << "Bus is already switched off";
        return;
    }
    readErrorCounters();
    ARMARX_INFO << "Switching off bus";
    switchON_OFF = OFF;

}

bool EtherCAT::isBusInOperationalMode()
{
    return busInOperationalMode;
}

/**
 * This closes the Bus
 */
void EtherCAT::closeBus()
{
    ARMARX_INFO << "Bus is shutting down";


    //from now we doing all we can to be not in Op mode and we don't want anybody to send PDO's anymore
    busInOperationalMode = false;


    //shutdown the slaves if the bus hasn't died
    if (!isBusDead && !error)
    {
        bool found;
        do
        {
            found = false;
            for (auto slave : slaves)
            {
                ARMARX_INFO << deactivateSpam(1) << "shutting down slave " << slave->getSlaveIdentifier().humanName << " (" << slave->getSlaveNumber() << "/" << slaves.size() << ")" << std::endl;
                found |= !slave->shutdown();
                //            {
                //                if((std::chrono::duration_cast<std::chrono::seconds>(
                //                        std::chrono::high_resolution_clock::now() - startCycle).count() < 5))
                //                {

                //                }
                //            }
            }
            updatePDO();
        }
        while (found);
    }

    //indicate that bus is closed
    isSDOWorking = false;
    pdoMapped = false;

    //shutting down  bus
    ///requesting init for all slaves
    ec_slave[0].state = EC_STATE_INIT;
    ec_writestate(0);

    ///closing bus
    ec_close();

    ARMARX_INFO << "Bus closed" << std::endl;
}

/**
 * This deactivates the Complete access mode in CoE for the given slave.
 * For Elmo's it is necessary to deactivate the CA mode otherwise soem isn't able to bring them into OP-Mode
 * @param slave the slave for which the CA mode will be deactivated
 */
void EtherCAT::deactivateCOECA(AbstractSlave* slave)
{
    ARMARX_INFO << "Deactivation CoE Complete Access for slave:" << slave->getSlaveNumber()
                << std::endl;
    uint8 config = ec_slave[slave->getSlaveNumber()].CoEdetails;
    config &= ~ECT_COEDET_SDOCA;
    ec_slave[slave->getSlaveNumber()].CoEdetails = config;
}

/**
 * If there is a erro on the bus this prints the AL Status code:
 */
void EtherCAT::printALStatusError(uint16_t slave)
{
    std::string name = "Unknown";

    AbstractSlavePtr slavePtr =  getSlaveAtIndex(slave);
    if (slavePtr)
    {
        name = slavePtr->getSlaveIdentifier().humanName;
    }

    ARMARX_ERROR << "Slave " << name << " (number: " << slave << ") State=0x" << std::hex << EtherCATStateToString(static_cast<ec_state>(ec_slave[slave].state))
                 << " StatusCode=0x" << ec_slave[slave].ALstatuscode  << " : "
                 << ec_ALstatuscode2string(ec_slave[slave].ALstatuscode)
                 << ", name: " << ec_slave[slave].name;
}

int EtherCAT::ecx_APRD_with_error_handling(uint16_t ADP, uint16_t ADO, uint16_t length, void* data, int timeout, uint16_t slaveIndex, const std::string& name, int port)
{
    int retval = ecx_APRD(ecx_context.port, ADP, ADO, length, data, timeout);
    if (retval <= 0)
    {
        std::stringstream ss;
        ss << "0x" << std::hex << std::setw(4) << std::setfill('0') << ADO;
        ARMARX_ERROR << "ecx_APRD failed: WC = " << retval << " (-1: EC_NOFRAME) . Slavenumber " << slaveIndex << " port:" << port << "\tname: " << name
                     << "\taddr: " << ss.str();
    }
    return retval;
}

void EtherCAT::readErrorCounters()
{
    IceUtil::Time start = IceUtil::Time::now(IceUtil::Time::Monotonic);
    for (uint16 slaveIndex = 1; slaveIndex <= *(ecx_context.slavecount); slaveIndex++)
    {
        std::string name = "Unknown";

        AbstractSlavePtr slavePtr =  getSlaveAtIndex(slaveIndex);
        if (slavePtr)
        {
            name = slavePtr->getSlaveIdentifier().humanName;
        }

        uint16 ADPh = (uint16)(1 - slaveIndex);

        //not used, only confusing info...
        //uint16_t configAddr = ecx_APRDw(ecx_context.port, ADPh, ECT_REG_STADR, 100000);
        for (int i = 0; i < 4; i++)
        {

            // Error handling taken from
            // ethercat_esc_datasheet_sec1_technology_2i2: Chapter 14 Error counters, Table 50:
            // see: RobotAPI/etc/doc/ethercat/ethercat_esc_datasheet_sec1_technology_2i2.pdf
            // or https://www.ethercat.org/download/documents/EtherCAT_Diagnosis_For_Users_DE.pdf
            // or RobotAPI/etc/doc/EtherCAT_Diagnosis_For_Users_DE.pdf
            uint8_t rxErrorCounter = 0;
            uint8_t invalidFrameCounter = 0; // or Physical error count
            uint8_t forwardedRxErrorCounter = 0; // shows the error counter of a predecessor
            uint8_t linkLostCounter = 0;
            int ret1 = ecx_APRD_with_error_handling(ADPh, 0x300 + i * 2, 1, &invalidFrameCounter, 100000, slaveIndex, name, i);
            int ret2 = ecx_APRD_with_error_handling(ADPh, 0x301 + i * 2, 1, &rxErrorCounter, 100000, slaveIndex, name, i);
            int ret3 = ecx_APRD_with_error_handling(ADPh, 0x308 + i, 1, &forwardedRxErrorCounter, 100000, slaveIndex, name, i);
            int ret4 = ecx_APRD_with_error_handling(ADPh, 0x310 + i, 1, &linkLostCounter, 100000, slaveIndex, name, i);

            if (rxErrorCounter > 0 || invalidFrameCounter > 0 || forwardedRxErrorCounter > 0 || linkLostCounter > 0)
            {
                ARMARX_ERROR << "Errors found for Slavenumber " << slaveIndex << " port:" << i << "\tname: " << name << ": "
                             << VAROUT((int)rxErrorCounter) << "\t" << VAROUT((int)invalidFrameCounter) << "\t" << VAROUT((int)forwardedRxErrorCounter) << "\t" << VAROUT((int)linkLostCounter);
            }
            else if (ret1 > 0 && ret2 > 0 && ret3 > 0 && ret4 > 0)
            {
                ARMARX_INFO << "no errors for Slavenumber " << slaveIndex << " port:" << i << "\tname: " << name;
            }
        }
        IceUtil::Time elapsed = (IceUtil::Time::now(IceUtil::Time::Monotonic) - start);
        if (elapsed.toMilliSeconds() > 10)
        {
            updatePDO();
            ARMARX_INFO << "Updated BUS to prevent timeout, " << elapsed << " has passed since last bus update.";
            start = IceUtil::Time::now(IceUtil::Time::Monotonic);
        }
    }
}

void EtherCAT::errorHandling()
{
    uint16 slave;
    //if the bus is in already in safe op then we have an error and don't need any more stuff to do
    error = ec_slave[0].state == EC_STATE_SAFE_OP;

    if (lastWorkCounter == expectedWKC)
    {
        noErrorIterations++;
    }
    ARMARX_ON_SCOPE_EXIT
    {
        firstErrorCheck = false;
    };
    if (((lastWorkCounter < expectedWKC) || ec_group[currentGroup].docheckstate) && !error)
    {


        ARMARX_RT_LOGF_WARN("RECOVERY - Wkc: %s - %d/%d, state: %s - iterations without error: %d", ((lastWorkCounter < expectedWKC) ? "NOT OK" : "OK"),
                            lastWorkCounter, expectedWKC, (ec_group[currentGroup].docheckstate ? "NOT OK" : "OK"), noErrorIterations);

        //actually there is something wrong so we have an error lets see if we can find an fix it
        error = true;
        if (checkErrorCountersOnWKCError /* || (!firstErrorCheck && noErrorIterations == 0)*/)
        {
            readErrorCounters();
        }
        noErrorIterations = 0;

        /* one ore more slaves are not responding */
        //This is hard SOEM code not the easy stuff, but works...
        ec_group[currentGroup].docheckstate = FALSE;
        ec_readstate();
        for (slave = 1; slave <= ec_slavecount; slave++)
        {
            if ((ec_slave[slave].group == currentGroup) && (ec_slave[slave].state != EC_STATE_OPERATIONAL))
            {
                ec_group[currentGroup].docheckstate = TRUE;
                if (ec_slave[slave].state == (EC_STATE_SAFE_OP + EC_STATE_ERROR))
                {
                    ARMARX_RT_LOGF_WARN("ERROR : slave %d is in SAFE_OP + ERROR, attempting ack.", slave);
                    //Reading as much data form the slave we can
                    //AL Status code (EtherCAT)
                    ARMARX_RT_LOGF_WARN("EtherCAT::errorHandling - AbstractSlave %d State=0x%2.2x StatusCode=0x%4.4x : %s\n",
                                        slave, ec_slave[slave].state, ec_slave[slave].ALstatuscode,
                                        ec_ALstatuscode2string(ec_slave[slave].ALstatuscode));
                    //Abort Code (Coe - Ds 301)
                    ARMARX_RT_LOGF_WARN("Error: %s \n", ec_elist2string());


                    ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
                    ec_writestate(slave);
                    //there is a chance to recover
                    error = false;
                }
                else if (ec_slave[slave].state == EC_STATE_SAFE_OP)
                {
                    ARMARX_RT_LOGF_WARN("slave %d is in SAFE_OP, change to OPERATIONAL.", slave);
                    ec_slave[slave].state = EC_STATE_OPERATIONAL;
                    ec_writestate(slave);
                    //we recovered
                    error = false;
                }
                else if (ec_slave[slave].state > 0)
                {
                    ARMARX_RT_LOGF_WARN("slave %d has a bad state", slave);
                    if (ec_reconfig_slave(slave, EC_TIMEOUTMON))
                    {
                        ec_slave[slave].islost = FALSE;
                        ARMARX_RT_LOGF_WARN("slave %d reconfigured", slave);
                        //we recovered
                        error = false;
                    }
                }
                else if (!ec_slave[slave].islost)
                {
                    /* re-check state */
                    ec_statecheck(slave, EC_STATE_OPERATIONAL, EC_TIMEOUTRET);
                    if (!ec_slave[slave].state)
                    {
                        ec_slave[slave].islost = TRUE;
                        ARMARX_ERROR << "ERROR : slave " << slave << " lost";
                    }
                }
            }
            //ARMARX_IMPORTANT << __LINE__ << " before is Lost " << slave << " " << (ec_slave[slave].islost ? "TRUE" : "False") ;
            if (ec_slave[slave].islost)
            {
                if (!ec_slave[slave].state)
                {
                    if (ec_recover_slave(slave, EC_TIMEOUTMON))
                    {
                        ec_slave[slave].islost = FALSE;
                        ARMARX_RT_LOGF_WARN("slave %d recovered", slave);
                        //we recovered
                        error = false;
                    }
                    //we couldn't recover the slave so it is lost
                    ARMARX_RT_LOGF_ERROR("Could not recover slave %d", slave);
                    ec_slave[slave].islost = TRUE;
                    error = true;
                    continue;
                }
                else
                {
                    ec_slave[slave].islost = FALSE;
                    ARMARX_RT_LOGF_WARN("slave %d found", slave);
                }
            }
        }
        if (!ec_group[currentGroup].docheckstate)
        {
            ARMARX_RT_LOGF_WARN("all slaves resumed into OPERATIONAL-Mode");
            error = false;
        }
        else
        {
            ARMARX_ERROR << "Bus is not ok! ";
            readErrorCounters();
            //if we get here we have an error but no solution to fix this, very sad
            error = true;
        }
    }

    //if there is an error and we don't already switched to safe op we can skipp this.
    if (error && ec_slave[0].state != EC_STATE_SAFE_OP)
    {
        ARMARX_ERROR << "Bus detected an Error, maybe one slave or the whole bus died! - Switching in Safe-Op Modus "
                     << "No Targets will be accepted bei the slaves anymore";
        //switching to safe op so we receive data but the slaves won't accept any new targets
        ec_slave[0].state = EC_STATE_SAFE_OP;
        ec_writestate(0);
    }

    // check SOEM error list(e.g. from mailbox) for errors
    if (ec_iserror())
    {
        ec_errort error;
        while (ec_poperror(&error))
        {
            auto slave = getSlaveAtIndex(error.Slave);
            if (error.Etype == EC_ERR_TYPE_EMERGENCY)
            {
                ARMARX_RT_LOGF_WARN("Found emergency error for slave %s (id: %d) from timestamp %d: error code: %d, error reg: %d, error index: %d, error sub index: %d",
                                    (slave ? slave->getSlaveIdentifier().humanName.c_str() : "unknown"),
                                    error.Slave, error.Time.sec, error.ErrorCode, error.ErrorReg, error.Index, error.SubIdx);
            }
            else
            {
                ARMARX_RT_LOGF_WARN("Found error for slave %s (id: %d) from timestamp %d: error type: %s, error index: %d, error sub index: %d",
                                    (slave ? slave->getSlaveIdentifier().humanName.c_str() : "unknown"),
                                    error.Slave, error.Time.sec, EC_ErrorTypeToString(error.Etype), error.Index, error.SubIdx);
            }
        }
    }
}

std::vector<SensorDevicePtr> EtherCAT::getSensDevs() const
{
    return sensDevs;
}

std::vector<ControlDevicePtr> EtherCAT::getCtrlDevs() const
{
    return ctrlDevs;
}

bool EtherCAT::rebootBus()
{
    isSDOWorking = false;
    ARMARX_IMPORTANT << "Current bus state: " << EC_StateToString(ec_slave[0].state);
    if (ec_slave[0].state == EC_STATE_INIT)
    {
        return startBus(false);
    }
    return true;
}

const char* EtherCAT::EC_StateToString(uint16 state)
{
    switch (state)
    {
        case EC_STATE_NONE:
            return "EC_STATE_NONE";
        case EC_STATE_INIT:
            return "EC_STATE_INIT";
        case EC_STATE_PRE_OP:
            return "EC_STATE_PRE_OP";
        case EC_STATE_BOOT:
            return "EC_STATE_BOOT";
        case EC_STATE_SAFE_OP:
            return "EC_STATE_SAFE_OP";
        case EC_STATE_OPERATIONAL:
            return "EC_STATE_OPERATIONAL";
        case EC_STATE_ACK:
            return "EC_STATE_ACK";
    }
    throw std::logic_error {__FILE__ ": " + to_string(__LINE__) + ": unhandled state: " + to_string(state)};
}

const char* EtherCAT::EC_ErrorTypeToString(uint16 errorType)
{
    switch (errorType)
    {
        case EC_ERR_TYPE_SDO_ERROR:
            return "EC_ERR_TYPE_SDO_ERROR";
        case EC_ERR_TYPE_EMERGENCY:
            return "EC_ERR_TYPE_EMERGENCY";
        case EC_ERR_TYPE_PACKET_ERROR:
            return "EC_ERR_TYPE_PACKET_ERROR";
        case EC_ERR_TYPE_SDOINFO_ERROR:
            return "EC_ERR_TYPE_SDOINFO_ERROR";
        case EC_ERR_TYPE_FOE_ERROR:
            return "EC_ERR_TYPE_FOE_ERROR";
        case EC_ERR_TYPE_FOE_BUF2SMALL:
            return "EC_ERR_TYPE_FOE_BUF2SMALL";
        case EC_ERR_TYPE_FOE_PACKETNUMBER:
            return "EC_ERR_TYPE_FOE_PACKETNUMBER";
        case EC_ERR_TYPE_SOE_ERROR:
            return "EC_ERR_TYPE_SOE_ERROR";
        case EC_ERR_TYPE_MBX_ERROR:
            return "EC_ERR_TYPE_MBX_ERROR";
        case EC_ERR_TYPE_FOE_FILE_NOTFOUND:
            return "EC_ERR_TYPE_FOE_FILE_NOTFOUND";
    }
    throw std::logic_error {__FILE__ ": " + to_string(__LINE__) + ": unhandled error type: " + to_string(errorType)};
}




const std::atomic_bool& EtherCAT::getIsBusDead() const
{
    return isBusDead;
}

/**
 * Uperror the PDO and updates the lastWorkingCounter to latest receive event
 * @see EtherCAT::lastWorkingCounter
 */
void EtherCAT::updatePDO()
{

    static int count = 0;
    static long tx_max = 0;
    static long rx_max = 0;


    auto PDO_Tx = IceUtil::Time::now();
    auto PDO_Rx = IceUtil::Time::now();
    //TIMING_START(PDO_Tx);
    ec_send_processdata();
    //TIMING_END(PDO_Tx);
    long tx_elapsed = (IceUtil::Time::now() - PDO_Tx).toMicroSeconds();


    //TIMING_START(PDO_Rx);
    lastWorkCounter = ec_receive_processdata(EC_TIMEOUTMON * 10);
    //TIMING_END(PDO_Rx);
    long rx_elapsed = (IceUtil::Time::now() - PDO_Rx).toMicroSeconds();

    tx_max = std::max(tx_max, tx_elapsed);
    rx_max = std::max(rx_max, rx_elapsed);

    count++;
    if (count >= 1000 && false)
    {
        count = 0;
        ARMARX_RT_LOGF_INFO("TX max: %d, µs, RX max: %d µs", tx_max, rx_max);

        tx_max = 0;
        rx_max = 0;
    }
}

/**
 * Returns all identifiied slaves on the bus
 * @return
 */
std::vector<AbstractSlavePtr> EtherCAT::getSlaves()
{
    return slaves;
}

EtherCAT::~EtherCAT()
{

}

/**
 * This starts the bus.
 * If the is already started then nothing will be done.
 * @see EtherCAT::startBus()
 */
bool EtherCAT::switchBusON()
{
    //check if the bus is already running
    if (switchON_OFF == ON)
    {
        return true;
    }
    //otherwise start it
    switchON_OFF = ON;
    return startBus(true);
}

/**
 * Performs a SDO write to the slave to the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slavenumber of the slave on the bus
 * @param [IN] value the value that will be written to the slaves
 * @param [IN] index the index of the Entry where the value is written to
 * @param [IN] subindex the subindex of the Entry
 * @param [IN] CompleteAccess with this flag you can activate writing in complete access mode
 * @return TRUE when write was successful otherwise FALSE
 */
bool EtherCAT::writeByteBuffer(unsigned char* buf, int buflen, uint16_t slave, uint16_t index, uint8_t subindex,
                               bool CompleteAccess)
{
    if (!busShouldBeRunning())
    {
        return false;
    }
    int workCount;
    workCount = ec_SDOwrite(slave, index, subindex, (boolean) CompleteAccess, buflen, buf, 5000);
    ARMARX_DEBUG << "Writing Buffer to slave: " << slave << " index 0x" << std::hex << index << std::dec << ":" << subindex << ", wc " << workCount << ": " << (workCount > 0 ? "success" : " failure");
    if (workCount > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool EtherCAT::readByteBuffer(uint16_t slave, uint16_t index, uint8_t subindex, unsigned char* buf, int buflen,
                              bool CompleteAccess)
{
    return generalSDORead(slave, index, subindex, buflen, buf, CompleteAccess);
}

/**
 * Performs a SDO write to the slave to the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slavenumber of the slave on the bus
 * @param [IN] value the value that will be written to the slaves
 * @param [IN] index the index of the Entry where the value is written to
 * @param [IN] subindex the subindex of the Entry
 * @param [IN] CompleteAccess with this flag you can activate writing in complete access mode
 * @return TRUE when write was successful otherwise FALSE
 */
bool EtherCAT::writeSDO(uint16_t slave, int8_t value, uint16_t index, uint8_t subindex, bool CompleteAccess)
{
    int buflen = 1;
    return generalSDOWrite(slave, index, subindex, buflen, &value, CompleteAccess);
}

/**
 * Performs a SDO write to the slave to the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slavenumber of the slave on the bus
 * @param [IN] value the value that will be written to the slaves
 * @param [IN] index the index of the Entry where the value is written to
 * @param [IN] subindex the subindex of the Entry
 * @param [IN] CompleteAccess with this flag you can activate writing in complete access mode
 * @return TRUE when write was successful otherwise FALSE
 */
bool EtherCAT::writeSDO(uint16_t slave, uint8_t value, uint16_t index, uint8_t subindex, bool CompleteAccess)
{
    int buflen = 1;
    return generalSDOWrite(slave, index, subindex, buflen, &value, CompleteAccess);
}

/**
 * Performs a SDO write to the slave to the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slavenumber of the slave on the bus
 * @param [IN] value the value that will be written to the slaves
 * @param [IN] index the index of the Entry where the value is written to
 * @param [IN] subindex the subindex of the Entry
 * @param [IN] CompleteAccess with this flag you can activate writing in complete access mode
 * @return TRUE when write was successful otherwise FALSE
 */
bool EtherCAT::writeSDO(uint16_t slave, uint16_t value, uint16_t index, uint8_t subindex, bool CompleteAccess)
{
    int buflen = 2;
    return generalSDOWrite(slave, index, subindex, buflen, &value, CompleteAccess);
}

/**
 * Performs a SDO write to the slave to the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slavenumber of the slave on the bus
 * @param [IN] value the value that will be written to the slaves
 * @param [IN] index the index of the Entry where the value is written to
 * @param [IN] subindex the subindex of the Entry
 * @param [IN] CompleteAccess with this flag you can activate writing in complete access mode
 * @return TRUE when write was successful otherwise FALSE
 */
bool EtherCAT::writeSDO(uint16_t slave, uint32_t value, uint16_t index, uint8_t subindex, bool CompleteAccess)
{
    int buflen = 4;
    return generalSDOWrite(slave, index, subindex, buflen, &value, CompleteAccess);
}

/**
 * Performs a SDO write to the slave to the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slavenumber of the slave on the bus
 * @param [IN] value the value that will be written to the slaves
 * @param [IN] index the index of the Entry where the value is written to
 * @param [IN] subindex the subindex of the Entry
 * @param [IN] CompleteAccess with this flag you can activate writing in complete access mode
 * @return TRUE when write was successful otherwise FALSE
 */
bool EtherCAT::writeSDO(uint16_t slave, int32_t value, uint16_t index, uint8_t subindex, bool CompleteAccess)
{
    int buflen = 4;
    return generalSDOWrite(slave, index, subindex, buflen, &value, CompleteAccess);
}

/**
 * Performs a SDO read from the slave from the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slave it will read from
 * @param [IN] index the index of de object dictonary it will read from
 * @param [IN] subindex the sub index of the entry
 * @param [OUT] value in this value the read value will be written
 * @return TRUE when read was successful otherwise FALSE
 */
bool EtherCAT::readSDO(uint16_t slave, uint16_t index, uint8_t subindex, uint8_t& value) const
{
    int buflen = 1;
    return generalSDORead(slave, index, subindex, buflen, &value);
}

/**
 * Performs a SDO read from the slave from the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slave it will read from
 * @param [IN] index the index of de object dictonary it will read from
 * @param [IN] subindex the sub index of the entry
 * @param [OUT] value in this value the read value will be written
 * @return TRUE when read was successful otherwise FALSE
 */
bool EtherCAT::readSDO(uint16_t slave, uint16_t index, uint8_t subindex, int8_t& value) const
{
    int buflen = 1;
    return generalSDORead(slave, index, subindex, buflen, &value);

}

/**
 * Performs a SDO read from the slave from the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slave it will read from
 * @param [IN] index the index of de object dictonary it will read from
 * @param [IN] subindex the sub index of the entry
 * @param [OUT] value in this value the read value will be written
 * @return TRUE when read was successful otherwise FALSE
 */
bool EtherCAT::readSDO(uint16_t slave, uint16_t index, uint8_t subindex, uint16_t& value, bool CompleteAccess) const
{
    int buflen = 2;
    return generalSDORead(slave, index, subindex, buflen, &value, CompleteAccess);
}

bool EtherCAT::readSDO(uint16_t slave, uint16_t index, uint8_t subindex, int16_t& value) const
{
    int buflen = 2;
    return generalSDORead(slave, index, subindex, buflen, &value);
}

/**
 * Performs a SDO read from the slave from the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slave it will read from
 * @param [IN] index the index of de object dictonary it will read from
 * @param [IN] subindex the sub index of the entry
 * @param [OUT] value in this value the read value will be written
 * @return TRUE when read was successful otherwise FALSE
 */
bool EtherCAT::readSDO(uint16_t slave, uint16_t index, uint8_t subindex, int32_t& value) const
{
    int buflen = 4;
    return generalSDORead(slave, index, subindex, buflen, &value);

}

/**
 * Performs a SDO read from the slave from the given index an subindex returns true if it succeed.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slave it will read from
 * @param [IN] index the index of de object dictonary it will read from
 * @param [IN] subindex the sub index of the entry
 * @param [OUT] value in this value the read value will be written
 * @return TRUE when read was successful otherwise FALSE
 */
bool EtherCAT::readSDO(uint16_t slave, uint16_t index, uint8_t subindex, uint32_t& value) const
{
    int buflen = 4;
    return generalSDORead(slave, index, subindex, buflen, &value);
}

/**
 * This will return the Vendor ID of the slave with the given slave number
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave the slave number of the requested slave
 * @param [OUT] vendorID
 * @return TRUE when read was successful otherwise FALSE
 */
bool EtherCAT::getVendorID(uint16_t slave, uint32_t& vendorID) const
{
    //    uint32 serial;
    bool retVal = readSDO(slave, 0x1018, 1, vendorID);
    //    getSerialNumber(slave, serial);
    ARMARX_INFO << "Vendor ID of slave " << slave << " 0x" << std::hex << vendorID << std::dec << " (" << vendorID
                << ")";
    //                << " serial number: " << serial;
    //    if (retVal && (vendorID == ELMO_VENDOR_ID))
    //    {
    //        ARMARX_DEBUG << "(Elmomc)";
    //    }
    //    else if (retVal && (vendorID == H2T_VENDOR_ID))
    //    {
    //        ARMARX_DEBUG << "(H2T)";
    //    }
    //    else if (retVal)
    //    {
    //        ARMARX_WARNING << "Unknown vendor";
    //    }
    //    else
    //    {
    //        ARMARX_ERROR << "reading Vendor Id failed";
    //    }
    return retVal;
}


bool EtherCAT::getSerialNumber(uint16 slave, uint32& serialNumber) const
{
    bool retVal = readSDO(slave, 0x1018, 4, serialNumber);
    return retVal;
}


bool EtherCAT::getProductCode(uint16_t slave, uint32_t& productCode) const
{
    bool retVal = readSDO(slave, 0x1018, 2, productCode);
    //printf("Product Code of slave %d: 0x%x (%d)", slave, productCode, productCode);
    ARMARX_DEBUG << "Product Code of slave " << slave << ": 0x" << std::hex << productCode << std::dec
                 << " (" << productCode << ")";
    //    if (retVal && (productCode == ELMO_ACTOR_PRODUCT_CODE))
    //    {
    //        ARMARX_DEBUG << "actor elmo";
    //    }
    //    else if (retVal && (productCode == H2T_SENSOBOARD_PRODUCT_CODE))
    //    {
    //        ARMARX_DEBUG << "sensor board";
    //    }
    //    else if (retVal)
    //    {
    //        ARMARX_WARNING << "Unknown product code ";
    //    }
    //    else
    //    {
    //        ARMARX_ERROR << "reading Product Code failed";
    //    }
    return retVal;
}

/**
 * This is the private genearl SDO write function which doesn't do any typ checks to the given write value.
 * It checks if the SDO write was successful an prints a message
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave
 * @param [IN] index
 * @param [IN] subindex
 * @param [IN] buflen
 * @param [IN] buf
 * @param [IN] ca
 * @return
 */
bool EtherCAT::generalSDOWrite(uint16_t slave, uint16_t index, uint8_t subindex, int buflen, void* buf, bool ca)
{
    if (!busShouldBeRunning())
    {
        ARMARX_WARNING << "Bus is not running no write";
        return false;
    }
    std::unique_lock lock(etherCatMutex);
    IceUtil::Time start = IceUtil::Time::now();
    int workCount = ec_SDOwrite(slave, index, subindex, (boolean) ca, buflen, buf, SDO_WRITE_TIMEOUT);
    IceUtil::Time end = IceUtil::Time::now();
    //printf("EtherCAT::generalSDOWrite - Writing 0x%x (%d) to slave: %d index 0x%x:%d : ", *((int*) buf),
    //       *((int*) buf), slave, index, subindex);
    /*ARMARX_INFO << "Writing 0x" << std::hex << std::uppercase << *((int*) buf) << std::dec << "(" << *((int*) buf) << ")"
                << " to slave: " << slave << " index 0x" << std::hex << std::uppercase << index << std::dec << ":"
                << (int32) subindex;*/
    if (workCount > 0)
    {
        //ARMARX_INFO << "success.\n";
        return true;
    }
    else
    {
        ARMARX_WARNING << std::hex << "Error while writing at 0x" << index << ":" << (int)subindex;
        ARMARX_WARNING << "failure. work counter: " << workCount << " writing took " << (end - start).toMicroSeconds() << std::endl;
        printALStatusError(slave);
        //Read all the errors
        ARMARX_WARNING << ec_elist2string();
        return false;
    }
}

/**
 * This is the private genearl SDO write function which doesn't do any typ checks to the given write value.
 * It checks if the SDO write was successful an prints a message.
 * EtherCAT::startBus(string ifname) hast do be called once before.
 * @see EtherCAT::startBus()
 * @param [IN] slave
 * @param [IN] index
 * @param [IN] subindex
 * @param [IN] buflen
 * @param [OUT] buf
 * @param [IN] ca complete access by default false
 * @return
 */
bool EtherCAT::generalSDORead(uint16_t slave, uint16_t index, uint8_t subindex, int buflen, void* buf, bool ca) const
{
    if (!busShouldBeRunning())
    {
        return false;
    }
    std::unique_lock lock(etherCatMutex);
    IceUtil::Time start = IceUtil::Time::now();
    int workCount = ec_SDOread(slave, index, subindex, (boolean) ca, &buflen, buf, SDO_READ_TIMEOUT);
    IceUtil::Time end = IceUtil::Time::now();
    if (workCount > 0)
    {
        //printf("EtherCAT::generalSDORead- Reading 0x%x (%d), from slave: %d index 0x%x:%d\n", *((uint16*) buf),
        //       *((uint16*) buf), slave, index,
        //       subindex);
        /*ARMARX_INFO << "Reading 0x" << std::hex << std::uppercase << *((uint16*) buf) << std::dec << "(" << *((uint16*) buf) << ")"
                    << " from slave: " << slave << " index 0x" << std::hex << std::uppercase << index << std::dec << ":"
                    << (int32) subindex;*/
        return true;
    }
    else
    {
        ARMARX_WARNING << std::hex << "Error while reading index 0x" << index << ":" << (int)subindex
                       << " of slave " << slave << " into a buffer of len " << buflen
                       << " ca = " << ca;
        ARMARX_WARNING << "work counter (has to be >0): " << workCount << " reading took " << (end - start).toMicroSeconds() << " µs - error message from SOEM: " << std::string(ec_elist2string());
        return false;
    }
}

void* EtherCAT::getIOMap() const
{
    return static_cast<void*>(ec_slave[0].outputs);
}

int EtherCAT::getMappedSize()
{
    return actualMappedSize;
}

AbstractSlavePtr EtherCAT::getSlaveAtIndex(uint16_t slaveIndex) const
{
    for (AbstractSlavePtr slave : slaves)
    {
        //        ARMARX_INFO << "Checking slave: " << slave->getSlaveNumber() << " vs  " << slaveId;
        if (slave->getSlaveNumber() == slaveIndex)
        {
            return slave;
        }
    }
    return AbstractSlavePtr();
}

std::array<char, IOmapSize>& EtherCAT::getIOMapBuffer()
{
    return IOmap;
}

const std::atomic_bool& EtherCAT::getError() const
{
    return error;
}

bool EtherCAT::isPDOMapped() const
{
    return pdoMapped;
}

void EtherCAT::setDeviceContainerPtr(const DeviceContainerPtr& deviceContainerPtr)
{
    EtherCAT::deviceContainerPtr = deviceContainerPtr;
}

void EtherCAT::setMainUnitPtr(RobotUnit* mainUnitPtr)
{
    EtherCAT::mainUnitPtr = mainUnitPtr;
}

std::string EtherCAT::EtherCATStateToString(u_int16_t state)
{
    switch (state)
    {
        case EC_STATE_NONE:
            return "EC_STATE_NONE";
        case EC_STATE_INIT:
            return "EC_STATE_INIT";
        case EC_STATE_PRE_OP:
            return "EC_STATE_PRE_OP";
        case EC_STATE_BOOT:
            return "EC_STATE_BOOT";
        case EC_STATE_SAFE_OP:
            return "EC_STATE_SAFE_OP";
        case EC_STATE_OPERATIONAL:
            return "EC_STATE_OPERATIONAL";
        case EC_STATE_ERROR:
            return "EC_STATE_ERROR or EC_STATE_ACK";
    }
    return "UNKNOWN_STATE";
}

bool EtherCAT::isEmergencyStopActive() const
{
    bool found = false;
    for (const AbstractSlavePtr& slave : this->slaves)
    {
        if (slave->isEmergencyStopActive())
        {
            found = true; // dont break so that isEmergencyStopActive executed for each slave
        }
    }
    return found;
}


bool EtherCAT::getCheckErrorCountersOnWKCError() const
{
    return checkErrorCountersOnWKCError;
}

void EtherCAT::setCheckErrorCountersOnWKCError(bool value)
{
    checkErrorCountersOnWKCError = value;
}


