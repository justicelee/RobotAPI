/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotAPIVariantWidget
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotAPIVariantWidget.h"

#include <QLabel>
#include <QString>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QTableWidget>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <RobotAPI/interface/core/FramedPoseBase.h>
#include <RobotAPI/interface/core/LinkedPoseBase.h>
#include <RobotAPI/interface/core/OrientedPoint.h>
#include <RobotAPI/interface/core/PoseBase.h>

namespace armarx::detail
{
    RobotAPIVariantWidgetDummySymbol::RobotAPIVariantWidgetDummySymbol(int) {}
}

namespace armarx::VariantDataWidgets
{
    class Vector2BaseWidget: public VariantDataWidgetBase
    {
    public:
        Vector2BaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelX = new QLabel;
            labelY = new QLabel;
            l->addRow("X", labelX);
            l->addRow("Y", labelY);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            Vector2BasePtr v = Vector2BasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelX->setText(QString::number(v->x));
            labelY->setText(QString::number(v->y));
        }
    private:
        QLabel* labelX;
        QLabel* labelY;
    };
    VariantDataWidgetFactoryRegistration<Vector2BaseWidget> registerVector2BaseWidget {Vector2Base::ice_staticId()};

    class Vector3BaseWidget: public VariantDataWidgetBase
    {
    public:

        Vector3BaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelX = new QLabel;
            labelY = new QLabel;
            labelZ = new QLabel;
            l->addRow("X", labelX);
            l->addRow("Y", labelY);
            l->addRow("Z", labelZ);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            Vector3BasePtr v = Vector3BasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelX->setText(QString::number(v->x));
            labelY->setText(QString::number(v->y));
            labelZ->setText(QString::number(v->z));
        }
    private:
        QLabel* labelX;
        QLabel* labelY;
        QLabel* labelZ;
    };
    VariantDataWidgetFactoryRegistration<Vector3BaseWidget> registerVector3BaseWidget {Vector3Base::ice_staticId()};

    class FramedPositionBaseWidget: public VariantDataWidgetBase
    {
    public:

        FramedPositionBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelAg = new QLabel;
            labelFr = new QLabel;
            labelX = new QLabel;
            labelY = new QLabel;
            labelZ = new QLabel;
            l->addRow("Agent", labelAg);
            l->addRow("Frame", labelFr);
            l->addRow("X", labelX);
            l->addRow("Y", labelY);
            l->addRow("Z", labelZ);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            FramedPositionBasePtr v = FramedPositionBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelAg->setText(QString::fromStdString(v->agent));
            labelFr->setText(QString::fromStdString(v->frame));
            labelX->setText(QString::number(v->x));
            labelY->setText(QString::number(v->y));
            labelZ->setText(QString::number(v->z));
        }
    private:
        QLabel* labelAg;
        QLabel* labelFr;
        QLabel* labelX;
        QLabel* labelY;
        QLabel* labelZ;
    };
    VariantDataWidgetFactoryRegistration<FramedPositionBaseWidget> registerFramedPositionBaseWidget {FramedPositionBase::ice_staticId()};

    class FramedDirectionBaseWidget: public VariantDataWidgetBase
    {
    public:

        FramedDirectionBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelAg = new QLabel;
            labelFr = new QLabel;
            labelX = new QLabel;
            labelY = new QLabel;
            labelZ = new QLabel;
            l->addRow("Agent", labelAg);
            l->addRow("Frame", labelFr);
            l->addRow("X", labelX);
            l->addRow("Y", labelY);
            l->addRow("Z", labelZ);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            FramedDirectionBasePtr v = FramedDirectionBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelAg->setText(QString::fromStdString(v->agent));
            labelFr->setText(QString::fromStdString(v->frame));
            labelX->setText(QString::number(v->x));
            labelY->setText(QString::number(v->y));
            labelZ->setText(QString::number(v->z));
        }
    private:
        QLabel* labelAg;
        QLabel* labelFr;
        QLabel* labelX;
        QLabel* labelY;
        QLabel* labelZ;
    };
    VariantDataWidgetFactoryRegistration<FramedDirectionBaseWidget> registerFramedDirectionBaseWidget {FramedDirectionBase::ice_staticId()};

    class OrientedPointBaseWidget: public VariantDataWidgetBase
    {
    public:

        OrientedPointBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelPX = new QLabel;
            labelPY = new QLabel;
            labelPZ = new QLabel;
            labelNX = new QLabel;
            labelNY = new QLabel;
            labelNZ = new QLabel;
            l->addRow("PX", labelPX);
            l->addRow("PY", labelPY);
            l->addRow("PZ", labelPZ);
            l->addRow("NX", labelNX);
            l->addRow("NY", labelNY);
            l->addRow("NZ", labelNZ);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            OrientedPointBasePtr v = OrientedPointBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelPX->setText(QString::number(v->px));
            labelPY->setText(QString::number(v->py));
            labelPZ->setText(QString::number(v->pz));
            labelNX->setText(QString::number(v->nx));
            labelNY->setText(QString::number(v->ny));
            labelNZ->setText(QString::number(v->nz));
        }
    private:
        QLabel* labelPX;
        QLabel* labelPY;
        QLabel* labelPZ;
        QLabel* labelNX;
        QLabel* labelNY;
        QLabel* labelNZ;
    };
    VariantDataWidgetFactoryRegistration<OrientedPointBaseWidget> registerOrientedPointBaseWidget {OrientedPointBase::ice_staticId()};

    class FramedOrientedPointBaseWidget: public VariantDataWidgetBase
    {
    public:

        FramedOrientedPointBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelAg = new QLabel;
            labelFr = new QLabel;
            labelPX = new QLabel;
            labelPY = new QLabel;
            labelPZ = new QLabel;
            labelNX = new QLabel;
            labelNY = new QLabel;
            labelNZ = new QLabel;
            l->addRow("Agent", labelAg);
            l->addRow("Frame", labelFr);
            l->addRow("PX", labelPX);
            l->addRow("PY", labelPY);
            l->addRow("PZ", labelPZ);
            l->addRow("NX", labelNX);
            l->addRow("NY", labelNY);
            l->addRow("NZ", labelNZ);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            FramedOrientedPointBasePtr v = FramedOrientedPointBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelAg->setText(QString::fromStdString(v->agent));
            labelFr->setText(QString::fromStdString(v->frame));
            labelPX->setText(QString::number(v->px));
            labelPY->setText(QString::number(v->py));
            labelPZ->setText(QString::number(v->pz));
            labelNX->setText(QString::number(v->nx));
            labelNY->setText(QString::number(v->ny));
            labelNZ->setText(QString::number(v->nz));
        }
    private:
        QLabel* labelAg;
        QLabel* labelFr;
        QLabel* labelPX;
        QLabel* labelPY;
        QLabel* labelPZ;
        QLabel* labelNX;
        QLabel* labelNY;
        QLabel* labelNZ;
    };
    VariantDataWidgetFactoryRegistration<FramedOrientedPointBaseWidget> registerFramedOrientedPointBaseWidget {FramedOrientedPointBase::ice_staticId()};

    class QuaternionBaseWidget: public VariantDataWidgetBase
    {
    public:

        QuaternionBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelQW = new QLabel;
            labelQX = new QLabel;
            labelQY = new QLabel;
            labelQZ = new QLabel;
            l->addRow("QW", labelQW);
            l->addRow("QX", labelQX);
            l->addRow("QY", labelQY);
            l->addRow("QZ", labelQZ);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            QuaternionBasePtr v = QuaternionBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelQW->setText(QString::number(v->qw));
            labelQX->setText(QString::number(v->qx));
            labelQY->setText(QString::number(v->qy));
            labelQZ->setText(QString::number(v->qz));
        }
    private:
        QLabel* labelQW;
        QLabel* labelQX;
        QLabel* labelQY;
        QLabel* labelQZ;
    };
    VariantDataWidgetFactoryRegistration<QuaternionBaseWidget> registerQuaternionBaseWidget {QuaternionBase::ice_staticId()};

    class FramedOrientationBaseWidget: public VariantDataWidgetBase
    {
    public:

        FramedOrientationBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelAg = new QLabel;
            labelFr = new QLabel;
            labelQW = new QLabel;
            labelQX = new QLabel;
            labelQY = new QLabel;
            labelQZ = new QLabel;
            l->addRow("Agent", labelAg);
            l->addRow("Frame", labelFr);
            l->addRow("QW", labelQW);
            l->addRow("QX", labelQX);
            l->addRow("QY", labelQY);
            l->addRow("QZ", labelQZ);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            FramedOrientationBasePtr v = FramedOrientationBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelAg->setText(QString::fromStdString(v->agent));
            labelFr->setText(QString::fromStdString(v->frame));
            labelQW->setText(QString::number(v->qw));
            labelQX->setText(QString::number(v->qx));
            labelQY->setText(QString::number(v->qy));
            labelQZ->setText(QString::number(v->qz));
        }
    private:
        QLabel* labelAg;
        QLabel* labelFr;
        QLabel* labelQW;
        QLabel* labelQX;
        QLabel* labelQY;
        QLabel* labelQZ;
    };
    VariantDataWidgetFactoryRegistration<FramedOrientationBaseWidget> registerFramedOrientationBaseWidget {FramedOrientationBase::ice_staticId()};

    class PoseBaseWidget: public VariantDataWidgetBase
    {
    public:

        PoseBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelX = new QLabel;
            labelY = new QLabel;
            labelZ = new QLabel;
            labelQW = new QLabel;
            labelQX = new QLabel;
            labelQY = new QLabel;
            labelQZ = new QLabel;
            l->addRow("X", labelX);
            l->addRow("Y", labelY);
            l->addRow("Z", labelZ);
            l->addRow("QW", labelQW);
            l->addRow("QX", labelQX);
            l->addRow("QY", labelQY);
            l->addRow("QZ", labelQZ);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            PoseBasePtr v = PoseBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            ARMARX_CHECK_EXPRESSION(v->position);
            ARMARX_CHECK_EXPRESSION(v->orientation);
            labelX->setText(QString::number(v->position->x));
            labelY->setText(QString::number(v->position->y));
            labelZ->setText(QString::number(v->position->z));
            labelQW->setText(QString::number(v->orientation->qw));
            labelQX->setText(QString::number(v->orientation->qx));
            labelQY->setText(QString::number(v->orientation->qy));
            labelQZ->setText(QString::number(v->orientation->qz));
        }
    private:
        QLabel* labelX;
        QLabel* labelY;
        QLabel* labelZ;
        QLabel* labelQW;
        QLabel* labelQX;
        QLabel* labelQY;
        QLabel* labelQZ;
    };
    VariantDataWidgetFactoryRegistration<PoseBaseWidget> registerPoseBaseWidget {PoseBase::ice_staticId()};

    class FramedPoseBaseWidget: public VariantDataWidgetBase
    {
    public:

        FramedPoseBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelAg = new QLabel;
            labelFr = new QLabel;
            labelX = new QLabel;
            labelY = new QLabel;
            labelZ = new QLabel;
            labelQW = new QLabel;
            labelQX = new QLabel;
            labelQY = new QLabel;
            labelQZ = new QLabel;
            l->addRow("Agent", labelAg);
            l->addRow("Frame", labelFr);
            l->addRow("X", labelX);
            l->addRow("Y", labelY);
            l->addRow("Z", labelZ);
            l->addRow("QW", labelQW);
            l->addRow("QX", labelQX);
            l->addRow("QY", labelQY);
            l->addRow("QZ", labelQZ);
            update(v);
        }
        void update(const VariantDataPtr& p) override
        {
            FramedPoseBasePtr v = FramedPoseBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            ARMARX_CHECK_EXPRESSION(v->position);
            ARMARX_CHECK_EXPRESSION(v->orientation);
            labelAg->setText(QString::fromStdString(v->agent));
            labelFr->setText(QString::fromStdString(v->frame));
            labelX->setText(QString::number(v->position->x));
            labelY->setText(QString::number(v->position->y));
            labelZ->setText(QString::number(v->position->z));
            labelQW->setText(QString::number(v->orientation->qw));
            labelQX->setText(QString::number(v->orientation->qx));
            labelQY->setText(QString::number(v->orientation->qy));
            labelQZ->setText(QString::number(v->orientation->qz));
        }
    private:
        QLabel* labelAg;
        QLabel* labelFr;
        QLabel* labelX;
        QLabel* labelY;
        QLabel* labelZ;
        QLabel* labelQW;
        QLabel* labelQX;
        QLabel* labelQY;
        QLabel* labelQZ;
    };
    VariantDataWidgetFactoryRegistration<FramedPoseBaseWidget> registerFramedPoseBaseWidget {FramedPoseBase::ice_staticId()};
}
