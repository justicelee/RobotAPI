#include "ObjectPose.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotConfig.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>

#include "ice_conversions.h"


namespace armarx::objpose
{
    ObjectPose::ObjectPose()
    {
    }

    ObjectPose::ObjectPose(const data::ObjectPose& ice)
    {
        fromIce(ice);
    }

    void ObjectPose::fromIce(const data::ObjectPose& ice)
    {
        providerName = ice.providerName;
        objectType = ice.objectType;
        armarx::fromIce(ice.objectID, objectID);

        objectPoseRobot = ::armarx::fromIce(ice.objectPoseRobot);
        objectPoseGlobal = ::armarx::fromIce(ice.objectPoseGlobal);
        objectPoseOriginal = ::armarx::fromIce(ice.objectPoseOriginal);
        objectPoseOriginalFrame = ice.objectPoseOriginalFrame;

        robotConfig = ice.robotConfig;
        robotPose = ::armarx::fromIce(ice.robotPose);

        objpose::fromIce(ice.attachment, this->attachment);

        confidence = ice.confidence;
        timestamp = IceUtil::Time::microSeconds(ice.timestampMicroSeconds);

        objpose::fromIce(ice.localOOBB, localOOBB);
    }

    data::ObjectPose ObjectPose::toIce() const
    {
        data::ObjectPose ice;
        toIce(ice);
        return ice;
    }

    void ObjectPose::toIce(data::ObjectPose& ice) const
    {
        ice.providerName = providerName;
        ice.objectType = objectType;
        armarx::toIce(ice.objectID, objectID);

        ice.objectPoseRobot = new Pose(objectPoseRobot);
        ice.objectPoseGlobal = new Pose(objectPoseGlobal);
        ice.objectPoseOriginal = new Pose(objectPoseOriginal);
        ice.objectPoseOriginalFrame = objectPoseOriginalFrame;

        ice.robotConfig = robotConfig;
        ice.robotPose = new Pose(robotPose);

        objpose::toIce(ice.attachment, this->attachment);

        ice.confidence = confidence;
        ice.timestampMicroSeconds = timestamp.toMicroSeconds();

        objpose::toIce(ice.localOOBB, localOOBB);
    }

    void ObjectPose::fromProvidedPose(const data::ProvidedObjectPose& provided, VirtualRobot::RobotPtr robot)
    {
        providerName = provided.providerName;
        objectType = provided.objectType;
        armarx::fromIce(provided.objectID, objectID);

        objectPoseOriginal = ::armarx::fromIce(provided.objectPose);
        objectPoseOriginalFrame = provided.objectPoseFrame;

        armarx::FramedPose framed(objectPoseOriginal, objectPoseOriginalFrame, robot->getName());
        framed.changeFrame(robot, robot->getRootNode()->getName());
        objectPoseRobot = framed.toEigen();
        framed.changeToGlobal(robot);
        objectPoseGlobal = framed.toEigen();

        robotConfig = robot->getConfig()->getRobotNodeJointValueMap();
        robotPose = robot->getGlobalPose();

        attachment = std::nullopt;

        confidence = provided.confidence;
        timestamp = IceUtil::Time::microSeconds(provided.timestampMicroSeconds);

        objpose::fromIce(provided.localOOBB, localOOBB);
    }


    std::optional<simox::OrientedBoxf> ObjectPose::oobbRobot() const
    {
        if (localOOBB)
        {
            return localOOBB->transformed(objectPoseRobot);
        }
        return std::nullopt;
    }

    std::optional<simox::OrientedBoxf> ObjectPose::oobbGlobal() const
    {
        if (localOOBB)
        {
            return localOOBB->transformed(objectPoseGlobal);
        }
        return std::nullopt;
    }

}

namespace armarx
{

    void objpose::fromIce(const data::ObjectAttachmentInfo& ice, ObjectAttachmentInfo& attachment)
    {
        attachment.agentName = ice.agentName;
        attachment.frameName = ice.frameName;
        attachment.poseInFrame = ::armarx::fromIce(ice.poseInFrame);
    }

    void objpose::fromIce(const data::ObjectAttachmentInfoPtr& ice, std::optional<ObjectAttachmentInfo>& attachment)
    {
        if (ice)
        {
            attachment = ObjectAttachmentInfo();
            fromIce(*ice, *attachment);
        }
        else
        {
            attachment = std::nullopt;
        }
    }

    std::optional<objpose::ObjectAttachmentInfo> objpose::fromIce(const data::ObjectAttachmentInfoPtr& ice)
    {
        if (!ice)
        {
            return std::nullopt;
        }
        objpose::ObjectAttachmentInfo info;
        fromIce(*ice, info);
        return info;
    }


    void objpose::toIce(data::ObjectAttachmentInfo& ice, const ObjectAttachmentInfo& attachment)
    {
        ice.agentName = attachment.agentName;
        ice.frameName = attachment.frameName;
        ice.poseInFrame = new Pose(attachment.poseInFrame);
    }

    void objpose::toIce(data::ObjectAttachmentInfoPtr& ice, const std::optional<ObjectAttachmentInfo>& attachment)
    {
        if (attachment)
        {
            ice = new objpose::data::ObjectAttachmentInfo();
            toIce(*ice, *attachment);
        }
        else
        {
            ice = nullptr;
        }
    }

    objpose::data::ObjectAttachmentInfoPtr objpose::toIce(const std::optional<ObjectAttachmentInfo>& attachment)
    {
        if (!attachment)
        {
            return nullptr;
        }
        objpose::data::ObjectAttachmentInfoPtr ice = new objpose::data::ObjectAttachmentInfo();
        toIce(*ice, *attachment);
        return ice;
    }


    void objpose::fromIce(const data::ObjectPose& ice, ObjectPose& pose)
    {
        pose.fromIce(ice);
    }
    objpose::ObjectPose objpose::fromIce(const data::ObjectPose& ice)
    {
        return ObjectPose(ice);
    }

    void objpose::fromIce(const data::ObjectPoseSeq& ice, ObjectPoseSeq& poses)
    {
        poses.clear();
        poses.reserve(ice.size());
        for (const auto& i : ice)
        {
            poses.emplace_back(i);
        }
    }
    objpose::ObjectPoseSeq objpose::fromIce(const data::ObjectPoseSeq& ice)
    {
        ObjectPoseSeq poses;
        fromIce(ice, poses);
        return poses;
    }


    void objpose::toIce(data::ObjectPose& ice, const ObjectPose& pose)
    {
        pose.toIce(ice);
    }
    objpose::data::ObjectPose objpose::toIce(const ObjectPose& pose)
    {
        return pose.toIce();
    }

    void objpose::toIce(data::ObjectPoseSeq& ice, const ObjectPoseSeq& poses)
    {
        ice.clear();
        ice.reserve(poses.size());
        for (const auto& p : poses)
        {
            ice.emplace_back(p.toIce());
        }
    }
    objpose::data::ObjectPoseSeq objpose::toIce(const ObjectPoseSeq& poses)
    {
        data::ObjectPoseSeq ice;
        toIce(ice, poses);
        return ice;
    }

}

