#include <VirtualRobot/XML/ObjectIO.h>

#include <SimoxUtility/filesystem/list_directory.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include "ObjectFinder.h"

namespace armarx
{
    namespace fs = std::filesystem;

    ObjectFinder::ObjectFinder(const std::string& objectsPackageName) : packageName(objectsPackageName)
    {
        Logging::setTag("ObjectFinder");
    }

    void ObjectFinder::setPath(const std::string& path)
    {
        packageName = path;
        packageDataDir.clear();
    }

    void ObjectFinder::init() const
    {
        if (packageDataDir.empty())
        {
            CMakePackageFinder packageFinder(packageName);
            packageDataDir = packageFinder.getDataDir();
            if (packageDataDir.empty())
            {
                ARMARX_WARNING << "Could not find package '" << packageName << "'.";
                // throw LocalException() << "Could not find package '" << packageName << "'.";
            }
            else
            {
                ARMARX_VERBOSE << "Objects root directory: " << _rootDirAbs();
            }
        }
    }


    std::optional<ObjectInfo> ObjectFinder::findObject(const std::string& dataset, const std::string& name) const
    {
        init();
        if (!_ready())
        {
            return std::nullopt;
        }
        if (!dataset.empty())
        {
            return ObjectInfo(packageName, packageDataDir, dataset, name);
        }
        // Search for object in datasets.
        const auto& datasets = getDatasets();
        for (const path& dataset : datasets)
        {
            if (fs::is_directory(_rootDirAbs() / dataset / name))
            {
                return ObjectInfo(packageName, packageDataDir, dataset, name);
            }
        }

        std::stringstream ss;
        ss << "Did not find object '" << name << "' in any of these datasets:\n";
        for (const path& dataset : datasets)
        {
            ss << "- " << dataset << "\n";
        }
        ss << "Objects root directory: " << _rootDirAbs();
        ARMARX_VERBOSE << ss.str();

        return std::nullopt;
    }

    std::optional<ObjectInfo> ObjectFinder::findObject(const std::string& nameOrID) const
    {
        return findObject(ObjectID(nameOrID));
    }

    std::optional<ObjectInfo> ObjectFinder::findObject(const ObjectID& id) const
    {
        return findObject(id.dataset(), id.className());
    }
    std::optional<ObjectInfo> ObjectFinder::findObject(const objpose::ObjectPose& obj) const
    {
        return findObject(obj.objectID);
    }

    std::vector<std::string> ObjectFinder::getDatasets() const
    {
        // init();  // Done by called methods.
        std::vector<std::string> datasets;
        for (const auto& dir : getDatasetDirectories())
        {
            datasets.push_back(dir.filename());
        }
        return datasets;
    }

    std::vector<ObjectFinder::path> ObjectFinder::getDatasetDirectories() const
    {
        init();
        if (!_ready())
        {
            return {};
        }
        const bool local = false;
        std::vector<path> dirs = simox::fs::list_directory(_rootDirAbs(), local);
        std::vector<path> datasetDirs;
        for (const auto& p : dirs)
        {
            if (std::filesystem::is_directory(p))
            {
                datasetDirs.push_back(p);
            }
        }
        return datasetDirs;
    }

    std::vector<ObjectInfo> ObjectFinder::findAllObjects(bool checkPaths) const
    {
        init();
        if (!_ready())
        {
            return {};
        }
        const bool local = true;
        std::vector<ObjectInfo> objects;
        for (const path& datasetDir : simox::fs::list_directory(_rootDirAbs(), local))
        {
            if (fs::is_directory(_rootDirAbs() / datasetDir))
            {
                std::vector<ObjectInfo> dataset = findAllObjectsOfDataset(datasetDir, checkPaths);
                for (const auto& o : dataset)
                {
                    objects.push_back(o);
                }
            }
        }
        return objects;
    }

    std::map<std::string, std::vector<ObjectInfo>>
            ObjectFinder::findAllObjectsByDataset(bool checkPaths) const
    {
        // init();  // Done by called methods.
        std::map<std::string, std::vector<ObjectInfo>> objects;
        for (const std::string& dataset : getDatasets())
        {
            objects[dataset] = findAllObjectsOfDataset(dataset, checkPaths);
        }
        return objects;
    }

    std::vector<ObjectInfo> ObjectFinder::findAllObjectsOfDataset(const std::string& dataset, bool checkPaths) const
    {
        init();
        if (!_ready())
        {
            return {};
        }
        path datasetDir = _rootDirAbs() / dataset;
        if (!fs::is_directory(datasetDir))
        {
            ARMARX_WARNING << "Expected dataset directory for dataset '" << dataset << "': \n"
                           << datasetDir;
            return {};
        }

        std::vector<ObjectInfo> objects;
        const bool local = true;
        for (const path& dir : simox::fs::list_directory(datasetDir, local))
        {
            if (fs::is_directory(datasetDir / dir))
            {
                ObjectInfo object(packageName, packageDataDir, dataset, dir.filename());
                if (!checkPaths || object.checkPaths())
                {
                    objects.push_back(object);
                }
            }
        }
        return objects;
    }

    VirtualRobot::ManipulationObjectPtr
    ObjectFinder::loadManipulationObject(const std::optional<ObjectInfo>& ts)
    {
        if (!ts)
        {
            return nullptr;
        }
        const auto data = ts->simoxXML();
        ArmarXDataPath::FindPackageAndAddDataPath(data.package);
        std::string abs;
        if (!ArmarXDataPath::SearchReadableFile(data.relativePath, abs))
        {
            return nullptr;
        }
        return VirtualRobot::ObjectIO::loadManipulationObject(abs);
    }
    VirtualRobot::ManipulationObjectPtr
    ObjectFinder::loadManipulationObject(const objpose::ObjectPose& obj) const
    {
        return loadManipulationObject(findObject(obj));
    }
    VirtualRobot::ObstaclePtr
    ObjectFinder::loadObstacle(const std::optional<ObjectInfo>& ts)
    {
        if (!ts)
        {
            return nullptr;
        }
        const auto data = ts->simoxXML();
        ArmarXDataPath::FindPackageAndAddDataPath(data.package);
        std::string abs;
        if (!ArmarXDataPath::SearchReadableFile(data.relativePath, abs))
        {
            return nullptr;
        }
        return VirtualRobot::ObjectIO::loadObstacle(abs);
    }
    VirtualRobot::ObstaclePtr
    ObjectFinder::loadObstacle(const objpose::ObjectPose& obj) const
    {
        return loadObstacle(findObject(obj));
    }

    ObjectFinder::path ObjectFinder::_rootDirAbs() const
    {
        return packageDataDir / packageName;
    }

    ObjectFinder::path ObjectFinder::_rootDirRel() const
    {
        return packageName;
    }

    bool ObjectFinder::_ready() const
    {
        return !packageDataDir.empty();
    }

}

