#pragma once

#include <filesystem>
#include <optional>
#include <string>

#include "ObjectID.h"


namespace simox
{
    // #include <SimoxUtility/shapes/AxisAlignedBoundingBox.h>
    struct AxisAlignedBoundingBox;
    // #include <SimoxUtility/shapes/OrientedBox.h>
    template<class FloatT> class OrientedBox;
}

namespace armarx
{

    struct PackageFileLocation
    {
        /// Name of the ArmarX package.
        std::string package;

        /// Relative to the package's data directory.
        std::string relativePath;
        /// The absolute path (in the host's file system).
        std::filesystem::path absolutePath;
    };


    /**
     * @brief Accessor for the object files.
     */
    class ObjectInfo
    {
    public:
        using path = std::filesystem::path;

    public:

        ObjectInfo(const std::string& packageName, const path& packageDataDir,
                   const ObjectID& id);
        ObjectInfo(const std::string& packageName, const path& packageDataDir,
                   const std::string& dataset, const std::string& name);


        virtual ~ObjectInfo() = default;


        std::string package() const;

        std::string dataset() const;
        std::string name() const;
        /// Return "dataset/name".
        ObjectID id() const;
        std::string idStr() const;

        PackageFileLocation file(const std::string& extension, const std::string& suffix = "") const;

        PackageFileLocation simoxXML() const;
        PackageFileLocation wavefrontObj() const;

        PackageFileLocation boundingBoxJson() const;

        /**
         * @brief Load the AABB (axis-aligned bounding-box) from the bounding box JSON file.
         * @return Return the AABB if successful, `std::nullopt` if file does not exist.
         */
        std::optional<simox::AxisAlignedBoundingBox> loadAABB() const;
        /**
         * @brief Load the OOBB (object-oriented bounding box) from the bounding box JSON file.
         * The OOBB is defined the object's local frame.
         * @return Return the OOBB if successful, `std::nullopt` if file does not exist.
         */
        std::optional<simox::OrientedBox<float>> loadOOBB() const;


        /**
         * @brief Checks the existence of expected files.
         * If a file is does not exist, emits a warning returns false.
         * @return True if all existing files are found, false otherwise.
         */
        virtual bool checkPaths() const;


    private:

        path objectDirectory() const;


    private:

        std::string _packageName;
        path _packageDataDir;

        ObjectID _id;

    };

    std::ostream& operator<<(std::ostream& os, const ObjectInfo& rhs);


    inline bool operator==(const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() == rhs.id();
    }
    inline bool operator!=(const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() != rhs.id();
    }
    inline bool operator< (const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() < rhs.id();
    }
    inline bool operator> (const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() > rhs.id();
    }
    inline bool operator<=(const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() <= rhs.id();
    }
    inline bool operator>=(const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() >= rhs.id();
    }

}
