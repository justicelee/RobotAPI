#pragma once

#include <SimoxUtility/json/json.hpp>

#include "ObjectID.h"
#include "ObjectPose.h"

namespace armarx
{
    void to_json(nlohmann::json& j, const ObjectID& value);
    void from_json(const nlohmann::json& j, ObjectID& value);
}

namespace armarx::objpose
{
    void to_json(nlohmann::json& j, const ObjectPose& op);
    void from_json(const nlohmann::json& j, ObjectPose& op);
}
