#pragma once

#include <SimoxUtility/meta/enum/EnumNames.hpp>

#include <RobotAPI/interface/ArmarXObjects/ArmarXObjectsTypes.h>
#include <RobotAPI/interface/objectpose/object_pose_types.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>

#include "ObjectPose.h"
#include "ObjectID.h"

namespace simox
{
    // #include <SimoxUtility/shapes/AxisAlignedBoundingBox.h>
    struct AxisAlignedBoundingBox;
    // #include <SimoxUtility/shapes/OrientedBox.h>
    template<class FloatT> class OrientedBox;
}

namespace armarx::data
{
    std::ostream& operator<<(std::ostream& os, const ObjectID& id);
}

namespace armarx
{

    void fromIce(const data::ObjectID& ice, ObjectID& id);
    ObjectID fromIce(const data::ObjectID& id);

    void fromIce(const data::ObjectIDSeq& ice, std::vector<ObjectID>& ids);
    std::vector<ObjectID> fromIce(const data::ObjectIDSeq& id);

    void toIce(data::ObjectID& ice, const ObjectID& ids);
    data::ObjectID toIce(const ObjectID& ids);

    void toIce(data::ObjectIDSeq& ice, const std::vector<ObjectID>& ids);
    data::ObjectIDSeq toIce(const std::vector<ObjectID>& ids);

}

namespace armarx::objpose
{
    extern const simox::meta::EnumNames<objpose::ObjectTypeEnum> ObjectTypeEnumNames;

    objpose::AABB toIce(const simox::AxisAlignedBoundingBox& aabb);

    void fromIce(const Box& box, simox::OrientedBox<float>& oobb);
    void fromIce(const BoxPtr& box, std::optional<simox::OrientedBox<float>>& oobb);
    simox::OrientedBox<float> fromIce(const Box& box);

    void toIce(BoxPtr& box, const std::optional<simox::OrientedBox<float>>& oobb);
    void toIce(Box& box, const simox::OrientedBox<float>& oobb);
    Box toIce(const simox::OrientedBox<float>& oobb);
}
