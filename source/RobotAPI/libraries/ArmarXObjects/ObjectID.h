#pragma once

#include <string>


namespace armarx
{
    /**
     * @brief A known object ID of the form "Dataset/ClassName" or "Dataset/ClassName/InstanceName".
     */
    class ObjectID
    {
    public:

        ObjectID();
        ObjectID(const std::string& dataset, const std::string& className, const std::string& instancName = "");
        /// Construct from either a class name ("myobject") or ID ("mydataset/myobject", "mydataset/myclass/myinstance").
        ObjectID(const std::string& nameOrID);


        inline std::string dataset() const
        {
            return _dataset;
        }
        inline std::string className() const
        {
            return _className;
        }
        inline std::string instanceName() const
        {
            return _instanceName;
        }
        inline void setInstanceName(const std::string& instanceName)
        {
            this->_instanceName = instanceName;
        }

        /// Return "dataset/className" or "dataset/className/instanceName".
        std::string str() const;

        /// Indicates whether dataset and class name are equal.
        bool equalClass(const ObjectID& rhs) const;

        /// Indicates whether dataset, class name and instance name are equal.
        bool operator==(const ObjectID& rhs) const;
        inline bool operator!=(const ObjectID& rhs) const
        {
            return !operator==(rhs);
        }
        bool operator< (const ObjectID& rhs) const;
        inline bool operator> (const ObjectID& rhs) const
        {
            return rhs < (*this);
        }
        inline bool operator<=(const ObjectID& rhs) const
        {
            return !operator> (rhs);
        }
        inline bool operator>=(const ObjectID& rhs) const
        {
            return !operator< (rhs);
        }


    private:

        /// The dataset name in ArmarXObjects, e.g. "KIT", "YCB", "SecondHands", ...
        std::string _dataset;
        /// The class name in ArmarXObjects, e.g. "Amicelli", "001_chips_can", ...
        std::string _className;
        /// An optional instance name, chosen by the user.
        std::string _instanceName;

    };

    std::ostream& operator<<(std::ostream& os, const ObjectID& rhs);



}


