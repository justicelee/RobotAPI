#include "ObjectInfo.h"

#include <SimoxUtility/json.h>
#include <SimoxUtility/shapes/AxisAlignedBoundingBox.h>
#include <SimoxUtility/shapes/OrientedBox.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx
{
    namespace fs = std::filesystem;


    ObjectInfo::ObjectInfo(const std::string& packageName, const ObjectInfo::path& packageDataDir,
                           const ObjectID& id) :
        _packageName(packageName), _packageDataDir(packageDataDir), _id(id)
    {
    }

    ObjectInfo::ObjectInfo(const std::string& packageName, const ObjectInfo::path& packageDataDir,
                           const std::string& dataset, const std::string& name) :
        _packageName(packageName), _packageDataDir(packageDataDir), _id(dataset, name)
    {
    }

    std::string ObjectInfo::package() const
    {
        return _packageName;
    }

    std::string ObjectInfo::dataset() const
    {
        return _id.dataset();
    }

    std::string ObjectInfo::name() const
    {
        return _id.className();
    }

    ObjectID ObjectInfo::id() const
    {
        return _id;
    }

    std::string ObjectInfo::idStr() const
    {
        return _id.str();
    }

    ObjectInfo::path ObjectInfo::objectDirectory() const
    {
        return path(_packageName) / _id.dataset() / _id.className();
    }

    PackageFileLocation ObjectInfo::file(const std::string& _extension, const std::string& suffix) const
    {
        std::string extension = _extension;
        if (extension.at(0) != '.')
        {
            extension = "." + extension;
        }
        std::string filename = _id.className() + suffix + extension;

        PackageFileLocation loc;
        loc.package = _packageName;
        loc.relativePath = objectDirectory() / filename;
        loc.absolutePath = _packageDataDir / loc.relativePath;
        return loc;
    }

    PackageFileLocation ObjectInfo::simoxXML() const
    {
        return file(".xml");
    }

    PackageFileLocation ObjectInfo::wavefrontObj() const
    {
        return file(".obj");
    }

    PackageFileLocation ObjectInfo::boundingBoxJson() const
    {
        return file(".json", "_bb");
    }

    std::optional<simox::AxisAlignedBoundingBox> ObjectInfo::loadAABB() const
    {
        nlohmann::json j;
        try
        {
            j = nlohmann::read_json(boundingBoxJson().absolutePath);
        }
        catch (const std::exception& e)
        {
            ARMARX_ERROR << e.what();
            return std::nullopt;
        }

        nlohmann::json jaabb = j.at("aabb");
        auto center = jaabb.at("center").get<Eigen::Vector3f>();
        auto extents = jaabb.at("extents").get<Eigen::Vector3f>();
        auto min = jaabb.at("min").get<Eigen::Vector3f>();
        auto max = jaabb.at("max").get<Eigen::Vector3f>();

        simox::AxisAlignedBoundingBox aabb(min, max);

        static const float prec = 1e-4f;
        ARMARX_CHECK_LESS_EQUAL((aabb.center() - center).norm(), prec) << aabb.center().transpose() << "\n" << center.transpose();
        ARMARX_CHECK_LESS_EQUAL((aabb.extents() - extents).norm(), prec) << aabb.extents().transpose() << "\n" << extents.transpose();
        ARMARX_CHECK_LESS_EQUAL((aabb.min() - min).norm(), prec) << aabb.min().transpose() << "\n" << min.transpose();
        ARMARX_CHECK_LESS_EQUAL((aabb.max() - max).norm(), prec) << aabb.max().transpose() << "\n" << max.transpose();

        return aabb;
    }

    std::optional<simox::OrientedBox<float>> ObjectInfo::loadOOBB() const
    {
        nlohmann::json j;
        try
        {
            j = nlohmann::read_json(boundingBoxJson().absolutePath);
        }
        catch (const std::exception& e)
        {
            ARMARX_ERROR << e.what();
            return std::nullopt;
        }

        nlohmann::json joobb = j.at("oobb");
        auto pos = joobb.at("pos").get<Eigen::Vector3f>();
        auto ori = joobb.at("ori").get<Eigen::Quaternionf>().toRotationMatrix();
        auto extents = joobb.at("extents").get<Eigen::Vector3f>();

        Eigen::Vector3f corner = pos - ori * extents / 2;

        simox::OrientedBox<float> oobb(corner,
                                       ori.col(0) * extents(0),
                                       ori.col(1) * extents(1),
                                       ori.col(2) * extents(2));

        static const float prec = 1e-3f;
        ARMARX_CHECK_LESS_EQUAL((oobb.center() - pos).norm(), prec) << oobb.center().transpose() << "\n" << pos.transpose();
        ARMARX_CHECK(oobb.rotation().isApprox(ori, prec)) << oobb.rotation() << "\n" << ori;
        ARMARX_CHECK_LESS_EQUAL((oobb.dimensions() - extents).norm(), prec) << oobb.dimensions().transpose() << "\n" << extents.transpose();
        return oobb;
    }

    bool ObjectInfo::checkPaths() const
    {
        namespace fs = std::filesystem;
        bool result = true;

        if (!fs::is_regular_file(simoxXML().absolutePath))
        {
            ARMARX_WARNING << "Expected simox object file for object '" << *this << "': " << simoxXML().absolutePath;
            result = false;
        }
        if (!fs::is_regular_file(wavefrontObj().absolutePath))
        {
            ARMARX_WARNING << "Expected wavefront object file (.obj) for object '" << *this << "': " << wavefrontObj().absolutePath;
            result = false;
        }

        return result;
    }

}


std::ostream& armarx::operator<<(std::ostream& os, const ObjectInfo& rhs)
{
    return os << rhs.id();
}


