#include "json_conversions.h"

#include <SimoxUtility/json/eigen_conversion.h>
#include <SimoxUtility/shapes/json_conversions.h>

#include "ice_conversions.h"

void armarx::to_json(nlohmann::json& j, const ObjectID& id)
{
    j["dataset"] = id.dataset();
    j["className"] = id.className();
    j["instanceName"] = id.instanceName();
    j["str"] = id.str();
}

void armarx::from_json(const nlohmann::json& j, ObjectID& id)
{
    id =
    {
        j.at("dataset").get<std::string>(),
        j.at("className").get<std::string>(),
        j.at("instanceName").get<std::string>()
    };
}

void armarx::objpose::to_json(nlohmann::json& j, const ObjectPose& op)
{
    j["providerName"] = op.providerName;
    j["objectType"] = ObjectTypeEnumNames.to_name(op.objectType);

    j["objectID"] = op.objectID;

    j["objectPoseRobot"] = op.objectPoseRobot;
    j["objectPoseGlobal"] = op.objectPoseGlobal;
    j["objectPoseOriginal"] = op.objectPoseOriginal;
    j["objectPoseOriginalFrame"] = op.objectPoseOriginalFrame;

    j["robotConfig"] = op.robotConfig;
    j["robotPose"] = op.robotPose;

    j["confidence"] = op.confidence;
    j["timestampMicroSeconds"] = op.timestamp.toMicroSeconds();

    if (op.localOOBB)
    {
        j["localOOBB"] = *op.localOOBB;
    }
}


void armarx::objpose::from_json(const nlohmann::json& j, ObjectPose& op)
{
    op.providerName = j.at("providerName");
    op.objectType = ObjectTypeEnumNames.from_name(j.at("objectType"));

    op.objectID = j.at("objectID");

    op.objectPoseRobot = j.at("objectPoseRobot");
    op.objectPoseGlobal = j.at("objectPoseGlobal");
    op.objectPoseOriginal = j.at("objectPoseOriginal");
    op.objectPoseOriginalFrame = j.at("objectPoseOriginalFrame");

    op.robotConfig = j.at("robotConfig").get<std::map<std::string, float>>();
    op.robotPose = j.at("robotPose");

    op.confidence = j.at("confidence");
    op.timestamp = IceUtil::Time::microSeconds(j.at("timestampMicroSeconds"));

    if (j.count("localOOBB"))
    {
        op.localOOBB = j.at("localOOBB").get<simox::OrientedBoxf>();
    }
}
