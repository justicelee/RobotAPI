#pragma once

#include <optional>

#include <Eigen/Core>

#include <SimoxUtility/shapes/OrientedBox.h>
#include <VirtualRobot/VirtualRobot.h>

#include <IceUtil/Time.h>

#include <RobotAPI/interface/objectpose/object_pose_types.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>


namespace armarx::objpose
{

    struct ObjectAttachmentInfo
    {
        std::string frameName;
        std::string agentName;
        Eigen::Matrix4f poseInFrame;
    };


    /**
     * @brief An object pose as stored by the ObjectPoseObserver.
     */
    struct ObjectPose
    {
        ObjectPose();
        ObjectPose(const data::ObjectPose& ice);

        void fromIce(const data::ObjectPose& ice);

        data::ObjectPose toIce() const;
        void toIce(data::ObjectPose& ice) const;

        void fromProvidedPose(const data::ProvidedObjectPose& provided, VirtualRobot::RobotPtr robot);


        /// Name of the providing component.
        std::string providerName;
        /// Known or unknown object.
        ObjectTypeEnum objectType = AnyObject;

        /// The object ID, i.e. dataset and name.
        armarx::ObjectID objectID;

        Eigen::Matrix4f objectPoseRobot;
        Eigen::Matrix4f objectPoseGlobal;
        Eigen::Matrix4f objectPoseOriginal;
        std::string objectPoseOriginalFrame;

        std::map<std::string, float> robotConfig;
        Eigen::Matrix4f robotPose;

        std::optional<ObjectAttachmentInfo> attachment;

        /// Confidence in [0, 1] (1 = full, 0 = none).
        float confidence = 0;
        /// Source timestamp.
        IceUtil::Time timestamp = IceUtil::Time::microSeconds(-1);

        /// Object bounding box in object's local coordinate frame.
        /// @see oobbRobot(), oobbGlobal()
        std::optional<simox::OrientedBoxf> localOOBB;


        /// Get the OOBB in the robot frame (according to `objectPoseRobot`).
        std::optional<simox::OrientedBoxf> oobbRobot() const;
        /// Get the OOBB in the global frame (according to `objectPoseGlobal`).
        std::optional<simox::OrientedBoxf> oobbGlobal() const;

    };
    using ObjectPoseSeq = std::vector<ObjectPose>;


    void fromIce(const data::ObjectAttachmentInfo& ice, ObjectAttachmentInfo& attachment);
    void fromIce(const data::ObjectAttachmentInfoPtr& ice, std::optional<ObjectAttachmentInfo>& attachment);
    std::optional<ObjectAttachmentInfo> fromIce(const data::ObjectAttachmentInfoPtr& ice);

    void fromIce(const data::ObjectPose& ice, ObjectPose& pose);
    ObjectPose fromIce(const data::ObjectPose& ice);

    void fromIce(const data::ObjectPoseSeq& ice, ObjectPoseSeq& poses);
    ObjectPoseSeq fromIce(const data::ObjectPoseSeq& ice);


    void toIce(data::ObjectAttachmentInfo& ice, const ObjectAttachmentInfo& attachment);
    void toIce(data::ObjectAttachmentInfoPtr& ice, const std::optional<ObjectAttachmentInfo>& attachment);
    data::ObjectAttachmentInfoPtr toIce(const std::optional<ObjectAttachmentInfo>& ice);

    void toIce(data::ObjectPose& ice, const ObjectPose& pose);
    data::ObjectPose toIce(const ObjectPose& pose);

    void toIce(data::ObjectPoseSeq& ice, const ObjectPoseSeq& poses);
    data::ObjectPoseSeq toIce(const ObjectPoseSeq& poses);

}
