/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotAPIComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

#include "RobotStateComponentPlugin.h"

namespace armarx::plugins
{
    /**
     * @class DebugDrawerHelperComponentPlugin
     * @ingroup Library-RobotAPIComponentPlugins
     * @brief Brief description of class RobotAPIComponentPlugins.
     *
     * Detailed description of class RobotAPIComponentPlugins.
     */
    class DebugDrawerHelperComponentPlugin : public ComponentPlugin
    {
    public:
        DebugDrawerHelperComponentPlugin(ManagedIceObject& parent, std::string pre);
        using ComponentPlugin::ComponentPlugin;

        //helper
    public:
        const DebugDrawerHelper& getDebugDrawerHelper() const;
        DebugDrawerHelper& getDebugDrawerHelper();

        const DebugDrawerInterfacePrx& getDebugDrawerTopic() const;

        //other
    public:
        std::unique_lock<std::recursive_mutex> getDebugDrawerHelperLock() const;
        std::string getDebugDrawerLayerName() const;

        VirtualRobot::RobotPtr getDebugDrawerHelperRobot() const;

        bool synchronizeDebugDrawerHelperRobot() const;
        bool synchronizeDebugDrawerHelperRobot(Ice::Long timestamp) const;
        bool synchronizeDebugDrawerHelperRobot(const RobotStateConfig& state) const;

        void setClearLayersOnDisconnect(bool b);
        //hooks
    protected:
        void preOnInitComponent() override;
        void preOnConnectComponent() override;
        void postOnDisconnectComponent() override;
        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        //data
    private:
        using RobotStateComponentPlugin = armarx::plugins::RobotStateComponentPlugin;
        static constexpr auto               _propertyName = "DebugDrawerTopicName";
        std::string                         _pre;
        mutable std::recursive_mutex        _debugDrawerHelperMutex;
        std::unique_ptr<DebugDrawerHelper>  _debugDrawerHelper;
        DebugDrawerInterfacePrx             _debugDrawerTopic;
        RobotStateComponentPlugin*          _robotStateComponentPlugin{nullptr};
        bool                                _doNotClearLayersOnDisconnect = false;
    };
}

namespace armarx
{
    class DebugDrawerHelperComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        using DebugDrawerHelperComponentPlugin = armarx::plugins::DebugDrawerHelperComponentPlugin;
    private:
        DebugDrawerHelperComponentPlugin* _debugDrawerHelperComponentPlugin{nullptr};
    public:
        DebugDrawerHelperComponentPluginUser();

        const DebugDrawerHelperComponentPlugin& getDebugDrawerHelperPlugin() const;
        DebugDrawerHelperComponentPlugin& getDebugDrawerHelperPlugin();

        const DebugDrawerHelper& getDebugDrawerHelper() const;
        DebugDrawerHelper& getDebugDrawerHelper();

        const DebugDrawerInterfacePrx& getDebugDrawerTopic() const;

        //other
    public:
        std::unique_lock<std::recursive_mutex> getDebugDrawerHelperLock() const;
        std::string getDebugDrawerLayerName() const;

        VirtualRobot::RobotPtr getDebugDrawerHelperRobot() const;

        bool synchronizeDebugDrawerHelperRobot() const;
        bool synchronizeDebugDrawerHelperRobot(Ice::Long timestamp) const;
        bool synchronizeDebugDrawerHelperRobot(const RobotStateConfig& state) const;
    };
}
