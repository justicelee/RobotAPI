#pragma once

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/components/ArViz/Client/Client.h>

namespace armarx::plugins
{
    class ArVizComponentPlugin : public ComponentPlugin
    {
    public:
        using ComponentPlugin::ComponentPlugin;

        void preOnInitComponent() override;

        void preOnConnectComponent() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        armarx::viz::Client createClient();

        std::string getTopicName();

    private:
        static constexpr const char* PROPERTY_NAME = "ArVizTopicName";
        static constexpr const char* PROPERTY_DEFAULT = "ArVizTopic";
    };
}

namespace armarx
{
    /**
     * @brief Provides a ready-to-use ArViz client `arviz` as member variable.
     */
    class ArVizComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        ArVizComponentPluginUser();

        armarx::viz::Client createArVizClient();

        armarx::viz::Client arviz;

        armarx::viz::Client& getArvizClient()
        {
            if (!arviz.topic())
            {
                arviz = createArVizClient();
            }
            return arviz;
        }

    private:
        armarx::plugins::ArVizComponentPlugin* plugin = nullptr;
    };
}


namespace armarx::plugins
{
    // Legacy typedef.
    using ArVizComponentPluginUser = armarx::ArVizComponentPluginUser;
}
