#include "TrajectoryPlayerComponentPlugin.h"

namespace armarx
{
    namespace plugins
    {

        void TrajectoryPlayerComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void TrajectoryPlayerComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_trajectoryPlayer, PROPERTY_NAME);
        }

        void TrajectoryPlayerComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "TrajectoryPlayer",
                    "Name of the TrajectoryPlayer");
            }
        }

        TrajectoryPlayerInterfacePrx TrajectoryPlayerComponentPlugin::getTrajectoryPlayer()
        {
            return _trajectoryPlayer;
        }


    }
}

namespace armarx
{

    TrajectoryPlayerComponentPluginUser::TrajectoryPlayerComponentPluginUser()
    {
        addPlugin(plugin);
    }

    TrajectoryPlayerInterfacePrx TrajectoryPlayerComponentPluginUser::getTrajectoryPlayer()
    {
        return plugin->getTrajectoryPlayer();
    }


}


