#include "RobotUnitComponentPlugin.h"

namespace armarx::plugins
{
    void RobotUnitComponentPlugin::preOnInitComponent()
    {
        if (_robotUnitName.empty())
        {
            parent<Component>().getProperty(_robotUnitName, PROPERTY_NAME);
        }
        parent<Component>().usingProxy(_robotUnitName);
    }

    void RobotUnitComponentPlugin::preOnConnectComponent()
    {
        parent<Component>().getProxy(_robotUnit, _robotUnitName);
    }

    void RobotUnitComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(PROPERTY_NAME))
        {
            properties->defineRequiredProperty<std::string>(
                PROPERTY_NAME,
                "Name of the RobotUnit");
        }
    }

    RobotUnitInterfacePrx RobotUnitComponentPlugin::getRobotUnit() const
    {
        return _robotUnit;
    }

    void RobotUnitComponentPlugin::setRobotUnitName(const std::string& name)
    {
        ARMARX_CHECK_NOT_EMPTY(name);
        ARMARX_CHECK_EMPTY(_robotUnitName);
        _robotUnitName = name;
    }
    const std::string& RobotUnitComponentPlugin::getRobotUnitName() const
    {
        return _robotUnitName;
    }
}

namespace armarx
{
    RobotUnitComponentPluginUser::RobotUnitComponentPluginUser()
    {
        addPlugin(plugin);
    }

    RobotUnitInterfacePrx RobotUnitComponentPluginUser::getRobotUnit() const
    {
        return plugin->getRobotUnit();
    }

    plugins::RobotUnitComponentPlugin& RobotUnitComponentPluginUser::getRobotUnitComponentPlugin()
    {
        return *plugin;
    }
}


