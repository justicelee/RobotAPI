#pragma once

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/observers/GraspCandidateObserverInterface.h>

namespace armarx
{
    namespace plugins
    {
        class GraspCandidateObserverComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent()        override;
            void preOnConnectComponent()     override;
            void postOnDisconnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            grasping::GraspCandidateObserverInterfacePrx getGraspCandidateObserver();

            void setGraspCandidateObserverName(const std::string& name);
            const std::string& getGraspCandidateObserverName() const;

        private:
            static constexpr const char* PROPERTY_NAME = "GraspCandidateObserverName";
            std::string                                  _graspCandidateObserverName;
            grasping::GraspCandidateObserverInterfacePrx _graspCandidateObserver;
        };
    }
}

namespace armarx
{
    /**
     * @brief Provides a ready-to-use GraspCandidateObserver.
     */
    class GraspCandidateObserverComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        GraspCandidateObserverComponentPluginUser();
        grasping::GraspCandidateObserverInterfacePrx getGraspCandidateObserver();

        armarx::plugins::GraspCandidateObserverComponentPlugin& getGraspCandidateObserverComponentPlugin();
        const armarx::plugins::GraspCandidateObserverComponentPlugin& getGraspCandidateObserverComponentPlugin() const;

    private:
        armarx::plugins::GraspCandidateObserverComponentPlugin* plugin = nullptr;
    };
}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using GraspCandidateObserverComponentPluginUser = armarx::GraspCandidateObserverComponentPluginUser;
    }
}
