#include "HandUnitComponentPlugin.h"

namespace armarx
{
    namespace plugins
    {

        void HandUnitComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void HandUnitComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_handUnit, PROPERTY_NAME);
        }

        void HandUnitComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "HandUnit",
                    "Name of the HandUnit");
            }
        }

        HandUnitInterfacePrx HandUnitComponentPlugin::getHandUnit()
        {
            return _handUnit;
        }


    }
}

namespace armarx
{

    HandUnitComponentPluginUser::HandUnitComponentPluginUser()
    {
        addPlugin(plugin);
    }

    HandUnitInterfacePrx HandUnitComponentPluginUser::getHandUnit()
    {
        return plugin->getHandUnit();
    }


}


