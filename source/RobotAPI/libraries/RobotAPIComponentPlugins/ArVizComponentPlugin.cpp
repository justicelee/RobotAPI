#include "ArVizComponentPlugin.h"

namespace armarx::plugins
{
    std::string ArVizComponentPlugin::getTopicName()
    {
        return parentDerives<Component>() ?
               parent<Component>().getProperty<std::string>(makePropertyName(PROPERTY_NAME)) :
               std::string{PROPERTY_DEFAULT};
    }

    void ArVizComponentPlugin::preOnInitComponent()
    {
        parent().offeringTopic(getTopicName());
    }

    void ArVizComponentPlugin::preOnConnectComponent()
    {
        parent<ArVizComponentPluginUser>().arviz = createClient();
    }

    void ArVizComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(PROPERTY_NAME)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(PROPERTY_NAME),
                PROPERTY_DEFAULT,
                "Name of the ArViz topic");
        }
    }

    viz::Client ArVizComponentPlugin::createClient()
    {
        return viz::Client(parent(), getTopicName());
    }
}

namespace armarx
{
    ArVizComponentPluginUser::ArVizComponentPluginUser()
    {
        addPlugin(plugin);
    }

    viz::Client ArVizComponentPluginUser::createArVizClient()
    {
        return plugin->createClient();
    }
}
