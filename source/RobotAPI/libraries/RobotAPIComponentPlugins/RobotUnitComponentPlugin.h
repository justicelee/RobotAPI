#pragma once

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>


namespace armarx
{
    namespace plugins
    {

        class RobotUnitComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            RobotUnitInterfacePrx getRobotUnit() const;

            void setRobotUnitName(const std::string& name);
            const std::string& getRobotUnitName() const;
        private:
            static constexpr const char* PROPERTY_NAME = "RobotUnitName";
            RobotUnitInterfacePrx _robotUnit;
            std::string           _robotUnitName;
        };

    }
}

namespace armarx
{
    /**
     * @brief Provides a ready-to-use RobotUnit.
     */
    class RobotUnitComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        RobotUnitComponentPluginUser();
        RobotUnitInterfacePrx getRobotUnit() const;
        plugins::RobotUnitComponentPlugin& getRobotUnitComponentPlugin();
    private:
        armarx::plugins::RobotUnitComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using RobotUnitComponentPluginUser = armarx::RobotUnitComponentPluginUser;
    }
}
