#pragma once

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/components/NaturalIKInterface.h>


namespace armarx
{
    namespace plugins
    {

        class NaturalIKComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            NaturalIKInterfacePrx getNaturalIK();

        private:
            static constexpr const char* PROPERTY_NAME = "NaturalIKName";
            NaturalIKInterfacePrx _naturalIK;
        };

    }
}

namespace armarx
{
    /**
     * @brief Provides a ready-to-use NaturalIK.
     */
    class NaturalIKComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        NaturalIKComponentPluginUser();
        NaturalIKInterfacePrx getNaturalIK();

    private:
        armarx::plugins::NaturalIKComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using NaturalIKComponentPluginUser = armarx::NaturalIKComponentPluginUser;
    }
}
