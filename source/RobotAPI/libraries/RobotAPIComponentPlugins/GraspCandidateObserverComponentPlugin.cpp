#include "GraspCandidateObserverComponentPlugin.h"

namespace armarx
{
    namespace plugins
    {

        void GraspCandidateObserverComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
            if (!_graspCandidateObserver && _graspCandidateObserverName.empty())
            {
                parent<Component>().getProperty(_graspCandidateObserverName, makePropertyName(PROPERTY_NAME));
            }

            if (!_graspCandidateObserver)
            {
                parent<Component>().usingProxy(_graspCandidateObserverName);
            }
        }

        void GraspCandidateObserverComponentPlugin::preOnConnectComponent()
        {
            if (!_graspCandidateObserver)
            {
                parent<Component>().getProxy(_graspCandidateObserver, _graspCandidateObserverName);
            }
        }

        void GraspCandidateObserverComponentPlugin::postOnDisconnectComponent()
        {
            _graspCandidateObserver = nullptr;
        }

        void GraspCandidateObserverComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "GraspCandidateObserver",
                    "Name of the GraspCandidateObserver");
            }
        }

        grasping::GraspCandidateObserverInterfacePrx GraspCandidateObserverComponentPlugin::getGraspCandidateObserver()
        {
            return _graspCandidateObserver;
        }

        const std::string& GraspCandidateObserverComponentPlugin::getGraspCandidateObserverName() const
        {
            return _graspCandidateObserverName;
        }

        void GraspCandidateObserverComponentPlugin::setGraspCandidateObserverName(const std::string& name)
        {
            _graspCandidateObserverName = name;
        }
    }
}

namespace armarx
{
    GraspCandidateObserverComponentPluginUser::GraspCandidateObserverComponentPluginUser()
    {
        addPlugin(plugin);
    }

    grasping::GraspCandidateObserverInterfacePrx GraspCandidateObserverComponentPluginUser::getGraspCandidateObserver()
    {
        return plugin->getGraspCandidateObserver();
    }

    plugins::GraspCandidateObserverComponentPlugin& GraspCandidateObserverComponentPluginUser::getGraspCandidateObserverComponentPlugin()
    {
        ARMARX_CHECK_NOT_NULL(plugin);
        return *plugin;
    }

    const plugins::GraspCandidateObserverComponentPlugin& GraspCandidateObserverComponentPluginUser::getGraspCandidateObserverComponentPlugin() const
    {
        ARMARX_CHECK_NOT_NULL(plugin);
        return *plugin;
    }
}
