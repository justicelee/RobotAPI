#include "CartesianPositionControlComponentPlugin.h"

namespace armarx
{
    namespace plugins
    {

        void CartesianPositionControlComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void CartesianPositionControlComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_cartesianPositionControl, PROPERTY_NAME);
        }

        void CartesianPositionControlComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineRequiredProperty<std::string>(
                    PROPERTY_NAME,
                    "Name of the CartesianPositionControl");
            }
        }

        CartesianPositionControlInterfacePrx CartesianPositionControlComponentPlugin::getCartesianPositionControl()
        {
            return _cartesianPositionControl;
        }


    }
}

namespace armarx
{

    CartesianPositionControlComponentPluginUser::CartesianPositionControlComponentPluginUser()
    {
        addPlugin(plugin);
    }

    CartesianPositionControlInterfacePrx CartesianPositionControlComponentPluginUser::getCartesianPositionControl()
    {
        return plugin->getCartesianPositionControl();
    }


}


