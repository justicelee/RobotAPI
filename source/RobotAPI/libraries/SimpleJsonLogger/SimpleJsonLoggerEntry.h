/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/libraries/StructuralJson/JsonObject.h>
#include <Eigen/Dense>
#include <map>

namespace armarx
{
    class SimpleJsonLoggerEntry;
    using SimpleJsonLoggerEntryPtr = std::shared_ptr<SimpleJsonLoggerEntry>;

    class SimpleJsonLoggerEntry
    {
    public:
        SimpleJsonLoggerEntry();
        void AddAsArr(const std::string& key, const Eigen::Vector3f& vec);
        void AddAsArr(const std::string& key, const Eigen::VectorXf& vec);
        void AddAsObj(const std::string& key, const Eigen::Vector3f& vec);
        void AddAsArr(const std::string& key, const Eigen::Matrix4f& mat);
        void AddMatrix(const std::string& key, const Eigen::MatrixXf& mat);

        void Add(const std::string& key, const std::string& value);
        void Add(const std::string& key, float value);
        void Add(const std::string& key, const std::vector<float>& value);
        void Add(const std::string& key, const std::map<std::string, float>& value);

        static JsonArrayPtr ToArr(const Eigen::VectorXf& vec);
        static JsonArrayPtr ToArr(const std::vector<float>& vec);
        static JsonObjectPtr ToObj(Eigen::Vector3f vec);
        static JsonObjectPtr ToObj(const std::map<std::string, float>& value);
        static JsonArrayPtr ToArr(Eigen::Matrix4f mat);
        static JsonArrayPtr MatrixToArr(Eigen::MatrixXf mat);

        void AddTimestamp();

        JsonObjectPtr obj;
    };
}

