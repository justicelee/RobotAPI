armarx_component_set_name("SimpleJsonLogger")
armarx_set_target("Library: SimpleJsonLogger")

set(LIB_NAME       SimpleJsonLogger)

find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")

set(LIBS ArmarXCore StructuralJson VirtualRobot)
set(LIB_FILES   SimpleJsonLogger.cpp SimpleJsonLoggerEntry.cpp)
set(LIB_HEADERS SimpleJsonLogger.h   SimpleJsonLoggerEntry.h)
armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

# add unit tests
add_subdirectory(test)
