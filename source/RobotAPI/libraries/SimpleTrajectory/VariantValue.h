#pragma once

#include <boost/variant.hpp>

#include <Eigen/Eigen>


namespace armarx::trajectory
{

    /// Variant for trajectory values.
    using VariantValue = boost::variant<float, Eigen::MatrixXf, Eigen::Quaternionf>;

    /// ID of tracks.
    using TrackID = std::string;

}
