#include "Track.h"


namespace armarx::trajectory
{

    template <>
    void Track<VariantValue>::checkValueType(const VariantValue& value)
    {
        if (!empty() && value.type() != keyframes.front().value.type())
        {
            throw error::WrongValueTypeInKeyframe(id, value.type(), keyframes.front().value.type());
        }
    }

}
