#pragma once

#include "../exceptions.h"
#include "../VariantValue.h"


namespace armarx::trajectory::interpolate
{

    /**
     * @brief Linear interpolation visitor: Interpolates between the given values linearly.
     */
    class Linear : public boost::static_visitor<>
    {
    public:

        using result_type = VariantValue; ///< Exposed result type.

    public:

        /**
         * @brief Interpolator
         * @param t in [0, 1], where `t = 0` for `lhs` and `t = 1` for `rhs`.
         */
        Linear(float t) : t(t) {}

        template <typename U, typename V>
        float operator()(const U&, const V&) const
        {
            throw error::InterpolateDifferentTypesError();
        }

        float operator()(const float& lhs, const float& rhs) const
        {
            return (1 - t) * lhs + t * rhs;
        }

        Eigen::MatrixXf operator()(const Eigen::MatrixXf& lhs, const Eigen::MatrixXf& rhs) const
        {
            return (1 - t) * lhs + t * rhs;
        }

        Eigen::Quaternionf operator()(const Eigen::Quaternionf& lhs, const Eigen::Quaternionf& rhs) const
        {
            return lhs.slerp(t, rhs);
        }


    private:

        float t;

    };

    template <typename ReturnT>
    ReturnT linear(float t, const VariantValue& lhs, const VariantValue& rhs)
    {
        return boost::get<ReturnT>(boost::apply_visitor(Linear {t}, lhs, rhs));
    }

    template <>
    VariantValue linear<VariantValue>(float t, const VariantValue& lhs, const VariantValue& rhs);

}
