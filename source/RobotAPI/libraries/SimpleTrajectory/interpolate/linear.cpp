#include "linear.h"


namespace armarx::trajectory
{

    template <>
    VariantValue interpolate::linear<VariantValue>(float t, const VariantValue& lhs, const VariantValue& rhs)
    {
        // dont use boost::get
        return boost::apply_visitor(Linear {t}, lhs, rhs);
    }

}
