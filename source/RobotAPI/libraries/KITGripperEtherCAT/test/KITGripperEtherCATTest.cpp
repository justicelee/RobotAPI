/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ImagineRT::ArmarXObjects::ImagineEtherCAT
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::KITGripperEtherCAT

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/KITGripperEtherCAT/KITGripperBasisBoard/Misc/TorqueEstimation.h>

#include <iostream>

BOOST_AUTO_TEST_CASE(torqueEstimationPerformanceTest)
{
    TIMING_START(NN_Calc);
    int iterations = 1000;
    for (int i = 0; i < iterations; i++)
    {
        estimateTorque(rand() % 800, rand() % 3000);
    }
    TIMING_END(NN_Calc);
    BOOST_CHECK_EQUAL(true, true);
}
