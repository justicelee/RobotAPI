#pragma once
/* Includes ------------------------------------------------------------------*/

#include "TorqueEstimationWeights.h"

float imgIn[5];
float imgFcl1[32];
float imgFcl2[32];
float imgFcl3[32];
float imgFcl4[16];
float imgFcl5[8];
float imgFcl6[1];


uint8_t fcl1(void)
{
    uint8_t outFNum = sizeof imgFcl1 / sizeof(float);
    uint8_t inFNum = sizeof imgIn / sizeof(float);
    uint8_t outF = 0;
    uint8_t inF = 0;
    float buf = 0.0;
    memset(imgFcl1, 0.0, sizeof(imgFcl1));

    for (outF = 0; outF < outFNum; outF++)
    {
        for (inF = 0; inF < inFNum; inF++)
        {
            buf += imgIn[inF] * fc1Weights[inF][outF]; //OLDENTRIES-1-
        }

        buf += fc1Bias[outF];
        if (buf < 0)    //relu
        {
            buf = 0.0;
        }
        imgFcl1[outF] = buf;
        buf = 0.;
    }
    return 1;
}


uint8_t fcl2(void)
{
    uint8_t outFNum = sizeof imgFcl2 / sizeof(float);
    uint8_t inFNum = sizeof imgFcl1 / sizeof(float);
    uint8_t outF = 0;
    uint8_t inF = 0;
    float buf = 0.0;
    memset(imgFcl2, 0.0, sizeof(imgFcl2));

    for (outF = 0; outF < outFNum; outF++)
    {
        for (inF = 0; inF < inFNum; inF++)
        {
            buf += imgFcl1[inF] * fc2Weights[inF][outF];
        }

        buf += fc2Bias[outF];
        if (buf < 0)    //relu
        {
            buf = 0.0;
        }
        imgFcl2[outF] = buf;
        buf = 0.;
    }
    return 1;
}

uint8_t fcl3(void)
{
    uint8_t outFNum = sizeof imgFcl3 / sizeof(float);
    uint8_t inFNum = sizeof imgFcl2 / sizeof(float);
    uint8_t outF = 0;
    uint8_t inF = 0;
    float buf = 0.0;
    memset(imgFcl3, 0.0, sizeof(imgFcl3));

    for (outF = 0; outF < outFNum; outF++)
    {
        for (inF = 0; inF < inFNum; inF++)
        {
            buf += imgFcl2[inF] * fc3Weights[inF][outF];
        }

        buf += fc3Bias[outF];
        if (buf < 0)    //relu
        {
            buf = 0.0;
        }
        imgFcl3[outF] = buf;
        buf = 0.;
    }
    return 1;
}
uint8_t fcl4(void)
{
    uint8_t outFNum = sizeof imgFcl4 / sizeof(float);
    uint8_t inFNum = sizeof imgFcl3 / sizeof(float);
    uint8_t outF = 0;
    uint8_t inF = 0;
    float buf = 0.0;
    memset(imgFcl4, 0.0, sizeof(imgFcl4));

    for (outF = 0; outF < outFNum; outF++)
    {
        for (inF = 0; inF < inFNum; inF++)
        {
            buf += imgFcl3[inF] * fc4Weights[inF][outF];
        }

        buf += fc4Bias[outF];
        if (buf < 0)    //relu
        {
            buf = 0.0;
        }
        imgFcl4[outF] = buf;
        buf = 0.;
    }
    return 1;
}

uint8_t fcl5(void)
{
    uint8_t outFNum = sizeof imgFcl5 / sizeof(float);
    uint8_t inFNum = sizeof imgFcl4 / sizeof(float);
    uint8_t outF = 0;
    uint8_t inF = 0;
    float buf = 0.0;
    memset(imgFcl5, 0.0, sizeof(imgFcl5));

    for (outF = 0; outF < outFNum; outF++)
    {
        for (inF = 0; inF < inFNum; inF++)
        {
            buf += imgFcl4[inF] * fc5Weights[inF][outF];
        }

        buf += fc5Bias[outF];
        if (buf < 0)    //relu
        {
            buf = 0.0;
        }
        imgFcl5[outF] = buf;
        buf = 0.;
    }
    return 1;
}

uint8_t fcl6(void)
{
    uint8_t outFNum = sizeof imgFcl6 / sizeof(float);
    uint8_t inFNum = sizeof imgFcl5 / sizeof(float);
    uint8_t outF = 0;
    uint8_t inF = 0;
    float buf = 0.0;
    memset(imgFcl6, 0.0, sizeof(imgFcl6));

    for (outF = 0; outF < outFNum; outF++)
    {
        for (inF = 0; inF < inFNum; inF++)
        {
            buf += imgFcl5[inF] * fc6Weights[inF][outF];
        }

        buf += fc6Bias[outF];
        //if(buf < 0)   //relu
        //{
        //  buf = 0.0;
        //}
        imgFcl6[outF] = buf;
        buf = 0.;
    }
    return 1;
}


float linearModel_dcx22(int32_t nI, int32_t pwm)
{
    float n = (float) nI * 1000 * 60 / 4096; //ticks/ms -> U/min
    float T_f = 1000. / 231.; //übersetzung getriebe + Nm->m
    float motor_a = 226. + 30.; //Drehzahlkonstante   [min-1 V-1]
    float motor_b = 123.;   //Kennliniensteigung [min-1 mNm-1]
    float motor_eta = 0.4; //Wirkungsgrad Getriebe *Motor, max from datasheet:0.75*0.85
    float Umax = 48.; //Spannung bei pwm max
    float pwmmax = 3000.;
    float pwm_zero = 250.;
    float U;
    float T_motor;


    U = (float(fabs(pwm) - pwm_zero) / (pwmmax - pwm_zero))  *   Umax;
    if (U < 0)
    {
        U = 0;
    }
    if (pwm < 0)
    {
        U *= -1;
    }
    if (pwm == 0)
    {
        U = 0;
    }

    //U(M,n)=(n-b*M)/a
    T_motor = (U * motor_a - n) / -motor_b;
    auto T = T_motor * motor_eta / T_f;

    return T;
}


float estimateTorque(int32_t n, int32_t pwm)
{
    float n_input = (float)n / n_factor;
    float pwm_input = (float)pwm / pwm_factor;
    //    float inputData[6];
    static float pwmXn_old = 0;
    float pwmXn = n_input * pwm_input;
    float torque = 0.;

    if (pwmXn < 0)
    {
        pwmXn = -1;
        pwmXn_old = pwmXn;
    }
    else if (pwmXn > 0)
    {
        pwmXn = 1;
        pwmXn_old = pwmXn;
    }
    else
    {
        pwmXn = pwmXn_old;
    }

    //    powerDir = linearModel_dcx22(n, pwm);
    //    if(powerDir < 0)
    //    {
    //            powerDir = -1;
    //    }
    //    if(powerDir > 0 )
    //    {
    //            powerDir = 1;
    //    }

    imgIn[0] = n_input;
    imgIn[1] = pwm_input;
    imgIn[2] = n_input;
    imgIn[3] = pwm_input;
    imgIn[4] = pwmXn;
    fcl1();
    fcl2();
    fcl3();
    fcl4();
    fcl5();
    fcl6();
    torque = imgFcl6[0];
    return torque + linearModel_dcx22(n, pwm);
}
