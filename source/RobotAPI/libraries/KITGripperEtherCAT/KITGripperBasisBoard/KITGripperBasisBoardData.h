/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/libraries/ArmarXEtherCAT/AbstractData.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueIMU.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <ArmarXCore/observers/filters/rtfilters/AverageFilter.h>
#include "KITGripperBasisBoardSlave.h"

namespace armarx
{
    class   SensorValueKITGripperBasisBoard :
    //            virtual public SensorValue1DoFActuatorMotorTemperature,
        virtual public SensorValueIMU
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        int IMUTemperature;


        static SensorValueInfo<SensorValueKITGripperBasisBoard> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValueKITGripperBasisBoard> svi;
            //            svi.addBaseClass<SensorValue1DoFActuatorMotorTemperature>();
            svi.addMemberVariable(&SensorValueKITGripperBasisBoard::IMUTemperature, "IMUTemperature");
            svi.addBaseClass<SensorValueIMU>();
            return svi;
        }
        SensorValueKITGripperBasisBoard() = default;
        SensorValueKITGripperBasisBoard(SensorValueKITGripperBasisBoard&&) = default;
        SensorValueKITGripperBasisBoard(const SensorValueKITGripperBasisBoard&) = default;
        SensorValueKITGripperBasisBoard& operator=(SensorValueKITGripperBasisBoard&& other)
        {
            *this = other;
            return *this;
        }
        SensorValueKITGripperBasisBoard& operator=(const SensorValueKITGripperBasisBoard&) = default;

    };

    class KITGripperActorSensorData : virtual public SensorValue1DoFRealActuator
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        int32_t targetPWM;
        float relativePosition;
        float velocityTicksPerMs;
        float absoluteEncoderVelocity;
        int32_t maxPWM;
        int32_t minPWM;

        static SensorValueInfo<KITGripperActorSensorData> GetClassMemberInfo()
        {
            SensorValueInfo<KITGripperActorSensorData> svi;
            //            svi.addBaseClass<SensorValue1DoFActuatorMotorTemperature>();
            svi.addBaseClass<SensorValue1DoFRealActuator>();
            svi.addMemberVariable(&KITGripperActorSensorData::targetPWM, "targetPWM");
            svi.addMemberVariable(&KITGripperActorSensorData::relativePosition, "relativePosition");
            svi.addMemberVariable(&KITGripperActorSensorData::maxPWM, "maxPWM");
            svi.addMemberVariable(&KITGripperActorSensorData::minPWM, "minPWM");
            svi.addMemberVariable(&KITGripperActorSensorData::velocityTicksPerMs, "velocityTicksPerMs");
            svi.addMemberVariable(&KITGripperActorSensorData::absoluteEncoderVelocity, "absoluteEncoderVelocity");

            return svi;
        }

    };


    class ActorData
    {
    public:
        ActorData();
        void setTargetPWM(int32_t targetPWM);
        float getPosition() const;
        float getRelativePosition() const;
        float getCompensatedRelativePosition() const;
        float getVelocity() const;
        float getAcceleration() const;
        float getTorque() const;
        float getSoftLimitHi() const;
        float getSoftLimitLo() const;
        bool isLimitless() const;
        int32_t getTargetPWM() const;
        int32_t getVelocityTicks() const;

        int32_t getCurrentMinPWM() const;

        int32_t getCurrentMaxPWM() const;

        int32_t getSiblingControlActorIndex() const;

        bool getPositionControlEnabled() const;

        bool getVelocityControlEnabled() const;

        float getCurrentPWMBoundGradient() const;

        int32_t getCurrentPWMBoundOffset() const;

        float getParallelGripperDecouplingFactor() const;

        float getAbsoluteEncoderVelocity() const;

    private:
        u_int32_t rawABSEncoderTicks;
        LinearConvertedValue<int32_t> relativePosition;
        float adjustedRelativePosition;
        float relativePositionOffset = std::nan("");
        float parallelGripperDecouplingFactor = std::nanf("");
        LinearConvertedValue<u_int32_t> position;
        float sanitizedAbsolutePosition;
        LinearConvertedValue<int32_t> velocity;
        rtfilters::AverageFilter velocityFilter;
        float absoluteEncoderVelocity = 0.0f;
        float acceleration;
        float lastAbsolutePosition = std::nanf("");
        LinearConvertedValue<int32_t> torque;
        LinearConvertedValue<int32_t> targetPWM;
        int32_t* velocityTicks;
        int32_t currentMaxPWM = 0;
        int32_t currentMinPWM = 0;
        size_t maxPWM;
        int32_t parallelControlEnabled = -1;
        VirtualRobot::RobotNodePtr robotNode;
        LinearConvertedValue<int32_t> targetPWMPtr;
        bool positionControlEnabled = true;
        bool velocityControlEnabled = true;
        float currentPWMBoundGradient = 3.75;
        int32_t currentPWMBoundOffset = 1500;
        friend class KITGripperBasisBoardData;
    };
    using ActorDataPtr = std::shared_ptr<ActorData>;

    class KITGripperBasisBoardData : public AbstractData
    {
    public:
        KITGripperBasisBoardData(const RapidXmlReaderNode& node, DefaultRapidXmlReaderNode defaultConfigurationNode,
                                 KITGripperBasisBoardOUT_t* sensorOUT, KITGripperBasisBoardIN_t* sensorIN, VirtualRobot::RobotPtr robot);

        // AbstractData interface
    public:
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        ActorDataPtr& getActorData(size_t actorIndex);

        int8_t getIMUTemperature() const;

    private:
        KITGripperBasisBoardOUT_t* sensorOUT;
        KITGripperBasisBoardIN_t* sensorIN;

        std::vector<ActorDataPtr> actorData;

    };
    using KITGripperBasisBoardDataPtr = std::shared_ptr<KITGripperBasisBoardData>;

} // namespace armarx

