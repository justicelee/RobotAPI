/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "KITGripperBasisBoardData.h"
#include "KITGripperBasisBoardSlave.h"

#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>

#include <RobotAPI/libraries/ArmarXEtherCAT/AbstractFunctionalDevice.h>
#include <RobotAPI/libraries/ArmarXEtherCAT/SlaveIdentifier.h>

namespace armarx
{
    using KITGripperBasisBoardPtr = std::shared_ptr<class KITGripperBasisBoard>;
    using JointKITGripperStopMovementControllerPtr = std::shared_ptr<class JointKITGripperStopMovementController>;
    using JointKITGripperEmergencyStopControllerPtr = std::shared_ptr<class JointKITGripperEmergencyStopController>;
    using JointPWMVelocityControllerPtr = std::shared_ptr<class JointPWMVelocityController>;
    using JointPWMPositionControllerPtr = std::shared_ptr<class JointPWMPositionController>;
    using JointPWMZeroTorqueControllerPtr = std::shared_ptr<class JointPWMZeroTorqueController>;
    using JointKITGripperPWMPassThroughControllerPtr = std::shared_ptr<class JointKITGripperPWMPassThroughController>;
    using ParallelGripperPositionControllerPtr = std::shared_ptr<class ParallelGripperPositionController>;
    using ParallelGripperVelocityControllerPtr = std::shared_ptr<class ParallelGripperVelocityController>;
    using PWMVelocityControllerConfigurationPtr = std::shared_ptr<class PWMVelocityControllerConfiguration>;


    class KITGripperBasisBoard :
        public SensorDevice,
    //        public ControlDevice,
        public AbstractFunctionalDevice
    {
        static VirtualDeviceFactory::SubClassRegistry registry;
    public:

        class ActorRobotUnitDevice :
            public ControlDevice,
            public SensorDevice
        {
            friend class KITGripperBasisBoard;

            // SensorDevice interface
        public:
            ActorRobotUnitDevice(size_t connectorIndex, const std::string& deviceName, VirtualRobot::RobotNodePtr robotNode);
            const SensorValueBase* getSensorValue() const override
            {
                return &sensorValue;
            }

            void init(KITGripperBasisBoardPtr dev, KITGripperBasisBoardDataPtr actorDataPtr, RapidXmlReaderNode configNode, DefaultRapidXmlReaderNode defaultConfigurationNode);
        protected:
            KITGripperBasisBoardPtr board;
            //            KITGripperBasisBoardDataPtr dataPtr;
            VirtualRobot::RobotNodePtr robotNode;
            size_t actorIndex;
            ActorDataPtr actorDataPtr;

            JointKITGripperEmergencyStopControllerPtr emergencyController;
            JointKITGripperStopMovementControllerPtr stopMovementController;
            JointPWMVelocityControllerPtr velocityController;
            JointPWMPositionControllerPtr positionController;
            JointPWMZeroTorqueControllerPtr zeroTorqueController;
            JointKITGripperPWMPassThroughControllerPtr pwmController;

            /// The data object for copying to non-rt part
            KITGripperActorSensorData sensorValue;


            // SensorDevice interface
        public:
            void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

            // ControlDevice interface
        public:
            void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
            const VirtualRobot::RobotNodePtr& getRobotNode() const;
            ActorDataPtr getActorDataPtr() const;
        };
        using ActorRobotUnitDevicePtr = std::shared_ptr<ActorRobotUnitDevice>;

        KITGripperBasisBoard(RapidXmlReaderNode node, DefaultRapidXmlReaderNode defaultConfigurationNode, VirtualRobot::RobotPtr const& robot);
        void init(KITGripperBasisBoardSlavePtr slave);

        // AbstractFunctionalDevice interface
    public:
        void initData() override;

        // SensorDevice interface
    public:
        const SensorValueBase* getSensorValue() const override;
        const SlaveIdentifier& getSlaveIdentifier() const;
        const std::vector<ActorRobotUnitDevicePtr >& getDevices() const;
        size_t getActorIndex(const std::string& actorName);
    protected:
        // SensorDevice interface
    public:
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        //        // ControlDevice interface
        //    public:
        //        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

    private:
        RapidXmlReaderNode configNode;
        DefaultRapidXmlReaderNode defaultConfigurationNode;
        VirtualRobot::RobotPtr robot;
        KITGripperBasisBoardDataPtr dataPtr;
        KITGripperBasisBoardSlavePtr slave;
        SensorValueKITGripperBasisBoard sensorValue;
        SlaveIdentifier slaveIdentifier;
        std::vector<ActorRobotUnitDevicePtr > devices;
        PWMVelocityControllerConfigurationPtr velocityControllerConfigDataPtr;

    };


} // namespace armarx

