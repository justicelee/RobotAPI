/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/libraries/ArmarXEtherCAT/AbstractSlave.h>
#include <RobotAPI/libraries/ArmarXEtherCAT/EtherCATDeviceFactory.h>
#include <memory>

namespace armarx
{

    /**
     * @brief PDO mapping sensorB->master
     */
    struct KITGripperBasisBoardOUT_t
    {
        u_int8_t RawABSEncoderValueBytes[4];
        u_int8_t RawABSEncoderValueCRC;
        int16_t pad1;
        int8_t pad2;

        u_int8_t RawABSEncoder2ValueBytes[4];
        u_int8_t RawABSEncoder2ValueCRC;
        int16_t pad3;
        int8_t pad4;

        int16_t IMUVector1[3];
        int16_t IMUVector2[3];
        int16_t IMUQuaternionW;
        int16_t IMUQuaternionX;
        int16_t IMUQuaternionY;
        int16_t IMUQuaternionZ;
        int8_t IMUTemperature;

        int16_t pad5;
        int8_t pad6;


        int32_t motor1_current_pos;
        int32_t motor1_current_speed; // ticks pro milliseconds
        int32_t motor1_current_torque;
        int32_t motor2_current_pos;
        int32_t motor2_current_speed;
        int32_t motor2_current_torque;
        int32_t motor3_current_pos;
        int32_t motor3_current_speed;
        int32_t motor3_current_torque;

    } __attribute__((__packed__));

    /**
     * @brief PDO mapping master->sensorB
     */
    struct KITGripperBasisBoardIN_t
    {
        uint16_t LED_PG15;
        uint16_t LED_2;
        uint16_t LED_3;
        uint16_t LED_4;

        int32_t motor1_target_pwm;
        int32_t motor1_target_speed;
        int32_t motor1_target_torque;
        int32_t motor2_target_pwm;
        int32_t motor2_target_speed;
        int32_t motor2_target_torque;
        int32_t motor3_target_pwm;
        int32_t motor3_target_speed;
        int32_t motor3_target_torque;
        int32_t motor4_target_pwm;
    } __attribute__((__packed__));

    class KITGripperBasisBoardSlave;
    typedef std::shared_ptr<KITGripperBasisBoardSlave> KITGripperBasisBoardSlavePtr;


    class KITGripperBasisBoardSlave :
        public AbstractSlaveWithInputOutput<KITGripperBasisBoardIN_t, KITGripperBasisBoardOUT_t>
    {
    public:
        KITGripperBasisBoardSlave(const armarx::SlaveIdentifier slaveIdentifier, uint16_t slaveNumber);

        // AbstractSlave interface
    public:
        void doMappings() override;
        bool prepare() override;
        void execute() override;
        bool shutdown() override;
        void prepareForOp() override;
        bool hasError() override;
    };

    class KITGripperBasisBoardFactory : public EtherCATDeviceFactory
    {
        KITGripperBasisBoardFactory() {}
        // AbstractFactoryMethod
    public:
        static std::string getName()
        {
            return "KITGripperBasisBoardFactory";
        }
    private:
        static SubClassRegistry registry;
        static SharedPointerType createInstance(EtherCATFactoryArgs args);
    };
    using KITGripperBasisBoardFactoryPtr = std::shared_ptr<KITGripperBasisBoardFactory>;

} // namespace armarx

