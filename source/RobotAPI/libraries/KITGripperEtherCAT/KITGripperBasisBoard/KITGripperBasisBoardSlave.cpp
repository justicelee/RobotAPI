/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "KITGripperBasisBoard.h"
#include "KITGripperBasisBoardSlave.h"

#include <ethercat.h>
#include <RobotAPI/libraries/ArmarXEtherCAT/EtherCAT.h>
#include <RobotAPI/libraries/ArmarXEtherCAT/DeviceContainer.h>

namespace armarx
{



    KITGripperBasisBoardSlave::KITGripperBasisBoardSlave(const SlaveIdentifier slaveIdentifier, uint16_t slaveNumber) :
        AbstractSlaveWithInputOutput(slaveIdentifier, slaveNumber)
    {
        setTag("KITGripperBasisBoardSlave_" + slaveIdentifier.humanName);
    }

    void KITGripperBasisBoardSlave::doMappings()
    {
    }

    bool KITGripperBasisBoardSlave::prepare()
    {
        return true;
    }

    void KITGripperBasisBoardSlave::execute()
    {
        /*if (::armarx::ControlThreadOutputBuffer::GetRtLoggingInstance())
        {
            //ARMARX_RT_LOGF_INFO("relative position 1: %d, current speed 1: %d", (int)outputs->motor1_current_pos, (int)outputs->motor1_current_speed).deactivateSpam(0.5);
            //ARMARX_RT_LOGF_INFO("relative position 2: %d, current speed 2: %d", (int)outputs->motor2_current_pos, (int)outputs->motor2_current_speed).deactivateSpam(0.5);
            //ARMARX_RT_LOGF_INFO("relative position 3: %d, current speed 3: %d", (int)outputs->motor3_current_pos, (int)outputs->motor3_current_speed).deactivateSpam(0.5);
        }*/
    }

    bool KITGripperBasisBoardSlave::shutdown()
    {
        return true;
    }

    void KITGripperBasisBoardSlave::prepareForOp()
    {
    }

    bool KITGripperBasisBoardSlave::hasError()
    {
        return false;
    }

    /**
     * register this class in the super class factory
     */
    KITGripperBasisBoardFactory::SubClassRegistry KITGripperBasisBoardFactory::registry(KITGripperBasisBoardFactory::getName(), &KITGripperBasisBoardFactory::createInstance);

    KITGripperBasisBoardFactory::SharedPointerType KITGripperBasisBoardFactory::createInstance(EtherCATFactoryArgs args)
    {
        EtherCAT* etherCAT = std::get<0>(args);
        ARMARX_CHECK_EXPRESSION(etherCAT);
        auto slaveIndex = std::get<1>(args);
        auto deviceContainer = std::get<2>(args);

        auto devs = deviceContainer->getDevicesOfType<KITGripperBasisBoard>();
        //        if ((ec_slave[slaveIndex].mbx_proto & ECT_MBXPROT_COE) == 0) // TODO: valid for this slave?
        {
            for (auto& dev : devs)
            {
                if (ec_slave[slaveIndex].eep_man == H2T_VENDOR_ID &&
                    ec_slave[slaveIndex].eep_id == dev->getSlaveIdentifier().ProductID)
                {
                    ARMARX_INFO << "KITGripperBasisBoard '" << dev->getSlaveIdentifier().humanName << "' found";
                    auto slave = std::make_shared<KITGripperBasisBoardSlave>(dev->getSlaveIdentifier(), slaveIndex);
                    dev->init(slave);
                    KITGripperBasisBoardFactoryPtr objFac(new KITGripperBasisBoardFactory);
                    objFac->addSlave(slave);
                    objFac->addSensorDevice(dev);
                    for (auto& virtualDev : dev->getDevices())
                    {
                        objFac->addControlDevice(virtualDev);
                        objFac->addSensorDevice(virtualDev);
                    }
                    return objFac;
                }

            }
        }
        return SharedPointerType();
    }

}
