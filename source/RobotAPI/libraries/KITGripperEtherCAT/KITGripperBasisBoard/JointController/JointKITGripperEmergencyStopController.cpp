#include "JointKITGripperEmergencyStopController.h"

namespace armarx
{

    JointKITGripperEmergencyStopController::JointKITGripperEmergencyStopController(ActorDataPtr dataPtr) :
        dataPtr(dataPtr)
    {
        pid.reset(new PIDController(0, 5000, 0));
        pid->maxIntegral = 0.1;
    }
    void JointKITGripperEmergencyStopController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        pid->update(timeSinceLastIteration.toSecondsDouble(), dataPtr->getVelocity(), 0.0);
        float targetPwm = pid->getControlValue();
        if (std::isnan(targetPwm))
        {
            targetPwm = 0.0f;
        }
        //        dataPtr->setTargetPWM(targetPwm);
        lastPWM *= 0.9997;
        //        ARMARX_RT_LOGF_INFO("old pwm %d, new pwm: %.2f", dataPtr->getTargetPWM(), lastPWM);
        dataPtr->setTargetPWM(lastPWM);

    }

    ControlTargetBase* JointKITGripperEmergencyStopController::getControlTarget()
    {
        return &target;
    }

    void JointKITGripperEmergencyStopController::rtPreActivateController()
    {
        //        ARMARX_INFO << "Stopping gripper!";
        //        dataPtr->setTargetPWM(0);
        lastPWM = math::MathUtils::LimitTo(dataPtr->getTargetPWM(), 500);
    }


} // namespace armarx
