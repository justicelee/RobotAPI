#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include "../KITGripperBasisBoardData.h"


namespace armarx
{

    class JointKITGripperStopMovementController;
    typedef std::shared_ptr<JointKITGripperStopMovementController> JointKITGripperStopMovementControllerPtr;


    class JointKITGripperStopMovementController : public JointController
    {
    public:
        JointKITGripperStopMovementController(ActorDataPtr dataPtr);
    private:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        /**
         * Returns the Target for this controller, but as this is the Emergency controller it will ignored.
         * As this controller will just break
         * @return is type VelocityTarget but it will return a nullptr, because it won't be possible to set a target
         */
        ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
    private:
        DummyControlTargetStopMovement target;
        ActorDataPtr dataPtr;
    };

} // namespace armarx

