#include "JointKITGripperStopMovementController.h"

namespace armarx
{
    JointKITGripperStopMovementController::JointKITGripperStopMovementController(ActorDataPtr dataPtr) :
        dataPtr(dataPtr)
    {

    }

    void JointKITGripperStopMovementController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
    }

    ControlTargetBase* JointKITGripperStopMovementController::getControlTarget()
    {
        return &target;
    }

    void JointKITGripperStopMovementController::rtPreActivateController()
    {
        //        ARMARX_INFO << "Stopping gripper!";
        dataPtr->setTargetPWM(0);
    }
} // namespace armarx
