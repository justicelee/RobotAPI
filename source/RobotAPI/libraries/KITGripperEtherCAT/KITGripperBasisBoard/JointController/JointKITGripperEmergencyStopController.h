#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/libraries/core/PIDController.h>
#include "../KITGripperBasisBoardData.h"


namespace armarx
{

    class JointKITGripperEmergencyStopController;
    typedef std::shared_ptr<JointKITGripperEmergencyStopController> JointKITGripperEmergencyStopControllerPtr;


    class JointKITGripperEmergencyStopController : public JointController
    {
    public:
        JointKITGripperEmergencyStopController(ActorDataPtr dataPtr);
    private:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        /**
         * Returns the Target for this controller, but as this is the Emergency controller it will ignored.
         * As this controller will just break
         * @return is type VelocityTarget but it will return a nullptr, because it won't be possible to set a target
         */
        ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
    private:
        DummyControlTargetEmergencyStop target;
        ActorDataPtr dataPtr;
        PIDControllerPtr pid;
        float lastPWM = 0.0f;
    };



} // namespace

