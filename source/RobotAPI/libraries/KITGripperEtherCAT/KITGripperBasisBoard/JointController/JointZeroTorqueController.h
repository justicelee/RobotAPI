#pragma once

#include <memory>
#include <chrono>

#include <RobotAPI/libraries/core/PIDController.h>

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include "../KITGripperBasisBoardData.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXCore/core/services/tasks/ThreadPool.h>

#include <ArmarXCore/observers/filters/AverageFilter.h>
#include "PWMVelocityController.h"


namespace armarx
{
    using KITGripperBasisBoardPtr = std::shared_ptr<class KITGripperBasisBoard>;


    typedef std::shared_ptr<class PWMZeroTorqueControllerConfiguration> PWMZeroTorqueControllerConfigurationPtr;
    typedef std::shared_ptr<const PWMZeroTorqueControllerConfiguration> PWMZeroTorqueControllerConfigurationCPtr;

    class PWMZeroTorqueControllerConfiguration
    {
    public:
        PWMZeroTorqueControllerConfiguration() {}
        static PWMZeroTorqueControllerConfigurationCPtr CreateConfigDataFromXml(DefaultRapidXmlReaderNode node);
        float feedforwardVelocityToPWMFactor;
        float PWMDeadzone;
    };



    class JointPWMZeroTorqueController;
    typedef std::shared_ptr<JointPWMZeroTorqueController> JointPWMZeroTorqueControllerPtr;

    class JointPWMZeroTorqueController : public JointController
    {
    public:
        JointPWMZeroTorqueController(const std::string deviceName, KITGripperBasisBoardPtr board, ActorDataPtr jointData, PWMZeroTorqueControllerConfigurationCPtr config);
        ~JointPWMZeroTorqueController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;
        ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
    private:
        PWMZeroTorqueControllerConfigurationCPtr config;
        ControlTarget1DoFActuatorZeroTorque target;

        std::atomic<double> lastTargetVelocity;
        bool isLimitless;

        ActorDataPtr dataPtr;
        KITGripperBasisBoardPtr board;
        const std::string deviceName;
        size_t actorIndex = 0;
        mutable RemoteGuiInterfacePrx remoteGui;
        bool stopRequested = false;
        mutable ThreadPool::Handle threadHandle;
        // JointController interface
    protected:
        void rtPostDeactivateController() override;

        // JointController interface
    public:
        StringVariantBaseMap publish(const DebugDrawerInterfacePrx& draw, const DebugObserverInterfacePrx& observer) const override;
    };
}
