#pragma once

#include <memory>
#include <chrono>

#include <RobotAPI/libraries/core/PIDController.h>

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include "../KITGripperBasisBoardData.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXCore/core/services/tasks/ThreadPool.h>

#include <ArmarXCore/observers/filters/AverageFilter.h>
#include "JointPWMPositionController.h"
#include "PWMVelocityController.h"


namespace armarx
{
    using KITGripperBasisBoardPtr = std::shared_ptr<class KITGripperBasisBoard>;


    class ParallelGripperPositionController;
    typedef std::shared_ptr<ParallelGripperPositionController> ParallelGripperPositionControllerPtr;

    class ParallelGripperPositionController : public JointPWMPositionController
    {
    public:
        ParallelGripperPositionController(const std::string deviceName, KITGripperBasisBoardPtr board, ActorDataPtr jointData, PWMPositionControllerConfigurationCPtr positionControllerConfigDataPtr);
        ~ParallelGripperPositionController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;
    private:
        uint32_t linkedJointConnectorIndex = -1;
        ActorDataPtr linkedDataPtr;



        // JointController interface
    protected:
        void rtPreActivateController() override;
    };
}
