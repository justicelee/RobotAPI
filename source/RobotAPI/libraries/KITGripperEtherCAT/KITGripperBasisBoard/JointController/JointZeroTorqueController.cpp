#include <chrono>

#include <ArmarXCore/core/logging/Logging.h>
#include "JointZeroTorqueController.h"
#include "../KITGripperBasisBoard.h"
#include <RobotAPI/components/units/RobotUnit/util/ControlThreadOutputBuffer.h>
#include <ArmarXCore/core/ManagedIceObject.h>


#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>

#include <ArmarXCore/core/application/Application.h>

#include <RobotAPI/components/units/RobotUnit/RobotUnitModules/RobotUnitModuleDevices.h>

namespace armarx
{
    PWMZeroTorqueControllerConfigurationCPtr PWMZeroTorqueControllerConfiguration::CreateConfigDataFromXml(DefaultRapidXmlReaderNode node)
    {
        PWMZeroTorqueControllerConfiguration configData;

        configData.feedforwardVelocityToPWMFactor = node.first_node("feedforwardVelocityToPWMFactor").value_as_float();
        configData.PWMDeadzone = node.first_node("PWMDeadzone").value_as_float();


        return std::make_shared<PWMZeroTorqueControllerConfiguration>(configData);

    }

    JointPWMZeroTorqueController::JointPWMZeroTorqueController(const std::string deviceName, KITGripperBasisBoardPtr board, ActorDataPtr jointData,
            PWMZeroTorqueControllerConfigurationCPtr config) : JointController(),
        config(config), target(), board(board), deviceName(deviceName)
    {
        actorIndex = board->getActorIndex(deviceName);
        dataPtr = jointData;


        this->isLimitless = jointData->isLimitless();

    }

    JointPWMZeroTorqueController::~JointPWMZeroTorqueController() noexcept(true)
    {
        stopRequested = true;
        try
        {
            threadHandle.join();
        }
        catch (...)
        {

        }
    }

    void JointPWMZeroTorqueController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            float targetPWM = dataPtr->getVelocity() * config->feedforwardVelocityToPWMFactor;
            targetPWM += math::MathUtils::Sign(dataPtr->getVelocity()) * config->PWMDeadzone;
            //        targetPWM = math::MathUtils::LimitTo(targetPWM, 1500);
            dataPtr->setTargetPWM(targetPWM);

            //            ARMARX_RT_LOGF_INFO("target velocity: %.3f, current velocity: %.3f, target pwm: %d, kp: %.3f ki: %f, kd: %f, max acc: %.3f",
            //                                target.velocity, dataPtr->getVelocity(), targetPWM, pid->Kp, pid->Ki, pid->Kd, controller.acceleration).deactivateSpam(1);


        }
        else
        {
            ARMARX_ERROR << "invalid target set for actor";
        }
    }

    ControlTargetBase* JointPWMZeroTorqueController::getControlTarget()
    {
        return &target;
    }

    void JointPWMZeroTorqueController::rtPreActivateController()
    {
        lastTargetVelocity = dataPtr->getVelocity();
        //    controller.reset(dataPtr->getVelocity());
    }

    void JointPWMZeroTorqueController::rtPostDeactivateController()
    {
        ARMARX_RT_LOGF_INFO("Setting PWM to 0");
        dataPtr->setTargetPWM(0);
    }

    StringVariantBaseMap JointPWMZeroTorqueController::publish(const DebugDrawerInterfacePrx& draw, const DebugObserverInterfacePrx& observer) const
    {

        if (!remoteGui && !threadHandle.isValid())
        {
            threadHandle = Application::getInstance()->getThreadPool()->runTask([this]
            {
                return;
                //            std::string guiTabName;
                //            while (!stopRequested)
                //            {
                //                ManagedIceObjectPtr object;
                //                ARMARX_IMPORTANT << deactivateSpam(1) << "Trying to get parent";
                //                try
                //                {
                //                    object = ManagedIceObjectPtr::dynamicCast(getParent().getOwner());
                //                    ARMARX_CHECK_EXPRESSION(object);
                //                    remoteGui = object->getProxy<RemoteGuiInterfacePrx>("RemoteGuiProvider", false, "", false);
                //                    if (!remoteGui)
                //                    {
                //                        return;
                //                    }
                //                    ARMARX_IMPORTANT << deactivateSpam(1) << "Got Proxy";
                //                    guiTabName = getParent().getDeviceName() + getControlMode();
                //                    break;
                //                }
                //                catch (...)
                //                {
                //                    handleExceptions();
                //                    sleep(1);
                //                }

                //            }
                //            if (remoteGui)
                //            {
                //                ARMARX_IMPORTANT << "Creating GUI " << guiTabName;
                //                using namespace RemoteGui;



                //                //                auto vLayout = makeVBoxLayout();

                //                //                {
                //                //                    WidgetPtr KpLabel = makeTextLabel("Kp: ");

                //                //                    WidgetPtr KiSlider = makeFloatSlider("KpSlider")
                //                //                                         .min(0.0f).max(5000.0f)
                //                //                                         .value(config->p);
                //                //                    WidgetPtr line = makeHBoxLayout()
                //                //                                     .children({KpLabel, KiSlider});

                //                //                    vLayout.addChild(line);

                //                //                }


                //                //                {
                //                //                    WidgetPtr KiLabel = makeTextLabel("Ki: ");
                //                //                    WidgetPtr KiSlider = makeFloatSlider("KiSlider")
                //                //                                         .min(0.0f).max(50000.0f)
                //                //                                         .value(config->i);

                //                //                    WidgetPtr line = makeHBoxLayout()
                //                //                                     .children({KiLabel, KiSlider});

                //                //                    vLayout.addChild(line);

                //                //                }

                //                //                {
                //                //                    WidgetPtr KdLabel = makeTextLabel("Kd: ");
                //                //                    WidgetPtr KdSlider = makeFloatSlider("KdSlider")
                //                //                                         .min(0.0f).max(50.0f)
                //                //                                         .steps(100)
                //                //                                         .value(config->d);

                //                //                    WidgetPtr line = makeHBoxLayout()
                //                //                                     .children({KdLabel, KdSlider});

                //                //                    vLayout.addChild(line);
                //                //                    vLayout.addChild(new VSpacer);
                //                //                }

                //                //        WidgetPtr spin = makeFloatSpinBox("KpSpin")
                //                //                         .min(0.0f).max(2.0f)
                //                //                         .steps(20).decimals(2)
                //                //                         .value(0.4f);




                //                WidgetPtr groupBox = makeGroupBox("GroupBox")
                //                                     .label("Group")
                //                                     .child(vLayout);

                //                remoteGui->createTab(guiTabName, groupBox);

                //                while (!stopRequested)
                //                {
                //                    RemoteGui::TabProxy tab(remoteGui, guiTabName);
                //                    tab.receiveUpdates();
                //                    this->controller.pid->Kp = tab.getValue<float>("KpSlider").get();
                //                    this->controller.pid->Ki = tab.getValue<float>("KiSlider").get();
                //                    this->controller.pid->Kd = tab.getValue<float>("KdSlider").get();
                //                    usleep(100000);
                //                }
                //            }

            });
        }
        return {};
        //    return {{"lastTargetVelocity", new Variant(lastTargetVelocity.load())},
        //        {"filteredVelocity", new Variant(controller.lastActualVelocity.load())},
        //        {"pidIntegralCV", new Variant(controller.pid->integral * controller.pid->Ki)},
        //        {"pidPropCV", new Variant(controller.pid->previousError * controller.pid->Kp)},
        //        {"pidDiffCV", new Variant(controller.pid->derivative * controller.pid->Kd)}
    }
}

