#pragma once

#include <memory>
#include <chrono>

#include <RobotAPI/libraries/core/PIDController.h>

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include "../KITGripperBasisBoardData.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXCore/core/services/tasks/ThreadPool.h>

#include <ArmarXCore/observers/filters/AverageFilter.h>
#include "PWMVelocityController.h"


namespace armarx
{
    using KITGripperBasisBoardPtr = std::shared_ptr<class KITGripperBasisBoard>;
    typedef std::shared_ptr<class PWMPositionControllerConfiguration> PWMPositionControllerConfigurationPtr;
    typedef std::shared_ptr<const PWMPositionControllerConfiguration> PWMPositionControllerConfigurationCPtr;


    class PWMPositionControllerConfiguration
    {
    public:
        PWMPositionControllerConfiguration() {}
        static PWMPositionControllerConfigurationCPtr CreatePWMPositionControllerConfigDataFromXml(DefaultRapidXmlReaderNode node);
        float maxVelocityRad;
        float maxAccelerationRad;
        float maxDecelerationRad;
        float maxDt;
        float p;
        float i;
        float d;
        float maxIntegral;
        float feedforwardVelocityToPWMFactor;
        float feedforwardTorqueToPWMFactor;
        float PWMDeadzone;
        float velocityUpdatePercent;
        float conditionalIntegralErrorTreshold;
        bool feedForwardMode;
    };


    class JointPWMPositionController;
    typedef std::shared_ptr<JointPWMPositionController> JointPWMPositionControllerPtr;

    class JointPWMPositionController : public JointController
    {
    public:
        JointPWMPositionController(const std::string deviceName, KITGripperBasisBoardPtr board, ActorDataPtr jointData, PWMPositionControllerConfigurationCPtr positionControllerConfigDataPtr);
        ~JointPWMPositionController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;
        ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
    protected:
        PWMPositionControllerConfigurationCPtr config;
        //        PWMVelocityController controller;
        //        PositionThroughVelocityControllerWithAccelerationAndPositionBounds posController;
        MinJerkPositionController posController;
        PIDControllerPtr pidPosController;
        ControlTarget1DoFActuatorPosition target;

        std::atomic<double> lastTargetVelocity, targetPWM;
        bool isLimitless;

        ActorDataPtr dataPtr;
        KITGripperBasisBoardPtr board;
        const std::string deviceName;
        size_t actorIndex = 0;
        mutable RemoteGuiInterfacePrx remoteGui;
        bool stopRequested = false;
        mutable ThreadPool::Handle threadHandle;
        const SensorValue1DoFActuator* sensorValue;
        // JointController interface
    protected:
        void rtPostDeactivateController() override;

        // JointController interface
    public:
        StringVariantBaseMap publish(const DebugDrawerInterfacePrx& draw, const DebugObserverInterfacePrx& observer) const override;
    };
}
