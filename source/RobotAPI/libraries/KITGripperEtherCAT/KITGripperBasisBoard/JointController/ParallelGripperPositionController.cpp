#include <chrono>

#include <ArmarXCore/core/logging/Logging.h>
#include "ParallelGripperPositionController.h"
#include "../KITGripperBasisBoard.h"
#include <RobotAPI/components/units/RobotUnit/util/ControlThreadOutputBuffer.h>
#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>
#include <ArmarXCore/core/ManagedIceObject.h>


#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>

#include <ArmarXCore/core/application/Application.h>

#include <RobotAPI/components/units/RobotUnit/RobotUnitModules/RobotUnitModuleDevices.h>

namespace armarx
{
    ParallelGripperPositionController::ParallelGripperPositionController(const std::string deviceName, KITGripperBasisBoardPtr board,
            ActorDataPtr jointData,
            PWMPositionControllerConfigurationCPtr positionControllerConfigDataPtr) :
        JointPWMPositionController(deviceName, board, jointData, positionControllerConfigDataPtr)
    {
        linkedJointConnectorIndex = jointData->getSiblingControlActorIndex();
    }

    ParallelGripperPositionController::~ParallelGripperPositionController() noexcept(true)
    {

    }

    void ParallelGripperPositionController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            float linkedPositionFactor = 2.0 / 3.0;
            target.position += (linkedDataPtr->getRelativePosition() * linkedPositionFactor);
            ARMARX_RT_LOGF_INFO("target.position %.2f, relative partner pos: %.2f", target.position, linkedDataPtr->getRelativePosition()).deactivateSpam(0.5);
            JointPWMPositionController::rtRun(sensorValuesTimestamp, timeSinceLastIteration);
        }
        else
        {
            ARMARX_ERROR << deactivateSpam(1) << "invalid target set for actor " << getParent().getDeviceName();
        }
    }

    void ParallelGripperPositionController::rtPreActivateController()
    {
        linkedDataPtr = board->getDevices().at(linkedJointConnectorIndex)->getActorDataPtr();
        ARMARX_CHECK_EXPRESSION_W_HINT(linkedDataPtr, "index: " << linkedJointConnectorIndex);
        JointPWMPositionController::rtPreActivateController();
    }
}
