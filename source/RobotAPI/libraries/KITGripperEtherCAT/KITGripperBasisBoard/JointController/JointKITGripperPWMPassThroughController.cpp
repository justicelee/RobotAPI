#include "JointKITGripperPWMPassThroughController.h"
#include <boost/algorithm/clamp.hpp>
#include "../KITGripperBasisBoard.h"

namespace armarx
{
    ControlTargetBase* JointKITGripperPWMPassThroughController::getControlTarget()
    {
        return &target;
    }

    JointKITGripperPWMPassThroughController::JointKITGripperPWMPassThroughController(const std::string deviceName, ActorDataPtr jointData) :
        jointData(jointData)
    {}

    void JointKITGripperPWMPassThroughController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            jointData->setTargetPWM(target.current * 1000);
        }
    }

    void JointKITGripperPWMPassThroughController::rtPreActivateController()
    {
        jointData->setTargetPWM(0);

    }
}
