#pragma once

#include <memory>
#include <chrono>

#include <RobotAPI/libraries/core/PIDController.h>

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include "../KITGripperBasisBoardData.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXCore/core/services/tasks/ThreadPool.h>

#include <ArmarXCore/observers/filters/AverageFilter.h>
#include "JointPWMVelocityController.h"
#include "PWMVelocityController.h"


namespace armarx
{
    using KITGripperBasisBoardPtr = std::shared_ptr<class KITGripperBasisBoard>;


    class ParallelGripperVelocityController;
    typedef std::shared_ptr<ParallelGripperVelocityController> ParallelGripperVelocityControllerPtr;

    class ParallelGripperVelocityController : public JointPWMVelocityController
    {
    public:
        ParallelGripperVelocityController(const std::string deviceName, KITGripperBasisBoardPtr board, ActorDataPtr jointData, PWMVelocityControllerConfigurationCPtr velocityControllerConfigDataPtr);
        ~ParallelGripperVelocityController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;

        void rtPreActivateController() override;
    private:

        uint32_t linkedJointConnectorIndex = -1;

        ActorDataPtr linkedDataPtr;

    };
}
