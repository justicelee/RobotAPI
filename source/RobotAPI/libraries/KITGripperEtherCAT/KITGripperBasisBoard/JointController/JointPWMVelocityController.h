#pragma once

#include <memory>
#include <chrono>

#include <RobotAPI/libraries/core/PIDController.h>

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include "../KITGripperBasisBoardData.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXCore/core/services/tasks/ThreadPool.h>

#include <ArmarXCore/observers/filters/AverageFilter.h>
#include "PWMVelocityController.h"


namespace armarx
{
    using KITGripperBasisBoardPtr = std::shared_ptr<class KITGripperBasisBoard>;


    class JointPWMVelocityController;
    typedef std::shared_ptr<JointPWMVelocityController> JointPWMVelocityControllerPtr;

    class JointPWMVelocityController : public JointController
    {
    public:
        JointPWMVelocityController(const std::string deviceName, KITGripperBasisBoardPtr board, ActorDataPtr jointData, PWMVelocityControllerConfigurationCPtr velocityControllerConfigDataPtr);
        ~JointPWMVelocityController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;
        ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
    protected:
        PWMVelocityControllerConfigurationCPtr config;
        PWMVelocityController controller;
        VelocityControllerWithRampedAccelerationAndPositionBounds velController;

        ControlTarget1DoFActuatorVelocity target;

        std::atomic<double> lastTargetVelocity, lastTargetAcceleration;
        bool isLimitless;

        ActorDataPtr dataPtr;
        KITGripperBasisBoardPtr board;
        const std::string deviceName;
        size_t actorIndex = 0;
        mutable RemoteGuiInterfacePrx remoteGui;
        bool stopRequested = false;
        mutable ThreadPool::Handle threadHandle;
        const SensorValue1DoFActuator* sensorValue;
        // JointController interface
    protected:
        void rtPostDeactivateController() override;

        // JointController interface
    public:
        StringVariantBaseMap publish(const DebugDrawerInterfacePrx& draw, const DebugObserverInterfacePrx& observer) const override;
    };
}
