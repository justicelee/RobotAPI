#include <chrono>

#include <ArmarXCore/core/logging/Logging.h>
#include "ParallelGripperVelocityController.h"
#include "../KITGripperBasisBoard.h"
#include <RobotAPI/components/units/RobotUnit/util/ControlThreadOutputBuffer.h>
#include <ArmarXCore/core/ManagedIceObject.h>


#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>

#include <ArmarXCore/core/application/Application.h>

#include <RobotAPI/components/units/RobotUnit/RobotUnitModules/RobotUnitModuleDevices.h>

namespace armarx
{
    ParallelGripperVelocityController::ParallelGripperVelocityController(const std::string deviceName, KITGripperBasisBoardPtr board, ActorDataPtr jointData,
            PWMVelocityControllerConfigurationCPtr velocityControllerConfigDataPtr) :
        JointPWMVelocityController(deviceName, board, jointData, velocityControllerConfigDataPtr)
    {
        this->linkedJointConnectorIndex = jointData->getSiblingControlActorIndex();

    }

    ParallelGripperVelocityController::~ParallelGripperVelocityController() noexcept(true)
    {

    }

    void ParallelGripperVelocityController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            float linkedVelocityFactor = 2.0f / 3.0f;
            target.velocity += linkedVelocityFactor * linkedDataPtr->getVelocity();
            JointPWMVelocityController::rtRun(sensorValuesTimestamp, timeSinceLastIteration);
        }
        else
        {
            ARMARX_ERROR << "invalid target set for actor";
        }
    }


    void ParallelGripperVelocityController::rtPreActivateController()
    {
        linkedDataPtr = board->getDevices().at(linkedJointConnectorIndex)->getActorDataPtr();
        ARMARX_CHECK_EXPRESSION_W_HINT(linkedDataPtr, "index: " << linkedJointConnectorIndex);
        JointPWMVelocityController::rtPreActivateController();
    }
}







