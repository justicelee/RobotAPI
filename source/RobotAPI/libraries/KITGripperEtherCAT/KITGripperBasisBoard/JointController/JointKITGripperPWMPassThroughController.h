#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include "../KITGripperBasisBoardData.h"
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

namespace armarx
{

    class JointKITGripperPWMPassThroughController;
    typedef std::shared_ptr<JointKITGripperPWMPassThroughController> JointKITGripperPWMPassThroughControllerPtr;

    class JointKITGripperPWMPassThroughController : public armarx::JointController
    {
    public:
        JointKITGripperPWMPassThroughController(const std::string deviceName, ActorDataPtr jointData);

        // JointController interface
        ControlTargetBase* getControlTarget() override;
    protected:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
    private:
        ControlTarget1DoFActuatorCurrent target;
        ActorDataPtr jointData;

        // JointController interface
    protected:
        void rtPreActivateController() override;
    };

}
