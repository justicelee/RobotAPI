/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "PWMVelocityController.h"

#include <ArmarXCore/observers/filters/rtfilters/AverageFilter.h>

namespace armarx
{

    PWMVelocityController::PWMVelocityController(PWMVelocityControllerConfigurationCPtr velocityControllerConfigDataPtr) :
        config(velocityControllerConfigDataPtr)
    {



        pid.reset(new PIDController(velocityControllerConfigDataPtr->p,
                                    velocityControllerConfigDataPtr->i,
                                    velocityControllerConfigDataPtr->d));
        pid->pdOutputFilter.reset(new rtfilters::AverageFilter(10));
        pid->maxIntegral = velocityControllerConfigDataPtr->maxIntegral;
        pid->conditionalIntegralErrorTreshold = velocityControllerConfigDataPtr->conditionalIntegralErrorTreshold;
        pid->threadSafe = false;
    }

    double PWMVelocityController::run(IceUtil::Time const& deltaT, double currentVelocity, double targetVelocity, double gravityTorque)
    {
        double targetPWM = 0;
        if (!this->config->feedForwardMode)
        {
            lastActualVelocity = lastActualVelocity * (1.0 - config->velocityUpdatePercent) + currentVelocity * config->velocityUpdatePercent;
            pid->update(deltaT.toSecondsDouble(), lastActualVelocity, targetVelocity);
            targetPWM = pid->getControlValue();
        }
        float torqueFF = config->feedforwardTorqueToPWMFactor * -gravityTorque;
        //        ARMARX_INFO << deactivateSpam(1) << VAROUT(torqueFF);
        targetPWM += torqueFF;


        //feed forward
        if (std::abs(targetVelocity) > 0.001 && std::abs(currentVelocity) < 0.0001f)
        {
            targetPWM += config->PWMDeadzone * math::MathUtils::Sign(targetVelocity); // deadzone
        }
        targetPWM += config->feedforwardVelocityToPWMFactor * targetVelocity; // approx. feedforward vel




        //            ARMARX_RT_LOGF_INFO("target velocity: %.3f, current velocity: %.3f, target pwm: %d, kp: %.3f ki: %f, kd: %f, max acc: %.3f",
        //                                target.velocity, dataPtr->getVelocity(), targetPWM, pid->Kp, pid->Ki, pid->Kd, controller.acceleration).deactivateSpam(1);



        return targetPWM;
    }

    void PWMVelocityController::reset(double currentVelocity)
    {
        lastActualVelocity = currentVelocity;
        pid->reset();
    }

    PWMVelocityControllerConfigurationCPtr PWMVelocityControllerConfiguration::CreatePWMVelocityControllerConfigDataFromXml(DefaultRapidXmlReaderNode node)
    {
        PWMVelocityControllerConfiguration configData;

        configData.maxVelocityRad = node.first_node("maxVelocityRad").value_as_float();
        configData.maxAccelerationRad = node.first_node("maxAccelerationRad").value_as_float();
        configData.maxDecelerationRad = node.first_node("maxDecelerationRad").value_as_float();
        configData.maxDt = node.first_node("maxDt").value_as_float();
        configData.directSetVLimit = node.first_node("directSetVLimit").value_as_float();
        configData.p = node.first_node("p").value_as_float();
        configData.i = node.first_node("i").value_as_float();
        configData.d = node.first_node("d").value_as_float();
        configData.maxIntegral = node.first_node("maxIntegral").value_as_float();
        configData.feedforwardVelocityToPWMFactor = node.first_node("feedforwardVelocityToPWMFactor").value_as_float();
        configData.feedforwardTorqueToPWMFactor = node.first_node("feedforwardTorqueToPWMFactor").value_as_float();
        configData.PWMDeadzone = node.first_node("PWMDeadzone").value_as_float();
        configData.velocityUpdatePercent = node.first_node("velocityUpdatePercent").value_as_float();
        configData.conditionalIntegralErrorTreshold = node.first_node("conditionalIntegralErrorTreshold").value_as_float();
        configData.feedForwardMode = node.first_node("FeedForwardMode").value_as_bool("1", "0");

        return std::make_shared<PWMVelocityControllerConfiguration>(configData);

    }

} // namespace armarx
