#include <chrono>

#include <ArmarXCore/core/logging/Logging.h>
#include "JointPWMPositionController.h"
#include "../KITGripperBasisBoard.h"
#include <RobotAPI/components/units/RobotUnit/util/ControlThreadOutputBuffer.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <boost/algorithm/clamp.hpp>

#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>

#include <ArmarXCore/core/application/Application.h>

#include <RobotAPI/components/units/RobotUnit/RobotUnitModules/RobotUnitModuleDevices.h>

#include <ArmarXCore/observers/filters/rtfilters/AverageFilter.h>

namespace armarx
{
    JointPWMPositionController::JointPWMPositionController(const std::string deviceName, KITGripperBasisBoardPtr board,
            ActorDataPtr jointData,
            PWMPositionControllerConfigurationCPtr positionControllerConfigDataPtr) : JointController(),
        config(positionControllerConfigDataPtr),
        //    controller(positionControllerConfigDataPtr),
        target(), board(board), deviceName(deviceName)
    {
        actorIndex = board->getActorIndex(deviceName);
        sensorValue = board->getDevices().at(actorIndex)->getSensorValue()->asA<SensorValue1DoFActuator>();
        ARMARX_CHECK_EXPRESSION_W_HINT(sensorValue, deviceName);
        dataPtr = jointData;

        posController.desiredDeceleration = positionControllerConfigDataPtr->maxDecelerationRad;
        posController.desiredJerk = 100;
        posController.maxDt = positionControllerConfigDataPtr->maxDt;
        posController.maxV = positionControllerConfigDataPtr->maxVelocityRad;
        posController.p = 4;
        posController.phase2SwitchDistance =  0.1;
        ARMARX_CHECK_GREATER_EQUAL(jointData->getSoftLimitHi(), jointData->getSoftLimitLo());
        //    controller.positionLimitHiHard = dataPtr->getHardLimitHi();
        //    posController.positionLimitHi = jointData->getSoftLimitHi();
        //    controller.positionLimitLoHard = dataPtr->getHardLimitLo();
        //    posController.positionLimitLo = jointData->getSoftLimitLo();
        //    posController.pControlPosErrorLimit = 0.02f;
        //    posController.pid->Kp = posController.calculateProportionalGain();
        //    ARMARX_IMPORTANT << "position Kp " << posController.pid->Kp;

        this->isLimitless = jointData->isLimitless();
        pidPosController.reset(new PIDController(positionControllerConfigDataPtr->p, positionControllerConfigDataPtr->i, positionControllerConfigDataPtr->d));
        pidPosController->maxIntegral = positionControllerConfigDataPtr->maxIntegral;
        pidPosController->differentialFilter.reset(new rtfilters::AverageFilter(10));

    }

    JointPWMPositionController::~JointPWMPositionController() noexcept(true)
    {
        stopRequested = true;
        try
        {
            threadHandle.join();
        }
        catch (...)
        {

        }
    }

    void JointPWMPositionController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            auto currentPosition = dataPtr->getPosition();
            //        float targetPosition = boost::algorithm::clamp(target.position,
            //                               std::min(currentPosition, posController.positionLimitLo), // lo or current position
            //                               std::max(currentPosition, posController.positionLimitHi)); // hi or current position

            if (isLimitless)
            {
                ARMARX_RT_LOGF_WARNING("Position controller not implemented for limitless joints").deactivateSpam(10);
                return;

            }
            else
            {
                //            posController.currentPosition =  currentPosition;
            }
            posController.currentV = lastTargetVelocity;
            posController.dt = timeSinceLastIteration.toSecondsDouble();
            posController.setTargetPosition(target.position);
            //        ARMARX_CHECK_EXPRESSION(posController.validParameters());
            auto r = posController.run();
            posController.currentPosition = r.position;
            posController.currentAcc = r.acceleration;
            double newVel = r.velocity;
            //        double newVel = posController.p * (posController.targetPosition - posController.currentPosition);
            //        newVel = math::MathUtils::LimitTo(newVel, posController.maxV);
            //            ARMARX_INFO << deactivateSpam(1) << VAROUT(newVel);
            if (std::isnan(newVel))
            {
                newVel = 0;
            }
            pidPosController->update(timeSinceLastIteration.toSecondsDouble(), currentPosition, r.position);
            auto targetPWM = pidPosController->getControlValue();
            //        auto targetPWM = static_cast<int>(controller.run(timeSinceLastIteration, dataPtr->getVelocity(), newVel));
            newVel = math::MathUtils::LimitTo(newVel, posController.maxV);

            float torqueFF = config->feedforwardTorqueToPWMFactor * -sensorValue->gravityTorque;
            targetPWM += torqueFF;
            targetPWM += newVel * config->feedforwardVelocityToPWMFactor;

            //        ARMARX_INFO << deactivateSpam(0.1) << VAROUT(pidPosController->previousError) << VAROUT(r.acceleration)
            //                    << VAROUT(target.position) << VAROUT(targetPWM) << VAROUT(pidPosController->Kp) << VAROUT(pidPosController->Ki)
            //                    << VAROUT(torqueFF);

            if (std::isnan(targetPWM))
            {
                ARMARX_ERROR << deactivateSpam(1) << "Target PWM of " << getParent().getDeviceName() << " is NaN!";
                targetPWM = 0.0f;
            }
            float updateRatio = 0.3;
            this->targetPWM = (1.0 - updateRatio) * this->targetPWM + updateRatio * targetPWM;
            float pwmDiff = std::abs(dataPtr->getTargetPWM() - targetPWM);
            //        ARMARX_INFO << deactivateSpam(0.3) << VAROUT(pwmDiff) << VAROUT(dataPtr->getTargetPWM()) << VAROUT(targetPWM) << VAROUT(dataPtr->getVelocity());
            if (pwmDiff > 5 || dataPtr->getVelocity() > 0.01) // avoid jittering when standing still
            {
                //            ARMARX_INFO << deactivateSpam(0.0, std::to_string(targetPWM)) << "Setting new targetPWM to" << targetPWM << " diff: " << pwmDiff << " vel: " << dataPtr->getVelocity();
                dataPtr->setTargetPWM(targetPWM);
            }

            this->targetPWM = targetPWM;
            lastTargetVelocity = newVel;
            //        auto name = getParent().getDeviceName().c_str();
            //        ARMARX_RT_LOGF_INFO("%s: position: %.f, target position: %.f, targetvelocity: %.f, target PWM: %d", name,
            //                            currentPosition, targetPosition, newVel, targetPWM).deactivateSpam(1);
            //        ARMARX_INFO << deactivateSpam(1) << VAROUT(name) << VAROUT(currentPosition) << VAROUT(targetPosition) << VAROUT(newVel) << VAROUT(targetPWM);


        }
        else
        {
            ARMARX_ERROR << deactivateSpam(1) << "invalid target set for actor " << getParent().getDeviceName();
        }
    }

    ControlTargetBase* JointPWMPositionController::getControlTarget()
    {
        return &target;
    }

    void JointPWMPositionController::rtPreActivateController()
    {
        targetPWM = 0.0f;
        lastTargetVelocity = dataPtr->getVelocity();
        posController.currentAcc = dataPtr->getAcceleration();
        posController.currentPosition = dataPtr->getPosition();
        posController.currentV = dataPtr->getVelocity();
        pidPosController->reset();
        //    controller.reset(dataPtr->getVelocity());
    }

    void JointPWMPositionController::rtPostDeactivateController()
    {
        //    ARMARX_RT_LOGF_INFO("Setting PWM to 0");
        //    dataPtr->setTargetPWM(0);
    }

    StringVariantBaseMap JointPWMPositionController::publish(const DebugDrawerInterfacePrx& draw, const DebugObserverInterfacePrx& observer) const
    {

        if (!remoteGui)
        {
            threadHandle = Application::getInstance()->getThreadPool()->runTask([this]
            {
                std::string guiTabName;
                while (!stopRequested)
                {
                    ManagedIceObjectPtr object;
                    ARMARX_IMPORTANT << deactivateSpam(1) << "Trying to get parent";
                    try
                    {
                        object = ManagedIceObjectPtr::dynamicCast(getParent().getOwner());
                        ARMARX_CHECK_EXPRESSION(object);
                        remoteGui = object->getProxy<RemoteGuiInterfacePrx>("RemoteGuiProvider", false, "", false);
                        if (!remoteGui)
                        {
                            continue;
                        }
                        ARMARX_IMPORTANT << deactivateSpam(1) << "Got Proxy";
                        guiTabName = getParent().getDeviceName() + getControlMode();
                        break;
                    }
                    catch (...)
                    {
                        sleep(1);
                    }

                }
                if (remoteGui)
                {
                    ARMARX_IMPORTANT << "Creating GUI " << guiTabName;
                    using namespace RemoteGui;



                    auto vLayout = makeVBoxLayout();

                    {
                        WidgetPtr KpLabel = makeTextLabel("Kp: ");

                        WidgetPtr KpSlider = makeFloatSlider("KpSlider")
                                             .min(0.0f).max(pidPosController->Kp * 5)
                                             .value(pidPosController->Kp);
                        WidgetPtr KpLabelValue = makeTextLabel(std::to_string(pidPosController->Kp * 5));
                        WidgetPtr line = makeHBoxLayout()
                                         .children({KpLabel, KpSlider, KpLabelValue});

                        vLayout.addChild(line);

                    }


                    {
                        WidgetPtr KiLabel = makeTextLabel("Ki: ");
                        WidgetPtr KiSlider = makeFloatSlider("KiSlider")
                                             .min(0.0f).max(pidPosController->Ki * 5)
                                             .value(pidPosController->Ki);
                        WidgetPtr KiLabelValue = makeTextLabel(std::to_string(pidPosController->Ki * 5));

                        WidgetPtr line = makeHBoxLayout()
                                         .children({KiLabel, KiSlider, KiLabelValue});

                        vLayout.addChild(line);

                    }

                    {
                        WidgetPtr KdLabel = makeTextLabel("Kd: ");
                        WidgetPtr KdSlider = makeFloatSlider("KdSlider")
                                             .min(-10.0f * pidPosController->Kd).max(10.0f * pidPosController->Kd)
                                             .steps(1000)
                                             .value(pidPosController->Kd);
                        WidgetPtr KdLabelValue = makeTextLabel(std::to_string(pidPosController->Kd * 10));

                        WidgetPtr line = makeHBoxLayout()
                                         .children({KdLabel, KdSlider, KdLabelValue});

                        vLayout.addChild(line);
                        vLayout.addChild(new VSpacer);
                    }

                    //        WidgetPtr spin = makeFloatSpinBox("KpSpin")
                    //                         .min(0.0f).max(2.0f)
                    //                         .steps(20).decimals(2)
                    //                         .value(0.4f);




                    WidgetPtr groupBox = makeGroupBox("GroupBox")
                                         .label("Group")
                                         .child(vLayout);

                    remoteGui->createTab(guiTabName, groupBox);

                    while (!stopRequested)
                    {
                        RemoteGui::TabProxy tab(remoteGui, guiTabName);
                        tab.receiveUpdates();
                        //                    this->controller.pid->Kp = tab.getValue<float>("KpSlider").get();
                        //                    this->controller.pid->Ki = tab.getValue<float>("KiSlider").get();
                        //                    this->controller.pid->Kd = tab.getValue<float>("KdSlider").get();
                        pidPosController->Kp = tab.getValue<float>("KpSlider").get();
                        pidPosController->Ki = tab.getValue<float>("KiSlider").get();
                        pidPosController->Kd = tab.getValue<float>("KdSlider").get();
                        usleep(100000);
                    }
                }

            });
        }
        return {{"lastTargetVelocity", new Variant(lastTargetVelocity.load())},
            {"targetPosition", new Variant(posController.currentPosition)}, // position of profile generator is target position
            {"posError", new Variant(posController.getTargetPosition() - posController.currentPosition)},
            {"pidError", new Variant(pidPosController->previousError)},
            //        {"filteredVelocity", new Variant(controller.lastActualVelocity.load())},
            {"pidIntegralCV", new Variant(pidPosController->integral * pidPosController->Ki)},
            {"pidIntegral", new Variant(pidPosController->integral)},
            {"pidPropCV", new Variant(pidPosController->previousError * pidPosController->Kp)},
            {"pidDiffCV", new Variant(pidPosController->derivative * pidPosController->Kd)},
            //        {"pospidIntegralCV", new Variant(posController.pid->integral * posController.pid->Ki)},
            //        {"pospidIntegral", new Variant(posController.pid->integral)},
            //        {"pospidPropCV", new Variant(posController.pid->previousError * posController.pid->Kp)},
            //        {"pospidDiffCV", new Variant(posController.pid->derivative * posController.pid->Kd)},
            //        {"pidUsed", new Variant(posController.getCurrentlyPIDActive())},
            {"desiredPWM", new Variant(targetPWM.load())}


        };
    }






    PWMPositionControllerConfigurationCPtr PWMPositionControllerConfiguration::CreatePWMPositionControllerConfigDataFromXml(DefaultRapidXmlReaderNode node)
    {
        PWMPositionControllerConfiguration configData;

        configData.maxVelocityRad = node.first_node("maxVelocityRad").value_as_float();
        configData.maxAccelerationRad = node.first_node("maxAccelerationRad").value_as_float();
        configData.maxDecelerationRad = node.first_node("maxDecelerationRad").value_as_float();
        configData.maxDt = node.first_node("maxDt").value_as_float();
        configData.p = node.first_node("p").value_as_float();
        configData.i = node.first_node("i").value_as_float();
        configData.d = node.first_node("d").value_as_float();
        configData.maxIntegral = node.first_node("maxIntegral").value_as_float();
        configData.feedforwardVelocityToPWMFactor = node.first_node("feedforwardVelocityToPWMFactor").value_as_float();
        configData.feedforwardTorqueToPWMFactor = node.first_node("feedforwardTorqueToPWMFactor").value_as_float();
        configData.PWMDeadzone = node.first_node("PWMDeadzone").value_as_float();
        configData.velocityUpdatePercent = node.first_node("velocityUpdatePercent").value_as_float();
        configData.conditionalIntegralErrorTreshold = node.first_node("conditionalIntegralErrorTreshold").value_as_float();
        configData.feedForwardMode = node.first_node("FeedForwardMode").value_as_bool("1", "0");

        return std::make_shared<PWMPositionControllerConfiguration>(configData);
    }
}
