/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "KITGripperBasisBoard.h"
#include <VirtualRobot/Nodes/RobotNode.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include "JointController/JointPWMPositionController.h"
#include "JointController/JointZeroTorqueController.h"
#include "JointController/JointKITGripperEmergencyStopController.h"
#include "JointController/JointKITGripperStopMovementController.h"
#include "JointController/JointPWMVelocityController.h"
#include "JointController/JointKITGripperPWMPassThroughController.h"
#include "JointController/ParallelGripperPositionController.h"
#include "JointController/ParallelGripperVelocityController.h"
#include "Misc/TorqueEstimation.h"
namespace armarx
{
    VirtualDeviceFactory::SubClassRegistry KITGripperBasisBoard::registry("KITGripperBasisBoard", &VirtualDeviceFactory::createInstance<KITGripperBasisBoard>);

    KITGripperBasisBoard::KITGripperBasisBoard(RapidXmlReaderNode node, DefaultRapidXmlReaderNode defaultConfigurationNode, const VirtualRobot::RobotPtr& robot) :
        DeviceBase(node.attribute_value("name")),
        SensorDevice(node.attribute_value("name")),
        //        ControlDevice(node.attribute_value("name")),
        AbstractFunctionalDevice(defaultConfigurationNode.first_node("KITGripperBasisBoardDefaultConfiguration")
                                 .add_node_at_end(node)),
        configNode(node),
        defaultConfigurationNode(defaultConfigurationNode),
        robot(robot),
        slaveIdentifier(node.first_node("Identifier"))
    {
        slaveIdentifier.humanName = node.attribute_value("name");
        ARMARX_VERBOSE << "found " << configNode.nodes("Actor").size() << " actors";
        for (auto motorNode : configNode.nodes("Actor"))
        {
            auto connectorIndex = motorNode.attribute_as_uint("connector");
            auto name = motorNode.attribute_value("name");
            auto enabled = motorNode.attribute_as_bool("enabled", "true", "false");

            if (enabled)
            {
                ARMARX_VERBOSE << "Found motor configuration for connector index " << connectorIndex;
                //                auto actorData = dataPtr->getActorData(connectorIndex);
                //                ARMARX_CHECK_EXPRESSION_W_HINT(actorData, name);
                auto robotNode = robot->getRobotNode(name);
                ARMARX_CHECK_EXPRESSION_W_HINT(robotNode, name);
                ARMARX_INFO << "Creating actor class for " << name;
                KITGripperBasisBoard::ActorRobotUnitDevicePtr ptr  = std::make_shared<KITGripperBasisBoard::ActorRobotUnitDevicePtr::element_type>(connectorIndex, name, robotNode);
                devices.push_back(ptr);

            }
            else
            {
                ARMARX_INFO << "motor at Index " << connectorIndex << " disabled";
            }
        }
    }

    void KITGripperBasisBoard::init(KITGripperBasisBoardSlavePtr slave)
    {
        this->slave = slave;
        initialized = true;

    }

    void KITGripperBasisBoard::initData()
    {
        dataPtr = std::make_shared<KITGripperBasisBoardData>(configNode, defaultConfigurationNode,
                  slave->getOutputsPtr(), slave->getInputsPtr(), robot);

        for (auto motorNode : configNode.nodes("Actor"))
        {
            auto enabled = motorNode.attribute_as_bool("enabled", "true", "false");
            auto name = motorNode.attribute_value("name");
            if (enabled)
            {
                auto i = getActorIndex(name);
                devices.at(i)->init(std::dynamic_pointer_cast<KITGripperBasisBoard>(shared_from_this()), dataPtr,
                                    motorNode, defaultConfigurationNode);
            }

        }

    }

    const SensorValueBase* KITGripperBasisBoard::getSensorValue() const
    {
        return &sensorValue;
    }

    const SlaveIdentifier& KITGripperBasisBoard::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }

    void KITGripperBasisBoard::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        // TODO: read IMU
        sensorValue.IMUTemperature = dataPtr->getIMUTemperature();
    }

    //    void KITGripperBasisBoard::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    //    {
    //        // TODO: write LED targets
    //    }

    const std::vector<KITGripperBasisBoard::ActorRobotUnitDevicePtr>& KITGripperBasisBoard::getDevices() const
    {
        return devices;
    }

    size_t KITGripperBasisBoard::getActorIndex(const std::string& actorName)
    {
        size_t i = 0;
        for (auto& actor : devices)
        {
            if (actor->getRobotNode()->getName() == actorName)
            {
                return i;
            }
            i++;
        }
        throw LocalException() << "Could not find actor with name: " << actorName << "\nactors:\n" << ARMARX_STREAM_PRINTER { for (auto& actor : devices)
    {
        out << actor->getDeviceName();
        }
                                                                                                                            };
    }

    KITGripperBasisBoard::ActorRobotUnitDevice::ActorRobotUnitDevice(size_t connectorIndex, const std::string& deviceName, VirtualRobot::RobotNodePtr robotNode) :
        DeviceBase(deviceName),
        ControlDevice(deviceName),
        SensorDevice(deviceName),
        actorIndex(connectorIndex)
    {
        ARMARX_CHECK_EXPRESSION_W_HINT(robotNode, deviceName);
        this->robotNode = robotNode;
        ARMARX_INFO << deviceName << " actor created";
    }

    void KITGripperBasisBoard::ActorRobotUnitDevice::init(KITGripperBasisBoardPtr dev, KITGripperBasisBoardDataPtr dataPtr,
            RapidXmlReaderNode configNode, DefaultRapidXmlReaderNode defaultConfigurationNode)
    {
        this->board = dev;
        this->actorDataPtr = dataPtr->getActorData(actorIndex);
        emergencyController.reset(new JointKITGripperEmergencyStopController(actorDataPtr));
        addJointController(emergencyController.get());
        stopMovementController.reset(new JointKITGripperStopMovementController(actorDataPtr));
        addJointController(stopMovementController.get());
        auto positionControllerCfg =   PWMPositionControllerConfiguration::CreatePWMPositionControllerConfigDataFromXml(
                                           defaultConfigurationNode.first_node("JointPWMPositionControllerDefaultConfiguration")
                                           .add_node_at_end(configNode.first_node("JointPWMPositionControllerConfig")));

        auto velocityControllerCfg =   PWMVelocityControllerConfiguration::CreatePWMVelocityControllerConfigDataFromXml(
                                           defaultConfigurationNode.first_node("JointPWMVelocityControllerDefaultConfiguration")
                                           .add_node_at_end(configNode.first_node("JointPWMVelocityControllerConfig")));
        auto zeroTorqueControllerCfg =   PWMZeroTorqueControllerConfiguration::CreateConfigDataFromXml(
                                             defaultConfigurationNode.first_node("JointPWMZeroTorqueControllerDefaultConfiguration")
                                             .add_node_at_end(configNode.first_node("JointPWMZeroTorqueControllerConfig")));

        pwmController.reset(new JointKITGripperPWMPassThroughController(getDeviceName(), actorDataPtr));
        addJointController(pwmController.get());
        //        ARMARX_CHECK_EQUAL_W_HINT(
        //            configNode.has_node("ParallelGripperDecoupplingFactor"),
        //            configNode.has_node("SiblingConnectorId"),
        //            "Either both or none have to be set.");
        //        auto tempConfigNode = defaultConfigurationNode.first_node("KITGripperActorDefaultConfiguration").
        //                              add_node_at_end(configNode);
        //        parallelGripperDecouplingFactor = tempConfigNode.first_node("ParallelGripperDecoupplingFactor").value_as_float();

        //        if (configNode.has_node("ParallelGripperDecoupplingFactor"))
        //        {
        //            const int siblingConnectorId = configNode.first_node("SiblingConnectorId").value_as_int32();

        //            //get sibling
        //            for (const ActorRobotUnitDevicePtr& dev : board->devices)
        //            {
        //                if (dev->actorIndex == siblingConnectorId)
        //                {
        //                    sibling = dev.get();
        //                    break;
        //                }
        //            }
        //            ARMARX_CHECK_NOT_NULL_W_HINT(sibling, "Sibling with connector index " << siblingConnectorId << " not found");
        //            ARMARX_INFO << "Device " << board->getDeviceName() << ", actor " << getDeviceName() << " is using "
        //                        << sibling->getDeviceName() << " as a sibling for relative position sensor values";
        //        }

        if (false && actorDataPtr->getSiblingControlActorIndex() != -1)
        {
            ARMARX_IMPORTANT << "Using coupled mode for " << getDeviceName();
            // Add Controllers for ParallelGripper
            if (actorDataPtr->getVelocityControlEnabled())
            {
                velocityController.reset(new ParallelGripperVelocityController(getDeviceName(), dev, actorDataPtr, velocityControllerCfg));
                addJointController(velocityController.get());
            }
            else
            {
                ARMARX_VERBOSE << "Velocity Control disabled for " << getDeviceName();
            }
            if (actorDataPtr->getPositionControlEnabled())
            {
                positionController.reset(new ParallelGripperPositionController(getDeviceName(), dev, actorDataPtr, positionControllerCfg));
                addJointController(positionController.get());
            }
            else
            {
                ARMARX_VERBOSE << "Position Control disabled for " << getDeviceName();
            }
            //            //TODO: Does PG get zero torque controller? If so, a special one?
            //            zeroTorqueController.reset(new JointPWMZeroTorqueController(getDeviceName(), dev, actorPtr, zeroTorqueControllerCfg));
            //            addJointController(zeroTorqueController.get());
            return;
        }
        else
        {
            if (actorDataPtr->getVelocityControlEnabled())
            {
                velocityController.reset(new JointPWMVelocityController(getDeviceName(), dev, actorDataPtr, velocityControllerCfg));
                addJointController(velocityController.get());
            }
            else
            {
                ARMARX_VERBOSE << "Velocity Control disabled for " << getDeviceName();
            }
            zeroTorqueController.reset(new JointPWMZeroTorqueController(getDeviceName(), dev, actorDataPtr, zeroTorqueControllerCfg));
            addJointController(zeroTorqueController.get());
            if (actorDataPtr->getPositionControlEnabled())
            {
                positionController.reset(new JointPWMPositionController(getDeviceName(), dev, actorDataPtr, positionControllerCfg));
                addJointController(positionController.get());
            }
            else
            {
                ARMARX_VERBOSE << "Position Control disabled for " << getDeviceName();
            }
        }
    }

    ActorDataPtr KITGripperBasisBoard::ActorRobotUnitDevice::getActorDataPtr() const
    {
        return actorDataPtr;
    }

    const VirtualRobot::RobotNodePtr& KITGripperBasisBoard::ActorRobotUnitDevice::getRobotNode() const
    {
        return robotNode;
    }

    void KITGripperBasisBoard::ActorRobotUnitDevice::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        sensorValue.position = actorDataPtr->getPosition();
        sensorValue.relativePosition = actorDataPtr->getRelativePosition();
        sensorValue.velocity = actorDataPtr->getVelocity();
        sensorValue.acceleration = actorDataPtr->getAcceleration();
        sensorValue.absoluteEncoderVelocity = actorDataPtr->getAbsoluteEncoderVelocity();
        sensorValue.targetPWM = actorDataPtr->getTargetPWM();
        sensorValue.motorCurrent = actorDataPtr->getTargetPWM();
        sensorValue.minPWM = actorDataPtr->getCurrentMinPWM();
        sensorValue.maxPWM = actorDataPtr->getCurrentMaxPWM();
        sensorValue.velocityTicksPerMs = actorDataPtr->getVelocityTicks();
        sensorValue.torque = estimateTorque(sensorValue.velocityTicksPerMs, sensorValue.targetPWM);
    }

    void KITGripperBasisBoard::ActorRobotUnitDevice::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {


    }

} // namespace



