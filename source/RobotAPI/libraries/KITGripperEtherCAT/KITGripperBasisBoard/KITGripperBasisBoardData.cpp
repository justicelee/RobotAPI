/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "KITGripperBasisBoardData.h"
#include <VirtualRobot/Robot.h>
#include <RobotAPI/libraries/core/math/MathUtils.h>
namespace armarx
{

    KITGripperBasisBoardData::KITGripperBasisBoardData(const RapidXmlReaderNode& node, DefaultRapidXmlReaderNode defaultConfigurationNode, KITGripperBasisBoardOUT_t* sensorOUT, KITGripperBasisBoardIN_t* sensorIN, VirtualRobot::RobotPtr robot) :
        sensorOUT(sensorOUT),
        sensorIN(sensorIN)
    {
        ARMARX_CHECK_EXPRESSION(sensorOUT);
        ARMARX_CHECK_EXPRESSION(sensorIN);
        actorData.resize(3);
        for (auto motorNode : node.nodes("Actor"))
        {
            auto connectorIndex = motorNode.attribute_as_uint("connector");
            auto name = motorNode.attribute_value("name");
            //auto enabled = motorNode.attribute_as_bool("enabled", "true", "false");
            auto conversionNode = defaultConfigurationNode.first_node("KITGripperBasisBoardConversionParametersDefaultConfig").
                                  add_node_at_end(motorNode.first_node("ConversionParameters"));
            auto configNode = defaultConfigurationNode.first_node("KITGripperActorDefaultConfiguration").
                              add_node_at_end(motorNode);
            auto positionControlEnabled = configNode.first_node("PositionControlEnabled").value_as_bool("1", "0");
            auto velocityControlEnabled = configNode.first_node("VelocityControlEnabled").value_as_bool("1", "0");
            ARMARX_IMPORTANT << "Creating actor data class for " << name << " at index " << connectorIndex;
            auto initActorData = [&](int* position, int* velocity, int* torque, int* targetPWM)
            {
                actorData.at(connectorIndex).reset(new ActorData);
                actorData.at(connectorIndex)->targetPWMPtr.init(targetPWM, conversionNode.first_node("pwm"), std::nan("1"), true, "targetPWMPtr");
                actorData.at(connectorIndex)->maxPWM = configNode.first_node("maxPWM").value_as_uint32();
                actorData.at(connectorIndex)->position.init(&actorData.at(connectorIndex)->rawABSEncoderTicks, conversionNode.first_node("position"), std::nan("1"), true, "position");
                actorData.at(connectorIndex)->relativePosition.init(position, conversionNode.first_node("relativePosition"), std::nan("1"), true, "relativePosition");
                actorData.at(connectorIndex)->velocity.init(velocity, conversionNode.first_node("velocity"), std::nan("1"), true, "velocity");
                actorData.at(connectorIndex)->torque.init(torque, conversionNode.first_node("torque"), std::nan("1"), true, "torque");
                actorData.at(connectorIndex)->robotNode = robot->getRobotNode(name);
                actorData.at(connectorIndex)->velocityTicks = velocity;
                actorData.at(connectorIndex)->positionControlEnabled = positionControlEnabled;
                actorData.at(connectorIndex)->velocityControlEnabled = velocityControlEnabled;

                actorData.at(connectorIndex)->parallelGripperDecouplingFactor = configNode.first_node("ParallelGripperDecouplingFactor").value_as_float();
                actorData.at(connectorIndex)->parallelControlEnabled = configNode.first_node("ParallelControlSiblingIndex").value_as_int32();
                actorData.at(connectorIndex)->currentPWMBoundGradient = configNode.first_node("CurrentPWMBoundGradient").value_as_float();
                actorData.at(connectorIndex)->currentPWMBoundOffset = configNode.first_node("CurrentPWMBoundOffset").value_as_int32();
            };
            switch (connectorIndex)
            {
                case 0:
                    initActorData(&sensorOUT->motor1_current_pos, &sensorOUT->motor1_current_speed, &sensorOUT->motor1_current_torque, &sensorIN->motor1_target_pwm);
                    break;
                case 1:
                    initActorData(&sensorOUT->motor2_current_pos, &sensorOUT->motor2_current_speed, &sensorOUT->motor2_current_torque, &sensorIN->motor2_target_pwm);
                    break;
                case 2:
                    initActorData(&sensorOUT->motor3_current_pos, &sensorOUT->motor3_current_speed, &sensorOUT->motor3_current_torque, &sensorIN->motor3_target_pwm);
                    break;
                default:
                    throw LocalException("Motor index out of range: ") << connectorIndex;
            }

        }
    }

    void KITGripperBasisBoardData::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        int i = 0;
        double dt = timeSinceLastIteration.toSecondsDouble();
        for (auto& ptr : actorData)
        {
            if (!ptr)
            {
                ++i;
                continue;
            }
            ActorData& d = *ptr;


            d.relativePosition.read();

            if (d.getSiblingControlActorIndex() >= 0)
            {
                auto& d2 = actorData.at(d.getSiblingControlActorIndex());
                d.adjustedRelativePosition = d.relativePosition.value +
                                             d2->relativePosition.value * d.parallelGripperDecouplingFactor;
            }
            else
            {
                d.adjustedRelativePosition = d.relativePosition.value;
            }

            if (std::isnan(d.relativePositionOffset)) // initialize on first run
            {
                d.relativePositionOffset = -d.relativePosition.value + d.position.value;
            }

            if (i == 0)
            {
                d.rawABSEncoderTicks = (((uint32_t)sensorOUT->RawABSEncoderValueBytes[0] << 24 | (uint32_t)sensorOUT->RawABSEncoderValueBytes[1] << 16 | (uint32_t)sensorOUT->RawABSEncoderValueBytes[2] << 8 | (uint32_t)sensorOUT->RawABSEncoderValueBytes[3]) & 0xFFFFF000) >> 12;
            }
            else if (i == 1)
            {
                d.rawABSEncoderTicks = (((uint32_t)sensorOUT->RawABSEncoder2ValueBytes[0] << 24 | (uint32_t)sensorOUT->RawABSEncoder2ValueBytes[1] << 16 | (uint32_t)sensorOUT->RawABSEncoder2ValueBytes[2] << 8 | (uint32_t)sensorOUT->RawABSEncoder2ValueBytes[3]) & 0xFFFFF000) >> 12;
            }
            else
            {
                d.rawABSEncoderTicks = 0;
            }


            if (i > 1)
            {
                d.position.value = d.adjustedRelativePosition;
            }
            else
            {
                d.position.read();
            }
            d.sanitizedAbsolutePosition = math::MathUtils::angleModPI(d.position.value);

            //ARMARX_RT_LOGF_INFO("position %d, relative position: %d", (int)d.rawABSEncoderTicks, d.relativePosition.value).deactivateSpam(0.5);
            if (!std::isnan(d.lastAbsolutePosition))
            {
                d.absoluteEncoderVelocity = math::MathUtils::AngleDelta(d.lastAbsolutePosition, d.sanitizedAbsolutePosition) / timeSinceLastIteration.toSecondsDouble();
            }
            d.lastAbsolutePosition = d.sanitizedAbsolutePosition;
            float oldVelocity = d.velocity.value;
            d.velocity.read();
            d.velocity.value = d.velocityFilter.update(sensorValuesTimestamp, d.velocity.value);
            d.torque.read();
            d.currentMaxPWM = std::round(*d.velocityTicks * d.currentPWMBoundGradient + d.currentPWMBoundOffset);
            d.currentMinPWM = std::round(*d.velocityTicks * d.currentPWMBoundGradient - d.currentPWMBoundOffset);
            d.acceleration = (d.velocity.value - oldVelocity) / dt;
            //            d.acceleration.read();
            //            d.gravityTorque.read();
            //            d.motorCurrent.read();
            //            d.motorTemperature.read();
            i++;
        }

    }

    void KITGripperBasisBoardData::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        //        int i = 0;
        //        for (auto& ptr : actorData)
        //        {
        //            if (!ptr)
        //            {
        //                ++i;
        //                continue;
        //            }
        //            ActorData& d = *ptr;
        //            d.targetPWMPtr.write();

        //            i++;
        //        }

        for (auto& ptr : actorData)
        {
            if (!ptr)
            {

                continue;
            }
            ActorData& d = *ptr;
            if (this->getIMUTemperature() > 87)
            {
                ARMARX_RT_LOGF_WARNING("IMU Temperature of a gripper board is too high! %d degree celcius").deactivateSpam(5);
                d.setTargetPWM(0);
            }
        }
    }

    ActorDataPtr& KITGripperBasisBoardData::getActorData(size_t actorIndex)
    {
        ARMARX_CHECK_LESS(actorIndex, actorData.size());
        ARMARX_CHECK_EXPRESSION_W_HINT(actorData.at(actorIndex), actorIndex);
        return actorData.at(actorIndex);
    }

    int8_t KITGripperBasisBoardData::getIMUTemperature() const
    {
        return sensorOUT->IMUTemperature;
    }

    ActorData::ActorData() :
        velocityFilter(10)
    {

    }

    void ActorData::setTargetPWM(int32_t targetPWM)
    {
        targetPWM = math::MathUtils::LimitMinMax(currentMinPWM, currentMaxPWM, targetPWM);
        targetPWM = math::MathUtils::LimitTo(targetPWM, maxPWM);

        targetPWMPtr.value = targetPWM;
        targetPWMPtr.write();
        //        ARMARX_RT_LOGF_INFO(" pwm: %d, raw pwm: %d, factor: %.f", targetPWMPtr.value, targetPWMPtr.getRaw(), targetPWMPtr.getFactor());
    }

    float ActorData::getPosition() const
    {
        return sanitizedAbsolutePosition;
    }

    float ActorData::getRelativePosition() const
    {
        return adjustedRelativePosition;
    }

    float ActorData::getCompensatedRelativePosition() const
    {
        return adjustedRelativePosition + relativePositionOffset;
    }

    float ActorData::getVelocity() const
    {
        return velocity.value;
    }

    float ActorData::getAcceleration() const
    {
        return acceleration;
    }

    float ActorData::getTorque() const
    {
        return torque.value;
    }

    float ActorData::getSoftLimitHi() const
    {
        return robotNode->getJointLimitHigh();
    }

    float ActorData::getSoftLimitLo() const
    {
        return robotNode->getJointLimitLo();
    }

    bool ActorData::isLimitless() const
    {
        return robotNode->isLimitless();
    }

    int32_t ActorData::getTargetPWM() const
    {
        return targetPWMPtr.value;
    }

    int32_t ActorData::getVelocityTicks() const
    {
        return *velocityTicks;
    }

    int32_t ActorData::getCurrentMinPWM() const
    {
        return currentMinPWM;
    }

    int32_t ActorData::getCurrentMaxPWM() const
    {
        return currentMaxPWM;
    }

    int32_t ActorData::getSiblingControlActorIndex() const
    {
        return parallelControlEnabled;
    }

    bool ActorData::getPositionControlEnabled() const
    {
        return positionControlEnabled;
    }

    bool ActorData::getVelocityControlEnabled() const
    {
        return velocityControlEnabled;
    }

    float ActorData::getCurrentPWMBoundGradient() const
    {
        return currentPWMBoundGradient;
    }

    int32_t ActorData::getCurrentPWMBoundOffset() const
    {
        return currentPWMBoundOffset;
    }

    float ActorData::getParallelGripperDecouplingFactor() const
    {
        return parallelGripperDecouplingFactor;
    }

    float ActorData::getAbsoluteEncoderVelocity() const
    {
        return absoluteEncoderVelocity;
    }

} // namespace armarx
