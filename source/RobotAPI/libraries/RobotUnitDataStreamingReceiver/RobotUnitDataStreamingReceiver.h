/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnitDataStreamingReceiver
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/ManagedIceObject.h>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

namespace armarx::detail::RobotUnitDataStreamingReceiver
{
    TYPEDEF_PTRS_HANDLE(Receiver);
}
namespace armarx
{
    TYPEDEF_PTRS_SHARED(RobotUnitDataStreamingReceiver);
    /**
    * @defgroup Library-RobotUnitDataStreamingReceiver RobotUnitDataStreamingReceiver
    * @ingroup RobotAPI
    * A description of the library RobotUnitDataStreamingReceiver.
    *
    * @class RobotUnitDataStreamingReceiver
    * @ingroup Library-RobotUnitDataStreamingReceiver
    * @brief Brief description of class RobotUnitDataStreamingReceiver.
    *
    * Detailed description of class RobotUnitDataStreamingReceiver.
    */
    class RobotUnitDataStreamingReceiver
    {
    public:
        using clock_t    = std::chrono::high_resolution_clock;
        using timestep_t = RobotUnitDataStreaming::TimeStep;
        using entry_t    = RobotUnitDataStreaming::DataEntry;

        RobotUnitDataStreamingReceiver(
            const ManagedIceObjectPtr& obj,
            const RobotUnitInterfacePrx& ru,
            const RobotUnitDataStreaming::Config& cfg);
        ~RobotUnitDataStreamingReceiver();

        std::deque<timestep_t>& getDataBuffer();
        const RobotUnitDataStreaming::DataStreamingDescription& getDataDescription() const;

        static void visitEntry(auto&& f, const timestep_t& st, const entry_t& e)
        {
            using enum_t = RobotUnitDataStreaming::DataEntryType;
            // *INDENT-OFF*
            switch (e.type)
            {
                case enum_t::NodeTypeBool  : f(st.bools  .at(e.index)); break;
                case enum_t::NodeTypeByte  : f(st.bytes  .at(e.index)); break;
                case enum_t::NodeTypeShort : f(st.shorts .at(e.index)); break;
                case enum_t::NodeTypeInt   : f(st.ints   .at(e.index)); break;
                case enum_t::NodeTypeLong  : f(st.longs  .at(e.index)); break;
                case enum_t::NodeTypeFloat : f(st.floats .at(e.index)); break;
                case enum_t::NodeTypeDouble: f(st.doubles.at(e.index)); break;
            };
            // *INDENT-ON*
        }

        static void visitEntries(auto&& f, const timestep_t& st, const auto& cont)
        {
            for (const entry_t& e : cont)
            {
                visitEntry(f, st, e);
            }
        }

    private:
        ManagedIceObjectPtr                                 _obj;
        RobotUnitInterfacePrx                               _ru;
        detail::RobotUnitDataStreamingReceiver::ReceiverPtr _receiver;
        RobotUnitDataStreaming::ReceiverPrx                 _proxy;
        RobotUnitDataStreaming::DataStreamingDescription    _description;
        std::deque<RobotUnitDataStreaming::TimeStep>        _data_buffer;
        long                                                _last_iteration_id = -1;
    };
}
