/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ControllerUIUtility
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoteGui.h"
namespace armarx::RemoteGui
{
    detail::GroupBoxBuilder makeConfigGui(
        const std::string& name,
        const NJointCartesianWaypointControllerRuntimeConfig& val)
    {
        detail::GroupBoxBuilder builder = makeConfigGui(name, val.wpCfg);
        auto& cs = builder.child(0)->children;

        cs.emplace_back(RemoteGui::makeTextLabel("Force limit:"));
        cs.emplace_back(RemoteGui::makeFloatSpinBox(name + "_forceThreshold")
                        .min(-1)
                        .max(1000)
                        .value(val.forceThreshold)
                        .decimals(3));
        cs.emplace_back(RemoteGui::makeCheckBox(name + "_forceThresholdInRobotRootZ")
                        .value(val.forceThresholdInRobotRootZ)
                        .label("Threshold only in root z"));


        cs.emplace_back(new RemoteGui::Widget);
        cs.emplace_back(RemoteGui::makeCheckBox(name + "_optimizeNullspaceIfTargetWasReached")
                        .value(val.optimizeNullspaceIfTargetWasReached)
                        .label("Optimize nullspace if target was reached"));
        cs.emplace_back(new RemoteGui::HSpacer);

        return builder;
    }
}
namespace armarx::RemoteGui::detail::_getValue
{
    void GetValueOutputParameter <NJointCartesianWaypointControllerRuntimeConfig, void>::call(
        NJointCartesianWaypointControllerRuntimeConfig& cfg,
        RemoteGui::ValueMap const& values, std::string const& name)
    {
        getValue(cfg.wpCfg,          values, name);
        getValue(cfg.forceThreshold, values, name + "_forceThreshold");
        getValue(cfg.forceThresholdInRobotRootZ, values, name + "_forceThresholdInRobotRootZ");
        getValue(cfg.optimizeNullspaceIfTargetWasReached, values, name + "_optimizeNullspaceIfTargetWasReached");
    }
}
