
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore RobotAPIControllerUIUtility)
 
armarx_add_test(RobotAPIControllerUIUtilityTest ControllerUIUtilityTest.cpp "${LIBS}")
