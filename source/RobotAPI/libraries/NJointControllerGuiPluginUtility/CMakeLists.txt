set(LIB_NAME       NJointControllerGuiPluginUtility)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

set(COMPONENT_LIBS
    SimpleConfigDialog
    RobotAPIComponentPlugins
)
set(SOURCES 
    detail/NJointControllerGuiPluginBase.cpp
)
set(HEADERS
    NJointControllerGuiPluginBase.h
    detail/NJointControllerGuiPluginBase.h
)
set(GUI_MOC_HDRS detail/NJointControllerGuiPluginBase.h)
set(GUI_UIS)

if(ArmarXGui_FOUND)
    armarx_gui_library("${LIB_NAME}" "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
