/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DebugDrawerConfigWidget
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DebugDrawerConfigWidget.h"
#include <QInputDialog>

namespace armarx
{

    DebugDrawerConfigWidget::DebugDrawerConfigWidget(const DebugDrawerComponentPtr& debugDrawer, QWidget* parent)
        : QWidget(parent),
          debugDrawer(debugDrawer)
    {
        ui.setupUi(this);
        refresh();

        connect(ui.pushButtonRefresh, &QPushButton::released, this, &DebugDrawerConfigWidget::refresh);
        connect(ui.listWidgetLayerVisibility, &QListWidget::itemChanged, this, &DebugDrawerConfigWidget::onVisibilityChanged);
        connect(ui.checkBoxDefaultLayerVisibility, &QCheckBox::stateChanged, this, &DebugDrawerConfigWidget::onDefaultLayerVisibilityChanged);
        connect(ui.listWidgetDefaultLayerVisibility, &QListWidget::itemChanged, this, &DebugDrawerConfigWidget::onDefaultLayerVisibilityPerLayerChanged);
        connect(ui.pushButtonDefaultLayerVisibilityAdd, &QPushButton::released, this, &DebugDrawerConfigWidget::onAddDefaultLayerVisibility);
        connect(ui.pushButtonDefaultLayerVisibilityRemove, &QPushButton::released, this, &DebugDrawerConfigWidget::onRemoveDefaultLayerVisibility);
    }

    void DebugDrawerConfigWidget::loadSettings(QSettings* settings)
    {
        ui.checkBoxDefaultLayerVisibility->setCheckState(settings->value(QString::fromStdString("DefaultLayerVisibility")).toBool() ? Qt::Checked : Qt::Unchecked);
        settings->beginGroup("layer");
        for (const auto& key : settings->allKeys())
        {
            QListWidgetItem* item = new QListWidgetItem(key, ui.listWidgetDefaultLayerVisibility);
            item->setCheckState(settings->value(key).toBool() ? Qt::Checked : Qt::Unchecked);
            ui.listWidgetDefaultLayerVisibility->addItem(item);
        }
        settings->endGroup();
    }

    void DebugDrawerConfigWidget::saveSettings(QSettings* settings)
    {
        settings->setValue(QString::fromStdString("DefaultLayerVisibility"), ui.checkBoxDefaultLayerVisibility->checkState() == Qt::Checked);
        settings->beginGroup("layer");
        for (const auto& layerInfo : debugDrawer->getAllDefaultLayerVisibilities())
        {
            settings->setValue(QString::fromStdString(layerInfo.first), layerInfo.second);
        }
        settings->endGroup();
    }

    void DebugDrawerConfigWidget::setDebugDrawer(const DebugDrawerComponentPtr& debugDrawer)
    {
        this->debugDrawer = debugDrawer;
        onDefaultLayerVisibilityChanged(ui.checkBoxDefaultLayerVisibility->checkState());
        for (int i = 0; i < ui.listWidgetDefaultLayerVisibility->count(); ++i)
        {
            onDefaultLayerVisibilityPerLayerChanged(ui.listWidgetDefaultLayerVisibility->item(i));
        }
        refresh();
    }

    DebugDrawerComponentPtr DebugDrawerConfigWidget::getDebugDrawer() const
    {
        return debugDrawer;
    }

    void DebugDrawerConfigWidget::refresh()
    {
        if (!debugDrawer)
        {
            return;
        }
        ui.listWidgetLayerVisibility->clear();
        for (const auto& layerInfo : debugDrawer->layerInformation())
        {
            QListWidgetItem* item = new QListWidgetItem(QString::fromStdString(layerInfo.layerName), ui.listWidgetLayerVisibility);
            item->setCheckState(layerInfo.visible ? Qt::Checked : Qt::Unchecked);
            ui.listWidgetLayerVisibility->addItem(item);
        }
        ui.checkBoxDefaultLayerVisibility->setCheckState(debugDrawer->getDefaultLayerVisibility() ? Qt::Checked : Qt::Unchecked);
        ui.listWidgetDefaultLayerVisibility->clear();
        for (const auto& layerInfo : debugDrawer->getAllDefaultLayerVisibilities())
        {
            QListWidgetItem* item = new QListWidgetItem(QString::fromStdString(layerInfo.first), ui.listWidgetDefaultLayerVisibility);
            item->setCheckState(layerInfo.second ? Qt::Checked : Qt::Unchecked);
            ui.listWidgetDefaultLayerVisibility->addItem(item);
        }
    }

    void DebugDrawerConfigWidget::onVisibilityChanged(QListWidgetItem* item)
    {
        if (!debugDrawer)
        {
            return;
        }
        debugDrawer->enableLayerVisu(item->text().toStdString(), item->checkState() == Qt::Checked);
    }

    void DebugDrawerConfigWidget::onDefaultLayerVisibilityChanged(int checkState)
    {
        if (!debugDrawer)
        {
            return;
        }
        debugDrawer->setDefaultLayerVisibility(checkState != Qt::Unchecked);
    }

    void DebugDrawerConfigWidget::onDefaultLayerVisibilityPerLayerChanged(QListWidgetItem* item)
    {
        if (!debugDrawer)
        {
            return;
        }
        debugDrawer->setDefaultLayerVisibility(item->text().toStdString(), item->checkState() == Qt::Checked);
    }

    void DebugDrawerConfigWidget::onAddDefaultLayerVisibility()
    {
        bool ok;
        QString text = QInputDialog::getText(this, "Layer name",
                                             "Layer name:", QLineEdit::Normal,
                                             ui.listWidgetLayerVisibility->selectedItems().empty() ? "" : ui.listWidgetLayerVisibility->selectedItems().front()->text(), &ok);
        if (ok && !text.isEmpty())
        {
            QListWidgetItem* item = new QListWidgetItem(text, ui.listWidgetDefaultLayerVisibility);
            item->setCheckState(debugDrawer->getDefaultLayerVisibility() ? Qt::Checked : Qt::Unchecked);
            ui.listWidgetDefaultLayerVisibility->addItem(item);
        }
    }

    void DebugDrawerConfigWidget::onRemoveDefaultLayerVisibility()
    {
        for (const auto& selected : ui.listWidgetDefaultLayerVisibility->selectedItems())
        {
            if (debugDrawer)
            {
                debugDrawer->removeDefaultLayerVisibility(selected->text().toStdString());
            }
            ui.listWidgetDefaultLayerVisibility->takeItem(ui.listWidgetDefaultLayerVisibility->row(selected));
        }
    }

}
