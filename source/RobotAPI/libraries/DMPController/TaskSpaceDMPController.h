/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DMPController
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_LIB_RobotAPI_TaskSpaceDMPController_H
#define _ARMARX_LIB_RobotAPI_TaskSpaceDMPController_H


#include <dmp/representation/dmp/umitsmp.h>


#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/MathTools.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <RobotAPI/libraries/core/Trajectory.h>

namespace armarx
{

    struct PhaseStopParams
    {
        float goDist = 80;
        float backDist = 50;
        float maxValue = 100;
        float slop = 1000;
        float Kpos = 1;
        float Dpos = 2;
        float Kori = 1;
        float Dori = 0;
        float mm2radi = 10;
    };

    struct TaskSpaceDMPControllerConfig
    {
        int DMPKernelSize = 50;
        std::string DMPMode = "Linear";
        std::string DMPStyle = "Discrete";
        float DMPAmplitude = 1;
        float oriAmplitude = 1;
        float motionTimeDuration = 10;
        PhaseStopParams phaseStopParas;
    };

    struct DebugInfo
    {
        double canVal;
        double mpcFactor;
        double poseError;
        double posiError;
        double oriError;
    };

    /**
    * @defgroup Library-TaskSpaceDMPController TaskSpaceDMPController
    * @ingroup Library-RobotUnit-NJointControllers
    * A description of the library TaskSpaceDMPController.
    *
    * @class TaskSpaceDMPController
    * @ingroup Library-TaskSpaceDMPController
    * @brief Brief description of class TaskSpaceDMPController.
    *
    * Detailed description of class TaskSpaceDMPController.
    */
    class TaskSpaceDMPController
    {
    public:
        TaskSpaceDMPController(std::string name, const TaskSpaceDMPControllerConfig& config, bool isPhaseStopControl = true)
        {
            this->config = config;

            int ModeInd;
            if (config.DMPMode == "MinimumJerk")
            {
                ModeInd = 2;
            }
            else
            {
                ModeInd = 1;
            }


            dmpPtr.reset(new DMP::UMITSMP(config.DMPKernelSize, ModeInd));
            canVal = config.motionTimeDuration;

            targetPoseVec.resize(7);
            targetVel.resize(6);
            targetVel.setZero();
            currentState.resize(7);

            this->isPhaseStopControl = isPhaseStopControl;
            dmpName = name;
            this->paused = false;
            tau = 1;
        }

        std::string getName()
        {
            return dmpName;
        }


        void flow(double deltaT, const Eigen::Matrix4f& currentPose, const Eigen::VectorXf& twist);
        double flow(double canVal, double deltaT, const Eigen::Matrix4f& currentPose, const Eigen::VectorXf& twist);

        Eigen::VectorXf getTargetVelocity()
        {
            return targetVel;
        }

        std::vector<double> getTargetPose()
        {
            return targetPoseVec;
        }

        Eigen::Matrix4f getTargetPoseMat()
        {
            Eigen::Matrix4f res = VirtualRobot::MathTools::quat2eigen4f(targetPoseVec.at(4), targetPoseVec.at(5), targetPoseVec.at(6), targetPoseVec.at(3));
            res(0, 3) = targetPoseVec.at(0);
            res(1, 3) = targetPoseVec.at(1);
            res(2, 3) = targetPoseVec.at(2);

            return res;
        }

        Eigen::Matrix4f getIntegratedPoseMat()
        {
            Eigen::Matrix4f res = VirtualRobot::MathTools::quat2eigen4f(currentState.at(4).pos,
                                  currentState.at(5).pos,
                                  currentState.at(6).pos,
                                  currentState.at(3).pos);
            res(0, 3) = currentState.at(0).pos;
            res(1, 3) = currentState.at(1).pos;
            res(2, 3) = currentState.at(2).pos;

            return res;
        }

        void learnDMPFromFiles(const std::vector<std::string>& fileNames, const std::vector<double>& ratios);
        void learnDMPFromFiles(const std::vector<std::string>& fileNames);

        void learnDMPFromSampledTrajectory(const DMP::Vec<DMP::SampledTrajectoryV2 >& trajs);
        void learnDMPFromTrajectory(const TrajectoryPtr& traj);

        void setViaPose(double canVal, const Eigen::Matrix4f& viaPose);
        void setViaPose(double canVal, const std::vector<double>& viaPoseWithQuaternion);

        void removeAllViaPoints();
        void prepareExecution(const Eigen::Matrix4f& currentPose, const Eigen::Matrix4f& goalPose);
        void prepareExecution(const std::vector<double>& currentPoseVec, const std::vector<double>& goalPoseVec);

        void setSpeed(double times);
        void setAmplitude(double amp);

        void setGoalPose(const Eigen::Matrix4f& goalPose)
        {
            setViaPose(dmpPtr->getUMin(), goalPose);
        }

        void setGoalPoseVec(const std::vector<double> goalPoseVec)
        {
            setViaPose(dmpPtr->getUMin(), goalPoseVec);
        }

        void learnDMPFromSampledTrajectory(const DMP::Vec<DMP::SampledTrajectoryV2>& trajs, const std::vector<double>& ratios);
        void setRatios(const std::vector<double>& ratios);

        DebugInfo debugData;
        std::vector<double> eigen4f2vec(const Eigen::Matrix4f& pose);

        DMP::UMITSMPPtr getDMP()
        {
            return dmpPtr;
        }

        void pauseController()
        {
            this->paused = true;
        }
        void resumeController()
        {
            this->paused = false;
        }

        void setWeights(const std::vector<std::vector<double> >& weights)
        {
            dmpPtr->setWeights(weights);
        }

        void setTranslWeights(const std::vector<std::vector<double> >& weights)
        {
            ARMARX_CHECK_EQUAL(weights.size(), 3);

            for (size_t i = 0; i < 3; ++i)
            {
                dmpPtr->setWeights(i, weights[i]);
            }
        }

        void setRotWeights(const std::vector<std::vector<double> >& weights)
        {
            ARMARX_CHECK_EQUAL(weights.size(), 4);

            for (size_t i = 0; i < 4; ++i)
            {
                dmpPtr->setWeights(3 + i, weights[i]);
            }
        }

        DMP::DVec2d getWeights()
        {
            return dmpPtr->getWeights();
        }

        DMP::DVec2d getTranslWeights()
        {
            DMP::DVec2d res;
            DMP::DVec2d weights = getWeights();
            for (size_t i = 0; i < 3; ++i)
            {
                res.push_back(weights[i]);
            }
            return res;
        }

        DMP::DVec2d getRotWeights()
        {
            DMP::DVec2d res;
            DMP::DVec2d weights = getWeights();
            for (size_t i = 3; i < 7; ++i)
            {
                res.push_back(weights[i]);
            }
            return res;
        }

        double canVal;
        bool isPhaseStopControl;
        std::string dmpName;
        DMP::UMITSMPPtr dmpPtr;
        TaskSpaceDMPControllerConfig config;
    private:

        double tau;
        DMP::DVec goalPoseVec;

        Eigen::VectorXf targetVel;
        DMP::DVec targetPoseVec;

        DMP::Vec<DMP::DMPState > currentState;
        bool paused;


        bool isDisturbance;


        void getError(const Eigen::Matrix4f& pose, Eigen::Vector3f& position, Eigen::Quaterniond& quaternion, double& posiError, double& oriError);

        Eigen::Quaterniond oldDMPAngularVelocity;
    };

    using TaskSpaceDMPControllerPtr = std::shared_ptr<TaskSpaceDMPController>;

}

#endif
