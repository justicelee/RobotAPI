set(LIB_NAME aroncodegeneration)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")


set(LIBS
    aron
)

set(LIB_FILES
    AronCodeGenerator.cpp
)

set(LIB_HEADERS
    AronCodeGenerator.h
)

armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")
#add_dependencies("${LIB_NAME}" AronCodeGeneratorAppRun)
