/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// ArmarX
#include <RobotAPI/interface/aron.h>

using namespace std;

// TODO: REMOVE!!!
namespace armarx
{
    namespace aron
    {


        /*        // General Concepts
                template <typename CppType>
                concept IsAronPrimitiveCppType = false
                                         #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
                                         || (std::is_same<CppType, lowerType>::value)

                                                 HANDLE_PRIMITIVE_TYPES
        #undef RUN_ARON_MACRO
                                                 ;

                template <typename AronData, typename CppType>
                concept AreAronPrimitiveDependent = false
                                            #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
                                            || (std::is_same<AronData, Aron##upperType>::value && std::is_same<CppType, lowerType>::value)

                                                    HANDLE_PRIMITIVE_TYPES
        #undef RUN_ARON_MACRO
                                                    ;

                template <typename AronData>
                concept IsAronData = false
                             #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
                             || (std::is_same<AronData, Aron##upperType>::value)

                                     HANDLE_ALL_ARON_DATA
        #undef RUN_ARON_MACRO
                                     ;

                template <typename AronAbstractTypeType>
                concept IsAronAbstractType = false
                                     #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
                                     || (std::is_same<AronAbstractTypeType, Aron##upperType##Type>::value)

                                             HANDLE_ALL_ARON_TYPES
        #undef RUN_ARON_MACRO
                                             ;

                template <typename AronAbstractTypeType, typename AronData, typename CppType>
                concept AreAronAbstractTypeAndPrimitiveDependent = false
                #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
                || (std::is_same<AronAbstractTypeType, Aron##upperType##Type>::value && std::is_same<AronData, Aron##upperType>::value && std::is_same<CppType, lowerType>::value)

                        HANDLE_PRIMITIVE_TYPES
        #undef RUN_ARON_MACRO
                        ;



                // Compound
                template <typename Aron>
                concept isAron = IsAronData<Aron> || IsAronAbstractType<Aron>;





                // AronDataNavigator Concepts
                namespace data
                {
                    class AronDataNavigator;

        #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
            class Aron##upperType##DataNavigator;

                    HANDLE_CONTAINER_DATA
        #undef RUN_ARON_MACRO

                    template<typename AronDataType, typename CppType> requires AreAronPrimitiveDependent<AronDataType, CppType>
                    class AronPrimitiveDataNavigator;

                    template <typename AronDataNavigator>
                    concept IsAronDataNavigator = false
                                      #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
                                      || (std::is_same<AronDataNavigator, Aron##upperType##DataNavigator>::value)

                                                  HANDLE_CONTAINER_DATA
        #undef RUN_ARON_MACRO

                                      #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
                                      || (std::is_same<AronDataNavigator, AronPrimitiveDataNavigator<Aron##upperType, lowerType>>::value)

                                                  HANDLE_PRIMITIVE_TYPES
        #undef RUN_ARON_MACRO
                                                  ;
                }





                // AronAbstractTypeNavigator Concepts
                namespace types
                {
                    class AronAbstractTypeNavigator;

        #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
            class Aron##upperType##TypeNavigator;

                    HANDLE_CONTAINER_TYPES
        #undef RUN_ARON_MACRO

                    template<typename AronAbstractTypeType, typename AronDataType, typename CppType> requires AreAronAbstractTypeAndPrimitiveDependent<AronAbstractTypeType, AronDataType, CppType>
                    class AronPrimitiveTypeNavigator;

                    template <typename AronAbstractTypeNavigator>
                    concept IsAronAbstractTypeNavigator = false
                                              #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
                                              || (std::is_same<AronAbstractTypeNavigator, Aron##upperType##TypeNavigator>::value)

                                                          HANDLE_CONTAINER_TYPES
        #undef RUN_ARON_MACRO

                                              #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
                                              || (std::is_same<AronAbstractTypeNavigator, AronPrimitiveTypeNavigator<Aron##upperType##Type, Aron##upperType, lowerType>>::value)

                                                          HANDLE_PRIMITIVE_TYPES
        #undef RUN_ARON_MACRO
                                                          ;
                }*/
    }
}
