/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <unordered_map>

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronException.h>

#include <RobotAPI/libraries/aron/aroncore/AronConcepts.h>
#include <RobotAPI/libraries/aron/aroncore/AronDescriptor.h>
#include <RobotAPI/libraries/aron/aroncore/AronResolver.h>
#include <RobotAPI/libraries/aron/aroncore/AronPath.h>

namespace armarx
{
    namespace aron
    {
        namespace navigator
        {
            template <typename Descriptor, typename Aron> //requires isAron<Aron>&& isTypeDescriptor<Descriptor>
            class AronNavigator;

            template <typename Descriptor, typename Aron> //requires isAron<Aron>&& isTypeDescriptor<Descriptor>
            using AronNavigatorPtr = std::shared_ptr<AronNavigator<Descriptor, Aron>>;

            template <typename Descriptor, typename Aron> //requires isAron<Aron>&& isTypeDescriptor<Descriptor>
            class AronNavigator
            {
            public:
                // constructors
                AronNavigator() = delete;
                AronNavigator(const Descriptor& descriptor, const AronPath& path) :
                    descriptor(descriptor),
                    path(path)
                {
                }

                // public member functions
                Descriptor getDescriptor() const
                {
                    return descriptor;
                }

                AronPath getPath() const
                {
                    return path;
                }

                std::string pathToString() const
                {
                    return path.toString();
                }

                // virual definitions
                virtual typename Aron::PointerType getResult() const = 0;

            protected:
                static void CheckAronPtrForNull(const typename Aron::PointerType& data, const std::string& messageIfIsNull)
                {
                    if (data.get() == nullptr)
                    {
                        throw exception::AronPtrIsNullException("AronNavigator", "checkAronPtrForNull", "Could not cast an AronPtr. The Ptr was NULL. " + messageIfIsNull);
                    }
                }

            private:
                // members
                const Descriptor descriptor;
                const AronPath path;
            };
        }
    }
}
