/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AronPrimitiveDataNavigator.h"

#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigatorFactory.h>

namespace armarx
{
namespace aron
{
namespace datanavigator
{
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    /* constructors */ \
    Aron##upperType##DataNavigator::Aron##upperType##DataNavigator(const AronPath& path) : \
    AronNavigator<AronDataDescriptor, data::AronData>::AronNavigator(AronDataDescriptor::eAron##upperType, path), \
    AronDataNavigator(AronDataDescriptor::eAron##upperType, path), \
    aron(new data::Aron##upperType()) \
{ \
    aron->value = {}; \
} \
    Aron##upperType##DataNavigator::Aron##upperType##DataNavigator(const data::Aron##upperType##Ptr& o, const AronPath& path) : \
    AronNavigator<AronDataDescriptor, data::AronData>::AronNavigator(AronDataDescriptor::eAron##upperType, path), \
    AronDataNavigator(AronDataDescriptor::eAron##upperType, path), \
    aron(o) \
{ \
} \
\
    /* static methods */ \
    Aron##upperType##DataNavigatorPtr Aron##upperType##DataNavigator::DynamicCast(const AronDataNavigatorPtr& n) \
{ \
    Aron##upperType##DataNavigatorPtr casted = std::dynamic_pointer_cast<Aron##upperType##DataNavigator>(n); \
    CheckDataNavigatorPtrForNull(casted, "The path was: " + n->pathToString()); \
    return casted; \
} \
\
    /* public member functions */ \
    void Aron##upperType##DataNavigator::setValue(const lowerType& x) \
{ \
    aron->value = x; \
} \
    \
    lowerType Aron##upperType##DataNavigator::getValue() const \
{ \
    return aron->value; \
} \
    \
    /* virtual implementations */ \
    data::AronDataPtr Aron##upperType##DataNavigator::getResult() const \
{ \
    return aron; \
} \
\
    type::AronTypePtr Aron##upperType##DataNavigator::recalculateType() const \
{ \
    return nullptr; \
} \
\
    std::vector<AronDataNavigatorPtr> Aron##upperType##DataNavigator::getChildren() const \
{ \
    return {}; \
} \
\
    size_t Aron##upperType##DataNavigator::childrenSize() const \
{ \
    return 0; \
} \

HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
}
}
}
