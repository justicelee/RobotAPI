/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "AronDataNavigator.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronException.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigatorFactory.h>

namespace armarx
{
namespace aron
{
namespace datanavigator
{
// static data members
const AronDataNavigatorFactoryPtr AronDataNavigator::FACTORY = AronDataNavigatorFactoryPtr(new AronDataNavigatorFactory());

// constructors
AronDataNavigator::AronDataNavigator(const AronDataDescriptor& descriptor, const AronPath& path) :
    navigator::AronNavigator<AronDataDescriptor, data::AronData>::AronNavigator(descriptor, path)
{
}

// static methods
AronDataNavigatorPtr AronDataNavigator::FromAronData(const data::AronDataPtr& a, const AronPath& path)
{
    return FACTORY->create(a, path);
}

void AronDataNavigator::CheckDataNavigatorPtrForNull(const AronDataNavigatorPtr& data, const std::string& messageIfIsNull)
{
    if (data.get() == nullptr)
    {
        throw exception::AronDataPtrIsNullException("AronDataNavigator", "CheckDataNavigatorPtrForNull", "Could not cast an AronDataNavigatorPtr. The Ptr was NULL. " + messageIfIsNull);
    }
}
}
}
}
