/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigator.h>

namespace armarx
{
namespace aron
{
namespace datanavigator
{

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    class Aron##upperType##DataNavigator; \
    typedef std::shared_ptr<Aron##upperType##DataNavigator> Aron##upperType##DataNavigatorPtr; \
    \
    class Aron##upperType##DataNavigator : \
    virtual public AronDataNavigator \
{ \
    public: \
    using PointerType = Aron##upperType##DataNavigatorPtr; \
    \
    /* constructors */ \
    Aron##upperType##DataNavigator() = delete; \
    Aron##upperType##DataNavigator(const AronPath&); \
    Aron##upperType##DataNavigator(const data::Aron##upperType##Ptr&, const AronPath&); \
    \
    /* static methods */ \
    static Aron##upperType##DataNavigatorPtr DynamicCast(const AronDataNavigatorPtr& n); \
    \
    /* public member functions */ \
    void setValue(const lowerType& x); \
    lowerType getValue() const; \
    \
    /* virtual implementations */ \
    virtual data::AronDataPtr getResult() const override; \
    virtual std::vector<AronDataNavigatorPtr> getChildren() const override; \
    virtual size_t childrenSize() const override; \
    \
    virtual type::AronTypePtr recalculateType() const override; \
    \
    private: \
    /* members */ \
    data::Aron##upperType##Ptr aron; \
};

HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
}
}
}
