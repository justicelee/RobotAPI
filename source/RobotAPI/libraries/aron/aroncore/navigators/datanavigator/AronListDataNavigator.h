/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronListTypeNavigator.h>

namespace armarx
{
    namespace aron
    {
        namespace datanavigator
        {
            class AronListDataNavigator;
            typedef std::shared_ptr<AronListDataNavigator> AronListDataNavigatorPtr;

            class AronListDataNavigator :
                virtual public AronDataNavigator
            {
            public:
                using PointerType = AronListDataNavigatorPtr;

            public:
                // constructors
                AronListDataNavigator() = delete;
                AronListDataNavigator(const AronPath& path);
                AronListDataNavigator(const data::AronListPtr&, const AronPath& path);

                // static methods
                static AronListDataNavigatorPtr DynamicCast(const AronDataNavigatorPtr& n);

                // public member functions
                void addElement(const AronDataNavigatorPtr&);
                AronDataNavigatorPtr getElement(unsigned int) const;

                // virtual implementations
                virtual data::AronDataPtr getResult() const override;
                virtual std::vector<AronDataNavigatorPtr> getChildren() const override;
                virtual size_t childrenSize() const override;

                virtual type::AronTypePtr recalculateType() const override;

            private:
                std::vector<AronDataNavigatorPtr> childrenNavigators;
                data::AronListPtr aron;
            };
        }
    }
}
