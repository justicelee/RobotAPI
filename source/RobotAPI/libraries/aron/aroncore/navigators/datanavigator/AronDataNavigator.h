/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <unordered_map>

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/AronNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/AronPath.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTypeNavigator.h>

namespace armarx
{
namespace aron
{
namespace datanavigator
{
class AronDataNavigatorFactory;
typedef std::shared_ptr<AronDataNavigatorFactory> AronDataNavigatorFactoryPtr;

class AronDataNavigator;
typedef std::shared_ptr<AronDataNavigator> AronDataNavigatorPtr;

class AronDataNavigator :
        virtual public navigator::AronNavigator<AronDataDescriptor, data::AronData>
{
public:
    using PointerType = AronDataNavigatorPtr;

public:
    // constructors
    AronDataNavigator(const AronDataDescriptor&, const AronPath&);

    // static methods
    static AronDataNavigatorPtr FromAronData(const data::AronDataPtr&, const AronPath& = AronPath());
    //static AronDataNavigatorPtr FromAronDataWithTypeInfo(const data::AronDataPtr&, const type::AronTypePtr&, const AronPath& = AronPath());
    //static AronDataNavigatorPtr FromAronDataWithTypeInfo(const data::AronDataPtr&, const typenavigator::AronTypeNavigatorPtr&, const AronPath& = AronPath());

    template<typename AronNavigatorType>
    static typename AronNavigatorType::PointerType DynamicCast(const AronDataNavigatorPtr& n)
    {
        return AronNavigatorType::DynamicCast(n);
    }

    // virtual definitions of base class
    virtual data::AronDataPtr getResult() const override = 0;

    // virtual definitions
    virtual std::vector<AronDataNavigatorPtr> getChildren() const = 0;
    virtual size_t childrenSize() const = 0;

    //virtual bool fullfillsType(const type::AronTypePtr&) const = 0;
    virtual type::AronTypePtr recalculateType() const = 0;

protected:
    static void CheckDataNavigatorPtrForNull(const AronDataNavigatorPtr&, const std::string&);

private:
    static const AronDataNavigatorFactoryPtr FACTORY;
};
}
}
}
