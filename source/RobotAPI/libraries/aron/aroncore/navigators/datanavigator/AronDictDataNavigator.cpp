/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "AronDictDataNavigator.h"

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigatorFactory.h>

namespace armarx
{
namespace aron
{
namespace datanavigator
{

// constructors
AronDictDataNavigator::AronDictDataNavigator(const data::AronDictPtr& o, const AronPath& path) :
    AronNavigator<AronDataDescriptor, data::AronData>::AronNavigator(AronDataDescriptor::eAronDict, path),
    AronDataNavigator(AronDataDescriptor::eAronDict, path),
    aron(o)
{
    CheckAronPtrForNull(aron, pathToString());

    for (const auto& [key, dataPtr] : aron->elements)
    {
        childrenNavigators[key] = AronDataNavigator::FromAronData(dataPtr, AronPath(path, "[" + key + "]"));
    }
}

AronDictDataNavigator::AronDictDataNavigator(const AronPath& path) :
    AronNavigator<AronDataDescriptor, data::AronData>::AronNavigator(AronDataDescriptor::eAronDict, path),
    AronDataNavigator(AronDataDescriptor::eAronDict, path),
    aron(new data::AronDict())
{
}

// static methods
AronDictDataNavigatorPtr AronDictDataNavigator::DynamicCast(const AronDataNavigatorPtr& n)
{
    AronDictDataNavigatorPtr casted = std::dynamic_pointer_cast<AronDictDataNavigator>(n);
    AronDataNavigator::CheckDataNavigatorPtrForNull(casted, "");
    return casted;
}

// public member functions
std::vector<std::string> AronDictDataNavigator::getAllKeys() const
{
    std::vector<std::string> ret;
    for (const auto& [key, _] : childrenNavigators)
    {
        ret.push_back(key);
    }
    return ret;
}

void AronDictDataNavigator::addElement(const std::string& key, const AronDataNavigatorPtr& data)
{
    this->childrenNavigators[key] = data;
    this->aron->elements[key] = data->getResult();
}

AronDataNavigatorPtr AronDictDataNavigator::getElement(const std::string& key) const
{
    auto it = childrenNavigators.find(key);
    if (it == childrenNavigators.end())
    {
        std::string all_keys = "";
        for (const auto& child : this->getAllKeys())
        {
            all_keys += child + ", ";
        }
        throw LocalException("AronDictDataNavigator: Could not find key '" + key + "'. Found keys: " + all_keys + ". The path was: " + pathToString());
    }
    return it->second;
}

// virtual implementations
data::AronDataPtr AronDictDataNavigator::getResult() const
{
    return aron;
}

type::AronTypePtr AronDictDataNavigator::recalculateType() const
{
    return nullptr;
}

std::vector<AronDataNavigatorPtr> AronDictDataNavigator::getChildren() const
{
    std::vector<AronDataNavigatorPtr> ret(childrenNavigators.size());
    for (const auto& [key, nav] : childrenNavigators)
    {
        ret.push_back(nav);
    }
    return ret;
}

size_t AronDictDataNavigator::childrenSize() const
{
    return childrenNavigators.size();
}
}
}
}
