/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <unordered_map>

// BaseClass
#include <RobotAPI/libraries/aron/aroncore/navigators/AronNavigatorFactory.h>

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronAllDataNavigators.h>
#include <RobotAPI/libraries/aron/aroncore/AronDescriptor.h>

namespace armarx
{
    namespace aron
    {
        namespace datanavigator
        {
            class AronDataNavigatorFactory;
            typedef std::shared_ptr<AronDataNavigatorFactory> AronDataNavigatorFactoryPtr;

            class AronDataNavigatorFactory :
                virtual public navigator::AronNavigatorFactory<data::AronDataPtr, AronDataNavigatorPtr>
            {
            public:
                AronDataNavigatorFactory() = default;
                virtual AronDataNavigatorPtr create(const data::AronDataPtr&, const AronPath&) const override;
                virtual AronDataNavigatorPtr createSpecific(const data::AronDataPtr&, const AronPath&) const override;

            private:
                static const std::map<AronDataDescriptor, AronDataNavigatorFactoryPtr> FACTORIES;
            };

            // Factories
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    class Aron##upperType##DataNavigatorFactory : \
        virtual public AronDataNavigatorFactory \
    { \
    public: \
        Aron##upperType##DataNavigatorFactory() = default; \
        virtual AronDataNavigatorPtr createSpecific(const data::AronDataPtr&, const AronPath&) const override; \
    };

            HANDLE_CONTAINER_DATA
            HANDLE_COMPLEX_DATA
            HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
        }
    }
}
