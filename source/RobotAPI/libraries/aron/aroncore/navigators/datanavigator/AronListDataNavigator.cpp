/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "AronListDataNavigator.h"

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigatorFactory.h>


namespace armarx
{
    namespace aron
    {
        namespace datanavigator
        {
            // constructors
            AronListDataNavigator::AronListDataNavigator(const data::AronListPtr& l, const AronPath& path) :
                AronNavigator<AronDataDescriptor, data::AronData>::AronNavigator(AronDataDescriptor::eAronList, path),
                AronDataNavigator(AronDataDescriptor::eAronList, path),
                aron(l)
            {
                CheckAronPtrForNull(aron, pathToString());

                unsigned int i = 0;
                for (const auto& dataPtr : l->elements)
                {
                    childrenNavigators.push_back(FromAronData(dataPtr, AronPath(path, "[" + std::to_string(i++) + "]")));
                }
            }

            AronListDataNavigator::AronListDataNavigator(const AronPath& path) :
                AronNavigator<AronDataDescriptor, data::AronData>::AronNavigator(AronDataDescriptor::eAronList, path),
                AronDataNavigator(AronDataDescriptor::eAronList, path),
                aron(new data::AronList())
            {
            }

            // static methods
            AronListDataNavigatorPtr AronListDataNavigator::DynamicCast(const AronDataNavigatorPtr& n)
            {
                AronListDataNavigatorPtr casted = std::dynamic_pointer_cast<AronListDataNavigator>(n);
                CheckDataNavigatorPtrForNull(casted, "");
                return casted;
            }

            // public member functions
            void AronListDataNavigator::addElement(const AronDataNavigatorPtr& n)
            {
                childrenNavigators.push_back(n);
                aron->elements.push_back(n->getResult());
            }

            AronDataNavigatorPtr AronListDataNavigator::getElement(unsigned int i) const
            {
                if (i >= childrenNavigators.size())
                {
                    throw LocalException("AronListDataNavigator: Index (" + std::to_string(i) + ") out of bounds. The path is " + pathToString());
                }
                return childrenNavigators[i];
            }

            // virtual implementations
            data::AronDataPtr AronListDataNavigator::getResult() const
            {
                return aron;
            }

            type::AronTypePtr AronListDataNavigator::recalculateType() const
            {
                return nullptr;
            }


            std::vector<AronDataNavigatorPtr> AronListDataNavigator::getChildren() const
            {
                return childrenNavigators;
            }

            size_t AronListDataNavigator::childrenSize() const
            {
                return childrenNavigators.size();
            }
        }
    }
}
