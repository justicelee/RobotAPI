/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "AronDataNavigator.h"

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/libraries/aron/AronDataNavigatorFactory.h>

namespace armarx
{
    namespace aron
    {
        AronDataNavigator::AronDataNavigator(const AronDataPtr& data, const AronNavigatorPath& path) :
            path(path),
            data(data)
        {

        }

        void AronDataNavigator::checkAronDataPtr(const AronDataPtr& data, const std::string& message) const
        {
            if (data.get() == nullptr)
            {
                throw LocalException(message + " - The path was: " + pathToString());
            }
        }

        AronNavigatorPath AronDataNavigator::getPath() const
        {
            return path;
        }

        std::string AronDataNavigator::pathToString() const
        {
            return path.toString();
        }

        AronDataPtr AronDataNavigator::getDataPtr() const
        {
            return data;
        }
    }
}
