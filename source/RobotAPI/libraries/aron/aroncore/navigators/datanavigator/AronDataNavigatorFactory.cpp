/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL

// Header
#include "AronDataNavigatorFactory.h"

namespace armarx
{
    namespace aron
    {
        namespace datanavigator
        {

            // Map types to factories
            const std::map<AronDataDescriptor, AronDataNavigatorFactoryPtr> AronDataNavigatorFactory::FACTORIES =
            {
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    {AronResolver::GetTypeDescriptorForAronDataId(typeid(data::Aron##upperType)), AronDataNavigatorFactoryPtr(new Aron##upperType##DataNavigatorFactory())}, \

                HANDLE_CONTAINER_DATA
                HANDLE_COMPLEX_DATA
                HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
            };

            // Access method
            AronDataNavigatorPtr AronDataNavigatorFactory::create(const data::AronDataPtr& aron, const AronPath& path) const
            {
                if (!aron)
                {
                    throw LocalException("AronDataNavigatorFactory: An AronDataPtr is NULL! Cannot create navigator!");
                }

                auto factory_iterator = FACTORIES.find(AronResolver::GetTypeDescriptorForAronData(aron));
                if (factory_iterator == FACTORIES.end())
                {
                    throw LocalException("AronDataNavigatorFactory: Cannot find the desired factory. Cannot create navigator!");
                }
                return factory_iterator->second->createSpecific(aron, path);
            }

            AronDataNavigatorPtr AronDataNavigatorFactory::createSpecific(const data::AronDataPtr&, const AronPath&) const
            {
                throw LocalException("AronDataNavigatorFactory: Called disallowed method of an AronDataNavigatorFactory. Use child class instead!");
            }

            // Factories
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    AronDataNavigatorPtr Aron##upperType##DataNavigatorFactory::createSpecific(const data::AronDataPtr& aron, const AronPath& path) const \
    { \
        data::Aron##upperType##Ptr aronCasted = data::Aron##upperType##Ptr::dynamicCast(aron); \
        return AronDataNavigatorPtr(new Aron##upperType##DataNavigator(aronCasted, path)); \
    }

            HANDLE_CONTAINER_DATA
            HANDLE_COMPLEX_DATA
            HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

        }
    }
}
