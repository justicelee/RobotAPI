/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <map>

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigator.h>

namespace armarx
{
    namespace aron
    {
        namespace datanavigator
        {
            class AronNDArrayDataNavigator;
            typedef std::shared_ptr<AronNDArrayDataNavigator> AronNDArrayDataNavigatorPtr;

            class AronNDArrayDataNavigator :
                virtual public AronDataNavigator
            {
            public:
                using PointerType = AronNDArrayDataNavigatorPtr;

            public:
                // constructors
                AronNDArrayDataNavigator() = delete;
                AronNDArrayDataNavigator(const AronPath& path);
                AronNDArrayDataNavigator(const data::AronNDArrayPtr&, const AronPath& path);

                // static methods
                static AronNDArrayDataNavigatorPtr DynamicCast(const AronDataNavigatorPtr& n);

                // public member functions
                unsigned char* getData() const;
                void setData(unsigned int, const unsigned char*);

                // virtual implementations
                virtual data::AronDataPtr getResult() const override;
                virtual std::vector<AronDataNavigatorPtr> getChildren() const override;
                virtual size_t childrenSize() const override;

                virtual type::AronTypePtr recalculateType() const override;

            private:
                data::AronNDArrayPtr aron;
            };
        }
    }
}
