/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "AronNDArrayDataNavigator.h"

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigatorFactory.h>



namespace armarx
{
    namespace aron
    {
        namespace datanavigator
        {
            // constructors
            AronNDArrayDataNavigator::AronNDArrayDataNavigator(const data::AronNDArrayPtr& o, const AronPath& path) :
                AronNavigator<AronDataDescriptor, data::AronData>::AronNavigator(AronDataDescriptor::eAronNDArray, path),
                AronDataNavigator(AronDataDescriptor::eAronNDArray, path),
                aron(o)
            {

            }

            AronNDArrayDataNavigator::AronNDArrayDataNavigator(const AronPath& path) :
                AronNavigator<AronDataDescriptor, data::AronData>::AronNavigator(AronDataDescriptor::eAronNDArray, path),
                AronDataNavigator(AronDataDescriptor::eAronNDArray, path),
                aron(new data::AronNDArray())
            {

            }

            // static methods
            AronNDArrayDataNavigatorPtr AronNDArrayDataNavigator::DynamicCast(const AronDataNavigatorPtr& n)
            {
                AronNDArrayDataNavigatorPtr casted = std::dynamic_pointer_cast<AronNDArrayDataNavigator>(n);
                CheckDataNavigatorPtrForNull(casted, "");
                return casted;
            }

            // public member functions
            unsigned char* AronNDArrayDataNavigator::getData() const
            {
                return aron->data.data();
            }

            void AronNDArrayDataNavigator::setData(unsigned int elements, const unsigned char* src)
            {
                aron->data = std::vector<unsigned char>(elements);
                memcpy(aron->data.data(), src, elements);
            }

            // virtual implementations
            data::AronDataPtr AronNDArrayDataNavigator::getResult() const
            {
                return aron;
            }

            type::AronTypePtr AronNDArrayDataNavigator::recalculateType() const
            {
                return nullptr;
            }


            std::vector<AronDataNavigatorPtr> AronNDArrayDataNavigator::getChildren() const
            {
                return {};
            }

            size_t AronNDArrayDataNavigator::childrenSize() const
            {
                return 0;
            }
        }
    }
}
