/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronPCLPointcloudTypeNavigator.h"

namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {
            // constructors
            AronPCLPointcloudTypeNavigator::AronPCLPointcloudTypeNavigator(const AronPath& path) :
                navigator::AronNavigator<AronTypeDescriptor, type::AronType>::AronNavigator(AronTypeDescriptor::eAronPCLPointcloudType, path),
                AronTypeNavigator(AronTypeDescriptor::eAronPCLPointcloudType, path),
                AronNDArraySerializerTypeNavigator(AronTypeDescriptor::eAronEigenMatrixType, path),
                type(new type::AronPCLPointcloudType({0, 0}, ""))
            {
            }

            std::vector<int> AronPCLPointcloudTypeNavigator::getDimensions() const
            {
                return type->dimensions;
            }

            void AronPCLPointcloudTypeNavigator::setDimensions(const std::vector<int>& d)
            {
                if (d.size() > 2 || d.empty())
                {
                    throw armarx::LocalException("Wrong number of elements in dimension array");
                }

                type->dimensions = d;
            }

            unsigned int AronPCLPointcloudTypeNavigator::getWidth() const
            {
                return type->dimensions[0];
            }

            unsigned int AronPCLPointcloudTypeNavigator::getHeight() const
            {
                return type->dimensions[1];
            }

            std::string AronPCLPointcloudTypeNavigator::getUsedType() const
            {
                return type->typeName;
            }

            void AronPCLPointcloudTypeNavigator::setWidth(const unsigned int& w)
            {
                type->dimensions[0] = w;
            }

            void AronPCLPointcloudTypeNavigator::setHeight(const unsigned int& h)
            {
                type->dimensions[1] = h;
            }

            void AronPCLPointcloudTypeNavigator::setUsedType(const std::string& u)
            {
                type->typeName = u;
            }

            // static methods
            AronPCLPointcloudTypeNavigatorPtr AronPCLPointcloudTypeNavigator::DynamicCast(const AronTypeNavigatorPtr& n)
            {
                AronPCLPointcloudTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronPCLPointcloudTypeNavigator>(n);
                CheckTypeNavigatorPtrForNull(casted, "");
                return casted;
            }

            // virtual implementations
            type::AronTypePtr AronPCLPointcloudTypeNavigator::getResult() const
            {
                if(type->dimensions.size() != 2 || std::any_of(type->dimensions.begin(), type->dimensions.end(), [](int i) { return i<0; }) || type->typeName.empty())
                {
                    throw armarx::LocalException("AronPCLPointcloudTypeNavigator: Wrong parameters set!");
                }
                return type;
            }
        }
    }
}

