/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronObjectTypeNavigator.h"


namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {

            // constructors
            AronObjectTypeNavigator::AronObjectTypeNavigator(const AronPath& path) :
                navigator::AronNavigator<AronTypeDescriptor, type::AronType>::AronNavigator(AronTypeDescriptor::eAronObjectType, path),
                AronTypeNavigator(AronTypeDescriptor::eAronObjectType, path),
                AronDictSerializerTypeNavigator(AronTypeDescriptor::eAronObjectType, path),
                type((new type::AronObjectType()))
            {
            }

            // static methods
            AronObjectTypeNavigatorPtr AronObjectTypeNavigator::DynamicCast(const AronTypeNavigatorPtr& n)
            {
                AronObjectTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronObjectTypeNavigator>(n);
                CheckTypeNavigatorPtrForNull(casted, "The path was: " + n->pathToString());
                return casted;
            }

            // public member functions
            void AronObjectTypeNavigator::setAcceptedType(const AronTypeNavigatorPtr &)
            {
                throw armarx::LocalException("Called invalid function!");
            }

            AronTypeNavigatorPtr AronObjectTypeNavigator::getAcceptedType() const
            {
                throw armarx::LocalException("Called invalid function!");
            }

            void AronObjectTypeNavigator::addAcceptedType(const std::string& k, const AronTypeNavigatorPtr& v)
            {
                type->elementTypes[k] = v->getResult();
                memberNavigators[k] = v;
            }

            void AronObjectTypeNavigator::setName(const std::string& n)
            {
               //path.setRootIdentifier(n);
               type->objectName = n;
            }

            void AronObjectTypeNavigator::setExtends(const AronObjectTypeNavigatorPtr& p)
            {
                type->parent = type::AronObjectTypePtr::dynamicCast(p->getResult());
                extends = p;
            }

            std::string AronObjectTypeNavigator::getName() const
            {
                return type->objectName;
            }

            std::map<std::string, AronTypeNavigatorPtr> AronObjectTypeNavigator::getAcceptedTypes() const
            {
                return memberNavigators;
            }

            AronObjectTypeNavigatorPtr AronObjectTypeNavigator::getExtends() const
            {
                return extends;
            }

            // virtual implementations
            type::AronTypePtr AronObjectTypeNavigator::getResult() const
            {
                if(type->objectName.empty())
                {
                    throw armarx::LocalException("AronObjectTypeNavigator: Name not set!");
                }

                return type;
            }
        }
    }
}

