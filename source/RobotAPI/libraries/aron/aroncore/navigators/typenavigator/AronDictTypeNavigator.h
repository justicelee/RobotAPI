/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base Class
#include "AronTypeNavigator.h"


namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {
            class AronDictTypeNavigator;
            typedef std::shared_ptr<AronDictTypeNavigator> AronDictTypeNavigatorPtr;

            class AronDictTypeNavigator :
                virtual public AronDictSerializerTypeNavigator
            {
            public:
                using PointerType = AronDictTypeNavigatorPtr;

            public:
                // constructors
                AronDictTypeNavigator() = delete;
                AronDictTypeNavigator(const AronPath& path);

                // static methods
                static AronDictTypeNavigatorPtr DynamicCast(const AronTypeNavigatorPtr& n);

                // virtual implementations
                virtual void setAcceptedType(const AronTypeNavigatorPtr&) override;
                virtual AronTypeNavigatorPtr getAcceptedType() const override;

                virtual void addAcceptedType(const std::string &, const AronTypeNavigatorPtr &) override;
                virtual std::map<std::string, AronTypeNavigatorPtr> getAcceptedTypes() const override;

                virtual type::AronTypePtr getResult() const override;

            private:
                // members
                AronTypeNavigatorPtr acceptedTypeNavigator;
                type::AronDictTypePtr type;

            };
        }
    }
}
