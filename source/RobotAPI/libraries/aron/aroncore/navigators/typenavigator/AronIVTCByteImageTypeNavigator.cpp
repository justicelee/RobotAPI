/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronIVTCByteImageTypeNavigator.h"

namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {
            // constructors
            AronIVTCByteImageTypeNavigator::AronIVTCByteImageTypeNavigator(const AronPath& path) :
                navigator::AronNavigator<AronTypeDescriptor, type::AronType>::AronNavigator(AronTypeDescriptor::eAronIVTCByteImageType, path),
                AronTypeNavigator(AronTypeDescriptor::eAronIVTCByteImageType, path),
                AronNDArraySerializerTypeNavigator(AronTypeDescriptor::eAronEigenMatrixType, path),
                type(new type::AronIVTCByteImageType({0, 0}, ""))
            {
            }

            std::vector<int> AronIVTCByteImageTypeNavigator::getDimensions() const
            {
                return type->dimensions;
            }

            void AronIVTCByteImageTypeNavigator::setDimensions(const std::vector<int>& d)
            {
                if (d.size() > 2 || d.empty())
                {
                    throw armarx::LocalException("Wring number of elements in dimension array");
                }

                type->dimensions = d;
            }

            unsigned int AronIVTCByteImageTypeNavigator::getWidth() const
            {
                return type->dimensions[0];
            }

            unsigned int AronIVTCByteImageTypeNavigator::getHeight() const
            {
                return type->dimensions[1];
            }

            std::string AronIVTCByteImageTypeNavigator::getUsedType() const
            {
                return type->typeName;
            }

            void AronIVTCByteImageTypeNavigator::setWidth(const unsigned int& w)
            {
                type->dimensions[0] = w;
            }

            void AronIVTCByteImageTypeNavigator::setHeight(const unsigned int& h)
            {
                type->dimensions[1] = h;
            }

            void AronIVTCByteImageTypeNavigator::setUsedType(const std::string& u)
            {
                type->typeName = u;
            }

            // static methods
            AronIVTCByteImageTypeNavigatorPtr AronIVTCByteImageTypeNavigator::DynamicCast(const AronTypeNavigatorPtr& n)
            {
                AronIVTCByteImageTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronIVTCByteImageTypeNavigator>(n);
                CheckTypeNavigatorPtrForNull(casted, "The path was: " + n->pathToString());
                return casted;
            }

            // virtual implementations
            type::AronTypePtr AronIVTCByteImageTypeNavigator::getResult() const
            {
                if(type->dimensions.size() != 2 || std::any_of(type->dimensions.begin(), type->dimensions.end(), [](int i) { return i<0; }) || type->typeName.empty())
                {
                    throw armarx::LocalException("AronIVTCByteImageTypeNavigator: Wrong parameters set!");
                }
                return type;
            }
        }
    }
}

