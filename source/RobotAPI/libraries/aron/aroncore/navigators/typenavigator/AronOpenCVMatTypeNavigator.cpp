/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronOpenCVMatTypeNavigator.h"

namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {
            // constructors
            AronOpenCVMatTypeNavigator::AronOpenCVMatTypeNavigator(const AronPath& path) :
                navigator::AronNavigator<AronTypeDescriptor, type::AronType>::AronNavigator(AronTypeDescriptor::eAronOpenCVMatType, path),
                AronTypeNavigator(AronTypeDescriptor::eAronOpenCVMatType, path),
                AronNDArraySerializerTypeNavigator(AronTypeDescriptor::eAronEigenMatrixType, path),
                type(new type::AronOpenCVMatType({}, ""))
            {
            }

            std::string AronOpenCVMatTypeNavigator::getUsedType() const
            {
                return type->typeName;
            }

            std::vector<int> AronOpenCVMatTypeNavigator::getDimensions() const
            {
                return type->dimensions;
            }

            void AronOpenCVMatTypeNavigator::setUsedType(const std::string& u)
            {
                type->typeName = u;
            }

            void AronOpenCVMatTypeNavigator::setDimensions(const std::vector<int>& d)
            {
                type->dimensions = d;
            }


            // static methods
            AronOpenCVMatTypeNavigatorPtr AronOpenCVMatTypeNavigator::DynamicCast(const AronTypeNavigatorPtr& n)
            {
                AronOpenCVMatTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronOpenCVMatTypeNavigator>(n);
                CheckTypeNavigatorPtrForNull(casted, "");
                return casted;
            }

            // virtual implementations
            type::AronTypePtr AronOpenCVMatTypeNavigator::getResult() const
            {
                if(type->typeName.empty() || type->dimensions.empty())
                {
                    throw armarx::LocalException("AronOpenCVMatTypeNavigator: Wrong parameters!");
                }
                return type;
            }
        }
    }
}

