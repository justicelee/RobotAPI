/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronEigenMatrixTypeNavigator.h"

namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {
            // constructors
            AronEigenMatrixTypeNavigator::AronEigenMatrixTypeNavigator(const AronPath& path) :
                navigator::AronNavigator<AronTypeDescriptor, type::AronType>::AronNavigator(AronTypeDescriptor::eAronEigenMatrixType, path),
                AronTypeNavigator(AronTypeDescriptor::eAronEigenMatrixType, path),
                AronNDArraySerializerTypeNavigator(AronTypeDescriptor::eAronEigenMatrixType, path),
                type(new type::AronEigenMatrixType({}, ""))
            {
            }

            std::string AronEigenMatrixTypeNavigator::getUsedType() const
            {
                return type->typeName;
            }

            std::vector<int> AronEigenMatrixTypeNavigator::getDimensions() const
            {
                return type->dimensions;
            }

            void AronEigenMatrixTypeNavigator::setUsedType(const std::string& u)
            {
                type->typeName = u;
            }

            void AronEigenMatrixTypeNavigator::setDimensions(const std::vector<int>& d)
            {
                type->dimensions = d;
            }

            // static methods
            AronEigenMatrixTypeNavigatorPtr AronEigenMatrixTypeNavigator::DynamicCast(const AronTypeNavigatorPtr& n)
            {
                AronEigenMatrixTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronEigenMatrixTypeNavigator>(n);
                CheckTypeNavigatorPtrForNull(casted, "The path was: " + n->pathToString());
                return casted;
            }

            // virtual implementations
            type::AronTypePtr AronEigenMatrixTypeNavigator::getResult() const
            {
                if(type->typeName.empty() || type->dimensions.empty())
                {
                    throw armarx::LocalException("AronEigenMatrixTypeNavigator: Either used type is empty or no dimension set!");
                }
                return type;
            }
        }
    }
}

