/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// Base Class
#include "AronTypeNavigator.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronConcepts.h>

namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    class Aron##upperType##TypeNavigator; \
    typedef std::shared_ptr<Aron##upperType##TypeNavigator> Aron##upperType##TypeNavigatorPtr; \
    \
    class Aron##upperType##TypeNavigator : \
        virtual public AronTypeNavigator \
    { \
    public: \
        using PointerType = Aron##upperType##TypeNavigatorPtr; \
        \
        /* constructors */ \
        Aron##upperType##TypeNavigator() = delete; \
        Aron##upperType##TypeNavigator(const AronPath&); \
        \
        /* static methods */ \
        static Aron##upperType##TypeNavigatorPtr DynamicCast(const AronTypeNavigatorPtr&); \
        \
        /* virtual implementations */ \
        virtual type::AronTypePtr getResult() const override; \
        \
    private: \
        type::Aron##upperType##TypePtr type; \
    };

            HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
        }
    }
}
