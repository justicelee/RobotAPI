/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronTupleTypeNavigator.h"


namespace armarx
{
namespace aron
{
namespace typenavigator
{
// constructors
AronTupleTypeNavigator::AronTupleTypeNavigator(const AronPath& path) :
    navigator::AronNavigator<AronTypeDescriptor, type::AronType>::AronNavigator(AronTypeDescriptor::eAronTupleType, path),
    AronTypeNavigator(AronTypeDescriptor::eAronTupleType, path),
    AronListSerializerTypeNavigator(AronTypeDescriptor::eAronTupleType, path),
    type(new type::AronTupleType())
{
}

// static methods
AronTupleTypeNavigatorPtr AronTupleTypeNavigator::DynamicCast(const AronTypeNavigatorPtr& n)
{
    AronTupleTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronTupleTypeNavigator>(n);
    CheckTypeNavigatorPtrForNull(casted, "The path was: " + n->pathToString());
    return casted;
}

// public member functions
void AronTupleTypeNavigator::setAcceptedType(const AronTypeNavigatorPtr &)
{
    throw armarx::LocalException("Called invalid function!");
}

AronTypeNavigatorPtr AronTupleTypeNavigator::getAcceptedType() const
{
    throw armarx::LocalException("Called invalid function!");
}

void AronTupleTypeNavigator::addAcceptedType(const AronTypeNavigatorPtr& v)
{
    type->elementTypes.push_back(v->getResult());
    acceptedTypeNavigators.push_back(v);
}

std::vector<AronTypeNavigatorPtr> AronTupleTypeNavigator::getAcceptedTypes() const
{
    return acceptedTypeNavigators;
}

// virtual implementations
type::AronTypePtr AronTupleTypeNavigator::getResult() const
{
    if(acceptedTypeNavigators.empty())
    {
        throw armarx::LocalException("AronTupleTypeNavigator: Has no elements!");
    }
    return type;
}
}
}
}

