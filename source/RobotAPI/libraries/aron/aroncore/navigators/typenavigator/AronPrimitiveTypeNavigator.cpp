/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// STD/STL
#include <string>
#include <map>

// Header
#include "AronPrimitiveTypeNavigator.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronConcepts.h>

namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    /* constructors */ \
    Aron##upperType##TypeNavigator::Aron##upperType##TypeNavigator(const AronPath& path) : \
        navigator::AronNavigator<AronTypeDescriptor, type::AronType>::AronNavigator(AronTypeDescriptor::eAron##upperType##Type, path), \
        AronTypeNavigator(AronTypeDescriptor::eAron##upperType##Type, path), \
        type(new type::Aron##upperType##Type()) \
    { \
    } \
    \
    /* static methods */ \
    Aron##upperType##TypeNavigatorPtr Aron##upperType##TypeNavigator::DynamicCast(const AronTypeNavigatorPtr& n)\
    {\
        Aron##upperType##TypeNavigatorPtr casted = std::dynamic_pointer_cast<Aron##upperType##TypeNavigator>(n);\
        CheckTypeNavigatorPtrForNull(casted, "The path was: " + n->pathToString());\
        return casted;\
    }\
    \
    /* virtual implementations */\
    type::AronTypePtr Aron##upperType##TypeNavigator::getResult() const\
    {\
        return type; \
    }

            HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
        }
    }
}
