/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL
#include <string>
#include <map>

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronTypeNavigator.h"

// ArmarX
//#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTypeNavigatorFactory.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronObjectTypeNavigator.h>

namespace armarx
{
namespace aron
{
namespace typenavigator
{
// constructors
AronTypeNavigator::AronTypeNavigator(const AronTypeDescriptor& d, const AronPath& path) :
    navigator::AronNavigator<AronTypeDescriptor, type::AronType>::AronNavigator(d, path)
{
}

AronTypeNavigatorPtr AronTypeNavigator::FromAronType(const type::AronTypePtr&, const AronPath&)
{
    return nullptr;
}

void AronTypeNavigator::CheckTypeNavigatorPtrForNull(const AronTypeNavigatorPtr& data, const std::string& messageIfIsNull)
{
    if (data.get() == nullptr)
    {
        throw exception::AronTypePtrIsNullException("AronTypeNavigator", "CheckTypeNavigatorPtrForNull", "Could not cast an AronTypeNavigatorPtr. The Ptr was NULL. " + messageIfIsNull);
    }
}
}
}
}


