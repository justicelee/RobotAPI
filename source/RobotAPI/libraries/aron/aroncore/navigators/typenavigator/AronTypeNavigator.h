/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <vector>
#include <map>

// Boost
#include <boost/core/demangle.hpp>
#include <boost/algorithm/string.hpp>

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronException.h>
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/AronNavigator.h>

namespace armarx
{
namespace aron
{
namespace typenavigator
{
class AronTypeNavigatorFactory;
typedef std::shared_ptr<AronTypeNavigatorFactory> AronTypeNavigatorFactoryPtr;

class AronObjectTypeNavigator;
typedef std::shared_ptr<AronObjectTypeNavigator> AronObjectTypeNavigatorPtr;

class AronTypeNavigator;
typedef std::shared_ptr<AronTypeNavigator> AronTypeNavigatorPtr;

class AronTypeNavigator :
        virtual public navigator::AronNavigator<AronTypeDescriptor, type::AronType>
{
public:
    using PointerType = AronTypeNavigatorPtr;

public:
    // constructors
    AronTypeNavigator() = delete;
    AronTypeNavigator(const AronTypeDescriptor&, const AronPath&);

    // virtual methods
    virtual type::AronTypePtr getResult() const override = 0;

    // static methods
    static AronTypeNavigatorPtr FromAronType(const type::AronTypePtr&, const AronPath& = AronPath());

    template<typename AronAbstractTypeNavigatorClass>
    static typename AronAbstractTypeNavigatorClass::PointerType DynamicCast(const AronTypeNavigatorPtr& n)
    {
        return AronAbstractTypeNavigatorClass::DynamicCast(n);
    }

protected:
    static void CheckTypeNavigatorPtrForNull(const AronTypeNavigatorPtr& data, const std::string&);
};




class AronDictSerializerTypeNavigator;
typedef std::shared_ptr<AronDictSerializerTypeNavigator> AronDictSerializerTypeNavigatorPtr;

class AronDictSerializerTypeNavigator :
        virtual public AronTypeNavigator
{
public:
    using PointerType = AronDictSerializerTypeNavigatorPtr;

public:
    AronDictSerializerTypeNavigator() = delete;
    AronDictSerializerTypeNavigator(const AronTypeDescriptor& n, const AronPath& path) :
        AronTypeNavigator(n, path)
    {}

    // virtual methods
    virtual void setAcceptedType(const AronTypeNavigatorPtr&) = 0;
    virtual void addAcceptedType(const std::string&, const AronTypeNavigatorPtr&) = 0;

    virtual AronTypeNavigatorPtr getAcceptedType() const = 0;
    virtual std::map<std::string, AronTypeNavigatorPtr> getAcceptedTypes() const = 0;

    // static methods
    static AronDictSerializerTypeNavigatorPtr DynamicCast(const AronTypeNavigatorPtr& n)
    {
        AronDictSerializerTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronDictSerializerTypeNavigator>(n);
        CheckTypeNavigatorPtrForNull(casted, "");
        return casted;
    }
};



class AronListSerializerTypeNavigator;
typedef std::shared_ptr<AronListSerializerTypeNavigator> AronListSerializerTypeNavigatorPtr;

class AronListSerializerTypeNavigator :
        virtual public AronTypeNavigator
{
public:
    using PointerType = AronListSerializerTypeNavigatorPtr;

public:
    AronListSerializerTypeNavigator() = delete;
    AronListSerializerTypeNavigator(const AronTypeDescriptor& n, const AronPath& path) :
        AronTypeNavigator(n, path)
    {}

    // virtual methods
    virtual void setAcceptedType(const AronTypeNavigatorPtr&) = 0;
    virtual void addAcceptedType(const AronTypeNavigatorPtr&) = 0;

    virtual AronTypeNavigatorPtr getAcceptedType() const = 0;
    virtual std::vector<AronTypeNavigatorPtr> getAcceptedTypes() const = 0;

    // static methods
    static AronListSerializerTypeNavigatorPtr DynamicCast(const AronTypeNavigatorPtr& n)
    {
        AronListSerializerTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronListSerializerTypeNavigator>(n);
        CheckTypeNavigatorPtrForNull(casted, "");
        return casted;
    }
};



class AronNDArraySerializerTypeNavigator;
typedef std::shared_ptr<AronNDArraySerializerTypeNavigator> AronNDArraySerializerTypeNavigatorPtr;

class AronNDArraySerializerTypeNavigator :
        virtual public AronTypeNavigator
{
public:
    using PointerType = AronNDArraySerializerTypeNavigatorPtr;

public:
    AronNDArraySerializerTypeNavigator() = delete;
    AronNDArraySerializerTypeNavigator(const AronTypeDescriptor& n, const AronPath& path) :
        AronTypeNavigator(n, path)
    {}

    // virtual methods
    virtual void setDimensions(const std::vector<int>&) = 0;
    virtual void setUsedType(const std::string&) = 0;

    virtual std::string getUsedType() const = 0;
    virtual std::vector<int> getDimensions() const = 0;

    // static methods
    static AronNDArraySerializerTypeNavigatorPtr DynamicCast(const AronTypeNavigatorPtr& n)
    {
        AronNDArraySerializerTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronNDArraySerializerTypeNavigator>(n);
        CheckTypeNavigatorPtrForNull(casted, "");
        return casted;
    }
};
}
}
}
