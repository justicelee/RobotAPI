/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronDictTypeNavigator.h"


namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {
            // constructors
            AronDictTypeNavigator::AronDictTypeNavigator(const AronPath& path) :
                AronNavigator<AronTypeDescriptor, type::AronType>::AronNavigator(AronTypeDescriptor::eAronDictType, path),
                AronTypeNavigator(AronTypeDescriptor::eAronDictType, path),
                AronDictSerializerTypeNavigator(AronTypeDescriptor::eAronDictType, path),
                type(new type::AronDictType())
            {

            }

            void AronDictTypeNavigator::addAcceptedType(const std::string &, const AronTypeNavigatorPtr &)
            {
                throw armarx::LocalException("Called invalid function!");
            }

            std::map<std::string, AronTypeNavigatorPtr> AronDictTypeNavigator::getAcceptedTypes() const
            {
                throw armarx::LocalException("Called invalid function!");
            }

            AronTypeNavigatorPtr AronDictTypeNavigator::getAcceptedType() const
            {
                return acceptedTypeNavigator;
            }

            void AronDictTypeNavigator::setAcceptedType(const AronTypeNavigatorPtr& a)
            {
                type->acceptedType = a->getResult();
                acceptedTypeNavigator = a;
            }

            // static methods
            AronDictTypeNavigatorPtr AronDictTypeNavigator::DynamicCast(const AronTypeNavigatorPtr& n)
            {
                AronDictTypeNavigatorPtr casted = std::dynamic_pointer_cast<AronDictTypeNavigator>(n);
                CheckTypeNavigatorPtrForNull(casted, "The path was: " + n->pathToString());
                return casted;
            }

            // virtual implementations
            type::AronTypePtr AronDictTypeNavigator::getResult() const
            {
                if (type->acceptedType.get() == nullptr)
                {
                    throw armarx::LocalException("AronDictTypeNavigator: Accepted type not set!");
                }

                return type;
            }
        }
    }
}

