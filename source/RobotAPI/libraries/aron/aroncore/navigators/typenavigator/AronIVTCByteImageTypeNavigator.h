/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base Class
#include "AronTypeNavigator.h"

namespace armarx
{
    namespace aron
    {
        namespace typenavigator
        {

            class AronIVTCByteImageTypeNavigator;
            typedef std::shared_ptr<AronIVTCByteImageTypeNavigator> AronIVTCByteImageTypeNavigatorPtr;

            class AronIVTCByteImageTypeNavigator :
                virtual public AronNDArraySerializerTypeNavigator
            {
            public:
                using PointerType = AronIVTCByteImageTypeNavigatorPtr;

            public:
                // constructors
                AronIVTCByteImageTypeNavigator() = delete;
                AronIVTCByteImageTypeNavigator(const AronPath& path);

                unsigned int getWidth() const;
                unsigned int getHeight() const;


                void setWidth(const unsigned int&);
                void setHeight(const unsigned int&);

                // static methods
                static AronIVTCByteImageTypeNavigatorPtr DynamicCast(const AronTypeNavigatorPtr& n);

                // virtual implementations
                virtual void setDimensions(const std::vector<int> &) override;
                virtual void setUsedType(const std::string&) override;

                virtual std::vector<int> getDimensions() const override;
                virtual std::string getUsedType() const override;

                virtual type::AronTypePtr getResult() const override;

            private:
                // members
                type::AronIVTCByteImageTypePtr type;
            };
        }
    }
}
