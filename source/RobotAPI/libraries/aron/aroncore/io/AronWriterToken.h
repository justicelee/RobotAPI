/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

#include <RobotAPI/libraries/aron/aroncore/AronConfig.h>

using namespace std;

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            template<typename AronDescriptorTypename, typename AronNavigatorTypename>
            class AronWriterToken;

            template<typename AronDescriptorTypename, typename AronNavigatorTypename>
            using AronWriterTokenPtr = std::shared_ptr<AronWriterToken<AronDescriptorTypename, AronNavigatorTypename>> ;

            template<typename AronDescriptorTypename, typename AronNavigatorTypename>
            class AronWriterToken
            {
            public:
                AronWriterToken() = delete;
                AronWriterToken(const typename AronNavigatorTypename::PointerType& e) :
                    element(e), currentIndex(0), currentKey("")
                {

                };

                // virtual definitions
                virtual void addElement(const typename AronNavigatorTypename::PointerType&) = 0;
                virtual std::string toElementAccessor() const = 0;

                // public member functions
                AronDescriptorTypename getDescriptor() const
                {
                    return element->getDescriptor();
                }
                typename AronNavigatorTypename::PointerType getElement() const
                {
                    return element;
                }

                // index functions
                void increaseCurrentIndex()
                {
                    currentIndex++;
                };
                unsigned int getCurrentIndex() const
                {
                    return currentIndex;
                }

                // key functions
                void setCurrentKey(const std::string& s)
                {
                    currentKey = s;
                }
                std::string getCurrentKey() const
                {
                    return currentKey;
                }

            private:
                // members
                typename AronNavigatorTypename::PointerType element;

                // current index
                unsigned int currentIndex;

                // current key
                std::string currentKey;
            };
        }
    }
}
