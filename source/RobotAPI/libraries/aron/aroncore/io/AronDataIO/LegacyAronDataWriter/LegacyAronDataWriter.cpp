/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>
#include <numeric>

// Header
#include "LegacyAronDataWriter.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronResolver.h>

using namespace std;

namespace armarx
{
namespace aron
{
namespace io
{

void LegacyAronDataWriter::SetupWriterFromAronDataPtr(AronDataWriterPtr& writer, const data::AronDataPtr& aron)
{
    AronDataDescriptor desc = AronResolver::GetTypeDescriptorForAronData(aron);
    switch(desc)
    {
    case eAronDict:
    {
        data::AronDictPtr casted = data::AronDictPtr::dynamicCast(aron);
        writer->writeStartDict();
        for(const auto& [key, value] : casted->elements)
        {
            writer->writeKey(key);
            LegacyAronDataWriter::SetupWriterFromAronDataPtr(writer, value);
        }
        writer->writeEndDict();
        break;
    }
    case eAronList:
    {
        data::AronListPtr casted = data::AronListPtr::dynamicCast(aron);
        writer->writeStartList();
        for(const auto& value : casted->elements)
        {
            LegacyAronDataWriter::SetupWriterFromAronDataPtr(writer, value);
        }
        writer->writeEndList();
        break;
    }
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    case eAron##upperType: \
    { \
        data::Aron##upperType##Ptr casted = data::Aron##upperType##Ptr::dynamicCast(aron); \
        writer->write##upperType(casted->data.size(), casted->data.data()); \
        break; \
    }

HANDLE_COMPLEX_DATA
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    case eAron##upperType: \
    { \
        data::Aron##upperType##Ptr casted = data::Aron##upperType##Ptr::dynamicCast(aron); \
        writer->write##upperType(casted->value); \
        break; \
    }

        HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

    default:
    {
        throw armarx::aron::exception::AronInvalidDataException("LegacyAronDataWriter", "SetupWriterFromAronDataPtr", "Data-Type could not be resolved.");
    }
    }
}

}
}
}
