/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>

// BaseClass
#include <RobotAPI/libraries/aron/aroncore/io/AronReader.h>

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

#include <RobotAPI/libraries/aron/aroncore/AronConfig.h>

using namespace std;

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            class AronDataReader;
            typedef std::shared_ptr<AronDataReader> AronDataReaderPtr;

            class AronDataReader :
                    virtual public AronReader
            {
            public:
                AronDataReader() = default;

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool readStart##upperType() = 0; \
    virtual bool readEnd##upperType() = 0; \

                HANDLE_CONTAINER_DATA
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual unsigned char* read##upperType() = 0; \

                HANDLE_COMPLEX_DATA
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual lowerType read##upperType() = 0; \

                HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

                virtual std::string readKey() = 0;
                virtual bool readMember(const std::string&) = 0;
            };
        }
    }
}
