/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller (fabian dot peller at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AronDataJSONWriter.h"

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            bool AronDataJSONWriter::writeStartDict()
            {
                ss << "{";
                writeNewLineWithIndentIncrease();
                return true;
            }

            bool AronDataJSONWriter::writeEndDict()
            {
                ss << "},";
                writeNewLineWithIndentDecrease();
                return true;
            }

            bool AronDataJSONWriter::writeStartList()
            {
                ss << "[";
                writeNewLineWithIndentIncrease();
                return true;
            }

            bool AronDataJSONWriter::writeEndList()
            {
                ss << "],";
                writeNewLineWithIndentDecrease();
                return true;
            }

            bool AronDataJSONWriter::writeNDArray(const unsigned int& num, const unsigned char* data)
            {
                ss << "[ ";
                for (unsigned int i = 0; i < num; ++i)
                {
                    ss << data[i] << ", ";
                }
                ss << " ],";
                writeNewLine();
                return true;
            }

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronDataJSONWriter::write##upperType(const lowerType& x) \
    { \
        ss << "{"; \
        writeNewLineWithIndentIncrease(); \
        ss << "\"Type\"" << ": \"" << std::string(#upperType) << "\","; \
        writeNewLine(); \
        ss << "\"Value\"" << ": \"" << x << "\","; \
        writeNewLineWithIndentDecrease(); \
        ss << "},"; \
        writeNewLine(); \
        \
        return true; \
    }

            HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

            bool AronDataJSONWriter::writeKey(const std::string& k)
            {
                ss << k << ": ";
                return true;
            }
        }
    }
}
