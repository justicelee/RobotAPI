/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller (fabian dot peller at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AronDataXMLWriter.h"

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            bool AronDataXMLWriter::writeStartDict()
            {
                ss << "<Dict>";
                writeNewLineWithIndentIncrease();
                return true;
            }

            bool AronDataXMLWriter::writeEndDict()
            {
                ss << "</Dict>";
                writeNewLineWithIndentDecrease();
                return true;
            }

            bool AronDataXMLWriter::writeStartList()
            {
                ss << "<List>";
                writeNewLineWithIndentIncrease();
                return true;
            }

            bool AronDataXMLWriter::writeEndList()
            {
                ss << "</List>";
                writeNewLineWithIndentDecrease();
                return true;
            }

            bool AronDataXMLWriter::writeNDArray(const unsigned int& num, const unsigned char* data)
            {
                ss << "<NDArray>";
                for (unsigned int i = 0; i < num; ++i)
                {
                    ss << data[i] << ", ";
                }
                ss << "</NDArray>";
                writeNewLine();
                return true;
            }

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronDataXMLWriter::write##upperType(const lowerType& x) \
    { \
        ss << ("<" + std::string(#upperType) + ">") << x << ("</" + std::string(#upperType) + ">"); \
        return true; \
    }

            HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

            bool AronDataXMLWriter::writeKey(const std::string& k)
            {
                ss << "<Key>" << k << "</Key>";
                writeNewLine();
                return true;
            }
        }
    }
}
