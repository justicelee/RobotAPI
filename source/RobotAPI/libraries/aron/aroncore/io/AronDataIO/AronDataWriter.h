/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>

// BaseClass
#include <RobotAPI/libraries/aron/aroncore/io/AronWriter.h>

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

#include <RobotAPI/interface/aron.h>

using namespace std;

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            class AronDataWriter;
            typedef std::shared_ptr<AronDataWriter> AronDataWriterPtr;

            class AronDataWriter :
                    virtual public AronWriter
            {
            public:
                AronDataWriter() = default;

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool writeStart##upperType() = 0; \
    virtual bool writeEnd##upperType() = 0; \

                HANDLE_CONTAINER_DATA
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool write##upperType(const unsigned int&, const unsigned char*) = 0; \

                HANDLE_COMPLEX_DATA
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool write##upperType(const lowerType&) = 0; \

                HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

                virtual bool writeKey(const std::string&) = 0;
            };
        }
    }
}
