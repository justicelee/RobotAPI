/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// Header
#include "AronDataNavigatorReaderToken.h"

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

namespace armarx
{
    namespace aron
    {
        namespace io
        {

            // constructors
            AronDataNavigatorReaderToken::AronDataNavigatorReaderToken(const datanavigator::AronDataNavigatorPtr& data) :
                data(data),
                cnt(0),
                key("")
            {
                if (getDescriptor() == AronDataDescriptor::eAronDict)
                {
                    allKeys = datanavigator::AronDictDataNavigator::DynamicCast(data)->getAllKeys();
                }
            }

            // public member functions
            AronDataDescriptor AronDataNavigatorReaderToken::getDescriptor() const
            {
                return data->getDescriptor();
            }

            bool AronDataNavigatorReaderToken::checkedAllChildren() const
            {
                return cnt >= data->childrenSize();
            }

            // List and Tuple member functions
            unsigned int AronDataNavigatorReaderToken::getCurrentIndex() const
            {
                if (getDescriptor() != AronDataDescriptor::eAronList)
                {
                    throw LocalException("AronDataNavigatorReaderToken: Cant get index of a non-list token.");
                }
                return cnt;
            }

            // Dict member functions
            std::string AronDataNavigatorReaderToken::getCurrentKey() const
            {
                if (cnt >= allKeys.size())
                {
                    throw LocalException("AronDataNavigatorReaderToken: Cant get key at index " + std::to_string(cnt) + " of vector with size " + std::to_string(allKeys.size()) + ".");
                }
                return allKeys[cnt];
            }

            // Dict member functions
            void AronDataNavigatorReaderToken::addCurrentKey(const std::string& k)
            {
                allKeys.push_back(k);
            }

            // virtual definitions
            datanavigator::AronDataNavigatorPtr AronDataNavigatorReaderToken::getElementAndIncreaseCnt()
            {
                switch (getDescriptor())
                {
                    case eAronDict:
                    {
                        datanavigator::AronDictDataNavigatorPtr casted = datanavigator::AronDictDataNavigator::DynamicCast(data);
                        datanavigator::AronDataNavigatorPtr ret = casted->getElement(getCurrentKey());
                        cnt++;
                        return ret;
                    }
                    case eAronList:
                    {
                        datanavigator::AronListDataNavigatorPtr casted = datanavigator::AronListDataNavigator::DynamicCast(data);
                        datanavigator::AronDataNavigatorPtr ret = casted->getElement(getCurrentIndex());
                        cnt++;
                        return ret;
                    }
                    default:
                        throw LocalException("AronDataNavigatorReaderToken: Could not resove a type of a navigator. Allowed are only container and complex types due to performance.");
                }
            }
        }
    }
}
