/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <stack>

// BaseClass
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/classReaders/AronDataClassReader.h>

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/classReaders/AronDataNavigatorReader/AronDataNavigatorReaderToken.h>

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            class AronDataNavigatorReader;
            typedef std::shared_ptr<AronDataNavigatorReader> AronDataNavigatorReaderPtr;

            class AronDataNavigatorReader :
                virtual public AronDataClassReader<data::AronDataPtr>
            {
            public:
                using PointerType = AronDataNavigatorReaderPtr;

            public:
                // constructors
                AronDataNavigatorReader(const data::AronDataPtr& aron);

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool readStart##upperType() override; \
    virtual bool readEnd##upperType() override; \

                HANDLE_CONTAINER_DATA
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual unsigned char* read##upperType() override; \

                HANDLE_COMPLEX_DATA
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual lowerType read##upperType() override; \

                HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

                virtual std::string readKey() override;
                virtual bool readMember(const std::string&) override;

            private:
                datanavigator::AronDataNavigatorPtr input;
                std::stack<AronDataNavigatorReaderTokenPtr> stack;
                bool readInitialStart;
            };
        }
    }
}
