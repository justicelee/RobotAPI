/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>

// Base Class
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/AronDataReader.h>

using namespace std;

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            template <typename T>
            class AronDataClassReader;

            template <typename T>
            using AronDataClassReaderPtr = std::shared_ptr<AronDataClassReader<T>>;

            template <typename T>
            class AronDataClassReader :
                    virtual public AronDataReader
            {
            public:
                AronDataClassReader(const T& input) :
                    data(input)
                {
                }


            protected:
                T data;
            };
        }
    }
}
