/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>

// Header
#include "AronDataNavigatorReader.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronListDataNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronPrimitiveDataNavigator.h>

using namespace std;

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            AronDataNavigatorReader::AronDataNavigatorReader(const data::AronDataPtr& aron) :
                AronDataClassReader<data::AronDataPtr>(aron),
                input(datanavigator::AronDataNavigator::FromAronData(aron, AronPath())),
                readInitialStart(false)
            {

            }

            bool AronDataNavigatorReader::readStartList()
            {
                if (!readInitialStart)
                {
                    datanavigator::AronListDataNavigatorPtr l = datanavigator::AronListDataNavigator::DynamicCast(input);
                    AronDataNavigatorReaderTokenPtr token = AronDataNavigatorReaderTokenPtr(new AronDataNavigatorReaderToken(l));
                    stack.push(token);

                    readInitialStart = true;
                    return true;
                }
                AronDataNavigatorReaderTokenPtr lastToken = stack.top();
                datanavigator::AronListDataNavigatorPtr list = datanavigator::AronDataNavigator::DynamicCast<datanavigator::AronListDataNavigator>(lastToken->getElementAndIncreaseCnt());

                AronDataNavigatorReaderTokenPtr newToken = AronDataNavigatorReaderTokenPtr(new AronDataNavigatorReaderToken(list));
                stack.push(newToken);
                return true;
            }

            bool AronDataNavigatorReader::readStartDict()
            {
                if (!readInitialStart)
                {
                    datanavigator::AronDictDataNavigatorPtr obj = datanavigator::AronDictDataNavigator::DynamicCast(input);
                    AronDataNavigatorReaderTokenPtr token = AronDataNavigatorReaderTokenPtr(new AronDataNavigatorReaderToken(obj));
                    stack.push(token);

                    readInitialStart = true;
                    return true;
                }
                AronDataNavigatorReaderTokenPtr lastToken = stack.top();
                datanavigator::AronDictDataNavigatorPtr dict = datanavigator::AronDataNavigator::DynamicCast<datanavigator::AronDictDataNavigator>(lastToken->getElementAndIncreaseCnt());

                AronDataNavigatorReaderTokenPtr newToken = AronDataNavigatorReaderTokenPtr(new AronDataNavigatorReaderToken(dict));
                stack.push(newToken);
                return true;
            }

            bool AronDataNavigatorReader::readEndList()
            {
                AronDataNavigatorReaderTokenPtr token = stack.top();
                if (token->getDescriptor() != AronDataDescriptor::eAronList)
                {
                    throw LocalException("AronDataNavigatorReader: A token in the stack has the wrong type. Expected List!");
                }

                if (token->checkedAllChildren())
                {
                    stack.pop();
                    return true;
                }
                return false;
            }

            bool AronDataNavigatorReader::readEndDict()
            {
                AronDataNavigatorReaderTokenPtr token = stack.top();
                if (token->getDescriptor() != AronDataDescriptor::eAronDict)
                {
                    throw LocalException("AronDataNavigatorReader: A token in the stack has the wrong type. Expected Dict!");
                    return false;
                }

                if (token->checkedAllChildren())
                {
                    stack.pop();
                    return true;
                }
                return false;
            }

            // Complex Data
            unsigned char* AronDataNavigatorReader::readNDArray()
            {
                AronDataNavigatorReaderTokenPtr token = stack.top();
                datanavigator::AronDataNavigatorPtr nav = token->getElementAndIncreaseCnt();
                datanavigator::AronNDArrayDataNavigatorPtr casted = datanavigator::AronNDArrayDataNavigator::DynamicCast(nav);

                return casted->getData();
            }

            // Read primitives
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    lowerType AronDataNavigatorReader::read##upperType() \
    { \
        AronDataNavigatorReaderTokenPtr token = stack.top(); \
        datanavigator::AronDataNavigatorPtr nav = token->getElementAndIncreaseCnt(); \
        datanavigator::Aron##upperType##DataNavigatorPtr casted = datanavigator::Aron##upperType##DataNavigator::DynamicCast(nav); \
        return casted->getValue(); \
    }

            HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

            // ReadKey
            std::string AronDataNavigatorReader::readKey()
            {
                AronDataNavigatorReaderTokenPtr token = stack.top();
                return token->getCurrentKey();
            }

            bool AronDataNavigatorReader::readMember(const std::string& k)
            {
                AronDataNavigatorReaderTokenPtr token = stack.top();
                token->addCurrentKey(k);
                return true;
            }
        }
    }
}
