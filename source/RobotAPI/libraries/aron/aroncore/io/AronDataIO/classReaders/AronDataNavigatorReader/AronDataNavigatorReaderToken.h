/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronConfig.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronListDataNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDictDataNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronNDArrayDataNavigator.h>

using namespace std;

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            // BaseClass
            class AronDataNavigatorReaderToken;
            typedef std::shared_ptr<AronDataNavigatorReaderToken> AronDataNavigatorReaderTokenPtr;

            class AronDataNavigatorReaderToken
            {
            public:
                using PointerType = AronDataNavigatorReaderTokenPtr;

            public:
                // constructors
                AronDataNavigatorReaderToken(const datanavigator::AronDataNavigatorPtr& data);

                // public member functions
                AronDataDescriptor getDescriptor() const;
                bool checkedAllChildren() const;
                datanavigator::AronDataNavigatorPtr getElementAndIncreaseCnt();

                // List and Tuple member functions
                unsigned int getCurrentIndex() const;

                // Dict and object member functions
                void addCurrentKey(const std::string&);
                std::string getCurrentKey() const;

            private:
                // members
                datanavigator::AronDataNavigatorPtr data;
                unsigned int cnt;

                // dict and object members
                std::string key;
                std::vector<std::string> allKeys;
            };
        }
    }
}
