/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// Header
#include "AronDataNavigatorWriterToken.h"

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/libraries/aron/aroncore/AronDescriptor.h>

namespace armarx
{
namespace aron
{
namespace io
{
// constructor
AronDataNavigatorWriterToken::AronDataNavigatorWriterToken(const datanavigator::AronDataNavigatorPtr& d) :
    AronWriterToken<AronDataDescriptor, datanavigator::AronDataNavigator>(d)
{

}

void AronDataNavigatorWriterToken::addElement(const datanavigator::AronDataNavigatorPtr& n)
{
    switch (getDescriptor())
    {
    case eAronDict:
    {
        datanavigator::AronDictDataNavigatorPtr casted = datanavigator::AronDictDataNavigator::DynamicCast(getElement());
        casted->addElement(getCurrentKey(), n);
        break;
    }
    case eAronList:
    {
        datanavigator::AronListDataNavigatorPtr casted = datanavigator::AronListDataNavigator::DynamicCast(getElement());
        casted->addElement(n);
        increaseCurrentIndex();
        break;
    }
    default:
        throw LocalException("AronDataNavigatorWriterToken: Could not resove a type of a navigator. Allowed are only containers. The path was: " + getElement()->pathToString());
    }
}

std::string AronDataNavigatorWriterToken::toElementAccessor() const
{
    switch (getDescriptor())
    {
    case eAronDict:
        return "[" + getCurrentKey() + "]";
    case eAronList:
        return "[" + std::to_string(getCurrentIndex()) + "]";
    default:
        throw LocalException("AronDataNavigatorWriterToken: Could not resove a type of a navigator. Allowed are only container and complex types due to performance. The path was: " + getElement()->pathToString());
    }
}
}
}
}
