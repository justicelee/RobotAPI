/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>
#include <numeric>

// Header
#include "AronDataNavigatorWriter.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronListDataNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronPrimitiveDataNavigator.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronListTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronObjectTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTupleTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronPrimitiveTypeNavigator.h>

using namespace std;

namespace armarx
{
namespace aron
{
namespace io
{

AronDataNavigatorWriter::AronDataNavigatorWriter() :
    wroteInitialStartContainer(false)
{

}

AronPath AronDataNavigatorWriter::generateAronPath()
{
    AronPath path;
    if (!wroteInitialStartContainer)
    {
        wroteInitialStartContainer = true;
    }
    else
    {
        AronDataNavigatorWriterTokenPtr parent_token = stack.top();
        path = AronPath(parent_token->getElement()->getPath(), parent_token->toElementAccessor());
    }
    return path;
}

// Containers
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronDataNavigatorWriter::writeStart##upperType() \
{ \
    AronPath path = generateAronPath(); \
    datanavigator::AronDataNavigatorPtr data = datanavigator::AronDataNavigatorPtr(new datanavigator::Aron##upperType##DataNavigator(path)); \
    AronDataNavigatorWriterTokenPtr new_token = AronDataNavigatorWriterTokenPtr(new AronDataNavigatorWriterToken(data)); \
    stack.push(new_token); \
    return true; \
} \
    \
    bool AronDataNavigatorWriter::writeEnd##upperType() \
{ \
    AronDataNavigatorWriterTokenPtr token = stack.top(); \
    lastRemovedElement = token->getElement(); \
    stack.pop(); \
    \
    if (stack.size() > 0) \
{ \
    AronDataNavigatorWriterTokenPtr prevToken = stack.top(); \
    prevToken->addElement(lastRemovedElement); \
} \
    return true; \
}

HANDLE_CONTAINER_DATA
#undef RUN_ARON_MACRO

// Complex Types
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronDataNavigatorWriter::write##upperType(const unsigned int& num, const unsigned char* data) \
{ \
    AronPath path = generateAronPath(); \
    AronDataNavigatorWriterTokenPtr token = stack.top(); \
    datanavigator::Aron##upperType##DataNavigatorPtr aron(new datanavigator::Aron##upperType##DataNavigator(path)); \
    aron->setData(num, data); \
    token->addElement(aron); \
    return true; \
}

HANDLE_COMPLEX_DATA
#undef RUN_ARON_MACRO


#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronDataNavigatorWriter::write##upperType(const lowerType& x) \
{ \
    AronPath path = generateAronPath(); \
    AronDataNavigatorWriterTokenPtr token = stack.top(); \
    datanavigator::Aron##upperType##DataNavigatorPtr aron(new datanavigator::Aron##upperType##DataNavigator(path)); \
    aron->setValue(x); \
    token->addElement(aron); \
    return true; \
}

HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

bool AronDataNavigatorWriter::writeKey(const std::string& k)
{
    AronDataNavigatorWriterTokenPtr token = stack.top();
    token->setCurrentKey(k);
    return true;
}

data::AronDataPtr AronDataNavigatorWriter::getResult() const
{
    if (lastRemovedElement == nullptr)
    {
        throw LocalException("AronDataNavigatorWriter: Could not return NULL. Perhaps you never called a writeEnd* method?");
    }

    return lastRemovedElement->getResult();
}
}
}
}
