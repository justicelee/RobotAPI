/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <stack>

// BaseClass
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/classWriters/AronDataClassWriter.h>

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

#include <RobotAPI/libraries/aron/aroncore/AronConcepts.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronDataNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/classWriters/AronDataNavigatorWriter/AronDataNavigatorWriterToken.h>

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            class AronDataNavigatorWriter;
            typedef std::shared_ptr<AronDataNavigatorWriter> AronDataNavigatorWriterPtr;

            class AronDataNavigatorWriter :
                virtual public AronDataClassWriter<data::AronDataPtr>
            {
            public:
                AronDataNavigatorWriter();

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool writeStart##upperType() override; \
    virtual bool writeEnd##upperType() override;

                HANDLE_CONTAINER_DATA
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool write##upperType(const unsigned int&, const unsigned char*) override; \

                HANDLE_COMPLEX_DATA
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool write##upperType(const lowerType&) override;

                HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

                virtual bool writeKey(const std::string&) override;

                virtual data::AronDataPtr getResult() const override;

            private:
                AronPath generateAronPath();

            private:
                bool wroteInitialStartContainer;
                datanavigator::AronDataNavigatorPtr lastRemovedElement;
                std::stack<AronDataNavigatorWriterTokenPtr> stack;
            };
        }
    }
}
