/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>

// Base Class
#include <RobotAPI/libraries/aron/aroncore/io/AronWriterToken.h>

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronConfig.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronAllTypeNavigators.h>

namespace armarx
{
namespace aron
{
namespace io
{
class AronTypeNavigatorWriterToken;
typedef std::shared_ptr<AronTypeNavigatorWriterToken> AronTypeNavigatorWriterTokenPtr;

class AronTypeNavigatorWriterToken :
        virtual public AronWriterToken<AronTypeDescriptor, typenavigator::AronTypeNavigator>
{
public:
    using PointerType = AronTypeNavigatorWriterTokenPtr;

public:
    // constructor
    AronTypeNavigatorWriterToken() = delete;
    AronTypeNavigatorWriterToken(const typenavigator::AronTypeNavigatorPtr&);

    // virtual member functions
    virtual void addElement(const typenavigator::AronTypeNavigatorPtr&) override;
    virtual std::string toElementAccessor() const override;

    // name functions
    void setName(const std::string&);
};
}
}
}
