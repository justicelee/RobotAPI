/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>
#include <numeric>

// Header
#include "AronTypeNavigatorWriter.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronListDataNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/datanavigator/AronPrimitiveDataNavigator.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronListTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronObjectTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTupleTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronPrimitiveTypeNavigator.h>

using namespace std;

namespace armarx
{
namespace aron
{
namespace io
{

AronTypeNavigatorWriter::AronTypeNavigatorWriter() :
    wroteInitialStartContainer(false)
{

}

AronPath AronTypeNavigatorWriter::generateAronPath()
{
    AronPath path;
    if (!wroteInitialStartContainer)
    {
        wroteInitialStartContainer = true;
    }
    else
    {
        AronTypeNavigatorWriterTokenPtr parent_token = stack.top();
        path = AronPath(parent_token->getElement()->getPath(), parent_token->toElementAccessor());
    }
    return path;
}

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronTypeNavigatorWriter::writeStart##upperType##Type() \
{ \
    AronPath path = generateAronPath(); \
    typenavigator::AronTypeNavigatorPtr type = typenavigator::AronTypeNavigatorPtr(new typenavigator::Aron##upperType##TypeNavigator(path)); \
    AronTypeNavigatorWriterTokenPtr new_token = AronTypeNavigatorWriterTokenPtr(new AronTypeNavigatorWriterToken(type)); \
    stack.push(new_token); \
    return true; \
} \
    bool AronTypeNavigatorWriter::writeEnd##upperType##Type() \
{ \
    AronTypeNavigatorWriterTokenPtr token = stack.top(); \
    lastRemovedElement = token->getElement(); \
    stack.pop(); \
    \
    if (stack.size() > 0) \
{ \
    AronTypeNavigatorWriterTokenPtr prevToken = stack.top(); \
    prevToken->addElement(lastRemovedElement); \
} \
    return true; \
}

HANDLE_CONTAINER_TYPES
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronTypeNavigatorWriter::write##upperType##Type(const std::vector<int>& dims, const std::string& t) \
{ \
    AronPath path = generateAronPath(); \
    AronTypeNavigatorWriterTokenPtr token = stack.top(); \
    typenavigator::Aron##upperType##TypeNavigatorPtr aron(new typenavigator::Aron##upperType##TypeNavigator(path)); \
    aron->setUsedType(t); \
    aron->setDimensions(dims); \
    token->addElement(aron); \
    return true; \
}

HANDLE_COMPLEX_TYPES
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronTypeNavigatorWriter::write##upperType##Type() \
{ \
    AronPath path = generateAronPath(); \
    AronTypeNavigatorWriterTokenPtr token = stack.top(); \
    typenavigator::Aron##upperType##TypeNavigatorPtr aron(new typenavigator::Aron##upperType##TypeNavigator(path)); \
    token->addElement(aron); \
    return true; \
}

HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

bool AronTypeNavigatorWriter::writeKey(const std::string& k)
{
    AronTypeNavigatorWriterTokenPtr token = stack.top();
    token->setCurrentKey(k);
    return true;
}

bool AronTypeNavigatorWriter::writeObjectName(const std::string& n)
{
    AronTypeNavigatorWriterTokenPtr token = stack.top();
    token->setName(n);
    return false;
}

type::AronTypePtr AronTypeNavigatorWriter::getResult() const
{
    if (lastRemovedElement == nullptr)
    {
        throw LocalException("AronDataNavigatorWriter: lastRemovedElement is NULL. Perhaps you never called a writeEnd* method?");
    }
    return lastRemovedElement->getResult();
}
}
}
}
