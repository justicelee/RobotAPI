/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// Header
#include "AronTypeNavigatorWriterToken.h"

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/libraries/aron/aroncore/AronDescriptor.h>

namespace armarx
{
namespace aron
{
namespace io
{
// constructor
AronTypeNavigatorWriterToken::AronTypeNavigatorWriterToken(const typenavigator::AronTypeNavigatorPtr& t) :
    AronWriterToken<AronTypeDescriptor, typenavigator::AronTypeNavigator>(t)
{
}

void AronTypeNavigatorWriterToken::addElement(const typenavigator::AronTypeNavigatorPtr& n)
{
    switch (getDescriptor())
    {
    case eAronDictType:
    {
        typenavigator::AronDictTypeNavigatorPtr casted = typenavigator::AronDictTypeNavigator::DynamicCast(getElement());
        casted->setAcceptedType(n);
        break;
    }
    case eAronListType:
    {
        typenavigator::AronListTypeNavigatorPtr casted = typenavigator::AronListTypeNavigator::DynamicCast(getElement());
        casted->setAcceptedType(n);
        break;
    }
    case eAronObjectType:
    {
        typenavigator::AronObjectTypeNavigatorPtr casted = typenavigator::AronObjectTypeNavigator::DynamicCast(getElement());
        casted->addAcceptedType(getCurrentKey(), n);
        break;
    }
    case eAronTupleType:
    {
        typenavigator::AronTupleTypeNavigatorPtr casted = typenavigator::AronTupleTypeNavigator::DynamicCast(getElement());
        casted->addAcceptedType(n);
        increaseCurrentIndex();
        break;
    }
    default:
        throw LocalException("AronTypeNavigatorWriterToken: Could not resove a type of a navigator. Allowed are only containers. The path was: " + getElement()->pathToString());
    }
}

std::string AronTypeNavigatorWriterToken::toElementAccessor() const
{
    switch (getDescriptor())
    {
    case eAronDictType:
    case eAronListType:
        return "[ACCEPTED_TYPE]";
    case eAronObjectType:
        return "\"" + getCurrentKey() + "\"";
    case eAronTupleType:
        return "<" + std::to_string(getCurrentIndex()) + ">";
    default:
        throw LocalException("AronTypeNavigatorWriterToken: Could not resove a type of a navigator. Allowed are only containers. The path was: " + getElement()->pathToString());
    }
}

void AronTypeNavigatorWriterToken::setName(const std::string& n)
{
    if (getDescriptor() != AronTypeDescriptor::eAronObjectType)
    {
        throw LocalException("AronTypeNavigatorWriterToken: Cant set the name of a non-object token. The path was: " + getElement()->pathToString());
    }
    typenavigator::AronObjectTypeNavigatorPtr casted = typenavigator::AronObjectTypeNavigator::DynamicCast(getElement());
    casted->setName(n);
}
}
}
}
