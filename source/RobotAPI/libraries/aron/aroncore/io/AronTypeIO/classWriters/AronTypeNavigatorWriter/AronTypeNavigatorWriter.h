/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <stack>

// BaseClass
#include <RobotAPI/libraries/aron/aroncore/io/AronTypeIO/classWriters/AronTypeClassWriter.h>

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

#include <RobotAPI/libraries/aron/aroncore/AronConcepts.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronTypeIO/classWriters/AronTypeNavigatorWriter/AronTypeNavigatorWriterToken.h>

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            class AronTypeNavigatorWriter;
            typedef std::shared_ptr<AronTypeNavigatorWriter> AronTypeNavigatorWriterPtr;

            class AronTypeNavigatorWriter :
                virtual public AronTypeClassWriter<type::AronTypePtr>
            {
            public:
                AronTypeNavigatorWriter();

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool writeStart##upperType##Type() override; \
    virtual bool writeEnd##upperType##Type() override; \

                HANDLE_CONTAINER_TYPES
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool write##upperType##Type(const std::vector<int>&, const std::string&) override; \

                HANDLE_COMPLEX_TYPES
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    virtual bool write##upperType##Type() override; \

                HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

                virtual bool writeKey(const std::string&) override;
                virtual bool writeObjectName(const std::string&) override;

                virtual type::AronTypePtr getResult() const override;

            private:
                AronPath generateAronPath();

            private:
                bool wroteInitialStartContainer;
                typenavigator::AronTypeNavigatorPtr lastRemovedElement;
                std::stack<AronTypeNavigatorWriterTokenPtr> stack;
            };
        }
    }
}
