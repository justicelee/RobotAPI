/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller (fabian dot peller at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AronTypeXMLWriter.h"

namespace armarx
{
namespace aron
{
namespace io
{
bool AronTypeXMLWriter::writeStartDictType()
{
    ss << "<ObjectType>";
    writeNewLineWithIndentIncrease();
    return true;
}

bool AronTypeXMLWriter::writeEndDictType()
{
    ss << "</ObjectType>";
    writeNewLineWithIndentDecrease();
    return true;
}

bool AronTypeXMLWriter::writeStartListType()
{
    ss << "<ListType>";
    writeNewLineWithIndentIncrease();
    return true;
}

bool AronTypeXMLWriter::writeEndListType()
{
    ss << "</ListType>";
    writeNewLineWithIndentDecrease();
    return true;
}

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronTypeXMLWriter::write##upperType##Type(const std::vector<int>& d, const std::string& t) \
{ \
    ss << "<NDArrayType>"; \
    writeNewLineWithIndentIncrease(); \
    ss << "<Type>" << std::string(#upperType) << "</Type>"; \
    writeNewLine(); \
    ss << "<UsedType>" << t << "</UsedType>"; \
    writeNewLine(); \
    ss << "<Dimensions>"; \
    for(const int i : d) \
    { \
        ss << i << ", "; \
    } \
    ss << "</Dimensions>"; \
    writeNewLineWithIndentDecrease(); \
    ss << "</NDArrayType>"; \
    writeNewLine(); \
    \
    return true; \
}

HANDLE_COMPLEX_TYPES
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronTypeXMLWriter::write##upperType##Type() \
{ \
    ss << "<PrimitiveType>"; \
    writeNewLineWithIndentIncrease(); \
    ss << "<Type>" << std::string(#upperType) << "</Type>"; \
    writeNewLineWithIndentDecrease(); \
    ss << "</PrimitiveType>"; \
    writeNewLine(); \
    \
    return true; \
}

HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

bool AronTypeXMLWriter::writeKey(const std::string& k)
{
    ss << "<Key>" << k << "</Key>";
    return true;
}

bool AronTypeXMLWriter::writeObjectName(const std::string& n)
{
    ss << "<Name>" << n << "</Name>";
    writeNewLine();
    return true;
}
}
}
}
