/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller (fabian dot peller at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AronTypeJSONWriter.h"

namespace armarx
{
namespace aron
{
namespace io
{
bool AronTypeJSONWriter::writeStartDictType()
{
    ss << "{";
    writeNewLineWithIndentIncrease();
    return true;
}

bool AronTypeJSONWriter::writeEndDictType()
{
    ss << "},";
    writeNewLineWithIndentDecrease();
    return true;
}

bool AronTypeJSONWriter::writeStartListType()
{
    ss << "[";
    writeNewLineWithIndentIncrease();
    return true;
}

bool AronTypeJSONWriter::writeEndListType()
{
    ss << "],";
    writeNewLineWithIndentDecrease();
    return true;
}

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronTypeJSONWriter::write##upperType##Type(const std::vector<int>& d, const std::string& t) \
{ \
    ss << "{"; \
    writeNewLineWithIndentIncrease(); \
    ss << "\"Type\"" << ": \"" << std::string(#upperType) << "\","; \
    writeNewLine(); \
    ss << "\"UsedType\"" << ": \"" << t << "\","; \
    writeNewLine(); \
    ss << "\"Dimensions\"" << ": [ "; \
    for(const int i : d) \
    { \
        ss << i << ", "; \
    } \
    ss << " ],"; \
    writeNewLineWithIndentDecrease(); \
    ss << "},"; \
    writeNewLine(); \
    \
    return true; \
}

HANDLE_COMPLEX_TYPES
#undef RUN_ARON_MACRO

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    bool AronTypeJSONWriter::write##upperType##Type() \
{ \
    ss << "{"; \
    writeNewLineWithIndentIncrease(); \
    ss << "\"Type\"" << ": \"" << std::string(#upperType) << "\","; \
    writeNewLineWithIndentDecrease(); \
    ss << "},"; \
    writeNewLine(); \
    \
    return true; \
}

HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO

bool AronTypeJSONWriter::writeKey(const std::string& k)
{
    ss << k << ": ";
    return true;
}

bool AronTypeJSONWriter::writeObjectName(const std::string& n)
{
    ss << "Name: \"" << n << "\",";
    writeNewLine();
    return true;
}

}
}
}
