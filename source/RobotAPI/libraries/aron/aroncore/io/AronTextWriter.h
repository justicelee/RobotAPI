/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <stack>
#include <sstream>

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/io/AronClassWriter.h>

namespace armarx
{
    namespace aron
    {
        namespace io
        {
            class AronTextWriter;
            typedef std::shared_ptr<AronTextWriter> AronTextWriterPtr;

            class AronTextWriter :
                virtual public AronClassWriter<std::string>
            {
            public:
                AronTextWriter() :
                    level(0),
                    indentChars("    ")
                {

                }

                AronTextWriter(int level, const std::string& indentChars) :
                    level(level),
                    indentChars(indentChars)
                {

                }

                virtual std::string getResult() const override
                {
                    return ss.str();
                }

            protected:
                void increaseIndent()
                {
                    level++;
                }

                void decreaseIndent()
                {
                    if (level == 0)
                    {
                        return;
                    }
                    level--;
                }

                void writeNewLine()
                {
                    ss << "\n";
                    indent();
                }

                void writeNewLineWithIndentIncrease()
                {
                    ss << "\n";
                    increaseIndent();
                    indent();
                }

                void writeNewLineWithIndentDecrease()
                {
                    ss << "\n";
                    decreaseIndent();
                    indent();
                }

                void indent()
                {
                    for (unsigned int i = 0; i < level; ++i)
                    {
                        ss << indentChars;
                    }
                }

            protected:
                std::stringstream ss;
                unsigned int level;
                std::string indentChars;
            };
        }
    }
}
