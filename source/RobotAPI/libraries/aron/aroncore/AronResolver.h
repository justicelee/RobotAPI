/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <typeindex>
#include <typeinfo>

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronException.h>
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/AronConfig.h>
#include <RobotAPI/libraries/aron/aroncore/AronDescriptor.h>

namespace armarx
{
    namespace aron
    {
        class AronResolver
        {
        private:  // disallow creating this object. static use only
            AronResolver() = default;

        public:
            static AronTypeDescriptor GetTypeDescriptorForAronType(const type::AronTypePtr&);

            static AronTypeDescriptor GetTypeDescriptorForAronTypeId(const std::type_index&);

            static AronDataDescriptor GetTypeDescriptorForAronData(const data::AronDataPtr&);

            static AronDataDescriptor GetTypeDescriptorForAronDataId(const std::type_index&);

            static bool TypesAreCorresponding(const AronTypeDescriptor&, const AronDataDescriptor&);
        };
    }
}
