/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <typeindex>
#include <typeinfo>

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/AronConfig.h>

namespace armarx
{
    namespace aron
    {
        enum AronTypeDescriptor
        {
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    eAron##upperType##Type,

            HANDLE_ALL_ARON_TYPES
#undef RUN_ARON_MACRO
        };

        enum AronDataDescriptor
        {
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    eAron##upperType,

            HANDLE_ALL_ARON_DATA
#undef RUN_ARON_MACRO
        };
    }
}
