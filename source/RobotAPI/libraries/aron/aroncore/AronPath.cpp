/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "AronPath.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronException.h>

namespace armarx
{
    namespace aron
    {
        AronPath::AronPath() :
            rootIdentifier("\\"),
            delimeter("->")
        {

        }

        AronPath::AronPath(const std::string& s, const std::string& d):
            rootIdentifier(s),
            delimeter(d)
        {

        }

        AronPath::AronPath(const AronPath& p) :
            rootIdentifier(p.getRootIdentifier()),
            delimeter(p.getDelimeter()),
            path(p.getPath())
        {

        }

        AronPath::AronPath(const AronPath& p, const std::string& s) :
            AronPath(p)
        {
            appendPath(s);
        }

        AronPath::AronPath(const AronPath& p, const std::string& s, const std::string& s2) :
            AronPath(p)
        {
            appendPath(s);
            appendPath(s2);
        }

        void AronPath::setRootIdentifier(const std::string& s)
        {
            rootIdentifier = s;
        }

        std::string AronPath::getRootIdentifier() const
        {
            return rootIdentifier;
        }

        void AronPath::setDelimeter(const std::string& d)
        {
            delimeter = d;
        }

        std::string AronPath::getDelimeter() const
        {
            return delimeter;
        }

        void AronPath::appendPath(const std::string& str)
        {
            path.push_back(str);
        }

        std::vector<std::string> AronPath::getPath() const
        {
            return path;
        }

        std::string AronPath::getLastElement() const
        {
            if (!path.size())
            {
                throw LocalException("AronNavigatorPath: Try to access last element of empty vector.");
            }
            return path.back();
        }

        std::string AronPath::toString() const
        {
            std::stringstream ss;
            ss << rootIdentifier;
            for (const std::string& s : path)
            {
                ss << delimeter << s;
            }
            return ss.str();
        }
    }
}
