/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/core/demangle.hpp>

// Header
#include "AronResolver.h"

namespace armarx
{
    namespace aron
    {
        AronTypeDescriptor AronResolver::GetTypeDescriptorForAronType(const type::AronTypePtr& a)
        {
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    { \
        type::Aron##upperType##TypePtr aron = type::Aron##upperType##TypePtr::dynamicCast(a); \
        if(aron.get() != nullptr) \
        { \
            return eAron##upperType##Type; \
        } \
    }
            HANDLE_ALL_ARON_TYPES
#undef RUN_ARON_MACRO
            throw exception::AronInvalidAbstractTypeException("AronTypeResolver", "GetTypeForAronAbstractType", "Could not cast AronAbstractType to a valid type.");
        }

        AronTypeDescriptor AronResolver::GetTypeDescriptorForAronTypeId(const std::type_index& a)
        {
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    { \
        if(a == std::type_index(typeid(type::Aron##upperType##Type))) \
        { \
            return eAron##upperType##Type; \
        } \
    }
            HANDLE_ALL_ARON_TYPES
#undef RUN_ARON_MACRO
            throw exception::AronInvalidAbstractTypeException("AronTypeResolver", "GetTypeForAronTypeId", "Could not cast AronAbstractTypeId to a valid type. The typeid was " + boost::core::demangle(a.name()));
        }

        AronDataDescriptor AronResolver::GetTypeDescriptorForAronData(const data::AronDataPtr& a)
        {
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    { \
        data::Aron##upperType##Ptr aron = data::Aron##upperType##Ptr::dynamicCast(a); \
        if(aron.get() != nullptr) \
        { \
            return eAron##upperType; \
        } \
    }
            HANDLE_ALL_ARON_DATA
#undef RUN_ARON_MACRO
            throw exception::AronInvalidDataException("AronTypeResolver", "GetTypeForAronData", "Could not cast AronData to a valid type.");
        }

        AronDataDescriptor AronResolver::GetTypeDescriptorForAronDataId(const std::type_index& a)
        {
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    { \
        if(a == std::type_index(typeid(data::Aron##upperType))) \
        { \
            return eAron##upperType; \
        } \
    }
            HANDLE_ALL_ARON_DATA
#undef RUN_ARON_MACRO
            throw exception::AronInvalidDataException("AronTypeResolver", "GetTypeForAronDataTypeId", "Could not cast AronDataTypeId to a valid type. The typeid was " + boost::core::demangle(a.name()));
        }

        bool AronResolver::TypesAreCorresponding(const AronTypeDescriptor& t, const AronDataDescriptor& d)
        {
#define RUN_ARON_MACRO(typeUpperType, typeLowerType, typeCapsType, dataUpperType, dataLowerType, dataCapsType) \
    if(t == AronTypeDescriptor::eAron##typeUpperType##Type && d == AronDataDescriptor::eAron##dataUpperType) \
    { \
        return true; \
    }

            HANDLE_ALL_CORRESPONDING
#undef RUN_ARON_MACRO
            return false;
        }
    }
}
