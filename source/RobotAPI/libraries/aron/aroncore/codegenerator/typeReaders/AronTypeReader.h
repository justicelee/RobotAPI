/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <filesystem>

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronObjectTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/AronReaderInfo.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/AronWriterInfo.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/AronGenerateTypeInfo.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace typereader
            {
                template <typename Input>
                class AronTypeReader;

                template <typename Input>
                using AronTypeReaderPtr = std::shared_ptr<AronTypeReader<Input>>;

                template <typename Input>
                class AronTypeReader
                {
                public:
                    AronTypeReader() = default;

                    virtual void parseFile(const std::string& filename) = 0;
                    virtual void parseFile(const std::filesystem::path& file) = 0;

                    std::vector<std::string> getCodeIncludes() const
                    {
                        return codeIncludes;
                    }
                    std::vector<std::string> getAronIncludes() const
                    {
                        return aronIncludes;
                    }
                    std::vector<AronWriterInfoPtr> getWriters() const
                    {
                        return writers;
                    }
                    std::vector<AronReaderInfoPtr> getReaders() const
                    {
                        return readers;
                    }
                    std::vector<AronGenerateTypeInfoPtr> getGenerateTypes() const
                    {
                        return generateTypes;
                    }

                protected:
                    std::vector<std::string> alreadyParsedXMLFiles;

                    std::vector<std::string> codeIncludes;
                    std::vector<std::string> aronIncludes;
                    std::vector<AronReaderInfoPtr> readers;
                    std::vector<AronWriterInfoPtr> writers;
                    std::vector<AronGenerateTypeInfoPtr> generateTypes;
                };
            }
        }
    }
}
