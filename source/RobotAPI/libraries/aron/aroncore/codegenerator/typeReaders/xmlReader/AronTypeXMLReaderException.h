/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronException.h>

namespace armarx
{
    namespace aron
    {
        namespace exception
        {
            /////////////////////////////////////////
            /// AronType XML Reader Exception ///////
            /////////////////////////////////////////
            class AronXMLTagNotDefinedException :
                public exception::AronException
            {
            public:
                AronXMLTagNotDefinedException() = delete;
                AronXMLTagNotDefinedException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronXMLAttributeNotDefinedException :
                public exception::AronException
            {
            public:
                AronXMLAttributeNotDefinedException() = delete;
                AronXMLAttributeNotDefinedException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronXMLTagDefinedTwiceException :
                public exception::AronException
            {
            public:
                AronXMLTagDefinedTwiceException() = delete;
                AronXMLTagDefinedTwiceException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronUnexpectedXMLTagFoundException :
                public exception::AronException
            {
            public:
                AronUnexpectedXMLTagFoundException() = delete;
                AronUnexpectedXMLTagFoundException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronUnexpectedXMLAttributeFoundException :
                public exception::AronException
            {
            public:
                AronUnexpectedXMLAttributeFoundException() = delete;
                AronUnexpectedXMLAttributeFoundException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };
        }
    }
}
