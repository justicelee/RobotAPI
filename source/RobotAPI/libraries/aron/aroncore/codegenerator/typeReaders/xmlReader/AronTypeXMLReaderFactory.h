/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <map>
#include <vector>
#include <stack>

// Base Class
#include <RobotAPI/libraries/aron/aroncore/codegenerator/typeReaders/AronTypeReader.h>

// ArmarX
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/AronNavigatorFactory.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronAllTypeNavigators.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/AronGenerateTypeInfo.h>

using namespace std;

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace typereader
            {
                class AronTypeXMLReaderFactory;
                typedef std::shared_ptr<AronTypeXMLReaderFactory> AronTypeXMLReaderFactoryPtr;

                class AronTypeXMLReaderFactory :
                    virtual public navigator::AronNavigatorFactory<RapidXmlReaderNode, typenavigator::AronTypeNavigatorPtr>
                {
                public:
                    AronTypeXMLReaderFactory() = default;

                    virtual typenavigator::AronTypeNavigatorPtr create(const RapidXmlReaderNode&, const AronPath&) const override;
                    virtual typenavigator::AronTypeNavigatorPtr createSpecific(const RapidXmlReaderNode&, const AronPath&) const override = 0;

                    static std::map<std::string, AronGenerateTypeInfoPtr> GetAllKnownGeneratedPublicObjects();

                protected:
                    static typenavigator::AronTypeNavigatorPtr ResolveTypename(const std::string&);

                protected:
                    static std::stack<AronGenerateTypeInfoPtr> TheObjectWeAreGoingToGenerateNowStack;
                    static std::map<std::string, AronGenerateTypeInfoPtr> AllKnownGeneratedPublicObjects;

                private:
                    static const std::map<std::string, AronTypeXMLReaderFactoryPtr> Factories;
                };

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    class Aron##upperType##TypeXMLReaderFactory : \
        virtual public AronTypeXMLReaderFactory \
    { \
    public: \
        Aron##upperType##TypeXMLReaderFactory() = default; \
        virtual typenavigator::AronTypeNavigatorPtr createSpecific(const RapidXmlReaderNode&, const AronPath&) const override; \
    };

                HANDLE_CONTAINER_TYPES
                HANDLE_COMPLEX_TYPES
                HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
            }
        }
    }
}
