/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <map>

// ArmarX
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/libraries/aron/aroncore/AronConcepts.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronObjectTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronListTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronPrimitiveTypeNavigator.h>


namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace typereader
            {
                class AronTypeXMLReaderData
                {
                public:
                    // Definition tags
                    const static std::string ARON_TYPE_DEFINITION_TAG;

                    const static std::string ARON_CODE_INCLUDES_TAG;
                    const static std::string ARON_ARON_INCLUDES_TAG;
                    const static std::string ARON_GENERATE_TYPES_TAG;

                    const static std::string ARON_INCLUDE_TAG;
                    const static std::string ARON_GENERATE_TYPE_TAG;

                    // Attribute names
                    const static std::string ARON_METHOD_ATTRIBUTE_NAME;
                    const static std::string ARON_RETURN_ATTRIBUTE_NAME;
                    const static std::string ARON_ARGUMENT_TYPE_ATTRIBUTE_NAME;
                    const static std::string ARON_INCLUDE_ATTRIBUTE_NAME;
                    const static std::string ARON_READER_ATTRIBUTE_NAME;
                    const static std::string ARON_WRITER_ATTRIBUTE_NAME;
                    const static std::string ARON_EXTENDS_ATTRIBUTE_NAME;
                    const static std::string ARON_NAME_ATTRIBUTE_NAME;
                    const static std::string ARON_KEY_ATTRIBUTE_NAME;
                    const static std::string ARON_TYPE_ATTRIBUTE_NAME;
                    const static std::string ARON_WIDTH_ATTRIBUTE_NAME;
                    const static std::string ARON_HEIGHT_ATTRIBUTE_NAME;

                    // Second level tags. Only important if in specific top level tag
                    const static std::string ARON_OBJECT_CHILD_TAG;
                    const static std::string ARON_TYPE_TAG;
                    const static std::string ARON_DIMENSIONS_TAG;
                    const static std::string ARON_DIMENSION_TAG;

                    // Top Level type tags
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    const static std::string ARON_GENERATE_##capsType##_TAG;

                    HANDLE_ALL_ARON_TYPES
#undef RUN_ARON_MACRO

                    static void EnforceAttribute(const RapidXmlReaderNode& node, const std::string& att);
                    static bool HasAttribute(const RapidXmlReaderNode& node, const std::string& att);
                    static std::string GetAttribute(const RapidXmlReaderNode& node, const std::string& att);
                    static std::string GetAttributeWithDefault(const RapidXmlReaderNode& node, const std::string& att, const std::string def);
                    static bool HasTagName(const RapidXmlReaderNode& node, const std::string& name);
                    static void EnforceTagName(const RapidXmlReaderNode& node, const std::string& name);
                    static std::string GetTagName(const RapidXmlReaderNode& node);
                    static bool CheckMinChildSize(const RapidXmlReaderNode& node, const size_t size);
                    static bool CheckMaxChildSize(const RapidXmlReaderNode& node, const size_t size);
                    static bool CheckExactChildSize(const RapidXmlReaderNode& node, const size_t size);
                    static void EnforceChildSize(const RapidXmlReaderNode& node, const size_t size);
                };
            }
        }
    }
}
