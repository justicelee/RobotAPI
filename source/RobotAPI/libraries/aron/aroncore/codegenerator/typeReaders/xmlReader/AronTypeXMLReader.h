/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <filesystem>

// Base Class
#include <RobotAPI/libraries/aron/aroncore/codegenerator/typeReaders/AronTypeReader.h>

// ArmarX
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronObjectTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/typeReaders/xmlReader/AronTypeXMLReaderFactory.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace typereader
            {
                class AronTypeXMLReader;
                typedef std::shared_ptr<AronTypeXMLReader> AronTypeXMLReaderPtr;

                class AronTypeXMLReader :
                    virtual public codegeneration::typereader::AronTypeReader<std::string>
                {
                public:
                    AronTypeXMLReader() = default;

                    virtual void parseFile(const std::string&) override;
                    virtual void parseFile(const std::filesystem::path&) override;

                private:
                    void parse(const RapidXmlReaderPtr& node);

                    std::string readCppInclude(const RapidXmlReaderNode& node) const;
                    std::string readAronInclude(const RapidXmlReaderNode& node) const;

                    typenavigator::AronObjectTypeNavigatorPtr readGenerateType(const RapidXmlReaderNode& node) const;

                private:
                    AronObjectTypeXMLReaderFactory factory;
                };
            }
        }
    }
}
