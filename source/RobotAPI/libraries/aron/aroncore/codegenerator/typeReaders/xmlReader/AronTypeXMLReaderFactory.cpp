/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronTypeXMLReaderFactory.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/codegenerator/typeReaders/xmlReader/AronTypeXMLReaderException.h>

#include <RobotAPI/libraries/aron/aroncore/codegenerator/typeReaders/xmlReader/AronTypeXMLData.h>

namespace armarx
{
namespace aron
{
namespace codegeneration
{
namespace typereader
{
std::stack<AronGenerateTypeInfoPtr> AronTypeXMLReaderFactory::TheObjectWeAreGoingToGenerateNowStack = {};
std::map<std::string, AronGenerateTypeInfoPtr> AronTypeXMLReaderFactory::AllKnownGeneratedPublicObjects = {};

const std::map<std::string, AronTypeXMLReaderFactoryPtr> AronTypeXMLReaderFactory::Factories =
{
    #define RUN_ARON_MACRO(upperType, lowerType, capsType) \
{AronTypeXMLReaderData::ARON_GENERATE_##capsType##_TAG, AronTypeXMLReaderFactoryPtr(new Aron##upperType##TypeXMLReaderFactory())}, \

    HANDLE_CONTAINER_TYPES
    HANDLE_COMPLEX_TYPES
    HANDLE_PRIMITIVE_TYPES
    #undef RUN_ARON_MACRO
};

typenavigator::AronTypeNavigatorPtr AronTypeXMLReaderFactory::create(const RapidXmlReaderNode& node, const AronPath& path) const
{
    const std::string tag = boost::algorithm::to_lower_copy(node.name());
    if (tag == "")
    {
        throw exception::AronXMLTagNotDefinedException("AronTypeXMLReaderFactory", "create", "An node is empty. Cannot find facory for empty nodes.");
    }

    auto factory_iterator = Factories.find(tag);
    if (factory_iterator != Factories.end())
    {
        return factory_iterator->second->createSpecific(node, path);
    }

    // Search for known objects
    return ResolveTypename(tag);
}

std::map<std::string, AronGenerateTypeInfoPtr> AronTypeXMLReaderFactory::GetAllKnownGeneratedPublicObjects()
{
    return AllKnownGeneratedPublicObjects;
}

typenavigator::AronTypeNavigatorPtr AronTypeXMLReaderFactory::ResolveTypename(const std::string& name)
{
    const auto public_it = AllKnownGeneratedPublicObjects.find(name);
    if(public_it != AllKnownGeneratedPublicObjects.end())
    {
        return public_it->second->correspondingObjectType;
    }

    throw armarx::LocalException("AronTypeXMLReaderFactory: Typename '"+name+"' not found!! Please note that you cant make use of neted objects yet!");
}

// Object type
typenavigator::AronTypeNavigatorPtr AronObjectTypeXMLReaderFactory::createSpecific(const RapidXmlReaderNode& node, const AronPath& path) const
{
    AronTypeXMLReaderData::EnforceAttribute(node, AronTypeXMLReaderData::ARON_NAME_ATTRIBUTE_NAME);
    const std::string extends = AronTypeXMLReaderData::GetAttributeWithDefault(node, AronTypeXMLReaderData::ARON_EXTENDS_ATTRIBUTE_NAME, "");
    const std::string name = AronTypeXMLReaderData::GetAttribute(node, AronTypeXMLReaderData::ARON_NAME_ATTRIBUTE_NAME);

    bool object_is_nested = TheObjectWeAreGoingToGenerateNowStack.size() > 0;

    // set the new object
    typenavigator::AronObjectTypeNavigatorPtr aronObjectType = typenavigator::AronObjectTypeNavigatorPtr(new typenavigator::AronObjectTypeNavigator(path));
    if (object_is_nested)
    {
        if (name.rfind("::") != string::npos)
        {
            throw armarx::LocalException("AronObjectTypeXMLReaderFactory: Namespaces are not allowed in nested classes!");
        }
    }
    aronObjectType->setName(name);

    AronGenerateTypeInfoPtr theObjectWeAreGoingToProduceNow = AronGenerateTypeInfoPtr(new AronGenerateTypeInfo());
    theObjectWeAreGoingToProduceNow->typeName = name;
    theObjectWeAreGoingToProduceNow->correspondingObjectType = aronObjectType;

    if (extends != "")
    {
        map<std::string, AronGenerateTypeInfoPtr>::iterator it = AllKnownGeneratedPublicObjects.find(extends);
        if (it != AllKnownGeneratedPublicObjects.end())
        {
            AronGenerateTypeInfoPtr prodObj = it->second;
            aronObjectType->setExtends(prodObj->correspondingObjectType);
        }
        else
        {
            throw exception::AronTypeNotFoundException("AronObjectTypeXMLReaderFactory", "createSpecific", "An extends name could not be found. Perhaps you need to change the order of your types or you have to add another UseType!");
        }
    }

    TheObjectWeAreGoingToGenerateNowStack.push(theObjectWeAreGoingToProduceNow);
    for (const RapidXmlReaderNode& objectChild : node.nodes())
    {
        AronTypeXMLReaderData::EnforceTagName(objectChild, AronTypeXMLReaderData::ARON_OBJECT_CHILD_TAG);
        AronTypeXMLReaderData::EnforceChildSize(objectChild, 1);

        AronTypeXMLReaderData::EnforceAttribute(objectChild, AronTypeXMLReaderData::ARON_KEY_ATTRIBUTE_NAME);
        const std::string key = objectChild.attribute_value(AronTypeXMLReaderData::ARON_KEY_ATTRIBUTE_NAME.c_str());

        std::vector<RapidXmlReaderNode> children = objectChild.nodes();
        typenavigator::AronTypeNavigatorPtr childNavigator = create(children[0], AronPath(path, key));
        aronObjectType->addAcceptedType(key, childNavigator);
    }

    // remove current item from stack
    TheObjectWeAreGoingToGenerateNowStack.pop();

    if (object_is_nested)
    {
        // We are in a local nested class since there is a parent class in stack
        AronGenerateTypeInfoPtr parent = TheObjectWeAreGoingToGenerateNowStack.top();
        parent->nestedObjects.insert(std::make_pair(theObjectWeAreGoingToProduceNow->typeName, theObjectWeAreGoingToProduceNow));
    }
    else
    {
        // We are a public top level class. Add to AllProducedPublicObjects
        AllKnownGeneratedPublicObjects.insert(std::make_pair(theObjectWeAreGoingToProduceNow->typeName, theObjectWeAreGoingToProduceNow));
    }

    return aronObjectType;
}

// List type (List)
typenavigator::AronTypeNavigatorPtr AronListTypeXMLReaderFactory::createSpecific(const RapidXmlReaderNode& node, const AronPath& path) const
{
    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_GENERATE_LIST_TAG);

    typenavigator::AronListTypeNavigatorPtr list(new typenavigator::AronListTypeNavigator(path));
    AronTypeXMLReaderData::EnforceChildSize(node, 1);
    std::vector<RapidXmlReaderNode> listTypeNodeChildren = node.nodes();
    const RapidXmlReaderNode typeNode = listTypeNodeChildren[0];
    typenavigator::AronTypeNavigatorPtr type = create(typeNode, AronPath(path, "type"));

    list->setAcceptedType(type);
    return list;
}

// List type (Dict)
typenavigator::AronTypeNavigatorPtr AronDictTypeXMLReaderFactory::createSpecific(const RapidXmlReaderNode& node, const AronPath& path) const
{
    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_GENERATE_DICT_TAG);

    typenavigator::AronDictTypeNavigatorPtr dict(new typenavigator::AronDictTypeNavigator(path));
    AronTypeXMLReaderData::EnforceChildSize(node, 1);
    std::vector<RapidXmlReaderNode> dictTypeNodeChildren = node.nodes();
    const RapidXmlReaderNode typeNode = dictTypeNodeChildren[0];
    typenavigator::AronTypeNavigatorPtr type = create(typeNode, AronPath(path, "type"));
    dict->setAcceptedType(type);

    return dict;
}

// List types (Tuple)
typenavigator::AronTypeNavigatorPtr AronTupleTypeXMLReaderFactory::createSpecific(const RapidXmlReaderNode& node, const AronPath& path) const
{
    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_GENERATE_TUPLE_TAG);
    std::vector<RapidXmlReaderNode> nodeChildren = node.nodes();

    if (nodeChildren.size() < 2)
    {
        throw LocalException("AronTupleTypeXMLReaderFactory: A tuple must have at least two types.");
    }

    typenavigator::AronTupleTypeNavigatorPtr tuple(new typenavigator::AronTupleTypeNavigator(path));
    unsigned int i = 0;
    for (const RapidXmlReaderNode& tupleTypeDeclarationNode : nodeChildren)
    {
        AronTypeXMLReaderData::EnforceChildSize(tupleTypeDeclarationNode, 1);

        std::vector<RapidXmlReaderNode> typeNodeChildren = tupleTypeDeclarationNode.nodes();
        const RapidXmlReaderNode typeNode = typeNodeChildren[0];

        typenavigator::AronTypeNavigatorPtr type = create(typeNode, AronPath(path, "<" + std::to_string(i++) + ">"));
        tuple->addAcceptedType(type);
    }
    return tuple;
}

// Complex type (IVTCByteImage)
typenavigator::AronTypeNavigatorPtr AronIVTCByteImageTypeXMLReaderFactory::createSpecific(const RapidXmlReaderNode& node, const AronPath& path) const
{
    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_GENERATE_IVT_CBYTE_IMAGE_TAG);
    AronTypeXMLReaderData::EnforceAttribute(node, AronTypeXMLReaderData::ARON_WIDTH_ATTRIBUTE_NAME);
    AronTypeXMLReaderData::EnforceAttribute(node, AronTypeXMLReaderData::ARON_HEIGHT_ATTRIBUTE_NAME);
    AronTypeXMLReaderData::EnforceAttribute(node, AronTypeXMLReaderData::ARON_TYPE_ATTRIBUTE_NAME);

    int width = std::stoi(node.attribute_value(AronTypeXMLReaderData::ARON_WIDTH_ATTRIBUTE_NAME.c_str()));
    int height = std::stoi(node.attribute_value(AronTypeXMLReaderData::ARON_HEIGHT_ATTRIBUTE_NAME.c_str()));
    std::string type = node.attribute_value(AronTypeXMLReaderData::ARON_TYPE_ATTRIBUTE_NAME.c_str());;


    typenavigator::AronIVTCByteImageTypeNavigatorPtr complex(new typenavigator::AronIVTCByteImageTypeNavigator(path));
    complex->setWidth(width);
    complex->setHeight(height);
    complex->setUsedType(type);
    return complex;
}

// Complex type (EigenMatrix)
typenavigator::AronTypeNavigatorPtr AronEigenMatrixTypeXMLReaderFactory::createSpecific(const RapidXmlReaderNode& node, const AronPath& path) const
{
    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_GENERATE_EIGEN_MATRIX_TAG);
    throw LocalException("AronTupleTypeXMLReaderFactory: Not implemented yet. The path was: " + path.toString());
}

// Complex type (OpenCVMat)
typenavigator::AronTypeNavigatorPtr AronOpenCVMatTypeXMLReaderFactory::createSpecific(const RapidXmlReaderNode& node, const AronPath& path) const
{
    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_GENERATE_OPENCV_MAT_TAG);
    throw LocalException("AronTupleTypeXMLReaderFactory: Not implemented yet. The path was: " + path.toString());
}

// Complex type (PCLPointcloud)
typenavigator::AronTypeNavigatorPtr AronPCLPointcloudTypeXMLReaderFactory::createSpecific(const RapidXmlReaderNode& node, const AronPath& path) const
{
    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_GENERATE_PCL_POINTCLOUD_TAG);
    throw LocalException("AronTupleTypeXMLReaderFactory: Not implemented yet. The path was: " + path.toString());
}

// Primitve Types
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    typenavigator::AronTypeNavigatorPtr Aron##upperType##TypeXMLReaderFactory::createSpecific(const RapidXmlReaderNode&, const AronPath& path) const \
{ \
    return typenavigator::AronTypeNavigatorPtr(new typenavigator::Aron##upperType##TypeNavigator(path)); \
}

HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
}
}
}
}
