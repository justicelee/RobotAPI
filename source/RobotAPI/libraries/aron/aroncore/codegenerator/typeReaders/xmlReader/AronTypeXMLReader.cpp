/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL


// Header
#include "AronTypeXMLReader.h"

// ArmarX
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/libraries/aron/aroncore/codegenerator/typeReaders/xmlReader/AronTypeXMLReaderException.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/typeReaders/xmlReader/AronTypeXMLData.h>
//#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTypeNavigatorFactory.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace typereader
            {
                void AronTypeXMLReader::parseFile(const std::string& filename)
                {
                    RapidXmlReaderPtr reader = RapidXmlReader::FromFile(filename);
                    parse(reader);
                }

                void AronTypeXMLReader::parseFile(const std::filesystem::path& file)
                {
                    RapidXmlReaderPtr reader = RapidXmlReader::FromFile(file.string());
                    parse(reader);
                }

                // private method reading nodes
                void AronTypeXMLReader::parse(const RapidXmlReaderPtr& reader)
                {
                    RapidXmlReaderNode root = reader->getRoot();

                    // Check Header
                    AronTypeXMLReaderData::EnforceTagName(root, AronTypeXMLReaderData::ARON_TYPE_DEFINITION_TAG);

                    AronTypeXMLReaderData::CheckMaxChildSize(root, 4);
                    std::vector<RapidXmlReaderNode> children = root.nodes();

                    int cpp_includes_index = -1;
                    int include_aron_file_index = -1;
                    int generate_types_index = -1;

                    int index = 0;
                    for (const auto& child : children)
                    {
                        if (AronTypeXMLReaderData::HasTagName(child, AronTypeXMLReaderData::ARON_CODE_INCLUDES_TAG))
                        {
                            if (cpp_includes_index != -1)
                            {
                                throw exception::AronXMLTagDefinedTwiceException("AronTypeXMLReader", "parse", "Already found an include tag. Please check the xml file.");
                            }
                            else
                            {
                                cpp_includes_index = index;
                            }
                        }
                        else if (AronTypeXMLReaderData::HasTagName(child, AronTypeXMLReaderData::ARON_ARON_INCLUDES_TAG))
                        {
                            if (include_aron_file_index != -1)
                            {
                                throw exception::AronXMLTagDefinedTwiceException("AronTypeXMLReader", "parse", "Already found an use type tag. Please check the xml file.");
                            }
                            else
                            {
                                include_aron_file_index = index;
                            }
                        }
                        else if (AronTypeXMLReaderData::HasTagName(child, AronTypeXMLReaderData::ARON_GENERATE_TYPES_TAG))
                        {
                            if (generate_types_index != -1)
                            {
                                throw exception::AronXMLTagDefinedTwiceException("AronTypeXMLReader", "parse", "Already found an generate type tag. Please check the xml file.");
                            }
                            else
                            {
                                generate_types_index = index;
                            }
                        }
                        else
                        {
                            throw exception::AronUnexpectedXMLTagFoundException("AronTypeXMLReader", "parse", "Found the unexpected xml tag: " + child.name());
                        }
                        index++;
                    }

                    // 1. Check includes
                    if (cpp_includes_index != -1)
                    {
                        std::vector<RapidXmlReaderNode> includes = children[cpp_includes_index].nodes();
                        for (const auto& include : includes)
                        {
                            this->codeIncludes.push_back(readCppInclude(include));
                        }
                    }

                    // 2. Check AronIncludes
                    if (include_aron_file_index != -1)
                    {
                        for (const auto& aronInclude : children[include_aron_file_index].nodes())
                        {
                            // right now unused
                            this->aronIncludes.push_back(readAronInclude(aronInclude));
                        }
                    }

                    // 3. Check GenerateTypes
                    if (generate_types_index != -1)
                    {
                        for (const auto& generateType : children[generate_types_index].nodes())
                        {
                            // ugly workaround
                            const typenavigator::AronObjectTypeNavigatorPtr aronObj = readGenerateType(generateType);
                            const std::map<std::string, AronGenerateTypeInfoPtr> generatedObjects = factory.GetAllKnownGeneratedPublicObjects();
                            generateTypes.push_back(generatedObjects.at(aronObj->getName()));
                        }
                    }
                }

                std::string AronTypeXMLReader::readCppInclude(const RapidXmlReaderNode& node) const
                {
                    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_INCLUDE_TAG);
                    AronTypeXMLReaderData::EnforceAttribute(node, AronTypeXMLReaderData::ARON_INCLUDE_ATTRIBUTE_NAME);
                    const std::string include = node.attribute_value(AronTypeXMLReaderData::ARON_INCLUDE_ATTRIBUTE_NAME.c_str());
                    return include;
                }

                std::string AronTypeXMLReader::readAronInclude(const RapidXmlReaderNode& node) const
                {
                    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_INCLUDE_TAG);

                    const std::string xmlinclude = AronTypeXMLReaderData::GetAttribute(node, AronTypeXMLReaderData::ARON_INCLUDE_ATTRIBUTE_NAME);

                    // parse parent xml file and add objects to alreday known
                    AronTypeXMLReader anotherReader;
                    anotherReader.parseFile(xmlinclude);

                    return xmlinclude;
                }

                typenavigator::AronObjectTypeNavigatorPtr AronTypeXMLReader::readGenerateType(const RapidXmlReaderNode& node) const
                {
                    AronTypeXMLReaderData::EnforceTagName(node, AronTypeXMLReaderData::ARON_GENERATE_TYPE_TAG);
                    return typenavigator::AronObjectTypeNavigator::DynamicCast(factory.createSpecific(node, AronPath()));

                }
            }
        }
    }
}
