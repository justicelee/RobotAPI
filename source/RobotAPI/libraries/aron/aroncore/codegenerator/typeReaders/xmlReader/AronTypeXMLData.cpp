/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL


// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronTypeXMLData.h"

// ArmarX
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/typeReaders/xmlReader/AronTypeXMLReaderException.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace typereader
            {
                const std::string AronTypeXMLReaderData::ARON_TYPE_DEFINITION_TAG = "arontypedefinition";

                const std::string AronTypeXMLReaderData::ARON_CODE_INCLUDES_TAG = "codeincludes";
                const std::string AronTypeXMLReaderData::ARON_ARON_INCLUDES_TAG = "aronincludes";
                const std::string AronTypeXMLReaderData::ARON_GENERATE_TYPES_TAG = "generatetypes";

                const std::string AronTypeXMLReaderData::ARON_INCLUDE_TAG = "include";
                const std::string AronTypeXMLReaderData::ARON_GENERATE_TYPE_TAG = "generatetype";

                const std::string AronTypeXMLReaderData::ARON_INCLUDE_ATTRIBUTE_NAME = "include";
                const std::string AronTypeXMLReaderData::ARON_METHOD_ATTRIBUTE_NAME = "method";
                const std::string AronTypeXMLReaderData::ARON_RETURN_ATTRIBUTE_NAME = "return";
                const std::string AronTypeXMLReaderData::ARON_ARGUMENT_TYPE_ATTRIBUTE_NAME = "argumenttype";
                const std::string AronTypeXMLReaderData::ARON_WRITER_ATTRIBUTE_NAME = "writer";
                const std::string AronTypeXMLReaderData::ARON_READER_ATTRIBUTE_NAME = "reader";
                const std::string AronTypeXMLReaderData::ARON_EXTENDS_ATTRIBUTE_NAME = "extends";
                const std::string AronTypeXMLReaderData::ARON_NAME_ATTRIBUTE_NAME = "name";
                const std::string AronTypeXMLReaderData::ARON_TYPE_ATTRIBUTE_NAME = "type";
                const std::string AronTypeXMLReaderData::ARON_KEY_ATTRIBUTE_NAME = "key";
                const std::string AronTypeXMLReaderData::ARON_WIDTH_ATTRIBUTE_NAME = "width";
                const std::string AronTypeXMLReaderData::ARON_HEIGHT_ATTRIBUTE_NAME = "height";

                const std::string AronTypeXMLReaderData::ARON_OBJECT_CHILD_TAG = "objectchild";
                const std::string AronTypeXMLReaderData::ARON_TYPE_TAG = "type";
                const std::string AronTypeXMLReaderData::ARON_DIMENSIONS_TAG = "dimensions";
                const std::string AronTypeXMLReaderData::ARON_DIMENSION_TAG = "dimension";

                // Top Level type tags
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    const std::string AronTypeXMLReaderData::ARON_GENERATE_##capsType##_TAG = #lowerType;

                HANDLE_ALL_ARON_TYPES
#undef RUN_ARON_MACRO

                void AronTypeXMLReaderData::EnforceAttribute(const RapidXmlReaderNode& node, const std::string& att)
                {
                    if (!HasAttribute(node, att))
                    {
                        throw exception::AronXMLAttributeNotDefinedException("AronTypeXMLReaderData", "EnforceAttribute", "A <" + node.name() + ">-tag does not have the correct attribute. Expected: " + att);
                    }
                }

                bool AronTypeXMLReaderData::HasAttribute(const RapidXmlReaderNode& node, const std::string& att)
                {
                    return node.has_attribute(att.c_str());
                }

                std::string AronTypeXMLReaderData::GetAttribute(const RapidXmlReaderNode& node, const std::string& att)
                {
                    EnforceAttribute(node, att);
                    return node.attribute_value(att.c_str());
                }

                std::string AronTypeXMLReaderData::GetAttributeWithDefault(const RapidXmlReaderNode& node, const std::string& att, const std::string def)
                {
                    if (!(HasAttribute(node, att)))
                    {
                        return def;
                    }
                    return node.attribute_value(att.c_str());
                }

                bool AronTypeXMLReaderData::HasTagName(const RapidXmlReaderNode& node, const std::string& name)
                {
                    return (boost::algorithm::to_lower_copy(name) == boost::algorithm::to_lower_copy(node.name()));
                }

                void AronTypeXMLReaderData::EnforceTagName(const RapidXmlReaderNode& node, const std::string& name)
                {
                    if (!(HasTagName(node, name)))
                    {
                        throw exception::AronXMLTagNotDefinedException("AronTypeXMLReaderData", "EnforceTagName", "The node <" + node.name() + "> has the wrong tag. Expected: <" + name + ">");
                    }
                }

                std::string AronTypeXMLReaderData::GetTagName(const RapidXmlReaderNode& node)
                {
                    return boost::algorithm::to_lower_copy(node.name());
                }

                bool AronTypeXMLReaderData::CheckMinChildSize(const RapidXmlReaderNode& node, const size_t size)
                {
                    std::vector<RapidXmlReaderNode> children = node.nodes();
                    return children.size() >= size;
                }

                bool AronTypeXMLReaderData::CheckMaxChildSize(const RapidXmlReaderNode& node, const size_t size)
                {
                    std::vector<RapidXmlReaderNode> children = node.nodes();
                    return children.size() <= size;
                }

                bool AronTypeXMLReaderData::CheckExactChildSize(const RapidXmlReaderNode& node, const size_t size)
                {
                    std::vector<RapidXmlReaderNode> children = node.nodes();
                    return children.size() == size;
                }

                void AronTypeXMLReaderData::EnforceChildSize(const RapidXmlReaderNode& node, const size_t size)
                {
                    if (!AronTypeXMLReaderData::CheckExactChildSize(node, size))
                    {
                        throw LocalException("AronTypeXMLReaderData: The node <" + node.name() + "> has the wrong number of children. Expected: " + std::to_string(size) + " children but it has: " + std::to_string(node.nodes().size()) + ".");
                    }
                }
            }
        }
    }
}
