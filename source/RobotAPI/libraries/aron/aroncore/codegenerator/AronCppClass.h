/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

#include <RobotAPI/libraries/aron/aroncore/AronConcepts.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/AronDataReader.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/AronDataWriter.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronTypeIO/AronTypeReader.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronTypeIO/AronTypeWriter.h>

using namespace std;

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            class AronCppClass;
            typedef std::shared_ptr<AronCppClass> AronCppClassPtr;

            class AronCppClass
            {
            public:
                using PointerType = AronCppClassPtr;

            public:
                AronCppClass() = default;
                virtual void reset() = 0;
                virtual void initialize() = 0;
                virtual void read(io::AronDataReader& r) = 0;
                virtual void write(io::AronDataWriter& w) const = 0;
                virtual void writeType(io::AronTypeWriter& w) const = 0;
            };
        }
    }
}
