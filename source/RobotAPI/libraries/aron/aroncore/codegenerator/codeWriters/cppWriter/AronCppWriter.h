/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <set>
#include <map>
#include <vector>

// Parent class
#include <RobotAPI/libraries/aron/aroncore/codegenerator/codeWriters/AronCodeWriter.h>

// ArmarX
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronAllTypeNavigators.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/codeWriters/cppWriter/typeSerializers/AronAllCppSerializers.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                class AronTypeClassCppWriter;
                typedef std::shared_ptr<AronTypeClassCppWriter> AronConnectorCppWriterPtr;

                class AronTypeClassCppWriter :
                    virtual public AronCodeWriter
                {
                public:
                    AronTypeClassCppWriter() = delete;
                    AronTypeClassCppWriter(const std::string&, const std::vector<std::string>&);

                    virtual void generateTypeClasses(const std::vector<AronGenerateTypeInfoPtr>&) override;

                protected:
                    virtual void addToAronMethod() override;
                    virtual void addFromAronMethod() override;
                    virtual void addToAronTypeMethod() override;

                private:
                    void generateInnerTypeClasses(CppClassPtr& classToAdd, const std::map<std::string, AronGenerateTypeInfoPtr>& localGenerateTypes);
                    CppClassPtr setupCppClassFromObjectType(const typenavigator::AronObjectTypeNavigatorPtr&) const;

                private:
                };
            }
        }
    }
}
