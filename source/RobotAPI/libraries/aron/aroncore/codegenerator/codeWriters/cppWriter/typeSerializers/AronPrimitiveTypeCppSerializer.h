/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// Base Class
#include "AronTypeCppSerializer.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/AronConcepts.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronPrimitiveTypeNavigator.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    class Aron##upperType##TypeCppSerializer; \
    typedef std::shared_ptr<Aron##upperType##TypeCppSerializer> Aron##upperType##TypeCppSerializerPtr; \
    \
    class Aron##upperType##TypeCppSerializer : \
        virtual public AronTypeCppSerializer \
    { \
    public: \
        using PointerType = Aron##upperType##TypeCppSerializerPtr; \
        \
    public: \
        /* constructors */ \
        Aron##upperType##TypeCppSerializer(const typenavigator::Aron##upperType##TypeNavigatorPtr& e); \
        \
        /* virtual implementations */ \
        virtual std::vector<CppFieldPtr> getPublicVariableDeclarations(const std::string& name) const override; \
        virtual std::vector<std::pair<std::string, std::string>> getCtorInitializers(const std::string&) const override; \
        virtual CppBlockPtr getCtorBlock(const std::string&) const override; \
        virtual CppBlockPtr getResetBlock(const std::string& accessor) const override; \
        virtual CppBlockPtr getInitializeBlock(const std::string& accessor) const override; \
        virtual CppBlockPtr getWriteTypeBlock(const std::string& accessor) const override; \
        virtual CppBlockPtr getWriteBlock(const std::string& accessor) const override; \
        virtual CppBlockPtr getReadBlock(const std::string& accessor) const override; \
        CppBlockPtr getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const override; \
        \
    private: \
        /* members */ \
        typenavigator::Aron##upperType##TypeNavigatorPtr typenavigator; \
    };

                    HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
                }
            }
        }
    }
}
