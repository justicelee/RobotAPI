/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// STD/STL
#include <string>
#include <map>

// Header
#include "AronPrimitiveTypeCppSerializer.h"

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {

#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    /* constructors */ \
    Aron##upperType##TypeCppSerializer::Aron##upperType##TypeCppSerializer(const typenavigator::Aron##upperType##TypeNavigatorPtr& e) : \
        AronTypeCppSerializer(boost::core::demangle(typeid(lowerType).name()), boost::core::demangle(typeid(data::Aron##upperType).name()), boost::core::demangle(typeid(type::Aron##upperType##Type).name())), \
        typenavigator(e) \
    { \
        \
    } \
    \
    /* virtual implementations */ \
    std::vector<CppFieldPtr> Aron##upperType##TypeCppSerializer::getPublicVariableDeclarations(const std::string& name) const \
    { \
        CppFieldPtr field = CppFieldPtr(new CppField(getCppTypename(), name)); \
        return {field}; \
    } \
    \
    std::vector<std::pair<std::string, std::string>> Aron##upperType##TypeCppSerializer::getCtorInitializers(const std::string&) const \
    { \
        return {}; \
    } \
    \
    CppBlockPtr Aron##upperType##TypeCppSerializer::getCtorBlock(const std::string&) const \
    { \
        CppBlockPtr b = CppBlockPtr(new CppBlock()); \
        return b; \
    } \
    \
    CppBlockPtr Aron##upperType##TypeCppSerializer::getResetBlock(const std::string& accessor) const \
    { \
        CppBlockPtr b = CppBlockPtr(new CppBlock()); \
        b->addLine(accessor + " = {};"); \
        return b; \
    } \
    \
    CppBlockPtr Aron##upperType##TypeCppSerializer::getInitializeBlock(const std::string& accessor) const \
    { \
        CppBlockPtr b = CppBlockPtr(new CppBlock()); \
        b->addLine(accessor + " = {};"); \
        return b; \
    } \
    \
    CppBlockPtr Aron##upperType##TypeCppSerializer::getWriteTypeBlock(const std::string&) const \
    { \
        CppBlockPtr b = CppBlockPtr(new CppBlock()); \
        b->addLine("w.write" + std::string(#upperType) + "Type();"); \
        return b; \
    } \
    \
    CppBlockPtr Aron##upperType##TypeCppSerializer::getWriteBlock(const std::string& accessor) const \
    { \
        CppBlockPtr b = CppBlockPtr(new CppBlock()); \
        b->addLine("w.write" + std::string(#upperType) + "(" + accessor + ");"); \
        return b; \
    } \
    \
    CppBlockPtr Aron##upperType##TypeCppSerializer::getReadBlock(const std::string& accessor) const \
    { \
        CppBlockPtr b = CppBlockPtr(new CppBlock()); \
        b->addLine(accessor + " = r.read" + std::string(#upperType) + "();"); \
        return b; \
    } \
    \
    CppBlockPtr Aron##upperType##TypeCppSerializer::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const \
    { \
        CppBlockPtr b = CppBlockPtr(new CppBlock()); \
        b->addLine("if (not (" + accessor + " == " + otherInstanceAccessor + "))"); \
        b->addLine("\t return false;"); \
        return b; \
    }

                    HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
                }
            }
        }
    }
}
