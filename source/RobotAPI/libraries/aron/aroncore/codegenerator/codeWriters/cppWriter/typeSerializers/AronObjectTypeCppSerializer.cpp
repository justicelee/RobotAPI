/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronObjectTypeCppSerializer.h"


namespace armarx
{
namespace aron
{
namespace codegeneration
{
namespace classwriter
{
namespace cppSerializer
{

const std::string AronObjectTypeCppSerializer::EXTENDS_ITERATOR_ACCESSOR = "_base_class";

// constructors
AronObjectTypeCppSerializer::AronObjectTypeCppSerializer(const typenavigator::AronObjectTypeNavigatorPtr& e) :
    AronTypeCppSerializer(e->getName(), boost::core::demangle(typeid(data::AronDict).name()), boost::core::demangle(typeid(type::AronObjectType).name())),
    typenavigator(e)
{
    //AddObjectTypeToAllObjectTypesList(AronObjectTypeNavigatorPtr(this));
}

std::vector<CppFieldPtr> AronObjectTypeCppSerializer::getPublicVariableDeclarations(const std::string& name) const
{
    if (name == "") // object defintiion
    {
        std::vector<CppFieldPtr> fields;
        for (const auto& [key, member] : typenavigator->getAcceptedTypes())
        {
            auto member_s = FromAronTypeNaviagtorPtr(member);
            std::vector<CppFieldPtr> member_fields = member_s->getPublicVariableDeclarations(key);
            fields.insert(fields.end(), member_fields.begin(), member_fields.end());
        }
        return fields;
    }
    else // object usage
    {
        CppFieldPtr field = CppFieldPtr(new CppField(getCppTypename(), name));
        return {field};
    };
}

std::vector<std::pair<std::string, std::string>> AronObjectTypeCppSerializer::getCtorInitializers(const std::string&) const
{
    return {};
}

CppBlockPtr AronObjectTypeCppSerializer::getCtorBlock(const std::string&) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    return b;
}

CppBlockPtr AronObjectTypeCppSerializer::getResetBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    if (typenavigator->getExtends() != nullptr)
    {
        const auto extends_s = FromAronTypeNaviagtorPtr(typenavigator->getExtends());
        b->addLine(extends_s->getCppTypename() + "::reset();");
    }

    for (const auto& [key, child] : typenavigator->getAcceptedTypes())
    {
        auto child_s = FromAronTypeNaviagtorPtr(child);
        CppBlockPtr b2 = child_s->getResetBlock((accessor != "" ? (accessor + "." + key) : key));
        b->appendBlock(b2);
    }
    return b;
}

CppBlockPtr AronObjectTypeCppSerializer::getInitializeBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    if (typenavigator->getExtends() != nullptr)
    {
        const auto extends_s = FromAronTypeNaviagtorPtr(typenavigator->getExtends());
        b->addLine(extends_s->getCppTypename() + "::initialize();");
    }

    for (const auto& [key, child] : typenavigator->getAcceptedTypes())
    {
        const auto child_s = FromAronTypeNaviagtorPtr(child);
        CppBlockPtr b2 = child_s->getInitializeBlock((accessor != "" ? (accessor + "." + key) : key));
        b->appendBlock(b2);
    }
    return b;
}

CppBlockPtr AronObjectTypeCppSerializer::getWriteTypeBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());

    if (typenavigator->getExtends() != nullptr)
    {
        const auto extends_s = FromAronTypeNaviagtorPtr(typenavigator->getExtends());
        b->addLine(extends_s->getCppTypename() + "::writeType(w);");
    }

    b->addLine("w.writeStartObjectType();");
    b->addLine("w.writeObjectName(\"" + typenavigator->getName() + "\");");
    for (const auto& [key, child] : typenavigator->getAcceptedTypes())
    {
        const auto child_s = FromAronTypeNaviagtorPtr(child);
        b->addLine("w.writeKey(\"" + key + "\");");
        CppBlockPtr b2 = child_s->getWriteTypeBlock((accessor != "" ? (accessor + "." + key) : key));
        b->appendBlock(b2);
    }
    b->addLine("w.writeEndObjectType();");
    return b;
}

CppBlockPtr AronObjectTypeCppSerializer::getWriteBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    if (typenavigator->getExtends() != nullptr)
    {
        const auto extends_s = FromAronTypeNaviagtorPtr(typenavigator->getExtends());
        b->addLine(extends_s->getCppTypename() + "::write(w, aron_type);");
    }

    b->addLine("w.writeStartDict();");
    for (const auto& [key, child] : typenavigator->getAcceptedTypes())
    {
        const auto child_s = FromAronTypeNaviagtorPtr(child);
        b->addLine("w.writeKey(\"" + key + "\");");
        CppBlockPtr b2 = child_s->getWriteBlock((accessor != "" ? (accessor + "." + key) : key));
        b->appendBlock(b2);
    }
    b->addLine("w.writeEndDict();");
    return b;
}

CppBlockPtr AronObjectTypeCppSerializer::getReadBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    if (typenavigator->getExtends() != nullptr)
    {
        const auto extends_s = FromAronTypeNaviagtorPtr(typenavigator->getExtends());
        b->addLine(extends_s->getCppTypename() + "::read(r);");
    }

    b->addLine("r.readStartDict();");
    for (const auto& [key, child] : typenavigator->getAcceptedTypes())
    {
        const auto child_s = FromAronTypeNaviagtorPtr(child);
        b->addLine("r.readMember(\"" + key + "\");");
        CppBlockPtr b2 = child_s->getReadBlock((accessor != "" ? (accessor + "." + key) : key));
        b->appendBlock(b2);
    }
    b->addLine("r.readEndDict();");
    return b;
}

CppBlockPtr AronObjectTypeCppSerializer::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    if (accessor == "") // object definition, so we are no member of some other class
    {
        if (typenavigator->getExtends() != nullptr)
        {
            const auto extends_s = FromAronTypeNaviagtorPtr(typenavigator->getExtends());
            b->addLine("if (not (" + extends_s->getCppTypename() + "::operator== (" + otherInstanceAccessor + ")))");
            b->addLine("\t return false;");
        }
        for (const auto& [key, child] : typenavigator->getAcceptedTypes())
        {
            auto child_s = FromAronTypeNaviagtorPtr(child);
            CppBlockPtr b2 = child_s->getEqualsBlock(key, otherInstanceAccessor + "." + key);
            b->appendBlock(b2);
        }
    }
    else // object usage with existing operator==
    {
        b->addLine("if (not (" + accessor + " == " + otherInstanceAccessor + "))");
        b->addLine("\t return false;");
    }
    return b;
}
}
}
}
}
}

