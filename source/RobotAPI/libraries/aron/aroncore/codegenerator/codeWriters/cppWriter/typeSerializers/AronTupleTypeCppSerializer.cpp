/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronTupleTypeCppSerializer.h"


namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {

                    const std::string AronTupleTypeCppSerializer::TUPLE_ITERATOR_ACCESSOR = "_tupleiterator";

                    // constructors
                    AronTupleTypeCppSerializer::AronTupleTypeCppSerializer(const typenavigator::AronTupleTypeNavigatorPtr& e) :
                        AronTypeCppSerializer("std::tuple<" + boost::algorithm::join(ExtractCppTypenamesFromList(e->getAcceptedTypes()), ", ") + ">", boost::core::demangle(typeid(data::AronList).name()), boost::core::demangle(typeid(type::AronTupleType).name())),
                        typenavigator(e)
                    {

                    }

                    std::vector<CppFieldPtr> AronTupleTypeCppSerializer::getPublicVariableDeclarations(const std::string& name) const
                    {
                        CppFieldPtr field = CppFieldPtr(new CppField(getCppTypename(), name));
                        return {field};
                    }

                    std::vector<std::pair<std::string, std::string>> AronTupleTypeCppSerializer::getCtorInitializers(const std::string&) const
                    {
                        return {};
                    }

                    CppBlockPtr AronTupleTypeCppSerializer::getCtorBlock(const std::string&) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronTupleTypeCppSerializer::getInitializeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());

                        unsigned int i = 0;
                        for (const auto& child : typenavigator->getAcceptedTypes())
                        {
                            auto child_s = FromAronTypeNaviagtorPtr(child);
                            CppBlockPtr b2 = child_s->getInitializeBlock("std::get<" + std::to_string(i++) + ">(" + accessor + ")");
                            b->appendBlock(b2);
                        }
                        return b;
                    }

                    CppBlockPtr AronTupleTypeCppSerializer::getWriteTypeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("w.writeStartTupleType();");

                        std::string escaped_accessor = EscapeAccessor(accessor);
                        unsigned int i = 0;
                        for (const auto& type : typenavigator->getAcceptedTypes())
                        {
                            std::string accessor_iterator = escaped_accessor + TUPLE_ITERATOR_ACCESSOR + std::to_string(i++);
                            auto type_s = FromAronTypeNaviagtorPtr(type);
                            b->addLine("w.writeKey(\"" + std::to_string(i) + "\");");
                            CppBlockPtr b2 = type_s->getWriteTypeBlock(accessor_iterator);
                            b->appendBlock(b2);
                        }
                        b->addLine("w.writeEndTupleType();");
                        return b;
                    }

                    CppBlockPtr AronTupleTypeCppSerializer::getResetBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());

                        unsigned int i = 0;
                        for (const auto& type : typenavigator->getAcceptedTypes())
                        {
                            auto type_s = FromAronTypeNaviagtorPtr(type);
                            CppBlockPtr b2 = type_s->getResetBlock("std::get<" + std::to_string(i++) + ">(" + accessor + ")");
                            b->appendBlock(b2);
                        }
                        return b;
                    }

                    CppBlockPtr AronTupleTypeCppSerializer::getWriteBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("w.writeStartList();");

                        unsigned int i = 0;
                        for (const auto& type : typenavigator->getAcceptedTypes())
                        {
                            auto type_s = FromAronTypeNaviagtorPtr(type);
                            CppBlockPtr b2 = type_s->getWriteBlock("std::get<" + std::to_string(i++) + ">(" + accessor + ")");
                            b->appendBlock(b2);
                        }
                        b->addLine("w.writeEndList();");
                        return b;
                    }

                    CppBlockPtr AronTupleTypeCppSerializer::getReadBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("r.readStartList();");

                        unsigned int i = 0;
                        for (const auto& type : typenavigator->getAcceptedTypes())
                        {
                            auto type_s = FromAronTypeNaviagtorPtr(type);
                            CppBlockPtr b2 = type_s->getReadBlock("std::get<" + std::to_string(i++) + ">(" + accessor + ")");
                            b->appendBlock(b2);
                        }
                        b->addLine("r.readEndList();");
                        return b;
                    }

                    CppBlockPtr AronTupleTypeCppSerializer::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("if (not (" + accessor + " == " + otherInstanceAccessor + "))");
                        b->addLine("\t return false;");
                        return b;
                    }
                }
            }
        }
    }
}

