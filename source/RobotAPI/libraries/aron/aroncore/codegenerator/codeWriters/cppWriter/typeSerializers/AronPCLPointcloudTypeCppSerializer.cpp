/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronPCLPointcloudTypeCppSerializer.h"

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {

                    const std::map<std::string, std::string> AronPCLPointcloudTypeCppSerializer::ACCEPTED_TYPES =
                    {
                        {"PointXYZRGBL", "pcl::PointXYZRGBL"}
                    };

                    // constructors
                    AronPCLPointcloudTypeCppSerializer::AronPCLPointcloudTypeCppSerializer(const typenavigator::AronPCLPointcloudTypeNavigatorPtr& n) :
                        AronTypeCppSerializer("pcl::PointCloud<" + ACCEPTED_TYPES.at(n->getUsedType()) + ">", boost::core::demangle(typeid(data::AronNDArray).name()), boost::core::demangle(typeid(type::AronPCLPointcloudType).name())),
                        typenavigator(n)
                    {
                    }

                    std::vector<CppFieldPtr> AronPCLPointcloudTypeCppSerializer::getPublicVariableDeclarations(const std::string& name) const
                    {
                        CppFieldPtr field = CppFieldPtr(new CppField(getCppTypename(), name));
                        return {field};
                    }

                    std::vector<std::pair<std::string, std::string>> AronPCLPointcloudTypeCppSerializer::getCtorInitializers(const std::string&) const
                    {
                        return {};
                    }

                    CppBlockPtr AronPCLPointcloudTypeCppSerializer::getCtorBlock(const std::string&) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronPCLPointcloudTypeCppSerializer::getResetBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronPCLPointcloudTypeCppSerializer::getInitializeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronPCLPointcloudTypeCppSerializer::getWriteTypeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronPCLPointcloudTypeCppSerializer::getWriteBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("w.writeBlob(reinterpret_cast<unsigned char*>(" + accessor + ".points.data()));");
                        return b;
                    }

                    CppBlockPtr AronPCLPointcloudTypeCppSerializer::getReadBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        //PointCloud (uint32_t width_, uint32_t height_, const PointT& value_ = PointT ())
                        b->addLine(accessor + " = " + getCppTypename() + "(" + std::to_string(typenavigator->getWidth()) + ", " + std::to_string(typenavigator->getHeight()) + ", " + typenavigator->getUsedType() + ");");
                        b->addLine("memcpy(" + accessor + ".points.data(), reinterpret_cast<" + typenavigator->getUsedType() + "*>(r.readBlob()), " + accessor + ".width * " + accessor + ".height);");
                        return b;
                    }

                    CppBlockPtr AronPCLPointcloudTypeCppSerializer::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }
                }
            }
        }
    }
}

