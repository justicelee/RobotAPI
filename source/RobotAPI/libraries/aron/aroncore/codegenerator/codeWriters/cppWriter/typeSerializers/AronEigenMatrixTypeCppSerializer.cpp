/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronEigenMatrixTypeCppSerializer.h"

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {

                    const std::map<std::string, std::string> AronEigenMatrixTypeCppSerializer::ACCEPTED_TYPES =
                    {
                        {"short", "short"},
                        {"int", "int"},
                        {"long", "long"},
                        {"float", "float"},
                        {"double", "double"},
                        {"complex float", "std::complex<float>"},
                        {"complex double", "std::complex<double>"}
                    };

                    namespace
                    {
                        // anonymous helper functions
                        std::string implodeDimensionVector(const std::vector<int>& dimensions)
                        {
                            if (dimensions.size() == 0)
                            {
                                return "";
                            }

                            std::stringstream ss;
                            for (unsigned int i = 0; i < dimensions.size() - 1; ++i)
                            {
                                ss << dimensions[i] << ", ";
                            }
                            ss << dimensions[dimensions.size() - 1];
                            return ss.str();
                        }
                    }

                    // constructors
                    AronEigenMatrixTypeCppSerializer::AronEigenMatrixTypeCppSerializer(const typenavigator::AronEigenMatrixTypeNavigatorPtr& n) :
                        AronTypeCppSerializer("::Eigen::Matrix<" + ACCEPTED_TYPES.at(n->getUsedType()) + ", " + implodeDimensionVector(n->getDimensions()) + ">", boost::core::demangle(typeid(data::AronNDArray).name()), boost::core::demangle(typeid(type::AronEigenMatrixType).name())),
                        typenavigator(n)
                    {
                    }

                    std::vector<CppFieldPtr> AronEigenMatrixTypeCppSerializer::getPublicVariableDeclarations(const std::string& name) const
                    {
                        CppFieldPtr field = CppFieldPtr(new CppField(getCppTypename(), name));
                        return {field};
                    }

                    std::vector<std::pair<std::string, std::string>> AronEigenMatrixTypeCppSerializer::getCtorInitializers(const std::string&) const
                    {
                        return {};
                    }

                    CppBlockPtr AronEigenMatrixTypeCppSerializer::getCtorBlock(const std::string&) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronEigenMatrixTypeCppSerializer::getResetBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + " = {};");
                        return b;
                    }

                    CppBlockPtr AronEigenMatrixTypeCppSerializer::getWriteTypeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        //b->addLine(getAronTypePtrTypename() + " " + accessor + " = " + getAronTypePtrTypename() + "(new " + getAronTypeTypename() + "());");
                        return b;
                    }

                    CppBlockPtr AronEigenMatrixTypeCppSerializer::getInitializeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + " = {};");
                        return b;
                    }

                    CppBlockPtr AronEigenMatrixTypeCppSerializer::getWriteBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("w.writeBlob(reinterpret_cast<unsigned char*>(" + accessor + ".data()));");
                        return b;
                    }

                    CppBlockPtr AronEigenMatrixTypeCppSerializer::getReadBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + " = " + getCppTypename() + "(reinterpret_cast<" + typenavigator->getUsedType() + "*>(r.readBlob())); // Eigen already performs memcpy");
                        return b;
                    }

                    CppBlockPtr AronEigenMatrixTypeCppSerializer::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }
                }
            }
        }
    }
}
