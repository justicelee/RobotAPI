/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL
#include <string>
#include <cctype>
#include <map>

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronTypeCppSerializer.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/codegenerator/codeWriters/cppWriter/typeSerializers/AronTypeCppSerializerFactory.h>

namespace armarx
{
namespace aron
{
namespace codegeneration
{
namespace classwriter
{
namespace cppSerializer
{
// constantes
const std::string AronTypeCppSerializer::ARON_DATA_NAME = boost::core::demangle(typeid(armarx::aron::data::AronData).name());
const std::string AronTypeCppSerializer::ARON_DATA_PTR_NAME = AronTypeCppSerializer::ARON_DATA_NAME + "::PointerType";

const std::string AronTypeCppSerializer::ARON_TYPE_NAME = boost::core::demangle(typeid(armarx::aron::type::AronType).name());
const std::string AronTypeCppSerializer::ARON_TYPE_PTR_NAME = AronTypeCppSerializer::ARON_TYPE_NAME + "::PointerType";

const std::map<std::string, std::string> AronTypeCppSerializer::RESOLVE_TYPENAMES =
{
    {"string", "std::string"},
};

const std::map<std::string, std::string> AronTypeCppSerializer::ESCAPE_ACCESSORS =
{
    {"->", "_ptr_"},
    {".", "_dot_"},
    {"[", "_lbr_"},
    {"]", "_rbr_"},
};

const AronTypeCppSerializerFactoryPtr AronTypeCppSerializer::FACTORY = AronTypeCppSerializerFactoryPtr(new AronTypeCppSerializerFactory());


// static methods
std::string AronTypeCppSerializer::ResolveCppTypename(const std::string& enteredTypeName)
{
    auto i = AronTypeCppSerializer::RESOLVE_TYPENAMES.find(enteredTypeName);
    if (i != AronTypeCppSerializer::RESOLVE_TYPENAMES.end())
    {
        return i->second;
    }
    return enteredTypeName;
}

std::vector<std::string> AronTypeCppSerializer::ResolveCppTypenames(const std::vector<std::string>& enteredTypeNames)
{
    std::vector<std::string> ret;
    for(const auto& t : enteredTypeNames)
    {
        ret.push_back(AronTypeCppSerializer::ResolveCppTypename(t));
    }
    return ret;
}

std::string AronTypeCppSerializer::EscapeAccessor(const std::string& accessor)
{
    std::string escaped_accessor = accessor;
    for (const auto& [key, value] : ESCAPE_ACCESSORS)
    {
        boost::algorithm::replace_all(escaped_accessor, key, value);
    }
    return escaped_accessor;
}

std::string AronTypeCppSerializer::UnescapeAccessor(const std::string& accessor)
{
    std::string unescaped_accessor = accessor;
    for (const auto& [key, value] : ESCAPE_ACCESSORS)
    {
        boost::algorithm::replace_all(unescaped_accessor, value, key);
    }
    return unescaped_accessor;
}

std::string AronTypeCppSerializer::ExtractCppTypenamesFromNavigator(const typenavigator::AronTypeNavigatorPtr& n)
{
    AronTypeCppSerializerPtr cpp = AronTypeCppSerializer::FromAronTypeNaviagtorPtr(n);
    return cpp->getCppTypename();
}
std::vector<std::string> AronTypeCppSerializer::ExtractCppTypenamesFromList(const std::vector<typenavigator::AronTypeNavigatorPtr>& n)
{
    std::vector<std::string> ret;
    for(const auto& typenav : n)
    {
        ret.push_back(AronTypeCppSerializer::ExtractCppTypenamesFromNavigator(typenav));
    }
    return ret;
}

AronTypeCppSerializerPtr AronTypeCppSerializer::FromAronTypeNaviagtorPtr(const typenavigator::AronTypeNavigatorPtr& n)
{
    return FACTORY->create(n, n->getPath());
}

// constructors
AronTypeCppSerializer::AronTypeCppSerializer(const std::string& cppName, const std::string& aronDataTypename, const std::string& aronTypeTypename) :
    cppTypename(cppName),
    aronDataTypename(aronDataTypename),
    aronTypeTypename(aronTypeTypename)
{

}

std::string AronTypeCppSerializer::getCppTypename() const
{
    return cppTypename;
}

std::string AronTypeCppSerializer::getAronDataTypename() const
{
    return aronDataTypename;
}

std::string AronTypeCppSerializer::getAronDataPtrTypename() const
{
    return getAronDataTypename() + "::PointerType";
}

std::string AronTypeCppSerializer::getAronTypeTypename() const
{
    return aronTypeTypename;
}

std::string AronTypeCppSerializer::getAronTypePtrTypename() const
{
    return getAronTypeTypename() + "::PointerType";
}

CppCtorPtr AronTypeCppSerializer::toCtor(const std::string& name) const
{
    CppCtorPtr c = CppCtorPtr(new CppCtor(name + "()"));
    std::vector<std::pair<std::string, std::string>> initList = this->getCtorInitializers("");
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    b->addLine("initialize();");
    b->appendBlock(this->getCtorBlock(""));
    c->addInitListEntries(initList);
    c->setBlock(b);

    return c;
}

CppMethodPtr AronTypeCppSerializer::toResetMethod() const
{
    std::stringstream doc;
    doc << "@brief reset() - This method resets all member variables to default. \n";
    doc << "@return - nothing";

    CppMethodPtr m = CppMethodPtr(new CppMethod("virtual void reset() override", doc.str()));
    CppBlockPtr b = this->getResetBlock("");
    m->setBlock(b);
    return m;
}

CppMethodPtr AronTypeCppSerializer::toInitializeMethod() const
{
    std::stringstream doc;
    doc << "@brief initialize() - This method initializes member variables according to the XML type description. \n";
    doc << "@return - nothing";

    CppMethodPtr m = CppMethodPtr(new CppMethod("virtual void initialize() override", doc.str()));
    CppBlockPtr b = this->getInitializeBlock("");
    m->setBlock(b);
    return m;
}

CppMethodPtr AronTypeCppSerializer::toWriteTypeMethod() const
{
    std::stringstream doc;
    doc << "@brief writeType() - This method returns a new type from the class structure using a type writer implementation. \n";
    doc << "@return - the result of the writer implementation";

    CppMethodPtr m = CppMethodPtr(new CppMethod("virtual void writeType(armarx::aron::io::AronTypeWriter& w) const override", doc.str()));
    CppBlockPtr b = this->getWriteTypeBlock("");
    m->setBlock(b);
    return m;
}

CppMethodPtr AronTypeCppSerializer::toWriteMethod() const
{
    std::stringstream doc;
    doc << "@brief write() - This method returns a new type from the member data types using a data writer implementation. \n";
    doc << "@param w - The writer implementation\n";
    doc << "@return - the result of the writer implementation";

    CppMethodPtr m = CppMethodPtr(new CppMethod("virtual void write(armarx::aron::io::AronDataWriter& w) const override", doc.str()));
    CppBlockPtr b = this->getWriteBlock("");
    m->setBlock(b);
    return m;
}

CppMethodPtr AronTypeCppSerializer::toReadMethod() const
{
    std::stringstream doc;
    doc << "@brief read() - This method sets the struct members to new values given in a data reader implementation. \n";
    doc << "@param r - The reader implementation\n";
    doc << "@return - nothing";

    CppMethodPtr m = CppMethodPtr(new CppMethod("virtual void read(armarx::aron::io::AronDataReader& r) override", doc.str()));
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    b->addLine("this->reset();");
    b->addLine("");
    CppBlockPtr b2 = this->getReadBlock("");
    b->appendBlock(b2);
    m->setBlock(b);
    return m;
}

CppMethodPtr AronTypeCppSerializer::toSpecializedDataWriterMethod(const std::string& returnname, const std::string& methodname, const std::string& writerName) const
{
    std::stringstream doc;
    doc << "@brief specializedDataWrite() - This method returns a new data from the member data types using a writer implementation. \n";
    doc << "@return - the result of the writer implementation";

    CppMethodPtr m = CppMethodPtr(new CppMethod(returnname + " " + methodname + "() const", doc.str()));
    m->addLine(writerName + " writer;");
    m->addLine("this->write(writer);");
    m->addLine("return writer.getResult();");
    return m;
}

CppMethodPtr AronTypeCppSerializer::toSpecializedDataReaderMethod(const std::string& argumentname, const std::string& methodname, const std::string& readerName) const
{
    std::stringstream doc;
    doc << "@brief specializedDataRead() - This method sets the struct members to new values given in a reader implementation. \n";
    doc << "@return - nothing";

    CppMethodPtr m = CppMethodPtr(new CppMethod("void " + methodname + "(const " + argumentname + "& input)", doc.str()));
    m->addLine(readerName + " reader(input);");
    m->addLine("this->read(reader);");
    return m;
}

CppMethodPtr AronTypeCppSerializer::toSpecializedTypeWriterMethod(const std::string& returnname, const std::string& methodname, const std::string& writerName) const
{
    std::stringstream doc;
    doc << "@brief specializedTypeWrite() - This method returns a new type from the member data types using a writer implementation. \n";
    doc << "@return - the result of the writer implementation";

    CppMethodPtr m = CppMethodPtr(new CppMethod(returnname + " " + methodname + "() const", doc.str()));
    m->addLine(writerName + " writer;");
    m->addLine("this->writeType(writer);");
    m->addLine("return writer.getResult();");
    return m;
}

CppMethodPtr AronTypeCppSerializer::toSpecializedTypeReaderMethod(const std::string& argumentname, const std::string& methodname, const std::string& readerName) const
{
    std::stringstream doc;
    doc << "@brief specializedTypeRead() - This method sets the structure of the members to new values given in a reader implementation. \n";
    doc << "@return - nothing";

    CppMethodPtr m = CppMethodPtr(new CppMethod("void " + methodname + "(const " + argumentname + "& input)", doc.str()));
    m->addLine(readerName + " reader(input);");
    m->addLine("this->readType(reader);");
    return m;
}

CppMethodPtr AronTypeCppSerializer::toEqualsMethod() const
{
    std::stringstream doc;
    doc << "@brief operator==() - This method checks whether all values equal another instance. \n";
    doc << "@param i - The other instance\n";
    doc << "@return - true, if all members are the same, false otherwise";

    CppMethodPtr m = CppMethodPtr(new CppMethod("bool operator==(const " + this->getCppTypename() + "& i) const", doc.str()));
    CppBlockPtr b = this->getEqualsBlock("", "i");
    b->addLine("return true;");
    m->setBlock(b);
    return m;
}
}
}
}
}
}


