/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronCppWriter.h"

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                AronTypeClassCppWriter::AronTypeClassCppWriter(const std::string& producerName, const std::vector<std::string>& additionalIncludesFromXMLFile) :
                    AronCodeWriter(producerName, additionalIncludesFromXMLFile)
                {
                    addToAronMethod();
                    addFromAronMethod();
                    addToAronTypeMethod();
                }

                void AronTypeClassCppWriter::addToAronMethod()
                {
                    // The toAron Serializer is visible by default
                    AronWriterInfoPtr toAron = AronWriterInfoPtr(new AronWriterInfo());
                    toAron->methodName = "toAron";
                    toAron->returnType = "armarx::aron::data::AronDataPtr";
                    toAron->writerClassType = "armarx::aron::io::AronDataNavigatorWriter";
                    toAron->include = "<RobotAPI/libraries/aron/aroncore/io/AronDataIO/classWriters/AronDataNavigatorWriter/AronDataNavigatorWriter.h>";
                    dataWriters.push_back(toAron);
                }

                void AronTypeClassCppWriter::addFromAronMethod()
                {
                    // The fromAron method is visible by deafult
                    AronReaderInfoPtr fromAron = AronReaderInfoPtr(new AronReaderInfo());
                    fromAron->methodName = "fromAron";
                    fromAron->argumentType = "armarx::aron::data::AronDataPtr";
                    fromAron->readerClassType = "armarx::aron::io::AronDataNavigatorReader";
                    fromAron->include = "<RobotAPI/libraries/aron/aroncore/io/AronDataIO/classReaders/AronDataNavigatorReader/AronDataNavigatorReader.h>";
                    dataReaders.push_back(fromAron);
                }

                void AronTypeClassCppWriter::addToAronTypeMethod()
                {
                    // The toAron Serializer is visible by default
                    AronWriterInfoPtr toAronType = AronWriterInfoPtr(new AronWriterInfo());
                    toAronType->methodName = "toAronType";
                    toAronType->returnType = "armarx::aron::type::AronTypePtr";
                    toAronType->writerClassType = "armarx::aron::io::AronTypeNavigatorWriter";
                    toAronType->include = "<RobotAPI/libraries/aron/aroncore/io/AronTypeIO/classWriters/AronTypeNavigatorWriter/AronTypeNavigatorWriter.h>";
                    typeWriters.push_back(toAronType);
                }

                void AronTypeClassCppWriter::generateTypeClasses(const std::vector<AronGenerateTypeInfoPtr>& generateTypes)
                {
                    for (const auto& publicGenerateObjectType : generateTypes)
                    {
                        // Convert to Object type and create class object
                        if (publicGenerateObjectType->correspondingObjectType == nullptr)
                        {
                            throw LocalException("An received public type is null. Abort due to error!");
                        }

                        cppSerializer::AronObjectTypeCppSerializerPtr objectSerializer = cppSerializer::AronObjectTypeCppSerializerPtr(new cppSerializer::AronObjectTypeCppSerializer(publicGenerateObjectType->correspondingObjectType));

                        CppClassPtr c = setupCppClassFromObjectType(publicGenerateObjectType->correspondingObjectType);
                        if (publicGenerateObjectType->correspondingObjectType->getExtends() != nullptr)
                        {
                            cppSerializer::AronObjectTypeCppSerializerPtr extendsSerializer = cppSerializer::AronObjectTypeCppSerializerPtr(new cppSerializer::AronObjectTypeCppSerializer(publicGenerateObjectType->correspondingObjectType->getExtends()));
                            c->addInherit(extendsSerializer->getCppTypename());
                        }
                        else
                        {
                            c->addInherit("virtual public armarx::aron::codegeneration::AronCppClass");
                            c->addInclude("<RobotAPI/libraries/aron/aroncore/codegenerator/AronCppClass.h>");
                        }

                        // Add includes and guard
                        c->addClassDoc("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
                                       "!!!!!!AUTOGENERATED CLASS. Please do NOT edit. Instead, edit the corresponding .xml file!!!!!!\n"
                                       "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        c->setPragmaOnceIncludeGuard(true);
                        c->addInclude("<string>");
                        c->addInclude("<vector>");
                        c->addInclude("<map>");
                        c->addInclude("<RobotAPI/interface/aron.h>");
                        for (const std::string& s : additionalIncludes)
                        {
                            c->addInclude(s);
                        }

                        generateInnerTypeClasses(c, publicGenerateObjectType->nestedObjects);

                        // ctor
                        c->addCtor(objectSerializer->toCtor(c->getName()));

                        // Generic methods
                        CppMethodPtr equals = objectSerializer->toEqualsMethod();
                        c->addMethod(equals);

                        CppMethodPtr reset = objectSerializer->toResetMethod();
                        c->addMethod(reset);

                        CppMethodPtr setup = objectSerializer->toInitializeMethod();
                        c->addMethod(setup);

                        CppMethodPtr writeType = objectSerializer->toWriteTypeMethod();
                        c->addMethod(writeType);

                        CppMethodPtr write = objectSerializer->toWriteMethod();
                        c->addMethod(write);

                        CppMethodPtr read = objectSerializer->toReadMethod();
                        c->addMethod(read);

                        // Writermethods
                        for (const AronWriterInfoPtr& info : dataWriters)
                        {
                            c->addInclude(info->include);
                            CppMethodPtr convert = objectSerializer->toSpecializedDataWriterMethod(info->returnType, info->methodName, info->writerClassType);
                            c->addMethod(convert);
                        }

                        // Add methods to set the member variables
                        for (const AronReaderInfoPtr& info : dataReaders)
                        {
                            c->addInclude(info->include);
                            CppMethodPtr convert = objectSerializer->toSpecializedDataReaderMethod(info->argumentType, info->methodName, info->readerClassType);
                            c->addMethod(convert);
                        }

                        // Writermethods
                        for (const AronWriterInfoPtr& info : typeWriters)
                        {
                            c->addInclude(info->include);
                            CppMethodPtr convert = objectSerializer->toSpecializedTypeWriterMethod(info->returnType, info->methodName, info->writerClassType);
                            c->addMethod(convert);
                        }

                        // Add methods to set the member variables
                        for (const AronReaderInfoPtr& info : typeReaders)
                        {
                            c->addInclude(info->include);
                            CppMethodPtr convert = objectSerializer->toSpecializedTypeReaderMethod(info->argumentType, info->methodName, info->readerClassType);
                            c->addMethod(convert);
                        }

                        typeClasses.push_back(c);
                    }
                }

                void AronTypeClassCppWriter::generateInnerTypeClasses(CppClassPtr& classToAdd, const std::map<std::string, AronGenerateTypeInfoPtr>& localGenerateTypes)
                {
                    // setup inner classes
                    for (const auto& [_, localGenerateObjectType] : localGenerateTypes)
                    {
                        cppSerializer::AronObjectTypeCppSerializerPtr objectSerializer = cppSerializer::AronObjectTypeCppSerializerPtr(new cppSerializer::AronObjectTypeCppSerializer(localGenerateObjectType->correspondingObjectType));

                        CppClassPtr inner = setupCppClassFromObjectType(localGenerateObjectType->correspondingObjectType);
                        CppMethodPtr inner_equals = objectSerializer->toEqualsMethod();
                        inner->addMethod(inner_equals);

                        generateInnerTypeClasses(inner, localGenerateObjectType->nestedObjects);
                        classToAdd->addInnerClass(inner);
                    }
                }

                CppClassPtr AronTypeClassCppWriter::setupCppClassFromObjectType(const typenavigator::AronObjectTypeNavigatorPtr& o) const
                {
                    cppSerializer::AronObjectTypeCppSerializerPtr objectSerializer = cppSerializer::AronObjectTypeCppSerializerPtr(new cppSerializer::AronObjectTypeCppSerializer(o));

                    std::vector<std::string> split;
                    std::string cppTypename = objectSerializer->getCppTypename();
                    boost::algorithm::split(split, cppTypename, boost::is_any_of("::"), boost::token_compress_on);
                    if (split.size() < 1)
                    {
                        throw LocalException("AronTypeClassCppWriter: The cpp name of an inner type was empty. Please check the type definition file if all object names are set correctly.");
                    }

                    std::vector<std::string> namespaces(split);
                    namespaces.pop_back();

                    CppClassPtr c = CppClassPtr(new CppClass(namespaces, split[split.size() - 1]));

                    for (const auto& [key, child] : o->getAcceptedTypes())
                    {
                        cppSerializer::AronTypeCppSerializerPtr child_s = cppSerializer::AronTypeCppSerializer::FromAronTypeNaviagtorPtr(child);
                        c->addPublicField(child_s->getCppTypename() + " " + key + ";");
                    }
                    return c;
                }
            }
        }
    }
}





