/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <vector>
#include <map>

// Boost
#include <boost/core/demangle.hpp>
#include <boost/algorithm/string.hpp>

// ArmarX
#include <ArmarXCore/libraries/cppgen/CppBlock.h>
#include <ArmarXCore/libraries/cppgen/CppField.h>
#include <ArmarXCore/libraries/cppgen/CppCtor.h>
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>

#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/AronException.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTypeNavigator.h>

namespace armarx
{
namespace aron
{
namespace codegeneration
{
namespace classwriter
{
namespace cppSerializer
{
class AronTypeCppSerializerFactory;
typedef std::shared_ptr<AronTypeCppSerializerFactory> AronTypeCppSerializerFactoryPtr;

class AronTypeCppSerializer;
typedef std::shared_ptr<AronTypeCppSerializer> AronTypeCppSerializerPtr;

class AronTypeCppSerializer
{
public:
    using PointerType = AronTypeCppSerializerPtr;

public:
    // constructors
    AronTypeCppSerializer(const std::string& cppName, const std::string& aronDataTypename, const std::string& aronTypeTypename);

    // public member methods
    std::string getCppTypename() const;

    std::string getAronDataTypename() const;
    std::string getAronDataPtrTypename() const;

    std::string getAronTypeTypename() const;
    std::string getAronTypePtrTypename() const;

    CppMethodPtr toSpecializedDataWriterMethod(const std::string&, const std::string&, const std::string&) const;
    CppMethodPtr toSpecializedDataReaderMethod(const std::string&, const std::string&, const std::string&) const;

    CppMethodPtr toSpecializedTypeWriterMethod(const std::string&, const std::string&, const std::string&) const;
    CppMethodPtr toSpecializedTypeReaderMethod(const std::string&, const std::string&, const std::string&) const;

    // virtual override definitions
    virtual std::vector<CppFieldPtr> getPublicVariableDeclarations(const std::string&) const = 0;

    CppCtorPtr toCtor(const std::string&) const;
    virtual std::vector<std::pair<std::string, std::string>> getCtorInitializers(const std::string&) const = 0;
    virtual CppBlockPtr getCtorBlock(const std::string&) const = 0;

    CppMethodPtr toResetMethod() const;
    virtual CppBlockPtr getResetBlock(const std::string&) const = 0;

    CppMethodPtr toWriteTypeMethod() const;
    virtual CppBlockPtr getWriteTypeBlock(const std::string&) const = 0;

    CppMethodPtr toInitializeMethod() const;
    virtual CppBlockPtr getInitializeBlock(const std::string&) const = 0;

    CppMethodPtr toWriteMethod() const;
    virtual CppBlockPtr getWriteBlock(const std::string&) const = 0;

    CppMethodPtr toReadMethod() const;
    virtual CppBlockPtr getReadBlock(const std::string&) const = 0;

    CppMethodPtr toEqualsMethod() const;
    virtual CppBlockPtr getEqualsBlock(const std::string&, const std::string&) const = 0;

    // static methods
    static std::string ResolveCppTypename(const std::string&);
    static std::vector<std::string> ResolveCppTypenames(const std::vector<std::string>&);
    static std::string EscapeAccessor(const std::string&);
    static std::string UnescapeAccessor(const std::string&);

    static std::string ExtractCppTypenamesFromNavigator(const typenavigator::AronTypeNavigatorPtr&);
    static std::vector<std::string> ExtractCppTypenamesFromList(const std::vector<typenavigator::AronTypeNavigatorPtr>&);

    static AronTypeCppSerializerPtr FromAronTypeNaviagtorPtr(const typenavigator::AronTypeNavigatorPtr&);

private:
    // members
    const static std::string ARON_DATA_NAME;
    const static std::string ARON_DATA_PTR_NAME;

    const static std::string ARON_TYPE_NAME;
    const static std::string ARON_TYPE_PTR_NAME;

    static const std::map<std::string, std::string> RESOLVE_TYPENAMES;
    static const std::map<std::string, std::string> ESCAPE_ACCESSORS;

    static const AronTypeCppSerializerFactoryPtr FACTORY;

    std::string cppTypename;
    std::string aronDataTypename;
    std::string aronTypeTypename;
};
}
}
}
}
}
