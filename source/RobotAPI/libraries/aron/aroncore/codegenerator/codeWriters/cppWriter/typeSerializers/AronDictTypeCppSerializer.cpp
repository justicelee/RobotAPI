/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronDictTypeCppSerializer.h"


namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {
                    const std::string AronDictTypeCppSerializer::DICT_ITERATOR_ACCESSOR = "_dicttype";
                    const std::string AronDictTypeCppSerializer::DICT_KEY_ACCESSOR = "_dictkey";
                    const std::string AronDictTypeCppSerializer::DICT_VALUE_ACCESSOR = "_dictval";

                    // constructors
                    AronDictTypeCppSerializer::AronDictTypeCppSerializer(const typenavigator::AronDictTypeNavigatorPtr& n) :
                        AronTypeCppSerializer("std::map<std::string, " + FromAronTypeNaviagtorPtr(n->getAcceptedType())->getCppTypename() + ">", boost::core::demangle(typeid(data::AronDict).name()), boost::core::demangle(typeid(type::AronDictType).name())),
                        typenavigator(n)
                    {

                    }

                    // virtual implementations
                    std::vector<CppFieldPtr> AronDictTypeCppSerializer::getPublicVariableDeclarations(const std::string& name) const
                    {
                        CppFieldPtr field = CppFieldPtr(new CppField(getCppTypename(), name));
                        return {field};
                    }

                    std::vector<std::pair<std::string, std::string>> AronDictTypeCppSerializer::getCtorInitializers(const std::string&) const
                    {
                        return {};
                    }

                    CppBlockPtr AronDictTypeCppSerializer::getCtorBlock(const std::string&) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronDictTypeCppSerializer::getResetBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + ".clear();");
                        return b;
                    }

                    CppBlockPtr AronDictTypeCppSerializer::getInitializeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + " = {};");
                        return b;
                    }

                    CppBlockPtr AronDictTypeCppSerializer::getWriteTypeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("w.writeStartDictType();");
                        //b->addLine("w.writeKey();");

                        std::string escaped_accessor = EscapeAccessor(accessor);
                        std::string accessor_iterator = escaped_accessor + DICT_ITERATOR_ACCESSOR;

                        auto type_s = FromAronTypeNaviagtorPtr(typenavigator->getAcceptedType());
                        CppBlockPtr b2 = type_s->getWriteTypeBlock(accessor_iterator);
                        b->appendBlock(b2);
                        b->addLine("w.writeEndDictType();");
                        return b;
                    }

                    CppBlockPtr AronDictTypeCppSerializer::getWriteBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("w.writeStartDict();");

                        std::string escaped_accessor = EscapeAccessor(accessor);
                        std::string accessor_iterator_key = escaped_accessor + DICT_KEY_ACCESSOR;
                        std::string accessor_iterator_val = escaped_accessor + DICT_VALUE_ACCESSOR;

                        b->addLine("for (const auto& [" + accessor_iterator_key + ", " + accessor_iterator_val + "] : " + accessor + ") ");

                        auto type_s = FromAronTypeNaviagtorPtr(typenavigator->getAcceptedType());
                        CppBlockPtr b2 = CppBlockPtr(new CppBlock());
                        b2->addLine("w.writeKey(" + accessor_iterator_key + ");");
                        b2 = CppBlock::MergeBlocks(b2, type_s->getWriteBlock(accessor_iterator_val));
                        b->addBlock(b2);

                        b->addLine("w.writeEndDict();");
                        return b;
                    }

                    CppBlockPtr AronDictTypeCppSerializer::getReadBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("r.readStartDict();");

                        std::string escaped_accessor = EscapeAccessor(accessor);
                        std::string accessor_iterator = escaped_accessor + DICT_ITERATOR_ACCESSOR;
                        std::string accessor_iterator_key = escaped_accessor + DICT_KEY_ACCESSOR;

                        auto type_s = FromAronTypeNaviagtorPtr(typenavigator->getAcceptedType());

                        b->addLine("while(!r.readEndDict())");
                        CppBlockPtr b2 = CppBlockPtr(new CppBlock());
                        b2->addLine("std::string " + accessor_iterator_key + " = r.readKey();");
                        b2->addLine(type_s->getCppTypename() + " " + accessor_iterator + ";");
                        b2 = CppBlock::MergeBlocks(b2, type_s->getReadBlock(accessor_iterator));
                        b2->addLine(accessor + "[" + accessor_iterator_key + "] = " + accessor_iterator + ";");

                        b->addBlock(b2);
                        return b;
                    }

                    CppBlockPtr AronDictTypeCppSerializer::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("if (not (" + accessor + " == " + otherInstanceAccessor + "))");
                        b->addLine("\t return false;");
                        return b;
                    }
                }
            }
        }
    }
}

