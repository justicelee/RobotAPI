/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronIVTCByteImageTypeCppSerializer.h"

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {

                    const std::map<std::string, std::pair<std::string, int>> AronIVTCByteImageTypeCppSerializer::ACCEPTED_TYPES =
                    {
                        {"GrayScale", std::pair<std::string, int>("CByteImage::ImageType::eGrayScale", 1)},
                        {"RGB24", std::pair<std::string, int>("CByteImage::ImageType::eRGB24", 3)},
                        {"RGB24Split", std::pair<std::string, int>("CByteImage::ImageType::eRGB24Split", 3)}
                    };

                    // constructors
                    AronIVTCByteImageTypeCppSerializer::AronIVTCByteImageTypeCppSerializer(const typenavigator::AronIVTCByteImageTypeNavigatorPtr& e) :
                        AronTypeCppSerializer("std::shared_ptr<CByteImage>", boost::core::demangle(typeid(data::AronNDArray).name()), boost::core::demangle(typeid(type::AronIVTCByteImageType).name())),
                        typenavigator(e)
                    {
                        // check if type exists
                        ACCEPTED_TYPES.at(typenavigator->getUsedType());
                    }

                    std::vector<CppFieldPtr> AronIVTCByteImageTypeCppSerializer::getPublicVariableDeclarations(const std::string& name) const
                    {
                        CppFieldPtr field = CppFieldPtr(new CppField(getCppTypename(), name));
                        return {field};
                    }

                    std::vector<std::pair<std::string, std::string>> AronIVTCByteImageTypeCppSerializer::getCtorInitializers(const std::string&) const
                    {
                        return {};
                    }

                    CppBlockPtr AronIVTCByteImageTypeCppSerializer::getCtorBlock(const std::string&) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronIVTCByteImageTypeCppSerializer::getResetBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + " = " + getCppTypename() + "(new CByteImage(" + accessor + "->width, " + accessor + "->height, " + accessor + "->type));");
                        return b;
                    }

                    CppBlockPtr AronIVTCByteImageTypeCppSerializer::getInitializeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + " = " + getCppTypename() + "(new CByteImage(" + std::to_string(typenavigator->getWidth()) + ", " + std::to_string(typenavigator->getHeight()) + ", " + ACCEPTED_TYPES.at(typenavigator->getUsedType()).first + "));");
                        return b;
                    }

                    CppBlockPtr AronIVTCByteImageTypeCppSerializer::getWriteTypeBlock(const std::string&) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("w.writeIVTCByteImageType({" + std::to_string(typenavigator->getWidth()) + ", " + std::to_string(typenavigator->getHeight()) + "}, \"" + typenavigator->getUsedType() + "\");");
                        return b;
                    }

                    CppBlockPtr AronIVTCByteImageTypeCppSerializer::getWriteBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("w.writeNDArray((unsigned int) " + accessor + "->width * " + accessor + "->height * " + accessor + "->bytesPerPixel, reinterpret_cast<unsigned char*>(" + accessor + "->pixels));");
                        return b;
                    }

                    CppBlockPtr AronIVTCByteImageTypeCppSerializer::getReadBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + " = " + getCppTypename() + "(new CByteImage(" + accessor + "->width, " + accessor + "->height, " + accessor + "->type, false));");
                        b->addLine("memcpy(" + accessor + "->pixels, reinterpret_cast<unsigned char*>(r.readNDArray()), " + accessor + "->width * " + accessor + "->height * " + accessor + "->bytesPerPixel" + ");");
                        return b;
                    }

                    CppBlockPtr AronIVTCByteImageTypeCppSerializer::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("if (not (" + accessor + "->IsCompatible(" + otherInstanceAccessor + ".get())))");
                        b->addLine("\t return false;");
                        b->addLine("if (not (memcmp(" + accessor + "->pixels, " + otherInstanceAccessor + "->pixels, " + accessor + "->width * " + accessor + "->height * " + accessor + "->bytesPerPixel) == 0))");
                        b->addLine("\t return false;");
                        return b;
                    }
                }
            }
        }
    }
}

