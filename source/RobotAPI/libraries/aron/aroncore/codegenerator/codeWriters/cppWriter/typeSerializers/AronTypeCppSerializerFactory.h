/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <unordered_map>

// Base Class
#include <RobotAPI/libraries/aron/aroncore/codegenerator/codeWriters/AronSerializerFactory.h>

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronAllTypeNavigators.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/codeWriters/cppWriter/typeSerializers/AronAllCppSerializers.h>
#include <RobotAPI/libraries/aron/aroncore/AronDescriptor.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {
                    class AronTypeCppSerializerFactory;
                    typedef std::shared_ptr<AronTypeCppSerializerFactory> AronTypeCppSerializerFactoryPtr;

                    class AronTypeCppSerializerFactory :
                        virtual public classwriter::AronSerializerFactory<typenavigator::AronTypeNavigatorPtr, cppSerializer::AronTypeCppSerializerPtr>
                    {
                    public:
                        AronTypeCppSerializerFactory() = default;
                        virtual cppSerializer::AronTypeCppSerializerPtr create(const typenavigator::AronTypeNavigatorPtr&, const AronPath&) const override;
                        virtual cppSerializer::AronTypeCppSerializerPtr createSpecific(const typenavigator::AronTypeNavigatorPtr&, const AronPath&) const override;

                    private:
                        static const std::map<AronTypeDescriptor, AronTypeCppSerializerFactoryPtr> FACTORIES;
                    };

                    // Factories
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    class Aron##upperType##TypeCppSerializerFactory : \
        virtual public AronTypeCppSerializerFactory \
    { \
    public: \
        Aron##upperType##TypeCppSerializerFactory() = default; \
        virtual cppSerializer::AronTypeCppSerializerPtr createSpecific(const typenavigator::AronTypeNavigatorPtr&, const AronPath&) const override; \
    };

                    HANDLE_CONTAINER_TYPES
                    HANDLE_COMPLEX_TYPES
                    HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
                }
            }
        }
    }
}
