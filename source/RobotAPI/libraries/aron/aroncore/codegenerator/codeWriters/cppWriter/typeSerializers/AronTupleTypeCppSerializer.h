/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base Class
#include "AronTypeCppSerializer.h"

// ArmarX
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTupleTypeNavigator.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {

                    class AronTupleTypeCppSerializer;
                    typedef std::shared_ptr<AronTupleTypeCppSerializer> AronTupleTypeCppSerializerPtr;

                    class AronTupleTypeCppSerializer :
                        virtual public AronTypeCppSerializer
                    {
                    public:
                        using PointerType = AronTupleTypeCppSerializerPtr;

                    public:
                        // constructors
                        AronTupleTypeCppSerializer(const typenavigator::AronTupleTypeNavigatorPtr& e);

                        // virtual implementations
                        virtual std::vector<CppFieldPtr> getPublicVariableDeclarations(const std::string&) const override;
                        virtual std::vector<std::pair<std::string, std::string>> getCtorInitializers(const std::string&) const override;
                        virtual CppBlockPtr getCtorBlock(const std::string&) const override;
                        virtual CppBlockPtr getWriteTypeBlock(const std::string&) const override;
                        virtual CppBlockPtr getInitializeBlock(const std::string&) const override;
                        virtual CppBlockPtr getResetBlock(const std::string&) const override;
                        virtual CppBlockPtr getWriteBlock(const std::string&) const override;
                        virtual CppBlockPtr getReadBlock(const std::string&) const override;
                        virtual CppBlockPtr getEqualsBlock(const std::string&, const std::string&) const override;

                    private:
                        // members
                        static const std::string TUPLE_ITERATOR_ACCESSOR;

                        typenavigator::AronTupleTypeNavigatorPtr typenavigator;
                    };
                }
            }
        }
    }
}
