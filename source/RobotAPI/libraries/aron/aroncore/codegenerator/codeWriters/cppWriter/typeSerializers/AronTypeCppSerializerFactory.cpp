/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL

// Header
#include "AronTypeCppSerializerFactory.h"

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {
                    // Map types to factories
                    const std::map<AronTypeDescriptor, AronTypeCppSerializerFactoryPtr> AronTypeCppSerializerFactory::FACTORIES =
                    {
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    {AronTypeDescriptor::eAron##upperType##Type, AronTypeCppSerializerFactoryPtr(new Aron##upperType##TypeCppSerializerFactory())}, \

                        HANDLE_CONTAINER_TYPES
                        HANDLE_COMPLEX_TYPES
                        HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
                    };

                    // Access method
                    cppSerializer::AronTypeCppSerializerPtr AronTypeCppSerializerFactory::create(const typenavigator::AronTypeNavigatorPtr& n, const AronPath& path) const
                    {
                        if (!n)
                        {
                            throw LocalException("AronDataNavigatorFactory: An AronDataPtr is NULL! Cannot create navigator!");
                        }

                        auto factory_iterator = FACTORIES.find(AronResolver::GetTypeDescriptorForAronType(n->getResult()));
                        if (factory_iterator == FACTORIES.end())
                        {
                            throw LocalException("AronDataNavigatorFactory: Cannot find the desired factory. Cannot create navigator!");
                        }
                        return factory_iterator->second->createSpecific(n, path);
                    }

                    cppSerializer::AronTypeCppSerializerPtr AronTypeCppSerializerFactory::createSpecific(const typenavigator::AronTypeNavigatorPtr&, const AronPath&) const
                    {
                        throw LocalException("AronDataNavigatorFactory: Called disallowed method of an AronDataNavigatorFactory. Use child class instead!");
                    }

                    // Container Factories
#define RUN_ARON_MACRO(upperType, lowerType, capsType) \
    cppSerializer::AronTypeCppSerializerPtr Aron##upperType##TypeCppSerializerFactory::createSpecific(const typenavigator::AronTypeNavigatorPtr& n, const AronPath& path) const \
    { \
        typenavigator::Aron##upperType##TypeNavigatorPtr casted = typenavigator::Aron##upperType##TypeNavigator::DynamicCast(n); \
        return cppSerializer::AronTypeCppSerializerPtr(new cppSerializer::Aron##upperType##TypeCppSerializer(casted)); \
    }

                    HANDLE_CONTAINER_TYPES
                    HANDLE_COMPLEX_TYPES
                    HANDLE_PRIMITIVE_TYPES
#undef RUN_ARON_MACRO
                }
            }
        }
    }
}
