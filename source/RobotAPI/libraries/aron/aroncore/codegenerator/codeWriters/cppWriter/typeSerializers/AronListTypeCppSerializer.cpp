/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string/replace.hpp>

// Header
#include "AronListTypeCppSerializer.h"

namespace armarx
{
namespace aron
{
namespace codegeneration
{
namespace classwriter
{
namespace cppSerializer
{
const std::string AronListTypeCppSerializer::LIST_ITERATOR_ACCESSOR = "_listtype";

// constructors
AronListTypeCppSerializer::AronListTypeCppSerializer(const typenavigator::AronListTypeNavigatorPtr& e) :
    AronTypeCppSerializer("std::vector<" + FromAronTypeNaviagtorPtr(e->getAcceptedType())->getCppTypename() + ">", boost::core::demangle(typeid(data::AronList).name()), boost::core::demangle(typeid(type::AronListType).name())),
    typenavigator(e)
{

}

std::vector<CppFieldPtr> AronListTypeCppSerializer::getPublicVariableDeclarations(const std::string& name) const
{
    CppFieldPtr field = CppFieldPtr(new CppField(getCppTypename(), name));
    return {field};
}

std::vector<std::pair<std::string, std::string>> AronListTypeCppSerializer::getCtorInitializers(const std::string&) const
{
    return {};
}

CppBlockPtr AronListTypeCppSerializer::getCtorBlock(const std::string&) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    return b;
}

CppBlockPtr AronListTypeCppSerializer::getResetBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    b->addLine(accessor + ".clear();");
    return b;
}

CppBlockPtr AronListTypeCppSerializer::getInitializeBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    b->addLine(accessor + " = {};");
    return b;
}

CppBlockPtr AronListTypeCppSerializer::getWriteTypeBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    b->addLine("w.writeStartListType();");
    //b->addLine("w.writeKey(\"" + std::to_string(typenavigator->ACCEPTED_TYPE_INDEX) + "\");");
    auto type_s = FromAronTypeNaviagtorPtr(typenavigator->getAcceptedType());

    std::string escaped_accessor = EscapeAccessor(accessor);
    std::string accessor_iterator = escaped_accessor + LIST_ITERATOR_ACCESSOR;

    CppBlockPtr b2 = type_s->getWriteTypeBlock(accessor_iterator);
    b->appendBlock(b2);

    b->addLine("w.writeEndListType();");
    return b;
}

CppBlockPtr AronListTypeCppSerializer::getWriteBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    b->addLine("w.writeStartList();");

    std::string escaped_accessor = EscapeAccessor(accessor);
    std::string accessor_iterator = escaped_accessor + LIST_ITERATOR_ACCESSOR;

    auto type_s = FromAronTypeNaviagtorPtr(typenavigator->getAcceptedType());
    b->addLine("for(unsigned int " + accessor_iterator + " = 0; " + accessor_iterator + " < " + accessor + ".size(); ++" + accessor_iterator + ")");
    CppBlockPtr b2 = type_s->getWriteBlock(accessor + "[" + accessor_iterator + "]");
    b->addBlock(b2);
    b->addLine("w.writeEndList();");
    return b;
}

CppBlockPtr AronListTypeCppSerializer::getReadBlock(const std::string& accessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    b->addLine("r.readStartList();");

    std::string escaped_accessor = EscapeAccessor(accessor);
    std::string accessor_iterator = escaped_accessor + LIST_ITERATOR_ACCESSOR;

    b->addLine("while(!r.readEndList())");
    CppBlockPtr b2 = CppBlockPtr(new CppBlock());

    auto type_s = FromAronTypeNaviagtorPtr(typenavigator->getAcceptedType());
    b2->addLine(type_s->getCppTypename() + " " + accessor_iterator + ";");
    b2 = CppBlock::MergeBlocks(b2, type_s->getReadBlock(accessor_iterator));
    b2->addLine(accessor + ".push_back(" + accessor_iterator + ");");

    b->addBlock(b2);
    return b;
}

CppBlockPtr AronListTypeCppSerializer::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
{
    CppBlockPtr b = CppBlockPtr(new CppBlock());
    b->addLine("if (not (" + accessor + " == " + otherInstanceAccessor + "))");
    b->addLine("\t return false;");
    return b;
}
}
}
}
}
}
