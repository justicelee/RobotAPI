/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Boost
#include <boost/algorithm/string.hpp>

// Header
#include "AronOpenCVImageTypeCppSerializer.h"

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                namespace cppSerializer
                {

                    const std::map<std::string, std::string> AronOpenCVMatTypeCppSerializer::ACCEPTED_TYPES =
                    {
                    };

                    // constructors
                    AronOpenCVMatTypeCppSerializer::AronOpenCVMatTypeCppSerializer(const typenavigator::AronOpenCVMatTypeNavigatorPtr& e) :
                        AronTypeCppSerializer("cv::Mat", boost::core::demangle(typeid(data::AronNDArray).name()), boost::core::demangle(typeid(type::AronOpenCVMatType).name())),
                        typenavigator(e)
                    {
                        // check if type exists
                        ACCEPTED_TYPES.at(e->getUsedType());
                    }

                    std::vector<CppFieldPtr> AronOpenCVMatTypeCppSerializer::getPublicVariableDeclarations(const std::string& name) const
                    {
                        CppFieldPtr field = CppFieldPtr(new CppField(getCppTypename(), name));
                        return {field};
                    }

                    std::vector<std::pair<std::string, std::string>> AronOpenCVMatTypeCppSerializer::getCtorInitializers(const std::string&) const
                    {
                        return {};
                    }

                    CppBlockPtr AronOpenCVMatTypeCppSerializer::getCtorBlock(const std::string&) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronOpenCVMatTypeCppSerializer::getResetBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + " = " + getCppTypename() + "();");
                        return b;
                    }

                    CppBlockPtr AronOpenCVMatTypeCppSerializer::getInitializeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine(accessor + " = " + getCppTypename() + "();");
                        return b;
                    }

                    CppBlockPtr AronOpenCVMatTypeCppSerializer::getWriteTypeBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }

                    CppBlockPtr AronOpenCVMatTypeCppSerializer::getWriteBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        b->addLine("w.writeBlob(reinterpret_cast<unsigned char*>(" + accessor + ".pixels));");
                        return b;
                    }

                    CppBlockPtr AronOpenCVMatTypeCppSerializer::getReadBlock(const std::string& accessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        //
                        //b->addLine(accessor + " = " + getFullCppTypename() + "({" + implodeDimensionVector(dimensions) + "}, " + usedType + ", reinterpret_cast<unsigned char*>(r.readBlob()), 0);");
                        return b;
                    }

                    CppBlockPtr AronOpenCVMatTypeCppSerializer::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
                    {
                        CppBlockPtr b = CppBlockPtr(new CppBlock());
                        return b;
                    }
                }
            }
        }
    }
}

