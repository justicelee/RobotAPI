/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <map>
#include <vector>
#include <typeinfo>
#include <typeindex>

// ArmarX
#include <ArmarXCore/libraries/cppgen/MetaClass.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronObjectTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronListTypeNavigator.h>
#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronPrimitiveTypeNavigator.h>

#include <RobotAPI/libraries/aron/aroncore/codegenerator/AronWriterInfo.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/AronReaderInfo.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/AronGenerateTypeInfo.h>

namespace armarx
{
    namespace aron
    {
        namespace codegeneration
        {
            namespace classwriter
            {
                class AronCodeWriter
                {
                public:
                    AronCodeWriter() = delete;
                    AronCodeWriter(const std::string& producerName, const std::vector<std::string>& additionalIncludesFromXMLFile):
                        producerName(producerName),
                        additionalIncludes(additionalIncludesFromXMLFile)
                    {}
                    virtual void generateTypeClasses(const std::vector<AronGenerateTypeInfoPtr>&) = 0;

                    std::vector<MetaClassPtr> getTypeClasses() const
                    {
                        return typeClasses;
                    }

                protected:
                    virtual void addFromAronMethod() = 0;
                    virtual void addToAronMethod() = 0;
                    virtual void addToAronTypeMethod() = 0;

                protected:
                    std::vector<MetaClassPtr> typeClasses;

                    std::string producerName;
                    std::vector<AronWriterInfoPtr> dataWriters;
                    std::vector<AronReaderInfoPtr> dataReaders;
                    std::vector<AronWriterInfoPtr> typeWriters;
                    std::vector<AronReaderInfoPtr> typeReaders;
                    std::vector<std::string> additionalIncludes;
                };
            }
        }
    }
}
