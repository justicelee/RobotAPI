/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/**
 * This file contains the general structure of all accepted AronTypes.
 *
 * In general we distinguish we distinguish between container(complex) and primitive types.
 * Complex types include Objects, Lists, Dicts, Pairs and Blobs.
 * Pimitive types on the other hand include int, long, float, double, string and bools.
 *
 * A primitive always has only one member containing the value of the aron. A container type can have multiple members.
 *
 * Note that every AronData must have a corresponding AronAbstractType (both declared in aron.ice).
 * The AronData is a copy of cpp values while the AronAbstractType describes the type structure.
 *
 * In the following we descripe the types in more detail:
 * AronData:
 * |
 * |->Container:
 * |  |
 * |  |->AronObject:
 * |  |    This container represents cpp classes. It contains a std::map mapping from a membername (std::string) to AronData.
 * |  |    It is a recursive structure that again can contain other complex or primitive types.
 * |  |
 * |  |->AronList:
 * This container represents a std::vector<T> where T can be any datatype convertable to Aron.
 * TODO:
 */

/**
  * A rule from Google:
  * Should I use macros in C++?
  * As a rule, you should only use macros, when a better alternative does not exist.
  * They should not be used to generate code; you should simply write the code instead
  * (if the code is type-agnostic, write a template instead and use concepts).
  *
  * However I do not care... :D
  */

/************************************
 * TYPES ****************************
 ***********************************/

// All containertypes go here
#define HANDLE_CONTAINER_TYPES \
    RUN_ARON_MACRO(Object, object, OBJECT) \
    RUN_ARON_MACRO(List, list, LIST) \
    RUN_ARON_MACRO(Dict, dict, DICT) \
    RUN_ARON_MACRO(Tuple, tuple, TUPLE) \

#define HANDLE_CONTAINER_TYPES_EXCEPT_OBJECT \
    RUN_ARON_MACRO(List, list, LIST) \
    RUN_ARON_MACRO(Dict, dict, DICT) \
    RUN_ARON_MACRO(Tuple, tuple, TUPLE) \

// All complex types go here
#define HANDLE_COMPLEX_TYPES \
    RUN_ARON_MACRO(EigenMatrix, eigenmatrix, EIGEN_MATRIX) \
    RUN_ARON_MACRO(IVTCByteImage, ivtcbyteimage, IVT_CBYTE_IMAGE) \
    RUN_ARON_MACRO(OpenCVMat, opencvmat, OPENCV_MAT) \
    RUN_ARON_MACRO(PCLPointcloud, pclpointcloud, PCL_POINTCLOUD)

// All primitive types without cpp type.
// The target language type must be similar to the lowercase type
#define HANDLE_PRIMITIVE_TYPES \
    RUN_ARON_MACRO(Int, int, INT) \
    RUN_ARON_MACRO(Long, long, LONG) \
    RUN_ARON_MACRO(Float, float, FLOAT) \
    RUN_ARON_MACRO(Double, double, DOUBLE) \
    RUN_ARON_MACRO(String, string, STRING) \
    RUN_ARON_MACRO(Bool, bool, BOOL)

#define HANDLE_PRIMITIVE_TYPES_EXCEPT_STRING \
    RUN_ARON_MACRO(Int, int, INT) \
    RUN_ARON_MACRO(Long, long, LONG) \
    RUN_ARON_MACRO(Float, float, FLOAT) \
    RUN_ARON_MACRO(Double, double, DOUBLE) \
    RUN_ARON_MACRO(Bool, bool, BOOL)

// All combined
#define HANDLE_ALL_ARON_TYPES \
    HANDLE_CONTAINER_TYPES \
    HANDLE_COMPLEX_TYPES \
    HANDLE_PRIMITIVE_TYPES

/************************************
 * ABSTRACT TYPES *******************
 ***********************************/
#define HANDLE_CONTAINER_ARON_ABSTRACT_TYPES \
    RUN_ARON_MACRO(AronDictSerializerType, arondictserializertype, ARON_DICT_SERIALIZER_TYPE) \
    RUN_ARON_MACRO(AronListSerializerType, aronlistserializertype, ARON_LIST_SERIALIZER_TYPE)

#define HANDLE_COMPLEX_ARON_ABSTRACT_TYPES \
    RUN_ARON_MACRO(AronNDArraySerializerType, aronndarrayserializertype, ARON_NDARRAY_SERIALIZER_TYPE)

#define HANDLE_ALL_ARON_ABSTRACT_TYPES \
    HANDLE_CONTAINER_ARON_ABSTRACT_TYPES \
    HANDLE_COMPLEX_ARON_ABSTRACT_TYPES

/************************************
 * DATA *****************************
 ***********************************/

// All containerdata go here
#define HANDLE_CONTAINER_DATA \
    RUN_ARON_MACRO(List, list, LIST) \
    RUN_ARON_MACRO(Dict, dict, DICT)

#define HANDLE_COMPLEX_DATA \
    RUN_ARON_MACRO(NDArray, ndarray, NDARRAY)

// All three combined
#define HANDLE_ALL_ARON_DATA \
    HANDLE_CONTAINER_DATA \
    HANDLE_COMPLEX_DATA \
    HANDLE_PRIMITIVE_TYPES

/************************************
 * MIXED ****************************
 ***********************************/
#define HANDLE_CONTAINER_CORRESPONDING \
    RUN_ARON_MACRO(List, list, LIST, List, list, LIST) \
    RUN_ARON_MACRO(Tuple, tuple, TUPLE, List, list, LIST) \
    RUN_ARON_MACRO(Object, object, OBJECT, Dict, dict, DICT) \
    RUN_ARON_MACRO(Dict, dict, DICT, Dict, dict, DICT)

#define HANDLE_COMPLEX_CORRESPONDING \
    RUN_ARON_MACRO(EigenMatrix, eigenmatrix, EIGEN_MATRIX, NDArray, ndarray, NDARRAY) \
    RUN_ARON_MACRO(IVTCByteImage, ivtcbyteimage, IVT_CBYTE_IMAGE, NDArray, ndarray, NDARRAY) \
    RUN_ARON_MACRO(OpenCVMat, opencvmat, OPENCV_MAT, NDArray, ndarray, NDARRAY) \
    RUN_ARON_MACRO(PCLPointcloud, pclpointcloud, PCL_POINTCLOUD, NDArray, ndarray, NDARRAY)

#define HANDLE_PRIMITIVE_CORRESPONDING \
    RUN_ARON_MACRO(Int, int, INT, Int, int, INT) \
    RUN_ARON_MACRO(Long, long, LONG, Long, long, LONG) \
    RUN_ARON_MACRO(Float, float, FLOAT, Float, float, FLOAT) \
    RUN_ARON_MACRO(Double, double, DOUBLE, Double, double, DOUBLE) \
    RUN_ARON_MACRO(String, string, STRING, String, string, STRING) \
    RUN_ARON_MACRO(Bool, bool, BOOL, Bool, bool, BOOL)

#define HANDLE_ALL_CORRESPONDING \
    HANDLE_CONTAINER_CORRESPONDING \
    HANDLE_COMPLEX_CORRESPONDING \
    HANDLE_PRIMITIVE_CORRESPONDING

#define HANDLE_CONTAINER_ARON_TYPE_ARON_ABSTRACT_TYPE_CORRESPONDING \
    RUN_ARON_MACRO(List, list, LIST, AronListSerializerType, aronlistserializertype, ARON_LIST_SERIALIZER_TYPE) \
    RUN_ARON_MACRO(Tuple, tuple, TUPLE, AronListSerializerType, aronlistserializertype, ARON_LIST_SERIALIZER_TYPE) \
    RUN_ARON_MACRO(Object, object, OBJECT, AronDictSerializerType, arondictserializertype, ARON_DICT_SERIALIZER_TYPE) \
    RUN_ARON_MACRO(Dict, dict, DICT, AronDictSerializerType, arondictserializertype, ARON_DICT_SERIALIZER_TYPE)

#define HANDLE_COMPLEX_ARON_TYPE_ARON_ABSTRACT_TYPE_CORRESPONDING \
    RUN_ARON_MACRO(EigenMatrix, eigenmatrix, EIGEN_MATRIX, AronNDArraySerializerType, aronndarrayserializertype, ARON_NDARRAY_SERIALIZER_TYPE) \
    RUN_ARON_MACRO(IVTCByteImage, ivtcbyteimage, IVT_CBYTE_IMAGE, AronNDArraySerializerType, aronndarrayserializertype, ARON_NDARRAY_SERIALIZER_TYPE) \
    RUN_ARON_MACRO(OpenCVMat, opencvmat, OPENCV_MAT, AronNDArraySerializerType, aronndarrayserializertype, ARON_NDARRAY_SERIALIZER_TYPE) \
    RUN_ARON_MACRO(PCLPointcloud, pclpointcloud, PCL_POINTCLOUD, AronNDArraySerializerType, aronndarrayserializertype, ARON_NDARRAY_SERIALIZER_TYPE)

#define HANDLE_ALL_ARON_TYPE_ARON_ABSTRACT_TYPE_CORRESPONDING \
    HANDLE_CONTAINER_ARON_TYPE_ARON_ABSTRACT_TYPE_CORRESPONDING \
    HANDLE_COMPLEX_ARON_TYPE_ARON_ABSTRACT_TYPE_CORRESPONDING

#define HANDLE_CONTAINER_ARON_DATA_ARON_ABSTRACT_TYPE_CORRESPONDING \
    RUN_ARON_MACRO(List, list, LIST, AronListSerializerType, aronlistserializertype, ARON_LIST_SERIALIZER_TYPE) \
    RUN_ARON_MACRO(Dict, dict, DICT, AronDictSerializerType, arondictserializertype, ARON_DICT_SERIALIZER_TYPE)

#define HANDLE_COMPLEX_ARON_DATA_ARON_ABSTRACT_TYPE_CORRESPONDING \
    RUN_ARON_MACRO(NDArray, ndarray, NDARRAY, AronNDArraySerializerType, aronndarrayserializertype, ARON_NDARRAY_SERIALIZER_TYPE)

#define HANDLE_ALL_ARON_DATA_ARON_ABSTRACT_TYPE_CORRESPONDING \
    HANDLE_CONTAINER_ARON_DATA_ARON_ABSTRACT_TYPE_CORRESPONDING \
    HANDLE_COMPLEX_ARON_DATA_ARON_ABSTRACT_TYPE_CORRESPONDING
