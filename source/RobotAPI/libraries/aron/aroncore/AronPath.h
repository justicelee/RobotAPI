/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include<vector>
#include<string>

// ArmarX
#include <RobotAPI/interface/aron.h>

namespace armarx
{
    namespace aron
    {
        class AronPath
        {
        public:
            AronPath();
            AronPath(const std::string&, const std::string&);
            AronPath(const AronPath&);
            AronPath(const AronPath&, const std::string&);
            AronPath(const AronPath&, const std::string&, const std::string&);

            std::vector<std::string> getPath() const;
            std::string getLastElement() const;

            void setRootIdentifier(const std::string&);
            std::string getRootIdentifier() const;

            void setDelimeter(const std::string&);
            std::string getDelimeter() const;

            std::string toString() const;

        private:
            void appendPath(const std::string&);

        private:
            std::string rootIdentifier;
            std::string delimeter;
            std::vector<std::string> path;
        };
    }
}
