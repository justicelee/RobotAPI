#ifndef ARONTESTPRODUCER_ARON_TYPE_DEFINITION_INCLUDE_GUARD
#define ARONTESTPRODUCER_ARON_TYPE_DEFINITION_INCLUDE_GUARD

#include <string>
#include <vector>
#include <map>
#include <Eigen/Core>
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/aroncore/codegenerator/AronCppClass.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/classWriters/AronDataNavigatorWriter/AronDataNavigatorWriter.h>
#include <RobotAPI/libraries/aron/aroncore/io/AronDataIO/classReaders/AronDataNavigatorReader/AronDataNavigatorReader.h>

namespace armarx
{
    class NaturalIKResult
        : virtual public armarx::aron::codegeneration::AronCppClass
    {
    public:
        std::vector<float> jointValues;
        bool reached;

    public:
        NaturalIKResult()
        {
            reset();
        }

    public:
        /**
         * @brief reset() - This method resets all member variables to default.
         * @return - nothing
         */
        virtual void reset() override
        {
            jointValues.clear();
            reached = {};
        }
        /**
         * @brief reset() - This method sets special base-class members.
         * @return - nothing
         */
        virtual void setup() override
        {
            // setup aronType
            std::map<std::string, armarx::aron::AronAbstractType::PointerType> aronTypeRoot_objectiteratorcontainer;
            armarx::aron::AronFloatType::PointerType aronTypeRoot_jointValues_listtype = armarx::aron::AronFloatType::PointerType(new armarx::aron::AronFloatType("float"));
            armarx::aron::AronListType::PointerType aronTypeRoot_jointValues = armarx::aron::AronListType::PointerType(new armarx::aron::AronListType("std::vector<float>", aronTypeRoot_jointValues_listtype));
            aronTypeRoot_objectiteratorcontainer["jointValues"] = (aronTypeRoot_jointValues);
            armarx::aron::AronBoolType::PointerType aronTypeRoot_reached = armarx::aron::AronBoolType::PointerType(new armarx::aron::AronBoolType("bool"));
            aronTypeRoot_objectiteratorcontainer["reached"] = (aronTypeRoot_reached);
            armarx::aron::AronObjectType::PointerType aronTypeRoot = armarx::aron::AronObjectType::PointerType(new armarx::aron::AronObjectType("armarx::NaturalIKResult", aronTypeRoot_objectiteratorcontainer));
            aronType = aronTypeRoot;
        }
        /**
         * @brief read() - This method returns a new type from the member data types using a writer implementation.
         * @param w - The writer implementation
         * @return - the result of the writer implementation
         */
        virtual void write(armarx::aron::io::AronWriter& w) const override
        {
            w.writeStartDict();
            w.writeKey("jointValues");
            w.writeStartList();
            for (unsigned int jointValues_listiterator = 0; jointValues_listiterator < jointValues.size(); ++jointValues_listiterator)
            {
                w.writeFloat(jointValues[jointValues_listiterator]);
            }
            w.writeEndList();
            w.writeKey("reached");
            w.writeBool(reached);
            w.writeEndDict();
        }
        /**
         * @brief read() - This method sets the struct members to new values given in a reader implementation.
         * @param r - The reader implementation
         * @return - nothing
         */
        virtual void read(armarx::aron::io::AronReader& r) override
        {
            reset();

            r.readStartDict();
            r.readMember("jointValues");
            r.readStartList();
            while (!r.readEndList())
            {
                float jointValues_listiterator;
                jointValues_listiterator = r.readFloat();
                jointValues.push_back(jointValues_listiterator);
            }
            r.readMember("reached");
            reached = r.readBool();
            r.readEndDict();
        }
        /**
         * @brief specializedWrite() - This method returns a new type from the member data types using a writer implementation.
         * @return - the result of the writer implementation
         */
        armarx::aron::AronDataPtr toAron() const
        {
            armarx::aron::io::AronDataNavigatorWriter writer;
            this->write(writer);
            return writer.getResult();
        }
        /**
         * @brief specializedRead() - This method sets the struct members to new values given in a reader implementation.
         * @return - nothing
         */
        void fromAron(const armarx::aron::AronDataPtr& input)
        {
            armarx::aron::io::AronDataNavigatorReader reader(input);
            this->read(reader);
        }
    }; // class NaturalIKResult
} // namespace armarx

#endif // ARONTESTPRODUCER_ARON_TYPE_DEFINITION_INCLUDE_GUARD

