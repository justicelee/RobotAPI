/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::aron
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::aron

#define ARMARX_BOOST_TEST

// STD/STL
#include <iostream>
#include <cstdlib>
#include <ctime>

// IVT
#include <Image/ByteImage.h>

// Eigen
#include <Eigen/Core>

// Boost
#include <boost/algorithm/string.hpp>

// Test
#include <RobotAPI/Test.h>

// ArmarX
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>

// Generated File
#include "aron/NaturalIK.aron.generated.h"
#include "aron/ListTest.aron.generated.h"
#include "aron/SimpleDictTest.aron.generated.h"
#include "aron/PrimitiveTest.aron.generated.h"
#include "aron/SimpleObjectTest.aron.generated.h"
#include "aron/ImageTest.aron.generated.h"


using namespace armarx;
using namespace aron;

void initialize_random()
{
    std::srand(std::time(nullptr));
}

int generateRandom(int max, int min)
{
    int random = (std::rand() % max) + min;
    return random;
}

std::string generateRandomWord()
{
    vector<string> words = {"Lorem", "ipsum", "dolor", "sit", "amet", "consetetur", "sadipscing", "elitr"
                            "sed", "diam", "nonumy", "eirmod", "tempor", "invidunt", "ut", "labore", "et"
                            "dolore", "magna", "aliquyam", "eratsed"
                           };

    int i = generateRandom(words.size(), 0);
    return words[i];
}

std::vector<unsigned char> generateRandomBlob(unsigned int size)
{
    std::vector<unsigned char> new_blob(size, 0);
    for (unsigned int i = 0; i < size; ++i)
    {
        new_blob[i] = (generateRandom(127, 0));
    }
    return new_blob;
}

data::AronDataPtr generateAronDataFromType(const type::AronTypePtr& type)
{
    {
        type::AronObjectTypePtr t = type::AronObjectTypePtr::dynamicCast(type);
        if (t)
        {
            data::AronDictPtr d = data::AronDictPtr(new data::AronDict());
            for (const auto& [k, tt] : t->elementTypes)
            {
                d->elements[k] = generateAronDataFromType(tt);
            }
            return d;
        }
    }
    {
        type::AronDictTypePtr t = type::AronDictTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronDict();
        }
    }
    {
        type::AronListTypePtr t = type::AronListTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronList();
        }
    }
    {
        type::AronTupleTypePtr t = type::AronTupleTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronList();
        }
    }
    {
        type::AronEigenMatrixTypePtr t = type::AronEigenMatrixTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronNDArray();
        }
    }
    {
        type::AronIntTypePtr t = type::AronIntTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronInt();
        }
    }
    {
        type::AronLongTypePtr t = type::AronLongTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronLong();
        }
    }
    {
        type::AronFloatTypePtr t = type::AronFloatTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronFloat();
        }
    }
    {
        type::AronDoubleTypePtr t = type::AronDoubleTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronDouble();
        }
    }
    {
        type::AronStringTypePtr t = type::AronStringTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronString();
        }
    }
    {
        type::AronBoolTypePtr t = type::AronBoolTypePtr::dynamicCast(type);
        if (t)
        {
            return new data::AronBool();
        }
    }
    throw LocalException("generateAronDataFromType: No valid type found!");
}

void initializeRandomly(data::AronDataPtr& data, const type::AronTypePtr& type)
{
    BOOST_CHECK_NE(data.get(), nullptr);
    BOOST_CHECK_NE(type.get(), nullptr);

    // Containers
    {
        type::AronObjectTypePtr objectType = type::AronObjectTypePtr::dynamicCast(type);
        if (objectType)
        {
            data::AronDictPtr dict = data::AronDictPtr::dynamicCast(data);
            BOOST_CHECK_NE(dict.get(), nullptr);
            for (auto& [key, nextData] : dict->elements)
            {
                initializeRandomly(nextData, objectType->elementTypes[key]);
            }
            return;
        }
    }

    {
        type::AronListTypePtr listType = type::AronListTypePtr::dynamicCast(type);
        if (listType)
        {
            data::AronListPtr list = data::AronListPtr::dynamicCast(data);
            BOOST_CHECK_NE(list.get(), nullptr);

            list->elements.clear();
            int numElements = generateRandom(1024, 0);
            for (int i = 0; i < numElements; ++i)
            {
                data::AronDataPtr newData = generateAronDataFromType(listType->acceptedType);
                initializeRandomly(newData, listType->acceptedType);
                list->elements.push_back(newData);
            }
            return;
        }
    }

    {
        type::AronDictTypePtr dictType = type::AronDictTypePtr::dynamicCast(type);
        if (dictType)
        {
            data::AronDictPtr dict = data::AronDictPtr::dynamicCast(data);
            BOOST_CHECK_NE(dict.get(), nullptr);

            dict->elements.clear();
            int numElements = generateRandom(26, 0);
            for (int i = 0; i < numElements; ++i)
            {
                std::string key = generateRandomWord();
                data::AronDataPtr newData = generateAronDataFromType(dictType->acceptedType);
                initializeRandomly(newData, dictType->acceptedType);
                dict->elements[key] = newData;
            }
            return;
        }
    }

    {
        type::AronTupleTypePtr tupleType = type::AronTupleTypePtr::dynamicCast(type);
        if (tupleType)
        {
            data::AronListPtr list = data::AronListPtr::dynamicCast(data);
            BOOST_CHECK_NE(list.get(), nullptr);
            BOOST_CHECK_EQUAL(list->elements.size(), tupleType->elementTypes.size());

            int i = 0;
            for (auto& nextData : list->elements)
            {
                initializeRandomly(nextData, tupleType->elementTypes[i++]);
            }
            return;
        }
    }

    // Complex
    {
        type::AronEigenMatrixTypePtr eigenMatrixType = type::AronEigenMatrixTypePtr::dynamicCast(type);
        if (eigenMatrixType)
        {
            data::AronNDArrayPtr blob = data::AronNDArrayPtr::dynamicCast(data);
            BOOST_CHECK_NE(blob.get(), nullptr);
            return;
        }

        type::AronIVTCByteImageTypePtr ivtCByteImageType = type::AronIVTCByteImageTypePtr::dynamicCast(type);
        if (ivtCByteImageType)
        {
            data::AronNDArrayPtr blob = data::AronNDArrayPtr::dynamicCast(data);
            BOOST_CHECK_NE(blob.get(), nullptr);

            blob->data = generateRandomBlob(blob->data.size());
            return;
        }
    }


    // Primitives
    {
        type::AronIntTypePtr primitiveType = type::AronIntTypePtr::dynamicCast(type);
        if (primitiveType)
        {
            data::AronIntPtr primitiveData = data::AronIntPtr::dynamicCast(data);
            BOOST_CHECK_NE(primitiveData.get(), nullptr);

            primitiveData->value = generateRandom(123456789, -123456789);
            return;
        }
    }
    {
        type::AronLongTypePtr primitiveType = type::AronLongTypePtr::dynamicCast(type);
        if (primitiveType)
        {
            data::AronLongPtr primitiveData = data::AronLongPtr::dynamicCast(data);
            BOOST_CHECK_NE(primitiveData.get(), nullptr);

            primitiveData->value = generateRandom(123456789, -123456789);
            return;
        }
    }
    {
        type::AronFloatTypePtr primitiveType = type::AronFloatTypePtr::dynamicCast(type);
        if (primitiveType)
        {
            data::AronFloatPtr primitiveData = data::AronFloatPtr::dynamicCast(data);
            BOOST_CHECK_NE(primitiveData.get(), nullptr);

            primitiveData->value = generateRandom(123456789, -123456789);
            return;
        }
    }
    {
        type::AronDoubleTypePtr primitiveType = type::AronDoubleTypePtr::dynamicCast(type);
        if (primitiveType)
        {
            data::AronDoublePtr primitiveData = data::AronDoublePtr::dynamicCast(data);
            BOOST_CHECK_NE(primitiveData.get(), nullptr);

            primitiveData->value = generateRandom(123456789, -123456789);
            return;
        }
    }
    {
        type::AronStringTypePtr primitiveType = type::AronStringTypePtr::dynamicCast(type);
        if (primitiveType)
        {
            data::AronStringPtr primitiveData = data::AronStringPtr::dynamicCast(data);
            BOOST_CHECK_NE(primitiveData.get(), nullptr);

            primitiveData->value = generateRandomWord();
            return;
        }
    }
    {
        type::AronBoolTypePtr primitiveType = type::AronBoolTypePtr::dynamicCast(type);
        if (primitiveType)
        {
            data::AronBoolPtr primitiveData = data::AronBoolPtr::dynamicCast(data);
            BOOST_CHECK_NE(primitiveData.get(), nullptr);

            primitiveData->value = generateRandom(1, 0);
            return;
        }
    }
    throw LocalException("initializeRandomly: No valid type found!");
}

template <typename T>
void runTestWithInstances(T& k, T& k2)
{
    std::cout << "\t getting type 1" << std::endl;
    type::AronTypePtr k_type = k.toAronType();
    BOOST_CHECK_NE(k_type.get(), nullptr);

    std::cout << "\t getting type 2" << std::endl;
    type::AronTypePtr k2_type = k2.toAronType();
    BOOST_CHECK_NE(k_type.get(), nullptr);

    std::cout << "\t getting aron 1" << std::endl;
    data::AronDataPtr k_aron = k.toAron();
    BOOST_CHECK_NE(k_aron.get(), nullptr);

    std::cout << "\t initialize aron 1 randomly" << std::endl;
    initializeRandomly(k_aron, k_type);

    std::cout << "\t setting aron 2 from aron 1" << std::endl;
    k2.fromAron(k_aron);

    std::cout << "\t getting aron 2" << std::endl;
    data::AronDataPtr k2_aron = k2.toAron();
    BOOST_CHECK_NE(k2_aron.get(), nullptr);

    std::cout << "\t setting aron 1 from aron 2 and check for equality" << std::endl;
    k.fromAron(k2_aron);
    BOOST_CHECK_EQUAL((k == k2), true);
}

BOOST_AUTO_TEST_CASE(AronTests)
{
    initialize_random();

    std::cout << "Running NaturalIK test" << std::endl;
    NaturalIKResult k;
    NaturalIKResult k2;
    runTestWithInstances<NaturalIKResult>(k, k2);

    std::cout << "Running List test" << std::endl;
    ListTest l;
    ListTest l2;
    runTestWithInstances<ListTest>(l, l2);

    std::cout << "Running Dict test" << std::endl;
    DictTest d;
    DictTest d2;
    runTestWithInstances<DictTest>(d, d2);

    std::cout << "Running Primitive test" << std::endl;
    PrimitiveTest p;
    PrimitiveTest p2;
    runTestWithInstances<PrimitiveTest>(p, p2);

    std::cout << "Running SimpleClass test" << std::endl;
    SimpleClass o1;
    SimpleClass o12;
    runTestWithInstances<SimpleClass>(o1, o12);

    std::cout << "Running Object test" << std::endl;
    ObjectTest o2;
    ObjectTest o22;
    runTestWithInstances<ObjectTest>(o2, o22);

    std::cout << "Running IK test" << std::endl;
    ImageTest i;
    ImageTest i2;
    runTestWithInstances<ImageTest>(i, i2);
}
