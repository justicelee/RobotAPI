/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>

namespace armarx
{
    namespace aron
    {
        namespace exception
        {
            class AronException :
                public armarx::LocalException
            {
            public:
                AronException() = delete;
                AronException(const std::string& caller, const std::string& method, const std::string& reason) :
                    LocalException(caller + "::" + method + ": " + reason) // add reason and generate backtrace
                {

                }
            };


            /////////////////////////////////////////
            /// Aron Type and Data Exceptions ///////
            /////////////////////////////////////////
            class AronInvalidAbstractTypeException :
                public armarx::aron::exception::AronException
            {
            public:
                AronInvalidAbstractTypeException() = delete;
                AronInvalidAbstractTypeException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronInvalidDataException :
                public armarx::aron::exception::AronException
            {
            public:
                AronInvalidDataException() = delete;
                AronInvalidDataException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronTypeIsInconsistentException :
                public armarx::aron::exception::AronException
            {
            public:
                AronTypeIsInconsistentException() = delete;
                AronTypeIsInconsistentException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronDataIsInconsistentException :
                public armarx::aron::exception::AronException
            {
            public:
                AronDataIsInconsistentException() = delete;
                AronDataIsInconsistentException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };


            /////////////////////////////////////////
            /// Aron NullPtr Exceptions /////////////
            /////////////////////////////////////////
            class AronPtrIsNullException :
                public armarx::aron::exception::AronException
            {
            public:
                AronPtrIsNullException() = delete;
                AronPtrIsNullException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronDataPtrIsNullException :
                public armarx::aron::exception::AronPtrIsNullException
            {
            public:
                AronDataPtrIsNullException() = delete;
                AronDataPtrIsNullException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronPtrIsNullException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronTypePtrIsNullException :
                public armarx::aron::exception::AronPtrIsNullException
            {
            public:
                AronTypePtrIsNullException() = delete;
                AronTypePtrIsNullException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronPtrIsNullException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronTypeNotFoundException :
                public armarx::aron::exception::AronException
            {
            public:
                AronTypeNotFoundException() = delete;
                AronTypeNotFoundException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronTypeAreadyExistsException :
                public armarx::aron::exception::AronException
            {
            public:
                AronTypeAreadyExistsException() = delete;
                AronTypeAreadyExistsException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };

            class AronTypeAcceptedTypeNotFoundException :
                public armarx::aron::exception::AronException
            {
            public:
                AronTypeAcceptedTypeNotFoundException() = delete;
                AronTypeAcceptedTypeNotFoundException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };


            /////////////////////////////////////////
            /// Aron Factories //////////////////////
            /////////////////////////////////////////
            class AronFactoryNotFoundException :
                public armarx::aron::exception::AronException
            {
            public:
                AronFactoryNotFoundException() = delete;
                AronFactoryNotFoundException(const std::string& caller, const std::string& method, const std::string& reason) :
                    AronException(caller, method, reason) // add reason and generate backtrace
                {

                }
            };
        }
    }
}
