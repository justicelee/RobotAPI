/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once
#include <SimoxUtility/math/distance/delta_angle.h>

#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/IK/DifferentialIK.h>
#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>

#include <memory>


namespace armarx
{


    typedef std::shared_ptr<class CompositeDiffIK> CompositeDiffIKPtr;
    class CompositeDiffIK
    {
    public:
        struct Parameters
        {
            Parameters() {}
            // IK params
            size_t steps = 40;

            float maxJointAngleStep = 0.2f;
            float stepSize = 0.5f;
            bool resetRnsValues = true;
            bool returnIKSteps = false;
            float jointRegularizationTranslation = 1000;
            float jointRegularizationRotation = 1;
        };

        class NullspaceGradient
        {
        public:
            virtual void init(Parameters& params) = 0;
            virtual Eigen::VectorXf getGradient(Parameters& params) = 0;
            float kP = 1;
        };
        typedef std::shared_ptr<class NullspaceGradient> NullspaceGradientPtr;

        struct NullspaceTargetStep
        {
            Eigen::Vector3f posDiff;
            Eigen::Vector3f oriDiff;
            Eigen::Matrix4f tcpPose;
            Eigen::VectorXf cartesianVel;
            Eigen::VectorXf jointVel;
        };
        class NullspaceTarget : public NullspaceGradient
        {
        public:
            NullspaceTarget(const VirtualRobot::RobotNodeSetPtr& rns, const VirtualRobot::RobotNodePtr& tcp, const Eigen::Matrix4f& target, VirtualRobot::IKSolver::CartesianSelection mode);

            VirtualRobot::RobotNodePtr tcp;
            Eigen::Matrix4f target;
            VirtualRobot::IKSolver::CartesianSelection mode;
            CartesianPositionController pCtrl;
            CartesianVelocityController vCtrl;

            void init(Parameters&) override;
            Eigen::VectorXf getGradient(Parameters& params) override;

            std::vector<NullspaceTargetStep> ikSteps;
        };
        typedef std::shared_ptr<class NullspaceTarget> NullspaceTargetPtr;

        class NullspaceJointTarget : public NullspaceGradient
        {
        public:
            NullspaceJointTarget(const VirtualRobot::RobotNodeSetPtr& rns);
            NullspaceJointTarget(const VirtualRobot::RobotNodeSetPtr& rns, const Eigen::VectorXf& target, const Eigen::VectorXf& weight);
            void set(int index, float target, float weight);
            void set(const std::string& jointName, float target, float weight);
            void set(const VirtualRobot::RobotNodePtr& rn, float target, float weight);

            VirtualRobot::RobotNodeSetPtr rns;
            Eigen::VectorXf target;
            Eigen::VectorXf weight;

            void init(Parameters&) override;
            Eigen::VectorXf getGradient(Parameters& params) override;
        };
        typedef std::shared_ptr<class NullspaceJointTarget> NullspaceJointTargetPtr;

        class NullspaceJointLimitAvoidance : public NullspaceGradient
        {
        public:
            NullspaceJointLimitAvoidance(const VirtualRobot::RobotNodeSetPtr& rns);
            NullspaceJointLimitAvoidance(const VirtualRobot::RobotNodeSetPtr& rns, const Eigen::VectorXf& weight);
            void setWeight(int index, float weight);
            void setWeight(const std::string& jointName, float weight);
            void setWeight(const VirtualRobot::RobotNodePtr& rn, float weight);
            void setWeights(const VirtualRobot::RobotNodeSetPtr& rns, float weight);


            VirtualRobot::RobotNodeSetPtr rns;
            Eigen::VectorXf weight;

            void init(Parameters&) override;
            Eigen::VectorXf getGradient(Parameters& params) override;
        };
        typedef std::shared_ptr<class NullspaceJointLimitAvoidance> NullspaceJointLimitAvoidancePtr;

        struct TargetStep
        {
            Eigen::Vector3f posDiff;
            Eigen::Vector3f oriDiff;
            Eigen::Matrix4f tcpPose;
            Eigen::VectorXf cartesianVel;
        };

        class Target
        {
        public:
            Target(const VirtualRobot::RobotNodeSetPtr& rns, const VirtualRobot::RobotNodePtr& tcp, const Eigen::Matrix4f& target, VirtualRobot::IKSolver::CartesianSelection mode);
            VirtualRobot::RobotNodePtr tcp;
            Eigen::Matrix4f target;
            VirtualRobot::IKSolver::CartesianSelection mode;
            Eigen::MatrixXf jacobi;
            CartesianPositionController pCtrl;
            float maxPosError = 10.f;
            float maxOriError = 0.05f;

            std::vector<TargetStep> ikSteps;

            Eigen::Matrix4f getPoseError() const
            {
                return tcp->getPoseInRootFrame().inverse() * target;
            }
            Eigen::Vector3f getPosError() const
            {
                return (tcp->getPoseInRootFrame().inverse() * target).topRightCorner<3, 1>();
            }
            auto getOriError() const
            {
                return simox::math::delta_angle(tcp->getPoseInRootFrame(), target);
            }
        };
        typedef std::shared_ptr<Target> TargetPtr;




        struct Result
        {
            Eigen::VectorXf jointValues;
            Eigen::Vector3f posDiff;
            Eigen::Vector3f posDiffElbow;
            Eigen::Vector3f oriDiff;
            float posError;
            float posErrorElbow;
            float oriError;
            bool reached;
            Eigen::VectorXf jointLimitMargins;
            float minimumJointLimitMargin;
            Eigen::Vector3f elbowPosDiff;
        };

        struct SolveState
        {
            int rows = 0;
            int cols = 0;
            Eigen::VectorXf jointRegularization;
            Eigen::VectorXf cartesianRegularization;
            Eigen::VectorXf jointValues;
            Eigen::MatrixXf jacobi;
            Eigen::MatrixXf invJac;
            Eigen::MatrixXf nullspace;

        };

        static Eigen::VectorXf LimitInfNormTo(Eigen::VectorXf vec, float maxValue);

        CompositeDiffIK(const VirtualRobot::RobotNodeSetPtr& rns);

        void addTarget(const TargetPtr& target);
        TargetPtr addTarget(const VirtualRobot::RobotNodePtr& tcp, const Eigen::Matrix4f& target, VirtualRobot::IKSolver::CartesianSelection mode);

        void addNullspaceGradient(const NullspaceGradientPtr& gradient);
        NullspaceTargetPtr addNullspacePositionTarget(const VirtualRobot::RobotNodePtr& tcp, const Eigen::Vector3f& target);

        Result solve(Parameters params);
        static Eigen::MatrixXf CalculateNullspaceSVD(const Eigen::Matrix4f& jacobi);
        static Eigen::MatrixXf CalculateNullspaceLU(const Eigen::Matrix4f& jacobi);
        void step(Parameters& params, SolveState& s, int stepNr);

        int CartesianSelectionToSize(VirtualRobot::IKSolver::CartesianSelection mode);



    private:
        VirtualRobot::RobotNodeSetPtr rns;
        std::vector<TargetPtr> targets;
        std::vector<NullspaceGradientPtr> nullspaceGradients;
        VirtualRobot::DifferentialIKPtr ik;

    };
}
