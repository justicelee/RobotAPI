/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     SecondHands Demo (shdemo at armar6)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimpleDiffIK.h"

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>

#include <cfloat>

namespace armarx
{
    SimpleDiffIK::Result SimpleDiffIK::CalculateDiffIK(const Eigen::Matrix4f targetPose, VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp, Parameters params)
    {

        tcp = tcp ? tcp : rns->getTCP();
        CartesianVelocityController velocityController(rns);
        CartesianPositionController positionController(tcp);

        if (params.resetRnsValues)
        {
            for (VirtualRobot::RobotNodePtr rn : rns->getAllRobotNodes())
            {
                if (rn->isLimitless())
                {
                    rn->setJointValue(0);
                }
                else
                {
                    rn->setJointValue((rn->getJointLimitHi() + rn->getJointLimitLo()) / 2);
                }
            }
        }

        std::vector<IKStep> ikSteps;
        Eigen::VectorXf currentJointValues = rns->getJointValuesEigen();
        for (size_t i = 0; i <= params.stepsInitial + params.stepsFineTune; i++)
        {
            Eigen::Vector3f posDiff = positionController.getPositionDiff(targetPose);
            Eigen::Vector3f oriDiff = positionController.getOrientationDiff(targetPose);

            //ARMARX_IMPORTANT << VAROUT(posDiff) << VAROUT(oriDiff);

            Eigen::VectorXf cartesianVel(6);
            cartesianVel << posDiff(0), posDiff(1), posDiff(2), oriDiff(0), oriDiff(1), oriDiff(2);
            const Eigen::VectorXf jnv = params.jointLimitAvoidanceKp * velocityController.calculateJointLimitAvoidance();
            const Eigen::VectorXf jv = velocityController.calculate(cartesianVel, jnv, VirtualRobot::IKSolver::All);



            float stepLength = i < params.stepsInitial ? params.ikStepLengthInitial : params.ikStepLengthFineTune;
            Eigen::VectorXf jvClamped = jv * stepLength;

            float infNorm = jvClamped.lpNorm<Eigen::Infinity>();
            if (infNorm > params.maxJointAngleStep)
            {
                jvClamped = jvClamped / infNorm * params.maxJointAngleStep;
            }

            if (params.returnIKSteps)
            {
                IKStep s;
                s.posDiff = posDiff;
                s.oriDiff = oriDiff;
                s.cartesianVel = cartesianVel;
                s.jnv = jnv;
                s.jv = jv;
                s.infNorm = infNorm;
                s.jvClamped = jvClamped;
                ikSteps.emplace_back(s);
            }


            Eigen::VectorXf newJointValues = currentJointValues + jvClamped;
            rns->setJointValues(newJointValues);
            currentJointValues = newJointValues;
        }

        Result result;
        result.ikSteps = ikSteps;
        result.jointValues = rns->getJointValuesEigen();
        result.posDiff = positionController.getPositionDiff(targetPose);
        result.oriDiff = positionController.getOrientationDiff(targetPose);
        result.posError = positionController.getPositionError(targetPose);
        result.oriError = positionController.getOrientationError(targetPose);
        result.reached = result.posError < params.maxPosError && result.oriError < params.maxOriError;

        result.jointLimitMargins = Eigen::VectorXf::Zero(rns->getSize());
        result.minimumJointLimitMargin = FLT_MAX;
        for (size_t i = 0; i < rns->getSize(); i++)
        {
            VirtualRobot::RobotNodePtr rn = rns->getNode(i);
            if (rn->isLimitless())
            {
                result.jointLimitMargins(i) = M_PI;
            }
            else
            {
                result.jointLimitMargins(i) = std::min(rn->getJointValue() - rn->getJointLimitLo(), rn->getJointLimitHi() - rn->getJointValue());
                result.minimumJointLimitMargin = std::min(result.minimumJointLimitMargin, result.jointLimitMargins(i));
            }
        }

        return result;
    }

    SimpleDiffIK::Reachability SimpleDiffIK::CalculateReachability(const std::vector<Eigen::Matrix4f> targets, const Eigen::VectorXf& initialJV, VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp, SimpleDiffIK::Parameters params)
    {
        Reachability r;
        rns->setJointValues(initialJV);
        for (const Eigen::Matrix4f& target : targets)
        {
            Result res = CalculateDiffIK(target, rns, tcp, params);
            r.aggregate(res);
        }
        if (!r.reachable)
        {
            r.minimumJointLimitMargin = -1;
        }
        return r;
    }

    SimpleDiffIKProvider::SimpleDiffIKProvider(VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp, SimpleDiffIK::Parameters params)
        : rns(rns), tcp(tcp), params(params)
    {
    }

    DiffIKResult SimpleDiffIKProvider::SolveAbsolute(const Eigen::Matrix4f& targetPose)
    {
        params.resetRnsValues = true;
        SimpleDiffIK::Result result = SimpleDiffIK::CalculateDiffIK(targetPose, rns, tcp, params);
        DiffIKResult r;
        r.jointValues = rns->getJointValuesEigen();
        r.oriError = result.oriError;
        r.posError = result.posError;
        r.reachable = result.reached;
        return r;

    }

    DiffIKResult SimpleDiffIKProvider::SolveRelative(const Eigen::Matrix4f& targetPose, const Eigen::VectorXf& startJointValues)
    {
        params.resetRnsValues = false;
        rns->setJointValues(startJointValues);
        SimpleDiffIK::Result result = SimpleDiffIK::CalculateDiffIK(targetPose, rns, tcp, params);
        DiffIKResult r;
        r.jointValues = rns->getJointValuesEigen();
        r.oriError = result.oriError;
        r.posError = result.posError;
        r.reachable = result.reached;
        return r;
    }

}
