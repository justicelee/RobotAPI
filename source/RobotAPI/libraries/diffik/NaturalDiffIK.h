/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/IK/DifferentialIK.h>


namespace armarx
{
    class NaturalDiffIK
    {
    public:
        enum class Mode {Position, All};
        struct Parameters
        {
            Parameters() {}
            // IK params
            float ikStepLengthInitial = 0.2f;
            float ikStepLengthFineTune = 0.5f;
            size_t stepsInitial = 25;
            size_t stepsFineTune = 10;
            float maxPosError = 10.f;
            float maxOriError = 0.05f;
            float jointLimitAvoidanceKp = 0.f;
            float elbowKp = 1.0f;
            float maxJointAngleStep = 0.1f;
            bool resetRnsValues = true;
            bool returnIKSteps = false;
        };
        struct IKStep
        {
            Eigen::VectorXf jointValues;
            Eigen::Vector3f pdTcp;
            Eigen::Vector3f odTcp;
            Eigen::Vector3f pdElb;
            Eigen::Matrix4f tcpPose;
            Eigen::Matrix4f elbPose;
            Eigen::VectorXf cartesianVel;
            Eigen::VectorXf cartesianVelElb;
            Eigen::VectorXf jvElb;
            Eigen::VectorXf jvLA;
            Eigen::VectorXf jv;
            Eigen::VectorXf jvClamped;
        };

        struct Result
        {
            Eigen::VectorXf jointValues;
            Eigen::Vector3f posDiff;
            Eigen::Vector3f posDiffElbow;
            Eigen::Vector3f oriDiff;
            float posError;
            float posErrorElbow;
            float oriError;
            bool reached;
            Eigen::VectorXf jointLimitMargins;
            float minimumJointLimitMargin;
            Eigen::Vector3f elbowPosDiff;
            std::vector<IKStep> ikSteps;
        };

        static Eigen::VectorXf LimitInfNormTo(Eigen::VectorXf vec, float maxValue);
        static Result CalculateDiffIK(const Eigen::Matrix4f& targetPose, const Eigen::Vector3f& elbowTarget, VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp, VirtualRobot::RobotNodePtr elbow, Mode setOri, Parameters params = Parameters());
        static VirtualRobot::IKSolver::CartesianSelection ModeToCartesianSelection(Mode mode);
    };
}
