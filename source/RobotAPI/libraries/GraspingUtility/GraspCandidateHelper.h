/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <boost/shared_ptr.hpp>

#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>

#include <VirtualRobot/Robot.h>

#include <RobotAPI/libraries/core/Pose.h>

namespace armarx
{
    typedef boost::shared_ptr<class GraspCandidateHelper> GraspCandidateHelperPtr;

    class GraspCandidateHelper
    {
    public:
        GraspCandidateHelper(const grasping::GraspCandidatePtr& candidate, VirtualRobot::RobotPtr robot);
        GraspCandidateHelper() = default;
        GraspCandidateHelper(GraspCandidateHelper&&) = default;
        GraspCandidateHelper(const GraspCandidateHelper&) = default;
        GraspCandidateHelper& operator=(GraspCandidateHelper&&) = default;
        GraspCandidateHelper& operator=(const GraspCandidateHelper&) = default;

        Eigen::Matrix4f getGraspPoseInRobotRoot() const;
        Eigen::Matrix4f getPrePoseInRobotRoot(float approachDistance) const;
        Eigen::Matrix3f getGraspOrientationInRobotRoot() const;
        Eigen::Vector3f getGraspPositionInRobotRoot() const;

        Eigen::Matrix4f getGraspPoseInGlobal() const;
        Eigen::Matrix3f getGraspOrientationInGlobal() const;
        Eigen::Vector3f getGraspPositionInGlobal() const;

        Eigen::Vector3f getApproachVector() const;
        bool isTopGrasp();
        bool isSideGrasp();

        const grasping::GraspCandidatePtr getGraspCandidate() const
        {
            return candidate;
        }
        void setGraspCandidate(const grasping::GraspCandidatePtr& p);

        Eigen::Vector3f defrost(const Vector3BasePtr& base) const
        {
            return Vector3Ptr::dynamicCast(base)->toEigen();
        }
        Eigen::Matrix4f defrost(const PoseBasePtr& base) const
        {
            return PosePtr::dynamicCast(base)->toEigen();
        }

        const VirtualRobot::RobotPtr& getRobot() const
        {
            return robot;
        }

    private:
        grasping::GraspCandidatePtr candidate;
        VirtualRobot::RobotPtr robot;
    };
}
