/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspCandidateHelper.h"
#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

using namespace armarx;


GraspCandidateHelper::GraspCandidateHelper(const grasping::GraspCandidatePtr& candidate, VirtualRobot::RobotPtr robot)
    : candidate(candidate), robot(robot)
{
    ARMARX_CHECK_NOT_NULL(candidate);
    ARMARX_CHECK_NOT_NULL(robot);
}

Eigen::Matrix4f GraspCandidateHelper::getGraspPoseInRobotRoot() const
{
    const Eigen::Matrix4f curRobotPose = robot->getGlobalPose();
    const Eigen::Matrix4f graspPose = curRobotPose.inverse() * getGraspPoseInGlobal();
    return graspPose;
}

Eigen::Matrix4f GraspCandidateHelper::getPrePoseInRobotRoot(float approachDistance) const
{
    return math::Helpers::TranslatePose(getGraspPoseInRobotRoot(), getApproachVector() * approachDistance);
}

Eigen::Matrix3f GraspCandidateHelper::getGraspOrientationInRobotRoot() const
{
    return getGraspPoseInRobotRoot().topLeftCorner<3, 3>();
}

Eigen::Vector3f GraspCandidateHelper::getGraspPositionInRobotRoot() const
{
    return math::Helpers::GetPosition(getGraspPoseInRobotRoot());
}

Eigen::Matrix4f GraspCandidateHelper::getGraspPoseInGlobal() const
{
    const Eigen::Matrix4f oldGraspPose = defrost(candidate->graspPose);
    const Eigen::Matrix4f oldRobotPose = defrost(candidate->robotPose);
    return oldRobotPose * oldGraspPose;
}
Eigen::Matrix3f GraspCandidateHelper::getGraspOrientationInGlobal() const
{
    return getGraspPoseInGlobal().topLeftCorner<3, 3>();
}
Eigen::Vector3f GraspCandidateHelper::getGraspPositionInGlobal() const
{
    return math::Helpers::GetPosition(getGraspPoseInGlobal());
}

Eigen::Vector3f GraspCandidateHelper::getApproachVector() const
{
    return defrost(candidate->approachVector);
}

bool GraspCandidateHelper::isTopGrasp()
{
    return candidate->executionHints->approach == grasping::ApproachType::TopApproach;
}

bool GraspCandidateHelper::isSideGrasp()
{
    return candidate->executionHints->approach == grasping::ApproachType::SideApproach;
}


void GraspCandidateHelper::setGraspCandidate(const grasping::GraspCandidatePtr& p)
{
    ARMARX_CHECK_NOT_NULL(p);
    candidate = p;
}
