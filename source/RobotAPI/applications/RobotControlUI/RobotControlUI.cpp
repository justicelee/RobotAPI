/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Applications
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotControlUI.h"
#include <ArmarXCore/core/application/Application.h>

#include <iostream>
#include <ArmarXCore/observers/ObserverObjectFactories.h>

namespace armarx
{
    void RobotControlUI::onInitComponent()
    {
        usingProxy("RobotControl");
        stateId = -1;
        controlTask = new RunningTask<RobotControlUI>(this, &RobotControlUI::run, "RobotControlUI");
    }

    void RobotControlUI::onConnectComponent()
    {
        robotProxy = getProxy<RobotControlInterfacePrx>("RobotControl");
        controlTask->start();
    }

    void RobotControlUI::onExitComponent()
    {
        controlTask->stop();
    }

    void RobotControlUI::run()
    {
        std::string eventstring;
        std::cout << "Please insert the event string: " <<  std::flush;
        //    cin >> eventstring;
        eventstring = "EvLoadScenario";

        if (eventstring == "q")
        {
            //        shutdown();
        }
        else
        {
            std::cout << "Please insert the state id of the state that should process the event: " <<  std::flush;
            int id;
            //        cin >> id;
            id = 11;
            std::cout << "sending to id:" << id << std::endl;
            EventPtr evt = new Event("EVENTTOALL", eventstring);
            StateUtilFunctions::addToDictionary(evt, "proxyName", "RemoteStateOfferer");
            StateUtilFunctions::addToDictionary(evt, "stateName", "MoveArm");
            //robotProxy->issueEvent(id, evt);
        }

        //    cin >> eventstring;
    }
}
